<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->unsignedBigInteger('ticket_category_id')->nullable();
            $table->foreign('ticket_category_id')->references('id')->on('ticket_categories');
            $table->unsignedBigInteger('ticket_sub_category_id')->nullable();
            $table->foreign('ticket_sub_category_id')->references('id')->on('ticket_sub_categories');
            $table->text('subject')->nullable();
            $table->text('message')->nullable();
            $table->enum('status',['active','close'])->default('active');
            $table->enum('is_delete',['1','0'])->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
}
