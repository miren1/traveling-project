<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMandateRegistrationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mandate_registrations', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('memberships')->onDelete('cascade');
            $table->string('txn_status')->nullable();
            $table->string('txn_msg')->nullable();
            $table->string('txn_err_msg')->nullable();
            $table->string('clnt_txn_ref')->nullable();
            $table->string('tpsl_bank_cd')->nullable();
            $table->string('tpsl_txn_id')->nullable();
            $table->string('txn_amt')->nullable();
            $table->string('tpsl_txn_time')->nullable();
            $table->string('bal_amt')->nullable();
            $table->string('card_id')->nullable();
            $table->string('alias_name')->nullable();
            $table->string('BankTransactionID')->nullable();
            $table->string('mandate_reg_no')->nullable();
            $table->string('token')->nullable();
            $table->string('hash')->nullable();
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mandate_registrations');
    }
}
