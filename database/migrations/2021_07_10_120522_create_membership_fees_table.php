<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMembershipFeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('membership_fees', function (Blueprint $table) {
            $table->id();
            $table->integer('member_id');
            $table->foreign('member_id')->references('member_id')->on('memberships');
            $table->string('payment_id');
            $table->integer('payment_amount');
            $table->string('reference_id');
            $table->string('tx_status');
            $table->string('payment_mode');
            $table->string('tx_msg');
            $table->string('tx_time');
            $table->string('signature');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('membership_fees');
    }
}
