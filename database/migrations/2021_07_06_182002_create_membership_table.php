<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMembershipTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('membership', function (Blueprint $table) {
            $table->id();
            $table->BigInteger('plan_id')->nullable();
            $table->foreign('plan_id')->references('id')->on('plans');
            $table->integer('member_id')->nullable();
            $table->text('prefix');
            $table->text('name');
            $table->text('dob');
            $table->string('adharno');
            $table->string('panno');
            $table->string('passportno');
            $table->date('passportdate');
            $table->string('occupation');
            $table->string('email');
            $table->string('annual_income');
            $table->string('primary_contact');
            $table->string('secondary_contact');
            $table->string('whatsapp_number');
            $table->string('resi_address');
            $table->string('city');
            $table->string('state');
            $table->string('pincode');
            $table->enum('jj_tip', ['facebook', 'instagram', 'linkedin', 'twitter', 'newspaper', 'email', 'member', 'agentname', 'employeeref', 'other']);
            $table->string('photo');
            $table->string('password');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('membership');
    }
}
