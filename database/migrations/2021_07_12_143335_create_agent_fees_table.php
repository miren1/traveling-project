<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgentFeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agent_fees', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('agent_id');
            $table->foreign('agent_id')->references('id')->on('agents');
            $table->string('payment_id');
            $table->integer('payment_amount');
            $table->string('reference_id');
            $table->string('tx_status');
            $table->string('payment_mode');
            $table->string('tx_msg');
            $table->string('tx_time');
            $table->enum('type', ['fees', 'recharge']);
            $table->string('signature');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agent_fees');
    }
}
