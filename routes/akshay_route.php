<?php 

// front routes

Route::get('/','front\HomeController@new_index')->name('home');
Route::get('/login','FrontLoginController@index')->name('login');
Route::post('/login','FrontLoginController@Login')->name('front.login');
Route::post('/login/phone','FrontLoginController@phoneCheck')->name('phone.check');
Route::post('/login/mobileOtp','FrontLoginController@LoginMobileOtp')->name('phone.loginOtp');
Route::post('/login/emailOtp','FrontLoginController@LoginEmailOtp')->name('email.loginOtp');
Route::post('/login/OTP','FrontLoginController@LoginOTP')->name('login.OTP');
Route::post('/login/email','FrontLoginController@emailCheck')->name('email.check');
Route::post('/signup','FrontLoginController@SignUp')->name('signup');
Route::post('/signup/OTP','front\SignupController@SignupOTP')->name('signup.OTP');
Route::post('/signup/mobileOtp','front\SignupController@GetOtp')->name('signup.mobileotp');
Route::post('/signup/emailOtp','front\SignupController@GetEmailOtp')->name('signup.emailotp');
Route::post('/agent-signup','FrontLoginController@AgentSignUp')->name('agent.signup');
Route::any('customer/email/{id}','FrontLoginController@checkEmail');
Route::any('invite/email/{id}','FrontLoginController@InvitecheckEmail');
Route::any('customer/allphone/{id}','FrontLoginController@check_Phone');
Route::any('customer/phone/{id}','FrontLoginController@checkPhone');
Route::any('agent/phone/{id}','FrontLoginController@AgentcheckPhone');
Route::any('agent/email/{id}','FrontLoginController@AgentcheckEmail');

// front other pages

Route::get('/about-us','front\HomeController@AboutUs')->name('about-us');
Route::get('/blog','front\HomeController@Blog')->name('blog');
Route::get('/gallery','front\HomeController@Gallery')->name('gallery');
Route::get('/FAQ','front\HomeController@FAQ')->name('FAQ');
Route::get('/terms-condition','front\HomeController@TermsCondition')->name('terms-condition');
Route::get('/privacy-policy','front\HomeController@PrivacyPolicy')->name('privacy-policy');
Route::get('/career','front\HomeController@Careers')->name('career');
Route::get('/membership-plan-detail','front\HomeController@MembershipPlandetail')->name('plan.detail');

Route::get('/membership-detail','front\HomeController@MembershipIndex')->name('membership.detail');

Route::get('/get-membership','front\HomeController@getAllMember')->name('getall.member');

Route::get('/become-member/{id}','front\MemberController@BecomeMember')->name('become-member');

//Route::get('/invite-member/{id}','front\MemberController@BecomeMember')->name('invite-member');
Route::post('/store-member','front\MemberController@Store')->name('store-member');
Route::any('/mandate/{id}','front\MemberController@mandate_registration')->name('mandate-regi');
Route::any('/mandate_response/{id}/{merchant_id}/{txt_id}/{consumer_id}','front\MemberController@mandate_response')->name('mandate-response');

//Route::any('/mendate', 'CommonController@mendate_registration');

Route::any('/payment_returnURL/{id}/{type}','front\MemberController@ReturnUrl');
Route::any('/payment_notifyURL','front\MemberController@NotifyUrl')->name('notify-url');
//Route::any('/success_payment','front\MemberController@NotifyUrl')->name('success-notify-url');
//Route::any('/error_payment','front\MemberController@NotifyUrl')->name('error-notify-url');

Route::post('/leave/show','LeaveController@show')->name('leave.show');

Route::prefix('tour')->group(function () {
    Route::get('/plans/{id?}','front\TourController@index')->name('tour.plans');


});
// Logout
Route::get('/front-logout',function(){
    Auth::guard('webagent')->logout();
    Auth::guard('webcustomer')->logout();
    Auth::guard('webmember')->logout();
    return redirect()->route('login');
})->name('front.logout');




