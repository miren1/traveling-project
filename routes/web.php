<?php

//dd('hello');

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Route::get('/', function () {
//     return view('welcome');
// });
// Auth::routes();
// Route::get('/home', 'HomeController@index')->name('home');
// Auth::routes();
// Route::get('/home', 'HomeController@index')->name('home');

Route::get('/clear-cache', function () {
    Artisan::call('cache:clear');
    Artisan::call('route:clear');
    Artisan::call('config:clear');
    Artisan::call('view:clear');
    return "Cache is clear";
});




Route::any('/admindLoginCheck', 'AdminController@admindLoginCheck')->name('admindLoginCheck');
/**All Admin Routes  */

Route::any('/adminlogin', 'AdminController@adminLogin')->name('adminlogin');

Route::group(['middleware' => 'CheckAdminSession'], function () {

    Route::any('/admindashboard', 'AdminController@dashboard')->name('admindashboard');
    Route::any('/MailTamplate', 'AdminController@MailTamplate')->name('MailTamplate');
    Route::any('/adminlogout', 'AdminController@adminlogout')->name('adminlogout');
    /***************Admin Notification********************/
    // (Last Three Month Request Admin Can Approve)
    Route::any('/allrequest', 'AdminController@allRequest')->name('allrequest');
    Route::any('/inquirydetails/{id}', 'AdminController@inquirydetails')->name('inquirydetails');
    Route::any('/inquirydelete/{id}', 'AdminController@inquirydelete')->name('inquirydelete');
    Route::any('/allinquiry', 'AdminController@allinquiry')->name('allinquiry');
    Route::any('/inquirymail/{id}', 'AdminController@inquirymail')->name('inquirymail');
    Route::any('/editinquiry/{id}', 'AdminController@editinquiry')->name('editinquiry');
    Route::any('/adminEditInquiry', 'AdminController@adminEditInquiry')->name('adminEditInquiry');
    /*****************Photo Image Gallery *************************** */
    Route::any('/deleteimage/{id}', 'CommonController@deleteimage')->name('deleteimage');
    Route::any('/amintourinsertvideo/{id}', 'CommonController@amintourinsertvideo')->name('amintourinsertvideo');
    Route::any('/insertadminvideo/{id}', 'CommonController@insertadminvideo')->name('insertadminvideo');
    Route::any('/tourdelete/{id}', 'CommonController@tourdelete')->name('tourdelete');
    Route::any('/updatetour/{id}', 'CommonController@updatetour')->name('updatetour');
    Route::any('/inserttour', 'CommonController@inserttour')->name('inserttour');
    Route::post('/inserttouradmin', 'CommonController@inserttouradmin')->name('inserttouradmin');
    Route::post('/additenary', 'CommonController@additenary')->name('additenary');
    Route::any('/itenaryshow', 'CommonController@itenaryshow')->name('itenaryshow');
    Route::any('/itenaryinfo', 'CommonController@itenaryinfo')->name('itenaryinfo');
    Route::any('/allitenaryinfo', 'CommonController@allitenaryinfo')->name('allitenaryinfo');
    Route::any('/itenaryEdit/{id}', 'CommonController@itenaryEdit')->name('itenaryEdit');
    Route::any('/deleteItenary/{id}', 'CommonController@deleteItenary')->name('deleteItenary');
    Route::any('/updatetouradmin', 'CommonController@updatetouradmin')->name('updatetouradmin');
    /********************* Admin Employees *********************/
    //Admin Employees All Routes :
    Route::any('/adminshowemployee', 'AdminController@showEmployee')->name('adminshowemployee');
    Route::post('/emailVerifyEmp','AdminController@emailVerifyEmp')->name('emailVerifyEmp');
    Route::post('/verifyMobileEmp','AdminController@verifyMobileEmp')->name('verifyMobileEmp');
    Route::any('/admininsertemployee', 'AdminController@insertEmployee')->name('admininsertemployee');
    Route::any('/createEmployee', 'AdminController@createEmployee')->name('createEmployee');
    Route::get('/employeedetails/{id}', 'AdminController@employeedetails')->name('employeedetails');
    Route::get('/employeeedit/{id}', 'AdminController@EditEmployee')->name('employeeedit');
    Route::any('/empleave', 'AdminController@EmployLeave')->name('employleave');
    Route::any('/updatetourform/{id}', 'AdminController@updatetourform')->name('updatetourform');
    // Admin Agent Routes
    // Route::any('/adminshowagent', 'AdminController@showAgent')->name('adminshowagent');
    // Route::any('/detailagent/{id}', 'AdminController@detailagent')->name('detailagent');
    // Route::any('/agentdelete/{id}', 'AdminController@agentdelete')->name('agentdelete');
    // Route::any('/agentdetails/{id}', 'AdminController@agentdetail')->name('agentdetail');


    Route::middleware(['auth'])->prefix('admin-agent')->group(function () {

        Route::get('/', 'AgentController@index')->name('admin-agent');
        Route::any('/list', 'AgentController@list')->name('admin-agent-list');
        Route::any('/add/{id?}', 'AgentController@Add')->name('admin-agent-add');
        Route::post('/store', 'AgentController@Store')->name('admin-agent-store');
        Route::get('/view/{id?}', 'AgentController@view')->name('admin-agent-view');
        Route::post('/delete/{id?}', 'AgentController@delete')->name('admin-agent-delete');

        //Route::any('/getCity/{id?}/{aid}','EmployeeController@getCity')->name('getCity');
        // Route::any('/agentdelete/{id}','AdminController@agentdelete')->name('agentdelete');
        // Route::any('/agentdetails/{id}','AdminController@agentdetail')->name('agentdetail');

    });

    Route::any('/adminshowtours', 'EmployeeController@showtours')->name('adminshowtours');
     

  

     

    // Route::middleware(['auth'])->prefix('admin-tours')->group(function () {
   
    //     Route::get('/', 'ToursController@index')->name('admin-tours');
    //     Route::any('/list', 'ToursController@list')->name('admin-agent-tours');
    //     Route::any('/add/{id?}', 'ToursController@Add')->name('admin-tours-add');
    //     Route::post('/store', 'ToursController@Store')->name('admin-tours-store');
    //     Route::get('/view/{id?}', 'ToursController@view')->name('admin-tours-view');
    //     Route::post('/delete/{id?}', 'ToursController@delete')->name('admin-tours-delete');

    //     //Route::any('/getCity/{id?}/{aid}','EmployeeController@getCity')->name('getCity');
    //     // Route::any('/agentdelete/{id}','AdminController@agentdelete')->name('agentdelete');
    //     // Route::any('/agentdetails/{id}','AdminController@agentdetail')->name('agentdetail');

    // });


    Route::middleware(['auth'])->prefix('admin-employee')->group(function () {

        Route::any('/show', 'EmployController@show')->name('employee.show');
        Route::any('/status', 'EmployController@Status')->name('employee.status');

    });

    Route::middleware(['auth'])->prefix('admin-banner')->group(function () {
        Route::any('/', 'SliderController@index')->name('admin-banner.index');
        Route::any('/show', 'SliderController@show')->name('admin-banner.show');
        Route::any('/add', 'SliderController@create')->name('admin-banner.add');
        Route::any('/store', 'SliderController@store')->name('admin-banner.store');
        Route::any('/edit/{id}', 'SliderController@edit')->name('admin-banner.edit');
        Route::any('/update', 'SliderController@update')->name('admin-banner.update');
        Route::any('/delete', 'SliderController@destroy')->name('admin-banner.destroy');
    });

    
    Route::any('/adminAgentForm', 'AdminController@adminAgentForm')->name('adminAgentForm');
    Route::any('/agentCreation', 'AdminController@agentCreation')->name('agentCreation');
    Route::any('/agentlive', 'AdminController@agentlive')->name('agentlive');
    Route::any('/editagent/{id}', 'AdminController@editagent')->name('editagent');
    Route::any('/updateagent', 'AdminController@updateagent')->name('updateagent');
    
    Route::any('/editsalary/{id}', 'CommonController@editsalary')->name('editsalary');
    Route::any('/editsalarybyadmin', 'CommonController@editsalarybyadmin')->name('editsalarybyadmin');
    Route::any('/insertsalary', 'CommonController@insertsalary')->name('insertsalary');
    Route::any('/salarylive', 'CommonController@salarylive')->name('salarylive');
    Route::any('/editsalaryslip', 'CommonController@editsalaryslip')->name('editsalaryslip');


    Route::any('/Review', 'CommonController@Review')->name('Review');
    Route::any('/reviewdelete/{id}', 'CommonController@reviewdelete')->name('reviewdelete');
    Route::any('/adminReviewForm', 'CommonController@adminReviewForm')->name('adminReviewForm');
    Route::any('/ReviewCreation', 'CommonController@ReviewCreation')->name('ReviewCreation');
    Route::any('/editreview', 'CommonController@editreview')->name('editreview');
    Route::any('/updatereview/{id}', 'CommonController@updatereview')->name('updatereview');

    Route::any('/edititi', 'AdminController@edititi')->name('edititi');

    //Admin Show package
    Route::any('/adminshowpackages', 'AdminController@showPackage')->name('adminshowpackages');
    Route::any('/admininsertPackageForm', 'AdminController@admininsertPackageForm')->name('admininsertPackageForm');
    Route::any('/createplan', 'AdminController@createplan')->name('createplan');
    Route::any('/updatepackageform/{id}', 'AdminController@updatepackageform')->name('updatepackageform');
    Route::any('/updateeplan', 'AdminController@updateeplan')->name('updateeplan');
    Route::any('/tourdetail/{id}', 'EmployeeController@tourdetail')->name('tourdetail');
    // Route::any('/adminshowpackages', 'AdminController@showPackage')->name('adminshowpackages');
    // Package Route Complete 
    // Admin Profile Route Start 
    Route::any('/deletevideo/{id}', 'CommonController@deletevideo')->name('deletevideo');
    Route::any('/getAdminProfile', 'AdminController@getAdminProfile')->name('getAdminProfile');
    Route::any('/updateAdmin', 'AdminController@updateAdmin')->name('updateAdmin');
    Route::any('/changePasword', 'AdminController@changePasword')->name('changePasword');
    Route::any('/changePaswordUpdate', 'AdminController@changePasswordUpdate')->name('changePasswordUpdate');
    
    Route::any('/photogallery/{id}', 'CommonController@photogallery')->name('photogallery');
    Route::any('/amintourinsertphoto/{id}', 'CommonController@amintourinsertphoto')->name('amintourinsertphoto');
    Route::any('/photogallertinsert', 'CommonController@photogallertinsert')->name('photogallertinsert');
    // kajal's route start 
    // Customer Route *****/

    Route::any('/showsalary', 'CommonController@showsalary')->name('showsalary');
    Route::any('/addsalary', 'CommonController@addsalary')->name('addsalary');



    Route::any('/showevent', 'CommonController@showevent')->name('showevent');
    Route::any('/eventdelete/{id}', 'CommonController@eventdelete')->name('eventdelete');
    Route::any('/adminEventForm', 'CommonController@adminEventForm')->name('adminEventForm');
    Route::any('/eventCreation', 'CommonController@eventCreation')->name('eventCreation');
    Route::any('/editevent/{id}', 'CommonController@editevent')->name('editevent');
    Route::any('/updateevent', 'CommonController@updateevent')->name('updateevent');

    // Route::any('/adminshowcustomer', 'AdminController@showCustomer')->name('adminshowcustomer');
    // Route::any('/adminformcustomer', 'AdminController@adminformcustomer')->name('adminformcustomer');
    Route::any('/adminCreateCustomer', 'AdminController@adminCreateCustomer')->name('adminCreateCustomer');
    Route::any('/customerdetails/{id}', 'AdminController@customerdetails')->name('customerdetails');
    Route::any('/editcustomerform/{id}', 'AdminController@editcustomerform')->name('editcustomerform');
    Route::any('/adminEditCustomer', 'AdminController@adminEditCustomer')->name('adminEditCustomer');
    Route::any('/customerlive', 'AdminController@customerlive')->name('customerlive');
    Route::any('/adminshowpackages', 'AdminController@showPackage')->name('adminshowpackages');
    Route::any('/inquirydelete/{id}', 'AdminController@inquirydelete')->name('inquirydelete');
    Route::any('/customerdelete/{id}', 'AdminController@customerdelete')->name('customerdelete');
    Route::any('/packagedelete/{id}', 'AdminController@packagedelete')->name('packagedelete');
    Route::any('/adminaddComments', 'AdminController@adminaddComments')->name('adminaddComments');
    Route::any('/inquieylive', 'AdminController@inquieylive')->name('inquieylive');
    // Ayush Routes 
    Route::any('/showbannerimage', 'AdminController@showbannerimage')->name('showbannerimage');
    Route::any('/uploadimage', 'AdminController@uploadimage')->name('uploadimage');


    //==================Membership Plan Start======================//
  	Route::any('/membership-plan', 'AdminController@emp_membership_plan')->name('membershippackage');
    Route::any('/add-membership-plan', 'AdminController@add_membership_plan')->name('add_membership_plan');
    Route::post('/create-membership-plan', 'AdminController@create_membership_plan');

    //==================Membership Plan Start======================//

    // ****************************Target Creation*************************************************
    Route::any('/targetCreation', 'CommonController@targetCreation')->name('targetCreation');
    Route::any('/inserttarget', 'CommonController@inserttarget')->name('inserttarget');
    Route::any('/showtarget', 'CommonController@showtarget')->name('showtarget');
    Route::any('/deletetarget/{id}', 'CommonController@deletetarget')->name('deletetarget');
    Route::any('/edittarget/{id}', 'CommonController@edittarget')->name('edittarget');
    Route::any('/updatetarget', 'CommonController@updatetarget')->name('updatetarget');
    /***********************************Manage Itenary *******************************************/
    Route::any('/adminitenary/{id}', 'AdminController@adminitenary')->name('adminitenary');
    Route::any('/showitenary', 'AdminController@showitenary')->name('showitenary');
    Route::any('/itineryelete/{id}', 'AdminController@itineryelete')->name('itineryelete');
    Route::any('/updateitinery/{id}', 'AdminController@updateitinery')->name('updateitinery');
    Route::any('/paymentgatway', 'CommonController@paymentgatway')->name('paymentgatway');
    Route::any('/cashpayment', 'CommonController@cashpayment')->name('cashpayment');
    Route::any('/cashpaymentdone', 'CommonController@cashpaymentdone')->name('cashpaymentdone');
    Route::any('/cashpaymentfinal', 'CommonController@cashpaymentfinal')->name('cashpaymentfinal');

    //*********************************Leavs Routes******************************************* */
    Route::any('/leaves_info', 'AdminController@leaves_info')->name('leaves_info');
    Route::any('/adminLeaveForm', 'AdminController@adminLeaveForm')->name('adminLeaveForm');
    Route::any('/createLeaveAdmin', 'AdminController@createLeaveAdmin')->name('createLeaveAdmin');
    Route::any('/showLeave', 'AdminController@showLeave')->name('showLeave');
    Route::any('/LeaveReport', 'AdminController@LeaveReport');
    Route::any('/AllLeaveReport', 'AdminController@AllLeaveReport');
    Route::any('leavestatus','AdminController@leaveTemplate');
    Route::any('/AllLeave', 'AdminController@FestivalLeaveLog')->name('FestivalLeave');
    Route::any('/editleave/{id}', 'AdminController@editleave')->name('editleave');
    Route::any('/updateleaves', 'AdminController@updateleaves')->name('updateleaves');
    Route::any('/deleteleaves/{id}', 'AdminController@deleteleaves')->name('deleteleaves');
    Route::any('/leavedelete/{id}', 'AdminController@leavedelete')->name('leavedelete');
    Route::any('/totalLeave', 'AdminController@totalLeave')->name('totalLeave');
    Route::any('/updatetotalLeaves', 'AdminController@updatetotalLeaves')->name('updatetotalLeaves');
    Route::any('/inserttotalLeave', 'AdminController@inserttotalLeave')->name('inserttotalLeave');
    Route::any('/creationtotaLeave', 'AdminController@creationtotaLeave')->name('creationtotaLeave');
    Route::any('/photogallery/{id}', 'CommonController@photogallery')->name('photogallery');
    //****************************Messenger Application Routes**************************** */
    Route::any('/adminmessenger', 'AdminFunctionController@adminmessenger')->name('adminmessenger');

    //*******Admin Approve Leavs */
    Route::any('/AdeminLeaveResult/{id}/{action}', 'AdminController@AdeminLeaveResult')->name('AdeminLeaveResult');
    Route::any('/DetailLeaveRequest/{id}', 'AdminController@DetailLeaveRequest')->name('DetailLeaveRequest');
    Route::any('/AdeminEmpResult', 'EmployController@StatusMail')->name('AdeminEmpResult');
    
    // **************************************
    Route::get('message/{id}', 'EmployeeController@getMessage')->name('message');
    Route::post('message', 'EmployeeController@sendMessage');
    // kajal's route end
    //****************All Inquiry Route ***********************************
    Route::any('/adminInquiryForm', 'AdminController@adminInquiryForm')->name('adminInquiryForm');
    Route::any('/createInquiry', 'AdminController@createInquiry')->name('createInquiry');
    Route::any('/allsalarygenerate/{monthsalary?}', 'CommonController@allsalarygenerate')->name('allsalarygenerate');
    Route::any('/salarygenerate', 'CommonController@salarygenerate')->name('salarygenerate');
    Route::any('/createsalary', 'CommonController@createsalary')->name('createsalarysalarygenerate');


    Route::any('/showpackages', 'EmployeeController@showPackage')->name('showpackages');
    Route::any('/showtours', 'EmployeeController@showtours')->name('showtours');
    Route::any('/tourfill', 'EmployeeController@tourfill')->name('tourfill');
    Route::any('/tourfilter', 'EmployeeController@tourfilter')->name('tourfilter');
    Route::any('/tourdetail/{id}', 'EmployeeController@tourdetail')->name('tourdetail');

    
}); // admin middleware close 
Route::any('/Emp_LeaveReport', 'EmployeeController@Emp_LeaveReport');
    Route::any('/AllEmpLeaveReport', 'EmployeeController@AllEmpLeaveReport');
Route::any('/employeeLogin', 'EmployeeController@employeeLogin')->name('employeeLogin');
Route::any('/employeeLoginCheck', 'EmployeeController@employeeLoginCheck')->name('employeeLoginCheck');
Route::any('/createyourProfile/{id}', [
    'uses' => 'EmployeeController@createyourProfile',
    'as' => 'createyourProfile'
])->name('createyourProfile');
// Employeement fillup section 
Route::any('/empPersonalDetail', [
    'uses' => 'EmployeeController@empPersonalDetail',
    'as' => 'createyourProfile'
])->name('empPersonalDetail');
Route::any('/fillEmployeemenDetails', [
    'uses' => 'EmployeeController@fillEmployeemenDetails',
    'as' => 'fillEmployeemenDetails'
])->name('fillEmployeemenDetails');

Route::any('/fillEmployeemenDetailsbyAjax', [
    'uses' => 'EmployeeController@fillEmployeemenDetailsbyAjax',
    'as' => 'fillEmployeemenDetailsbyAjax'
])->name('fillEmployeemenDetailsbyAjax');

Route::any('/fillloyeemenDetailsdocuments', [
    'uses' => 'EmployeeController@fillloyeemenDetailsdocuments',
    'as' => 'fillloyeemenDetailsdocuments'
])->name('fillloyeemenDetailsdocuments');
Route::any('/sendemployeement/{id}', 'EmployeeController@sendemployeement')->name('sendemployeement');
Route::any('/getCity', 'EmployeeController@getCity')->name('getCity');
Route::any('/getState', 'EmployeeController@getState')->name('getState');
Route::any('/fillFamilyDetailsbyAjax', 'EmployeeController@fillFamilyDetailsbyAjax')->name('fillFamilyDetailsbyAjax');


Route::any('/updatenoCompanyajax', 'EmployeeController@updatenoCompanyajax')->name('updatenoCompanyajax');

/*
***************************************Employee Routes - Middleware for employee **************************************
*/
Route::any('/emplive', 'AdminController@emplive')->name('emplive');
Route::group(['middleware' => 'CheckUser'], function () {   // Call Employee Midlewear here .. 
    Route::any('/empchangePaswordUpdate', 'EmployeeController@empchangePaswordUpdate')->name('empchangePaswordUpdate');


    //**************************************EmployeeCheck************************************************** */

    Route::any('/emplogout', 'EmployeeController@emplogout')->name('emplogout');
    Route::any('/allempinquiry', 'EmployeeController@allempinquiry')->name('allempinquiry');
    Route::any('/empdashboard', 'EmployeeController@empdashboard')->name('empdashboard');
    Route::any('/empinquirydetails/{id}', 'EmployeeController@empinquirydetails')->name('empinquirydetails');
    Route::any('/empaddComments', 'EmployeeController@empaddComments')->name('empaddComments');
    Route::any('/employeeeditinquiry', 'EmployeeController@employeeeditinquiry')->name('employeeeditinquiry');
    Route::any('/empeditUpdateform/{id}', 'EmployeeController@empeditUpdateform')->name('empeditUpdateform');
    Route::any('/empinquieylive', 'EmployeeController@empinquieylive')->name('empinquieylive');
    Route::any('/empexchange/{id}', 'EmployeeController@empexchange')->name('empexchange');
    Route::any('/updatecommentsemp', 'EmployeeController@updatecommentsemp')->name('updatecommentsemp');
    Route::any('/employeemembershippackage', 'EmployeeController@employeemembershippackage')->name('employeemembershippackage');
    Route::any('/filterole', 'EmployeeController@filterole')->name('filterole');

    Route::any('/empleaves_info', 'EmployeeController@empleaves_info')->name('empleaves_info');
    Route::any('/employeeLeaveForm', 'EmployeeController@employeeLeaveForm')->name('employeeLeaveForm');
    Route::any('/createLeave', 'EmployeeController@createLeave')->name('createLeave');
    // Route::get('leave','NotificationController@notify');
    Route::any('/empchat', 'EmployeeController@empchat')->name('empchat');
    Route::any('/documentempdw', 'EmployeeController@documentempdw')->name('documentempdw');
    Route::any('/empapprovenotify/{id}', 'EmployeeController@empapprovenotify')->name('empapprovenotify');
    /**********************************************Events*************************************** */


    Route::any('/employeetarget', 'EmployeeController@employeetarget')->name('employeetarget');
    Route::any('/empleavereport', 'EmployeeController@empleavereport')->name('empleavereport');
    Route::any('/employeeleavereport', 'EmployeeController@employeeleavereport')->name('employeeleavereport');
    Route::any('/emptargetAjax', 'EmployeeController@emptargetAjax')->name('emptargetAjax');
    Route::any('/employeesalaryreport', 'EmployeeController@employeesalaryreport')->name('employeesalaryreport');
    Route::any('/employeesalaryselect', 'EmployeeController@employeesalaryselect')->name('employeesalaryselect');
    Route::any('/empleaveshow', 'EmployeeController@empleaveshow')->name('empleaveshow');
    Route::any('/empleaveAjax', 'EmployeeController@empleaveAjax')->name('empleaveAjax');
    Route::any('/ShowLeaveDetail/{id}', 'EmployeeController@ShowLeaveDetail')->name('ShowLeaveDetail');
    Route::get('/message/{id}', 'EmployeeController@getMessage')->name('message');
    Route::post('message', 'EmployeeController@sendMessage');

    Route::any('/Yourprofile/{id}', 'AdminController@employeedetails')->name('YourProfile');
    Route::any('/empchangepasswordform', 'EmployeeController@empchangepasswordform')->name('empchangepasswordform');
    Route::any('/EmpLeaveResult/{id}', 'EmployeeController@EmpLeaveResult')->name('EmpLeaveResult');
    Route::any('/allemprequest', 'EmployeeController@allemprequest')->name('allemprequest');
}); // Close Employee Middlewear

// ************************ Employee All Routes******************************************************
Route::any('/allempinquiry', 'EmployeeController@allempinquiry')->name('allempinquiry');


//Route::any('/', 'UserController@index')->name('userindex');
Route::any('/indexPage', 'UserController@indexPage')->name('indexPage');
Route::any('/packagedetail/{id}', 'UserController@Packagedetail')->name('packagedetail');
Route::any('/memberplans', 'UserController@memberplans')->name('memberplans');
Route::any('/inquiry', 'UserController@inquiry')->name('inquiry');
Route::any('/inquiryinsert', 'UserController@inquiryinsert')->name('inquiryinsert');
Route::any('/customerRegistration', 'UserController@customerRegistration')->name('customerRegistration');
Route::post('/gettourspecificdata', 'UserController@gettourspecificdata')->name('gettourspecificdata');
Route::any('/demoalert', 'UserController@demoalert')->name('demoalert');
Route::any('/sendmailuser', 'UserController@sendmailuser')->name('sendmailuser');




Route::middleware(['auth'])->prefix('campaign')->group(function () {

    Route::get('/', 'CampaignController@index')->name('campaign');
    Route::any('/list', 'CampaignController@list')->name('campaign-list');
    Route::any('/add', 'CampaignController@add')->name('campaign-add');
    Route::post('/store', 'CampaignController@store_campaign')->name('campaign-store');
    Route::get('/view/{id?}', 'CampaignController@view')->name('campaign-view');
    Route::post('/delete/{id?}', 'CampaignController@delete')->name('campaign-delete');

    Route::any('/email_list/{id?}', 'CampaignController@email_list')->name('campaign-email_list');
    Route::get('/email_view/{id?}', 'CampaignController@email_view')->name('campaign-view');
    Route::post('/email_delete/{id?}', 'CampaignController@email_delete')->name('campaign-email-delete');

});

//h-d route


Route::middleware(['auth'])->prefix('employee-tours')->group(function () {

        Route::any('/', 'ToursController@showtours')->name('employee-tours');
        Route::any('/tourfill', 'ToursController@tourfill')->name('employee-tourfill');
        Route::any('/tourdetail/{id?}', 'ToursController@tourdetail')->name('employee-tourdetail');
        //Route::any('/tourdetail/{id}', 'EmployeeController@tourdetail')->name('tourdetail');
});


Route::middleware(['auth'])->prefix('admin-setting')->group(function () {
    Route::get('/', 'SettingController@index')->name('admin.setting');
    Route::any('/store', 'SettingController@store')->name('admin-setting.store');
});

//=======================Interactive Category=================
Route::middleware(['auth'])->prefix('admin-category')->group(function () {
        Route::get('/', 'Interactive_categoryController@index')->name('category');

        Route::get('/sub_category', 'Interactive_categoryController@sub_category')->name('subcategory');

        Route::get('/category_assignment', 'Interactive_categoryController@category_assignment');

        Route::get('/create_category', 'Interactive_categoryController@create');

        Route::get('/create_sub_category', 'Interactive_categoryController@create_sub_category');

        Route::get('/create_category_assignment', 'Interactive_categoryController@create_category_assignment');
        
        Route::any('/store', 'Interactive_categoryController@store');
        
        Route::any('/sub_cat_store', 'Interactive_categoryController@sub_cat_store');
        
        Route::any('/getAll', 'Interactive_categoryController@getAll');
        Route::any('/getAll_sub_cat', 'Interactive_categoryController@getAll_sub_cat');

        Route::any('/edit/{id?}', 'Interactive_categoryController@edit');

        Route::any('/edit_sub_category/{id?}', 'Interactive_categoryController@edit_sub_category');

        Route::any('/edit_category_assign/{id?}', 'Interactive_categoryController@edit_category_assign');

        Route::any('/destroy/{id?}', 'Interactive_categoryController@destroy');

        Route::any('/destroy_sub_category/{id?}', 'Interactive_categoryController@destroy_sub_category');

        Route::any('/destroy_assign_category/{id?}', 'Interactive_categoryController@destroy_assign_category');

        Route::any('/checkcategory/{name}/{id}', 'Interactive_categoryController@checkcategory');

        //Route::any('/checkSubcategory/{name?}{id?}', 'Interactive_categoryController@checkSubcategory');

        Route::any('/get_assign_data', 'Interactive_categoryController@get_assign_data');

        Route::any('/get_subCategory/{id}/{emp_id}', 'Interactive_categoryController@get_subCategory');

        Route::any('/get_Category/{id}', 'Interactive_categoryController@get_Category');

        Route::any('/get_assign_data', 'Interactive_categoryController@get_assign_data');

        Route::any('/get_subCategory/{id?}', 'Interactive_categoryController@get_subCategory');

        Route::any('/store_category_assignment/{id?}', 'Interactive_categoryController@store_category_assignment');

        Route::any('/checkSubcategory/{name}/{id}', 'Interactive_categoryController@checkSubcategory');

        Route::any('/check_on_Subcategory/{name}/{id}', 'Interactive_categoryController@check_on_Subcategory');
});

     Route::get('/membership-plans', 'front\PlanController@index')->name('front.membership_plan');
     Route::get('/membership-info/{type?}', 'front\PlanController@info')->name('front.membershipinfo');

     Route::get('/askforcall', 'front\AskforcallController@index')->name('front.askforcall');
     Route::post('/save-askforcall', 'front\AskforcallController@Store')->name('front.save-askforcall');

     Route::middleware(['auth'])->prefix('admin-askforcall')->group(function () {
    
        Route::get('/', 'AskforcallController@index')->name('askforcalls');
        Route::any('/list/{id}', 'AskforcallController@list')->name('askforcalls-list');
        Route::post('/delete/{id?}', 'AskforcallController@delete')->name('askforcalls-delete');
        Route::get('/view/{id?}', 'AskforcallController@view')->name('askforcalls-view');

        Route::get('/askforcall_view/{id}', 'AskforcallController@askforcall_view')->name('askforcall-view');
    });

    Route::middleware(['auth'])->prefix('admin-customer')->group(function () {
        Route::any('/', 'CustomerController@index')->name('adminshowcustomer');
        Route::any('/show', 'CustomerController@show')->name('admin-customer.show');
        Route::any('/add', 'CustomerController@create')->name('admin-customer.add');
        Route::any('/store', 'CustomerController@store')->name('admin-customer.store');
        Route::any('/edit/{id}', 'CustomerController@edit')->name('admin-customer.edit');
        Route::any('/detail/{id}', 'CustomerController@Detail')->name('admin-customer.detail');
        Route::any('/update', 'CustomerController@update')->name('admin-customer.update');
        Route::any('/delete', 'CustomerController@destroy')->name('admin-customer.destroy');
    });

    Route::middleware(['auth'])->prefix('/employee-ticket-generator')->group(function () {
        Route::any('/', 'EmployeegenrateController@index')->name('employee-ticket-generator-indxe');
        Route::any('/add', 'EmployeegenrateController@add')->name('employee-ticket-generator-add');
        Route::post('/store', 'EmployeegenrateController@Store')->name('employee-ticket-generator-store');
        Route::get('/list', 'EmployeegenrateController@list')->name('employee-ticket-generator-list');
        Route::get('/view/{id?}', 'EmployeegenrateController@view')->name('employee-ticket-generator-view');
        Route::post('/sendmsg', 'EmployeegenrateController@send_message')->name('employee-ticket-generator-sendmsg');
        Route::get('/getlist/{id?}', 'EmployeegenrateController@get_messages')->name('employee-ticket-generator-get_messages');
        Route::any('/getCat/{id?}', 'EmployeegenrateController@getCat')->name('employee-ticket-generator-getCat');

        Route::any('/getcount/{id?}', 'EmployeegenrateController@getcount')->name('employee-ticket-generator-getcount');

        Route::get('/getmsg/{id?}', 'EmployeegenrateController@get_receive_msg')->name('employee-ticket-generator-get_receive_msg');

    });



    Route::middleware(['auth'])->prefix('/groups')->group(function () {
        Route::get('/', 'GroupController@index')->name('groups-indxe');
        Route::any('/delete', 'GroupController@delete')->name('groups-delete');
        Route::any('/checkname', 'GroupController@checkName')->name('groups-checkName');
        Route::get('/list', 'GroupController@list')->name('groups-list');
        Route::post('/store', 'GroupController@store')->name('groups-store');
        Route::get('/lead-view/{id?}', 'GroupController@list_view')->name('groups-view');
        Route::get('/lead-list/{id?}', 'GroupController@list_lead')->name('groups-list-lead');
        Route::any('/lead_delete', 'GroupController@lead_delete')->name('groups-lead-delete');
        
        Route::get('/number-view/{id?}', 'GroupController@lead_view')->name('groups-lead-view');
        Route::get('/lead-edit/{id?}', 'GroupController@lead_edit')->name('groups-lead-edit');
        Route::post('/lead-store', 'GroupController@lead_store')->name('groups-lead-store');
        Route::post('/assign-employee', 'GroupController@assign_employee')->name('groups-assign-employee');
        Route::get('/list_employee/{id?}', 'GroupController@list_employee')->name('groups-list-employee');
        Route::any('/unassign_delete', 'GroupController@Unassign_delete')->name('groups-unassign');
        Route::get('/employee-leads/{id?}/{group_id?}', 'GroupController@employee_leads')->name('employee-leads');
        Route::get('/employee-leads-list/{id?}/{group_id?}', 'GroupController@employee_leads_list')->name('employee-leads-list');
        Route::any('/lead_unassign_delete', 'GroupController@lead_unassign_delete')->name('lead-groups-unassign');
        Route::get('/view_status/{id?}/{emp_id?}', 'GroupController@view_status')->name('groups-view-status');
    });   
    
    Route::middleware(['auth'])->prefix('/leads')->group(function () {
        Route::get('/', 'EmpleadController@index')->name('leads-indxe');
        Route::post('/list', 'EmpleadController@list')->name('leads-list');
        Route::get('/view/{id?}', 'EmpleadController@view')->name('leads-view');
        Route::post('/assign_employee', 'EmpleadController@assign_employee')->name('leads-assign-employee');
        Route::get('/assign_list', 'EmpleadController@assign_list')->name('leads-assign-list');
        Route::get('/user_list', 'EmpleadController@user_list')->name('leads-user-list');
        Route::get('/status_list/{id?}', 'EmpleadController@status_list')->name('leads-status-list');
        Route::get('/assign_status_list/{id?}', 'EmpleadController@assign_status_list')->name('leads-assign-status-list');
        Route::post('/status_store', 'EmpleadController@store_lead')->name('status-lead-store');

    });



    Route::middleware(['auth'])->prefix('/call-status')->group(function () {
        Route::get('/', 'CallstatusController@index')->name('call-status-indxe');
        Route::get('/list', 'CallstatusController@list')->name('call-status-list');
        Route::post('/store', 'CallstatusController@store')->name('call-status-store');
        Route::any('/delete', 'CallstatusController@delete')->name('call-status-delete');
        Route::any('/checkname/{id?}', 'CallstatusController@checkName')->name('call-status-checkName');
       
    });

    //front side
    // Route::prefix('/tickets')->group(function () {
    //     Route::get('/', 'front\TicketController@index')->name('front.tickets');
    //     Route::get('/view/{id?}', 'front\TicketController@view')->name('front.view');
    // });

    
    Route::any('/documentemp', 'AdminController@documentemp')->name('documentemp');