<?php 
Route::any('/LeaveCron', 'CommonController@LeaveCron');

Route::any('/forgotpass', 'AdminController@Forgotpass')->name('forgotpass');
Route::any('/forgotpassword', 'AdminController@ForgotPassword')->name('forgotpassword');

Route::post('/forgot/email','AdminController@Forgotpass_emailCheck')->name('forgot.email.check');
Route::any('/resetpassword/{email}/{type}','AdminController@Resetpassword')->name('resetpassword');

Route::any('/resetpass','AdminController@Resetpass')->name('resetpass');

Route::any('/upicallback','front\MemberController@icici_call_back')->name('upicallback');

Route::post('/flight-availability','front\HomeController@check_flight')->name('flight-availability');
Route::get('/flight-list-details','front\HomeController@flight_list')->name('flight-list-details');

// Route::get('/index','front\HomeController@index')->name('index');
Route::get('/packages-demo','front\HomeController@packages_demo')->name('packages-demo');
Route::get('/intinerary-demo','front\HomeController@intinerary_demo')->name('intinerary-demo');
Route::get('/membershipplan','front\HomeController@membership_demo')->name('membership-demo');
Route::get('/about_us','front\HomeController@AboutUs')->name('AboutUsDemo');
Route::get('/about-us','front\HomeController@AboutUs')->name('about-us');
// Route::get('/allblog','front\HomeController@new_blog')->name('allblog');
Route::get('/blog/details','front\HomeController@new_blog')->name('blogAll');
Route::any('blog/{slug}','front\HomeController@blog_details')->name('BlogDetails');

Route::get('/gallery/images','front\HomeController@gallery_design')->name('gallery_design');
Route::get('/all-image/{slug}','front\HomeController@all_images')->name('all_image');

Route::any('/all/review','front\HomeController@new_review')->name('NewReview');


Route::get('/become_member/{id}/{type}', 'front\PlanController@user_become_member')->name('front.become_member');

Route::any('customer/Editemail/{id}','CustomerController@checkEditEmail');

Route::prefix('/ads')->group(function () {
    Route::get('/', 'AdsController@index')->name('ads-list');
    Route::get('/add', 'AdsController@add')->name('ads-add');
    Route::post('/store', 'AdsController@store')->name('ads-store');
    Route::post('/list', 'AdsController@list')->name('ads-list');
    Route::get('/edit/{id}', 'AdsController@edit')->name('edit-ads');
    Route::get('/delete/{id}', 'AdsController@destroy')->name('delete-ads');
    Route::get('/checktitle/{id?}', 'AdsController@CheckTitle');
});

Route::prefix('/blogs')->group(function () {
    Route::get('/', 'BlogController@index')->name('blog-list');
    Route::get('/add', 'BlogController@add')->name('blog-add');
    Route::post('/store', 'BlogController@store')->name('blog-store');
    Route::post('/list', 'BlogController@list')->name('blog-list');
    Route::get('/edit/{id}', 'BlogController@edit')->name('edit-blog');
    Route::get('/delete/{id}', 'BlogController@destroy')->name('delete-blog');
    Route::get('/checkslug/{id?}', 'BlogController@CheckSlug');
    
});

Route::prefix('/jj-career')->group(function () {
    Route::get('/', 'CareersController@index')->name('career-list');
    Route::get('/add', 'CareersController@add')->name('career-add');
    Route::post('/store', 'CareersController@store')->name('career-store');
    Route::post('/list', 'CareersController@list')->name('career-list');
    Route::get('/edit/{id}', 'CareersController@edit')->name('edit-career');
    Route::get('/delete/{id}', 'CareersController@destroy')->name('delete-career');
});

Route::prefix('/review')->group(function () {
    Route::get('/', 'ReviewsController@index')->name('review-list');
    Route::get('/record', 'ReviewsController@record')->name('review-record');
    Route::post('/store', 'ReviewsController@store')->name('review-store');
    Route::post('/list', 'ReviewsController@list')->name('review-list');
    Route::get('/image_list/{id}', 'ReviewsController@Imagelist')->name('review-image-list');
    Route::post('/select', 'ReviewsController@Select')->name('select-image');
    Route::get('/show/{id}/{status}', 'ReviewsController@Show')->name('show-image');
    Route::get('/delete/{id}', 'ReviewsController@destroy')->name('delete-review');
});

Route::prefix('/tour-gallery')->group(function () {
    Route::get('/', 'GalleryController@index')->name('gallery-list');
    Route::get('/add', 'GalleryController@add')->name('gallery-add');
    Route::post('/store', 'GalleryController@store')->name('gallery-store');
    Route::post('/list', 'GalleryController@list')->name('gallery-list');
    Route::get('/edit/{id}', 'GalleryController@edit')->name('edit-gallery');
    Route::get('/delete/{id}/{gallery_id}', 'GalleryController@destroy')->name('delete-gallery');
    Route::get('/checkcategory/{id?}', 'GalleryController@checkcategory');
    Route::get('/checkslug/{id?}', 'GalleryController@CheckSlug');
});

Route::prefix('/announcements')->group(function () {
    Route::get('/', 'AnnouncementsController@index')->name('announcements-list');
    Route::get('/add', 'AnnouncementsController@add')->name('announcements-add');
    Route::post('/store', 'AnnouncementsController@store')->name('announcements-store');
    Route::post('/list', 'AnnouncementsController@list')->name('announcements-list');
    Route::get('/edit/{id}', 'AnnouncementsController@edit')->name('edit-announcements');
    Route::get('/delete/{id}', 'AnnouncementsController@destroy')->name('delete-announcements');
    Route::get('/check/{user}', 'AnnouncementsController@CheckRecord');
    Route::get('/checkemployee/{employee}', 'AnnouncementsController@CheckEmp');
    Route::get('/checkagent/{agent}', 'AnnouncementsController@CheckAgent');
    Route::get('/checkmember/{member}', 'AnnouncementsController@CheckMember');
    Route::get('/checkuser/{user}', 'AnnouncementsController@CheckUser');
    Route::get('/checkboxCheck','AnnouncementsController@checkboxCheck');
});

Route::prefix('/event')->group(function () {
    Route::get('/', 'EventController@index')->name('event-list');
    Route::get('/add', 'EventController@add')->name('event-add');
    Route::post('/store', 'EventController@store')->name('event-store');
    Route::post('/list', 'EventController@list')->name('event-list');
    Route::get('/edit/{id}', 'EventController@edit')->name('edit-event');
    Route::get('/delete/{id}', 'EventController@destroy')->name('delete-event');
});

?>