<?php

//data
Route::prefix('/tickets')->group(function () {
    Route::get('/', 'front\TicketController@index')->name('front.tickets');
    Route::get('/view/{id?}', 'front\TicketController@view')->name('front.tickets.view');
    Route::post('/store', 'front\TicketController@store')->name('front.tickets.store');
    
});
