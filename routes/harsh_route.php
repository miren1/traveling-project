<?php
Route::any('/topup-return-url/{id}/{type}', 'front\TicketController@TopUpReturnUrl');

//ticket module 
Route::prefix('/tickets')->group(function () {
    Route::get('/', 'front\TicketController@index')->name('front.tickets');
    Route::post('/list', 'front\TicketController@list')->name('front.tickets.list');
    Route::get('/view/{id?}', 'front\TicketController@view')->name('front.tickets.view');
    Route::post('/getcat', 'front\TicketController@getCat')->name('front.tickets.getcat');
    Route::post('/AllgetCat', 'front\TicketController@AllgetCat')->name('front.tickets.AllgetCat');
    Route::post('/store', 'front\TicketController@store')->name('front.tickets.store');
    Route::get('/getlist/{id?}', 'front\TicketController@get_messages')->name('front.tickets.get_messages');
    Route::any('/getcount/{id?}', 'front\TicketController@getcount')->name('front.tickets.getcount');
    Route::post('/sendmsg', 'front\TicketController@send_message')->name('front.tickets.sendmsg');
    Route::post('/topup', 'front\TicketController@topup')->name('wallet-recharge');
    
});

Route::prefix('/dashboard')->group(function () {
    Route::get('/', 'front\DashboardController@index')->name('front.dashboard');
    Route::get('/reviews', 'front\DashboardController@reviews');
    Route::post('/update_profile', 'front\DashboardController@update_profile')->name('front-profile-update');
});

//admin side all tickets for data 
Route::prefix('/all-tickets')->group(function () {
    Route::get('/', 'AdminticketController@index')->name('all-tickets.index');
    Route::post('/list', 'AdminticketController@list')->name('all-tickets.list');
    Route::get('/view/{id?}', 'AdminticketController@view')->name('all-tickets.view');
    Route::any('/getcount/{id?}', 'AdminticketController@getcount')->name('all-tickets.getcount');
    Route::get('/getmsg/{id?}', 'AdminticketController@get_receive_msg')->name('all-tickets.get_receive_msg');
    Route::get('/getlist/{id?}', 'AdminticketController@get_messages')->name('all-tickets.get_messages');
    Route::post('/sendmsg', 'AdminticketController@send_message')->name('all-tickets.sendmsg');
    Route::any('/status_chnage', 'AdminticketController@status_chnage')->name('all-tickets.chnage');
    Route::post('/getcat', 'AdminticketController@getCat')->name('all-tickets.getcat');
});


Route::middleware(['auth'])->prefix('/ticket-receive')->group(function () {
    Route::any('/', 'TicketreciveController@index')->name('ticket-receive-indxe');
    Route::post('/list', 'TicketreciveController@list')->name('ticket-receive-list');
    Route::get('/view/{id?}', 'TicketreciveController@view')->name('ticket-receive-view');
    Route::post('/sendmsg', 'TicketreciveController@send_message')->name('ticket-receive-sendmsg');
    Route::get('/getlist/{id?}', 'TicketreciveController@get_messages')->name('ticket-receive-get_messages');
    Route::post('/getCat', 'TicketreciveController@getCat')->name('ticket-receive-getCat');
    Route::any('/status_chnage', 'TicketreciveController@status_chnage')->name('ticket-receive-status-chnage');

    Route::any('/getcount/{id?}', 'TicketreciveController@getcount')->name('ticket-receive-getcount');
    Route::get('/getmsg/{id?}', 'TicketreciveController@get_receive_msg')->name('ticket-receive-get_receive_msg');
});

//front

Route::prefix('packages')->group(function () {
    Route::get('/{slug?}','front\TourController@index')->name('tour-slug');
    Route::post('/getTourType','front\TourController@getTourType')->name('tour.getTourType');
    Route::post('/gettours','front\TourController@getTours')->name('tour.getTours');
    Route::get('/tour/{slug?}','front\TourController@view')->name('tour.plans');
    route::get('/kedarnaath','front\TourController@view')->name('info');
});

Route::middleware(['auth'])->prefix('/member-plans')->group(function () {
    
    Route::get('/', 'PlanController@index')->name('member-plans');
    Route::any('/list', 'PlanController@list')->name('member-plans-list');
    Route::post('/delete/{id?}', 'PlanController@delete')->name('member-plans-delete');
    Route::any('/add/{id?}', 'PlanController@add')->name('member-plans-add');
    Route::any('/add-member/{id?}', 'PlanController@memberIndex')->name('add-member');
    Route::post('/member-add/store', 'PlanController@MemberAdd')->name('add-member.store');
    Route::post('/store', 'PlanController@store')->name('member-plans-store');
    Route::get('/view/{id?}', 'PlanController@view')->name('member-plans-view');
    Route::get('/add_member/{id?}','PlanController@add_member')->name('member-plans-add-member');
    
    Route::get('/invited_members','PlanController@member_req')->name('member-plans-add-invited_members');
    Route::any('/list_member_req','PlanController@list_member_req')->name('member-plans-show-req');

    Route::any('/memberlist', 'PlanController@membershiplist');
    Route::any('/member_list', 'PlanController@member_list');
});
Route::any('/store_member_req','PlanController@store_member_req')->name('member-plans-store-member-req');
Route::any('/createmember/{id?}', ['uses' => 'CreatememberController@create','as' => 'createmember'])->name('createmember');

Route::any('/catcheckSlug/{id?}', 'AdminController@checkSlug')->name('Admin-cat-slug');

Route::any('/tourcheckSlug/{id?}', 'CommonController@tourcheckSlug')->name('Admin-cat-tourcheckSlug');

Route::get('/careers','front\HomeController@Careers')->name('Careers');