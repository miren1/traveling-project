<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Career extends Model
{
    use Notifiable;
    
    protected $table = 'careers';
    protected $primaryKey = 'id';
    protected $fillable = [
        'department','opening_title', 'number_of_openings', 'relevant_experience_required', 'base_location','roles_responsibilities','primary_skills','pre_requiesite','','advantage','work_flexibility','salary_range','incentives','benefits','deleted_at'
    ];
}
