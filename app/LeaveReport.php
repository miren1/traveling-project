<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LeaveReport extends Model
{
    //
    protected $table='leaves';
    protected $fillable=['employee_fk_id ','reason','leave_date_start','leave_date_end','approve_status','approve_reason','leave_photos','leave_type ','leave_mode','user_notification_status','is_paid','total_days','total_leave_days'];


    function employee()
    {
        return $this->hasOne('App\User','id','employee_fk_id');
    }
}
