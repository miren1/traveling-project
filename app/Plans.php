<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Plans extends Model
{
    protected $table = 'plans';
    protected $primaryKey = 'id';
    protected $fillable = [
        'user_id', 'type', 'fixed_amt', 'years', 'role', 'mebership_fees', 'free_nights_room', 'free_nights_to_avail_in', 'free_night_in_any_type_of_hotel', 'monthly_emi', 'quarterly_emi', 'half_yearly_emi', 'annual_emi', 'emi_dafulter_charges', 'flexbility_to_move_free_night_to_next_year', 'easy_upgrade_of_membership_plans', 'referral', 'commission_am', 'commission_first_payment', 'commission_second_payment', 'agent_target', 'commission_employee', 'commission_payment', 'is_delete'
     ];
}
