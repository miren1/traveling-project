<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Leave extends Model
{
    //
    protected $table='leaves_info_details';
    protected $fillable=['leave_date','title','color','start_date','end_date'];
}
