<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Toure extends Model
{
    protected $table = 'toures';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name', 'package_type', 'days', 'banner_img', 'small_info', 'detail1', 'detail2', 'detail3', 'detail4', 'inclusion', 'optional_charges', 'exclusion', 'essentials', 'clothing_essential', 'price'
    ];


    public function pack()
    {
       return $this->hasOne('App\Packagtype','id','package_type');
    }

    public function itinerary()
    {
       return $this->hasMany('App\Mediatour','tour_fk_id','id');
    }

}
