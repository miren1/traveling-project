<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Ads_blog extends Model
{
    use Notifiable;
    
    protected $table = 'ads_blogs';
    protected $primaryKey = 'id';
    protected $fillable = [
        'ads_id', 'blog_id'
    ];

    function allads()
    {
        return $this->hasMany('App\Ads', 'id', 'ads_id');
    }
}
