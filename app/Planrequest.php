<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Planrequest extends Model
{

    protected $table = 'plan_requests';
    protected $primaryKey = 'id';
    protected $fillable = [
        'user_id', 'plan_id', 'first_name', 'last_name', 'mobile_number', 'whats_up', 'email', 'status', 'is_delete' ];
}
