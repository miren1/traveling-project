<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Ads extends Authenticatable
{
    use Notifiable;
    
    protected $table = 'ads';
    protected $primaryKey = 'id';
    protected $fillable = [
        'title','ads_type', 'ads_image', 'ads_video_url', 'affiliate_url','position','is_deleted'
    ];

    function adsblogs()
    {
        return $this->hasMany('App\Ads_blog', 'ads_id', 'id');
    }
}
