<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    protected $table = 'media_tours';
    protected $primaryKey = 'id';
    protected $fillable = [
        'tour_fk_id', 'slug', 'caption', 'imagepath'
    ];
}
