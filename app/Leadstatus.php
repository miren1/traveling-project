<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Leadstatus extends Model
{
    protected $table = 'lead_status';
    protected $primaryKey = 'id';
    protected $fillable = [
        'user_id', 'employee_id', 'group_id', 'group_lead_id', 'call_status_id', 'remarks','date_time', 'is_delete'
    ];


    public function employee()
    {
       return $this->hasOne('App\User','id','employee_id');
    }

    public function user()
    {
       return $this->hasOne('App\User','id','user_id');
    }
    
    public function call_status()
    {
        return $this->hasOne('App\Callstatus','id','call_status_id');
    }

    public function activity()
    {
      return $this->hasMany('App\Assignlead','group_lead_id','group_lead_id');
    }

}
