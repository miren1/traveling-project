<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Membership extends Model
{
    protected $table = 'plans';
    protected $primaryKey = 'id';
    protected $fillable = [
        'fixed_amt', 'years', 'role', 'mebership_fees', 'free_nights_room', 'free_nights_to_avail_in', 'free_night_in_any_type_of_hotel', 'monthly_emi', 'quarterly_emi', 'exampleRadios', 'half_yearly_emi', 'annual_emi', 'emi_dafulter_charges', 'flexbility_to_move_free_night_to_next_year', 'easy_upgrade_of_membership_plans', 'referral'
    ];
}
