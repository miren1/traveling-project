<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery_images extends Model
{
    protected $table = 'gallery_images';
    protected $primaryKey = 'id';
    protected $fillable = [
        'gallery_id', 'image'
    ];

    function all_image()
    {
        return $this->hasMany('App\Tour_gallery', 'id', 'gallery_id')->where('deleted_at', '0');
    }
}
