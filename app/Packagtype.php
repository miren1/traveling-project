<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Packagtype extends Model
{
    //
    protected $table = 'packag_types';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name', 'details', 'created_at', 'updated_at', 'deleted_at', 'photo', 'tag_line'
    ];
   
}
