<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $fillable = ['user_id','askforcall_group_id','sendgrid_key','contact_mail','help_desk_mail','executive_mail','askforcall_user_id'];
}
