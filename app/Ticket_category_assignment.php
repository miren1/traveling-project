<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket_category_assignment extends Model
{
    
    protected $fillable = [
        'user_id','admin_id','agent_id','ticket_cat_id','ticket_sub_cat_id','is_delete','created_at','updated_at'
    ];
    public function ticket_categories()
    {
       return $this->hasOne('App\Ticket_category','id','ticket_cat_id');
    }

    public function ticket_sub_categories()
    {
       return $this->hasOne('App\Ticket_sub_category','id','ticket_sub_cat_id');
    }

    public function users()
    {
       return $this->hasOne('App\User','id','user_id');
    }
}
