<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $table = 'groups';
    protected $primaryKey = 'id';
    protected $fillable = [
        'user_id', 'name', 'status', 'is_delete'
    ];

    function grouplead()
    {
        return $this->hasMany('App\Grouplead', 'group_id', 'id')->where('is_delete', '0');
    }
}
