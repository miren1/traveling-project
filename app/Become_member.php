<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Become_member extends Authenticatable
{
    use Notifiable;
    
    protected $table = 'memberships';
    protected $primaryKey = 'id';
    protected $fillable = [
        'plan_id', 'member_id', 'prefix', 'name', 'dob', 'adharno', 'panno', 'passportno', 'passportdate', 'occupation', 'email', 'annual_income', 'primary_contact', 'secondary_contact', 'whatsapp_number','resi_address','city','state','pincode','jj_tip','photo','password'
    ];
}
