<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket_category extends Model
{
    //protected $table = 'agents';
    //protected $primaryKey = 'id';
    protected $fillable = [
        'user_id', 'name', 'dec', 'is_delete'
    ];
}
