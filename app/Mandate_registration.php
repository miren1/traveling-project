<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mandate_registration extends Model
{
    protected $table='mandate_registrations';
    protected $fillable=['user_id','txn_status','txn_msg','txn_err_msg','clnt_txn_ref','tpsl_bank_cd','tpsl_txn_id','txn_amt','tpsl_txn_time','bal_amt','card_id','alias_name','BankTransactionID','mandate_reg_no','token','hash'];
}
