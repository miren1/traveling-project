<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mediatour extends Model
{
    protected $table = 'itinerary';
    protected $primaryKey = 'id';
    protected $fillable = [
        'tour_fk_id', 'details', 'day_number', 'caption', 'images'
    ];
}
