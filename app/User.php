<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','prefix','middlename','surname','contact_number_alt','c_house_no','c_society_name','c_landmark','c_city_id','c_state_id','c_pincode','whatsup_number','contact_number','verification_status','p_house_no','p_society_name','p_landmark','p_city_id','p_state_id','p_pincode','family_details','know_jagjoyu','reference_name','photo','aadhar_card_id','aadhar_card_pic','pancard_id','pancard_photo','driving_id','driving_photo','marksheet_10th_photo','marksheet_12th_photo','marksheet_bachelors_photo','marksheet_masters_photo','any_other_certificates','employeement_details','employeement_photos','employee_approved','employee_status','designation','joining_date','salary'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function mainleads()
    {
       return $this->hasmany('App\Assignlead','employee_id','id')->where('status','active');
    }


  

    public function  scopeDataleads($query, $type)
    {
       return $query->hasmany('App\Assignlead','employee_id','id')->where('group_id',$type)->count;
    }

   

}
