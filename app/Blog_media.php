<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Blog_media extends Model
{
    use Notifiable;
    
    protected $table = 'blog_medias';
    protected $primaryKey = 'id';
    protected $fillable = [
        'blog_id','image', 'deleted_at'
    ];
}
