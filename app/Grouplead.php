<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Grouplead extends Model
{
    protected $table = 'group_leads';
    protected $primaryKey = 'id';
    protected $fillable = [
        'user_id', 'group_id', 'fname', 'lname', 'email', 'number', 'wnumber', 'area', 'city', 'state', 'country', 'status', 'is_delete'
    ];
    public function group()
    {
       return $this->hasOne('App\Group','id','group_id');
    }
    public function Assignlead()
    {
       return $this->hasOne('App\Assignlead','id','group_id')->count();
    }

    public function activity()
    {
      return $this->hasMany('App\Leadstatus','group_lead_id','id');
    }
    

}
