<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $table = 'tickets';
    protected $primaryKey = 'id';
    protected $fillable = [
        'user_id', 'ticket_category_id','agent_id', 'member_id', 'customer_id', 'ticket_sub_category_id', 'file','file_type', 'message', 'status', 'is_delete'
    ];

    public function user()
    {
        return $this->hasOne('App\User','id','user_id');
    }

    public function ticket_categories()
    {
       return $this->hasOne('App\Ticket_category','id','ticket_category_id');
    }
    public function ticket_sub_categories()
    {
       return $this->hasOne('App\Ticket_sub_category','id','ticket_sub_category_id');
    }

    public function agent()
    {
        return $this->hasOne('App\Agent','id','agent_id');
    }
    public function customer()
    {
        return $this->hasOne('App\Customer','id','customer_id');
    }

    public function lastmessage()
    {
        return $this->hasOne('App\Ticketmessage','ticket_id','id')->orderBy('id', 'DESC');
    }

}
