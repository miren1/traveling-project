<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Agent_fee extends Model
{
    protected $table = 'agent_fees';
    protected $primaryKey = 'id';
    protected $fillable = ['agent_id','payment_id','payment_amount','reference_id','tx_status','payment_mode','tx_msg','tx_time','signature', 'type'];
}
