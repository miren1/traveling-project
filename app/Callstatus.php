<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Callstatus extends Model
{
    protected $table = 'call_status';
    protected $primaryKey = 'id';
    protected $fillable = [
        'user_id', 'name', 'is_delete'
    ];
}
