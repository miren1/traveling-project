<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'agents';
    protected $primaryKey = 'id';
    protected $fillable = [
        'user_id', 'first_name', 'last_name', 'company_name', 'official_address', 'contact_details', 'alternate_number', 'house_no', 'society_name', 'landmark', 'city_fk_id', 'state_fk_id', 'pincode', 'company_pan', 'company_gst_no', 'company_email', 'password', 'deleted_at'
    ];
}
