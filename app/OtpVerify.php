<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OtpVerify extends Model
{
    protected $fillable = ['mobile','email','otp','email_otp','otp_at','type'];
}
