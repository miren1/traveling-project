<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use App\User;
class Review extends Model
{
    use Notifiable;
    
    protected $table = 'reviews';
    protected $primaryKey = 'id';
    protected $fillable = [
        'user_id','role','review_image_id','content','deleted_at'
    ];

    function review_image()
    {
        return $this->hasMany('App\Review_image', 'review_id', 'id');
    }

    function member()
    {
        return $this->hasMany('App\AddMembership', 'id', 'member_id');
    }
    function user()
    {
        return $this->hasMany('App\User', 'id', 'user_id');
    }
    function customer()
    {
        return $this->hasMany('App\Customer', 'id', 'user_id');
    }
}
