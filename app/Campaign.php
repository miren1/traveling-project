<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Campaign extends Model
{
    protected $table = 'campaigns';
    protected $primaryKey = 'id';
    protected $fillable = [
        'user_id', 'name', 'count', 'is_delete'
    ];

    public function email()
    {
       return $this->hasMany('App\Email','campaign_id','id');
    } 
}
