<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Membership_Fee extends Model
{
    //use Notifiable;
    protected $table = 'membership_fees';
    protected $primaryKey = 'id';
    protected $fillable = ['member_id','payment_id','payment_amount','reference_id','tx_status','payment_mode','exampleRadios','tx_msg','tx_time','signature','type','payerVa','payeeVA','PayerName','PayerMobile','refundAmount','onlineRefund','note','collectByDate','merchantId','merchantName','terminalId','merchantTranId','TxnInitDate','TxnCompletionDate','BankRRN','payment_type'];
}
