<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Review_image extends Model
{
    use Notifiable;
    
    protected $table = 'review_images';
    protected $primaryKey = 'id';
    protected $fillable = [
        'review_id','image'
    ];

    
}
