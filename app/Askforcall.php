<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Askforcall extends Model
{
    protected $table = 'askforcalls';
    protected $primaryKey = 'id';
    protected $fillable = [
        'user_id', 'first_name', 'last_name', 'email','cat_id','sub_cat_id', 'contactnumber', 'wpnumber', 'datetime', 'is_delete'
    ];

}
