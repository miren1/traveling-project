<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Agent extends Authenticatable
{
    use Notifiable;
    
    protected $table = 'agents';
    protected $primaryKey = 'id';
    protected $fillable = [
        'user_id', 'first_name', 'last_name', 'company_name', 'official_address', 'contact_details', 'alternate_number', 'house_no', 'society_name', 'landmark', 'city_fk_id', 'state_fk_id', 'pincode', 'wallet', 'company_pan','company_gst_no', 'company_email', 'password', 'deleted_at'
    ];
}
