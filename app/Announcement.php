<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Announcement extends Model
{
    use Notifiable;
    
    protected $table = 'announcements';
    protected $primaryKey = 'id';
    protected $fillable = [
        'content','image','all_user', 'employee', 'angent', 'member','user','deleted_at'
    ];
}
