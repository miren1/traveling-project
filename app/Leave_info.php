<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Leave_info extends Model
{
    //
    protected $table='leaves_info';
    protected $fillable=['casual','planned','year'];

}
