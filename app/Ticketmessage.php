<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticketmessage extends Model
{
    protected $table = 'ticket_messages';
    protected $primaryKey = 'id';
    protected $fillable = [
        'user_id', 'ticket_id', 'ticket_category_id', 'ticket_sub_category_id', 'type','file','file_type', 'message', 'status', 'is_view', 'is_delete'
    ];


    public function user()
    {
       return $this->hasOne('App\User','id','user_id');
    }

    public function ticket()
    {
       return $this->hasOne('App\Ticket','id','ticket_id');
    }
}
