<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Assignlead extends Model
{
    protected $table = 'assign_leads';
    protected $primaryKey = 'id';
    protected $fillable = [
        'user_id','group_id','employee_id', 'group_lead_id', 'status'
    ];

    public function employee()
    {
       return $this->hasOne('App\User','id','employee_id')->with('mainleads');
    }

    public function user()
    {
       return $this->hasOne('App\User','id','user_id');
    }

   //  public function employee()
   //  {
   //     return $this->hasOne('App\User','id','employee_id');
   //  }


    public function scopeOfType($query, $type)
    {
        return $query->hasOne('App\User','id','employee_id')->ofType($type);
    }

    public function lead()
    {
       return $this->hasOne('App\Grouplead','id','group_lead_id');
    }

    public function group()
    {
       return $this->hasOne('App\Group','id','group_id');
    }

    public function activity()
    {
      return $this->hasMany('App\Leadstatus','group_lead_id','group_lead_id');
    }

   
    

}
