<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tour_gallery extends Model
{
    protected $table = 'galleries';
    protected $primaryKey = 'id';
    protected $fillable = [
        'category', 'title', 'tag', 'startdate', 'enddate', 'deleted_at'
    ];

    function gallery_image()
    {
        return $this->hasMany('App\Gallery_images', 'gallery_id', 'id')->where('deleted_at', '0');
    }
}
