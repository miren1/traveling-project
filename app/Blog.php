<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Blog extends Model
{
    use Notifiable;
    
    protected $primaryKey = 'id';
    protected $fillable = [
        'media_id','title', 'content', 'deleted_at'
    ];

    function blogs_ads()
    {
        return $this->hasMany('App\Ads_blog', 'blog_id', 'id');
    }

    function blogs_media()
    {
        return $this->hasMany('App\Blog_media', 'blog_id', 'id');
    }
}
