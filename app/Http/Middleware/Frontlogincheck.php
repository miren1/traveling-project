<?php

namespace App\Http\Middleware;

use Closure;

class Frontlogincheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
        if($request->session()->exists('type')){
            if($request->session()->get('type')=='agent' || $request->session()->get('type')=='customer' || $request->session()->get('type')=='member'){
                return $next($request);
            }else{
                return route('login');
            }
        }else{
            return route('login');
        }
    }
}
