<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * Indicates whether the XSRF-TOKEN cookie should be set on the response.
     *
     * @var bool
     */
    protected $addHttpCookie = true;

    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'leads/list','tickets/list','all-tickets/list','ticket-receive/list','packages/gettours','packages/getTourType','payment_returnURL/*/*','topup-return-url/*/*','payment_notifyURL','mandate/*','mandate_response/*/*/*/*','upicallback','verification'
        //
    ];
}
