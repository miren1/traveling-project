<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Packagtype;
use App\Toure;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\DB;
use Charts;
use Session;
use Pusher\Pusher;
use Illuminate\Support\Facades\Input;
use Mail;
use App\User;
use App\Message;
use Auth;
use Carbon\Carbon;
use App\Leave;
use App\Approve;
use Calendar;
use DateTime;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;  // Import Hash facade
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;



class ToursController extends Controller
{
    public function showtours(Request $request)
    {

      $plancat = Packagtype::where('deleted_at','1')->get();
      $packages = Toure::paginate(6);
  
      return view('employee.show_tours', compact('packages','plancat'));
     
    }



    public function tourfill()
    {
      $query = $_REQUEST['query'];

      $output = '';
      $data = [];
        if($query != '')
        {
          if($query == 'All')
          {
            $data = DB::table('toures')->get();
            // dd($data);
          }
          else{
            $data = DB::table('toures')
                    ->where('package_type','=', $query)
                    ->get();
          }
            $total_row = $data->count();
        }
        if($total_row > 0)
        {
          foreach($data as $value)
          {
            $output .= '
                        <div class="card box-shadow col-xl-4 col-md-6 mt-3" >
                          <div class="card-header">
                            <h4 class="py-md-4 py-xl-3 py-2"><center>'.$value->name .'</center></h4>
                          </div>
                          <div class="card-body">
                            <img src="'.url("/media/").'/'.$value->banner_img.'" style="width:100%;height:300px"><br><br>
                            <a href="'.url('/tourdetail/').'/'.base64_encode($value->id).'">
                            <button type="button" class="btn btn-sm btn-block py-2" style="background:#f3941e;color:white">Details</button>
                            </a>
                            <br>
                             <a href="'.url('/updatetourform/').'/'.base64_encode($value->id).'">
                            <button type="button" class="btn btn-sm btn-block py-2" style="background:#f3941e;color:white">Update</button>
                            </a>
                            <br>
                            <a href="'.url('/tourdelete/').'/'.base64_encode($value->id).'">
                            <button type="button" class="btn btn-sm btn-block py-2" style="background:#f3941e;color:white">Delete</button>
                            </a>
                          </div>
                        </div>';              
          } 
    
        $data = array(
         'table_data'  => $output,
         'total_data'  => $total_row
        );

        echo json_encode($data);
      }else{
              $output = '
                        <div  style="margin-left: 500px; font-size:20px"  >
                         <b>No Data Found.</b></div>
                        ';   
              $data = array(
                 'table_data'  => $output,
                 'total_data'  => $total_row
                );

        echo json_encode($data);
      }
    }




  public function tourdetail(Request $request,$id)
  {
    
    $result = DB::table('toures')->where('id',base64_decode($id))->get();
    $itenary = DB::table('itinerary')->where('tour_fk_id',$result[0]->id)->orderBy('day_number')->get();
    if($request->session()->get('role') == 'admin')
    {      return view('admin.tourDetails',compact('result','itenary')); }
    else{
    {   
        
     return view('employee.tourDetails',compact('result','itenary')); }

    }
  }
   public function tourfilter()
    {
      $query = $_REQUEST['query'];
      $company = $_REQUEST['company'];
      $output = '';
      $data = [];
      if ($company != "" && $query == "") {
        if($company == "All"){
            $data = DB::table('toures')->paginate(6);
        }else{

            $data = DB::table('toures')->where('package_type','=',$company)->paginate(6);
        }
      }elseif ($company == "" && $query != "") {
       $data = DB::table('toures')->where('name','LIKE','%'.$query.'%')->paginate(6);
      }elseif ($company != "" && $query != ""){
         if($company == "All"){

           $data = DB::table('toures')->where('name','LIKE','%'.$query.'%')->paginate(6);
         }else{

            $data = DB::table('toures')->where('name','LIKE','%'.$query.'%')->where('package_type','=',$company)->paginate(6);
         }
      }
      $total_row = $data->count();
        if($total_row > 0)
        {
          foreach($data as $value)
          {
            $output .= '
                        <div class="card box-shadow col-xl-4 col-md-6 mt-3" >
                          <div class="card-header">
                            <h4 class="py-md-4 py-xl-3 py-2"><center>'.$value->name .'</center></h4>
                          </div>
                          <div class="card-body">
                            <img src="'.url("/media/").'/'.$value->banner_img.'" style="width:100%;height:300px"><br><br>
                            <a href="'.url('/tourdetail/').'/'.base64_encode($value->id).'">
                            <button type="button" class="btn btn-sm btn-block py-2" style="background:#f3941e;color:white">Details</button>
                            </a>
                            <br>
                             <a href="'.url('/updatetourform/').'/'.base64_encode($value->id).'">
                            <button type="button" class="btn btn-sm btn-block py-2" style="background:#f3941e;color:white">Update</button>
                            </a>
                            <br>
                            <a href="'.url('/tourdelete/').'/'.base64_encode($value->id).'">
                            <button type="button" class="btn btn-sm btn-block py-2" style="background:#f3941e;color:white">Delete</button>
                            </a>
                          </div>
                        </div>';              
          } 
        $output .= '<div style="margin-left:40%; margin-bottom:5%;" id="page"> '.$data->links().'</div>';
        $data = array(
         'table_data'  => $output,
         'total_data'  => $total_row
        );

        echo json_encode($data);
      }else{
              $output = '
                        <div  style="margin-left: 500px; font-size:20px"  >
                         <b>No Data Found.</b></div>
                        ';   
              $data = array(
                 'table_data'  => $output,
                 'total_data'  => $total_row
                );

        echo json_encode($data);
      }
    }

}