<?php

namespace App\Http\Controllers;

use Auth;
use Mail;
use Charts;
use Session;
use App\User;
use Calendar;
use Response;
use App\Agent;
use App\Leave;
use App\Toure;
use App\Message;
use App\Setting;
use App\Customer;
use Pusher\Pusher;
use App\Leave_info;
use App\Membership;
use App\LeaveReport;
use Hashids\Hashids;
use App\AddMembership;
use App\Charts\UserChart;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Bus\DispatchesJobs;
use ArielMejiaDev\LarapexCharts\Facades\LarapexChart;
//use ConsoleTVs\Charts\Charts;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use AdminHelper;

class AdminController extends Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public function dashboard()
    {
        
        $year = date('Y');
        $month = date('m');
        $monthInquiry = DB::table('inquiry')->whereYear('created_at', '=', $year)
            ->whereMonth('created_at', '=', $month)
            ->count();
        $monthAgent =  DB::table('agents')->whereYear('created_at', '=', $year)
            ->whereMonth('created_at', '=', $month)
            ->count();
        $MonthCustomer =  DB::table('customers')->whereYear('created_at', '=', $year)
            ->whereMonth('created_at', '=', $month)
            ->count();

        $TotalOrder = 0;

        $monthMember =  DB::table('members')->whereYear('created_at', '=', $year)
            ->whereMonth('created_at', '=', $month)
            ->count();

        $MonthTarget = DB::table('targets')->whereYear('month_year', '=', $year)
            ->whereMonth('month_year', '=', $month)
            ->sum('target');

        $users = DB::table('members')->where(DB::raw("(DATE_FORMAT(created_at,'%Y'))"), date('Y'))
            ->get();
        $inq = DB::table('inquiry')->where(DB::raw("(DATE_FORMAT(created_at,'%Y'))"), date('Y'))
            ->get();

        /* $chart = Charts::database($users, 'bar', 'highcharts')
			      ->title("Monthly new Memberers")
			      ->elementLabel("Total Member")
			      ->dimensions(500,500)
                  ->responsive(false)
                 ->groupByMonth(date('Y'), true);

             $chart1 = Charts::database($inq, 'bar', 'highcharts')
			      ->title("Monthly New Inquiry")
			      ->elementLabel("Total Inquiry")
			      ->dimensions(500, 500)
			      ->responsive(false)
			      ->groupByMonth(date('Y'), true);*/
        $chart = '';
        $chart1 = ';';

        return view('admin.index', compact('monthInquiry', 'monthAgent', 'MonthCustomer', 'TotalOrder', 'monthMember', 'MonthTarget', 'chart', 'chart1'));
    }


    public function emailVerifyEmp(Request $request)
    {
        // dd($request->all());
        $data['employee'] = User::where('email','like','%'.$request->email.'%')->count();
        $data['customers'] = Customer::where('email','like','%'.$request->email.'%')->count();
        $data['agent'] = Agent::where('company_email','like','%'.$request->email.'%')->count();
        $data['members']= AddMembership::where('status','member')->where('email','like','%'.$request->email.'%')->count();
        $data['mandate'] = AddMembership::where('status','mandate')->where('email','like','%'.$request->email.'%')->count();
        // dd($data);
        if ($data['employee'] != 0 || $data['customers'] != 0 || $data['agent'] != 0 || $data['members'] != 0 || $data['mandate'] != 0) 
        {
            return response()->json(['status'=>'error']);
        }else{
            return response()->json(['status'=>'success']);
        }
    }

    public function verifyMobileEmp(Request $request)
    {
        $data['employee'] =  User::where('contact_number','like','%'.$request->mobileNo.'%')->Where('whatsup_number','like','%'.$request->mobileNo.'%')->count();

        $data['customers'] = Customer::where('mobile_number','like','%'.$request->mobileNo.'%')->where('whats_up','like','%'.$request->mobileNo.'%')->count();

        $data['agent'] = Agent::where('contact_details','like','%'.$request->mobileNo.'%')->count();
        
        $data['members']= AddMembership::where('status','member')->where('primary_contact','like','%'.$request->mobileNo.'%')->Where('secondary_contact','like','%'.$request->mobileNo.'%')->where('whatsapp_number','like','%'.$request->mobileNo.'%')->count();
        
        $data['mandate'] = AddMembership::where('status','mandate')->where('primary_contact','like','%'.$request->mobileNo.'%')->Where('secondary_contact','like','%'.$request->mobileNo.'%')->where('whatsapp_number','like','%'.$request->mobileNo.'%')->count();
        // dd($data);
        if ($data['employee'] != 0 || $data['customers'] != 0 || $data['agent'] != 0 || $data['members'] != 0 || $data['mandate'] != 0) 
        {
            return response()->json(['status'=>'error']);
        }else{
            return response()->json(['status'=>'success']);
        }
    }



    public function emplive()
    {

        $query = $_REQUEST['query'];
        $output = '';
        $data = [];

        if ($query != '') {
            $data = DB::table('users')
                ->where('name', 'like', $query . '%')
                ->orWhere('contact_number', 'like', $query . '%')
                ->orWhere('id', 'like', $query . '%')
                ->get();
            $total_row = $data->count();
        } else {
            $total_row = 0;
        }

        if ($total_row > 0) {
            foreach ($data as $row) {

                $output .= '
          <tr>
           <td>' . "JJ-EMP-" . $row->id . '</td>
           <td>' . $row->name . '</td>
           <td>' . $row->surname . '</td>
           <td>' . $row->contact_number . '</td>
           <td>' . $row->email . '</td>
          <td>' . $row->joining_date . '</td>
         <td>' . $row->salary . '</td>
          <td>
          
          <a href="/employeedetails/' . $row->id . '">
            <i class="fa fa-info-circle" aria-hidden="true" title="Detail View" style="font-size:20px"></i>
          </a>
            
            <a href="/employeeedit/' . $row->id . '">
           
             <i class="fa fa-edit" aria-hidden="true" title="Edit" style="font-size:20px"></i>
             </a>

               <i class="fa fa-stop-circle" aria-hidden="true" data-toggle="modal" data-target="#myModal" style="font-size:20px;color:red" title="Leave" data-myvalue="<?php  echo $row->id?>" >
        </i>
       
			</td>
          </tr>
          ';
            }
        } else {
            //   echo "have aama jay che";
            $output = '
         <tr>
          <td align="center" colspan="6">No Data Found</td>
         </tr>
         ';
        }

        $data = array(
            'table_data'  => $output,
            'total_data'  => $total_row
        );

        echo json_encode($data);
    }
    //***************** Agent Functions ****************************
    public function showAgent()
    {
        $agents = DB::table('agents')->where(['deleted_at' => 1])->paginate(10);
        return view('admin.show_agent', compact('agents'));
    }
    public function detailagent()
    {
        return view('admin.detail_agent');
    }
    public function editagent($id)
    {
        $state = DB::table('states')->get();
        $agentData = DB::table('agents')->select('*', 'cities.id as cid', 'cities.name as cityname', 'agents.id as aid')->join('cities', 'cities.id', '=', 'agents.city_fk_id')->where('agents.id', $id)->get();
        return view('admin.edit_agentform', compact('agentData', 'state'));
    }
    public function updatetourform($id)
    {
        //dd(base64_decode($id));
        $result = DB::table('packag_types')->where('deleted_at', 1)->get();
        $data = Toure::find(base64_decode($id));
       
        $small_info = json_decode($data->small_info,true);
		
        $detail = array();

        $detail['detail1'] = json_decode($data->detail1,true);
        $detail['detail2'] = json_decode($data->detail2,true);
        $detail['detail3'] = json_decode($data->detail3,true);
        $detail['detail4'] = json_decode($data->detail4,true);        
    
        //dd($data);    

        return view('admin.updatetourform', compact('result', 'data','small_info','detail'));
    }
    public function updateagent()
    {
        $id = $_POST['id'];
        $data = Input::except('_method', '_token');
        $result = DB::table('agents')
            ->where('id', $id)
            ->update($data);

        Session::flash('UpdateAgent', 'Agent Sucessfully Updated');
        return redirect()->route('adminshowagent');
    }


    public function leaveTemplate()
    {
        $dataTemp = array(
            'name' => 'umang patel',
            'sdate' => '2021-06-12',
            'edate' => '2021-06-12',
            'reason' => 'Birthday',
            'type' => 'half',
            'approve_status' => 'Approve'

        );

        //dd($dataTemp);

        return view('mail.leaveApprove', compact('dataTemp'));
    }

    public function AdeminLeaveResult($id, $action)
    {
        $userid = DB::table('leaves')->where('id', $id)->get();
        //print_r($userid);
        $uid = $userid[0]->employee_fk_id;
        $year = date("Y");
        $mode =  $userid[0]->leave_type;
        $total_leave = DB::table('leaves_info')->where('year', $year)->get();
        $total_pl = $total_leave[0]->planned;
        $total_cl = $total_leave[0]->casual;
        //echo $total_cl;
        $diff = 0;
        //echo $uid;
        // 	echo $mode;
        // die;
        $remaining_leave = DB::table('leaves')->where('approve_status', 'Approve')
            ->where('employee_fk_id', $uid)->where('leave_type', $mode)->sum('total_leave_days');

        if ($mode == 'CL') {
            $diff = $remaining_leave  - $total_cl;

            if ($diff >= 0) {
                $data = array(
                    'approve_status' => $action,
                    'total_days' => $userid[0]->total_leave_days,
                    'is_paid' => 'yes'
                    //'total_leave_days'=>
                );
            }
            if ($diff < 0) {
                $temp = $userid[0]->total_leave_days + $diff;
                if ($temp > 0) {
                    $data = array(
                        'approve_status' => $action,
                        'total_days' => $temp,
                        'is_paid' => 'yes'
                        //'total_leave_days'=>
                    );
                }
                if ($temp < 0) {
                    $data = array(
                        'approve_status' => $action,
                        //'total_days'=>$temp,
                        'is_paid' => 'No'
                        //'total_leave_days'=>
                    );
                }
            }
        }
        if ($mode == 'PL') {
            $diff = $remaining_leave  - $total_pl;


            if ($diff >= 0) {
                $data = array(
                    'approve_status' => $action,
                    'total_days' => $userid[0]->total_leave_days,
                    'is_paid' => 'yes'
                    //'total_leave_days'=>
                );
            }
            if ($diff < 0) {
                $temp = $userid[0]->total_leave_days + $diff;
                if ($temp > 0) {
                    $data = array(
                        'approve_status' => $action,
                        'total_days' => $temp,
                        'is_paid' => 'yes'
                        //'total_leave_days'=>
                    );
                }
                if ($temp < 0) {
                    $data = array(
                        'approve_status' => $action,
                        //'total_days'=>$temp,
                        'is_paid' => 'No'
                        //'total_leave_days'=>
                    );
                }
            }
        }

        if ($mode == 'WFH') {
            $data = array(
                'approve_status' => $action,

                'is_paid' => 'No'
                //'total_leave_days'=>
            );
        }

        $result = DB::table('leaves')->where('id', $id)->update($data);
        $options = array(
            'cluster' => env('PUSHER_APP_CLUSTER'),
            'encrypted' => true
        );
        $pusher = new Pusher(
            env('PUSHER_APP_KEY'),
            env('PUSHER_APP_SECRET'),
            env('PUSHER_APP_ID'),
            $options
        );

        $username = DB::table('users')->where('id', $uid)->get();
        //dd($username);
        $dataemail = $username[0]->email;
        $emailss = trim($dataemail);
        $arr = $emailss;
        
        $dataTemp = array(
            'name' => $username[0]->name,
            'sdate' => $userid[0]->leave_date_start,
            'edate' => $userid[0]->leave_date_end,
            'reason' => $userid[0]->reason,
            'type' => $userid[0]->leave_type,
            'approve_status' => $action

        );

        $body = view('mail.leaveApprove',$dataTemp)->render();
        
        AdminHelper::sendEmailTo($body .$dataTemp['approve_status'],'Jag Joyu Admin Department' ,$arr);
        
        /*Mail::send('admin/leaveRequest', $dataTemp, function ($message) use ($arr) {
            $message->from('umang@xsquaretec.com', 'JagJoyu Leave Request Application');
            $message->to($arr)->subject('jagjoyu Leave Application');
        });*/

        $employeename = $username[0]->name;
        $count = DB::table('leaves')->where('user_notification_status', '0')->where('employee_fk_id', $uid)->where('approve_status', '!=', 'Pending')->count();

        $data['uid'] = $uid;
        $data['message'] =  "Your Request For";
        $data['Leave_type'] = $mode;
        $data['sdate']  = $userid[0]->leave_date_start;
        $data['edate'] = $userid[0]->leave_date_end;
        $data['count'] = $count;
        $data['approve_status'] = $action;

        $pusher->trigger('approve-channel', 'App\\Events\\Approve', $data);

       // return redirect()->route('admindashboard');

        notify()->success('Leave Approved Successfully !');
        return redirect('allrequest');
    }
    // public function AdeminEmpResult($id, $action)
    // {
    //     $host =  $_SERVER['HTTP_HOST'];
    //     //  
    //     $twopar = 'employeeLogin';
    //     if ($action == 'Approve') {
    //         $r = DB::table('users')->where('id', $id)->update(['employee_approved' => 1, 'send_employment' => 0]);
    //         $e  = Db::table('users')->where('id', $id)->get();
    //         $data = array(
    //             'name' => $e[0]->name,
    //             'link' => $host . "/" . $twopar
    //         );
    //         $arr = trim($e[0]->email);
    //         Mail::send('admin/empApprove', $data, function ($message) use ($arr) {
    //             $message->from('jagjoyu12@gmail.com', 'JagJoyu - Employment Confirmation');
    //             $message->to($arr)->subject('Employment Confirmation');
    //         });
    //     } else {
    //         $r = DB::table('users')->where('id', $id)->update(['send_employment' => 0]);

    //         $e  = Db::table('users')->where('id', $id)->get();
    //         $data = array(
    //             'name' => $e[0]->name,
    //             'link' => $host . "/" . $twopar
    //         );
    //         $arr = trim($e[0]->email);
    //         $r = DB::table('users')->where('id', $id)->update(['employee_approved' => 2]);
    //         Mail::send('admin/unapprove', $data, function ($message) use ($arr) {
    //             $message->from('jagjoyu12@gmail.com', 'JagJoyu - Employment Confirmation');
    //             $message->to($arr)->subject('Employment Confirmation');
    //         });
    //     }

    //     return redirect()->route('admindashboard');
    // }

    public function DetailLeaveRequest($id)
    {
        $leaveinfo = DB::table('leaves')->where('leaves.id', $id)->select('*', 'users.id as uid', 'leaves.id as lid')->join('users', 'leaves.employee_fk_id', '=', 'users.id')->get();
        return view('admin.detail_leave', compact('leaveinfo'));
    }
    public function agentdelete($id)
    {
        $result =  DB::table('agents')->where(['id' => $id])->delete(['id' => $id]);
        Session::flash('AgentDelete', 'Agent Successfully Deleted ');
        return redirect()->route('adminshowagent');
    }
    public function agentdetail($id)
    {
        $agentProfile =  DB::table('agents')->select('*', 'cities.name as city', 'states.name as state')->join('cities', 'cities.id', '=', 'agents.city_fk_id')->join('states', 'states.id', '=', 'agents.state_fk_id')->where(['agents.id' => $id])->get();
        return view('admin.detail_agent', compact('agentProfile'));
    }
    public function adminAgentForm()
    {
        $state = DB::table('states')->get();

        return view('admin.insert_agent', compact('state'));
    }
    public function agentCreation(Request $request)
    {
        $data = Input::except('_method', '_token');
        $company_email = $_POST['company_email'];

        $validator = Validator::make($request->all(), [
            'company_email' => 'required|email|unique:agents,company_email',

        ]);

        if ($validator->fails()) {
            Session::flash('error', 'Unique Email is Necessary');
        } else {
            $result = DB::table('agents')->insert($data);
            Session::flash('agenrCreate', 'Agent Create Successfully');
        }
        return redirect()->route('adminshowagent');;
    }
    public function salarylive()
    {
        $query = $_REQUEST['query'];
        $output = '';
        $data = [];

        if ($query != '') {
            $data = DB::table('salary')
                ->select('salary.*', 'users.id as eid', 'salary.id as id')
                ->join('users', 'users.id', '=', 'salary.emp_fk_id')
                ->where('fk_emp_id', $query)
                ->get();
            $total_row = $data->count();
        } else {
            $total_row = 0;
        }
        if ($total_row > 0) {
            foreach ($data as $row) {

                $output .= '
          <tr>
           <td>' . "JJ-EMP-" . $row->eid . '</td>
          <td>' . $row->name . '</td>
         <td>' . $row->salary . '</td>
         <td>' . $row->follow_date . '</td>
         
        
          <td>
         
            <a href="/editsalary/' . $row->id . '">
           
             <i class="fa fa-edit" aria-hidden="true" title="Edit" style="font-size:20px"></i>
             </a>

            

</td>
          </tr>
          ';
            }
        } else {
            //   echo "have aama jay che";
            $output = '
         <tr>
          <td align="center" colspan="6">No Data Found</td>
         </tr>
         ';
        }

        $data = array(
            'table_data'  => $output,
            'total_data'  => $total_row
        );

        echo json_encode($data);
    }

    public function agentlive()
    {
        $query = $_REQUEST['query'];
        $output = '';
        $data = [];

        if ($query != '') {
            $data = DB::table('agents')
                ->where('first_name', 'like', $query . '%')
                ->where('deleted_at', 1)
                ->orWhere('last_name', 'like', $query . '%')
                ->orWhere('id', 'like', $query)
                ->get();


            $total_row = $data->count();
        } else {
            $total_row = 0;
        }
        if ($total_row > 0) {
            foreach ($data as $row) {

                $output .= '
          <tr>
           <td>' . "JJ-AG-" . $row->id . '</td>
          <td>' . $row->first_name . '</td>
         <td>' . $row->last_name . '</td>
         <td>' . $row->company_name . '</td>
         <td>' . $row->contact_details . '</td>
         <td>' . $row->company_email . '</td>
         <td>' . $row->company_gst_no . '</td>
          <td>
          <a href="/agentdetails/' . $row->id . '">
<i class="fa fa-info-circle" aria-hidden="true" title="Detail View" style="font-size:20px"></i>
            </a>   
            <a href="/editagent/' . $row->id . '">
           
             <i class="fa fa-edit" aria-hidden="true" title="Edit" style="font-size:20px"></i>
             </a>

             <a href="/agentdelete/' . $row->id . '">


            <i class="fa fa-trash" aria-hidden="true" onclick="return confirm("Are you Sure Delete this Inquiry?")" style="color:red;font-size:20px" title="Delete"></i>
        </a>

</td>
          </tr>
          ';
            }
        } else {

            $output = '
         <tr>
          <td align="center" colspan="6">No Data Found</td>
         </tr>
         ';
        }

        $data = array(
            'table_data'  => $output,
            'total_data'  => $total_row
        );

        echo json_encode($data);
    }

    // *********************** Package Function *************************
    public function packagedelete($id)
    {
        $result =  DB::table('packag_types')->where(['id' => base64_decode($id)])->update(['deleted_at' => 0]);
        //Session::flash('PackageDelete', 'Plan Successfully Deleted ');
            if($result) {

                notify()->success('Package Successfully Deleted !!');
            } else {
                notify()->error('Package not Deleted Successfully!');
            }
        return redirect()->route('adminshowpackages');
    }
    public function showPackage()
    {
        $packages = DB::table('packag_types')->where(['deleted_at' => 1])->get();

        return view('admin.show_packages', compact('packages'));
    }
    public function admininsertPackageForm()
    {
        return view('admin.insertPackageForm');
    }
  
  	public function emp_membership_plan()
      {
        $packages = DB::table('plans')->where(['role'=>'customer'])->get();

        //dd($packages);
        return view('admin.membership', compact('packages'));
      }
  
  	public function add_membership_plan()
      {
        $packages = DB::table('plans')->where(['role'=>'customer'])->get();

        //dd($packages);
        return view('admin.add_membership_plan', compact('packages'));
      }
  
  	public function create_membership_plan(Request $request)
      {
        	/*dd($request->All());*/
      		$datainput = array(
                'fixed_amt' => $request->fixed_amt,
                'years' => $request->years,
                'role' => $request->role,
              	'mebership_fees' => $request->mebership_fees,
              	'free_nights_room' => $request->free_nights_room,
              	'free_nights_to_avail_in' => $request->free_nights_to_avail_in,
              	'free_night_in_any_type_of_hotel' => $request->free_night_in_any_type_of_hotel,
              	'monthly_emi' => $request->monthly_emi,
              	'quarterly_emi' => $request->quarterly_emi,
                'half_yearly_emi' => $request->half_yearly_emi,
              	'annual_emi' => $request->annual_emi,
              	'emi_dafulter_charges' => $request->emi_dafulter_charges,
              	'flexbility_to_move_free_night_to_next_year' => $request->flexbility_to_move_free_night_to_next_year,
              	'easy_upgrade_of_membership_plans' => $request->easy_upgrade_of_membership_plans,
              	'referral' => $request->referral,
                
            );

            //dd($datainput);
			 $result = Membership::create($datainput);

            if($result) {
                notify()->success('Membership Plan Successfully Added !!');
            } else {
                notify()->error('Membership Plan not Added Successfully!');
            }
            
            return redirect('membership-plan');

           // $user = DB::table('users')->insert($datainput);
      }
    
      
      public function checkSlug(Request $request ,$id = null){    
        $where = array('slug'=>$request->slug,'deleted_at'=>'1');
        
        if($id != ""){

            $where = array(
                array('slug',$request->slug),
                array('id','<>',$id),
                array('deleted_at','1'),
            );
        }
        $arrData = DB::table('packag_types')->where($where)->get();
        //dd($arrData,$where);
        if(count($arrData) != "0"){
            return response()->json(array('valid'=>false));
        }
        return response()->json(array('valid'=>true));
    }  
      
      
    public function createplan(Request $request)
    {
        if ($files = $request->file('photo')) {
            $photoname = time() . $files->getClientOriginalName();
            $destinationPath = 'uploads/plans_images';
            $files->move($destinationPath, $photoname);
        }
        $data['photo'] = $photoname;
        $data['name'] =$request->name;
        $data['tag_line'] =$request->tag_line;
        $data['details'] =$request->details;
        $data['user_id'] =Auth::user()->id;
        $data['slug']=$request->slug;
        //dd($data); 

        $result = DB::table('packag_types')->insert($data);
        if($result) {

                notify()->success('Package Successfully Added !!');
         } else {
                notify()->error('Package not Added Successfully!');
            }
        return redirect()->route('adminshowpackages');
    }
    public function updateeplan(Request $request)
    {
        $id = $_POST['id'];
        

        if ($files = $request->file('photo')) {
            $photoname = time() . $files->getClientOriginalName();
            $destinationPath = 'uploads/plans_images';
            $files->move($destinationPath, $photoname);
            $data['photo'] = $photoname;
        } else {
            $result = DB::table('packag_types')->where('id', $id)->get();
            $data['photo'] = $result[0]->photo;
        }


        $data['name'] =$request->name;
        $data['tag_line'] =$request->tag_line;
        $data['details'] =$request->details;
        $data['slug']=$request->slug;
        $result = DB::table('packag_types')
            ->where('id', $id)
            ->update($data);
      //dd($result);

        if($result) {

                notify()->success('Package Successfully Updated !!');
         } else {
                notify()->error('Package not Updated Successfully!');
            }


        return redirect()->route('adminshowpackages');
    }
    public function updatepackageform($id)
    {
        $package = DB::table('packag_types')->where('id', base64_decode($id))->get();
        return view('admin.edit_package', compact('package'));
    }

    //************************Customer Function ***************************** */
    public function customerdelete($id)
    {
        $result =  DB::table('customers')->where(['id' => $id])->update(['deleted_at' => 0]);
        return redirect()->route('adminshowcustomer');
    }
    // public function adminCreateCustomer()
    // {
    //     $data = Input::except('_method', '_token');
    //     $result = DB::table('customers')->insert($data);
    //     Session::flash('customerCreation', 'Customer Successfully Created ');
    //     return redirect()->route('adminshowcustomer');
    // }
    // public function adminformcustomer()
    // {
    //     return view('admin.create_customer_form');
    // }
    // public function showCustomer()
    // {
    //     $customers = DB::table('customers')->where(['deleted_at' => 1])->paginate(10);
    //     return view('admin.show_customer', compact('customers'));
    // }
    // public function customerdetails($id)
    // {
    //     $customer  = DB::table('customers')->select('*')->where('id', $id)
    //         ->get();
    //     // Booking Detail Remaining
    //     //   $customerbookingDetail = DB::table('inquiry_comments')->join('users', 'users.id', '=', 'inquiry_comments.emp_id_fk')->select('*', 'inquiry_comments.id as inqid')->where(['inq_id'=>$id])->paginate(10);
    //     return view('admin.detail_customer', compact('customer'));
    // }
    public function editcustomerform()
    {
        $customer = DB::table('customers')->get();
        return view('admin.edit_customer', compact('customer'));
    }
    public function customerlive()
    {
        $query = $_REQUEST['query'];
        $output = '';
        $data = [];

        if ($query != '') {
            $data = DB::table('customers')
                ->where('first_name', 'like', $query . '%')
                ->where('deleted_at', 1)
                ->orWhere('last_name', 'like', $query . '%')
                ->orWhere('id', 'like', $query)
                ->get();
            $total_row = $data->count();
        } else {
            $total_row = 0;
        }
        if ($total_row > 0) {
            foreach ($data as $row) {

                $output .= '
          <tr>
              <td>' . "JJ-USER-" . $row->id . '</td>
          <td>' . $row->first_name . '</td>
         <td>' . $row->last_name . '</td>
         <td>' . $row->mobile_number . '</td>
         <td>' . $row->email . '</td>
         <td>' . $row->whats_up . '</td>
         <td>' . $row->created_at . '</td>
          <td>

<a href="/customerdetails/' . $row->id . '">
<i class="fa fa-info-circle" aria-hidden="true" title="Detail View" style="font-size:20px"></i>
  </a>   

  <a href="/editcustomerform/' . $row->id . '">


   <i class="fa fa-edit" aria-hidden="true" title="Edit" style="font-size:20px"></i>
   </a>
   <a href="/customerdelete/' . $row->id . '">

  <i class="fa fa-trash" aria-hidden="true" onclick="return confirm("Are you Sure Delete this Customer?")" style="color:red;font-size:20px" title="Delete"></i>
</a>

</td>
          </tr>
          ';
            }
        } else {
            //   echo "have aama jay che";
            $output = '
         <tr>
          <td align="center" colspan="6">No Data Found</td>
         </tr>
         ';
        }

        $data = array(
            'table_data'  => $output,
            'total_data'  => $total_row
        );

        echo json_encode($data);
    }
    public function adminEditCustomer()
    {
        $id = $_POST['id'];
        $data = Input::except('_method', '_token');

        $result = DB::table('customers')
            ->where('id', $id)
            ->update($data);
        Session::flash('UpdateCustomer', 'Customer Sucessfully Updated');
        return redirect()->route('adminshowcustomer');
    }

    public function adminLogin()
    {
        return view('admin.adminlogin');
    }
    // public function showPackage()
    // {
    //     return view('admin.show_packages');
    // }
    public function insertEmployee()
    {
        return view('admin.insert_employee_form');
    }
    public function createEmployee(Request $request)
    {

        
        $data = $request->email;
        $emp_help_desk_assign  = 0;
        $executives = 0;

        $user = User::where('email', $request->email)->get();
        //dd($user);

        /*$validator = Validator::make($request->all(), [
            'email' => 'required|email|unique:users',
            'contact_number' => 'required|unique:users'
        ]);

        dd($validator);
*/
       // $Emp_data = User::where('')
        //dd($validator);
        if (isset($_POST['emp_help_desk_assign'])) {
            $emp_help_desk_assign = 1;
        }

        if (isset($_POST['executives'])) {
            $executives = 1;
        }

        if (count($user) > 0) {

            notify()->error('Unique Email is necessary !!');
            return redirect()->route('admininsertemployee');
          
        } else {
            
            $input = $request->all();

            $datainput = array(
                'email' => $data,
                'designation' => $request->designation,
                'salary' => $request->salary,
                'joining_date' => $request->follow_date,
                'executives' => $executives,
                'contact_number' => $request->contact_number,
                'emp_help_desk_assign' => $emp_help_desk_assign,
            );

            $user = DB::table('users')->insert($datainput);
            $lastid = DB::table('users')->where('email',$data)->first();

            $salaryarray = array(
                'salary' => $request->input('salary'),
                'follow_date' => $request->input('follow_date'),
                'fk_emp_id' => $lastid->id
            );

            $salary = DB::table('salary')->insert($salaryarray);
            $host =  $_SERVER['HTTP_HOST'];
            //   echo $host;
            $twopar = 'createyourProfile';
            $id = Crypt::encrypt($lastid->id);


            $link_creation = $host . "/" . $twopar . "/" . $id;

            $emailss = trim($data);
            /*$data = [
                'link_creation' => $link_creation
            ];*/
            //dd($data);
            $arr = $emailss;
            $body = view('mail.sendMail',compact('link_creation'))->render();
            // return $body;
            $emp = AdminHelper::sendEmailTo($body,'Verify Employee',$arr);

            if($emp){
                notify()->success('Employee Creation Done. Mail sent to Employee.');
            }else{
                notify()->error('Something went wrong please try again.');
            }
            /*Session::flash('admincreation', 'Employee Creation Done. Mail sent to Employee');
            Session::flash('class', 'success');*/
            return redirect()->route('admininsertemployee');
        }
    }

    /*public function MailTamplate(){
        return view('admin.mail.sendMail');
    }*/

    public function adminlogout(Request $request)
    {
        $request->session()->forget('admin_email');
        $request->session()->forget('admin_name');
        // $request->session()->forget('_token');
        $request->session()->flush();
        // Session::flush();
        // session_destroy();
        Session::flush();
        Session::regenerate(true);
        return redirect()->route('adminlogin');
    }
    public function admindLoginCheck(Request $request)
    {
        $email = $_POST['email'];
        $password = $_POST['password'];


        $userdata = array(
            'email'     => $email,
            'password'  => $password,
            'role' => 'admin'
        );
        if (Auth::attempt($userdata)) {
            //echo 'SUCCESS!';
            $email = Auth::user()->email;
            $id = Auth::user()->id;
            $name = Auth::user()->name;
            $surname = Auth::user()->surname;
            $fullname = $name . " " . $surname;
            $request->session()->put('admin_email', $email);
            $request->session()->put('admin_name', $name);
            $request->session()->put('role', 'admin');
            $request->session()->put('aid', $id);


            return redirect()->route('admindashboard');
        } else {
            Session::flash('error', 'Wrong Email Or Password Please Check');
            return redirect()->route('adminlogin');
        }
    }
    public function employeedetails(Request $request, $id)
    {
        $employeeProfile = DB::table('users')->where('id', '=', base64_decode($id))->first();
        $c_statename = DB::table('states')->where('id', $employeeProfile->c_state_id)->first();
        $c_cityname = DB::table('cities')->where('id', $employeeProfile->c_city_id)->first();
        $p_statename = DB::table('states')->where('id', $employeeProfile->p_state_id)->first();
        $p_cityname = DB::table('states')->where('id', $employeeProfile->p_city_id)->first();
        $current = date("Y/m/d");
        $salary =  DB::table('salary')->where('fk_emp_id', $employeeProfile->id)->where('follow_date', '<=', $current)->orderBy('follow_date', 'desc')->limit(1)->first();
        // dd($employeeProfile);
        if ($request->session()->get('role') == 'user') {

            return view('employee.detail_employee', compact('employeeProfile', 'c_statename', 'c_cityname', 'p_statename', 'p_cityname', 'salary'));
        } else {
            return view('admin.detail_employee', compact('employeeProfile', 'c_statename', 'c_cityname', 'p_statename', 'p_cityname', 'salary'));
        }
    }
    public function documentemp(Request $request)
    {
        $id = $request->id;
        $document = $request->document;
        $employeeProfile =  DB::table('users')->where('id', $id)->get();
        $file = "uploads/" . $employeeProfile[0]->$document;
        return response()->download($file, 'employee_document.pdf', [], 'inline');
    }
    public function EditEmployee($id)
    {
        $userDetail = DB::table('users')->where('id', base64_decode($id))->get();

        $state = DB::table("states")
            ->where("country_id", 101)
            ->pluck("name", "id");
        return view('admin.edit_employee_form', compact('userDetail', 'state'));
    }

    public function showEmployee()
    {
        // $employee =  DB::table('users')->where('leave_date','=',NULL)->where('employee_approved','=',1)->where('role','user')->paginate(10);

        return view('admin.show_employee');
    }
    public function EmployLeave(Request $request)
    {

        $id = $request['id'];
        $leave_date = $request['leave_date'];
        // dd($id);
        $employee =  DB::table('users')->where('id', $id);
        $employee->delete();
        // Session::flash('EmployeeLeaveUpdated', 'Employee Data Updated');
        return redirect()->route('adminshowemployee');
    }
    public function allRequest()
    {
        $employees = User::where('role','user')->where('employee_approved',1)->whereNull('leave_date')->get();
        // dd($employees);
        return view('admin.request', compact('employees'));
    }
    public function inquirydetails($id)
    {
        $inquries  = DB::table('inquiry')->join('users', 'users.id', '=', 'inquiry.emp_handle_by_fk')->select('*', 'inquiry.id as inqid')->where('inquiry.id', $id)
            ->get();
        $datacom = DB::table('inquiry_comments')->join('users', 'users.id', '=', 'inquiry_comments.emp_id_fk')->select('*', 'inquiry_comments.id as inqid')->where(['inq_id' => $id])->paginate(10);

        $admincomments =   DB::table('inquiry_comments')->select('*', 'inquiry_comments.id as inqid')->where(['emp_id_fk' => 0, 'inq_id' => $id])->paginate(10);

        return view('admin.detail_inquiry', compact('inquries', 'datacom', 'admincomments'));
    }
    public function adminaddComments()
    {
        $id = $_POST['id'];
        $data = array(
            'comments' => $_POST['comment'],
            'emp_id_fk' => 0,
            'inq_id' => $id
        );
        $ans =  DB::table('inquiry_comments')->insert($data);
        Session::flash('Comments Added', 'Comments Added');
        return back();
    }
    public function inquirydelete($id)
    {

        $result =  DB::table('inquiry')->where(['id' => $id])->update(['deleted_at' => 0, 'inq_status' => 'delete']);
        Session::flash('DeleteInquiey', 'Inquiry Sucessfully Deleted');

        return redirect()->route('allinquiry');
    }
    public function allinquiry()
    {

        $users = DB::table('inquiry')->join('users', 'users.id', '=', 'inquiry.emp_handle_by_fk')->select('*', 'inquiry.id as inqid', 'inquiry.email as iemail')->where('deleted_at', 1)
            ->paginate(10);




        //return view('employee.createprofile',compact('state','userDetail'));
        return view('admin.show_inquiry', compact('users'));
    }
    public function createInquiry()
    {
        $data = Input::except('_method', '_token');

        $datet = date('Y-m-d', strtotime($data['appointment_date']));
        $timet = date('h:i:s', strtotime($data['app_time']));
        $data['appointment_date'] = $datet . ' ' . $timet;
        $data = array(
            'first_name' => $_POST['first_name'],
            'last_name' => $_POST['last_name'],
            'mobile_no' => $_POST['mobile_no'],
            'whatsup_no' => $_POST['whatsup_no'],
            'appointment_date' => $data['appointment_date'],
            'emp_handle_by_fk' => $_POST['emp_handle_by_fk'],
            'email' => $_POST['email'],
            'country_id' => $_POST['country_id'],
            'state_id' => $_POST['state_id'],
            'city_id' => $_POST['city_id']
        );
        $ans = DB::table('inquiry')->insert($data);
        Session::flash('inquiryAdd', 'Inquiry Sucessfully Added');
        return redirect()->route('adminInquiryForm');
    }
    public function adminInquiryForm()
    {
        $users = DB::table('users')->where('employee_approved', 1)->where('leave_date', NULL)->where('role', 'user')->get();
        $country = DB::table('countries')->get();
        return view('admin.insert_inquiry', compact('users', 'country'));
    }
    public function inquirymail()
    {
        return view('admin.sentmail');
    }
    public function editinquiry($id)
    {
        $users = DB::table('users')->get();
        $country = DB::table('countries')->get();

        $inquiryData = DB::table('inquiry')->where('id', $id)->get();

        return view('admin.edit_inquiry', compact('users', 'inquiryData', 'country'));
    }
    public function adminEditInquiry()
    {

        $id = $_POST['id'];
        $data = Input::except('_method', '_token');

        $datet = date('Y-m-d', strtotime($data['appointment_date']));
        $timet = date('h:i:s', strtotime($data['app_time']));
        $data['appointment_date'] = $datet . ' ' . $timet;
        $data = array(
            'first_name' => $_POST['first_name'],
            'last_name' => $_POST['last_name'],
            'mobile_no' => $_POST['mobile_no'],
            'whatsup_no' => $_POST['whatsup_no'],
            'appointment_date' => $data['appointment_date'],
            'emp_handle_by_fk' => $_POST['emp_handle_by_fk'],
            'email' => $_POST['email'],
            'country_id' => $_POST['country_id'],
            'state_id' => $_POST['state_id'],
            'city_id' => $_POST['city_id'],
            'inq_status' => $data['inq_status'],

        );
        if ($_POST['inq_status'] == 'done') {
            $current = date("Y-m-d");
            $data['done_date'] = $current;
        }

        $result = DB::table('inquiry')
            ->where('id', $id)
            ->update($data);
        Session::flash('UpdateInquiey', 'Inquiry Sucessfully Updated');
        return redirect()->route('allinquiry');
    }
    public function inquieylive()
    {
        $query = $_REQUEST['query'];
        // $sdate = $_REQUEST['sdate'];
        // $status = $_REQUEST['status'];
        $output = '';
        $data = [];
        // && $sdate != '' && $status !=''
        if ($query != '') {
            $data = DB::table('inquiry')
                ->where('first_name', 'like', $query . '%')
                ->where('deleted_at', 1)
                ->orWhere('last_name', 'like', $query . '%')
                ->orWhere('mobile_no', 'like', $query . '%')
                ->orwhere('id', 'like', $query)
                ->get();
            $total_row = $data->count();
        }
        // if($query == '' && $sdate != '')
        // {
        //  $data = DB::table('inquiry')
        // ->where('created_at','like',$sdate)
        //  ->where('inq_status','like',$status)
        //  ->where('deleted_at',1)
        //  ->get();
        //  $total_row = $data->count();
        //  }
        //  if($query != '' && $sdate == '')
        //  {
        //   $data = DB::table('inquiry')
        //   ->where('first_name', 'like', $query.'%')
        //   ->where('deleted_at',1)
        //   ->where('inq_status','like',$status)
        //   ->orWhere('last_name', 'like', $query.'%')
        //   ->orWhere('mobile_no', 'like', $query.'%')
        //   ->orwhere('id','like',$query.'%')
        //   ->get();
        //   $total_row = $data->count();
        //  }
        //  if($query == '' && $sdate == '')
        //  {
        //      $data = DB::table('inquiry')
        //    ->where('inq_status','like',$status)
        //    ->where('deleted_at',1)
        //   ->get();
        //   $total_row = $data->count();
        //  }

        if ($total_row > 0) {
            foreach ($data as $row) {
                $a =  DB::table('users')->where('id', $row->emp_handle_by_fk)->get();


                $output .= '
        <tr>
          <td>' . "JJ-INQ-" . $row->id . '</td>
         <td>' . $row->first_name . '</td>
         <td>' . $row->mobile_no . '</td>
         <td>' . $row->email . '</td>
         <td>' . $row->appointment_date . '</td>
         <td>' . $row->inq_status . '</td>
         <td>' . $a[0]->name . '</td>
         <td>
                      <a href="/inquirydetails/' . $row->id . '">
                      
                      <i class="fa fa-info-circle" aria-hidden="true" title="Detail View" style="font-size:20px"></i>
                        </a>   

                      
                         <a href="/editinquiry/' . $row->id . '">
                       
                         <i class="fa fa-edit" aria-hidden="true" title="Edit" style="font-size:20px"></i>
                         </a>
                         <a href="/inquirydelete/' . $row->id . '">
                       
    <i class="fa fa-trash" aria-hidden="true" onclick="return confirm("Are you Sure Delete this Inquiry?")" style="color:red;font-size:20px" title="Delete"></i>
                    </a>

                </td>
        </tr>
        ';
            }
        } else {
            //   echo "have aama jay che";
            $output = '
       <tr>
        <td align="center" colspan="6">No Data Found</td>
       </tr>
       ';
        }

        $data = array(
            'table_data'  => $output,
            'total_data'  => $total_row
        );

        echo json_encode($data);
    }
    //******************************Admin Profiles *************************** */
    public function getAdminProfile(Request $request)
    {
        $aid =  $request->session()->get('aid');

        //  echo $aid;
        //  die;
        $result = DB::table('users')->where('id', '=', $aid)->get();
        return view('admin.changeprofile', compact('result'));
    }
    public function updateAdmin(Request $request)
    {
        $name = $_POST['name'];
        $aid = $request->session()->get('aid');
        $contact_number = $_POST['contact_number'];
        $request->session()->put('admin_name', $name);
        $rs = DB::table('users')
            ->where('id', $aid)
            ->update([
                'name' => $name,
                'contact_number' => $contact_number
            ]);
        Session::flash('profile_sucess', 'Profile Successfully Updated');
        return redirect()->route('getAdminProfile');
    }
    public function changePasword()
    {
        return view('admin.changepassword');
    }

    public function changePasswordUpdate(Request $request)
    {
        $password = $_POST['password'];
        $newPasssword = $_POST['npassword'];
        $id = $request->session()->get('aid');
        $result = DB::table('users')->where(['role' => 'admin'])->where('id', $id)->get();
        $userdata = array(
            'password' => $password,
            'id'  => $id,
            'role' => 'Admin'
        );
        $npass = Hash::make($password);
        if (Auth::attempt($userdata)) {
            $rs = DB::table('users')
                ->where('id', $id)
                ->update([
                    'password' => $npass
                ]);
            Session::flash('password_change', 'Password Successfully Changed');
        } else {
            Session::flash('password_change', 'Password is wrong');
        }
        return view('admin.changepassword');
    }
    // AAyush Upload Image
    public function showbannerimage()
    {
        return view('admin.show_bannerimage');
    }
    //this function is used to upload the image
    public function uploadimage(Request $request)
    {
        $input = $request->all();
        if ($files = $request->file('banner_img')) {
            //defining the upload the path
            $destationPath = public_path('/upload/'); //upload path
            foreach ($files as $img) {
                //upload orignal image
                $banner_img = $img->getClientOriginalName();
                $img->move($destationPath, $banner_img);
                //save into database
                $imagemodel = new Photo();
                $imagemodel->banner_img = "$banner_img";
                $imagemodel->save();
            }
        }
        return back()->with('success', 'your image is uploaded Successfully');
    }
    // ***************************Leaves Info********************************************
    public function leaves_info()
    {
        $data = Leave::all();
        /*$events = [];
        if ($data->count()) {
            foreach ($data as $key => $value) {

                $events[] = Calendar::event(
                    $value->title,
                    true,
                    new \DateTime($value->start_date),
                    new \DateTime($value->end_date . ' +1 day'),
                    null,
                    [
                        'color' => '#f05050',
                        'url' => 'pass here url and any route',
                    ]
                );
            }
        }
        dd($events);*/
        //$calendar = Calendar::addEvents($events);
        return view('admin.leaves', compact('data'));
    }
    public function adminLeaveForm()
    {
        $year = date("Y");
        $data = DB::table('leaves_info')->where('year', $year)->first();
        return view('admin.insert_leave', compact('data'));
    }

    public function LeaveReport()
    {
        //$year = date("Y");
        $data = DB::table('leaves')->get();
       
        return view('admin.show_leaves_report', compact('data'));
    }

    public function AllLeaveReport()
    {
        $user_id = Auth::user()->id;

        if (Auth::user()->role =='admin') {
            
            //$texts = LeaveReport::with('employee')->orderBy('created_at', 'desc')->get();

            $texts = DB::table('leaves')
            ->join('users', 'users.id', '=', 'leaves.employee_fk_id')
            ->select('leaves.*', 'users.name', 'users.middlename'
        , 'users.surname')
            ->get();

        }else{

            $texts = DB::table('leaves')
            ->join('users', 'users.id', '=', 'leaves.employee_fk_id')
            ->select('leaves.*', 'users.name', 'users.middlename'
        , 'users.surname')
            ->where('employee_fk_id ',$user_id)
            ->get();
            //$texts = LeaveReport::with('employee')->where('employee_fk_id ',$user_id)->orderBy('created_at', 'desc')->get();

        
        }
        return response()->json(array('data' => $texts));
        //return response()->json(array('data' => $texts));

      
    }

    public function createLeaveAdmin(Request $request)
    {
        if($request->id){
            $data = array(
                'title'=>$request->title,
                'start_date'=>$request->start_date,
            );
        //dd($data);
        $res = Leave::where('id',$request->id)->update($data);
        
        if($res)
            {
                notify()->success('Leave updated Successfully !');
            }
            else
            {
                notify()->error('Something Wrong Try Again..!');
            }
        }else{

            $leave = new Leave;
            $leave->title = $request->input('title');
            $leave->start_date = $request->input('start_date');
            $leave->end_date = $request->input('start_date');
            $res = $leave->save();
            if($res)
            {
                notify()->success('Leave added Successfully !');
            }
            else
            {
                notify()->error('Something Wrong Try Again..!');
            }
            //Session::flash('LeaveCreated', 'Leave Successfully Created');
        }
        return redirect()->route('adminLeaveForm');
    }
    public function showLeave()
    {
        $data = Leave::all();
        return view('admin.show_leaves', compact('data'));
    }

    public function FestivalLeaveLog()
    {
        //if (Auth::user()->role =='admin') {

            return datatables()->of(Leave::all())->toJson();
       // }
        //$data = Leave::all();
        
    }

    public function editleave($id)
    {   
        $id = base64_decode($id);
        $leave_data = Leave::find($id);
        $year = date("Y");
        $data = DB::table('leaves_info')->where('year', $year)->first();
        return view('admin.insert_leave', compact('leave_data','data'));
    }
    public function updateleaves(Request $request)
    {
        $data = Leave::find($request->input('id'));
        $data->title = $request->input('title');
        $data->start_date =  $request->input('start_date');
        $data->save();
        Session::flash('LeaveUpdates', 'Leaved  Successfully Updated');
        return redirect()->route('showLeave');
    }

    public function leavedelete($id)
    {

        $data = Leave::find($id);
        //dd($data);
        $data->delete();
        Session::flash('LeaveDeleted', 'Leaved  Successfully Deleted');
        return redirect()->route('adminLeaveForm');
    }
    public function totalLeave()
    {
        $year = date("Y");
        $data = DB::table('leaves_info')->where('year', $year)->get();
        return view('admin.total_leave', compact('data'));
    }
    public function updatetotalLeaves(Request $request)
    {
        $year = date("Y");
        $planned = $request->input('planned');
        $casual = $request->input('casual');
        $data = array(
            'casual' => $casual,
            'planned' => $planned,
            'year' => $year
        );
        
        $where=array('year'=>$year);
        //$result = DB::table('leaves_info')->where($where)->update($data);
        $result = Leave_info::where($where)->update($data);
        //dd($result);
        if($result)
            {
                notify()->success('Current Year Leave Updated !');
            }
            else
            {
                notify()->error('Something Wrong Try Again..!');
            }

        return redirect()->route('adminLeaveForm');
    }
    public function inserttotalLeave()
    {
        return view('admin.insert_total_leave');
    }
    public function creationtotaLeave(Request $request)
    {
        $year = $request->input('year');
        $planned = $request->input('planned');
        $casual = $request->input('casual');
        $data = array(
            'casual' => $casual,
            'planned' => $planned,
            'year' => $year
        );

        $result = DB::table('leaves_info')->where('year', $year)->count();

        if ($result > 0) {
            Session::flash('ToralLeaveError', 'This Year Leave Alredy Settled');

            return redirect()->route('totalLeave');
        } else {

            $result = DB::table('leaves_info')->insert($data);
            print_r($result);


            Session::flash('ToralLeaveYear', 'Total Leave Successfully Created');
            return redirect()->route('totalLeave');
        }
    }

    /*******************Itenary ***************************************/
    function adminitenary($id)
    {
        $data = DB::table('itinerary')->select('*', 'toures.id as tid', 'itinerary.id as id')->join('toures', 'toures.id', '=', 'itinerary.tour_fk_id')->where('tour_fk_id', $id)->get();
        if ($data->count() > 0) {
            return view('admin.showitenary', compact('data'));
        } else {
            return redirect()->route('itenaryshow');
        }
    }
    function edititi(Request $request)
    {
        $id = $_POST['id'];

        $data = Input::except('_method', '_token');
        if ($_FILES['images']['size'] > 0) {

            $image = $request->file('images');


            $name = time() . '.' . $image->getClientOriginalExtension();

            // $destinationPath = asset('itenary')."/";
            $image->move('./itenary', $name);
            $data['images'] = $name;
        }
        $result = DB::table('itinerary')->where('id', $id)->update($data);

        return back();
    }
    function updateitinery($id)
    {
        $data = DB::table('itinerary')->select('itinerary.*', 'toures.id as tid', 'toures.name as name')->join('toures', 'toures.id', '=', 'itinerary.tour_fk_id')
            ->where('itinerary.id', $id)->get();
        $toures = DB::table('toures')->get();
        return view('admin.updateitinery', compact('data', 'toures'));
    }
    function itineryelete($id)
    {
        $data = DB::table('itinerary')->where('id', $id)->delete();

        return back();
    }
	
    public function Forgotpass()
    {
    	return view('front.forgotpassword');
    }

    public function ForgotPassword(Request $request)
    {
        /*$host =  $_SERVER['HTTP_HOST'];
        $twopar = 'resetpassword';*/
        $email = Crypt::encrypt($request->email);
        $type = $request->type;

        $url = url('/resetpassword/'.$email.'/'.$type);

        $emailss = trim($request->email);
        
        $body = view('mail.forgotpass',compact('url'))->render();
       
        $emp = AdminHelper::sendEmailTo($body,'Forgot Passowrd',$emailss);

        if($emp){
            notify()->success('Forgot password link sucessfully send on your register Email.');
            return redirect('forgotpass');
        }else{
            notify()->error('Something went wrong please try again.');
            return redirect('forgotpass');
        }
    }

    public function Resetpassword(Request $request,  $email, $type)
    {   
        $type = $type;
        $email = Crypt::decrypt($email);
        return view('front.other.reset-password', compact('email','type'));
    }

    public function Resetpass(Request $request)
    {
        if($request->type == 'member'){
            $data = AddMembership::where('email',$request->email)->first();
            
            if($data)
            {
                $data = array(
                    'password'=>Hash::make($request->new_pass),
                );

                $where = array('email'=>$request->email);
                $isUpdated = AddMembership::where($where)->update($data);
                if($isUpdated){
                    notify()->success('Your password is Successfully reset.');
                    return redirect('login');
                }

            }else{
                notify()->error('Your Old password does not match please try again.');
                return redirect('forgotpass');
            }
        }

        if($request->type == 'employee'){
            $data = User::where('email',$request->email)->first();
            
            if($data)
            {
                $data = array(
                    'password'=>Hash::make($request->new_pass),
                );

                $where = array('email'=>$request->email);
                $isUpdated = User::where($where)->update($data);
                if($isUpdated){
                    notify()->success('Your password is Successfully reset.');
                    return redirect('login');
                }

            }else{
                notify()->error('Your Old password does not match please try again.');
                return redirect('forgotpass');
            }
        }

        if($request->type == 'agent'){

            $data = Agent::where('company_email',$request->email)->first();
            
            if($data)
            {
                $data = array(
                    'password'=>Hash::make($request->new_pass),
                );

                $where = array('company_email'=>$request->email);
                $isUpdated = Agent::where($where)->update($data);
                if($isUpdated){
                    notify()->success('Your password is Successfully reset.');
                    return redirect('login');
                }

            }else{
                notify()->error('Your Old password does not match please try again.');
                return redirect('forgotpass');
            }
            
        }

        if($request->type == 'customer'){
            
            $data = Customer::where('email',$request->email)->first();
            if($data)
            {
                $data = array(
                    'password'=>Hash::make($request->new_pass),
                );

                $where = array('email'=>$request->email);
                $isUpdated = Customer::where($where)->update($data);
                if($isUpdated){
                    notify()->success('Your password is Successfully reset.');
                    return redirect('login');
                }

            }else{
                notify()->error('Your Old password does not match please try again.');
                return redirect('forgotpass');
            }
        }
    }
   
}
