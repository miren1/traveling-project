if($leavetype == 'CL')
      {
        if($count > $cl)
        {
          $diff = $count - $cl;
           $lastdate =  date('Y-m-d', strtotime($sdate. $diff.' days'));
           $totalcountdays =  round(abs(strtotime($sdate) - strtotime($lastdate))/86400);
           $data1 = array(
            'employee_fk_id'=>$id,
            'reason'=>$reason,
            'leave_date_start'=>$sdate,
            'leave_date_end'=>$lastdate,
            'leave_photos'=>$image,
            'leave_mode'=>$leavemode,
            'leave_type'=>$leavetype,
            'total_days'=>$totalcountdays
          );
          $ans = DB::table('leaves')->insert($data1);
          $totalcountdays =  round(abs(strtotime($lastdate) - strtotime($edate))/86400);

          $data2 = array(
            'employee_fk_id'=>$id,
            'reason'=>$reason,
            'leave_date_start'=>$lastdate,
            'leave_date_end'=>$edate,
            'leave_photos'=>$image,
            'leave_mode'=>$leavemode,
            'ispaid'=>'yes',
            'leave_type'=>$leavetype,
            'total_days'=>$totalcountdays
          );
          $ans = DB::table('leaves')->insert($data2);
        }
      }
      else if($leavetype == 'PL')
      {
        if($count > $pl)
        {
          $diff = $count - $pl;
           $lastdate =  date('Y-m-d', strtotime($sdate. $diff.' days'));
           $totalcountdays =  round(abs(strtotime($sdate) - strtotime($lastdate))/86400);
            $data1 = array(
            'employee_fk_id'=>$id,
            'reason'=>$reason,
            'leave_date_start'=>$sdate,
            'leave_date_end'=>$lastdate,
            'leave_photos'=>$image,
            'leave_mode'=>$leavemode,
            'leave_type'=>$leavetype,
            'total_days'=>$totalcountdays
          );
          $ans = DB::table('leaves')->insert($data1);
          $totalcountdays =  round(abs(strtotime($lastdate) - strtotime($edate))/86400);
          $data2 = array(
            'employee_fk_id'=>$id,
            'reason'=>$reason,
            'leave_date_start'=>$lastdate,
            'leave_date_end'=>$edate,
            'leave_photos'=>$image,
            'leave_mode'=>$leavemode,
            'ispaid'=>'yes',
            'leave_type'=>$leavetype,
            'total_days'=>$totalcountdays
          );
          $ans = DB::table('leaves')->insert($data2);
        }
      }
