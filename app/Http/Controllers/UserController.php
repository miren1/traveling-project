<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Mail;
use Illuminate\Http\Request;

class UserController extends Controller
{
    
    public function index()
    {
        $tourpackage  = DB::table('packag_types')->where(['deleted_at'=>1])->get();
        return view('user.index',compact('tourpackage'));
    }
    public function indexPage()
    {
        $tourpackage  = DB::table('packag_types')->where(['deleted_at'=>1])->get();
        return view('user.indexPage',compact('tourpackage'));
    }
    public function Packagedetail($id)
    {
        $tourpackage  = DB::table('packag_types')->where(['deleted_at'=>1,'id'=>$id])->get();
            $tourPlanDetails = DB::table('toures')->where('package_type','=',$id)->get();

       // print_r($tourpackage);
       // die;
        return view('user.packagedetail',compact('tourpackage','tourPlanDetails'));
    }
    
    public function memberplans()
    {
        $plans = DB::table('plans')->get();

        return view('user.memberplans',compact('plans'));
    }
    public function inquiry()
    {
        return view('user.inquiry');
    }
	public function sendmailuser()
	{
    	$email = $_POST['email'];
    	
    	 $data = array('email'=>$email);
   
      Mail::send(['text'=>'mail'], $data, function($message) {
         $message->to('hr@jagjoyu.com', 'Jagjoyu User Inquiry')->subject
            ('Jagjoyu User Inquiry');
         $message->from('jagjoyu12@gmail.com','Jagjoyu');
      });
     return back();
    
	}
    public function customerRegistration(Request $request)
    {
        $data = Input::except('_method', '_token');
        $result = DB::table('customers')->insert($data);
        // Session::flash('customerCreation', 'Customer Successfully Created ');
        return back();
    }
    public function inquiryinsert(REQUEST $request)
    {
        // $this->validate($request, [
        //     'first_name' => 'required',
        //     'last_name' => 'required',
        //     'mobile_no' => 'required',
        //     'whatsup_no' => 'required',
        //     'appointment_date' => 'required',

        // ]);

        $data = Input::except('_method', '_token');
        $data['appointment_date']=date('Y-m-d h:i:s', strtotime($data['appointment_date']));
        $ans = DB::table('inquiry')->insert($data);
        // Session::flash('inquiryAdd', 'Inquiry Sucessfully Added');
        return redirect()->route('inquiry');
       
    }
    public function gettourspecificdata(Request $request)
    {
    
        $id = $request->postid; 
        $details = DB::table('toures')->where('id',$id)->get();
        $photos = DB::table('media_tours')->where('tour_fk_id',$id)->get();
        $itinerary = Db::table('itinerary')->where('tour_fk_id',$id)->get();
        $videos = DB::table('videos')->where('fk_tour',$id)->get();
        $tourPlanDetails = DB::table('toures')->where('package_type','=',$details[0]->package_type)->get();
        return view('user.getdata',compact('details','photos','itinerary','videos','tourPlanDetails'));
    }
    public function demoalert()
    {
        echo "hft";
        Alert::message('Robots are working!');



        // SweetAlert::message('Robots are working!');


    }
}
