<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Callstatus;
use App\User;


class CallstatusController extends Controller
{
   public function index()
   {
        return view('admin.callstatus.index');
   }

   public function list()
    {
        $user_id=Auth::user()->id;
        $Assignlead=Callstatus::where(['user_id'=>$user_id,'is_delete'=>'0']);
        
        return datatables()->of($Assignlead)->toJson();   
   }

   public function store(Request $request)
   {
    
    $dataArray= array();
    $dataArray['user_id']=Auth::user()->id;

    if($request->name){
        $dataArray['name']=$request->name;
    }

    if($request->id){


        $save=Callstatus::where('id',$request->id)->update($dataArray);

        if ($save) {
            $json['message'] = "Call Status Updated Successfully";
            $json['status'] = "success";
        } else {
                $json['message'] = "Call Status Not Updated Successfully";
                $json['status'] = "failed";
        }

    }else{

        $save=Callstatus::create($dataArray);


        if ($save) {
            $json['message'] = "Call Status Created Successfully";
            $json['status'] = "success";
        } else {
                $json['message'] = "Call Status Not Created Successfully";
                $json['status'] = "failed";
        }

    }

    return json_encode($json);


   }


   public function checkName(Request $request ,$id = null){    
    
    $where = array('name'=>$request->name,'is_delete'=>'0');
    
    if($request->id){
        $where = array(
            array('name',$request->name),
            array('id','!=',intval(base64_decode($request->id))),
            array('is_delete','0')
        );
    }
   // dd($where,$request->all());
    $arrData = Callstatus::where($where)->get();

    if(count($arrData) != "0"){
        return response()->json(array('valid'=>false));
    }
    return response()->json(array('valid'=>true));
 }


 public function delete(Request $request)
 {
    $delete=Callstatus::where('id',base64_decode($request->id))->update(['is_delete'=>'1']);
    if($delete)
    {
            
             $data['status'] = "success";
             $data['message'] = "Call Status is deleted successfully!";
         }else{
             $data['status'] = "error";
             $data['message'] = "Call Status is not deleted.";
         }
 
         return response()->json($data);
}
    

}
