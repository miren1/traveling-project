<?php

namespace App\Http\Controllers;

use Auth;
use App\Plans;
use App\Agent;
use App\Customer;
use App\Membership;
use App\Planrequest;
use App\AddMembership;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;

class CreatememberController extends Controller
{
    public function create(Request $request,$id=null)
    {

        $id = Crypt::decrypt($id); 

        $plan = AddMembership::find($id);
        $plan_details = Plans::find($plan->plan_id);

        if($plan){
            $user = Agent::find($plan->user_id);
        }else if($plan){
            $user = Customer::find($plan->user_id);
        }
        //dd($plan->status)
        if($plan->status == 'invite'){

            return view('employee.member.create',compact('plan','plan_details','user'));

        }else if($plan->status == 'member'){
            
            $member_request_details = AddMembership::find($id);
            $member_plan = Membership::where('id',$member_request_details->plan_id)->first();

            return view('front.mandate_registration',compact('member_request_details','member_plan'));

        }else{

            notify()->error('Link expired. please contact administration');
            return redirect(route('login'));
        }
    }
}
