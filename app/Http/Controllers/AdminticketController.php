<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ticket_sub_category;
use App\Ticket;
use App\Ticket_category;
use App\Ticketmessage;
use App\Ticket_category_assignment;
use Auth;
use DB;
use Carbon\Carbon;

class AdminticketController extends Controller
{
  
  
  public function index()
  {
    $data['ticket_category'] = Ticket_category::get();
    return view('admin.ticket.index')->with($data);
  }

  public function list(request $request)
  {

    $Ticket = Ticket::with(['user', 'agent', 'customer', 'ticket_categories', 'ticket_sub_categories'])->where('is_delete', '0')->orderBy('created_at', 'desc');
    if (isset($request->category_id)) {
      $Ticket->whereHas('ticket_categories', function ($q) use ($request) {
        $q->where('id', $request->category_id);
      });
    }
    if (isset($request->sub_category_id)) {
      $Ticket->whereHas('ticket_sub_categories', function ($q) use ($request) {
        $q->where('id', $request->sub_category_id);
      });
    }
    if (isset($request->status)) {
      $Ticket->where(['status' => $request->status]);
    }

    if (isset($request->start_date) && isset($request->end_date)) {

      $start  =  Carbon::parse($request->start_date)->format('Y-m-d') . ' 00:00:00';
      $end   = Carbon::parse($request->end_date)->format('Y-m-d') . ' 23:59:59';

      $Ticket->whereBetween('created_at', [$start, $end]);
    }

    if (isset($request->user)) {
      if ($request->user == 'employee') {
        $Ticket->whereHas('user', function ($q) use ($request) {
          $q->where('user_id', '!=', null);
        });
      } else if ($request->user == 'agent') {
        $Ticket->whereHas('agent', function ($q) use ($request) {
          $q->where('agent_id', '!=', null);
        });
      } else if ($request->user == 'customer') {
        $Ticket->whereHas('customer', function ($q) use ($request) {
          $q->where('customer_id', '!=', null);
        });
      } else {
      }
    }



    return datatables()->of($Ticket)->toJson();
  }


  public function getCat(Request $request)
  {
    $stateid = $request->cat_id;



    $where = array( 'ticket_cat_id' => $stateid);

    $cities = Ticket_sub_category::where($where)->get();
    return response()->json([
      'cat' => $cities
    ]);
  }


  public function view($id = '')
  {
    $data['ticket'] = Ticket::with(['user', 'ticket_categories', 'ticket_sub_categories'])->find(base64_decode($id));
    Ticketmessage::where(['ticket_id' => base64_decode($id), 'type' => 'send'])->update(['is_view' => '1']);
    $data['ticket_message_count'] = Ticketmessage::where(['ticket_id' => base64_decode($id), 'type' => 'send'])->count();
    return view('admin.ticket.view')->with($data);
  }


  public function getcount($id = null)
  {
    $data['msg_count'] = Ticketmessage::where(['ticket_id' => base64_decode($id), 'type' => 'send'])->count();
    return  json_encode($data);
  }


  public function send_message(Request $request)
  {
    //dd($request->all(),$request->file('media'));

    if ($request->ticket_id && $request->message) {

      $Ticket = Ticket::find(base64_decode($request->ticket_id));
      $ticket_id = base64_decode($request->ticket_id);
      $ticket_category_id = NULL;
      $ticket_sub_category_id = NULL;

      if ($Ticket->ticket_category_id) {
        $ticket_category_id = $Ticket->ticket_category_id;
      }
      if ($Ticket->ticket_sub_category_id) {
        $ticket_sub_category_id = $Ticket->ticket_sub_category_id;
      }



      $name = NULL;
      $file_type = NULL;
      if ($request->file('media')) {
        $file = $request->media;
        $extension = $file->getClientOriginalName();
        $name = strtotime('now') . '_' . $extension;
        $file_type = $file->getClientOriginalExtension();
        $file->move('public/ticket', $name);
      }

      $sentData = array(
        'user_id' => Auth::user()->id,
        'ticket_id' => $ticket_id,
        'ticket_category_id' => $ticket_category_id,
        'ticket_sub_category_id' => $ticket_sub_category_id,
        'type' => 'receive',
        'message' => $request->message,
        'file' => $name,
        'file_type' => $file_type,

      );

      $send = Ticketmessage::create($sentData);
      if ($send) {
        $status = true;
      } else {
        $status = false;
      }
    } else {
      $status = false;
    }
    return json_encode($status);
  }


  public function get_messages($id = null)
  {
    if ($id) {
      $where = array(
        'ticket_id' => base64_decode($id),
        'is_delete' => '0'
      );
      $data['ticket']  = Ticket::find(base64_decode($id));
      $data['message'] = Ticketmessage::with('user')->where($where)->orderBy('id', 'ASC')->get();
      return  json_encode($data);
    } else {
      $data = array();
      return  json_encode($data);
    }
  }


  public function status_chnage(Request $request)
  {


    $delete = Ticket::find(base64_decode($request->id))->update(['status' => $request->status]);

    // dd($delete,base64_decode($request->id),$request->status);
    if ($delete) {

      if ($request->status == 'close') {
        $status = '1';
      } else {
        $status = '0';
      }

      Ticketmessage::where('ticket_id', base64_decode($request->id))->update(['is_delete' => $status]);
      $data['status'] = "success";
      $data['message'] = "Ticket is " . $request->status . " successfully!";
    } else {
      $data['status'] = "error";
      $data['message'] = "Ticket is not " . $request->status . ".";
    }

    return response()->json($data);
  }



  public function get_receive_msg()
  {

    $user_id = Auth::user()->id;
    $Ticket = Ticket::where(['status' => 'active', 'is_delete' => '0', 'user_id' => $user_id])->orderBy('created_at', 'desc')->get()->toArray();
    $Ticket_category = array_column($Ticket, 'id');
    // dd($Ticket_category);
    $Ticketmessage = Ticketmessage::whereIn('ticket_id', $Ticket_category)
      ->whereRaw('id IN (select MAX(id) FROM ticket_messages where is_delete="0" AND type="send" AND is_view="0" GROUP BY ticket_id)')->where(['is_delete' => '0', 'type' => 'send', 'is_view' => '0']);
    $data['message_count'] = $Ticketmessage->count();
    $data['message'] = $Ticketmessage->limit(6)->get();
    return  json_encode($data);
  }
}
