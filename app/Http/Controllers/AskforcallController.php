<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ticket_category_assignment;
use App\Askforcall;
use App\Callstatus;
use App\Setting;
use Auth;

class AskforcallController extends Controller
{
    public function index()
    {
        $user_id['id'] = Auth::user()->id;
        if(Auth::user()->role=="admin"){
            return view('admin.askforcall.index');
        }
        else{
            return view('employee.askforcall.index',$user_id);
        }
    }

    public function list($id)
    {
        
        $data=Ticket_category_assignment::where('user_id', $id)->get();
        
        foreach($data as $value){
           
            return datatables()->of(Askforcall::where('is_delete','0')->where('cat_id', $value->ticket_cat_id)->where('sub_cat_id', $value->ticket_sub_cat_id)->orderBy('created_at', 'desc'))->toJson();

        }

    }

    public function askforcall_view($id = null)
    {
        
        $data['callstatus']=Callstatus::where('is_delete','0')->get();
        
        //$data['assignlead']= Assignlead::where(['status'=>'active','employee_id'=>Auth::user()->id,'group_lead_id'=>base64_decode($id)])->first();
        $data['askforcall']=Askforcall::where('id',base64_decode($id))->first();
       // dd($data['askforcall']);
        
        return view('employee.leads.askforcall_view')->with($data);
    }

    public function view($id=null)
    {
    	$data['plan']=Askforcall::find(base64_decode($id));

        $setting= Setting::first();
        if(Auth::user()->role=="admin"){
            return view('admin.askforcall.view')->with($data);
        }
        else if(Auth::user()->id==$setting->askforcall_user_id){
            return view('employee.askforcall.view')->with($data);
            //return view('employee.askforcall.index');
        }else{
            return redirect('/empdashboard');
        } 	
    }


    public function delete(Request $request)
    {
		$delete=Askforcall::find(base64_decode($request->id))->update(['is_delete'=>'1']);
      // dd($delete);
		if($delete)
        {
            $data['status'] = "success";
            $data['message'] = "Ask Call is deleted successfully!";
        }
        else
        {
            $data['status'] = "error";
            $data['message'] = "Ask Call is not deleted.";
        }

        return response()->json($data);
    }
}
