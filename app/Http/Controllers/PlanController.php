<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Plans;
use App\Setting;
use AdminHelper;
use App\Planrequest;
use App\AddMembership;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Input;


class PlanController extends Controller
{
    public function index()
    {
        $setting = Setting::first();
       
        if (Auth::user()->role == "admin") {
            return view('admin.plans.index');
        } else if (Auth::user()->role == "user") {

            return view('employee.plans.index');
        } else {
            return redirect('/empdashboard');
        }
    }

    public function list()
    {
        return datatables()->of(Plans::where('is_delete', '0')->orderBy('type', 'asc'))->toJson();
    }

    public function view($id = null)
    {
        $data['plan'] = Plans::find(base64_decode($id));
        $setting = Setting::first();
        if (Auth::user()->role == "admin") {

            return view('admin.plans.view')->with($data);
        } else if (Auth::user()->role == "user") {
            return view('employee.plans.view')->with($data);
        } else {
            return redirect('/empdashboard');
        }
    }

    public function add($id = '')
    {
        $data = array();
        if ($id) {
            $data['plan'] = Plans::find(base64_decode($id));
        }
        return view('admin.plans.add')->with($data);
    }

    public function delete(Request $request)
    {
        $delete = Plans::find(base64_decode($request->id))->update(['is_delete' => '1']);
        if ($delete) {
            $data['status'] = "success";
            $data['message'] = "Plan is deleted successfully!";
        } else {
            $data['status'] = "error";
            $data['message'] = "Plan is not deleted.";
        }
        return response()->json($data);
    }

    public function store(Request $request)
    {
        $datainput = array(
            'user_id' => Auth::user()->id,
            'type' => $request->type,
            'fixed_amt' => $request->fixed_amt,
            'years' => $request->years,
            'role' => $request->role,
            'mebership_fees' => $request->mebership_fees,
            'free_nights_room' => $request->free_nights_room,
            'free_nights_to_avail_in' => $request->free_nights_to_avail_in,
            'free_night_in_any_type_of_hotel' => $request->free_night_in_any_type_of_hotel,
            'monthly_emi' => $request->monthly_emi,
            'quarterly_emi' => $request->quarterly_emi,
            'half_yearly_emi' => $request->half_yearly_emi,
            'annual_emi' => $request->annual_emi,
            'emi_dafulter_charges' => $request->emi_dafulter_charges,
            'flexbility_to_move_free_night_to_next_year' => $request->flexbility_to_move_free_night_to_next_year,
            'easy_upgrade_of_membership_plans' => $request->easy_upgrade_of_membership_plans,
            'referral' => $request->referral,
            'commission_am' => $request->commission_am,
            'commission_first_payment' => $request->commission_first_payment,
            'commission_second_payment' => $request->commission_second_payment,
            'agent_target' => $request->agent_target,
            'commission_employee' => $request->commission_employee,
            'commission_payment' => $request->commission_payment
        );

        if ($request->id) {

            $result = Plans::where('id', $request->id)->update($datainput);

            if ($result) {
                notify()->success('Membership Plan Successfully Updated !!');
            } else {
                notify()->error('Membership Plan not Updated Successfully!');
            }
        } else {

            $result = Plans::create($datainput);

            if ($result) {
                notify()->success('Membership Plan Successfully Added !!');
            } else {
                notify()->error('Membership Plan not Added Successfully!');
            }
        }

        return redirect('member-plans');
    }

    public function memberIndex($id)
    {
        $id = base64_decode($id);
        return view('member/add-member', compact('id'));
    }

    public function MemberAdd(Request $request)
    {
        ///dd($request->all());
        return "done";
    }


    public function add_member($id = null)
    {

        $id = base64_decode($id);
        $plan = Plans::find($id);
        if (Auth::user()->role == "admin") {

            return view('admin.plans.addmembers', compact('id', 'plan'));
        } else if (Auth::user()->role == "user") {


            return view('employee.plans.addmembers', compact('id', 'plan'));
        } else {
            return redirect('/empdashboard');
        }
    }

    public function member_req()
    {
        if (Auth::user()->role == "admin") {

            return view('admin.plans.membershow');
        } else if (Auth::user()->role == "user") {

            return view('employee.plans.membershow');
        } else {
            return redirect('/empdashboard');
        }
    }
    public function list_member_req()
    {
        // dd(Planrequest::where(['user_id' => Auth::user()->id])->orderBy('id', 'DESC')->get());
        return datatables()->of(AddMembership::where(['user_id' => Auth::user()->id])->orderBy('id', 'DESC'))->toJson();
    }

    public function store_member_req(Request $request)
    {
        //dd($request->all());
        if(Auth::guard('webagent')->check()){
            $user_id = Auth::guard('webagent')->user()->id;
        }else if(Auth::guard('webcustomer')->check()){
            $user_id = Auth::guard('webcustomer')->user()->id;
        }else if(Auth::guard('webmember')->check()){
            $user_id = Auth::guard('webmember')->user()->id;
        }else{
            $user_id = Auth::id();
        }

        $planrequest = new AddMembership();
        $planrequest->plan_id = $request->id;
        $planrequest->name = $request->first_name.' '.$request->last_name;
        $planrequest->primary_contact = $request->mobile_number;
        $planrequest->whatsapp_number = $request->whats_up;
        $planrequest->email = $request->email;
        $planrequest->status = 'invite';

        if(Auth::guard('webagent')->check()){
            $planrequest->agent_id = Auth::guard('webagent')->user()->id;
        }else if(Auth::guard('webcustomer')->check()){
            $planrequest->customer_id = Auth::guard('webcustomer')->user()->id;
        }else if(Auth::guard('webmember')->check()){
            $planrequest->member_id = Auth::guard('webmember')->user()->id;
        }else{
            $planrequest->user_id = $user_id;
        }

        $save = $planrequest->save();
        if ($save) {

            $id = Crypt::encrypt($planrequest->id);
            
            $link_creation = url('/createmember/'.$id);
            
            $emailss = trim($request->email);
            $data = [
                'link_creation' => $link_creation
            ];
            $arr = $emailss;
            $body = view('mail.memberadd', compact('link_creation'))->render();

            AdminHelper::sendEmailTo($body, 'Invite For Membership', $arr);

            if($request->emp_invite == 'emp_invite')
            {
                $id=base64_encode($request->id);
                notify()->success('Member invited Successfully!!');
                return redirect('/member-plans/add_member/'.$id);  
            }else{
                if(Auth::guard('webagent')->check() || Auth::guard('webcustomer')->check() || Auth::guard('webmember')->check()){
                    return redirect('/become-member/'.base64_encode($request->id));
                }else{
                    return redirect('/member-plans/invited_members');  
                }
            }

            notify()->success('Member invited Successfully!!');
        }else{
            return redirect('/member-plans/invited_members');  
            
            notify()->error('Membership Plan invite  not send Successfully!');
        }
        
        
    }

    public function membershiplist()
    {
        return view('admin.membershipList');
    }

    public function member_list()
    {
        return datatables()->of(AddMembership::orderBy('created_at', 'desc'))->toJson();
    }
}
