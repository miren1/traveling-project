<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use DB;
use App\Ticket_category;
use App\Ticket_sub_category;
use App\Ticket_category_assignment;
use Auth;

class Interactive_categoryController extends Controller
{
	public function index()
    {
        return view('admin.category.index');
    }

    public function sub_category()
    {	
        return view('admin.category.sub_category');
    }

    public function category_assignment()
    {   
        return view('admin.category.category_assignment');
    }

    public function create()
    {
        return view('admin.category.add_category');
    }

    public function create_sub_category()
    {	
    	$result['ticket_categoies']=Ticket_category::all();
    	//dd($result);
        return view('admin.category.add_sub_category',$result);
        
        //return view('admin.category.add_sub_category',compact('category'));
    }

    public function create_category_assignment()
    {   
        $result['ticket_categoies']=Ticket_category::all();
        $result['ticket_sub_category']=Ticket_sub_category::all();
        $result['user']=User::where('role','user')->where('employee_approved','1')->whereNull('leave_date')->get();
        return view('admin.category.create_assignment',$result);
       
    }

    public function store(Request $request)
    {
        //dd($request->all());
        $data = array(
                'name'=>$request->ticket_category_name,
                'dec'=>$request->description,
            );
        
        if($request->id)
        {
            $where = array('id'=>$request->id);
            $isUpdated = Ticket_category::where($where)->update($data);
            notify()->success('Ticket Category Updated !');
        }
        else
        {
            $category = Ticket_category::create($data);
            if($category)
            {
                notify()->success('Ticket Category Added !');
            }
            else
            {
                notify()->error('Something Wrong Try Again..!');
            }
            return redirect('admin-category');
        }
        return redirect('admin-category');
    }

    public function sub_cat_store(Request $request)
    {
       //dd($request->all());
        if($request->id)
        {
            
           
            //dd($ticket_sub_category);
            $random_keys=array_rand($request->ticket_sub_category_name,1);

            $sub_category = $request->ticket_sub_category_name[$random_keys];

            $employee='0';
            $agent='0';
            $member='0';
            $user='0';
            if($request->employee){
                $employee='1';
            }
            if($request->agent){
                $agent='1';
            }
            if($request->member){
                $member='1';
            }
            if($request->user){
                $user='1';
            }

            $data = array(
                'ticket_cat_id'=>$request->category_id,
                'name'=>$sub_category,
                'dec'=>$request->description,
                'employee'=>$employee,
                'agent'=>$agent,
                'member'=>$member,
                'user'=>$user,
            );
            $where = array('id'=>$request->id);

            $isUpdated = Ticket_sub_category::where($where)->update($data);
            notify()->success('Ticket Sub Category Updated !');

        }else{

            $sub_cat_name = $request->ticket_sub_category_name;
            
            for ($i = 0; $i < count($sub_cat_name); $i++)
            {
            
                $employee='0';
                $agent='0';
                $member='0';
                $user='0';
                if($request->employee){
                    $employee='1';
                }
                if($request->agent){
                    $agent='1';
                }
                if($request->member){
                    $member='1';
                }
                if($request->user){
                    $user='1';
                }    
            $data[] = array(
                    'ticket_cat_id'=>$request->category_id,
                    'name'=>$sub_cat_name[$i],
                    'dec'=>$request->description,
                    'employee'=>$employee,
                    'agent'=>$agent,
                    'member'=>$member,
                    'user'=>$user
                );
            
            }
           // dd($data);
            $insert_data = collect($data);

            $chunks = $insert_data->chunk(500);
            
            foreach ($chunks as $chunk){
                //dd($chunk);
                $res = DB::table('ticket_sub_categories')->insert($chunk->toArray());

                if($res)
                {
                    notify()->success('Ticket Sub Category Added !');
                }
                else
                {
                    notify()->error('Something Wrong Try Again..!');
                }

            }

        }
        
        return redirect('admin-category/sub_category');
        
        
    }

    public function store_category_assignment(Request $request)
    {
       //dd($request->all());
        if($request->sub_category_id){
            $data = array(
                    'user_id'=>$request->employee_id,
                    'ticket_cat_id'=>$request->category_id,
                    'ticket_sub_cat_id'=>$request->sub_category_id,
                    'admin_id'=>Auth::user()->id,
                );
            //dd($data);
            if($request->id)
            {
                $where = array('id'=>$request->id);
                $isUpdated = Ticket_category_assignment::where($where)->update($data);
                notify()->success('Ticket Category Assignment updated !');
            }
            else
            {
            
                $category = Ticket_category_assignment::create($data);
               
                if($category)
                {
                    notify()->success('Ticket Category Assignment Successfully !');
                }
                else
                {
                    notify()->error('Something Wrong Try Again..!');
                }
                return redirect('admin-category/category_assignment');
            }

        }else{
           //dd($request->all());
            if($request->category_id != null){
               
            $employee_id = $request->employee_id;
            $ticket_cat_id = $request->category_id;
            
            $where = array('user_id'=>$request->employee_id);
            $where1 = array('ticket_cat_id'=>$request->category_id);

            $result=Ticket_category_assignment::where($where)->where($where1)->get();

            if(count($result) >= 1){

                for ($i = 0; $i < count($result); $i++){
                    $ticket_sub_cat_id[] = $result[$i]['ticket_sub_cat_id'];
                }

                $sub_cat_id=Ticket_sub_category::where('ticket_cat_id',$request->category_id)->whereNotIn('id',$ticket_sub_cat_id)->get();

                if(count($sub_cat_id) >= 1){

                    for ($i = 0; $i < count($sub_cat_id); $i++)    
                    {
                        $data[] = array(
                            'user_id'=>$request->employee_id,
                            'ticket_cat_id'=>$request->category_id,
                            'ticket_sub_cat_id'=>$sub_cat_id[$i]['id'],
                            'admin_id'=>Auth::user()->id,
                            ); 
                    }

                }else{

                    notify()->error('Ticket Category Already Assign Please Select Another !');

                    return redirect('admin-category/create_category_assignment');
                }
               
            }else{
                
                $sub_cat_id=Ticket_sub_category::where('ticket_cat_id',$request->category_id)->get();
                
                if(count($sub_cat_id) >= 1){
                    for ($i = 0; $i < count($sub_cat_id); $i++)    
                    {
                        $data[] = array(
                            'user_id'=>$request->employee_id,
                            'ticket_cat_id'=>$request->category_id,
                            'ticket_sub_cat_id'=>$sub_cat_id[$i]['id'],
                            'admin_id'=>Auth::user()->id,
                            ); 
                    }
                }else{
                    notify()->error('Sorry, We not found any sub category..!');
                    return redirect('admin-category/create_category_assignment');
                }
            }
               // dd($data);
                $insert_data = collect($data);

                $chunks = $insert_data->chunk(500);
                
                foreach ($chunks as $chunk){

                    $res = DB::table('ticket_category_assignments')->insert($chunk->toArray());

                    if($res)
                    {
                        notify()->success('Ticket Category Assignment Successfully !');
                    }
                    else
                    {
                        notify()->error('Something Wrong Try Again..!');

                    }

                }


            }else{
                notify()->error('Please select Category.!');
                return redirect('admin-category/create_category_assignment');
            }


        }

        return redirect('admin-category/category_assignment');
    }

    public function getAll()
    {
        if (Auth::user()->role =='admin') {

            return datatables()->of(Ticket_category::all())->toJson();

        }else{

         return datatables()->of(Ticket_category::where('user_id',Auth::user()->id))->toJson();
        }
    }

    public function getAll_sub_cat()
    {
        if (Auth::user()->role =='admin') {

            return datatables()->of(Ticket_sub_category::with('ticket_categories'))->toJson();

        }
    }

    public function get_assign_data()
    {
        if (Auth::user()->role =='admin') {

            return datatables()->of(Ticket_category_assignment::with(['ticket_categories','ticket_sub_categories','users'])->get())->toJson();

            //return datatables()->of(Ticket_sub_category::with('ticket_categories')->get())->toJson();

        }
    }

    public function edit($id)
    {
        $id = base64_decode($id);
        $category = Ticket_category::find($id);

        return view('admin.category.add_category',compact('category'));
    }

    public function edit_sub_category($id)
    {
        $id = base64_decode($id);
        $result['ticket_categoies']=Ticket_category::all();

        $result['ticket_sub_category'] = Ticket_sub_category::with('ticket_categories')->find($id);
        //dd($sub_category);
        return view('admin.category.add_sub_category',$result);   
    }

    public function edit_category_assign($id)
    {   
        $id = base64_decode($id);
        $where = array('id'=>$id);
        $result['ticket_assign']=Ticket_category_assignment::where($where)->first();
        //dd($result['ticket_assign']->user_id);
        $where = array('id'=>$result['ticket_assign']->user_id);
        //$result['user'] = User::where($where)->get();

        $user_where = array('employee_approved'=>'1');
        $result['user'] = User::where($user_where)->get();
        //dd($result['ticket_assign']->ticket_cat_id);
        
        //$where = array('id'=>$result['ticket_assign']->ticket_cat_id);
        $result['ticket_categoies'] = Ticket_category::get();

        $where = array('ticket_cat_id'=>$result['ticket_assign']->ticket_cat_id);
        $result['ticket_sub_category'] = Ticket_sub_category::where($where)->get();
        //dd($result['ticket_sub_category']);
        //dd($result);
        return view('admin.category.create_assignment',$result);   
    }

    public function destroy($id)
    {
    	//dd($id);
        $deleted = Ticket_category::destroy($id);

            if ($deleted) {
                $status = true;
            }else{
                $status = false;
            }
            return response()->json($status);
    }

    public function destroy_sub_category($id)
    {
        //dd($id);
        $deleted = Ticket_sub_category::destroy($id);

            if ($deleted) {
                $status = true;
            }else{
                $status = false;
            }
            return response()->json($status);
    }

    public function destroy_assign_category($id)
    {
        //dd($id);
        $deleted = Ticket_category_assignment::destroy($id);

            if ($deleted) {
                $status = true;
            }else{
                $status = false;
            }
            return response()->json($status);
    }

    public function checkcategory($name, $id)
    { 
        if($id == null){
            
            $data = Ticket_category::where('name',$name)->get(); 

        }else{
        
            $id = array($id);
            $data = Ticket_category::whereNotIn('id', $id)->where('name',$name)->get();  
            //dd($data);
        }
        
        return response()->json($data);
    }

    public function checkSubcategory($name,$id)
    {
        
        $data = Ticket_sub_category::where('ticket_cat_id',$id)->where('name',$name)->get();
        return response()->json($data);
    }

    public function check_on_Subcategory($name,$id)
    {
        //dd($name);
        $data = Ticket_sub_category::where('ticket_cat_id',$id)->where('name',$name)->get();
        
        return response()->json($data);
    }

    public function get_Category($id)
    {   
        $data = Ticket_category_assignment::where('user_id',$id)->get();

       /* if(count($data) >= 1){
            foreach($data as $row){
                $cat_id[] = $row->ticket_cat_id;
            }
            $cat_id = array_unique($cat_id);
            
            $cat_data = Ticket_category::whereNotIn('id',$cat_id)->get();

        }else{*/
            
            $cat_data = Ticket_category::all();    
        //}
        return response()->json($cat_data);
    }

    public function get_subCategory($id,$emp_id)
    {   
       
        $data = Ticket_category_assignment::where('user_id',$emp_id)->get();
        
        if(count($data) >= 1){
           
            foreach($data as $row){
                $sub_cat_id[] = $row->ticket_sub_cat_id;
                $cat_id[] = $row->ticket_cat_id;
            }
            $data = Ticket_sub_category::where('ticket_cat_id',$id)->whereNotIn('id',$sub_cat_id)->get();
        }else{
            
            $data = Ticket_sub_category::where('ticket_cat_id',$id)->get();
        }
    
        return response()->json($data);   
    }   
}
?>