<?php
namespace App\Http\Controllers;


use Illuminate\Routing\Controller as BaseController;

use Mail;
use Auth;
use Charts;
use Session;
use App\User;
use App\Toure;
use App\Itenary;
use App\Message;
use AdminHelper;
use Pusher\Pusher;
use App\LeaveReport;
use App\Mandate_registration;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class CommonController extends BaseController
{
  use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
	
  public function photogallery(Request $request,$id)
  {
    
    $id = base64_decode($id);
    $result= DB::table('media_tours')->where('tour_fk_id',$id)->count();
    if($result>0)
    {
      $result= DB::table('media_tours')->where('tour_fk_id',$id)->get();

      if($request->session()->get('role') == 'admin'){
        return view('admin.photogallery',compact('result')); 
      }else{
        return view('employee.photogallery',compact('result')); 
      }
    }else{
      $id = base64_encode($id);
      return redirect('/amintourinsertphoto/'.$id);
    }
  }
/*
  public function M_t_verification()
  {
      $id = '87';
      
      $data = Mandate_registration::find($id);
     // dd($data);

      $mandate_verifiy = AdminHelper::mandate_verification('T658345','1627642703226','c200024');
      dd($mandate_verifiy);
  }*/

  function insertsalary()
  {   
    $user = DB::table('users')->where('role','user')->where('leave_date',NULL)->get();
    return view('admin.insertsalary',compact('user'));
  }

  public function createsalary()
  {
    $data = array(
    	'salary'=>$_POST['salary'],
    	'fk_emp_id'=>$_POST['fk_emp_id'],
    	'follow_date'=>$_POST['follow_date'],
    );
    $result= DB::table('salary')->insert($data);

    Session::flash('salarycreate','Salary Created');
    return redirect()->route('showsalary');
  }

public function insertadminvideo(Request $request,$id)
{
          $id = $id;
          $caption = $_POST['caption'];
          $link = $_POST['link'];
          $data = array(
            'caption'=>$caption,
            'link'=>$link,
            'fk_tour'=>$id
          );
          DB::table('videos')->insert($data);
          return redirect('/amintourinsertvideo/'.$id);
}

public function updatetour($id)
{
  // $result = DB::table('toures')->join('itinerary','itinerary.tour_fk_id','=','toures.id')->where('toures.id',$id)->get();
  $result = DB::table('toures')->where('id',$id)->get();
  $itenary = DB::table('itinerary')->where('tour_fk_id',$result[0]->id)->orderBy('day_number')->get();
  return view('admin.updateTour',compact('result','itenary'));
}

public function tourdelete($id)
{
    $r = DB::table('toures')->where('id',base64_decode($id))->delete();
    Session::flash('TourDelete', 'Tour Deleted Successfully');
    return back();
} 

public function salarygenerate()
{
// $currentmonth = date('m');
// $currentyear = date('Y');
return view('admin.salarygenerate');
}

public function cashpaymentfinal()
{
if(isset($_POST['mdate']))
 {	$mdate = $_POST['mdate'];
	$id = $_POST['payid'];
	$data = array(
    'pay_done'=>'yes',
    'is_payment_done'=>'yes',
    'cashremarkdone'=>$_POST['cashremarkdone']
    );
}

$res = DB::table('salary_detail')->where('id',$id)->update($data);

return redirect()->route('allsalarygenerate');
}

public function editsalaryslip()
{
	$id = $_POST['id'];
    $data = Input::except('_method', '_token');
    
    $salarydata = DB::table('salary_detail')->where('id',$_POST['salaryid'])->get();
    $month_year = $salarydata[0]->month_year;
    $arr = explode("-",$month_year);
     $month = $arr[1];
    $year = $arr[0];
    $salary =  $salary = DB::table('salary')->where('fk_emp_id',$id)->orderBy('follow_date','desc')->limit(1)->whereYear('follow_date',"<=",$arr[0])->get();
	$monthsalary = $salary[0]->salary/12;
    $totalday =cal_days_in_month(CAL_GREGORIAN,$month,$year);
    $data = array(
     'pf' => $_POST['pf'],
    'other_charges' => $_POST['other_charges'],
    'otherallowance' => $_POST['otherallowance'],
    'basic_salary' => $_POST['basic_salary'],
    'house_rent'=>$_POST['houserent'],
    'grratuti' => $_POST['grratuti'],
    'total_leave_days' => $_POST['total_leave_days'],
	'esic' => $_POST['esic'],
    'emp_esic' => $_POST['emp_esic'],
    'tax' => $_POST['tax'],
    'remark' => $_POST['remark'],
    
    
    );
   
   
    $salaryid = $_POST['salaryid'];
    
    
    $tmp1 = $data['esic']+$data['basic_salary'] + $data['pf']+$data['otherallowance']+ $data['house_rent'];
 	$tmpb = $data['emp_esic']+$data['grratuti'] + $data['pf']+$data['tax'];
 	$temp = $monthsalary/$totalday;
 	$data['salary_leave_deducation'] = $temp  * $_POST['total_leave_days'];
 
 	$netSalary = $tmp1 + $tmpb - $data['salary_leave_deducation'];
 
 	 $data['total_amount'] = $netSalary;
    $data['amount'] = $netSalary;
    
     $result = Db::table('salary_detail')->where('id',$salaryid)->update($data);
   
    return back();
    
}

public function allsalarygenerate($monthsalary = null,Request $request)
{
	$currentmonth = date('m');
	$currentyear = date('Y');
	
 if(isset($_POST['monthsalary']))
 {
 $monthsalary = $_POST['monthsalary'];
 $startData  = explode('-',$monthsalary);
 $startYear = $startData[1];
 $startMonth = $startData[0];
 	if($currentyear < $startYear ) 
    {
    	Session::flash('salarygenerate', 'You Can Not Generate Salary For Future Year ');

		return view('admin.salarygenerate');
    		
    }
 	if($currentyear == $startYear AND $currentmonth<=$startMonth)
    {
    	Session::flash('salarygenerate', 'You Can Generate Salary After Month is Over ');

		return view('admin.salarygenerate');
    }
$monthsalary = $startYear."-".$startMonth."-"."1";
 $request->session()->put('monthsalary',$monthsalary);
 }
else
{
$monthsalary = $request->session()->get('monthsalary');
$startData  = explode('-',$monthsalary);
 $startYear = $startData[0];
 $startMonth = $startData[1];}

 $users = DB::table('users')->where('leave_date','=',NULL)
			->where('role','=','user')->where('employee_approved','1')
		  	->where([[DB::raw("year(joining_date)"), '=', $startYear],[DB::raw("month(joining_date)"), '<=',$startMonth]])
			->orWhere(DB::raw("year(joining_date)"), '<', $startYear)
 			->get();
 if($users->count() > 0 ) {
 foreach ($users as $value) {
$id = $value->id;
$check = DB::table('salary_detail')->where('emp_fk',$id)->whereMonth('month_year',$startMonth)->whereYear('month_year',$startYear)->get();
 
if($check->count() > 0)
{
			
}
else{
$salary =  $salary = DB::table('salary')->where('fk_emp_id',$id)->orderBy('follow_date','desc')->limit(1)->whereYear('follow_date',"<=",$startYear)->get();
$totalLeaveDays = Db::table('leaves')->where('employee_fk_id',$id)->where(DB::raw("(DATE_FORMAT(leave_date_start,'%Y'))"),$startYear)->where(DB::raw("(DATE_FORMAT(leave_date_start,'%m'))"),$startMonth)->where('is_paid','yes')->sum('total_days');
$total_days = cal_days_in_month(CAL_GREGORIAN,$startMonth,$startYear);
$totalWorkingDay =  $total_days - $totalLeaveDays;
$salarycalCulation['Tax'] = 200;
$salarycalCulation['Monthlysalary'] = ($salary[0]->salary/12);
$salarycalCulation['Basicsalary'] = ($salarycalCulation['Monthlysalary']-$salarycalCulation['Tax'])/2;
$salarycalCulation['HouseRent'] = $salarycalCulation['Basicsalary']/2;
if($salarycalCulation['Basicsalary'] <=15100)
{
	$salarycalCulation['Pf'] = 0;
}
else
{
$salarycalCulation['Pf'] = $salarycalCulation['Basicsalary']*12/100;
}
if($salarycalCulation['Monthlysalary'] <=25000)
{
$salarycalCulation['esic'] = ($salarycalCulation['Monthlysalary'] * 0.75 /100);
$salarycalCulation['emp_esic'] = ($salarycalCulation['Monthlysalary'] * 3.25 /100);

}
else 
{
$salarycalCulation['esic'] = 0;
$salarycalCulation['emp_esic'] = 0;
}

$salarycalCulation['Grratuti'] = ($salary[0]->salary*2.34825/100)/12;
 $salarycalCulation['Otherallowance'] = $salarycalCulation['Monthlysalary']- $salarycalCulation['Tax'] - $salarycalCulation['Pf'] - $salarycalCulation['Grratuti']-$salarycalCulation['HouseRent']-$salarycalCulation['Basicsalary']-$salarycalCulation['Pf']; 
$salarycalCulation['TotalFixedCashNoncompasation'] = $salarycalCulation['Grratuti'] + $salarycalCulation['Pf']+$salarycalCulation['Otherallowance'];
 $salarycalCulation['a'] = $salarycalCulation['esic']+$salarycalCulation['Basicsalary'] + $salarycalCulation['Pf']+$salarycalCulation['Otherallowance']+ $salarycalCulation['HouseRent'];
 $salarycalCulation['b'] = $salarycalCulation['emp_esic']+$salarycalCulation['Grratuti'] + $salarycalCulation['Pf']+$salarycalCulation['Tax'];
 $temp = $salarycalCulation['Monthlysalary']/$total_days;
 $salarycalCulation['deduction'] = $temp  * $totalLeaveDays;
 
 $netSalary = $salarycalCulation['a'] + $salarycalCulation['b'] - $salarycalCulation['deduction'];
$data = array(
  'emp_fk'=>$id,
  'month_year'=>$monthsalary,
  'basic_salary'=>$salarycalCulation['Basicsalary'],
  'house_rent'=>$salarycalCulation['HouseRent'],
  'pf'=>$salarycalCulation['Pf'],
  'grratuti'=>$salarycalCulation['Grratuti'],
  'otherallowance'=>$salarycalCulation['Otherallowance'],
  'total_leave_days'=>$totalLeaveDays,
  'tax'=>$salarycalCulation['Tax'],
  'amount'=>$netSalary,
  'total_amount'=>$netSalary,
	'workingdays'=>$totalWorkingDay,
	'salary_leave_deducation'=>$salarycalCulation['deduction'],
	'emp_esic'=>$salarycalCulation['emp_esic'],
    'esic'=>$salarycalCulation['esic'],

 );
$res = DB::table('salary_detail')->where('emp_fk',$id)->where('month_year',$monthsalary)->insert($data);
}			//Else Close
} // Foreache closed
} // User malya anu if close....
else 
{
	echo "No Users Payment For this Month... ";
	Session::flash('userPayment', 'No Salary Payment For This Month.');
	return back();
 
}

$usersal = Db::table('salary_detail')->select('*','users.name as username','salary_detail.id as salaryid','users.id as id')->join('users','users.id','=','salary_detail.emp_fk')->whereMonth('month_year',$startMonth)->whereYear('month_year',$startYear)->get();

return view('admin.allgenerate',compact('usersal','monthsalary'));
}
public function cashpayment()
{ 

  $amount = $_POST['amount'];
  if(isset($_POST['other_charges']))
  {
    $other_charges = $_POST['other_charges'];

  }
  else{
    $other_charges = 0;

  }
  $data =  array(
    'emp_fk'=>$_POST['id'],
    'amount'=>$_POST['amount'],
    'remark'=>$_POST['remark'],
    'other_charges'=>$_POST['other_charges'],
    'total_amount'=>$amount + $other_charges,
  	'label'=>$_POST['label'],
 	'month_year'=>$_POST['month_year']
    );
  	$users = DB::table('users')->where('leave_date','=',null)->where('role','user')->get();
	$monthsalary =$_POST['month_year'];
	$res = DB::table('salary_detail')->where('emp_fk',$_POST['id'])->where('month_year','=',$_POST['month_year'])->update($data);
	return back();
	//return back()->with(['monthsalary' => $monthsalary]);
	return redirect()->route('allsalarygenerate');
}
public function inserttour()
{
  $result = DB::table('packag_types')->where('deleted_at',1)->get();
  return view('admin.inserttour',compact('result'));
}

public function tourcheckSlug(Request $request ,$id = null){    
  $where = array('slug'=>$request->slug);
  
  if($id != ""){

      $where = array(
          array('slug',$request->slug),
          array('id','<>',$id),
      );
  }
  $arrData = DB::table('toures')->where($where)->get();
  //dd($arrData,$where);
  if(count($arrData) != "0"){
      return response()->json(array('valid'=>false));
  }
  return response()->json(array('valid'=>true));
}  
public function updatetouradmin(Request $request)
{

  $id = $request->id;
  $image = $request->file('banner_img');
  if($image)
  {
    $banner_img = time().'.'.$image->getClientOriginalExtension();
    $image->move('./media', $banner_img);
  }else{
    $banner_img = $request->imagebanner2; 
  }
    $name = $request->name;
    $package_type = $request->package_type;
    $days = $request->days;
    $region=$request->region;
    $small_info = array(
        'Region' =>$request->region,
        'Duration'=>$request->duration,
        'StartPoint'=>$request->start,
        'EndPoint'=>$request->end,
        'HighestAltitute'=>$request->altitute,
        'ApproxKm'=>$request->approx,
        'Grade'=>$request->grade
    );
    
    $detail1 = $request->detaile1label;
    $detail1value = $request->detail1value;
    $detail2 = $request->detaile2label;
    $detail2value = $request->detail2value;
    $detail3 = $request->detaile3label;
    $detail3value = $request->detail3value;
    $detail4 = $request->detaile4label;
    $detail4value = $request->detail4value;
    $detail1 = array(
      $detail1=>$detail1value
    );
    $detail2 = array(
      $detail2=>$detail2value
    );
    $detail3 = array(
      $detail3=>$detail3value
    );
    $detail4 = array(
      $detail4=>$detail4value
    );
    
    $inclusion = $request->inclusion;
    $optional_charges = $request->optional_charges;
    $exclusion = $request->exclusion;
    $essentials = $request->essentials;
    $clothing_essential = $request->clothing_essential;
    $small_info = json_encode($small_info);
    $detail1 = json_encode($detail1);
    $detail2 = json_encode($detail2);
    $detail3 = json_encode($detail3);
    $detail4 = json_encode($detail4);
    
    $datainfo = array(
      'name'=>$name,
      'slug'=>$request->slug,
      'price'=>$request->price,
      'person'=>$request->person,
      'package_type'=>$package_type,
      'days'=>$days,
      'small_info'=>$small_info,
      'detail1'=>$detail1,
      'detail2'=>$detail2,
      'detail3'=>$detail3,
      'detail4'=>$detail4,
      'inclusion'=>$inclusion,
      'optional_charges'=>$optional_charges,
      'exclusion'=>$exclusion,
      'essentials'=>$essentials,
      'clothing_essential'=>$clothing_essential,
      'banner_img'=>$banner_img
    );

    $result = DB::table('toures')->where('id',$id)->update($datainfo);

    if($result){
      notify()->success('Tour Sucessfully Updated!');
      return redirect('adminshowtours');
    }else{
       notify()->error('Tour not Updated Successfully!');
       return redirect('adminshowtours');
    }
      return back();
    }
public function inserttouradmin(Request $request)
{
    
    $image = $request->file('banner_img');
    $banner_img = time().'.'.$image->getClientOriginalExtension();
    // $destinationPath = asset('itenary')."/";
    $image->move('./media', $banner_img);
    $name = $request->name;

    $package_type = $request->package_type;
    $days = $request->days;
    $region=$request->region;

    $small_info = array(
      'Region' =>$request->region,
      'Duration'=>$request->duration,
      'StartPoint'=>$request->start,
      'EndPoint'=>$request->end,
      'HighestAltitute'=>$request->altitute,
      'ApproxKm'=>$request->approx,
      'Grade'=>$request->grade
    );

    $detail1 = $request->detaile1label;
    $detail1value = $request->detail1value;
    $detail2 = $request->detaile2label;
    $detail2value = $request->detail2value;
    $detail3 = $request->detaile3label;
    $detail3value = $request->detail3value;
    $detail4 = $request->detaile4label;
    $detail4value = $request->detail4value;

    $detail1 = array(
      $detail1=>$detail1value
    );
    $detail2 = array(
      $detail2=>$detail2value
    );
    $detail3 = array(
      $detail3=>$detail3value
    );

    $detail4 = array(
      $detail4=>$detail4value
    );

    $inclusion = $request->inclusion;

    $optional_charges = $request->optional_charges;
    $exclusion = $request->exclusion;
    $essentials = $request->essentials;
    $clothing_essential = $request->clothing_essential;

    $small_info = json_encode($small_info);
    $detail1 = json_encode($detail1);
    $detail2 = json_encode($detail2);

    $detail3 = json_encode($detail3);

    $detail4 = json_encode($detail4);

  $datainfo = array(
    'name'=>$name,
    'slug'=>$request->slug,
    'price'=>$request->price,
    'person'=>$request->person,
    'package_type'=>$package_type,
    'days'=>$days,
    'small_info'=>$small_info,
    'detail1'=>$detail1,
    'detail2'=>$detail2,
    'detail3'=>$detail3,
    'detail4'=>$detail4,
    'inclusion'=>$inclusion,
    'optional_charges'=>$optional_charges,
    'exclusion'=>$exclusion,
    'essentials'=>$essentials,
    'clothing_essential'=>$clothing_essential,
    'banner_img'=>$banner_img
  );
  $result = DB::table('toures')->insert($datainfo);
  // $tour = DB::table('toures')->get();
  //Session::flash('successtour', 'Tour Sucessfully Added');

  if ($result) {
   notify()->success('Tour Sucessfully Added!');
  } else {
    notify()->error('Tour not Added Successfully!');
  }

return redirect()->route('itenaryshow');
}
public function itenaryshow()
{
  $tour = DB::table('toures')->get();

  return view('admin.itenary',compact('tour'));
}

public function itenaryinfo()
{
  $data = DB::table('itinerary')->get();
  //dd($data);
  return view('admin.intenary_info',compact('data'));
}

public function allitenaryinfo()
{

  if (Auth::user()->role =='admin') {
      return datatables()->of(Itenary::all())->toJson();
  }
  
}

public function itenaryEdit($id)
  {
      $tour = DB::table('toures')->get();
      $id = base64_decode($id);
      $info = Itenary::find($id);

      return view('admin.itenary',compact('info','tour'));
  }

public function additenary(Request $request)
{
  //dd($request->all());
      $data1 = array(
        'tour_fk_id' =>$request->tour_fk_id,
        'details'=>$request->details,
        'day_number'=>$request->day_number,
        'caption'=>$request->caption, 
      );
      if($request->file('images')){
          $image = $request->file('images');
        $name = time().'.'.$image->getClientOriginalExtension();
        $image->move('./itenary', $name);
        $data1['images'] = $name;
      }

    if($request->id){
      
      $where = array('id'=>$request->id);
      $is_update= Itenary::where($where)->update($data1);
      if($is_update) {
          notify()->success('Itinerary Updated Successfully !');
        } else {
          notify()->error('Itinerary not Added Successfully!');
      }
      return redirect()->route('itenaryinfo');
    }else{
     $is_save= DB::table('itinerary')->insert($data1);
     if($is_save) {
          notify()->success('Itinerary Added Successfully !');
        } else {
          notify()->error('Itinerary not Added Successfully!');
      }
    }
  return redirect()->route('itenaryinfo');
}

public function deleteItenary($id)
{

  $itenary = Itenary::find($id);
  
    if($itenary){
            
      $is_delete = $itenary->delete();

      if ($is_delete) {
          $status = true;
      }else{
          $status = false;
      }
      return response()->json($status);
    }
            
}

public function Review()
{//reviews
  $reviews = DB::table('review')->get();
  return view('admin.show_review',compact('reviews'));
}
public function reviewdelete($id)
{
  $result = DB::table('review')->where('id',$id)->delete();
  Session::flash('deletereview', 'Review Deleted Successfully..');
  return redirect()->route('Review');
}
public function adminReviewForm()
{
  return view('admin.adminReviewForm');
}
public function ReviewCreation(Request $request)
{
  $username = $_POST['name'];
  $content = $_POST['content'];
  $image = $request->file('image');
  $name = time().'.'.$image->getClientOriginalExtension();
  // $destinationPath = asset('itenary')."/";
  $image->move('./reviews', $name);
  $data1 = array(
'name' =>$username,
'content'=>$_POST['content'],
'image'=>$name,

);
$res = DB::table('review')->insert($data1);
Session::flash('Creatreview', 'Review Creation Successfully..');
return redirect()->route('Review');
}

public function updatereview($id)
{  $data = DB::table('review')->where('id',$id)->get();

  return view('admin.editReviewForm',compact('data'));

}
public function showsalary()
{
		$data = DB::table('salary')->select('salary.*','users.id as uid','users.name as name','salary.id as id')
        ->join('users','salary.fk_emp_id','=','users.id')->get();
		return view('admin.showsalary',compact('data'));
}
public function editsalary($id)
{	$data = DB::table('salary')->where('id',$id)->get();
 	$users = DB::table('users')->select('id','name','surname')->where('role','user')->where('leave_date',NULL)->get();
   ;
	return view('admin.editsalary',compact('data','users'));
}
public function editsalarybyadmin()
{
	$id = $_POST['id'];
	$data = array(
    	'follow_date'=>$_POST['follow_date'],
    	'fk_emp_id'=>$_POST['fk_emp_id'],
    	'salary'=>$_POST['salary']
    );
	$result = DB::table('salary')->where('id',$id)->update($data);
	Session::flash('updatesalary', 'Salary Sucessfully Updated');

  return redirect()->route('showsalary');


}


public function editreview(Request $request)
{
  // echo $id;
  $username = $_POST['name'];
  $content = $_POST['content'];
  $id = $_POST['id'];
  if($request->file('image'))
  { 
    $image = $request->file('image');
    $name = time().'.'.$image->getClientOriginalExtension();
    // $destinationPath = asset('itenary')."/";
    $image->move('./reviews', $name);
    $data1 = array(
  'name' =>$username,
  'content'=>$_POST['content'],
  'image'=>$name,
  
  );
}
else{
  $data1 = array(
    'name'=>$username,
    'content'=>$_POST['content'],
  );
   

  }
  $res = DB::table('review')->WHERE('id',$id)->update($data1);

  return redirect()->route('Review');


}
// <!*********************Event ***************************************************]\
public function eventdelete($id)
{
  $result = DB::table('events')->where('id',$id)->delete();
  Session::flash('deleteevent', 'Event Deleted Successfully..');
  return redirect()->route('showevent');

}
public function adminEventForm()
{
  return view('admin.eventForm');

}
public function eventCreation(Request $request)
{
  $data = Input::except('_method', '_token');
  $image=$request->file('image');
  $imagename=time().$image->getClientOriginalName();  
  $destinationPath = 'uploads/events';
  $image->move($destinationPath,$imagename);
  // $data['aadhar_card_pic'] = $aadhar_card_picname;
  $data = array(
    'name'=>$_POST['name'],
    'sdate'=>$_POST['sdate'],
    'edate'=>$_POST['edate'],
    'title'=>$_POST['title'],
    'description'=>$_POST['description'],
    'image'=>$imagename,
);
$result = DB::table('events')->insert($data);
Session::flash('eventcreation', 'Event Created ..');
return redirect()->route('showevent');
}
public function editevent($id)
{
$data = DB::table('events')->where('id',$id)->get();
return view('admin.updateevent',compact('data'));
}
public function updateevent(Request $request)
{ 
  $id  = $_POST['id'];
  // echo $id;

  $data = array(
    'name'=>$_POST['name'],
    'sdate'=>$_POST['sdate'],
    'edate'=>$_POST['edate'],
    'title'=>$_POST['title'],
    'description'=>$_POST['description'],
);
if($request->file('image')){
  $image=$request->file('image');
  $imagename=time().$image->getClientOriginalName();  
  $destinationPath = 'uploads/events';
  $image->move($destinationPath,$imagename);
  $data['image'] = $imagename;
}
  $result = DB::table('events')->where('id',$id)->update($data);
  Session::flash('Eventupdate', 'Event Updated ..');
  return redirect()->route('showevent');
  // echo $result;
}
public function showevent()
{
  $data = DB::table('events')->orderByDesc('sdate')->get();

  return view('admin.show_event',compact('data'));
}
 public function deleteimage(Request $request,$id)
  {
      $img = DB::table('media_tours')->where('id',$id)->delete();
      return back();
  }
  public function deletevideo($id)
  {
   $img = DB::table('videos')->where('id',$id)->delete();
      return back();
  }
  function amintourinsertvideo(Request $request,$id)
  {
    $id = base64_decode($id);
    $result= DB::table('videos')->where('fk_tour',$id)->count();
    if($result>0)
    {
      $result= DB::table('videos')->where('fk_tour',$id)->get();
      return view('admin.insertvideo',compact('result'));
     
    }
    else{
      return view('admin.insertv',compact('id'));
    }
    
    
  }
public function paymentgatway()
{
		echo "wok..";
// $params = [
//     'order_id' => 'test0013',  
//     'amount' => '20.00', 
//     'currency' => 'INR', 
//     'description' => 'test', 
//     'name' => 'akii', 
//     'email' => 'akhiiw.office@gmail.com', 
//     'phone' => '8698330550', 
//     'city' => 'Nagpur', 
//     'country' => 'IND', 
//     'zip_code' => '440015', 
//     'return_url' => url('/payStatus')
// ];

// Aggrepay::startPayment($params)->send();
//Returns response
//$result = Aggrepay::aggrepayData($_POST);
//print_r($result);
}

  public function amintourinsertphoto(Request $request,$id)
  {

    $id = base64_decode($id);

    $tour = Toure::find($id);
     
    return view('admin.adintourinsertphoto',compact('tour')); 
    
  }
  public function photogallertinsert(Request $request)
  {
      // dd($request->all());
      $files  = $_FILES;
      $filearr = $files['upload_file']['name'];
      
      $id = base64_encode($request->id);
      $files = $request->file('upload_file');
      $data=[];
    
    if($request->hasFile('upload_file'))
    {
        foreach ($files as $file) {
          
            $name = time().$file->getClientOriginalName();
            $file->move('./media/', $name);
            $data[] .= $name;
        }

    }
    
    for($i=0;$i<count($data);$i++)
    { 
      
        $data1 = array(
        'imagepath'=>'/media/'.$data[$i],
        'slug'=>$request->slug,
        'tour_fk_id'=>$request->id,
      );
      
      $res = DB::table('media_tours')->insert($data1);

      if($res)
        {
            notify()->success('Images uploaded successfully done !');
        }
        else
        {
            notify()->error('Something Wrong Try Again..!');

        }
    }
    
    return redirect('photogallery/'.$id);
  }

  
 

// Target 
// *************************Target**************************
  function showtarget()
  { 
      $data = DB::table('targets')->select('*','users.id as userid','targets.id as id')->join('users','users.id','=','targets.employee_id_fk')->orderby('targets.created_at','DESC')->paginate(10);
      return view('admin.show_target',compact('data'));
  }
  function deletetarget($id)
  {
      $result = DB::table('targets')->where('id',$id)->delete($id);
      Session::flash('deleteTarget', 'Target Deleted ....');
      return redirect()->route('showtarget');

  }
  function targetCreation()
  {
    $users = DB::table('users')->where('role','user')->where('employee_approved',1)->where('leave_Date',NULL)->get();
    return view('admin.targetform',compact('users'));
}
function edittarget($id)
{
  $users = DB::table('users')->where('role','user')->get();
  $targets = DB::table('targets')->where('id',$id)->get();
  return view('admin.edit_target',compact('users','targets'));
}
function updatetarget()
{
  $data = Input::except('_method', '_token');
  $id = $data['id'];
  $sql = DB::table('targets')->where('id','=',$id)->update($data);
  Session::flash('updateTarget', 'Target Updated ....');

  return redirect()->route('showtarget');


}
function inserttarget(Request $request)
{
  $data = $request->except('_method', '_token');
  $arr = explode("-",$data['month_year']);
  $month = $arr[0];
  $year = $arr[1];
  $data['month_year'] = $year."-".$month."-"."1";
  

  $sql = DB::table('targets')->where('employee_id_fk','=',$data['employee_id_fk'])->where('month_year',$data['month_year'])->get();
  if($sql->count() > 0)
  {
    Session::flash('targetInserted', 'This Month Target Alredy Inserted ....');

  }
else{
  $result = DB::table('targets')->insert($data);
  Session::flash('targetInserted', 'Target Inserted ....');
}
  return back();
}


  public function LeaveCron(){

    //$employees = User::where('role','user')->where('employee_approved',1)->whereNull('leave_date')->get();

    $CL_Leave = LeaveReport::select('*',DB::raw('TIME(created_at) AS created_time'),DB::raw('DATE(created_at) AS created_date'))->where('approve_status','pending')->where('leave_type','CL')->get();
   
    $time = date('H:i:s');
    $date = date('Y-m-d');
    
        foreach($CL_Leave as $key=>$value){

           $leave_time = $value->created_time;
           $leave_date = $value->created_date;

           if($leave_date < $date){
            dd("date");
              if($leave_time < $time){
                dd('time');
                dd("Leave Approve");
              }

           }

            dd($leave_date);
        }

    $timestamp = (strtotime($item->time));
    $date = date('Y.j.n', $timestamp);
    $time = date('H:i:s', $timestamp);


  }

  
}