<?php

namespace App\Http\Controllers;

use LoveyCom\CashFree\PaymentGateway\Order;
use Illuminate\Http\Request;
use AdminHelper;

class PaymentController extends Controller
{
   public function index()
    {
      return view('admin.payment.index');
    }
    public function cashFreePayment(Request $request){
     
     //dd($request->all());
      $order = new Order();
     
      $od["orderId"] = $request->agent_id;
      $od["orderAmount"] = $request->amount;
      $od["orderNote"] = $request->note;
      $od["customerPhone"] = $request->agent_phone;
      $od["customerName"] = $request->agent_name;
      $od["customerEmail"] = $request->agent_email;
      $od["returnUrl"] = $request->returnUrl;
      $od["notifyUrl"] = $request->returnUrl;
      
      $order->create($od);
      
      $link[] = '';
      $link['link'] = $order->getLink($od['orderId']);
      $link['result'] = $request->all();
      //dd($link);
      return view('front.payment_return_url',$link);
    }

    public function confirmation()
    {
      return view('front.payment_return_url');
    }

}
