<?php

namespace App\Http\Controllers;

use AdminHelper;
use App\Customer;
use App\User;
use App\Agent;
use App\AddMembership;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class CustomerController extends Controller
{
    public function index()
    {
        return view('admin.show_customer');
    }

    public function create()
    {
        return view('admin.create_customer_form');
    }

    public function show(Request $request)
    {
        if ($request->ajax()) {
            $data = Customer::where('deleted_at',0)->get();
            // return $data;

            return \DataTables::of($data)
                ->make(true);
        }
    }

    public function store(Request $request)
    {
    
        if($request->id)
        {
            $data = array(
                'first_name'=>$request->first_name,
                'last_name'=>$request->last_name,
                'mobile_number'=>$request->mobile_number,
                'email'=>$request->email,
                'whats_up'=>$request->whats_up,
            );
            $customer = Customer::where('id',$request->id)->update($data);
            if($customer)
            {
                notify()->success('Customer Updated Successfully !');
            }
            else
            {
                notify()->error('Something Wrong Try Again..!');
            }
        
        }else{
            // dd($request->all());
            $number = $request->mobile_number;
            // dd($number);
            $wnumber = $request->whats_up;    
            $User = User::where('contact_number_alt',$number)->where('whatsup_number',$wnumber)->where('employee_approved','1')->count();
            $Agent = Agent::where('deleted_at','1')->where('contact_details',$number)->where('alternate_number',$wnumber)->count();
            $Member = AddMembership::where('deleted_at','1')->where('primary_contact',$number)->where('whatsapp_number',$wnumber)->count();
            // dd($Agent);
            // $where=array('deleted_at'=>'1');
            // $Agent=Agent::where($where)
            //     ->where(function($query) use ($number,$wnumber){

            //      $query->where(function($query) use ($number,$wnumber){
            //          $query->where('contact_details', $number);
            //          $query->orWhere('alternate_number', $wnumber);
            //      })
            //      ->orWhere(function($query) use ($number,$wnumber){
            //          $query->where('contact_details', $wnumber);
            //          $query->orWhere('alternate_number', $number);
            //      });
            //  })->first();

            // $where=array('deleted_at'=>'1');
            // $Member=AddMembership::where($where)
            //     ->where(function($query) use ($number,$wnumber){

            //      $query->where(function($query) use ($number,$wnumber){
            //          $query->where('primary_contact', $number);
            //          $query->orWhere('whatsapp_number', $wnumber);
            //      })
            //      ->orWhere(function($query) use ($number,$wnumber){
            //          $query->where('primary_contact', $wnumber);
            //          $query->orWhere('whatsapp_number', $number);
            //      });
            //  })->first();

            if($User > 0)
            {
                notify()->error('Unique Mobile Number required!');
                return redirect(route('login'));
            }else if($Agent > 0){
                notify()->error('Unique Mobile Number required!');
                return redirect(route('login'));
            }else if($Member > 0){
                notify()->error('Unique Mobile Number required!');
                return redirect(route('login'));
            }
            $data['first_name'] = $request->first_name;
            $data['last_name'] = $request->last_name;
            $data['mobile_number'] = $request->mobile_number;
            $data['whats_up'] = $request->whats_up;
            $data['email'] = $request->email;
            $data['password'] = Hash::make($request->password);
            $customer = Customer::create($data);
            if($customer)
            {   
                // dd($customer);
                $data['c_password'] = $request->password;
                $data['link'] = url('/login');
                $body = view('mail.customerCreate',compact('data'))->render();
                // // return $body;
                $arr = $request->email;
                AdminHelper::sendEmailTo($body,'Customer Registered',$arr);
                notify()->success('Customer Added Successfully !');
            }
            else
            {
                notify()->error('Something Wrong Try Again..!');
            }
        }
        return redirect(route('adminshowcustomer'));
    }

    public function Detail($id)
    {
        $customer  = Customer::where('id',base64_decode($id))->first();
        return view('admin.detail_customer', compact('customer'));
    }

    public function edit($id)
    {
        $customer = Customer::find(base64_decode($id));
        return view('admin.create_customer_form',compact('customer'));
    }

    public function destroy(Request $request)
    {
        $delete = Customer::where('id',$request->id)->update(['deleted_at'=>1]);
        if($delete)
        {
            $data['icon'] = "success";
            $data['title'] = "Success !";
            $data['message'] = "Customer Deleted !";
        }
        else
        {
            $data['icon'] = "error";
            $data['title'] = "Error !";
            $data['message'] = "Something Wrong Try Again..";
        }

        return response()->json($data);
    }

    public function checkEditEmail(Request $request,$id)
    { 
        if($id){
            $id = array($id);
            $arrData = 0;
            
            $Cust_Data = Customer::whereNotIn('id', $id)->where('email',$request->email)->where('deleted_at',0)->count(); 
            $arrData=$arrData+$Cust_Data;

            $Agent_Data = Agent::where('company_email',$request->email)->where('deleted_at',1)->count();
            $arrData=$arrData+$Agent_Data;

            $Member_Data = AddMembership::where('email',$request->email)->where('deleted_at',1)->count();
            $arrData=$arrData+$Member_Data;

            $User_Data = User::where('email',$request->email)->whereNull('leave_date')->count();
            $arrData=$arrData+$User_Data;

        }else{
        
            $Cust_Data = Customer::where('email',$request->email)->where('deleted_at',0)->count();
            $arrData=$arrData+$Cust_Data;

            $Agent_Data = Agent::where('company_email',$request->email)->where('deleted_at',1)->count();
            $arrData=$arrData+$Agent_Data;

            $Member_Data = AddMembership::where('email',$request->email)->where('deleted_at',1)->count();
            $arrData=$arrData+$Member_Data;

            $User_Data = User::where('email',$request->email)->whereNull('leave_date')->count();
            $arrData=$arrData+$User_Data;
        }
        if($arrData == 0){
            return response()->json(array('valid'=>true));
        }
        return response()->json(array('valid'=>false));
    }
}
