<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;

use Auth;
use App\Review;
use App\Review_image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Storage;

class ReviewsController extends Controller
{
    public function index()
    {
        if(Auth::guard('webcustomer')->user()){
            $user = Auth::guard('webcustomer')->user();
        }else if(Auth::guard('webagent')->user()){
            $user = Auth::guard('webagent')->user();
        }else if(Auth::guard('webmember')->user()){
            $user = Auth::guard('webmember')->user();
        }
       
        return view('front.review.add_review', compact('user'));
    }

    public function store(Request $request)
    {
        
        if($request->image){

            $review =New Review();
            if(Auth::guard('webmember')->user()){
                $review->member_id =$request->id;
            }else{
                $review->user_id =$request->id;    
            }
            $review->role =$request->role;
            $review->status ='hide';
            $review->content =$request->content;  
            $save = $review->save();

            $i = 0;
            foreach($request->file('image') as $file)
            {
                $review_image =New Review_image();
                
                $name = time(). $i .'.'.$file->extension();
                $file->move('uploads/review/', $name);  
                
                $review_image->review_id =$review->id;
                $review_image->image ='uploads/review/'.$name;
                
                $save = $review_image->save();
                $i++;
            }
            
            if($save){
                notify()->success('Review added successfully done');
                return redirect('review');
            }else{
                notify()->error('Something went wrong please try again!');  
                return redirect('review');
            }
           
        }  
    }

    public function record(Request $request)
    {
        return view('admin.review.list');
    }

    public function list(Request $request)
    {
        if (Auth::user()->role =='admin') {
            return datatables()->of(Review::with('review_image','member','customer')->where('deleted_at','0'))->toJson();
        }
    }

    public function Imagelist(Request $request, $id)
    {
        //dd($id);
        $id = base64_decode($id);
        $data['image'] = Review_image::where('review_id',$id)->get();
        $data['url'] = "http://$_SERVER[HTTP_HOST]";
        
        return view('admin.review.add',$data);
    }

    public function Select(Request $request)
    {
        $id = '';
        $data = Review_image::where('review_id', $request->id)->where('image_select','selected')->first();

        if($data){
            $id = $data->id;
            
            $data1 = array(
                'image_select'=>null,
            );
           
            $where1 = array('id'=>$id); 
            Review_image::where($where1)->update($data1);
        }

        $data = array(
            'image_select'=>'selected',
        );
        
        $where = array('id'=>$request->image);  
        $isUpdated = Review_image::where($where)->update($data);

        if($isUpdated){
            notify()->success('Image Selected successfully done');
            return redirect('review/record');
        }else{
            notify()->error('Something went wrong please try again!');  
            return redirect('review/record');
        }
    }

    public function Show(Request $request, $id, $status)
    {
        $id = base64_decode($id);

        $data = array(
            'status'=>$status,
        );
        $where = array('id'=>$id);
        $isUpdated = Review::where($where)->update($data);

        if($isUpdated){

            if($status == 'show'){
                notify()->success('Review approved is successfully done');
            }else{
                notify()->success('Review decline is successfully done');  
            }
            return redirect('review/record');
        }else{
            notify()->error('Something went wrong please try again!');  
            return redirect('review/record');
        }
    }

    public function destroy($id)
    {
        //dd($id);
        $deleted = Review::where('id',$id)->update(['deleted_at'=>'1']);
        
            if ($deleted) {
                $status = true;
            }else{
                $status = false;
            }
            return response()->json($status);
    }
}
