<?php

namespace App\Http\Controllers;

use App\LeaveReport;
use Illuminate\Http\Request;

class LeaveController extends Controller
{
    public function show(Request $request)
    {
        // dd($request->all());
        $leave = LeaveReport::with('employee')->whereDate('created_at','>=',$request->date['start'])
        ->whereDate('created_at','<=',$request->date['end'])->orderBy('created_at', 'DESC');
        if($request->employee)
        {
            $leave = $leave->where('employee_fk_id',$request->employee);
        }

        if($request->status)
        {
            $leave = $leave->where('approve_status',$request->status);
        }

        if($request->type)
        {
            $leave = $leave->where('leave_type',$request->type);
        }

        $leave = $leave->get();
        return \DataTables::of($leave)
                ->make(true);
    }
}
