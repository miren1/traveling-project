<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Agent;
use App\State;
use App\City;

class AgentController extends Controller
{
	public function index()
	{
		return view('admin.agent.index');
	}


	public function list()
	{
		return datatables()->of(Agent::where('deleted_at','1')->orderBy('created_at', 'desc'))->toJson();
	}


	public function Add($id = '')
	{

		if ($id!='') {
			
			$data['agent'] = Agent::find(base64_decode($id));
		} 
		$data['state'] = State::get();
		//dd($data);
		return view('admin.agent.add')->with($data);
	}


	public function store(Request $request)
	{

		$arrData = array();
		$arrData['first_name'] =	$request->first_name;
		$arrData['last_name'] =	$request->last_name;
		$arrData['company_name'] =	$request->company_name;
		$arrData['contact_details'] =	$request->contact_details;
		$arrData['alternate_number'] =	$request->alternate_number;
		$arrData['company_pan'] =	$request->company_pan;
		$arrData['company_gst_no'] =	$request->company_gst_no;
		$arrData['company_email'] =	$request->company_email;
		$arrData['house_no'] =	$request->house_no;
		$arrData['society_name'] =	$request->society_name;
		$arrData['landmark'] =	$request->landmark;
		$arrData['pincode'] =	$request->pincode;
		$arrData['state_fk_id'] =	$request->state_fk_id;
		$arrData['city_fk_id'] =	$request->city_fk_id;
		$arrData['pincode'] =	$request->pincode;
		
		if ($request->official_address) {
			$arrData['official_address'] =	$request->official_address;
		}

		//dd($arrData);	
		if ($request->id) {

			$where= array('id'=>$request->id);
			$is_update  = Agent::where($where)->update($arrData);
			if ($is_update) {
				notify()->success('Agent Update Successfully !');
			} else {
				notify()->error('Agent not Updated Successfully!');
			}

			return redirect('admin-agent');
		} else {

			$arrData['user_id'] = Auth::user()->id;
			$arrData['password'] =	$request->password;
			$isinserted = Agent::create($arrData);

			if ($isinserted) {

				notify()->success('Agent Added Successfully!');
			} else {
				notify()->error('Agent not Added Successfully!');
			}

			return redirect('admin-agent');
		}

		return redirect('admin-agent');
	}

	public function view($id = null)
	{
		$data=array();
		if($id!=null){
			$data['agent'] = Agent::find(base64_decode($id));
			if(isset($data['agent']->city_fk_id)){
			$data['city'] =City::find($data['agent']->city_fk_id);
			}
			if(isset($data['agent']->state_fk_id)){
			$data['state'] =State::find($data['agent']->state_fk_id);
			}
			//dd($data);

		}
		return view('admin.agent.view')->with($data);
	}

	public function delete(Request $request)
	{
		$delete=Agent::find(base64_decode($request->id))->update(['deleted_at'=>'0']);
		if($delete)
        {
            $data['status'] = "success";
            $data['message'] = "Agent is deleted successfully!";
        }
        else
        {
            $data['status'] = "error";
            $data['message'] = "Agent is not deleted.";
        }

        return response()->json($data);
	}


}
