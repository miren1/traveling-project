<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Group;
use DB;
use App\Grouplead;
use App\User;
use App\Assignlead;
use App\Callstatus;

class GroupController extends Controller
{
   public function index()
   {
       $data['group']=Group::where(['is_delete'=>'0','user_id'=>Auth::user()->id])->get();
       return view('admin.group.index')->with($data);
   }

   public function list()
   {
    $user_id=Auth::user()->id;
    //$group= Group::with('grouplead')->where(['is_delete'=>'0','user_id'=>$user_id])->orderBy('created_at', 'desc')->get();
    //dd($group);
    return datatables()->of(Group::withCount('grouplead')->where(['is_delete'=>'0','user_id'=>$user_id])->orderBy('created_at', 'desc'))->toJson();
   }

   public function delete(Request $request)
    {
         $delete=Group::where('id',base64_decode($request->id))->update(['is_delete'=>'1']);
         //dd(base64_decode($request->id));
         if($delete)
         {
             Grouplead::where('group_id',base64_decode($request->id))->update(['is_delete'=>'1']);
             $data['status'] = "success";
             $data['message'] = "Group is deleted successfully!";
         }else{
             $data['status'] = "error";
             $data['message'] = "Group is not deleted.";
         }
 
         return response()->json($data);
    }

    public function lead_delete(Request $request)
    {
         $delete=Grouplead::where('id',base64_decode($request->id))->delete(['is_delete'=>'1']);
         //dd(base64_decode($request->id));
         if($delete)
         {
             
             $data['status'] = "success";
             $data['message'] = "Group is deleted successfully!";
         }else{
             $data['status'] = "error";
             $data['message'] = "Group is not deleted.";
         }
 
         return response()->json($data);
    } 


   public function store(Request $request)
   {
       //dd($request->all());
            $group_id=NULL;
            if($request->group_id=="addnew"){
                $Group =New Group();
                $Group->user_id =Auth::user()->id;
                $Group->name =$request->name;
                $save= $Group->save();
                $group_id= $Group->id;
            }else{
                $group_id= $request->group_id;
            }
            if(isset($group_id)){

                $image = $request->file('csv_file');
                $extension = $image->getClientOriginalExtension();
                $path = $request->file('csv_file')->getRealPath();
                
                if($extension == "csv"){
                   $data=$this->csvToArray($path);
                   //dd($data);
                   unset($data[0]);
                   if(!empty($data)){                       
                        if(count($data[1])==9){
                            foreach ($data as $key => $value) 
                            {
                                $v[] = $value;
                                if(isset($value[0])){
                                    if($value[3]){
                                        //$where=array('is_delete'=>'0','number'=>$value[3],'group_id'=>$group_id);     
                                        $where=array('is_delete'=>'0');     
                                        
                                        $number = $value[3];
                                        $wnumber = $value[4];

                                        // $Grouplead=Grouplead::where($where)
                                        // ->where(function($query) use ($number,$wnumber){

                                        //      $query->where(function($query) use ($number,$wnumber){
                                        //          $query->where('number', $number);
                                        //          $query->orWhere('wnumber', $wnumber);
                                        //      })
                                        //      ->orWhere(function($query) use ($number,$wnumber){
                                        //          $query->where('number', $wnumber);
                                        //          $query->orWhere('wnumber', $number);
                                        //      });
                                        //  })->first();


                                        //dd($Grouplead);
                                        // if(!$Grouplead){
                                           $Grouplead= new Grouplead();
                                           $Grouplead->user_id=Auth::user()->id;
                                           $Grouplead->group_id=$group_id;
                                           $Grouplead->fname =$value[0];
                                           $Grouplead->lname =$value[1];
                                           $Grouplead->email=$value[2];
                                           $Grouplead->number=$value[3];
                                           $Grouplead->wnumber =$value[4];
                                           $Grouplead->area =$value[5];
                                           $Grouplead->city=$value[6];
                                           $Grouplead->state=$value[7];
                                           $Grouplead->country =$value[8]; 
                                           $save= $Grouplead->save();
                                        // }  
                                    }     
                                }
                            }
                            //dd($v);
                            notify()->success('Group Created Successfully !');
                        }else{
                            notify()->error('CSV Data are Not Proper!!');
                        }
                   }else{
                     notify()->error('Group not Created Successfully CSV File is empty !');    
                   }
                }else{
                     notify()->error('Group not send Successfully CSV File extension Are wrong!');
                }
            }else{
                notify()->error('Something Wrong Try Again..!');
            }   
            return redirect('/groups');
   }

   public function csvToArray($filename = '', $delimiter = ',')
    {
        if (!file_exists($filename) || !is_readable($filename))
            return false;

        $header = null;
        $data = array();
        if (($handle = fopen($filename, 'r')) !== false)
        {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== false)
            {
                    $data[] = $row;
            }
            fclose($handle);
        }

        return $data;
    }
    public function txtToArray($filename='')
    {
        foreach (explode("\n", $filename) as $key=>$line){
             $array[$key] = explode(',', $line);
        }
        return $array;
    }

    public function list_view($id='')
    {
        
        $data['group']=Group::find(base64_decode($id));
        $data['user']=User::where('role','user')->whereNull('leave_date')->where('employee_approved',1)->get();
        
        return view('admin.group.view')->with($data);
    }

    public function list_lead($id='')
    {
    //    dd(base64_decode($id));
      $user_id=Auth::user()->id;
      $Assignlead=Assignlead::where(['group_id'=>base64_decode($id)])->get()->toArray();
      
      $Assignlead_id = array_column($Assignlead,'group_lead_id');
        // dd($Assignlead);
      if(Auth::user()){
        $data= Grouplead::where('group_id',base64_decode($id))->whereNotIn('id',$Assignlead_id)->orderBy('created_at', 'desc');  
        }else{
        $data= Grouplead::whereNotIn('id',$Assignlead_id)->where(['is_delete'=>'0','user_id'=>$user_id,'group_id'=>base64_decode($id)])->orderBy('created_at', 'desc');  
        }

      return datatables()->of($data)->toJson();  
 
    }

    public function checkName(Request $request ,$id = null){    
        $where = array('name'=>$request->name,'is_delete'=>'0');
        
        
        $arrData = Group::where($where)->get();

        if(count($arrData) != "0"){
            return response()->json(array('valid'=>false));
        }
        return response()->json(array('valid'=>true));
    }



    public function lead_view($id='')
    {
        
        
        $data['group']=Grouplead::with('group')->find(base64_decode($id));
       
        return view('admin.group.leadview')->with($data);
    }

    public function lead_edit($id='')
    {
       
        
        $data['group']=Grouplead::with('group')->find(base64_decode($id));
       
        return view('admin.group.add')->with($data);
    }


    public function lead_store(Request $request)
    {
       
        $except = ['_token','save','id','group_id'];
        $id= $request->id;
        $group_id= $request->group_id;
        $request = request();
        $cleanup = $request->except($except);
        if($cleanup){
            $Grouplead=Grouplead::where('id',$id)->update($cleanup);
            if($Grouplead){
                notify()->success('Lead Updated Successfully!');
            }else{
                notify()->error('Lead not Updated Successfully!');
            }
            
        }else{
            notify()->error('Lead not Updated Successfully!');
        }    
    
        return redirect('groups/lead-view/'.$group_id);
        
    }


    public function assign_employee(Request $request)
    {
    if($request->id){
        $dataArray=$request->id;
        foreach($dataArray as $id){
           // dd($id);
            $Assignlead= Assignlead::where(['id'=>$id])->first();
            if(!$Assignlead){
                $info= array(
                    'user_id'=>Auth::user()->id,
                    'employee_id'=>$request->employee_id,
                    'group_lead_id'=>$id,
                    'group_id'=>$request->group_id,
                );
                //dd($info);
                Assignlead::create($info);
            }else{
                continue;
            }
           
        }
        $data['message']= 'Lead Assign Successfully';
        $data['status']= 'success';
        

     }else{
        $data['message']= 'Lead Not Assign Successfully';
        $data['status']= 'falil';
     }
     return  json_encode($data);

    }

    
    public function list_employee($id='')
    {
      $user_id= Auth::user()->id;
      $data =Assignlead::with('employee')->where(['group_id'=>base64_decode($id),'user_id'=>$user_id])->groupBy('employee_id')->get()->toArray();  

      $Assignlead_id = array_column($data,'employee_id');
      $Assignlead = array_column($data,'id');
      $u=User::withCount(['mainleads'])->whereIN('id',$Assignlead_id)->get()->toArray();

      return datatables()->of($u)->toJson();  
 
    }

    public function Unassign_delete(Request $request)
    {
         $delete=Assignlead::where(['employee_id'=>base64_decode($request->id),'group_id'=>base64_decode($request->group_id)])->delete();
    
         if($delete)
         {
             
             $data['status'] = "success";
             $data['message'] = "Leads Are Unassign successfully!";
         }else{
             $data['status'] = "error";
             $data['message'] = "Leads Are not Unassign.";
         }
 
         return response()->json($data);
    } 

    public function employee_leads($id = null,$group_id = null)
    {
        $data['id']=$id;
        $data['user']=User::find(base64_decode($id)); 
        $data['group_id']=$group_id;
        return view('admin.group.emp-leads')->with($data);
    }


    public function employee_leads_list($id='',$group_id = null)
    {
      $user_id=Auth::user()->id;

      $Assignlead=Assignlead::where(['employee_id'=>base64_decode($id),'group_id'=>base64_decode($group_id)])->get()->toArray();
      $Assignlead_id = array_column($Assignlead,'group_lead_id'); 
    
      return datatables()->of(Grouplead::withCount('activity')->whereIn('id',$Assignlead_id)->where(['is_delete'=>'0','user_id'=>$user_id])->orderBy('created_at', 'desc'))->toJson();  
 
    }


    public function lead_unassign_delete(Request $request)
    {
         $delete=Assignlead::where(['group_lead_id'=>base64_decode($request->id),'employee_id'=>base64_decode($request->employee_id)])->delete();
         if($delete)
         {
             $data['status'] = "success";
             $data['message'] = "Lead is Unassign successfully!";
         }else{
             $data['status'] = "error";
             $data['message'] = "Lead is not Unassign.";
         }
 
         return response()->json($data);
    } 



    public function view_status($id = null,$emp_id=null)
    {
        $data['emp_id']=$emp_id;
        $data['grouplead']=Grouplead::with('group')->find(base64_decode($id));
        $data['callstatus']=Callstatus::where('is_delete','0')->get();
        $data['assignlead']= Assignlead::where(['status'=>'active',
                                                'employee_id'=>Auth::user()->id,
                                                'group_lead_id'=>base64_decode($id)])->first();

        return view('admin.group.empview')->with($data);
   }


}
