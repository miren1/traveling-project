<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;

use Auth;
use App\Tour_gallery;
use App\Gallery_images;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Storage;

class GalleryController extends Controller
{
    public function index()
    {
        return view('admin.gallery.list');
    }
    
    public function add()
    {
        return view('admin.gallery.add');
    }

    public function store(Request $request)
    {
        //dd($request->all());
        if($request->id){

            $data = array(
                'category'=>$request->category,
                'title'=>$request->title,
                'tag'=>$request->tag,
                'slug'=>$request->slug,
                'startdate'=>$request->startdate,
                'enddate'=>$request->enddate,
                // 'seo_title'=>$request->seo_title,
                'seo_keywords'=>$request->seo_keywords,
                'seo_description'=>$request->seo_description,
            );
            
            $where = array('id'=>$request->id);
            $isUpdated = Tour_gallery::where($where)->update($data);

            if($request->file('image')){
                //dd($request->file('image'),"get"); 
                $j = 0;
                foreach($request->file('image') as $file)
                {
                    
                    $name = time(). $j .'.'.$file->extension();
                    $file->move('uploads/gallery/', $name);  
                    
                    $data2 = array(
                        'image'=>'uploads/gallery/'.$name,
                        'slug'=>$request->slug
                    );
                    // dd($request->image_id);
                    $where = array('id'=>$request->image_id);
                    
                    $isUpdated = Gallery_images::where($where)->update($data2);

                    $j++;
                }   
            }

            if($isUpdated){
                notify()->success('Gallery updated successfully done');
                return redirect('tour-gallery');
            }else{
                notify()->error('Something went wrong please try again!');  
                return redirect('tour-gallery');
            }

        }else{

            if($request->image){

                $gallery =New Tour_gallery();
                $gallery->category =$request->category;
                $gallery->title =$request->title;
                $gallery->tag =$request->tag;
                $gallery->slug =$request->slug;
                $gallery->startdate =$request->startdate;
                $gallery->enddate =$request->enddate;
                // $gallery->seo_title =$request->seo_title;
                $gallery->seo_keywords =$request->seo_keywords;
                $gallery->seo_description =$request->seo_description;
                $save = $gallery->save();

                $i = 0;
                foreach($request->file('image') as $file)
                {
                    $gallery_image =New Gallery_images();
                    
                    $name = time(). $i .'.'.$file->extension();
                    $file->move('uploads/gallery/', $name);  
                    
                    $gallery_image->gallery_id =$gallery->id;
                    $gallery_image->slug =$gallery->slug;
                    $gallery_image->image ='uploads/gallery/'.$name;
                    //dd($gallery_image);
                    $save = $gallery_image->save();
                    $i++;
                }
                
                if($save){
                    notify()->success('Gallery added successfully done');
                    return redirect('tour-gallery');
                }else{
                    notify()->error('Something went wrong please try again!');  
                    return redirect('tour-gallery');
                }
               
            }
            
        }    
    }

    public function list(Request $request)
    {
        if (Auth::user()->role =='admin') {
            
            // return datatables()->of(Tour_gallery::withCount('gallery_image')->where('deleted_at','0')->orderBy('created_at', 'desc'))->toJson();
            
            return datatables()->of(Gallery_images::with('all_image')->where('deleted_at','0')->orderBy('created_at', 'desc'))->toJson();
        }
    }

    public function edit($id)
    {
    
        $id = base64_decode($id);
        
        $photos = Gallery_images::with('all_image')->where('id',$id)->first();
        $photos['url'] = "http://$_SERVER[HTTP_HOST]";
        //dd($photos);
        return view('admin.gallery.add',compact('photos'));
    }

    public function destroy($id, $gallery_id)
    {
        $deleted = Gallery_images::where('id',$id)->update(['deleted_at'=>'1']);

        $data = Gallery_images::where('gallery_id',$gallery_id)->where('deleted_at','0')->get();
        
        if($data->count() == 0){
            Tour_gallery::where('id',$gallery_id)->update(['deleted_at'=>'1']);
        }
        
            if ($deleted) {
                $status = true;
            }else{
                $status = false;
            }
            return response()->json($status);
    }

    public function checkcategory(Request $request ,$id = null)
    { 
        //dd($request, $id);
        /*if($id == 'null'){
            $data = Tour_gallery::where('deleted_at','0')->where('category',$category)->get(); 
        }else{
            $id = array($id);
            $data = Tour_gallery::whereNotIn('id', $id)->where('deleted_at','0')->where('category',$category)->get();  
        }
        //dd($data);
        return response()->json($data);*/

        if($id != ""){
            $where = array(
                array('category',$request->category),
                array('id','<>',$id),
            );
            $arrData = Tour_gallery::where('deleted_at','0')->where($where)->get();
        }else{
            $where = array('category'=>$request->category);
            $arrData = Tour_gallery::where('deleted_at','0')->where($where)->get();
        }
        if(count($arrData) != "0"){
            return response()->json(array('valid'=>false));
        }
        return response()->json(array('valid'=>true));
    }

    public function CheckSlug(Request $request ,$id = null){

      if($id != ""){
          $where = array(
              array('slug',$request->slug),
              array('id','<>',$id),
          );
          $arrData = Tour_gallery::where('deleted_at','0')->where($where)->get();
      }else{
        $where = array('slug'=>$request->slug);
        $arrData = Tour_gallery::where('deleted_at','0')->where($where)->get();
      }
    
      if(count($arrData) != "0"){
          return response()->json(array('valid'=>false));
      }
      return response()->json(array('valid'=>true));
    } 
}
