<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;

use Auth;
use App\Announcement;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Storage;

class AnnouncementsController extends Controller
{
    public function index()
    {
        return view('admin.announcement.list');
    }
    
    public function add()
    {
        $employee = Announcement::where('employee','1')->where('deleted_at','0')->count();
        // dd($employee);
        $member = Announcement::where('member','1')->where('deleted_at','0')->count();
        $agent = Announcement::where('agent','1')->where('deleted_at','0')->count();
        $user = Announcement::where('user','1')->where('deleted_at','0')->count();
        $all_user = Announcement::where('all_user','1')->where('deleted_at','0')->count();
        return view('admin.announcement.add',compact('employee','member','agent','user','all_user'));
    }

    public function store(Request $request)
    {   
        
        if($request->id){

            
            $employee='0';
            $agent='0';
            $member='0';
            $user='0';
            $all_user='0';
            if($request->employee){
                $employee='1';
            }
            if($request->agent){
                $agent='1';
            }
            if($request->member){
                $member='1';
            }
            if($request->user){
                $user='1';
            }
            if($request->all_user){
                $all_user='1';
            }

            if($request->image){
                if($request->hasFile('image')){
                    $file = $request->image;
                    $filename = time() . '.' . $file->getClientOriginalExtension();
                    $file->move('uploads/announcement', $filename);
                }

                $data = array(
                    'content'=>$request->content,
                    'image'=>'uploads/announcement/'.$filename,
                    'all_user'=>$all_user,
                    'employee'=>$employee,
                    'agent'=>$agent,
                    'member'=>$member,
                    'user'=>$user,
                );
            }else{
            
                $data = array(
                    'content'=>$request->content,
                    'all_user'=>$all_user,
                    'employee'=>$employee,
                    'agent'=>$agent,
                    'member'=>$member,
                    'user'=>$user,
                );
            }
            //dd($data);
            $where = array('id'=>$request->id);
            $isUpdated = Announcement::where($where)->update($data);

            if($isUpdated){
                notify()->success('Announcement updated successfully done');
                return redirect('announcements');
            }else{
                notify()->error('Something went wrong please try again!');  
                return redirect('announcements');
            }

        }else{
                if ($request->employee) {
                    $employee = Announcement::where('employee','1')->where('created_at','1');
                }



            $employee='0';
            $agent='0';
            $member='0';
            $user='0';
            $all_user='0';
            if($request->employee){
                $employee='1';
            }
            if($request->agent){
                $agent='1';
            }
            if($request->member){
                $member='1';
            }
            if($request->user){
                $user='1';
            }
            if($request->all_user){
                $all_user='1';
            }

            $announcement =New Announcement();
            $announcement->content =$request->content;
            $announcement->all_user =$all_user;
            $announcement->employee =$employee;
            $announcement->agent =$agent;
            $announcement->member =$member;
            $announcement->user =$user;
            //dd($announcement);
            if($request->hasFile('image')){
                $file = $request->image;
                $filename = time() . '.' . $file->getClientOriginalExtension();
                $file->move('uploads/announcement', $filename);
            }

            $announcement->image ='uploads/announcement/'.$filename;
            //dd($announcement);
            $save = $announcement->save();
                
            
            if($save){
                notify()->success('Announcement added successfully done');
                return redirect('announcements');
            }else{
                notify()->error('Something went wrong please try again!');  
                return redirect('announcements');
            }
             
        }    
    }

    public function list(Request $request)
    {
        if (Auth::user()->role =='admin') {
            return datatables()->of(Announcement::where('deleted_at','0')->get())->toJson();
        }
    }

    public function edit($id)
    {
        $id = base64_decode($id);
        //dd($id);
        $data['announce'] = Announcement::where('id',$id)->first();
        $data['url'] = "http://$_SERVER[HTTP_HOST]";
        $data['employee'] = Announcement::where('employee','1')->where('deleted_at','0')->count();
        $data['member'] = Announcement::where('member','1')->where('deleted_at','0')->count();
        $data['agent'] = Announcement::where('agent','1')->where('deleted_at','0')->count();
        $data['user'] = Announcement::where('user','1')->where('deleted_at','0')->count();
        $data['all_user'] = Announcement::where('all_user','1')->where('deleted_at','0')->count();
        //dd($data);
        return view('admin.announcement.add',$data);
    }

    public function destroy($id)
    {
        //dd($id);
        $deleted = Announcement::where('id',$id)->update(['deleted_at'=>'1']);
        
        if ($deleted) {
            $status = true;
        }else{
            $status = false;
        }
        return response()->json($status);
    }

    public function CheckRecord($user)
    {

        $check = Announcement::where('all_user','1')->where('deleted_at','0')->get();
        if (count($check) == 1) {
            $status = true;
        }else{
            $status = false;
        }
        return response()->json($status);
    }

    public function CheckEmp($user)
    {
        $check = Announcement::where('employee','1')->where('deleted_at','0')->count();
        if ($check == 1) {
            $status = true;
        }else{
            $status = false;
        }
        return response()->json($status);
    }

    public function CheckAgent($user)
    {
        $check = Announcement::where('agent','1')->where('deleted_at','0')->get();
        if (count($check) == 1) {
            $status = true;
        }else{
            $status = false;
        }
        return response()->json($status);
    }

    public function CheckMember($user)
    {
        $check = Announcement::where('member','1')->where('deleted_at','0')->get();

        if (count($check) == 1) {
            $status = true;
        }else{
            $status = false;
        }
        return response()->json($status);
    }

    public function CheckUser($user)
    {
        $check = Announcement::where('user','1')->where('deleted_at','0')->get();
        
        if (count($check) == 1) {
            $status = true;
        }else{
            $status = false;
        }      
        return response()->json($status);
    }

    public function checkboxCheck()
    {
        $employee = Announcement::where('employee','1')->where('deleted_at','0')->count();
        $all_user = Announcement::where('all_user','1')->where('deleted_at','0')->count();
        $user = Announcement::where('user','1')->where('deleted_at','0')->count();
        $agent = Announcement::where('agent','1')->where('deleted_at','0')->count();
        $member = Announcement::where('member','1')->where('deleted_at','0')->count();
        // dd($check);

        if ($employee == 1 || $all_user == 1 || $user == 1 || $agent == 1 || $member == 1) {
            $status =true;
        }
        else{
            $status = false;
        }
        return response()->json($status);
    }
}
