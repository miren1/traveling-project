<?php

namespace App\Http\Controllers;

use App\Slider;
use Illuminate\Http\Request;

class SliderController extends Controller
{
    public function index()
    {
        return view('admin.slider.index');
    }
    public function show(Request $request)
    {
        if ($request->ajax()) {
            $data = Slider::all();
            // return $data;

            return \DataTables::of($data)
                ->make(true);
        }
    }

    public function create()
    {
        return view('admin.slider.add');
    }

    public function store(Request $request)
    {
        // dd($request->all());
        
        if($request->id)
        {
            $this->update($request);
            return redirect('admin-banner');
        }
        else
        {
            $data = array(
                'title'=>$request->title,
                'description'=>$request->description,
                'video_url'=>$request->video_url
            );
            if($request->file('image'))
            {
                $file = $request->image;
                $extension = $file->getClientOriginalName();
                $name = strtotime('now').'_'.$extension;
                
                $file->move('banner',$name);

                $data['image'] = $name;
            }
            
            $slider = Slider::create($data);
            if($slider)
            {
                notify()->success('Banner Added Successfully !');
            }
            else
            {
                notify()->error('Something Wrong Try Again..!');
            }

            return redirect('admin-banner');
        }
    }

    public function edit($id)
    {
        $id = base64_decode($id);
        $slider = Slider::find($id);

        return view('admin.slider.add',compact('slider'));
    }

    public function update($request)
    {
        $data = array(
            'title'=>$request['title'],
            'description'=>$request['description'],
            'video_url'=>$request['video_url']
        );
        $slider = Slider::find($request['id']);
        // dd($slider);
        if($request->file('image'))
        {
            // dd('true');
            $file = $request->image;
            $extension = $file->getClientOriginalName();
            $name = strtotime('now').'_'.$extension;
            
            $file->move('banner',$name);

            $data['image'] = $name;
            
            if($slider->image)
            {
                /*$link = '/banner/'.$slider->image;
                unlink($link);*/

                $link = 'banner/'.$slider->image;
            
                //unlink($link);
            }

        }
        // dd('false');
        $edit = Slider::where('id',$request['id'])->update($data);
        if($edit)
        {
            notify()->success('Banner Update Successfully !');
        }
        else
        {
            notify()->error('Something Wrong Try Again..!');
        }

        return redirect('admin-banner');

    }

    public function destroy(Request $request)
    {
        
        $slider = Slider::find($request->id);
        if($slider->image)
        {
            $link = 'banner/'.$slider->image;
            
            //unlink($link);
        }
        $delete = Slider::where('id',$request->id)->delete();
        if($delete)
        {
            $data['icon'] = "success";
            $data['title'] = "Success !";
            $data['message'] = "Banner Deleted !";
        }
        else
        {
            $data['icon'] = "error";
            $data['title'] = "Error !";
            $data['message'] = "Something Wrong Try Again..";
        }

        return response()->json($data);
    }
}
