<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;

use Auth;
use App\Ads;
use App\Blog;
use App\Ads_blog;
use App\Blog_media;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Storage;

class BlogController extends Controller
{
    public function index()
    {
        return view('admin.blog.list');
    }
    public function add()
    {
        $ads_details = Ads::where('is_deleted','0')->get();
        //dd($ads);
       // $blog = Blog::with('blogs_ads')->get();
        return view('admin.blog.add',compact('ads_details'));
    }

    public function store(Request $request)
    {
        // dd($request->all());
        // dd($request->id);
        
        if($request->id){
            
            if($request->hasFile('thumbnail_image')){
                $file = $request->thumbnail_image;
                $filename = time() . '.' . $file->getClientOriginalExtension();
                $file->move('uploads/blog', $filename);
                $file = 'uploads/blog/'. $filename;
            }else{
                $file = Blog::where('id', $request->id)->first();
                $file = $file->thumbnail_image;
            }
            if($request->images){
                $data = array(
                    'tag'=>$request->tag,
                    'title'=>$request->title,
                    'thumbnail_image'=>$file,
                    'content'=>$request->editorcontent,
                    // 'seo_title' =>$request->seo_title,
                    'seo_keywords' =>$request->seo_keywords,
                    'seo_description' =>$request->seo_description
                );
            }else{
                $data = array(
                    'tag'=>$request->tag,
                    'title'=>$request->title,
                    'content'=>$request->editorcontent,
                    // 'seo_title' =>$request->seo_title,
                    'seo_keywords' =>$request->seo_keywords,
                    'seo_description' =>$request->seo_description
                );
            }
                // dd($request->all());
            $where = array('id'=>$request->id);
            $isUpdated = Blog::where($where)->update($data);
            Ads_blog::where("blog_id",$request->id)->delete();
            foreach ($request->ads as $key => $value) {
                # code...
            $isUpdated = Ads_blog::create(["ads_id"=>$value,"blog_id"=>$request->id]);
            }
            // dd($isUpdated);
            // $isUpdated2  = Ads_blog::whereNotIn('ads_id',$request->ads)->where('blog_id', $request->id)->update(['blog_id'=>null]); 

            if($isUpdated){
                notify()->success('Blog updated successfully done');
                return redirect('blogs');
            }else{
                notify()->error('Something went wrong please try again!');  
                return redirect('blogs');
            }

        }else{
            // dd($request->all());
            $blog =New Blog();
            $blog->title =$request->title;
            $blog->tag =$request->tag;
            $blog->slug =$request->slug;
            $blog->content =$request->editorcontent;
            $blog->seo_keywords =$request->seo_keywords;
            $blog->seo_description =$request->seo_description;


            if($request->hasFile('thumbnail_image')){
                $file = $request->thumbnail_image;
                $filename = time() . '.' . $file->getClientOriginalExtension();
                $file->move('uploads/blog', $filename);
            }
            $blog->thumbnail_image ='uploads/blog/'.$filename;
            $save = $blog->save();

            foreach($request->ads as $key){
                // dd($key);
                $ads_blog = new Ads_blog();
                $ads_blog->ads_id =$key;
                $ads_blog->blog_id =$blog->id;
                $save = $ads_blog->save();
            }
            // dd('success');
            /*if($request->images){
                
                $j = 0;
                foreach($request->file('images') as $file)
                {
                    $blog_media = new Blog_media();
                    $blog_media->blog_id =$blog->id;
                    $blog_media->slug =$request->slug;

                    $filename = time(). $j .'.'.$file->extension();
                    $file->move('uploads/blog', $filename);  
                    
                    $blog_media->image ='uploads/blog/'.  $filename;
                    $save = $blog_media->save();

                    $j++;
                } 
            }*/

            if($save){
                notify()->success('Blog added successfully done');
                return redirect('blogs');
            }else{
                notify()->error('Something went wrong please try again!');  
                return redirect('blogs');
            }
        }    
    }

    public function list(Request $request)
    {
        if (Auth::user()->role =='admin') {
            return datatables()->of(Blog::with('blogs_ads')->where('deleted_at','0')->orderBy('id', 'DESC')->get())->toJson();
        }
    }
    public function edit($id)
    {
    
        $id = base64_decode($id);
        $blog = Blog::with('blogs_ads','blogs_media')->find($id);
        //$blog['url'] = "http://$_SERVER[HTTP_HOST]";
        $ads_details = Ads::where('is_deleted','0')->get();
        //dd($ads_details);
        return view('admin.blog.add',compact('blog','ads_details'));
    }

    public function destroy($id)
    {
        //dd($id);
        $deleted = Blog::where('id',$id)->update(['deleted_at'=>'1']);
        
            if ($deleted) {
                $status = true;
            }else{
                $status = false;
            }
            return response()->json($status);
    }

    /*public function CheckSlug($slug, $id)
    { 
        if($id == 'null'){
            $data = Blog::where('deleted_at','0')->where('slug',$slug)->get();
        }else{
            $id = array($id);
            $data = Blog::whereNotIn('id', $id)->where('slug',$slug)->get();  
        }
        return response()->json($data);
    }*/

    public function CheckSlug(Request $request ,$id = null){
      if($id != ""){
          $where = array(
              array('slug',$request->slug),
              array('id','<>',$id),
          );
          $arrData = Blog::where('deleted_at','0')->where($where)->get();
      }else{
        $where = array('slug'=>$request->slug);
        $arrData = Blog::where('deleted_at','0')->where($where)->get();
      }
      if(count($arrData) != "0"){
          return response()->json(array('valid'=>false));
      }
      return response()->json(array('valid'=>true));
    } 
}
