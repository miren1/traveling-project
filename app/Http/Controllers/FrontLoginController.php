<?php

namespace App\Http\Controllers;

use OTP;
use App\User;
use App\Agent;
use AdminHelper;
use App\Customer;
use App\AddMembership;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use LoveyCom\CashFree\PaymentGateway\Order;

class FrontLoginController extends Controller
{
    public function index()
    {
        return view('front.login');
    }

    public function SignUp(Request $request)
    {
        // dd($request->all());
        $arrData = array(
            'first_name'=>$request->first_name,
            'last_name'=>$request->last_name,
            'email'=>$request->email,
            'mobile_number'=>$request->mobile_number,
            'password'=>Hash::make($request->password),
            'whats_up'=>$request->whats_up,
        );
         // dd("error");
        $customer = Customer::create($arrData);
        if($customer)
        {
            notify()->success('Now You Can Login Safelly !','Signup Successfully');
        }
        else
        {
            notify()->error('Something Wrong !','Error');
        }

        return redirect(route('login'));
    }

    public function checkEmail(Request $request,$id)
    {
        // dd($request->all());
        if($request->company_email){
            $email = $request->company_email;
        }else{
            $email = $request->email;
        }
        
        $arrData = 0;
        
        $Cust_Data = Customer::where('email',$email)->where('deleted_at',0)->count();
        
        $arrData=$arrData+$Cust_Data;

        $Agent_Data = Agent::where('company_email',$email)->where('deleted_at',1)->count();
        $arrData=$arrData+$Agent_Data;

        $Member_Data = AddMembership::where('email',$email)->where('deleted_at',1)->count();
        $arrData=$arrData+$Member_Data;

        $User_Data = User::where('email',$email)->whereNull('leave_date')->count();
        $arrData=$arrData+$User_Data;

        if($arrData == 0){
            return response()->json(array('valid'=>true));
        }
        return response()->json(array('valid'=>false));
        
    }

    public function InvitecheckEmail(Request $request,$id)
    {

        
        $email = $request->email;
        
        $arrData = 0;
        
        $Cust_Data = Customer::where('email',$email)->where('deleted_at',0)->count();
        
        $arrData=$arrData+$Cust_Data;

        $Agent_Data = Agent::where('company_email',$email)->where('deleted_at',1)->count();
        $arrData=$arrData+$Agent_Data;

        $invite_Data = AddMembership::where('email',$email)->where('deleted_at',1)->where('status','mandate')->count();
        
        $arrData=$arrData+$invite_Data;
        
        $Member_Data = AddMembership::where('email',$email)->where('deleted_at',1)->where('status','member')->count();
        
        $arrData=$arrData+$Member_Data;
        
        $User_Data = User::where('email',$email)->whereNull('leave_date')->count();
        $arrData=$arrData+$User_Data;
        
        if($arrData == 0){
            return response()->json(array('valid'=>true));
        }
        return response()->json(array('valid'=>false));
        
    }

    public function check_Phone(Request $request,$id)
    {
        
        $arrData = 0;
        
        $Cust_Data = Customer::where('mobile_number',$request->contact)->where('deleted_at',0)->count();
        
        $arrData=$arrData+$Cust_Data;

        $Agent_Data = Agent::where('contact_details',$request->contact)->where('deleted_at',1)->count();
        $arrData=$arrData+$Agent_Data;

        $Member_Data = AddMembership::where('primary_contact',$request->contact)->where('deleted_at',1)->count();
        $arrData=$arrData+$Member_Data;

        $User_Data = User::where('contact_number',$request->contact)->whereNull('leave_date')->count();
        $arrData=$arrData+$User_Data;

        if($arrData == 0){
            return response()->json(array('valid'=>true));
        }
        return response()->json(array('valid'=>false));
       
    }

    public function checkPhone(Request $request,$id)
    {
        $where = array(
            array('mobile_number','like','%'.$request->mobile_number.'%'),
            array('id','<>',$id)
        );

        $arrData = Customer::where($where)->where('deleted_at',0)->get();

        if(count($arrData) != "0"){
            return response()->json(array('valid'=>false));
        }
        return response()->json(array('valid'=>true));
    }

    public function AgentcheckEmail(Request $request,$id)
    {
       
        $where = array(
            array('company_email','like','%'.$request->company_email.'%'),
            array('id','<>',$id)
        );

        $arrData = Agent::where($where)->get();
       
        if(count($arrData) != "0"){
            return response()->json(array('valid'=>false));
        }
        return response()->json(array('valid'=>true));
    }

    public function AgentcheckPhone(Request $request,$id)
    {
        // dd($request->all());
        $where = array(
            array('contact_details','like','%'.$request->contact_details.'%'),
            array('id','<>',$id)
        );

        $arrData = Agent::where($where)->get();
        // ($request->contry)?$contry=$request->contry:$contry="";
        if(count($arrData) != "0"){
            return response()->json(array('valid'=>false));
        }
        return response()->json(array('valid'=>true));
    }

    public function AgentSignUp(Request $request)
    {

        $request['password'] = Hash::make($request->password);
        $request['membership_fee'] = '1000';
        $agent = Agent::create($request->all())->id;
        //$agent = $agent.'_agent';

        $type = 'agent_fee';
        $id = base64_encode($agent);

        $ReturnUrl = url('/payment_returnURL/'.$id.'/'.$type);
        $NotifyUrl = url('/payment_notifyURL');
        
        try{
            //dd('try');
            $order = new Order();
            $od["orderId"] = time();
            $od["orderAmount"] = $request['membership_fee'];
            $od["customerPhone"] = $request->contact_details;
            $od["customerName"] = $request->first_name.''.$request->last_name;
            $od["customerEmail"] = $request->company_email;
            $od["returnUrl"] = $ReturnUrl;
            $od["notifyUrl"] = $NotifyUrl;
            
            $order->create($od);
            $link[] = '';
            $link = $order->getLink($od['orderId']);

        } catch (\Exception $e) {
            
            notify()->error($e->getMessage());    
            $model = Agent::find( $agent );
            $model->delete(); 
            
            return redirect(route('login'));
        }
        
        return Redirect::to($link->paymentLink);
            
    }

    public function phoneCheck(Request $request)
    {
        $arrData = 0;
        if($request->type=="customer")
        {
            $arrData = Customer::where('mobile_number',$request->contact)->where('deleted_at',0)->count();
        }
        if($request->type=="agent")
        {
            $arrData = Agent::where('contact_details',$request->contact)->where('deleted_at',1)->count();
        }
        if($request->type=="member")
        {
            $arrData = AddMembership::where('primary_contact',$request->contact)->count();
            //dd($arrData);
        }
        if($request->type=="employee")
        {
            $arrData = User::where('contact_number',$request->contact)->whereNull('leave_date')->count();
        }
        
        if($arrData == 1){
            return response()->json(array('valid'=>true));
        }
        return response()->json(array('valid'=>false));
    }

    public function emailCheck(Request $request)
    {
        //dd($request->all());
        $arrData = 0;
        if($request->type=="customer")
        {
            $arrData = Customer::where('email',$request->email)->where('deleted_at',0)->count();
            // dd($arrData);
        }
        if($request->type=="agent")
        {
            $arrData = Agent::where('company_email',$request->email)->where('deleted_at',1)->count();
        }
        if($request->type=="member")
        {
            $arrData = AddMembership::where('email',$request->email)->where('deleted_at',1)->count();
        }
        if($request->type=="employee")
        {
            $arrData = User::where('email',$request->email)->whereNull('leave_date')->count();
        }
        
        if($arrData == 1){
            return response()->json(array('valid'=>true));
        }
        return response()->json(array('valid'=>false));
    }

    public function Forgotpass_emailCheck(Request $request)
    {
        $arrData = 0;
        if($request->type=="customer")
        {
            $arrData = Customer::where('email',$request->email)->where('deleted_at',0)->count();
            // dd($arrData);
        }
        if($request->type=="agent")
        {
            $arrData = Agent::where('company_email',$request->email)->where('deleted_at',1)->count();
        }
        if($request->type=="member")
        {
            $arrData = AddMembership::where('email',$request->email)->where('deleted_at',1)->count();
        }
        if($request->type=="employee")
        {
            $arrData = User::where('email',$request->email)->whereNull('leave_date')->count();
        }
        if($request->type=="employee")
        {
            $arrData = User::where('email',$request->email)->whereNull('leave_date')->count();
        }
        
        if($arrData == 1){
            return response()->json(array('valid'=>true));
        }
        return response()->json(array('valid'=>false));
    }

    public function LoginMobileOtp(Request $request)
    {
        //$otp = 5252;//random_int(1000, 9999);
        $otp = random_int(1000, 9999);
        $to = $request->mobile;
        $date = date('Y-m-d H:i',strtotime('+5 minutes'));
        if($request->type=="customer")
        {
            $user = Customer::where('deleted_at','0')->where('mobile_number',$request->mobile)->first();
            $name = $user->first_name;
        }
        if($request->type=="agent")
        {
            $user = Agent::where('deleted_at','1')->where('contact_details',$request->mobile)->first();

            $name = $user->first_name;
        }
        if($request->type=="member")
        {
            $user = AddMembership::where('deleted_at','1')->where('primary_contact',$request->mobile)->first();
            $name = $user->name;
            //dd($name);
        }
        if($request->type=="employee")
        {
            $user = User::where('employee_approved','1')->where('contact_number',$request->mobile)->first();
            $name = $user->name;
        }
        $user->otp = $otp;
        $user->otp_at = $date;
        $sent_otp = $user->save();
       //dd($sent_otp);
        $sentOtp = OTP::SendOtp(ucfirst($name),$otp,$to,'LOGIN');
       // dd($sentOtp);
        if($sent_otp)
        {
            return response()->json(['success'=>true]);
        }
        else
        {
            return response()->json(['success'=>false]);
        }
    }

    public function LoginEmailOtp(Request $request)
    {
        $data['otp'] = random_int(1000, 9999);
        // dd($request->all());
        $to = $request->email;
        $date = date('Y-m-d H:i',strtotime('+5 minutes'));
        if($request->type=="customer")
        {
            $user = Customer::where('email',$request->email)->first();
            $data['name'] = $user->first_name;
        }
        if($request->type=="agent")
        {
            $user = Agent::where('company_email',$request->email)->first();
            $data['name'] = $user->first_name;
        }
        if($request->type=="member")
        {
            $user = AddMembership::where('email',$request->email)->first();
            $data['name'] = $user->name;
        }
        if($request->type=="employee")
        {
            $user = User::where('email',$request->email)->first();
            $data['name'] = $user->name;
        }
        $user->otp = $data['otp'];
        $user->otp_at = $date;
        $sent_otp = $user->save();

        $body = view('mail.otpmail',compact('data'))->render();
        // return $body;
        AdminHelper::sendEmailTo($body,'OTP Verification',$to);
        if($sent_otp)
        {
            return response()->json(['success'=>true]);
        }
        else
        {
            return response()->json(['success'=>false]);
        }
    }

    public function LoginOTP(Request $request)
    {
        if($request->contact)
        {
            if($request->type=="customer")
            {
                $count = Customer::where(['mobile_number'=>$request->contact,'deleted_at'=>'0','otp'=>$request->otp])
                ->where('otp_at','>=',date('Y-m-d H:i'))->count();
                
            }
            if($request->type=="agent")
            {
                $count = Agent::where(['contact_details'=>$request->contact,'otp'=>$request->otp])
                ->where('otp_at','>=',date('Y-m-d H:i'))->count();
            }
            if($request->type=="member")
            {
                $count = AddMembership::where(['primary_contact'=>$request->contact,'otp'=>$request->otp])->where('otp_at','>=',date('Y-m-d H:i'))->count();

            }
            if($request->type=="employee")
            {
                $count = User::where(['contact_number'=>$request->contact,'otp'=>$request->otp])
                ->where('otp_at','>=',date('Y-m-d H:i'))->count();
            }
        }
        else
        {
            if($request->type=="customer")
            {
                $count = Customer::where(['email'=>$request->email,'otp'=>$request->otp])
                ->where('otp_at','>',date('Y-m-d H:i'))->count();
            }
            if($request->type=="agent")
            {
                $count = Agent::where(['company_email'=>$request->email,'otp'=>$request->otp])
                ->where('otp_at','>',date('Y-m-d H:i'))->count();
            }
            if($request->type=="member")
            {
                $count = AddMembership::where(['email'=>$request->email,'otp'=>$request->otp])
                ->where('otp_at','>',date('Y-m-d H:i'))->count();
            }
            if($request->type=="employee")
            {
                $count = User::where(['email'=>$request->email,'otp'=>$request->otp])
                ->where('otp_at','>',date('Y-m-d H:i'))->count();
            }
        }

        if($count==1)
        {
            return response()->json(['valid'=>true]);
        }
        else
        {
            return response()->json(['valid'=>false]);
        }
    }

    public function Login(Request $request)
    {
        
        if($request->type=="employee")
        {
            if($request->contry=="india")
            {
                $userdata['contact_number'] = $request->contact;
            }
            else
            {
                $userdata['email'] = $request->email;
            }
            $userdata['password'] = $request->password;
            $userdata['employee_approved'] = 1;
            $userdata['role']='user';

            if (Auth::attempt($userdata)) {

                $email = Auth::user()->email;
                $id = Auth::user()->id;
                $name = Auth::user()->name;
                $surname = Auth::user()->surname;
                $role = Auth::user()->role;
                $fullname = $name." ".$surname;
                $request->session()->put('emp_email', $email);
                $request->session()->put('emp_name',$fullname);
                $request->session()->put('eid',$id);
                $request->session()->put('role',$role);
                User::where($userdata)->update(['otp'=>NULL,'otp_at'=>NULL]);
                notify()->success('Loged in','Success');
                return redirect(route('empdashboard'));
        
            } else { 
                notify()->error('Invalid creadentials!','Error');     
                return redirect()->back();
            }
        }

        if($request->type=="agent")
        {
        
            if($request->contry=="india")
            {
                $userdata['contact_details'] = $request->contact;
            }
            else
            {
                $userdata['company_email'] = $request->email;
            }
            $userdata['password'] = $request->password;
            $userdata['deleted_at'] = '1';
            if(Auth::guard('webagent')->attempt($userdata))
            {
               // return redirect()->route('front.dashboard');
                return redirect()->route('front.tickets');
            }
        }

        if($request->type=="customer")
        {
            // dd($request->all());
            if($request->contry=="india")
            {
                $userdata['mobile_number'] = $request->contact;
            }
            else
            {
                $userdata['email'] = $request->email;
            }
            $userdata['password'] = $request->password;
            $userdata['deleted_at'] = '0';
            if(Auth::guard('webcustomer')->attempt($userdata))
            {
                return redirect()->route('front.dashboard');
            }
        }

        if($request->type=="member")
        {
             
            if($request->contry=="india")
            {
                $userdata['primary_contact'] = $request->contact;
            }
            else
            {
                $userdata['email'] = $request->email;
            }
            $userdata['password'] = $request->password;
            $userdata['deleted_at'] = '1';
            if(Auth::guard('webmember')->attempt($userdata))
            {
                return redirect()->route('front.dashboard');
            }
        }

        notify()->error('Invalid Credintials','Invalid');
        return redirect()->back();
    }
}
