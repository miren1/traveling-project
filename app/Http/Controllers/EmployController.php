<?php

namespace App\Http\Controllers;

use App\User;
use App\Setting;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Auth;
use AdminHelper;

class EmployController extends Controller
{
    public function show(Request $request)
    {
        // dd($request->all());
        if ($request->ajax()) {
            $data = User::where('role', 'user')->whereNull('leave_date')->get();
            // return $data;

            return DataTables::of($data)
                ->make(true);
        }
    }

    public function StatusMail(Request $request)
    {
        // dd($request->all());
        $emp = User::where('id',base64_decode($request->id))->first();
        
        if($request->status=="Approve")
        {
            $data['status'] = "Approved";
            $data['login_link'] = url('/login');
            $update = array(
                'send_employment'=>0,
                'employee_approved'=>1
            );
        }
        else
        {
            $data['status'] = "Rejected";
            $update = array(
                'send_employment'=>0,
                'employee_approved'=>2
            );
        }
        $change = User::where('id',$emp->id)->update($update);
        if($change)
        {
            $body = view('mail.employeeStatus',$data)->render();
            // return $body;
            $arr = $emp->email;
            AdminHelper::sendEmailTo($body,'Employeement '.$data['status'],$arr);
            notify()->success('Employee '.$data['status'].' !');
        }
        else
        {
            notify()->error('Something Wrong Try Again..!');
        }

        return redirect()->route('adminshowemployee');
    }
}
