<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;

use Auth;
use App\Event;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Storage;

class EventController extends Controller
{
    public function index()
    {   
        return view('admin.event.list');
    }

    public function add()
    {
        return view('admin.event.add');
    }

    public function store(Request $request)
    {
        if($request->image){
            if($request->hasFile('image')){
                $file = $request->image;
                $filename = time() . '.' . $file->getClientOriginalExtension();
                $file->move('uploads/events', $filename);
            }
            $url = 'uploads/events/'.$filename;
        }else{
            $img = Event::find($request->id);
            $url = $img->image;
        }
        if($request->id){
            $data = array(
                'title'=>$request->title,
                'startdate'=>$request->startdate,
                'enddate'=>$request->enddate,
                'duration'=>$request->duration,
                'location_route'=>$request->location_route,
                'content'=>$request->content,
                'price'=>$request->price,
                'discount_offer'=>$request->discount_offer,
                'image'=>$url
            );
            //dd($data);
            $where = array('id'=>$request->id);
            $isUpdated = Event::where($where)->update($data);

            if($isUpdated){
                notify()->success('Event updated successfully done');
                return redirect('event');
            }else{
                notify()->error('Something went wrong please try again!');  
                return redirect('event');
            }

        }else{
            $event =New Event();
            $event->title =$request->title;
            $event->startdate =$request->startdate;
            $event->enddate =$request->enddate;
            $event->duration =$request->duration;
            $event->location_route =$request->location_route;
            $event->content =$request->content;
            $event->price =$request->price;
            $event->discount_offer =$request->discount_offer;
            $event->image =$url;
            $save = $event->save();
            if($save){
                notify()->success('Event added successfully done');
                return redirect('event');
            }else{
                notify()->error('Something went wrong please try again!');  
                return redirect('event');
            }
        }    
    }

    public function list(Request $request)
    {
        if (Auth::user()->role =='admin') {
            return datatables()->of(Event::where('deleted_at','0')->get())->toJson();
        }
    }

    public function edit($id)
    {
        
        $id = base64_decode($id);
        $event = Event::find($id);
        $event['url'] = "http://$_SERVER[HTTP_HOST]";
        
        return view('admin.event.add',compact('event'));
    }

    public function destroy($id)
    { 
        $deleted = Event::where('id',$id)->update(['deleted_at'=>'1']);
        
            if ($deleted) {
                $status = true;
            }else{
                $status = false;
            }
            return response()->json($status);
    }
}
