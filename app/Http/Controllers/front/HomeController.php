<?php

namespace App\Http\Controllers\front;

use Auth;
use App\Blog;
use App\Slider;
use App\Review;
use App\Career;
use App\Ads_blog;
use Carbon\Carbon;
use App\Packagtype;
use App\Announcement;
use App\Tour_gallery;
use App\AddMembership;
use App\Gallery_images;
use Carbon\CarbonInterval;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function index()
    {
        $data['sliders'] = Slider::get();
        // dd($data);
        $data['categories'] = Packagtype::where('deleted_at',1)->get();
        return view('front.index')->with($data);
    }
    
    public function AboutUs()
    {
        return view('front.other.about-us');
    }

    public function Blog()
    {
        return view('front.other.blog');
    }

    public function Gallery()
    {
        return view('front.other.gallery');
    }

    public function FAQ()
    {
        return view('front.other.FAQ');
    }

    public function TermsCondition()
    {
        return view('front.other.terms-condition');
    }

    public function PrivacyPolicy()
    {
        return view('front.other.privacy-policy');
    }

    public function MembershipIndex()
    {
        $member = AddMembership::all();
        return view('front.membership_detail',compact('member'));
    }

    public function getAllMember()
    {   
        
        if(Auth::user()){

            return datatables()->of(AddMembership::all())->toJson();

        }else if(Auth::guard('webagent')->check()){
            $user_id = Auth::guard('webagent')->user()->id;
            //return datatables()->of(AddMembership::where('user_id',$user_id)->get())->toJson();

            return datatables()->of(AddMembership::with('plan')->where('agent_id',$user_id)->get())->toJson();

        }else if(Auth::guard('webcustomer')->check()){
            $user_id = Auth::guard('webcustomer')->user()->id;
            return datatables()->of(AddMembership::with('plan')->where('customer_id',$user_id)->get())->toJson();

        }else if(Auth::guard('webmember')->check()){
            $user_id = Auth::guard('webmember')->user()->id;
            return datatables()->of(AddMembership::with('plan')->where('member_id',$user_id)->get())->toJson();
        }
    }

    public function Careers()
    {
        $data['career'] = Career::where('deleted_at', '0')->get();
     // dd($data);      
// 
        return view('front.careers',$data);
    }

    public function MembershipPlandetail()
    {
        return view('front.membership_plan_details');
    }

    public function check_flight(Request $request)
    {
        // TO generate TOKEN
        // dd('miren');
        $data = array("username"=>"jagjoyu_ticket","password"=>"amFnal85NzI1");
        $list = json_encode($data);
 
        // Prepare new cURL resource
        $ch = curl_init('https://stagingapi.aireo.in/tsapi/auth');
        // dd($ch);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $list);
        // dd($ch);
        // Set HTTP Header for POST request 
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($list))
        );
       
        $result = curl_exec($ch);
        dd($result);
        if($result == false){
            notify()->error('Something went wrong please try again!');  
                return redirect('/');
        }
        $result = json_decode($result , true);
        curl_close($ch);
        // End generate TOKEN

        //Start Check Flight availability
        $flight_list['data']['fromCity'] = $request->from;
        $flight_list['data']['toCity'] = $request->to;
        $flight_list['data']['leaveDate'] = $request->departing;
        $flight_list['data']['returnDate'] = $request->returning;
        $flight_list['data']['adult'] = $request->adult;
        $flight_list['data']['child'] = $request->child;

        $data = array("fromCity"=>$request->from,"toCity"=>$request->to,"leaveDate"=>$request->departing,"returnDate"=>$request->returning,"adult"=>$request->adult,"child"=>$request->child,"infant"=>"0","airline"=>"","class"=>"","fareType"=>$request->fare_type);
        $list = json_encode($data);
        // dd($list);
        // Prepare new cURL resource
        $ch = curl_init('https://stagingapi.aireo.in/tsapi/air/getAvailability');
        dd($ch);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $list);
         
        // Set HTTP Header for POST request 
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Authorization: Bearer ' . $result['data']['token'])
            //'Content-Length: ' . strlen($list))
        );
        
        $flight_list = curl_exec($ch);

        curl_close($ch);
        
        $flight_list = json_decode($flight_list, true);
        $flight_list['data']['fromCity'] = $request->from;
        $flight_list['data']['toCity'] = $request->to;
        $flight_list['data']['leaveDate'] = $request->departing;
        $flight_list['data']['returnDate'] = $request->returning;
        $flight_list['data']['adult'] = $request->adult;
        $flight_list['data']['child'] = $request->child;

        return view('front.flight-list-details', $flight_list['data'] );
    }

    public function flight_list(){
        return view('front.flight-list-details');
    }

    public function new_index(){

        $data['review'] = Review::with(["member","customer","review_image" => function($q){
            $q->where('image_select', '=', 'selected');
        }])->where('status','show')->where('deleted_at','0')->get();
        // dd($data);

        $data['url'] = "http://$_SERVER[HTTP_HOST]";

        $data['announcement'] = Announcement::where('all_user','1')->where('deleted_at','0')->first();
        $data['packagType'] =Packagtype::where('deleted_at','1')->get();
        $data['sliders'] = Slider::get();
        // dd($data);
        $data['categories'] = Packagtype::where('deleted_at',1)->get();
        // dd($data['announcement']);
        return view('front.index-demo',$data);
    }

    public function packages_demo(){
        return view('front.packages-demo');
    }

    public function intinerary_demo(){
        return view('front.itinerary-demo');
    }

    public function membership_demo(){
        return view('front.membership-demo');
    }

    public function about_us_demo(){
        return view('front.about-us-demo');
    }

    public function new_blog(){

        $data['blog'] = Blog::with('blogs_ads','blogs_media')->where('deleted_at','0')->get();
        //dd($data['blog']);
        $data['url'] = "http://$_SERVER[HTTP_HOST]";
        return view('front.new-blog', $data);
    }

    public function blog_details($slug)
    {
        $blog = Blog::with('blogs_ads','blogs_media')->where('slug',$slug)->where('deleted_at','0')->first();
        // dd($blog);
        if ($blog == null) {
            return redirect()->route('index');
        }
        $ads = Ads_blog::with('allads')->where('blog_id',$blog->id)->get();
        // dd($ads);
        // dd($data);
        $url = "http://$_SERVER[HTTP_HOST]";
        //dd($data);
        return view('front.blog_details', compact('blog','ads','url'));
    }

    public function gallery_design(Request $request)
    {

        $dt = Carbon::now();
        $image = Tour_gallery::with('gallery_image')->where('deleted_at','0')->where('startdate','<=',$dt)->where('enddate','>=',$dt)->orderBy('created_at', 'desc')->get();
        $url = "http://$_SERVER[HTTP_HOST]";
        //dd($image);
        return view('front.new_gallery',compact('image','url'));
    }

    public function all_images($slug)
    {
        //$data['image'] = Gallery_images::where('slug',$slug)->where('deleted_at','0')->get();
        $s  = $slug;
        $data['image'] = Tour_gallery::with('gallery_image')->
        where('slug',$slug)->where('deleted_at','0')->get();

        //dd($data['image']);
        $data['url'] = "http://$_SERVER[HTTP_HOST]";
        return view('front.all_image',$data);
    }

    public function new_review(Request $request)
    {
        $data['review'] = Review::with(["member","user","review_image" => function($q){
            $q->where('image_select', '=', 'selected');
        }])->where('status','show')->where('deleted_at','0')->get();

        $data['url'] = "http://$_SERVER[HTTP_HOST]";
        //dd($data);
        return view('front.new-review',$data);
    }

}
