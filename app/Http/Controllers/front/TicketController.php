<?php

namespace App\Http\Controllers\front;

use App\Http\Controllers\Controller;

use DB;
use Auth;
use App\Agent;
use App\Ticket;
use App\Agent_fee;
use Carbon\Carbon;
use App\Ticketmessage;
use App\Ticket_category;
use Illuminate\Http\Request;
use App\Ticket_sub_category;
use App\Ticket_category_assignment;
use Illuminate\Support\Facades\Redirect;
use LoveyCom\CashFree\PaymentGateway\Order;
use App\Announcement;

class TicketController extends Controller
{
  public function index()
  {
    if (Auth::guard('webagent')->user()) {
      $where = array('agent' => '1');
    } else if (Auth::guard('webcustomer')->user()) {
      $where = array('user' => '1');
    } else {

      $where = array('member' => '1');
    }
      $data['ticket_sub'] = Ticket_sub_category::with('ticket_categories')->where($where)->get();
      $ticket_cat_id = array_column($data['ticket_sub']->toArray(), 'ticket_cat_id');
      $data['ticket_category'] = Ticket_category::whereIn('id', $ticket_cat_id)->get();

      if(Auth::guard('webcustomer')->user()){
          $data['user'] = Auth::guard('webcustomer')->user();
      }else if(Auth::guard('webagent')->user()){
          $data['user'] = Auth::guard('webagent')->user();
      }else if(Auth::guard('webmember')->user()){
          $data['user'] = Auth::guard('webmember')->user();
      }
      //dd($data);
      $data['announcement'] = Announcement::where('agent','1')->where('deleted_at','0')->first();
    return view('front.ticket')->with($data);
  }

  public function view($id = '')
  {
    $data['ticket'] = Ticket::with(['user', 'ticket_categories', 'ticket_sub_categories'])->find(base64_decode($id));
    Ticketmessage::where(['ticket_id' => base64_decode($id), 'type' => 'send'])->update(['is_view' => '1']);
    $data['ticket_message_count'] = Ticketmessage::where(['ticket_id' => base64_decode($id), 'type' => 'send'])->count();

    return view('front.viewticket')->with($data);
  }


  public function getCat(Request $request)
  {
    $stateid = $request->cat_id;

    if (Auth::guard('webagent')->user()) {
      $where = array('agent' => '1','ticket_cat_id' => $stateid);
    } else if (Auth::guard('webcustomer')->user()) {
      $where = array('user' => '1','ticket_cat_id' => $stateid);
    } else {
      $where = array('member' => '1','ticket_cat_id' => $stateid);
    }

    $cities = Ticket_sub_category::where($where)->get();
    return response()->json([
      'cat' => $cities
    ]);
  }

  public function AllgetCat(Request $request)
  {

    $cat_id = $request->cat_id;
    
    $where = array('ticket_cat_id' => $cat_id);
    
    $cities = Ticket_sub_category::where($where)->get();


    //return  json_encode($data);
    return response()->json([
      'cat' => $cities
    ]);
  }


  public function store(Request $request)
  {

    $ticket_category_id = NUll;
    if ($request->category) {
      $ticket_category_id = $request->category;
    }
    $name = NULL;
    $file_type = NULL;
    if ($request->file('file')) {
      $file = $request->file;
      $extension = $file->getClientOriginalName();
      $name = strtotime('now') . '_' . $extension;
      $file_type = $file->getClientOriginalExtension();
      $file->move('public/ticket', $name);
    }
    $agent_id = null;
    $member_id = null;
    $customer_id = null;
    if (Auth::guard('webagent')->user()) {
      $agent_id = Auth::guard('webagent')->user()->id;
    } else if (Auth::guard('webcustomer')->user()) {
      $customer_id = Auth::guard('webcustomer')->user()->id;
    } else {
      $member_id = Auth::guard('webmember')->user()->id;
    }

    $dataFormater = array(
      'agent_id' => $agent_id,
      'member_id' => $member_id,
      'customer_id' => $customer_id,
      'ticket_category_id' => $ticket_category_id,
      'ticket_sub_category_id' => $request->sub_category,
      'file' => $name,
      'file_type' => $file_type,
      'message' => $request->message
    );

    $ticket = Ticket::create($dataFormater);

    if ($ticket) {
      notify()->success('Ticket Created Successfully !');
    } else {
      notify()->error('Something Wrong Try Again..!');
    }
    return redirect('/tickets');
  }

  public function list(Request $request)
  {

    if (Auth::guard('webagent')->user()) {
      $where = array('agent_id' => Auth::guard('webagent')->user()->id);
    } else if (Auth::guard('webcustomer')->user()) {
      $where = array('customer_id' => Auth::guard('webcustomer')->user()->id);
    } else {
      $where = array('member_id' => Auth::guard('webmember')->user()->id);
     // $where = array('member' => '1');
    }

    $Ticket = Ticket::with(['ticket_categories', 'ticket_sub_categories','lastmessage'])->where($where)->where(['is_delete' => '0'])->orderBy('created_at', 'desc');

    if (isset($request->category_id)) {
      $Ticket->whereHas('ticket_categories', function ($q) use ($request) {
        $q->where('id', $request->category_id);
      });
    }
    if (isset($request->sub_category_id)) {
      $Ticket->whereHas('ticket_sub_categories', function ($q) use ($request) {
        $q->where('id', $request->sub_category_id);
      });
    }
    if (isset($request->status)) {
      $Ticket->where(['status' => $request->status]);
    }

    if (isset($request->start_date) && isset($request->end_date)) {

      $start  =  Carbon::parse($request->start_date)->format('Y-m-d') . ' 00:00:00';
      $end   = Carbon::parse($request->end_date)->format('Y-m-d') . ' 23:59:59';
     
      $Ticket->whereBetween('created_at', [$start, $end]);
    }

    //dd($Ticket);
    return datatables()->of($Ticket)->toJson();
  }

  public function get_messages($id = null)
  {
    if ($id) {
      $where = array(
        'ticket_id' => base64_decode($id),
        'is_delete' => '0'
      );
      $data['ticket']  = Ticket::find(base64_decode($id));
      $data['message'] = Ticketmessage::with('user')->where($where)->orderBy('id', 'ASC')->get();
      return  json_encode($data);
    } else {
      $data = array();
      return  json_encode($data);
    }
  }

  public function getcount($id = null)
  {
    $data['msg_count'] = Ticketmessage::where(['ticket_id' => base64_decode($id), 'type' => 'receive'])->count();
    return  json_encode($data);
  }

  public function get_data(Request $request)
  {
  }
  public function send_message(Request $request)
  {

    if ($request->ticket_id && $request->message) {

      $Ticket = Ticket::find(base64_decode($request->ticket_id));
      $ticket_id = base64_decode($request->ticket_id);
      $ticket_category_id = NULL;
      $ticket_sub_category_id = NULL;

      if ($Ticket->ticket_category_id) {
        $ticket_category_id = $Ticket->ticket_category_id;
      }
      if ($Ticket->ticket_sub_category_id) {
        $ticket_sub_category_id = $Ticket->ticket_sub_category_id;
      }



      $name = NULL;
      $file_type = NULL;
      if ($request->file('media')) {
        $file = $request->media;
        $extension = $file->getClientOriginalName();
        $name = strtotime('now') . '_' . $extension;
        $file_type = $file->getClientOriginalExtension();
        $file->move('public/ticket', $name);
      }

      $sentData = array(
        //'user_id'=>Auth::user()->id, 
        'ticket_id' => $ticket_id,
        'ticket_category_id' => $ticket_category_id,
        'ticket_sub_category_id' => $ticket_sub_category_id,
        'type' => 'send',
        'message' => $request->message,
        'file' => $name,
        'file_type' => $file_type,

      );

      $send = Ticketmessage::create($sentData);
      if ($send) {
        $status = true;
      } else {
        $status = false;
      }
    } else {
      $status = false;
    }
    return json_encode($status);
  }

  public function list_info(Type $var = null)
  {
    return view();
  }

  public function topup(Request $request)
  {
      
      $id = base64_encode($request->agent_id);

      $type = 'recharge';
      $ReturnUrl = url('/topup-return-url/'.$id.'/'.$type);
      $NotifyUrl = url('/payment_notifyURL');
      $orderId = time();
      $order = new Order();
      
      $od["orderId"] = $orderId;
      $od["orderAmount"] = $request->amount;
      $od["orderNote"] = $request->note;
      $od["customerPhone"] = $request->phone_number;
      $od["customerName"] = $request->agent_name;
      $od["customerEmail"] = $request->email;
      $od["returnUrl"] = $ReturnUrl;
      $od["notifyUrl"] = $NotifyUrl;
      
      $order->create($od);
      $link[] = '';
      $link = $order->getLink($od['orderId']);
      //dd($link);
      return Redirect::to($link->paymentLink);
  }

  public function TopUpReturnUrl(Request $request, $id, $type)
  {
      
      if($request->txStatus == "CANCELLED"){

        notify()->success('You Cancelled transaction please try again'); 
        return redirect('tickets'); 
      }

      $id = base64_decode($id);
      
      if($id){
          $agent_data = Agent::where('id',$id)->first();

          $exist_data = Agent_fee::where('agent_id',$id)->first();

          if($agent_data){
            $total_amount = $agent_data->wallet + $request->orderAmount;
            $where2 =  array('id'=>$id);
            $wallet = array('wallet'=>$total_amount);
            $save_fee = Agent::where($where2)->update($wallet);
          }
      }
      
      if($request){
          if($exist_data){
              
              $data = array(
                  'payment_id' => $request->orderId,
                  'payment_amount' => $request->orderAmount,
                  'reference_id' => $request->referenceId,
                  'tx_status' => $request->txStatus,
                  'payment_mode' => $request->paymentMode,
                  'tx_msg' => $request->txMsg,
                  'tx_time' => $request->txTime,
                  'signature' => $request->signature,
                  'type' => $type
              );
              try{

                  $where =  array('agent_id'=>$id);
                  $save_recharge = Agent_fee::where($where)->update($data);

                  $fee = new Agent_Fee;
                  $fee->agent_id = $id;
                  $fee->payment_id = $request->orderId;
                  $fee->payment_amount = $request->orderAmount;
                  $fee->reference_id = $request->referenceId;
                  $fee->tx_status = $request->txStatus;
                  $fee->payment_mode = $request->paymentMode;
                  $fee->tx_msg = $request->txMsg;
                  $fee->tx_time = $request->txTime;
                  $fee->signature = $request->signature;
                  $fee->type = $type;
                  
                  $save_fee = $fee->save();

                  if($save_fee){
                      
                      if($fee->tx_status == 'SUCCESS'){
                          notify()->success('Your wallet reacharge succ done..!');
                          return redirect('/tickets');
                      }else{
                          notify()->error('Somthing went wrong please try again'); 
                          return redirect('/tickets');    
                      }

                  }else{
                          notify()->error('Somthing went wrong please try again');     
                          return redirect('/tickets');
                  }
                  
              } catch (\Exception $e) {
                  notify()->error($e->getMessage());    
                  return redirect('/tickets');
              }

          }else{
              
              try{

                    $fee = new Agent_Fee;
                    $fee->agent_id = $id;
                    $fee->payment_id = $request->orderId;
                    $fee->payment_amount = $request->orderAmount;
                    $fee->reference_id = $request->referenceId;
                    $fee->tx_status = $request->txStatus;
                    $fee->payment_mode = $request->paymentMode;
                    $fee->tx_msg = $request->txMsg;
                    $fee->tx_time = $request->txTime;
                    $fee->signature = $request->signature;
                    $fee->type = $type;

                    $save_fee = $fee->save();

                  if($save_fee){
                      if($fee->tx_status == 'SUCCESS'){
                          notify()->success('Your wallet reacharge succ done..!');
                          return redirect('/tickets');
                      }else{
                          notify()->error('Somthing went wrong please try again'); 
                          return redirect('/tickets');    
                      }
                  }else{
                          notify()->error('Somthing went wrong please try again');     
                          return redirect('/tickets');
                  }

              }catch(\Exception $e){
          
                  notify()->error($e->getMessage());    
                  return redirect('/tickets');
              }
              
          }
         
      }

    }

}
