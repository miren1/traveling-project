<?php

namespace App\Http\Controllers\front;

use App\Toure;
use App\Gallery;
use App\Packagtype;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;



class TourController extends Controller
{
    public function index($slug=null)
    {
        if($slug!=null){
            
            $data['packagtype'] = Packagtype::where('slug',$slug)->where('deleted_at','1')->first();
            return view('front.tours-plan')->with($data);
        }else{
            return redirect('/');
        }
    }

    public function getTourType(Request $request)
    {
        $Packagtype=Packagtype::where('deleted_at','1')->get();
        return response()->json($Packagtype);
    }

    public function getTours(Request $request)
    {
        $array =array('package_type'=>$request->id);
        if($request->name){
            $array['name']=$request->name;
        }
        //dd($array);
        $Toure =Toure::where($array)->get();
        return response()->json($Toure);
    }


    public function view($slug=null)
    {
        
        $data['toure']= Toure::with(['pack','itinerary'])->where('slug',$slug)->first();
        //dd($data['toure']);
        $data['gallery']= Gallery::where('slug',$slug)->get();
        
        $data['overview']= json_decode($data['toure']->small_info,true);
        if($data['toure']){    
            $data['detail1'] =  json_decode($data['toure']->detail1); 
            $data['detail2'] =  json_decode($data['toure']->detail2); 
            $data['detail3'] =  json_decode($data['toure']->detail3); 
            $data['detail4'] =  json_decode($data['toure']->detail4);
        } 
        //dd($data['overview']);
        return view('front.tours-view')->with($data);
    }
}
