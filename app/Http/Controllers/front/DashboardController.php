<?php

namespace App\Http\Controllers\front;

use Auth;
use App\Agent;
use App\AddMembership;
use App\Customer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use LoveyCom\CashFree\PaymentGateway\Order;
use App\Announcement;

class DashboardController extends Controller
{
    public function index()
    {
        $users = "";
        $agent = "";
        $member = "";
        if(Auth::guard('webcustomer')->user()){
            // dd("hii");
            $users = Auth::guard('webcustomer')->user();
        }else if(Auth::guard('webagent')->user()){
            $agent = Auth::guard('webagent')->user();
        }else if(Auth::guard('webmember')->user()){
            $member = Auth::guard('webmember')->user();
        }
    
        // $announcement = Announcement::where('member','1')->where('deleted_at','0')->first();
        if($users != ""){

            $announcement = Announcement::where('user','1')->where('deleted_at','0')->first();
            $user = $users;
        }elseif ($agent != "") {
            $announcement = Announcement::where('agent','1')->where('deleted_at','0')->first();
            # code...
             $user = $agent;
        }elseif ($member != "") {
            $announcement = Announcement::where('member','1')->where('deleted_at','0')->first();
            # code...
             $user = $member;
        }
        // dd($announcement);
        return view('front.dashboard',compact('user','announcement'));
    }

    public function update_profile(Request $request)
    {
        //dd($request->all());
        if($request->file('profile'))
        {   

            if($request->hasFile('profile')){
                $file = $request->profile;
                $filename = time() . '.' . $file->getClientOriginalExtension();
                $file->move('public/profile', $filename);
            }
        
            $update_profile = [
                'profile'=> 'public/profile/'.$filename,
            ];
            
            if($request->type=="agent"){
                $user_id = Agent::where('id',$request->id)->first();
            }elseif($request->type=="member"){
                $user_id = AddMembership::where('id',$request->id)->first();
            }else{
                $user_id = Customer::where('id',$request->id)->first();
            }   
            //dd($user_id);
            if($user_id)
            {
                //update query for update data

                if($request->type=="agent"){
                    $update = Agent::where('id',$request->id)->update($update_profile);  
                }elseif($request->type=="member"){
                    $update = AddMembership::where('id',$request->id)->update($update_profile);  
                }else{
                    $update = Customer::where('id',$request->id)->update($update_profile);  
                }   
               // $update = Agent::where('id',$request->id)->update($update_profile);  
                if($update)
                {
                    notify()->success('Your profile image successfully done.');
                }else{
                    notify()->error('Something went wrong please try again.!');
                }
            }
            return redirect('/dashboard');

        }else{
            notify()->error('Please select image!'); 
            return redirect('/dashboard');           
        }

    }

    public function reviews()
    {
        if(Auth::guard('webcustomer')->user()){
            $user = Auth::guard('webcustomer')->user();
        }else if(Auth::guard('webagent')->user()){
            $user = Auth::guard('webagent')->user();
        }else if(Auth::guard('webmember')->user()){
            $user = Auth::guard('webmember')->user();
        }
        //dd($user);

        return view('front.reviews');
    }
}
