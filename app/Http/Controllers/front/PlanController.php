<?php

namespace App\Http\Controllers\front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Plans;
use Illuminate\Support\Facades\Auth;


class PlanController extends Controller
{
    public function index()
    {
        $data['silver']=Plans::where('type','silver')->where('is_delete','0')->orderBy('years', 'ASC')->get();
        $data['gold']=Plans::where('type','gold')->where('is_delete','0')->orderBy('years', 'ASC')->get();
        $data['platinum']=Plans::where('type','platinum')->where('is_delete','0')->orderBy('years', 'ASC')->get();
        // dd($data['gold']);
        return view('front.membership_plan',$data);
    }


    public function info($type='')
    {
        if($type=='gold'){
            $data['name']='Gold';
            $data['gold']=Plans::where('type','gold')->where('is_delete','0')->orderBy('years', 'ASC')->get();
        }else if($type=='silver'){
            $data['name']='Silver';
            $data['silver']=Plans::where('type','silver')->where('is_delete','0')->orderBy('years', 'ASC')->get();
        }else if($type=='platinum'){
            $data['name']='Platinum';
            $data['platinum']=Plans::where('type','platinum')->where('is_delete','0')->orderBy('years', 'ASC')->get();
        }
        return view('front.membership_info')->with($data);
    }

    public function user_become_member($id, $type){
       // dd($id);
        $id = base64_decode($id);
        $plan=Plans::where('id', $id)->where('type',$type)->where('is_delete','0')->first();

        if(Auth::guard('webcustomer')->user()){
            $user = Auth::guard('webcustomer')->user();
        }
        //dd($plan);

        return view('employee.member.user_become_member',compact('plan','user'));
    }

}
