<?php

namespace App\Http\Controllers\front;

use DB;
use App\User;
use App\Plans;
use App\Agent;
use AdminHelper;
use App\Customer;
use App\Agent_fee;
use Carbon\Carbon;
use App\Membership;
use App\AddMembership;
use App\Membership_Fee;
use Illuminate\Http\Request;
use App\Mandate_registration;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use LoveyCom\CashFree\PaymentGateway\Order;

class MemberController extends Controller
{
    public function BecomeMember($id)
    {
        
        $plan = Plans::where('id',base64_decode($id))->where('is_delete','0')->first();
        
        if(Auth::guard('webagent')->check()){
            $user = Auth::guard('webagent')->user();
        }else if(Auth::guard('webcustomer')->check()){
            $user = Auth::guard('webcustomer')->user();
        }else if(Auth::guard('webmember')->check()){
            $user = Auth::guard('webmember')->user();
        }
        //return view('front.become-member',compact('plan','user'));
        return view('front.invite_member',compact('plan','user'));
    }

    public function Store(Request $request)
    {   
        //dd($request->all());
        if($request->hasFile('photo')){
            $file = $request->photo;
            $filename = time() . '.' . $file->getClientOriginalExtension();
            $file->move('uploads/member_image/', $filename);

            $filename = 'uploads/member_image/'.$filename;
        }else{
            $filename="null";
        }
        
        $prefix = $request->prefix;
        for ($i = 0; $i < count($prefix); $i++){
            
            if($i == 0){

                try{
                    $data = array(
                        'plan_id'=>$request->plan_id,
                        'prefix'=>$prefix[$i],
                        'name'=>$request->name[$i],
                        'dob'=>$request->dob[$i],
                        'adharno'=>$request->adharno[$i],
                        'panno'=>$request->panno[$i],
                        'passportno'=>$request->passportno[$i],
                        'passportdate'=>$request->passportdate[$i],
                        'occupation'=>$request->occupation[$i],
                        'email'=>$request->contact_email,
                        'annual_income'=>$request->annual_income,
                        'primary_contact'=>$request->primary_contact,
                        'secondary_contact'=>$request->secondary_contact,
                        'whatsapp_number'=>$request->whatsapp_number,
                        'resi_address'=>$request->resi_address,
                        'status'=>'invite',
                        'city'=>$request->city,
                        'state'=>$request->state,
                        'pincode'=>$request->pincode,
                        'ref_id'=>$request->ref_id,
                        'jj_tip'=>$request->exampleRadios,
                        'password'=>Hash::make($request->password),
                        'profile'=>$filename,
                    );
                    
                    if($request->user_store == "yes"){
                        $result = AddMembership::create($data)->id;

                        $last_id = $result;

                        /*if($last_id){
                            Customer::destroy($request->id);
                        }
                        */
                    }else{

                        if($request->id){
                        $result = AddMembership::where('id', $request->id)->update($data);
                        }else{
                            $result = AddMembership::create($data)->id;
                            $last_id = $result;
                        }
  
                    }
    
                }catch (\Exception $e){
                    notify()->error($e->getMessage());     
                    return redirect(url('membership-plans'));
                }

            }else{
                
                try{
                    $data = array(
                        'plan_id'=>$request->plan_id,
                        'member_id'=>$last_id,
                        'prefix'=>$prefix[$i],
                        'name'=>$request->name[$i],
                        'dob'=>$request->dob[$i],
                        'adharno'=>$request->adharno[$i],
                        'panno'=>$request->panno[$i],
                        'passportno'=>$request->passportno[$i],
                        'passportdate'=>$request->passportdate[$i],
                        'occupation'=>$request->occupation[$i],
                        'email'=>$last_id,
                        'annual_income'=>$last_id,
                        'primary_contact'=>$last_id,
                        'secondary_contact'=>$last_id,
                        'whatsapp_number'=>$last_id,
                        'resi_address'=>$last_id,
                        'status'=>$last_id,
                        'city'=>$last_id,
                        'state'=>$last_id,
                        'pincode'=>$last_id,
                        'exampleRadios'=>$last_id,
                        'ref_id'=>$last_id,
                        'password'=>$last_id,
                        'jj_tip'=>$last_id,
                        'profile'=>$last_id,
                    );
                    
                    if($request->user_store == "yes"){
                        $result = AddMembership::create($data)->id;
                        $last_id = $result;

                        /*if($last_id){
                            Customer::destroy($request->id);
                        }*/
                        
                    }else{

                        if($request->id){
                        $result = AddMembership::where('id', $request->id)->update($data);
                        }else{
                            $member = AddMembership::create($data)->id;
                        }
  
                    }
                   
                    if($member){
                        notify()->success('Member Added Successfully !');
                    }else{
                        notify()->error('Something Wrong Try Again..!');
                    }  

                }catch (\Exception $e){
                    notify()->error($e->getMessage());     
                    return redirect(route('membership-plans'));
                }        
            }

// Start UPI Payment mode section   
            if($request->paymentmode == "upi"){
                //dd($request->all());
                $collectByDate = Carbon::now()->addMinutes(5)->format('d/m/Y H:i A');
                 
                $data = [
                        
						"payerVa" => $request->bankupi,  // production pe user's vpa
						//"amount" => '1.00', // shoul be always 2 decimal
                        "amount" => $request->plan_amount, // shoul be always 2 decimal
                        "note" => $request->note,
                        //"collectByDate" => date('d/m/Y H:i A', strtotime('+5 minutes')),
                        "collectByDate" => $collectByDate,//now + 15min,
                        "merchantId" => '422421', //will provide by bank,
                        "subMerchantId" => '422421', // can send random number or merchantId
                        "subMerchantName" => 'jagjoyu', //can send test
                        "merchantName"  => 'jagjoyu', 
                        'terminalId'    => '5411',
                        "merchantTranId"    => 'MR'.time(), //should be unique every time.
                        "billNumber"        => 'BN'.time()  //should be unique every time.
                    ];
                    //dd($data);
                    $filepath=fopen($_SERVER['DOCUMENT_ROOT']."/ICICIPubliccert/rsa_apikey.pem","r");

                    $pub_key_string=fread($filepath,8192);	

                    fclose($filepath);

                    openssl_get_publickey($pub_key_string);

                    openssl_public_encrypt(json_encode($data),$crypttext,$pub_key_string); 

                    $encryptedRequest = json_encode(base64_encode($crypttext));

					$header = ['Content-type:text/plain'];

                    $httpUrl = 'https://apibankingone.icicibank.com/api/MerchantAPI/UPI/v0/CollectPay2/422421';

                    $logfile = $_SERVER['DOCUMENT_ROOT'].'/logs/ciblog.txt';  // folder need to be created but file will be created auto
				    //dd($logfile);
                    $log = "\n\n".'GUID - '.time()."==============CollectPay2=============\n";
					$log = "\n\n".'GUID - '.time()."==============".date('d-m-y h:i')."=============\n";
                    $log .= 'URL - '.$httpUrl."\n\n";
                    $log .= 'HEADER - '.json_encode($header)."\n\n";
                    $log .= 'REQUEST - '.json_encode($data)."\n\n";
                    $log .= 'REQUEST ENCRYPTED - '.json_encode($encryptedRequest)."\n\n";
					
                    file_put_contents($logfile, $log, FILE_APPEND | LOCK_EX);
                    
                    $curl = curl_init();

                    curl_setopt_array($curl, array(
                        CURLOPT_URL => $httpUrl,
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 60,
                        CURLOPT_CUSTOMREQUEST => 'POST',
                        CURLOPT_POSTFIELDS => $encryptedRequest,
                        CURLOPT_HTTPHEADER => $header
                    ));
                    
                    $raw_response = curl_exec($curl);
					
                    curl_close($curl);

                    $fp= fopen($_SERVER['DOCUMENT_ROOT']."/ICICIPubliccert/private_key.txt","r"); // it can be in any format				
                   	$priv_key_string=fread($fp,8192);
					
					fclose($fp);
					$private_key = openssl_pkey_get_private($priv_key_string, "");
					
					openssl_private_decrypt(base64_decode($raw_response), $response, $private_key); 

                    $log = "\n\n"."NOTE : ==============Collect Response=============\n";
					$log = "\n\n".'GUID - '.time()."==============".date('d-m-y h:i')."=============\n";
                    $log .= "URL - ".$httpUrl." \n";
                    $log .= "\n".'RESPONSE - '.json_encode($raw_response)."\n\n";
                    $log .= "RESPONSE DECRYPTED - ".$response."\n\n";
					
                    file_put_contents($logfile, $log, FILE_APPEND | LOCK_EX);
					
                    $output = json_decode($response);
                    //dd($output);
                    $date = date('Y-m-d H:i:s');
                    $id = base64_encode($request->id);

                    if($request->user_store == "yes"){
                        $member_id = $last_id;
                    }else{
                        $member_id = $request->id;
                    }
                    
                    try{
                        $data = array(
                            'member_id'=>$member_id,
                            'payerVa'=>$request->bankupi,
                            'collectByDate'=>$collectByDate,
                            'payment_amount'=>$request->plan_amount,
                            //'payment_amount'=>'1.00',
                            'note'=>$request->note,
                            'merchantId'=>$output->merchantId,
                            'terminalId'=>$output->terminalId,
                            'tx_status'=>$output->success,
                            'tx_msg'=>$output->message,
                            'tx_time'=>$date,
                            'merchantTranId'=>$output->merchantTranId,
                            'BankRRN'=>$output->BankRRN,
                        );
                            $result = Membership_Fee::create($data);                        
                    }catch (\Exception $e){
                        notify()->error($e->getMessage());     
                        return redirect(url('membership-plans'));
                    }
                    
//===================================================================================================
                    
                    if($output->success == 'true'){
                        
                            $data = [
                                "merchantId" => '422421', //will provide by bank,
                                "merchantTranId" => $output->merchantTranId, // can send random number or merchantId
                                "subMerchantId" => '422421', //can send test
                                'terminalId'    => '5411',
                            ];
                        
                            $filepath=fopen($_SERVER['DOCUMENT_ROOT']."/ICICIPubliccert/rsa_apikey.pem","r");
                            
                            $pub_key_string=fread($filepath,8192);  

                            fclose($filepath);

                            openssl_get_publickey($pub_key_string);

                            openssl_public_encrypt(json_encode($data),$crypttext,$pub_key_string); 

                            $encryptedRequest = json_encode(base64_encode($crypttext));
                            
                            $header = ['Content-type:text/plain'];

                            $httpUrl = 'https://apibankingone.icicibank.com/api/MerchantAPI/UPI/v0/TransactionStatus3/422421';

                            $logfile = $_SERVER['DOCUMENT_ROOT'].'/logs/ciblog.txt';  // folder need to be created but file will be created auto
                            
                            $log = "\n\n".'GUID - '.time()."==============CollectPay2=============\n";
                            $log = "\n\n".'GUID - '.time()."==============".date('d-m-y h:i')."=============\n";
                            $log .= 'URL - '.$httpUrl."\n\n";
                            $log .= 'HEADER - '.json_encode($header)."\n\n";
                            $log .= 'REQUEST - '.json_encode($data)."\n\n";
                            $log .= 'REQUEST ENCRYPTED - '.json_encode($encryptedRequest)."\n\n";
                            
                            file_put_contents($logfile, $log, FILE_APPEND | LOCK_EX);
                            
                            $curl = curl_init();
                            
                            curl_setopt_array($curl, array(
                                CURLOPT_URL => $httpUrl,
                                CURLOPT_RETURNTRANSFER => true,
                                CURLOPT_MAXREDIRS => 10,
                                CURLOPT_TIMEOUT => 60,
                                CURLOPT_CUSTOMREQUEST => 'POST',
                                CURLOPT_POSTFIELDS => $encryptedRequest,
                                CURLOPT_HTTPHEADER => $header
                            ));
                            
                            $raw_response = curl_exec($curl);
                            
                            curl_close($curl);

                            $fp= fopen($_SERVER['DOCUMENT_ROOT']."/ICICIPubliccert/private_key.txt","r"); // it can be in any format                
                            $priv_key_string=fread($fp,8192);
                            
                            fclose($fp);
                            $private_key = openssl_pkey_get_private($priv_key_string, "");
                            
                            openssl_private_decrypt(base64_decode($raw_response), $response, $private_key); 
                            //dd($response);
                            $log = "\n\n"."NOTE : ==============Collect Response=============\n";
                            $log = "\n\n".'GUID - '.time()."==============".date('d-m-y h:i')."=============\n";
                            $log .= "URL - ".$httpUrl." \n";
                            $log .= "\n".'RESPONSE - '.json_encode($raw_response)."\n\n";
                            $log .= "RESPONSE DECRYPTED - ".$response."\n\n";
                            
                            file_put_contents($logfile, $log, FILE_APPEND | LOCK_EX);
                            
                            $Trans_output = json_decode($response);

                            $date = date('Y-m-d H:i:s');
                            try{
                                
                                $data = array(
                                    'tx_status'=>$Trans_output->status,
                                    'tx_msg'=>$Trans_output->message,
                                    'tx_time'=>$date,
                                );

                                $where =  array('merchantTranId'=>$Trans_output->merchantTranId);
                                $save_fee = Membership_Fee::where($where)->update($data);
                                if($save_fee){
                                    notify()->success('Congratulations, You will get UPI request in your bank app please accept for payment');
                                    
                                    if($request->user_store == "yes"){
                                        $id = base64_encode($last_id);
                                    }else{
                                        $id = base64_encode($request->id);
                                    }
                                    
                                    return redirect(url('/mandate/'.$id));

                                }else{
                                    notify()->success('Something went wrong please try again.');
                                    return redirect(url('login'));
                                }
                                                                
                            }catch (\Exception $e){
                                notify()->error($e->getMessage());     
                                return redirect(url('login'));
                            }
                    }else{
                        
                        notify()->error("Something went wrong plase try again");     
                        return redirect(route('login'));
                    }

//==================Request for QR code generation===========================================//
//==================Request for QR code generation===========================================//

                   /*$QRdata = [
                        "merchantId" => '422421', //will provide by bank,
                        "amount" => '1.00', // shoul be always 2 decimal
                        "merchantTranId"    => 'MR'.time(), //should be unique every time.
                        "billNumber"        => 'BN'.time(),  //should be unique every time.
                        'terminalId'    => '5411'
                    ];

                    $QRfilepath=fopen($_SERVER['DOCUMENT_ROOT']."/ICICIPubliccert/rsa_apikey.pem","r"); 
                    $QRpub_key_string=fread($QRfilepath,8192);  

                    fclose($QRfilepath);

                    openssl_get_publickey($QRpub_key_string);

                    openssl_public_encrypt(json_encode($QRdata),$QRcrypttext,$QRpub_key_string); 

                    $QRencryptedRequest = json_encode(base64_encode($QRcrypttext));

                    $QRheader = ['Content-type:text/plain'];

                    $QRhttpUrl = 'https://apibankingone.icicibank.com/api/MerchantAPI/UPI/v0/QR/422421';

                    $QRlogfile = $_SERVER['DOCUMENT_ROOT'].'/logs/QRlog.txt';  // folder need to be created but file will be created auto
                
                    $QRlog = "\n\n".'GUID - '.time()."=============".date('d-m-y h:i')."=============\n";
                    $QRlog .= 'URL - '.$QRhttpUrl."\n\n";
                    $QRlog .= 'HEADER - '.json_encode($QRheader)."\n\n";
                    $QRlog .= 'REQUEST - '.json_encode($QRdata)."\n\n";
                    $QRlog .= 'REQUEST ENCRYPTED - '.json_encode($QRencryptedRequest)."\n\n";
                    
                    file_put_contents($QRlogfile, $QRlog, FILE_APPEND | LOCK_EX);
                    
                    $curl = curl_init();

                    curl_setopt_array($curl, array(
                        CURLOPT_URL => $QRhttpUrl,
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 60,
                        CURLOPT_CUSTOMREQUEST => 'POST',
                        CURLOPT_POSTFIELDS => $QRencryptedRequest,
                        CURLOPT_HTTPHEADER => $QRheader
                    ));
                    
                    $QRraw_response = curl_exec($curl);
                    
                    curl_close($curl);

                    $QRfp= fopen($_SERVER['DOCUMENT_ROOT']."/ICICIPubliccert/private_key.txt","r"); // it can be in any format                
                    $QRpriv_key_string=fread($QRfp,8192);
                    
                    fclose($QRfp);
                    $QRprivate_key = openssl_pkey_get_private($QRpriv_key_string, "");
                    
                    openssl_private_decrypt(base64_decode($QRraw_response), $QRresponse, $QRprivate_key); 

                    $QRlog = "\n\n".'GUID - '.time()."============".date('d-m-y h:i')."=============\n";
                    $QRlog .= "URL - ".$QRhttpUrl." \n";
                    $QRlog .= "\n".'RESPONSE - '.json_encode($QRraw_response)."\n\n";
                    $QRlog .= "RESPONSE DECRYPTED - ".$QRresponse."\n\n";
                    
                    file_put_contents($QRlogfile, $QRlog, FILE_APPEND | LOCK_EX);
                    
                    $QRoutput = json_decode($QRresponse);*/
                   

//==================End Request for QR code generation===========================================//
//==================End Request for QR code generation===========================================//

            }else{
// Start UPI Payment mode section 
                //dd("CARD");
                if($request->plan_amount){  
                    //dd($request->user_store);
                    if($request->user_store == "yes"){
                        $id = base64_encode($last_id);
                    }else{
                        //dd($last_id);
                        $id = base64_encode($request->id);
                    }
                    
                    $type = 'membership_fee';
                    
                    $ReturnUrl = url('/payment_returnURL/'.$id.'/'.$type);
                    $NotifyUrl = url('/payment_notifyURL');
                    
                    $plan_details = Plans::find($request->plan_id);
                    //dd($plan_details->mebership_fees);
                    $orderId = time();
                    try{
                        $order = new Order();
                        $od["orderId"] = $orderId;
                        $od["orderAmount"] = $plan_details->mebership_fees;
                        $od["orderNote"] = $request->description;
                        $od["customerPhone"] = $request->primary_contact;
                        $od["customerName"] = $request->name[$i];
                        $od["customerEmail"] = $request->contact_email;
                        $od["returnUrl"] = $ReturnUrl;
                        $od["notifyUrl"] = $NotifyUrl;    
                        
                        $order->create($od);

                        $link[] = '';

                        $link = $order->getLink($od['orderId']);
                        //dd($link);
                    }catch (\Exception $e){
                        notify()->error($e->getMessage());     
                        return redirect(url('membership-plans'));
                    }
                    
                    if($link->status == "OK"){
                        return Redirect::to($link->paymentLink);
                        
                    }else{
                        notify()->error("Something went wrong plase try again");     
                        return redirect(route('membership-plans'));
                    }
                }
            }
        }
    }
	
	public function icici_call_back(Request $request){
	    
	    $cryptedText = file_get_contents('php://input');

		$logfile = $_SERVER['DOCUMENT_ROOT'].'/logs/ciblog.txt'; 

		$fp= fopen($_SERVER['DOCUMENT_ROOT']."/ICICIPubliccert/private_key.txt","r"); 
        $priv_key_string=fread($fp,8192);
		
        fclose($fp);
        $private_key = openssl_get_privatekey($priv_key_string, "");

        openssl_private_decrypt(base64_decode($cryptedText), $response, $private_key); 
       	
        $log = "\n\n".'GUID - '.time()."======================CallBack LOG======================= \n";
		$log = "\n\n".'TIMEDATE - '.time()."=============".date('d-m-y h:i')."=============\n";
        $log .= 'RESPONSE - '.json_encode($cryptedText)."\n\n";
        $log .= 'RESPONSE DECRYPTED - '.$response."\n\n";
        
        file_put_contents($logfile, $log, FILE_APPEND | LOCK_EX);
        /*
        $response = '{"TxnStatus":"FAILURE","subMerchantId":"422421","PayerAmount":"5000.00","PayerName":"UMANG LALJIBHAI PATEL","terminalId":"5411","TxnInitDate":"20211021111444","merchantTranId":"MR1634795084","PayerMobile":"8866111719","BankRRN":"129407882364","merchantId":"422421","PayerVA":"pro.umang3079@icici","TxnCompletionDate":"20211021111710"}';
*/
        $output = json_decode($response);
       // dd($output);                                                            
        $where =  array('merchantTranId'=>$output->merchantTranId);
        $member_id = Membership_Fee::where($where)->first();
        
        $data = array(
            'tx_status'=>$output->TxnStatus,
            'PayerName'=>$output->PayerName,
            'PayerMobile'=>$output->PayerMobile,
            'payeeVA'=>$output->PayerVA
        );

        $where =  array('merchantTranId'=>$output->merchantTranId);
        Membership_Fee::where($where)->update($data);

        
        if($output->TxnStatus == 'SUCCESS'){

            $data2=array(
                'status' => 'member'
            );
            
            $where =  array('id'=>$member_id->member_id);
            AddMembership::where($where)->update($data2);
        }else{
            
            $id = $member_id->member_id;
            
            AddMembership::distroy($id);
        }
		
	}

    public function ReturnUrl(Request $request, $id, $type){

        $id = base64_decode($id);
        //dd($id);
        if($request->txStatus == "CANCELLED"){

            if($type == 'agent_fee'){

                Agent::destroy($id);
                
                notify()->success('You Cancelled transaction please try again'); 
                return redirect('login'); 
            }else{

                AddMembership::destroy($id);
                
                notify()->success('You Cancelled transaction please try again'); 

                Auth::guard('webcustomer')->logout();
                return redirect('login');  
            }
        }

        if($id){
            $exist_data = Membership_Fee::where('member_id',$id)->first();
        }else{
            $exist_data = Agent_Fee::where('agent_id',$id)->first();
        }
        if($request){
            if($exist_data){
                $data = array(
                    'payment_id' => $request->orderId,
                    'payment_amount' => $request->orderAmount,
                    'reference_id' => $request->referenceId,
                    'tx_status' => $request->txStatus,
                    'payment_mode' => $request->paymentMode,
                    'tx_msg' => $request->txMsg,
                    'tx_time' => $request->txTime,
                    'signature' => $request->signature,
                    'type' => $type
                );

                $data2=array(
                    'status' => 'member'
                );

                try{

                    if($type == 'agent_fee')
                    {
                        $where =  array('agent_id'=>$id);
                        $save_fee = Agent_fee::where($where)->update($data);
                    }else{
                        $where =  array('member_id'=>$id);
                        $save_fee = Membership_Fee::where($where)->update($data);

                        $where1 =  array('id'=>$id);
                        AddMembership::where($where1)->update($data2);
                    }
                    notify()->success('Congratulations, Your Membership fees paid successfully. Now you have to register for mandate.');
                    return redirect(url('mandate/'.base64_encode($id)));
                    
                } catch (\Exception $e) {
                    notify()->error($e->getMessage());    
                    return redirect(route('login'));
                }

            }else{
                
                try{
                    if($type == 'agent_fee' || $type == 'recharge')
                    {   
                        $fee = new Agent_Fee;
                        $fee->agent_id = $id;
                        $fee->payment_id = $request->orderId;
                        $fee->payment_amount = $request->orderAmount;
                        $fee->reference_id = $request->referenceId;
                        $fee->tx_status = $request->txStatus;
                        $fee->payment_mode = $request->paymentMode;
                        $fee->tx_msg = $request->txMsg;
                        $fee->tx_time = $request->txTime;
                        $fee->signature = $request->signature;
                        $fee->type = 'agent_fee';

                        $save_fee = $fee->save();

                        /*$where2 =  array('id'=>$id);
                        $wallet = array('wallet'=>$request->orderAmount);
                        dd($fee);
                        $pay_agent_fee = Agent::where($where2)->update($data);*/

                            if($fee->tx_status == 'SUCCESS'){
                                notify()->success('Congratulations! Agent register successfully done.');
                                return redirect(route('login'));
                            }else{
                                notify()->error('Somthing went wrong please try again.');
                                return redirect(route('login'));
                            }

                    }else{
                        
                        $fee = new Membership_Fee;
                        $fee->member_id = $id;
                        $fee->payment_id = $request->orderId;
                        $fee->payment_amount = $request->orderAmount;
                        $fee->reference_id = $request->referenceId;
                        $fee->tx_status = $request->txStatus;
                        $fee->payment_mode = $request->paymentMode;
                        $fee->tx_msg = $request->txMsg;
                        $fee->tx_time = $request->txTime;
                        $fee->signature = $request->signature;
                        $fee->type = 'membership_fee';
                        $save_fee = $fee->save();
                    }

                        $data2=array(
                            'status' => 'member'
                        );

                        $where1 =  array('id'=>$id);
                        AddMembership::where($where1)->update($data2);
                    if($save_fee){
                        if($fee->tx_status == 'SUCCESS'){
                            notify()->success('Congratulations! Membership fees have been paid successfully. Please submit the mandate form.');
                            return redirect(url('mandate/'.base64_encode($id)));
                        }else{
                            return view('front.payment_error');
                        }
                    }else{
                            notify()->error('Somthing went wrong please try again');     
                            return redirect(route('login'));
                    }

                } catch (\Exception $e) {
            
                    notify()->error($e->getMessage());    
                   
                    return redirect(route('login'));
                }
            } 
        }
    }

    public function mandate_registration($id)
    {
       
        $id = base64_decode($id);
        
        $member_request_details = DB::table('memberships')->where('id',$id)->first();
        //dd($member_request_details);
        $member_plan = Membership::where('id',$member_request_details->plan_id)->first();
        
        return view('front.mandate_registration',compact('member_request_details','member_plan'));
    }

    public function mandate_response(Request $request, $id, $merchant_id, $txt_id, $consumer_id)
    {

        //dd($request->all());
        $id = base64_decode($id);
        $response = explode('|', $request->msg);
        //dd($response);
        try{
            $mandate = new Mandate_registration;
            $mandate->user_id = $id;
            $mandate->consumer_id = $consumer_id;
            $mandate->merchant_id = $merchant_id;
            $mandate->txn_id = $txt_id;
            $mandate->txn_status = $response[0];
            $mandate->txn_msg = $response[1];
            $mandate->txn_err_msg = $response[2];
            $mandate->clnt_txn_ref = $response[3];
            $mandate->tpsl_bank_cd = $response[4];
            $mandate->tpsl_txn_id = $response[5];
            $mandate->txn_amt = $response[6];
            $mandate->clnt_rqst_meta = $response[7];
            $mandate->tpsl_txn_time = $response[8];
            $mandate->bal_amt = $response[9];
            $mandate->card_id = $response[10];
            $mandate->alias_name = $response[11];
            $mandate->BankTransactionID = $response[12];
            $mandate->mandate_reg_no = $response[13];
            $mandate->token = $response[14];
            $mandate->hash = $response[15];
            $save_mandate = $mandate->save();

            if($save_mandate){
                if($response[1] == 'success'){
                    
                    $data2=array(
                        'status' => 'mandate'
                    );
                    $where1 =  array('id'=>$id);
                    
                    AddMembership::where($where1)->update($data2);

                    //=============== Start Mamdate Verification API ============= //

                    $mandate_verifiy = AdminHelper::mandate_verification($merchant_id, $txt_id, $consumer_id);
                    //dd($mandate_verifiy);
                    //=============== Start Transection Schedule API ============= //

                    $bank_txn_id = $response[5];
                    $txn_amt = $response[6];
                    $mandate_reg_no = $response[13];
                    $t_identifier = $response[3];
                    $transaction_schedule = AdminHelper::transaction_schedule($merchant_id, $txt_id, $bank_txn_id, $txn_amt, $mandate_reg_no, $t_identifier);

                    //dd($transaction_schedule);
                    $m_txn_id = $transaction_schedule['merchantTransactionIdentifier'];

                    /*$data2=array(
                        'transaction_identifier' => $m_txn_id
                    );
                    $where1 =  array('id'=>$mandate->id);                    
                    Mandate_registration::where($where1)->update($data2);

                    //=============== End Transection Schedule API ============= //

                    //$identifier = Mandate_registration::find($mandate->id);
                    //$identifier_txn_id = $identifier->transaction_identifier;

                    $transaction_verification = AdminHelper::transaction_verification($merchant_id, $txt_id,);

                    //dd($transaction_verification); */

                    notify()->success('Congratulations! You have successfully submited your mandate form.');
                    return redirect(url('login'));
                }else{
                   
                    notify()->error($response[2]);
                    //return redirect(url('login'));
                    return redirect(url('mandate/'.base64_encode($id)));
                }

            }else{
              
                notify()->error('Somthing went wrong please try again');     
                return redirect(url('mandate/'.base64_encode($id)));
            }
        }catch (\Exception $e) {
           
            notify()->error($e->getMessage());
            return redirect(url('mandate/'.base64_encode($id)));
        }
        
        return redirect(url('login'));
    }

    public function NotifyUrl(Request $request){
        return view('front.payment_success');
    }


}
