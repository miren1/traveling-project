<?php

namespace App\Http\Controllers\front;

use App\OtpVerify;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use OTP;
use AdminHelper;

class SignupController extends Controller
{
    public function GetOtp(Request $request)
    {
        //dd($request->all());
        //$otp = 5252;
        $otp = random_int(1000, 9999);
        //dd($otp);
        $to = $request->mobile;
        $date = date('Y-m-d H:i',strtotime('+5 minutes'));
        $arrdata = array(
            'mobile'=>$to,
            'otp'=>$otp,
            'otp_at'=>$date,
            'type'=>$request->type,
        );
        //dd($arrdata);
        $sent_otp = OtpVerify::create($arrdata);

         $sentOtp = OTP::SendOtp(ucfirst($request->name),$otp,$to,'LOGIN');
        // dd($sentOtp);
        if($sent_otp)
        {
            return response()->json(['success'=>true]);
        }
        else
        {
            return response()->json(['success'=>false]);
        }
    }

    public function GetEmailOtp(Request $request)
    {
        // dd($request->all());
        $data['otp'] = random_int(1000, 9999);
        $data['name'] = ($request->name)?$request->name:'';
        $to = $request->email;
        $date = date('Y-m-d H:i',strtotime('+5 minutes'));
        $arrdata = array(
            'email'=>$to,
            'email_otp'=>$data['otp'],
            'otp_at'=>$date,
            'type'=>$request->type,
        );
        $sent_otp = OtpVerify::create($arrdata);

        $body = view('mail.otpmail',compact('data'))->render();
        // return $body;
        AdminHelper::sendEmailTo($body,'OTP Verification',$to);
        if($sent_otp)
        {
            return response()->json(['success'=>true]);
        }
        else
        {
            return response()->json(['success'=>false]);
        }
    }

    public function SignupOtp(Request $request)
    {
        // dd($request->all());
        if($request->status=="contact")
        {
            $where = array(
                'mobile'=>$request->contact,
                'otp'=>$request->contactOtp
            );
        }
        else
        {
            $where = array(
                'email'=>$request->email,
                'email_otp'=>$request->emailOtp
            );
        }
        // dd($where);
        $count = OtpVerify::where($where)->where('otp_at','>=',date('Y-m-d H:i'))->orderBy('created_at','DESC')->first();
        // return $count;
        if($count)
        {
            return response()->json(['valid'=>true]);
        }
        else
        {
            return response()->json(['valid'=>false]);
        }
    }
}
