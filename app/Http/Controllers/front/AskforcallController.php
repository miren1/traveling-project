<?php

namespace App\Http\Controllers\front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Askforcall;
use App\Ticket_category;
use App\Group;
use App\Grouplead;
use App\User;
use App\Assignlead;
use App\Setting;
use App\Ticket_category_assignment;

class AskforcallController extends Controller
{
    public function index()
    {
        $data['ticket_category'] = Ticket_category::all();
        return view('front.askforcall',$data);
    }

    public function Store(request $request)
    {
    	//dd($request->all());

    	$assign_emp = Ticket_category_assignment::where('ticket_cat_id',$request->cat)->where('ticket_sub_cat_id',$request->sub_cat)->first();
    	
		$user=User::where('id',$assign_emp->user_id)->first();
		
		$Askforcall =new Askforcall();
		//$Askforcall->user_id =$assign_emp->user_id;
		$Askforcall->cat_id = $request->cat;
		$Askforcall->sub_cat_id = $request->sub_cat;
		$Askforcall->first_name = $request->first_name;
		$Askforcall->last_name = $request->last_name;
		$Askforcall->email = $request->email;
		$Askforcall->contactnumber = $request->contactnumber;
		$Askforcall->wpnumber = $request->wpnumber;
		//dd($Askforcall);
	 	$data= $Askforcall->save();

	 	if($data){
    	 	notify()->success('Ask for Call Send Successfully !');
	 	}else{
			notify()->error('Ask for Call not Send Successfully !');
	 	}

//		$user=User::where('role','admin')->first();
		//$setting=Setting::where('user_id',$user->id)->first();	 
    	/*if($request->method('post')){
			if(isset($user)){

    		$Askforcall =new Grouplead();
			$Askforcall->user_id =$user->id;
			if($setting->askforcall_group_id){
				$Askforcall->group_id =$setting->askforcall_group_id;		
			}
    		$Askforcall->fname = $request->first_name;
    		$Askforcall->lname = $request->last_name;
    		$Askforcall->email = $request->email;
    		$Askforcall->cat_id = $request->cat;
    		$Askforcall->sub_cat_id = $request->sub_cat;
    		$Askforcall->number = $request->contactnumber;
    		$Askforcall->wnumber = $request->wpnumber;
    		//dd($Askforcall);
    	 	$data= $Askforcall->save();

    	 	if($data){
				if($Askforcall->id){
					$Assignlead =new Assignlead();
					$Assignlead->user_id =$user->id;
					$Assignlead->group_id =$setting->askforcall_group_id;
					$Assignlead->employee_id =$setting->askforcall_user_id;
					$Assignlead->group_lead_id =$Askforcall->id;
					$Assignlead->save();	
				}

    	 		 notify()->success('Ask for Call Send Successfully !');
    	 	}else{
				notify()->error('Ask for Call not Send Successfully !');
    	 	}
		}else{
			notify()->error('Somthing is wrong!!');
		}	*/ 

    	/*}else{
    		notify()->error('Somthing is wrong!!');
    	}*/
      return redirect('/askforcall');
    }
}
