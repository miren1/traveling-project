<?php

namespace App\Http\Controllers;
use Auth;
use DB;
use Illuminate\Http\Request;
use App\Ticket_sub_category;
use App\Ticket;
use App\Ticket_category;
use App\Ticketmessage;
use App\Ticket_category_assignment;

class EmployeegenrateController extends Controller
{
    public function index()
    { 
     return view('employee.ticketgenrate.index');  
    }

    public function add()
    { 
      
      $Ticket = Ticket_category_assignment::where('user_id',Auth::user()->id)->get()->toArray();
     
      $Ticket_category = array_column($Ticket, 'ticket_sub_cat_id');

      $where= array('employee'=>'1');
      
      $data['ticket_sub'] = Ticket_sub_category::whereNotIn('id',$Ticket_category)->with('ticket_categories')->where($where)->get();
     
      $ticket_cat_id= array_column($data['ticket_sub']->toArray(),'ticket_cat_id');
      $data['ticket_category']=Ticket_category::whereIn('id',$ticket_cat_id)->get();
     
      return view('employee.ticketgenrate.add')->with($data);  
    }


    public function getCat(Request $request)
    {
        $stateid = $request->cat_id;
        $Ticket = Ticket_category_assignment::where('user_id',Auth::user()->id)->get()->toArray();
        // dd($Ticket);
         $Ticket_category = array_column($Ticket, 'ticket_sub_cat_id');
        $where= array('employee'=>'1','ticket_cat_id'=>$stateid); 
        $cities= Ticket_sub_category::whereNotIn('id',$Ticket_category)->where($where)->get();
        return response()->json([
            'cat' => $cities
        ]);
    }

    public function Store(Request $request)
    {
      
      $ticket_category_id=NUll;
      if($request->category){
        $ticket_category_id =$request->category;
      }
      $name=NULL;
      $file_type=NULL;
      if($request->file('file'))
      {
                $file = $request->file;
                $extension = $file->getClientOriginalName();
                $name = strtotime('now').'_'.$extension;
                $file_type = $file->getClientOriginalExtension();
                $file->move('public/ticket',$name);
      }
      //dd($file_type);
      $dataFormater = array('user_id'=>Auth::user()->id,
                           'ticket_category_id'=>$ticket_category_id,
                          'ticket_sub_category_id'=>$request->sub_category,
                          'file'=>$name,
                          'file_type'=>$file_type,
                          'message'=>$request->message);
     
      $ticket= Ticket::create($dataFormater);

      if($ticket){
        notify()->success('Ticket Updated Successfully !');
      }else{
        notify()->error('Something Wrong Try Again..!');
      }
      return redirect('/employee-ticket-generator');
    }


    public function list()
    {
      $user_id=Auth::user()->id;
      return datatables()->of(Ticket::with(['ticket_categories','ticket_sub_categories'])->where(['is_delete'=>'0','user_id'=>$user_id])->orderBy('created_at', 'desc'))->toJson();
    }

    public function view($id='')
    {
      $data['ticket'] =Ticket::with(['ticket_categories','ticket_sub_categories'])->find(base64_decode($id));
      Ticketmessage::where(['ticket_id'=>base64_decode($id),'type'=>'receive'])->update(['is_view'=>'1']);
      $data['ticket_message_count']=Ticketmessage::where(['ticket_id'=>base64_decode($id),'type'=>'receive'])->count();
     
      return view('employee.ticketgenrate.view')->with($data);  
    }

    public function getcount($id = null)
    {
      $data['msg_count']= Ticketmessage::where(['ticket_id'=>base64_decode($id),'type'=>'receive'])->count();
      return  json_encode($data);
    }


    public function get_receive_msg()
    {

      $user_id=Auth::user()->id;
      $Ticket = Ticket::where(['status'=>'active','is_delete'=>'0','user_id'=>$user_id])->orderBy('created_at', 'desc')->get()->toArray();
      $Ticket_category = array_column($Ticket,'id');
     // dd($Ticket_category);
     $Ticketmessage= Ticketmessage::with('ticket')->whereIn('ticket_id',$Ticket_category)
      ->whereRaw('id IN (select MAX(id) FROM ticket_messages where is_delete="0" AND is_view="0" GROUP BY ticket_id)')->where(['is_delete'=>'0','type'=>'receive','is_view'=>'0']);
      
      $data['receive_message_count']= $Ticketmessage->count();
      $data['receive_message']= $Ticketmessage->limit(6)->get();



      $Ticketmessage2= Ticketmessage::with('ticket')->whereIn('ticket_id',$Ticket_category)
      ->whereRaw('id IN (select MAX(id) FROM ticket_messages where is_delete="0" AND type="send" AND is_view="0" GROUP BY ticket_id)')->where(['is_delete'=>'0','type'=>'send','is_view'=>'0']);
      $data['send_message_count']= $Ticketmessage2->count();
      $data['send_message']= $Ticketmessage2->limit(6)->get();

      return  json_encode($data);
    }



    public function send_message(Request $request){
     
      if($request->ticket_id&&$request->message){

        $Ticket=Ticket::find(base64_decode($request->ticket_id));
        $ticket_id=base64_decode($request->ticket_id);
        $ticket_category_id=NULL;
        $ticket_sub_category_id=NULL;

        if($Ticket->ticket_category_id){
          $ticket_category_id=$Ticket->ticket_category_id;
        }
        if($Ticket->ticket_sub_category_id){
          $ticket_sub_category_id=$Ticket->ticket_sub_category_id;
        }



      $name=NULL;
      $file_type=NULL;
      if($request->file('media'))
      {
                $file = $request->media;
                $extension = $file->getClientOriginalName();
                $name = strtotime('now').'_'.$extension;
                $file_type = $file->getClientOriginalExtension();
                $file->move('public/ticket',$name);
      }

       $sentData= array(
          'user_id'=>Auth::user()->id, 
          'ticket_id'=> $ticket_id, 
          'ticket_category_id'=>$ticket_category_id, 
          'ticket_sub_category_id'=>$ticket_sub_category_id, 
          'type'=>'send', 
          'message'=>$request->message,
          'file'=>$name,
          'file_type'=>$file_type, 
    
       );

       $send=Ticketmessage::create($sentData);
       if($send){
        $status = true;
       }else{
        $status = false;
       }
      }else{
        $status = false;
      }
      return json_encode($status);
    }


    public function get_messages($id = null)
    {
      if($id){
        $where=array('ticket_id'=>base64_decode($id),
                    'is_delete'=>'0');
        $data['ticket']  = Ticket::find(base64_decode($id));
        $data['message'] = Ticketmessage::with('user')->where($where)->orderBy('id', 'ASC')->get();
        return  json_encode($data);
      }else{
        $data= array();
        return  json_encode($data);
      }
      
    }


}