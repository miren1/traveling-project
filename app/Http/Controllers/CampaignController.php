<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Email;
use App\Campaign;
// use App\Imports\ContactsImport;
// use Maatwebsite\Excel\Facades\Excel;

class CampaignController extends Controller
{
    public function index()
    {
    	
    	return view('admin.campaign.index');
    }

    public function list()
    {
    	return datatables()->of(Campaign::withCount('email')->where('is_delete','1')->where('user_id',Auth::user()->id)->orderBy('created_at', 'desc'))->toJson();
    }


    public function add()
    {
    	return view('admin.campaign.add');
    }


    public function view($id=null)
    {
    	$data['campaign']=Campaign::find(base64_decode($id));
        //dd($data);
    	return view('admin.campaign.view')->with($data);
    }


   public function delete(Request $request)
   {
		$delete=Campaign::find(base64_decode($request->id))->update(['is_delete'=>'0']);
		if($delete)
        {
            $data['status'] = "success";
            $data['message'] = "Campaign is deleted successfully!";
        }
        else
        {
            $data['status'] = "error";
            $data['message'] = "Campaign is not deleted.";
        }

        return response()->json($data);
    }


    public function email_list($id=null)
    {
    	return datatables()->of(Email::where('campaign_id',base64_decode($id))->where('is_delete','1')->orderBy('created_at', 'desc'))->toJson();
    }

    public function store_campaign(Request $request)
    {
    	
    

        $Campaign=new Campaign();
        $Campaign->name=$request->name;
        $Campaign->user_id =Auth::user()->id;
        $create = $Campaign->save();

        //dd($Campaign->id);
        if($create){

            $image = $request->file('csv_file');
            //dd($image);
            $extension = $image->getClientOriginalExtension();
            $path = $request->file('csv_file')->getRealPath();
            //$file = File::get($request->file('csv_file'));
        
            if($extension == "csv"){
               $data=$this->csvToArray($path);
               unset($data[0]);

               if(!empty($data)){
                    if(count($data[1])==3){

                        foreach ($data as $key => $value) 
                        {
                            
                            if(isset($value[2])&&filter_var($value[2], FILTER_VALIDATE_EMAIL)){

                                    $MyArray = array('name' =>$value[0], 
                                                    'number'=>$value[1],
                                                    'to'=>$value[2],
                                                    'body'=>$request->message,
                                                    'user_id'=>Auth::user()->id,
                                                    'campaign_id'=>$Campaign->id);
                                    $send=$this->Email($MyArray);
                                    //dd($send);
                                    if($send['status']!='error'){
                                        $MyArray['status']="1";
                                    }else{
                                        $MyArray['status']="0";
                                    }
                                    Email::create($MyArray);
                            }

                        }
                        notify()->success('Campaign Send Successfully !');


                    }else{
                        notify()->error('CSV Data are Not Proper!!');
                    }
               }else{

                 notify()->error('Campaign not send Successfully CSV File is empty !');

               }



            }else{

                 notify()->error('Campaign not send Successfully CSV File extension Are wrong!');

            }

        }else{
            notify()->error('Campaign not send Successfully!');

        }

        

       return redirect('/campaign'); 

    }


    public function Email($data){
        
        $body = view('admin.emails.camping',$data)->render();

        return $this->sendEmail('akshay@xsquaretec.com',$body,'JagJoyu Offer',$data['to']);
        
    }


    public function sendEmail($from,$body,$subject,$send_to){

        $key='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';
        
        $email = new \SendGrid\Mail\Mail(); 
        $email->setFrom($from);
        $email->setSubject($subject);
        $email->addTo($send_to);
        $email->addContent("text/html", $body);
        $sendgrid = new \SendGrid($key);

        try {


            $response = $sendgrid->send($email);
        
            return array('status' => $response->statusCode());
        } catch (Exception $e) {

            return array('status' => 'error' , 'error' => $e->getMessage());
        }
    }

    public function csvToArray($filename = '', $delimiter = ',')
    {
        if (!file_exists($filename) || !is_readable($filename))
            return false;

        $header = null;
        $data = array();
        if (($handle = fopen($filename, 'r')) !== false)
        {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== false)
            {
                    $data[] = $row;
            }
            fclose($handle);
        }

        return $data;
    }
    public function txtToArray($filename='')
    {
        foreach (explode("\n", $filename) as $key=>$line){
             $array[$key] = explode(',', $line);
        }
        return $array;
    }



    public function email_view($id=null)
    {
    	$data['email']=Email::find($id);
    	return view('admin.campaign.email_view')->with($data);
    }

    public function email_delete(Request $request)
    {
		$delete=Email::find(base64_decode($request->id))->update(['is_delete'=>'0']);
		if($delete)
        {
            $data['status'] = "success";
            $data['message'] = "Email is deleted successfully!";
        }
        else
        {
            $data['status'] = "error";
            $data['message'] = "Email is not deleted.";
        }

        return response()->json($data);
    }




}
