<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;

use Auth;
use App\Ads;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Storage;

class AdsController extends Controller
{
    public function index()
    {
        return view('admin.ads.list');
    }

    public function add()
    {
        return view('admin.ads.add');
    }

    public function store(Request $request)
    {
        if($request->ads_type == "image" || $request->ads_type == "script"){
            
            if($request->ads_image){
                if($request->hasFile('ads_image')){
                    $file = $request->ads_image;
                    $filename = time() . '.' . $file->getClientOriginalExtension();
                    $file->move('uploads/ads', $filename);
                }
                $url = 'uploads/ads/'.$filename;
            }else{

                $img = Ads::find($request->id);
                $url = $img->ads_image;
            }

            
        }else{
            $url = Null;
        }
            if($request->id){
            // dd($request->all());

                // $v_url = preg_replace(
                //     "/\s*[a-zA-Z\/\/:\.]*youtu(be.com\/watch\?v=|.be\/)([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i",
                //     "//www.youtube.com/embed/$2",
                //     $request->ads_video_url
                // );
                
                if($request->ads_type == 'image'){
                    $data = array(
                        'title'=>$request->title,
                        'ads_type'=>$request->ads_type,
                        'ads_image'=>$url,
                        'ads_video_url'=>null,
                        'affiliate_url'=>null,
                        'position'=>$request->ads_position
                    );  
                }else if($request->ads_type == 'video'){
                    $data = array(
                        'title'=>$request->title,
                        'ads_type'=>$request->ads_type,
                        'ads_image'=>null,
                        'ads_video_url'=>$request->ads_video_url,
                        'affiliate_url'=>null,
                        'position'=>$request->ads_position
                    ); 
                }else{
                    $data = array(
                        'title'=>$request->title,
                        'ads_type'=>$request->ads_type,
                        'ads_image'=>$url,
                        'ads_video_url'=>null,
                        'affiliate_url'=>$request->affiliate_url,
                        'position'=>$request->ads_position
                    ); 
                }
                // dd($data);
                $where = array('id'=>$request->id);
                $isUpdated = Ads::where($where)->update($data);

                if($isUpdated){
                    notify()->success('Ads updated successfully done');
                    return redirect('ads');
                }else{
                    notify()->error('Something went wrong please try again!');  
                    return redirect('ads');
                }

            }else{

                // $v_url = preg_replace(
                //     "/\s*[a-zA-Z\/\/:\.]*youtu(be.com\/watch\?v=|.be\/)([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i",
                //     "//www.youtube.com/embed/$2",
                //     $request->ads_video_url
                // );
                // dd($v_url);
                // dd($request->all());
                $ads =New Ads();
                $ads->title =$request->title;
                $ads->ads_type =$request->ads_type;
                $ads->ads_image =$url;
                $ads->ads_video_url =$request->ads_video_url;
                $ads->affiliate_url =$request->affiliate_url;
                $ads->position =$request->ads_position;
                $save = $ads->save();
                if($save){
                    notify()->success('Ads added successfully done');
                    return redirect('ads');
                }else{
                    notify()->error('Something went wrong please try again!');  
                    return redirect('ads');
                }
            }    
    }

    public function list(Request $request)
    {
        if (Auth::user()->role =='admin') {
            return datatables()->of(Ads::where('is_deleted','0')->orderBy('id', 'DESC')->get())->toJson();
        }
    }

    public function edit($id)
    {
    
        $id = base64_decode($id);
        $ads = Ads::find($id);
        $ads['url'] = "http://$_SERVER[HTTP_HOST]";

        return view('admin.ads.add',compact('ads'));
    }

    public function destroy($id)
    {
        //dd($id);
        $deleted = Ads::where('id',$id)->update(['is_deleted'=>'1']);
        
            if ($deleted) {
                $status = true;
            }else{
                $status = false;
            }
            return response()->json($status);
    }

    public function CheckTitle(Request $request ,$id = null){    
      if($id != ""){
          $where = array(
              array('title',$request->title),
              array('id','<>',$id),
          );
          $arrData = Ads::where('is_deleted','0')->where($where)->get();
      }else{
        $where = array('title'=>$request->title);
        $arrData = Ads::where('is_deleted','0')->where($where)->get();
      }
      if(count($arrData) != "0"){
          return response()->json(array('valid'=>false));
      }
      return response()->json(array('valid'=>true));
    } 

}
