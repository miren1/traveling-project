<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;

use Auth;
use App\Career;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Storage;

class CareersController extends Controller
{
    public function index()
    {
        return view('admin.careers.list');
    }

    public function add()
    {
        return view('admin.careers.add');
    }

    public function store(Request $request)
    {

        if($request->id){

            $data = array(
                'department'=>$request->department,
                'opening_title'=>$request->opening_title,
                'number_of_openings'=>$request->number_of_openings,
                'relevant_experience_required'=>$request->relevant_experience_required,
                'base_location'=>$request->base_location,
                'roles_responsibilities'=>$request->roles_responsibilities,
                'primary_skills'=>$request->primary_skills,
                'pre_requiesite'=>$request->pre_requiesite,
                'advantage'=>$request->advantage,
                'work_flexibility'=>$request->work_flexibility,
                'salary_range'=>$request->salary_range,
                'incentives'=>$request->incentives,
                'benefits'=>$request->benefits
            );
            //dd($data);
            $where = array('id'=>$request->id);
            $isUpdated = Career::where($where)->update($data);

            if($isUpdated){
                notify()->success('Career updated successfully done');
                return redirect('jj-career');
            }else{
                notify()->error('Something went wrong please try again!');  
                return redirect('jj-career');
            }

        }else{

            $career =New Career();
            $career->department =$request->department;
            $career->opening_title =$request->opening_title;
            $career->number_of_openings =$request->number_of_openings;
            $career->relevant_experience_required =$request->relevant_experience_required;
            $career->base_location =$request->base_location;
            $career->roles_responsibilities =$request->roles_responsibilities;
            $career->primary_skills =$request->primary_skills;
            $career->pre_requiesite =$request->pre_requiesite;
            $career->advantage =$request->advantage;
            $career->work_flexibility =$request->work_flexibility;
            $career->salary_range =$request->salary_range;
            $career->incentives =$request->incentives;
            $career->benefits =$request->benefits;
            //dd($career);
            $save = $career->save();
            if($save){
                notify()->success('Career added successfully done');
                return redirect('jj-career');
            }else{
                notify()->error('Something went wrong please try again!');  
                return redirect('jj-career');
            }
        }    
    }

    public function list(Request $request)
    {
        if (Auth::user()->role =='admin') {
            return datatables()->of(Career::where('deleted_at','0')->orderBy('id', 'DESC')->get())->toJson();
        }
    }

    public function edit($id)
    {
        $id = base64_decode($id);
        $career = Career::find($id);
        
        return view('admin.careers.add',compact('career'));
    }

    public function destroy($id)
    {
        //dd($id);
        $deleted = Career::where('id',$id)->update(['deleted_at'=>'1']);
        
            if ($deleted) {
                $status = true;
            }else{
                $status = false;
            }
            return response()->json($status);
    }
}
