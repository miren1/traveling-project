<?php

namespace App\Http\Controllers;

use App\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Group;


class SettingController extends Controller
{
    public function index()
    {
        $id = Auth::user()->id;
        $setting = Setting::where('user_id',$id)->first();
        $users =User::where('role','user')->get();
        $groups =Group::where(['user_id'=>Auth::user()->id,'is_delete'=>'0'])->get();
        //dd($groups);
        return view('admin.setting.index',compact('setting','users','groups'));
    }

    public function store(Request $request)
    {
         //dd($request->all());
        $data = array(
            'user_id' => Auth::user()->id,
            'sendgrid_key' => $request->sendgrid_key,
            'contact_mail' => $request->contact_mail,
            'help_desk_mail' => $request->help_desk_mail,
            'executive_mail' => $request->executive_mail,
            'askforcall_user_id'=>$request->askforcall_user_id,
            'askforcall_group_id'=>$request->askforcall_group_id
        );

        if($request->id)
        {
            $update = Setting::where('id',$request->id)->update($data);
            if($update)
            {
                notify()->success('Setting Saved !');
            }
            else
            {
                notify()->error('Something Wrong Try Again..!');
            }
        }
        else
        {
            $update = Setting::create($data);
            if($update)
            {
                notify()->success('Setting Saved !');
            }
            else
            {
                notify()->error('Something Wrong Try Again..!');
            }
        }
        
        return redirect(route('admin.setting'));
    }
}
