<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\DB;
use Charts;
use Session;
use Pusher\Pusher;
use Illuminate\Support\Facades\Input;
use Mail;
use App\User;
use App\Message;
use Auth;
use Carbon\Carbon;
use App\Leave;
use App\Approve;
use Calendar;
use DateTime;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;  // Import Hash facade
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Packagtype;
use App\Toure;
use App\Announcement;


class EmployeeController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    // ***************************Employee Login **************************************************************
    public function employeeLogin()
    {
        return view('employee.employeelogin');
    }
    public function employeeLoginCheck(Request $request)
    {
      // dd($request->all());
        $email = $request->email;
        $password = $request->password;
        $userdata = array(
            'email'     => $request->email,
            'password'  => $request->password,
            'employee_approved'=>1,
            'role'=>'user'
        );
        
      //  print_r($userdata);
      //  die;
        if (Auth::attempt($userdata)) {
         // print_r($userdata);
          // die;
            
            $email = Auth::user()->email;
            $id = Auth::user()->id;
            $name = Auth::user()->name;
            $surname = Auth::user()->surname;
            $role = Auth::user()->role;
            $fullname = $name." ".$surname;
            $request->session()->put('emp_email', $userdata['email']);
            $request->session()->put('emp_name',$fullname);
            $request->session()->put('eid',$id);
            $request->session()->put('role',$role);
            return Redirect::to('empdashboard');
    
        } else { 
            Session::flash('error', 'Wrong Email Or Password Please Check');       
            return Redirect::to('employeeLogin');
    
        }
      }
public function employeeleavereport()
{
return view('employee.leavereport');

}
public function empleavereport(Request $request)
{
	//echo "working ajax...";
        $id = $request->session()->get('eid');
        $query = $_REQUEST['start'];
		$output = '';
        $data = [];
       if($query)
       {
        $startData  = explode(',',$query);
        $startYear = $startData[1];
        $startMonth = $startData[0];
       }
		if($query != '')
        {	$data = DB::table('leaves')
          ->where('employee_fk_id',$id)
          ->where([[DB::raw("(DATE_FORMAT(leave_date_start,'%Y'))"),$startYear],[DB::raw("(DATE_FORMAT(leave_date_start,'%m'))"),$startMonth]])
          ->orWhere([[DB::raw("(DATE_FORMAT(leave_date_end,'%Y'))"),$startYear],[DB::raw("(DATE_FORMAT(leave_date_end,'%m'))"),$startMonth]])
          ->get();
          $total_row = $data->count();
         }
   
  

    if($total_row > 0)
    {
      foreach($data as $row)
         {
          
            $output .= '
            <tr class="bg-light">
            <td>'.$row->leave_date_start.'</td>
           <td>'.$row->leave_date_end.'</td>
           <td>'.$row->approve_status.'</td>
           <td>'.$row->is_paid.'</td>
           <td>'.$row->approve_status.'</td>
           <td>'.$row->total_leave_days.'</td>
            <td>'.$row->total_days.'</td>
          </tr>';
           
           
         
         }
    }
    $data = array(
         'table_data'  => $output,
         'total_data'  => $total_row
        );
          echo json_encode($data);
  
}
      public function empchangepasswordform()
      {
        return view('employee.changepassword');
      }
      public function empchangePaswordUpdate(Request $request)
      {
      	$email = $request->session()->get('emp_email');
        $password = $_POST['password'];
        $newPasssword = $_POST['npassword'];
        $npass = Hash::make($newPasssword);
        $id = $request->session()->get('eid');
		$userdata = array(
            'email'     => $email,
            'password'  => $password,
            'role'=>'user'
        );
      if (Auth::attempt($userdata)) {
            $rs = DB::table('users')
                  ->where('id',$id)
                  ->update(['password' => $npass
       ]);
      Session::flash('password_change', 'Password Successfully Changed');
        }
        else{
            Session::flash('password_change', 'Password is wrong');
        }
     return back();
    }
public function allemprequest(Request $request)
{
 $id = $request->session()->get('eid');
 $LeaveInfo = DB::table('leaves')->where('employee_fk_id',$id)->where('user_notification_status',0)->where('approve_status','!=','pending')->get();
// print_r($LeaveInfo);
 return view('employee.request',compact('LeaveInfo'));
}
public function empapprovenotify($id)
{
$data  = array(
'user_notification_status'=>1
);
$LeaveInfo = DB::table('leaves')->where('id',$id)->update($data);
	return back();

}


public function Emp_LeaveReport()
    {
        $data = DB::table('leaves')->get();  
        return view('employee.show_leave_report', compact('data'));
    }

    public function AllEmpLeaveReport()
    {
        $user_id = Auth::user()->id;

        if (Auth::user()->role =='admin') {
            
            //$texts = LeaveReport::with('employee')->orderBy('created_at', 'desc')->get();

            $texts = DB::table('leaves')
            ->join('users', 'users.id', '=', 'leaves.employee_fk_id')
            ->select('leaves.*', 'users.name', 'users.middlename'
        , 'users.surname')
            ->get();

        }else{

            $texts = DB::table('leaves')
            ->join('users', 'users.id', '=', 'leaves.employee_fk_id')
            ->select('leaves.*', 'users.name', 'users.middlename'
        , 'users.surname')
            ->where('leaves.employee_fk_id',$user_id)
            ->get();
            //$texts = LeaveReport::with('employee')->where('employee_fk_id ',$user_id)->orderBy('created_at', 'desc')->get();

        
        }
        return response()->json(array('data' => $texts));
        //return response()->json(array('data' => $texts));

      
    }
	
    public function empdashboard(Request $request)
      {
        $id = $request->session()->get('eid');
        $targers  = DB::table('targets')->where(DB::raw("(DATE_FORMAT(month_year,'%Y'))"),date('Y'))->where('employee_id_fk',$id)->get();
      
        $year = date('Y');
        $month = date('m');
       $doneinq = DB::table('inquiry')->whereYear('done_date', '=', $year)
              		->whereMonth('created_at', '=', $month)
                    ->where('emp_handle_by_fk',$id)
              ->count();
       $new   = DB::table('inquiry')->where('inq_status','new')->where('emp_handle_by_fk',$id)->count();
      
      	$follow = DB::table('inquiry')->where('inq_status','follow')->where('emp_handle_by_fk',$id)->count();
        $announcement = Announcement::where('employee','1')->where('deleted_at','0')->first();
        // dd($announcement);
        return view('employee.index',compact('targers','new','doneinq','follow','announcement'));
      }
      public function employeetarget()
      {
        return view('employee.showtaget');
      }
      public function emptargetAjax(Request $request)
      {
       
        $id = $request->session()->get('eid');
        $query = $_REQUEST['start'];
      	$output = '';
        $data = [];
       
       if($query)
       {
        $startData  = explode('-',$query);
        $startYear = $startData[1];
        $startMonth = $startData[0];
       }
     
    if($query != '')
        {
        
          $data = DB::table('targets')
           	->whereYear('month_year','=',$startYear)
            ->whereMonth('month_year','=',$startMonth)
            ->where('employee_id_fk',$id)
            ->get();
    
    	$doneinq = DB::table('inquiry')
           	->whereYear('done_date','=',$startYear)
            ->whereMonth('done_date','=',$startMonth)
            ->where('emp_handle_by_fk',$id)
            ->count();
          $total_row = $data->count();
   
         }
      
   
// ->whereYear('month_year','>=',$startYear)
//  ->whereYear('month_year','<=',$endYear)
//  ->whereMonth('month_year','>=',$startMonth)
//  ->whereMonth('month_year','<=',$endMonth)
//  ->where('employee_id_fk',$id)
//  ->get();


// $total_row = $data->count();
// echo $total_row;
  
    if($total_row > 0)
    {
      
           if($doneinq >= $data[0]->target)
           {
            $output .= '
            <tr class="bg-light">
            <td>'.$data[0]->month_year.'</td>
           <td>'.$data[0]->target.'</td>
           <td>'.$doneinq.'</td>
          </tr>';
           }
           else{
            $output .= '
            <tr class="bg-danger">
            <td>'.$data[0]->month_year.'</td>
           <td>'.$data[0]->target.'</td>
           <td>'.$doneinq.'</td>
          </tr>';
           }
         
         
    }
    $data = array(
         'table_data'  => $output,
         'total_data'  => $total_row
        );
          echo json_encode($data);
        
      } 
      public function empleaveshow()
      {
        return view('employee.empleaves');
      }
      public function ShowLeaveDetail($id)
      {
          $leaves = DB::table('leaves')->where('id',$id)->get();
          return view('employee.show_empleaveDetail',compact('leaves'));
          
      }
      public function getMessage($user_id)
    {
        $my_id = Auth::id();
       Message::where(['from' => $user_id, 'to' => $my_id])->update(['is_read' => 1]);
       // Get all message from selected user
        $messages = Message::where(function ($query) use ($user_id, $my_id) {
            $query->where('from', $user_id)->where('to', $my_id);
        })->oRwhere(function ($query) use ($user_id, $my_id) {
            $query->where('from', $my_id)->where('to', $user_id);
        })->get();
        $options = array(
          'cluster' => 'ap2',
          'useTLS' => true
      );
        $pusher = new Pusher(
          env('PUSHER_APP_KEY'),
          env('PUSHER_APP_SECRET'),
          env('PUSHER_APP_ID'),
          $options
      );
      $counts = DB::table('messages')->where('is_read',0)->where('to',$user_id)->count();

      $data1 = array(
        'to'=>$my_id,
        'counts'=>$counts
      );

     echo $counts;echo $user_id;
      $pusher->trigger('message-channel', 'App\\Events\\Message',$data1);
      
      return view('employee.chatmessage', ['messages' => $messages]);
    }

    public function sendMessage(Request $request)
    {
        $from = Auth::id();
        $to = $request->receiver_id;
        $message = $request->message;

        $data = new Message();
        $data->from = $from;
        $data->to = $to;
        $data->message = $message;
        
        $data->is_read = 0; // message will be unread when sending message
        $data->save();

        // pusher
        $options = array(
            'cluster' => 'ap2',
            'useTLS' => true
        );

        $pusher = new Pusher(
            env('PUSHER_APP_KEY'),
            env('PUSHER_APP_SECRET'),
            env('PUSHER_APP_ID'),
            $options
        );
        $counts = DB::table('messages')->where('is_read',0)->where('to',$to)->count();
        

        $data1 = array(
          'to'=>$to,
          'counts'=>$counts
        );
      //  echo $counts;
        $pusher->trigger('message-channel', 'App\\Events\\Message',$data1);
      //  echo "count is " . $counts ;
        $data = ['from' => $from, 'to' => $to]; // sending from and to user id when pressed enter
        $pusher->trigger('my-channel', 'my-event', $data);
    }
public function documentempdw()
        {  $id = Input::get('id');
           $document = Input::get('document');
           $employeeProfile =  DB::table('users')->where('id',$id)->get();
           $file = "uploads/".$employeeProfile[0]->$document;
            return response()->download($file,'employee_document.pdf', [], 'inline');
        }
      public function empchat(Request $request)
      {
        $users = DB::select("select users.id, users.name, users.avatar, users.email, count(is_read) as unread 
        from users LEFT  JOIN  messages ON users.id = messages.from and is_read = 0 and messages.to = " . Auth::id() . "
        where users.id != " . Auth::id() . ".
        group by users.id, users.name, users.avatar, users.email");

        if($request->session()->get('role') == 'user')
        {
          return view('employee.employeechat',['users' => $users]);

        }
        if($request->session()->get('role') == 'admin')
        {
          return view('admin.employeechat',['users' => $users]);

         // return view('admin.employeechat',['users' => $users]);
        }
      }
      public function empleaveAjax(Request $request)
      {
        $output = '';
        $id = $request->session()->get('eid');
        $query = $_REQUEST['start'];
        $status = $_REQUEST['status'];
        $isApprove = $_REQUEST['isApprove'];
        $startData  = explode('-',$query);
        $startYear = $startData[0];
        $startMonth = $startData[1];
        if($status == 'all')
          {
           $leave = DB::table('leaves')->where('employee_fk_id',$id)->where(DB::raw("(DATE_FORMAT(leave_date_start,'%Y'))"),$startYear)->where(DB::raw("(DATE_FORMAT(leave_date_start,'%m'))"),$startMonth)->where('approve_status',$isApprove)->get();
            $total_row = $leave->count();
          }
          else
          {
           $leave = DB::table('leaves')->where('employee_fk_id',$id)->where(DB::raw("(DATE_FORMAT(leave_date_start,'%Y'))"),$startYear)->where(DB::raw("(DATE_FORMAT(leave_date_start,'%m'))"),$startMonth)->where('approve_status',$isApprove)->where('leave_type',$status)->get();
            $total_row = $leave->count();
          }
          if($total_row >0){
            foreach($leave as $row)
         {
           if($row->leave_mode == 1)
           {
            $row->leave_mode = 'Full';
           }
           else{
            $row->leave_mode = 'Half';
           }
          $output .= '
          <tr>
          <td>'.$row->leave_date_start.'</td>
         <td>'.$row->leave_date_end.'</td>
         <td>'.$row->leave_mode.'</td>
         <td>'.$row->is_paid.'</td>
         <td>'.$row->leave_type.'</td>
         <td>'.$row->total_days.'</td>
        
        <td>

<a href="/ShowLeaveDetail/'.$row->id.'">
<i class="fa fa-info-circle" aria-hidden="true" title="Detail View" style="font-size:20px"></i>
  </a>   
  </td>
          </tr>
          ';
            }  }
        
       else
        {
          //   echo "have aama jay che";
          $output = '
         <tr>
          <td align="center" colspan="6">No Data Found</td>
         </tr>
         ';
        }
  
        $data = array(
         'table_data'  => $output,
         'total_data'  => $total_row
        );
  
        echo json_encode($data);
      }
       
    public function allempinquiry(Request $request)
      {   
          $id = $request->session()->get('eid');
          $users = DB::table('inquiry')->where('emp_handle_by_fk',$id)->where('deleted_at',1)->where('inq_status','!=','done')->orderBy('created_at','asc')->paginate(10);
          return view('employee.show_inquiry',compact('users'));
      }
      public function empexchange(Request $request)
      {
        $eid = $request->session()->get('eid');
		
        $users = DB::table('users')->where('id','!=',$eid)->get()  ;
        return view('employee.exchange',compact('users'));
      }
      public function employeesalaryselect()
      {
        return view('employee.salaryslip');
      }
      public function employeesalaryreport(Request $request)
      {
      
      try{$selectData =  $_REQUEST['start'];
    $startData  = explode(',',$selectData);
   	$startYear = $startData[1];
    $startMonth = $startData[0];
     $id = $request->session()->get('eid');
     $monthName = date('F', mktime(0, 0, 0,$startMonth,10));
     //echo $id;
    $salaryDetail  = DB::table('salary_detail')
					->where('emp_fk',$id)->whereMonth('month_year','=',$startMonth)->whereyear('month_year','=',$startYear)->where('is_payment_done','yes')->get();
    //print_r($salaryDetail);
           // die;
           $user = DB::table('users')->where('id',$id)->get();
    $output = '<div class="myContainer" style="width: 980px; margin: 0 auto; overflow: hidden;">
                   <div class="head">
                    <div class="companyName">
                  <img src='.asset("images/Logo.jpg").' style="height:120px">
                  </div>
                <div class="salaryMonth"><b>
              Salary Slip For-</b> '.$monthName."-".$startYear.'
      </div>
  </div>
  <table class="empDetail">
   <tr>
      <th>
        Employ Id
      </th>
      <td>'.
       "JJ-EMP-" .$user[0]->id.'
     
      </td>
      </tr>
    <tr>
      <th>
        Employ Name
      </th>
      <td>'
       .ucfirst($user[0]->name)." " .ucfirst($user[0]->surname).'
     
      </td>
      <th>
       Bank Name:
      </th>
      <td>'
       .$user[0]->bank_name.'
      </th>
      </td>
    </tr>
    <tr>
      <th>
        Designation
      </th>
      <td>'.
       ucfirst($user[0]->designation).'
      </td>
      <th>
       Bank A/C no.
      </th>
      <td>
  '.$user[0]->acc_no.'
      </td>
    </tr>
    <tr>
      <th>
        Pan No
      </th>
      <td>'.
  $user[0]->pancard_id.'
      </td>
      <th>
      Payment Method
      </th>
      <td>'.
      $salaryDetail[0]->payment_type.'</td>
    </tr>
    <tr>
      <th>
      Joining Date 
      </th>
      <td>
    '.$user[0]->joining_date.'
      </td>
      
    </tr>
   
    <tr class="myBackground">
      <th colspan="2">
      Total Fixed Cash Compensation
      </th>
      <th colspan="2">
      Total Fixed Non-Cash Compensation
      </th>
    </tr>
    <tr>
      <th>
        Basic Salary
      </th>
      <td class="myAlign">'
       .$salaryDetail[0]->basic_salary.'
      </td>
      <th>
       Provident Fund
      </th>
      <td class="myAlign">
      '.$salaryDetail[0]->pf.'
      </td>
    </tr>
    <tr>
      <th>
      House Rent Allowance
      </th>
      <td class="myAlign">
      '.$salaryDetail[0]->house_rent.'
      </td>
      <th>
        Professional Tax
      </th>
      <td class="myAlign">
      '.$salaryDetail[0]->tax.'
      </td>
    </tr>
    <tr>
      <th>
      Other Allowance
      </th>
      <td class="myAlign">'
     .$salaryDetail[0]->otherallowance.'
      </td>
      <th>
      Gratuity
      </th>
      <td class="myAlign">
      '.$salaryDetail[0]->grratuti.'
      </td>
    </tr>
    <tr>
      <th>
      Employee Contribution to PF
      </th>
      <td class="myAlign">
      '.$salaryDetail[0]->pf.'
      </td>
      <th>
      Employee Contribution to PF
      </th>
      <td class="myAlign">'.
      $salaryDetail[0]->pf.'
      </td>
    </tr>
    <tr>
      <th>
      ESIC
      </th>
      <td class="myAlign">
      '.$salaryDetail[0]->esic.'
      </td>
      <th>
      Employee ESIC
      </th>
      <td class="myAlign">'.
      $salaryDetail[0]->emp_esic.'
      </td>
    </tr>
    <tr class="myTotalBackground">
      <th>
      Total Fixed Cash Compensation
      </th>
      <td>'.
       ($salaryDetail[0]->pf + $salaryDetail[0]->basic_salary + $salaryDetail[0]->otherallowance + $salaryDetail[0]->house_rent+$salaryDetail[0]->esic)
      .'</td>
     
      <th>
      Total Fixed Non-Cash Compensation
      </th>
      <td class="myAlign">'.
            ($salaryDetail[0]->pf + $salaryDetail[0]->tax + $salaryDetail[0]->grratuti+$salaryDetail[0]->emp_esic).'

      </td>
    </tr>
    <tr>
    <th>
    Total Leave Days
    </th>
    <td> '.
    $salaryDetail[0]->total_leave_days
    .'<th>
      
      Paid Leave Deduction
      </th>
      <td>'.
      $salaryDetail[0]->salary_leave_deducation
      .'</td>
     	</tr>
        <tr>
      
        <th>'.
        $salaryDetail[0]->label
        .'</th>
         <td>'.
        $salaryDetail[0]->other_charges
        .'<td>
        </tr>
        <tr>
        <th></th>
        <th></th>
       <th>
        Total Salary
        </th>
         <td>'.
			$salaryDetail[0]->total_amount
        .'<td>
      
        </tr>
    
    
   
  </table>
  <div class="tail">
    <div class="align-4">
     
    </div>
    <div class="align-4">
      
    </div>
    <div class="align-4">
      
    </div>
    <div class="align-4">
      ..............................................
    </div>
    <div class="align-2">
      (Employ Signature)
    </div>
    <div class="align-2">
      (Authorized Signature)
    </div>
  </div>
</div>';

$data = array(
  'table_data'  => $output,
 
 );
echo json_encode($data);
}
catch(Exception $e) {
  
  $output = 'No Data Found';
  $data = array(
    'table_data'  => $output,
   
   );

  // echo json_encode($data);
  
}
}
      public function updatecommentsemp(Request $request)
      {
        $eid = $request->session()->get('eid');
        $comments = $_POST['comments'];
        $inid = $_POST['inid'];
        $emp_handle_by_fk = $_POST['emp_handle_by_fk'];
        $employee =  DB::table('inquiry')->where('id',$inid)->update(['emp_handle_by_fk'=>$emp_handle_by_fk]);
        $data = array(
          'inq_id'=>$inid,
          'comments'=>$comments,
          'emp_id_fk'=>$eid
        );
        $comments = DB::table('inquiry_comments')->insert($data);
        Session::flash('inquiryTransfer', 'Inquiry Transfer Success');
         return redirect()->route('allempinquiry');
       
      }
      public function employeemembershippackage()
      {
        
        $packages = DB::table('plans')->where(['role'=>'customer'])->get();

        //dd($packages);
        return view('employee.membership', compact('packages'));
      }
      public function empinquirydetails($id)
     { 
        $inquries  = DB::table('inquiry')->join('users', 'users.id', '=', 'inquiry.emp_handle_by_fk')->select('*', 'inquiry.id as inqid')->where('inquiry.id',$id)
        ->get();

        $datacom = DB::table('inquiry_comments')->join('users', 'users.id', '=', 'inquiry_comments.emp_id_fk')->select('*', 'inquiry_comments.id as inqid','inquiry_comments.created_at as ByDate')->where(['inq_id'=>$id])->orderBy('ByDate','DESC')->paginate(10);
        return view('employee.detail_inquiry',compact('inquries','datacom'));
    }
    public function empaddComments()
    {$id = $_POST['id'];
      $eid = $_POST['eid'];
      $data = array(
            'comments'=>$_POST['comment'],
            'emp_id_fk'=>$_POST['eid'],
            'inq_id'=>$id

        );
       $ans =  DB::table('inquiry_comments')->insert($data);
       Session::flash('CommentsCreation', 'Comments Added');
       return back();
    }
    public function empeditUpdateform($id)
    {
        $inquiryData = DB::table('inquiry')->where('id',$id)->get();
    	$country = DB::table('countries')->get();
    	return view('employee.edit_inquiry',compact('inquiryData','country')); 
    }
    public function employeeeditinquiry(Request $request)
    {
        $id = $_POST['id'];
        $data = $request->except('_method', '_token');

        $temp2 = date('Y-m-d', strtotime($data['appointment_date']));
    	$temp = date('h:i:s',strtotime($data['app_time']));
    	$data['appointment_date'] = $temp2." ".$temp;
    
    
    $data = array(
        'first_name'=>$_POST['first_name'],
        'last_name'=>$_POST['last_name'],
        'mobile_no'=>$_POST['mobile_no'],
        'whatsup_no'=>$_POST['whatsup_no'],
        'appointment_date'=>$data['appointment_date'],
       	'inq_status'=>$data['inq_status'],
        'email'=>$_POST['email'],
        'country_id'=>$_POST['country_id'],
        'state_id'=>$_POST['state_id'],
        'city_id'=>$_POST['city_id']
        );
    if($_POST['inq_status'] == 'done')
    {
    	$current = date("Y-m-d");
    	$data['done_date'] = $current;
    	
    }

       
        $result = DB::table('inquiry')
        ->where('id',$id)
        ->update($data);
       Session::flash('UpdateInquiey', 'Inquiry Sucessfully Updated');
        return redirect()->route('allempinquiry');
    }
    public function employeeLeaveForm(Request $request)
    { $id = $request->session()->get('eid');
      $total_leave = DB::table('leaves_info')->get();
      $total_pl = $total_leave[0]->planned;
      $total_cl = $total_leave[0]->casual;

      $year = date("Y"); 
      $remaining_pl = DB::table('leaves')->whereYear('leave_date_start',$year)->where('approve_status','Approve')->where('employee_fk_id',$id)->where('leave_type','Pl')->sum('total_leave_days');

      $remaining_cl = DB::table('leaves')->whereYear('leave_date_start',$year)->where('approve_status','Approve')->where('employee_fk_id',$id)->where('leave_type','CL')->sum('total_leave_days');

      $remaining_pl = $total_pl - $remaining_pl;
      $remaining_cl = $total_cl - $remaining_cl;

    
    return view('employee.insert_leave',compact('remaining_pl','remaining_cl'));
    }
    public function createLeave(Request $request)
    {

      
      $id = $request->session()->get('eid');
      $reason = $_POST['reason'];
      $sdate = $_POST['sdate'];
      $edate = $_POST['edate'];
      $days = $_POST['days'];
      $leavemode = $_POST['leavemode'];
      $leavetype = $_POST['leavetype'];
      $cl = $_POST['cl'];
      $pl = $_POST['pl'];

      $images=array(); 
      if($files=$request->file('documentsleave')){
    
      foreach($files as $file){
          $name=time().$file->getClientOriginalName();
          $file->move('Leaves',$name);
          $images[]=$name;
      }
    }
    $image = implode(",", $images);
    function getSundays($startdate,$enddate) {
      
        $startweek=date("W",strtotime($startdate));
        $endweek=date("W",strtotime($enddate));
        $year=date("Y",strtotime($startdate));
        for($i=$startweek,$j=0;$i<=$endweek;$i++) {
         $result=getWeek($i,$year);
           if($result>$startdate&&$result<=$enddate) {
                $j++;
              }
        }
          return $j;
       
    }
    function getWeek($week, $year) {
      $dto = new DateTime();
      $result = $dto->setISODate($year, $week, 0)->format('Y-m-d');
      return $result;
    }
    $sundayCount =  getSundays($sdate,$edate);
    $year = date("Y");  
    $leavesdate = DB::table('leaves_info_details')->select('start_date')->whereYear('start_date',$year)->get();
    
   function check_in_range($start_date, $end_date,$date_from_user)
   {
 $start_ts = strtotime($start_date);
 $end_ts = strtotime($end_date);
 $user_ts = strtotime($date_from_user);
 if(($user_ts >= $start_ts && ($user_ts < $end_ts)))
 { 
         return 1;
 }
 else
 {
   
   return 0;
 } 
}
     foreach ($leavesdate as $value) {

    $resultOfLeaves = 0;
    $resultOfLeaves += check_in_range($sdate,$edate,$value->start_date);
  } // For each Closed
$count = round(abs(strtotime($sdate) - strtotime($edate))/86400) + 1;
$count =  $count - $sundayCount - $resultOfLeaves ;
if($leavemode == '0') // For The Half Day
{
  $count = $count/2;
}

     $data1 = array(
            'employee_fk_id'=>$id,
            'reason'=>$reason,
            'leave_date_start'=>$sdate,
            'leave_date_end'=>$edate,
            'leave_photos'=>$image,
            'leave_mode'=>$leavemode,
            'leave_type'=>$leavetype,
            'total_leave_days'=>$count
            
          );
      //dd($data1);
      $ans = DB::table('leaves')->insert($data1);

      $options = array(
        'cluster' => env('PUSHER_APP_CLUSTER'),
        'encrypted' => true
        );
$pusher = new Pusher(
            env('PUSHER_APP_KEY'),
            env('PUSHER_APP_SECRET'),
            env('PUSHER_APP_ID'), 
            $options
        );

  $username = DB::table('users')->where('id',$id)->get();
  $employeename = $username[0]->name;
  $count = DB::table('leaves')->where('approve_status','pending')->count();
$data['username'] = $employeename;
$data['message'] =  "Has Been Applied For";
$data['Leave_type'] = $leavetype;
$data['sdate']  = $sdate;
$data['edate'] = $edate;
$data['count'] = $count;

$pusher->trigger('leave-channel', 'App\\Events\\Leave',$data);

//Session::flash('CreateLeave', 'Leave Request Succefully Submited');

notify()->success('Leave Request Succefully Submited !');

return redirect()->route('employeeLeaveForm');
}


public function EmpLeaveResult($id)
{
 
    $data = array(
      'user_notification_status' =>1
    );
    $id = DB::table('leaves')->where('id',$id)->update($data);
    return redirect()->route('empdashboard');
}
    public function showPackage()
    {
        $packages = DB::table('packag_types')->where(['deleted_at'=>1])->get();
        return view('employee.show_packages', compact('packages'));
    }
    public function showtours(Request $request)
    {
 
      $plancat = Packagtype::where('deleted_at','1')->get();
      $packages = Toure::paginate(6);
      //dd($plancat,$request->all());
      if($request->session()->get('role') == 'admin')
      {
        return view('admin.show_tours', compact('packages','plancat'));
      }
      else{
        return view('employee.show_tours', compact('packages','plancat'));
      }
    }
    public function empinquieylive(Request $request)
    {
      $query = $_REQUEST['query'];
      // $sdate = $_REQUEST['sdate'];
      // $status = $_REQUEST['status'];
      $id = $request->session()->get('eid');
      
      $output = '';
      $data = [];
     // && $sdate != '' && $status !=''
    if($query != '')
      {
    
       $data = DB::table('inquiry')
         ->where('first_name', 'like', $query.'%')
         ->where('deleted_at',1)
         ->where('emp_handle_by_fk',$id)
         
         ->orWhere('last_name', 'like', $query.'%')
         ->orWhere('mobile_no', 'like', $query.'%')
         ->orWhere('id', 'like', $query)
         
         ->get();
         $total_row = $data->count();
       }
//        if($query == '' && $sdate != '')
//        {
//         $data = DB::table('inquiry')
//        ->where('created_at','like',$sdate)
//         ->where('inq_status','like',$status)
//         ->where('deleted_at',1)
//         ->where('emp_handle_by_fk',$id)
//         ->get();
//         $total_row = $data->count();
//         }
//         if($query != '' && $sdate == '')
//         {
//          $data = DB::table('inquiry')
//          ->where('first_name', 'like', $query.'%')
//          ->where('deleted_at',1)
//          ->where('emp_handle_by_fk',$id)
//          ->where('inq_status','like',$status)
//          ->orWhere('last_name', 'like', $query.'%')
//          ->orWhere('mobile_no', 'like', $query.'%')
//          ->get();
//          $total_row = $data->count();
//         }
//         if($query == '' && $sdate == '')
//         {
//             $data = DB::table('inquiry')
//           ->where('inq_status','like',$status)
//           ->where('deleted_at',1)
//           ->where('emp_handle_by_fk',$id)
//          ->get();
//          $total_row = $data->count();
//         }
      
      if($total_row > 0)
      {
       foreach($data as $row)
       {
           $a =  DB::table('users')->where('id',$row->emp_handle_by_fk)->get();
        
         
        $output .= '
        <tr>
         <td>'."JJ-INQ-".$row->id.'</td>
         <td>'.$row->first_name.'</td>
         <td>'.$row->mobile_no.'</td>
         <td>'.$row->email.'</td>
         <td>'.$row->appointment_date.'</td>
         <td>'.$row->inq_status.'</td>
         
         <td>
                      <a href="/empinquirydetails/'.$row->id.'">
                      
                      <i class="fa fa-info-circle" aria-hidden="true" title="Detail View" style="font-size:20px"></i>
                        </a>   

                      
                         <a href="/empeditUpdateform/'.$row->id.'">
                       
                         <i class="fa fa-edit" aria-hidden="true" title="Edit" style="font-size:20px"></i>
                         </a>
                         <a href="/empexchange/'.$row->id.'">
                       
                         <i class="fa fa-paper-plane" aria-hidden="true" title="Transfer Inquiry" style="font-size:20px"></i>
                    </a>

                </td>
        </tr>
        ';
       }
      }
     else
      {
        //   echo "have aama jay che";
        $output = '
       <tr>
        <td align="center" colspan="6">No Data Found</td>
       </tr>
       ';
      }

      $data = array(
       'table_data'  => $output,
       'total_data'  => $total_row
      );


      echo json_encode($data);
      
      }

      public function filterole()
      {
      
      $query = $_REQUEST['query'];
      $output = '';
      $data = [];
        if($query != '')
        {
        
        $data = DB::table('plans')
         ->where('role','=', $query)
         ->get();
         $total_row = $data->count();
        }

      //dd($data);

      if($total_row > 0)
      {
       foreach($data as $value)
       {
       
        $output .= '<div class="card box-shadow col-xl-4 col-md-6 mt-3" >
                        <div class="card-header">
                            <h4 class="py-md-4 py-xl-3 py-2"><center>'.$value->years." "."Years" .'</center></h4>
                        </div>

                        <div class="card-body">
                            <!-- <h5 class="card-title pricing-card-title">
                                <span class="align-top">$</span>59
                                <small class="text-muted">/ month</small>
                            </h5> -->
                           
                            <ul class="list-unstyled mt-3 mb-4">
                            <li class="py-2 border-bottom"><b>Role: </b>'.$value->role .'</li>
                       <li class="py-2 border-bottom"><b>Fixed Amount: </b>'.$value->fixed_amt .'</li>
                     <li class="py-2 border-bottom" style="text-align: justify;"><b>Mebership Fees:'.$value->mebership_fees.'</b></li>

                     <li class="py-2 border-bottom"><b>Free Nights Room:</b>'. $value->free_nights_room .'</li>
                     <li class="py-2 border-bottom"><b>Free Nights Avail In:</b>'. $value->free_nights_to_avail_in.'</li>
                     <li class="py-2 border-bottom"><b>Free Nights In Any Type Of Hotel: </b>'. $value->free_night_in_any_type_of_hotel.'</li>
                     <li class="py-2 border-bottom"><b>Monthly EMI:</b>'. $value->monthly_emi.'</li>
                     <li class="py-2 border-bottom"><b>Quarterly EMI:</b>'. $value->quarterly_emi.'</li>
                     <li class="py-2 border-bottom"><b>Anual EMI:</b>'. $value->annual_emi.'</li>

                     <li class="py-2 border-bottom"><b>Emi Dafulter Charges:</b>'. $value->emi_dafulter_charges.'</li>

                     <li class="py-2 border-bottom"><b>Flexbility To Move Free Night To Next Year:</b>'. $value->flexbility_to_move_free_night_to_next_year.'</li>

                     <li class="py-2 border-bottom"><b>Upgrade Plans:</b>'. $value->easy_upgrade_of_membership_plans.'</li>

                     <li class="py-2 border-bottom"><b>Referral:</b>'. $value->referral.'</li>
                     </ul>
                           
                        </div>
                        
                    </div>';
                  
                     
      } 

      $data = array(
       'table_data'  => $output,
       'total_data'  => $total_row
      );

      echo json_encode($data);
      }
    }

    public function tourdetail(Request $request,$id)
    {
      
      $result = DB::table('toures')->where('id',base64_decode($id))->get();
      $itenary = DB::table('itinerary')->where('tour_fk_id',$result[0]->id)->orderBy('day_number')->get();
      if($request->session()->get('role') == 'admin')
      {      return view('admin.tourDetails',compact('result','itenary')); }
      else{
      {   
          
       return view('employee.tourDetails',compact('result','itenary')); }

      }
    }
    public function empleaves_info()
    {
      $data = Leave::all();
        
        return view('employee.leaves', compact('data')); 
    }

    public function tourfill()
    {
      $query = $_REQUEST['query'];

      $output = '';
      $data = [];
        if($query != '')
        {
          if($query == 'All')
          {
            $data = DB::table('toures')->get();
            // dd($data);
          }
          else{
            $data = DB::table('toures')
                    ->where('package_type','=', $query)
                    ->get();
          }
            $total_row = $data->count();
        }
        if($total_row > 0)
        {
          foreach($data as $value)
          {
            $output .= '
                        <div class="card box-shadow col-xl-4 col-md-6 mt-3" >
                          <div class="card-header">
                            <h4 class="py-md-4 py-xl-3 py-2"><center>'.$value->name .'</center></h4>
                          </div>
                          <div class="card-body">
                            <img src="'.url("/media/").'/'.$value->banner_img.'" style="width:100%;height:300px"><br><br>
                            <a href="'.url('/tourdetail/').'/'.base64_encode($value->id).'">
                            <button type="button" class="btn btn-sm btn-block py-2" style="background:#f3941e;color:white">Details</button>
                            </a>
                            <br>
                             <a href="'.url('/updatetourform/').'/'.base64_encode($value->id).'">
                            <button type="button" class="btn btn-sm btn-block py-2" style="background:#f3941e;color:white">Update</button>
                            </a>
                            <br>
                            <a href="'.url('/tourdelete/').'/'.base64_encode($value->id).'">
                            <button type="button" class="btn btn-sm btn-block py-2" style="background:#f3941e;color:white">Delete</button>
                            </a>
                          </div>
                        </div>';              
          } 
    
        $data = array(
         'table_data'  => $output,
         'total_data'  => $total_row
        );

        echo json_encode($data);
      }else{
              $output = '
                        <div  style="margin-left: 500px; font-size:20px"  >
                         <b>No Data Found.</b></div>
                        ';   
              $data = array(
                 'table_data'  => $output,
                 'total_data'  => $total_row
                );

        echo json_encode($data);
      }
    }

    public function tourfilter()
    {
      $query = $_REQUEST['query'];
      $company = $_REQUEST['company'];
      $output = '';
      $data = [];
      if ($company != "" && $query == "") {
        if($company == "All"){
            $data = DB::table('toures')->paginate(6);
        }else{

            $data = DB::table('toures')->where('package_type','=',$company)->paginate(6);
        }
      }elseif ($company == "" && $query != "") {
       $data = DB::table('toures')->where('name','LIKE','%'.$query.'%')->paginate(6);
      }elseif ($company != "" && $query != ""){
         if($company == "All"){

           $data = DB::table('toures')->where('name','LIKE','%'.$query.'%')->paginate(6);
         }else{

            $data = DB::table('toures')->where('name','LIKE','%'.$query.'%')->where('package_type','=',$company)->paginate(6);
         }
      }
      $total_row = $data->count();
        if($total_row > 0)
        {
          foreach($data as $value)
          {
            $output .= '
                        <div class="card box-shadow col-xl-4 col-md-6 mt-3" >
                          <div class="card-header">
                            <h4 class="py-md-4 py-xl-3 py-2"><center>'.$value->name .'</center></h4>
                          </div>
                          <div class="card-body">
                            <img src="'.url("/media/").'/'.$value->banner_img.'" style="width:100%;height:300px"><br><br>
                            <a href="'.url('/tourdetail/').'/'.base64_encode($value->id).'">
                            <button type="button" class="btn btn-sm btn-block py-2" style="background:#f3941e;color:white">Details</button>
                            </a>
                            <br>
                             <a href="'.url('/updatetourform/').'/'.base64_encode($value->id).'">
                            <button type="button" class="btn btn-sm btn-block py-2" style="background:#f3941e;color:white">Update</button>
                            </a>
                            <br>
                            <a href="'.url('/tourdelete/').'/'.base64_encode($value->id).'">
                            <button type="button" class="btn btn-sm btn-block py-2" style="background:#f3941e;color:white">Delete</button>
                            </a>
                          </div>
                        </div>';              
          } 
        $output .= '<div style="margin-left:40%; margin-bottom:5%;" id="page"> '.$data->links().'</div>';
        $data = array(
         'table_data'  => $output,
         'total_data'  => $total_row
        );

        echo json_encode($data);
      }else{
              $output = '
                        <div  style="margin-left: 500px; font-size:20px"  >
                         <b>No Data Found.</b></div>
                        ';   
              $data = array(
                 'table_data'  => $output,
                 'total_data'  => $total_row
                );

        echo json_encode($data);
      }
    }
public function tourfilter1()
    {
      $query = $_REQUEST['query'];
      $company = $_REQUEST['company'];
      $output = '';
      $data = [];
        if($query != '')
        {

          if($query)
          { 
            // dd($query);
            $data = DB::table('toures')->where('name','LIKE','%'.$query.'%')->where('package_type','=',$company)->get();
          }else{
           
            $data = DB::table('toures')->get();
          }
            $total_row = $data->count();
          // dd($total_row);
        }
        if($total_row > 0)
        {
          foreach($data as $value)
          {
            $output .= '
                        <div class="card box-shadow col-xl-4 col-md-6 mt-3" >
                          <div class="card-header">
                            <h4 class="py-md-4 py-xl-3 py-2"><center>'.$value->name .'</center></h4>
                          </div>
                          <div class="card-body">
                            <img src="'.url("/media/").'/'.$value->banner_img.'" style="width:100%;height:300px"><br><br>
                            <a href="'.url('/tourdetail/').'/'.base64_encode($value->id).'">
                            <button type="button" class="btn btn-sm btn-block py-2" style="background:#f3941e;color:white">Details</button>
                            </a>
                            <br>
                             <a href="'.url('/updatetourform/').'/'.base64_encode($value->id).'">
                            <button type="button" class="btn btn-sm btn-block py-2" style="background:#f3941e;color:white">Update</button>
                            </a>
                            <br>
                            <a href="'.url('/tourdelete/').'/'.base64_encode($value->id).'">
                            <button type="button" class="btn btn-sm btn-block py-2" style="background:#f3941e;color:white">Delete</button>
                            </a>
                          </div>
                        </div>';              
          } 
    
        $data = array(
         'table_data'  => $output,
         'total_data'  => $total_row
        );

        echo json_encode($data);
      }else{
              $output = '
                       
                        <h4> No Data Found.</h4>
                        ';   
              $data = array(
                 'table_data'  => $output,
                 'total_data'  => $total_row
                );

        echo json_encode($data);
      }
    }

    /*****************Employee Logout ******************************************** */
      public function emplogout(Request $request)
    {
   
        $request->session()->forget('emp_email');
        $request->session()->forget('emp_name');
        $request->session()->forget('eid');
        // $request->session()->forget('_token');
        $request->session()->flush();
        // Session::flush();    
        // session_destroy();
        Session::flush();
       Auth::logout(); // logout user
        return redirect('/login');

        
        
        
    }
    
      /****************************************Employee All Perform routes ********** */
      
    //   *************************** Employee Dashboard performance route end ******
    public function createyourProfile(Request $request)
    {

      $last = last(request()->segments());
      $id = Crypt::decrypt($last);
      $checkUpdate = DB::table('users')->where('id',$id)->first();
        if(empty($checkUpdate))
        {
            echo "User Not Found";
        }else{
          if($checkUpdate->employee_approved == 1)
          {
              return view('employee.LinkNotFound');
          } else{
            $state = DB::table("states")
            ->where("country_id",101)
            ->pluck("name","id");
            $userDetail = DB::table("users")->where('id',$id)->get();
            return view('employee.createprofile',compact('state','userDetail'));
            //return view('admin.edit_employee_form',compact('state','userDetail'));
          }
        }
    }
    public function getCity(Request $request)
    {
        $stateid = $request->cat_id;
        $cities = DB::table("cities")
        ->where("state_id",$stateid)
        ->pluck("name","id");
        return response()->json([
            'cities' => $cities
        ]);
    }
	public function getState(Request $request)
    {
    	 $stateid = $request->cat_id;
        $states = DB::table("states")
        ->where("country_id",$stateid)
        ->pluck("name","id");
        return response()->json([
            'states' => $states
        ]);
    }
    public function updatenoCompanyajax(Request $request)
    {
        $no_company = $request->no_company;
        $id = $request->id;

        // $updateData = DB::table("users")->where("id",$id)->update(["no_company",$no_company]);

        $affected = DB::table('users')
              ->where('id', $id)
              ->update(['no_company' => $no_company]);
            //  echo $affected;
        //console.log($updateData);
      return 1;
      }
      public function sendemployeement($id)
      { 
        $username = Db::table('users')->select('name')->where('id',$id)->get();

        $dataupdate = DB::table('users')->where('id',$id)->update(
          [
            'send_employment'=>1
          ]
          );

        $options = array(
          'cluster' => env('PUSHER_APP_CLUSTER'),
          'encrypted' => true
          );
        $pusher = new Pusher(
              env('PUSHER_APP_KEY'),
              env('PUSHER_APP_SECRET'),
              env('PUSHER_APP_ID'), 
              $options
          );
          $count = DB::table('users')->where('send_employment',1)->count();
          $data = array(
              'id'=>$id,
              'username'=>$username[0]->name,
              'count'=>$count
          );
        
          $pusher->trigger('employeement-channel', 'App\\Events\\Employeement',$data);
          //  $pusher->trigger('employeement-channel', 'App\\Events\\Employeement',$data);


          Session::flash('Empmessage', 'You will get A mail when admin approve your request. After that you will ‘be’ able to Login.');

          return redirect()->route('employeeLogin');


      }
      // public function sendemployeement($id)
      // { 
      //   $username = Db::table('users')->select('name')->where('id',$id)->get();
      //   $options = array(
      //     'cluster' => env('PUSHER_APP_CLUSTER'),
      //     'encrypted' => true
      //     );
      //   $pusher = new Pusher(
      //         env('PUSHER_APP_KEY'),
      //         env('PUSHER_APP_SECRET'),
      //         env('PUSHER_APP_ID'), 
      //         $options
      //     );
      //     $e = DB::table('users')->where('id',$id)->update(['send_employment'=>1]);
      //     $countemp = DB::table('users')->where('send_employment',1)->where('employee_approved','0')->count();
      //     $data = array(
      //         'id'=>$id,
      //         'username'=>$username,
      //         'count'=>$countemp
      //     );
        
      //     $pusher->trigger('employeement-channel', 'App\\Events\\Employeement',$data);
      //     Session::flash('Empmessage', 'You Will Get Mail When Admin Approve Your Request . After That You Will Login ');

      //     return redirect()->route('employeeLogin');


      // }
      public function fillFamilyDetailsbyAjax(Request $request)
      {
      $data = $request->except('_method', '_token');
     
     
      if(isset($_POST['no_member']))
      {
       $no_member = $_POST['no_member'];
      }
      else 
      {
      $no_member = 0;
      }
       
        $id = $_POST['id'];
        $prefix  = $_POST['prefix'];
        $name = $_POST['name'];
        $mobile = $_POST['mobile'];
        $email = $_POST['email'];
        $address = $_POST['address'];
       
        $count = count($name);
        $in = [];
    
        for($i=0;$i<$count;$i++){
       
              $in[] = array(
              'prefix'=> $prefix[$i],
              'name' =>$name[$i],
              'mobile' =>$mobile[$i],
              'email'=>$email[$i],
              'address'=>$address[$i],
             
              );
            }
      $result = json_encode($in);

      $ans =   DB::table('users')
      ->where('id', $id)
      ->update(['no_member'=>$no_member]);

      $ans =   DB::table('users')
      ->where('id',$id)
      ->update(['family_details'=>$result]);

      return view("admin.show_employee");
      
      }
      public function fillEmployeemenDetailsbyAjax(Request $request)
      {
        $data = $request->except('_method', '_token');
        $id = $_POST['id'];
        $no_company = $_POST['no_company'];
        $companyname  = $_POST['companyname'];
        $sdata = $_POST['sdate'];
        $edate = $_POST['edate'];
        $designation = $_POST['designation'];
        $salary = $_POST['salary'];
        $reason = $_POST['reason'];
        $count = count($companyname);
        $in = [];
   
        for($i=0;$i<$count;$i++){
       
              $in[] = array(
              
              'companyname'=> $companyname[$i],
              'sdate' =>$sdata[$i],
              'edate' =>$edate[$i],
              'designation'=>$designation[$i],
              'salary'=>$salary[$i],
              'reason' => $reason[$i]
              );
            
          
          }
       $ans =   DB::table('users')
      ->where('id',$id)
      ->update(['no_company'=>$no_company]);

      $result = json_encode($in);

      $ans =   DB::table('users')
      ->where('id',$id)
      ->update(['employeement_details'=>$result]);
       return back()->withInput(['tab'=>'documents']);
      
     
   }
 function empPersonalDetail(Request $request)
{
 $id = $_POST['id'];
   $data = $request->except('_method', '_token');
 
 $check = DB::table('users')->select('password')->where('id',$id)->get();
  $img = DB::table('users')->select('avatar')->where('id',$id)->get();
 $img = $img[0]->avatar;
if($check[0]->password == NULL)
   {	$validator = Validator::make($request->all(),[
           'prefix' => 'required',
     			 'name'=>'required|alpha|max:30',
     			 'middlename'=>'required|alpha|max:30',
     			 'surname'=>'required|alpha|max:30',
     			 'contact_number'=>'required|digits:10',
     			 'password'=>'required|max:20|min:6',
     			 'whatsup_number'=>'required|digits:10',
     			 'photo'=>'required|file|mimes:jpeg,jpg,png|required'
     ]);
   if ($validator->fails()) {
    $message = $validator->messages();
   	Session::flash('EmployeementDetail',$message);
             return  redirect()->back()->withInput()->withErrors($validator->errors());
              } else{
   			 if(isset($_POST['password']))
        	{
            $data['password'] = Hash::make($_POST['password']);
            // echo $_POST['password'];
            // echo $data['password'];
            // die;

        	}
          $file = $request->file('photo');
          $name = time().$file->getClientOriginalName();
          $data['avatar'] = $name;
        	$data['photo']=$name;
          $file->move('./avatar',$name);
   
   		$ans = DB::table("users")
        ->where("id",$id)
        ->update($data);
   return back()->withInput(['tab'=>'address']);
     	}} // User First time Profile fill kre che ,, 
 		else
 		{		$validator = Validator::make($request->all(),[
                 'prefix' => 'required',
     			 'name'=>'required|alpha|max:30',
     			 'middlename'=>'required|alpha|max:30',
     			 'surname'=>'required|alpha|max:30',
     			 'contact_number'=>'required|digits:10',
     			'whatsup_number'=>'required|digits:10',
     			]);
        		if($_POST['password'] == '')
                {
                	$data['password'] = $check[0]->password;
                }
         		
         		if($_POST['password'] != '')
                {
                	$data['password'] = Hash::make($_POST['password']);
            		echo $_POST['password'];
            		echo $data['password'];
                }
        		if($_FILES['photo']['size'] == 0)
                {
                	$data['photo'] = $img ;
                	$data['avatar'] = $img;
               }
       		 if($_FILES['photo']['size'] != 0)
        	{	$file = $request->file('photo');
          $name = time().$file->getClientOriginalName();
          $data['avatar'] = $name;
        	$data['photo']=$name;
          $file->move('./avatar',$name);
             
        	}
         if($request->session()->get('role') == 'admin')
         {
         	if(isset($_POST['executives']))
            {
            	$data['executives'] = 1;
            }
         	else 
            {
            	$data['executives'] = 0;
            }
         	if(isset($_POST['emp_help_desk_assign']))
            {
            	$data['emp_help_desk_assign'] = 1;
            }
         else {
         $data['emp_help_desk_assign'] = 0;
         }
         }
         	
         	
       
         $ans = DB::table("users")
       	 		->where("id",$id)
        		->update($data);
     
        
         return back()->withInput(['tab'=>'address']);
         
         
        	}
 			
 }
function fillloyeemenDetailsdocuments(Request $request)
{
		$id = $_POST['id'];
   $data = $request->except('_method', '_token');
 	$check = DB::table('users')->select('*')->where('id',$id)->get();
  	if($check[0]->aadhar_card_id == NULL)
    {
    		// First Time Doc Fill kre che .... 
    		$validator = Validator::make($request->all(),[
                 'aadhar_card_id' => 'required',
     			 'aadhar_card_pic'=>'required|mimes:pdf',
     			 'pancard_id'=>'required',
     			 'pancard_photo'=>'required|mimes:pdf',
     			 'driving_id'=>'required',
            	'passport_id'=>'required',
            	'passport_date'=>'required',
            	'driving_photo'=>'required|mimes:pdf',
            	'marksheet_10th_photo'=>'required|mimes:pdf',
            	'marksheet_12th_photo'=>'required|mimes:pdf',
            	'marksheet_bachelors_photo'=>'required|mimes:pdf',
            	'marksheet_masters_photo'=>'required|mimes:pdf',
            	'any_other_certificates.*'=>'required|mimes:pdf',
            	'offerLetter'=>'required|mimes:pdf',
            	'appointment'=>'required|mimes:pdf',
            	'experience'=>'required|mimes:pdf',
            	'resignation'=>'required|mimes:pdf',
            	'increment'=>'required|mimes:pdf',
            	'salaryslips.*'=>'required|mimes:pdf',
            	'any_other_certificates.*'=>'required|mimes:pdf',
            	'salaryslips.*'=>'required|mimes:pdf'
            ]);
    		if ($validator->fails()) {
    		$message = $validator->messages();
   			Session::flash('EmployeementDetail',$message);
             return  redirect()->back()->withInput()->withErrors($validator->errors());
            	  //return back()->withInput(['tab'=>'documents'])->withErrors($validator->errors());
              }
    		else
            {
      if(isset($_FILES['aadhar_card_pic'])){
    $aadhar_card_pic=$request->file('aadhar_card_pic');
    $aadhar_card_picname=time().$aadhar_card_pic->getClientOriginalName();  
    $destinationPath = 'uploads';
    $aadhar_card_pic->move($destinationPath,$aadhar_card_picname);
    $data['aadhar_card_pic'] = $aadhar_card_picname;
  	}
if(isset($_FILES['pancard_photo'])){
      $pancard_photo=$request->file('pancard_photo');
        $pancard_photoname=time().$pancard_photo->getClientOriginalName();  
        $destinationPath = 'uploads';
        $pancard_photo->move($destinationPath,$pancard_photoname);
        $data['pancard_photo'] = $pancard_photoname;
}	if(isset($_FILES['driving_photo'])){
          $driving_photo=$request->file('driving_photo');
            $driving_photoname=time().$driving_photo->getClientOriginalName();  
            $destinationPath = 'uploads';
            $driving_photo->move($destinationPath,$driving_photoname);
            $data['driving_photo'] = $driving_photoname;
            }
		if(isset($_FILES['marksheet_10th_photo'])){
              $marksheet_10th_photo = $request->file('marksheet_10th_photo');
                $marksheet_10th_photoname=time().$marksheet_10th_photo->getClientOriginalName();  
                $destinationPath = 'uploads';
                $marksheet_10th_photo->move($destinationPath,$marksheet_10th_photoname);
                $data['marksheet_10th_photo'] = $marksheet_10th_photoname;
                }
            if(isset($_FILES['marksheet_12th_photo'])){
        $marksheet_12th_photo=$request->file('marksheet_12th_photo');
        $marksheet_12th_photoname=time().$marksheet_12th_photo->getClientOriginalName();  
         $destinationPath = 'uploads';
         $marksheet_12th_photo->move($destinationPath,$marksheet_12th_photoname);
         $data['marksheet_12th_photo'] = $marksheet_12th_photoname;
                        }
	if(isset($_FILES['marksheet_bachelors_photo'])){
          $marksheet_bachelors_photo=$request->file('marksheet_bachelors_photo');
    $marksheet_bachelors_photoname=time().$marksheet_bachelors_photo->getClientOriginalName();  
                        $destinationPath = 'uploads';
        $marksheet_bachelors_photo->move($destinationPath,$marksheet_bachelors_photoname);
        $data['marksheet_bachelors_photo'] = $marksheet_bachelors_photoname;
                        }
	if(isset($_FILES['marksheet_masters_photo'])){
          $marksheet_masters_photo=$request->file('marksheet_masters_photo');
        $marksheet_masters_photoname=time().$marksheet_masters_photo->getClientOriginalName();  
            $destinationPath = 'uploads';
                $marksheet_masters_photo->move($destinationPath,$marksheet_masters_photoname);
                $data['marksheet_masters_photo'] = $marksheet_masters_photoname;
          }
          if(isset($_FILES['passport_photo'])){
          $passport_photo=$request->file('passport_photo');
        $passport_photoname=time().$passport_photo->getClientOriginalName();  
            $destinationPath = 'uploads';
                $passport_photo->move($destinationPath,$passport_photoname);
                $data['passport_photo'] = $passport_photoname;
          }


   if(isset($_FILES['any_other_certificates'])){
          $any_other_certificates=$request->file('any_other_certificates');

        foreach($any_other_certificates as $file){
            $name=time().$file->getClientOriginalName();
            $file->move('uploads',$name);
            $images[]=$name;
          }
          	$any_other_certificatesname=implode(',',$images);
         	$data['any_other_certificates'] = $any_other_certificatesname;
        }
            if(isset($_FILES['appointment']))
            {				
         		 $appointment=$request->file('appointment');
            		$appointmentname=time().$appointment->getClientOriginalName();  
            			 $destinationPath = 'uploads';
                		 $appointment->move($destinationPath,$appointmentname);
                			$data['appointment'] = $appointmentname;
            }
              if(isset($_FILES['offerLetter']))
            {				
         		 $offerLetter=$request->file('offerLetter');
            		$offerLettername=time().$offerLetter->getClientOriginalName();  
            			 $destinationPath = 'uploads';
                		 $offerLetter->move($destinationPath,$offerLettername);
                			$data['offerLetter'] = $offerLettername;
            }
            	if(isset($_FILES['experience']))
                {
                	 $experience=$request->file('experience');
            		$oexperiencename=time().$experience->getClientOriginalName();  
            			 $destinationPath = 'uploads';
                		 $experience->move($destinationPath,$oexperiencename);
                			$data['experience'] = $oexperiencename;
                	
                }
            	if(isset($_FILES['resignation']))
                {
                	
                	 $resignation=$request->file('resignation');
            		$resignationname=time().$resignation->getClientOriginalName();  
            			 $destinationPath = 'uploads';
                
                		 $resignation->move($destinationPath,$resignationname);
                			$data['resignation'] = $resignationname;
                	
                }
          
            	if(isset($_FILES['statment']))
                {
                	 $statment=$request->file('statment');
            		$statmentname=time().$statment->getClientOriginalName();  
            			 $destinationPath = 'uploads';
               		 $data['statment'] = $statmentname;
                	
                		
                		 $statment->move($destinationPath,$statmentname);
                			
                	
                }
             
            	if(isset($_FILES['increment']))
                {
                	 $increment=$request->file('increment');
            		$incrementname=time().$increment->getClientOriginalName();  
            			 $destinationPath = 'uploads';
                		 $increment->move($destinationPath,$incrementname);
                			$data['increment'] = $incrementname;
                	
                }
            
          		if(isset($_FILES['salaryslips'])){
          $salaryslips=$request->file('salaryslips');

        foreach($salaryslips as $file){
            $name=time().$file->getClientOriginalName();
            $file->move('uploads',$name);
            $images[]=$name;
          }
          	$salaryslipsname=implode(',',$images);
         	$data['salaryslips'] = $salaryslipsname;
        }
            $ans = DB::table("users")
        	->where("id",$id)
        	->update($data);
            
	  return back()->withInput(['tab'=>'family']);
   

            }    
    }
    else { // User Update kartoo hy to ....
    
    	 if($_FILES['aadhar_card_pic']['size'] != 0){
       		
         	$validator = Validator::make($request->all(),[
                'aadhar_card_pic'=>'mimes:pdf',
              ]);
         			if ($validator->fails()) {
    			$message = $validator->messages();
   				// return  redirect()->back()->withInput()->withErrors($validator->errors());
            	  return back()->withInput(['tab'=>'documents'])->withErrors($validator->errors());
                    }
        // echo "working hrre also..";
         	  $aadhar_card_pic=$request->file('aadhar_card_pic');
    $aadhar_card_picname=time().$aadhar_card_pic->getClientOriginalName();  
    $destinationPath = 'uploads';
    $aadhar_card_pic->move($destinationPath,$aadhar_card_picname);
    $data['aadhar_card_pic'] = $aadhar_card_picname;
       
        
         }
    
    	if($_FILES['aadhar_card_pic']['size'] == 0)
        {
        echo "file 0 thai aama ..";
        	$data['aadhar_card_pic'] = $check[0]->aadhar_card_pic;
        }
   
  
   if($_FILES['pancard_photo']['size'] != 0)
    { 
   		$validator = Validator::make($request->all(),[
                'pancard_photo'=>'mimes:pdf',
              ]);
         			if ($validator->fails()) {
    			$message = $validator->messages();
   				// return  redirect()->back()->withInput()->withErrors($validator->errors());
            	  return back()->withInput(['tab'=>'documents'])->withErrors($validator->errors());
                    }
   		$pancard_photo=$request->file('pancard_photo');
        $pancard_photoname=time().$pancard_photo->getClientOriginalName();  
        $destinationPath = 'uploads';
        $pancard_photo->move($destinationPath,$pancard_photoname);
        $data['pancard_photo'] = $pancard_photoname;
    }
   if($_FILES['pancard_photo']['size'] == 0)
    {
   	 $data['pancard_photo'] = $check[0]->pancard_photo;
    		
    }
  if($_FILES['driving_photo']['size'] != 0)
    {
  			$validator = Validator::make($request->all(),[
                'driving_photo'=>'mimes:pdf',
              ]);
         			if ($validator->fails()) {
    			$message = $validator->messages();
   				// return  redirect()->back()->withInput()->withErrors($validator->errors());
            	  return back()->withInput(['tab'=>'documents'])->withErrors($validator->errors());
                    }
    	 $driving_photo=$request->file('driving_photo');
            $driving_photoname=time().$driving_photo->getClientOriginalName();  
            $destinationPath = 'uploads';
            $driving_photo->move($destinationPath,$driving_photoname);
            $data['driving_photo'] = $driving_photoname;
    }
   if($_FILES['driving_photo']['size'] == 0)
    {
    		 $data['driving_photo'] = $check[0]->driving_photo;
    }
     if($_FILES['passport_photo']['size'] != 0)
    {
  			$validator = Validator::make($request->all(),[
                'passport_photo'=>'mimes:pdf',
              ]);
         			if ($validator->fails()) {
    			$message = $validator->messages();
   				// return  redirect()->back()->withInput()->withErrors($validator->errors());
            	  return back()->withInput(['tab'=>'documents'])->withErrors($validator->errors());
                    }
    	 $passport_photo=$request->file('passport_photo');
            $passport_photoname=time().$passport_photo->getClientOriginalName();  
            $destinationPath = 'uploads';
            $passport_photo->move($destinationPath,$passport_photoname);
            $data['passport_photo'] = $passport_photoname;
    }
   if($_FILES['passport_photo']['size'] == 0)
    {
    		 $data['passport_photo'] = $check[0]->passport_photo;
    }
   if($_FILES['marksheet_10th_photo']['size'] != 0)
    {	
   			
   				$validator = Validator::make($request->all(),[
                'marksheet_10th_photo'=>'mimes:pdf',
              ]);
         			if ($validator->fails()) {
    			$message = $validator->messages();
   				// return  redirect()->back()->withInput()->withErrors($validator->errors());
            	  return back()->withInput(['tab'=>'documents'])->withErrors($validator->errors());
                    }
    		 $marksheet_10th_photo = $request->file('marksheet_10th_photo');
                $marksheet_10th_photoname=time().$marksheet_10th_photo->getClientOriginalName();  
                $destinationPath = 'uploads';
                $marksheet_10th_photo->move($destinationPath,$marksheet_10th_photoname);
                $data['marksheet_10th_photo'] = $marksheet_10th_photoname;
    }
   if($_FILES['marksheet_10th_photo']['size'] == 0)
    {
    	 $data['marksheet_10th_photo'] = $check[0]->marksheet_10th_photo;
    }
     if($_FILES['marksheet_12th_photo']['size'] != 0){
     	$validator = Validator::make($request->all(),[
                'marksheet_12th_photo'=>'mimes:pdf',
              ]);
         			if ($validator->fails()) {
    			$message = $validator->messages();
   				// return  redirect()->back()->withInput()->withErrors($validator->errors());
            	  return back()->withInput(['tab'=>'documents'])->withErrors($validator->errors());
                    }
        $marksheet_12th_photo=$request->file('marksheet_12th_photo');
        $marksheet_12th_photoname=time().$marksheet_12th_photo->getClientOriginalName();  
         $destinationPath = 'uploads';
         $marksheet_12th_photo->move($destinationPath,$marksheet_12th_photoname);
         $data['marksheet_12th_photo'] = $marksheet_12th_photoname;
                        }
     if($_FILES['marksheet_10th_photo']['size'] == 0)
    {
    	 $data['marksheet_12th_photo'] = $check[0]->marksheet_12th_photo;
    }
    if($_FILES['marksheet_bachelors_photo']['size'] != 0)
    {
    		$validator = Validator::make($request->all(),[
                'marksheet_bachelors_photo'=>'mimes:pdf',
              ]);
         			if ($validator->fails()) {
    			$message = $validator->messages();
   				// return  redirect()->back()->withInput()->withErrors($validator->errors());
            	  return back()->withInput(['tab'=>'documents'])->withErrors($validator->errors());
                    }
          $marksheet_bachelors_photo=$request->file('marksheet_bachelors_photo');
    	  $marksheet_bachelors_photoname=time().$marksheet_bachelors_photo->getClientOriginalName();  
           $destinationPath = 'uploads';
        $marksheet_bachelors_photo->move($destinationPath,$marksheet_bachelors_photoname);
        $data['marksheet_bachelors_photo'] = $marksheet_bachelors_photoname;
                        }
    	if($_FILES['marksheet_bachelors_photo']['size'] == 0)
        {
        	$data['marksheet_bachelors_photo'] = $check[0]->marksheet_bachelors_photo;
    	}
  	if($_FILES['marksheet_masters_photo']['size'] != 0){
    	$validator = Validator::make($request->all(),[
                'marksheet_masters_photo'=>'mimes:pdf',
              ]);
         			if ($validator->fails()) {
    			$message = $validator->messages();
   				// return  redirect()->back()->withInput()->withErrors($validator->errors());
            	  return back()->withInput(['tab'=>'documents'])->withErrors($validator->errors());
                    }
    
          $marksheet_masters_photo=$request->file('marksheet_masters_photo');
        $marksheet_masters_photoname=time().$marksheet_masters_photo->getClientOriginalName();  
            $destinationPath = 'uploads';
                $marksheet_masters_photo->move($destinationPath,$marksheet_masters_photoname);
                $data['marksheet_masters_photo'] = $marksheet_masters_photoname;
          }
    if($_FILES['marksheet_masters_photo']['size'] == 0) {
     $data['marksheet_masters_photo'] = $check[0]->marksheet_masters_photo;
    }
    if(!empty($_FILES['any_other_certificates']['error'])){
    		$validator = Validator::make($request->all(),[
                'any_other_certificates.*'=>'mimes:pdf',
              ]);
         			if ($validator->fails()) {
    			$message = $validator->messages();
   				// return  redirect()->back()->withInput()->withErrors($validator->errors());
            	  return back()->withInput(['tab'=>'documents'])->withErrors($validator->errors());
                    }
          $any_other_certificates=$request->file('any_other_certificates');

        foreach($any_other_certificates as $file){
            $name=time().$file->getClientOriginalName();
            $file->move('uploads',$name);
            $images[]=$name;
          }
          	$any_other_certificatesname=implode(',',$images);
         	$data['any_other_certificates'] = $any_other_certificatesname;
        }
   if(empty($_FILES['any_other_certificates']['error']))
    {
    	$data['any_other_certificates'] = $check[0]->any_other_certificates;
    }
    if($_FILES['appointment']['size'] != 0)
            {				
    			$validator = Validator::make($request->all(),[
                'appointment'=>'mimes:pdf',
              ]);
         			if ($validator->fails()) {
    			$message = $validator->messages();
   				// return  redirect()->back()->withInput()->withErrors($validator->errors());
            	  return back()->withInput(['tab'=>'documents'])->withErrors($validator->errors());
                    }
         		 $appointment=$request->file('appointment');
            		$appointmentname=time().$appointment->getClientOriginalName();  
            			 $destinationPath = 'uploads';
                		 $appointment->move($destinationPath,$appointmentname);
                			$data['appointment'] = $appointmentname;
            }
    if($_FILES['appointment']['size'] == 0)
    {
    	$data['appointment'] = $check[0]->appointment;
    }
    if($_FILES['offerLetter']['size'] != 0)
            {				$validator = Validator::make($request->all(),[
                'offerLetter'=>'mimes:pdf',
              ]);
         			if ($validator->fails()) {
    			$message = $validator->messages();
   				// return  redirect()->back()->withInput()->withErrors($validator->errors());
            	  return back()->withInput(['tab'=>'documents'])->withErrors($validator->errors());
                    }
         		 $offerLetter=$request->file('offerLetter');
            		$offerLettername=time().$offerLetter->getClientOriginalName();  
            			 $destinationPath = 'uploads';
                		 $offerLetter->move($destinationPath,$offerLettername);
                			$data['offerLetter'] = $offerLettername;
            }
    if($_FILES['offerLetter']['size'] == 0)
    {
    	$data['offerLetter'] = $check[0]->offerLetter;
    }
   if($_FILES['experience']['size'] != 0)
                {
   					$validator = Validator::make($request->all(),[
                'experience'=>'mimes:pdf',
              ]);
         			if ($validator->fails()) {
    			$message = $validator->messages();
   				// return  redirect()->back()->withInput()->withErrors($validator->errors());
            	  return back()->withInput(['tab'=>'documents'])->withErrors($validator->errors());
                    }
                	 $experience=$request->file('experience');
            		$oexperiencename=time().$experience->getClientOriginalName();  
            			 $destinationPath = 'uploads';
                		 $experience->move($destinationPath,$oexperiencename);
                			$data['experience'] = $oexperiencename;
                	
                }
    	if($_FILES['experience']['size'] == 0){
        	$data['experience'] = $check[0]->experience;
        }
   if($_FILES['resignation']['size'] != 0)
                {$validator = Validator::make($request->all(),[
                'resignation'=>'mimes:pdf',
              ]);
         			if ($validator->fails()) {
    			$message = $validator->messages();
   				// return  redirect()->back()->withInput()->withErrors($validator->errors());
            	  return back()->withInput(['tab'=>'documents'])->withErrors($validator->errors());
                    }
                	 $resignation=$request->file('resignation');
            		$resignationname=time().$resignation->getClientOriginalName();  
            			 $destinationPath = 'uploads';
                		 $resignation->move($destinationPath,$resignationname);
                			$data['experience'] = $resignationname;
                	
                }
    			if($_FILES['resignation']['size'] == 0){
                
                		$data['resignation'] = $check[0]->resignation;
                }
            	
    		if($_FILES['increment']['size'] != 0)
                {
            		$validator = Validator::make($request->all(),[
                'increment'=>'mimes:pdf',
              ]);
         			if ($validator->fails()) {
    			$message = $validator->messages();
   				// return  redirect()->back()->withInput()->withErrors($validator->errors());
            	  return back()->withInput(['tab'=>'documents'])->withErrors($validator->errors());
                    }
                	 $increment=$request->file('increment');
            		$incrementname=time().$increment->getClientOriginalName();  
            			 $destinationPath = 'uploads';
                		 $increment->move($destinationPath,$incrementname);
                			$data['statment'] = $incrementname;
                	
                }
    		if($_FILES['increment']['size'] == 0){$data['statment'] = $check[0]->increment;
    			}
    
    		if(empty($_FILES['salaryslips']['error'])){
            echo "file ma jay che .. pan selected nthi..";
            $validator = Validator::make($request->all(),[
                'salaryslips.*'=>'mimes:pdf',
              ]);
         			if ($validator->fails()) {
    			$message = $validator->messages();
   				 return  redirect()->back()->withInput()->withErrors($validator->errors());
            	  //return back()->withInput(['tab'=>'documents'])->withErrors($validator->errors());
                    }
           
          $salaryslips=$request->file('salaryslips');

        foreach($salaryslips as $file){
            $name=time().$file->getClientOriginalName();
            $file->move('uploads',$name);
            $images[]=$name;
          }
          	$salaryslipsname=implode(',',$images);
         	$data['salaryslips'] = $salaryslipsname;
        }
    	if(!empty($_FILES['salaryslips']['error'])) {
        
        	$data['salaryslips'] = $check[0]->salaryslips;
        }
    	//print_r($data);
       
    	$ans = DB::table("users")
        	->where("id",$id)
        	->update($data);
        
        //echo $ans;
            
	  return back()->withInput(['tab'=>'family']);
     // Else Close ... 
    }
  } // Function Close
     function fillEmployeemenDetails(Request $request)
    {
    $flag = 0;
	$id = $_POST['id'];
      $data = $request->except('_method', '_token');
        if(isset($_POST['password']))
        {
            $data['password'] = Hash::make($_POST['password']);
            echo $_POST['password'];
            echo $data['password'];
        }
     	if(isset($_POST['c_house_no']))
        {
        	$flag = 2; // Address Panwel 
        }
       
		$ans = DB::table("users")
        ->where("id",$id)
        ->update($data);
     
     if($flag == 2)
     {
     	return back()->withInput(['tab'=>'employee']);

     } }

    
   
            
        
    
    

}