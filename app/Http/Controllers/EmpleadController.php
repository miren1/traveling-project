<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Auth;
use App\Group;
use DB;
use App\Grouplead;
use App\User;
use App\Assignlead;
use App\Callstatus;
use App\Leadstatus;


class EmpleadController extends Controller
{
   public function index()
   {
        $data['user']=User::where('id','!=',Auth::user()->id)->where('role','user')->where('employee_approved','1')->get();

        $data['admins']=User::where('id','!=',Auth::user()->id)->get();
        $data['group']=Group::where('is_delete','0')->get();
        return view('employee.leads.index')->with($data);
   }

   public function list(Request $request, $id=null,$group=null)
   {
        $user_id=Auth::user()->id;
        $Assignlead=Assignlead::with('lead','group','user')->withCount('activity')->where(['employee_id'=>$user_id,'status'=>'active']);
       
        if(isset($request->group_id)){
            $Assignlead->whereHas('group', function($q) use($request){
                $q->where('id',$request->group_id);
            });
        }
        
        if(isset($request->user_id)){
            $Assignlead->whereHas('user', function($q) use($request){
                $q->where('id',$request->user_id);
            });
        }

        if(isset($request->activity_date)){
            $Assignlead->whereHas('activity', function($q) use($request){
                $q->where('date_time','<=',$request->activity_date);
            });
        }
       
        
        return datatables()->of($Assignlead->orderBy('created_at', 'desc'))->toJson();   
   }
   public function view($id = null)
   {
    
    $data['grouplead']=Grouplead::with('group')->find(base64_decode($id));

    $data['callstatus']=Callstatus::where('is_delete','0')->get();
    
    $data['assignlead']= Assignlead::where(['status'=>'active','employee_id'=>Auth::user()->id,'group_lead_id'=>base64_decode($id)])->first();
   // dd($data);
    return view('employee.leads.view')->with($data);
   }

   public function assign_employee(Request $request)
    {
   // dd($request->all());
    if($request->id){
        $dataArray=$request->id;
        foreach($dataArray as $id){
           // dd($id);
            $Assignlead= Assignlead::where(['id'=>$id])->first();
            $save= $Assignlead->update(['status'=>'close']); 
            //dd($save);
            if($Assignlead){
                $info= array(
                    'user_id'=>Auth::user()->id,
                    'employee_id'=>$request->employee_id,
                    'group_lead_id'=>$Assignlead->group_lead_id,
                    'group_id'=>$Assignlead->group_id,
                );
                //dd($info);
                Assignlead::create($info);
            }else{
                continue;
            }
           
        }
        $data['message']= 'Lead Assign to Employee Successfully';
        $data['status']= 'success';
        

     }else{
        $data['message']= 'Lead Not Assign to Employee Successfully';
        $data['status']= 'falil';
     }
     return  json_encode($data);

    }

    public function assign_list()
    {
         $user_id=Auth::user()->id;     
         return datatables()->of(Assignlead::with('lead','group','employee')->withCount('activity')->where(['user_id'=>$user_id,'status'=>'active'])->orderBy('created_at', 'desc'))->toJson();   
    }


    public function user_list()
    {
        $user_id=Auth::user()->id;
        $Assignlead=Assignlead::with('lead','group','user')->whereHas('user', function($q){
            $q->where('role','user');
        })->where(['employee_id'=>$user_id,'status'=>'active']);
        
        return datatables()->of($Assignlead)->toJson();   
   }

   public function status_list($id=null)
    {
        $Assignlead=Leadstatus::with('employee','call_status')->where(['group_lead_id'=>base64_decode($id)])->orderBy('created_at', 'desc');
        return datatables()->of($Assignlead)->toJson();   
   }


   public function assign_status_list($id=null)
   {
        $Assignlead=Assignlead::with('employee','user')->where(['group_lead_id'=>base64_decode($id)])->orderBy('created_at', 'desc');
        return datatables()->of($Assignlead)->toJson();   
   }


   public function store_lead(Request $request)
   {
    $dataArray= array();
    $dataArray['user_id']=Auth::user()->id;
    $dataArray['employee_id']=Auth::user()->id;
    if($request->group_lead_id){
        $dataArray['group_lead_id']=$request->group_lead_id;
        $Grouplead=Grouplead::find($request->group_lead_id);
        if($Grouplead){
            $dataArray['group_id']=$Grouplead->group_id;
        }
    }
    if($request->call_status_id){
        $dataArray['call_status_id']=$request->call_status_id;
    }
    if($request->remarks){
        $dataArray['remarks']=$request->remarks;
    }
    if($request->call_status){
        $dataArray['call_status']=$request->call_status;
    }
    if($request->date_time){
        $dataArray['date_time']=$request->date_time;
    }
    //dd($dataArray);

    if($request->id){


        $save=Leadstatus::where('id',$request->id)->update($dataArray);

        if ($save) {
            $json['message'] = "Lead Status Updated Successfully";
            $json['status'] = "success";
        } else {
                $json['message'] = "Lead Status Not Updated Successfully";
                $json['status'] = "failed";
        }

    }else{

        $save=Leadstatus::create($dataArray);


        if ($save) {
            $json['message'] = "Lead Status Created Successfully";
            $json['status'] = "success";
        } else {
                $json['message'] = "Lead Status Not Created Successfully";
                $json['status'] = "failed";
        }

    }

    return json_encode($json);


   }
    

}
