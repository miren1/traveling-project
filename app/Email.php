<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Email extends Model
{
    protected $table = 'emails';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name', 'number','user_id', 'campaign_id', 'from', 'to', 'body', 'status', 'is_delete'
    ];
}
