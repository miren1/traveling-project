<?php
namespace App\Helpers;

class OtpHelper
{
    public static function SendOtp($name, $otp, $to, $type)
    {
        // dd($name,$otp,$to,$type);
        try{
            //http Url to send sms.
            $url="https://www.smsidea.co.in/smsstatuswithid.aspx";
            $fields = array(
            'mobile' => env('SMS_UNAME'),
            'pass' => env('SMS_PASS'),
            'senderid' => env('SMS_SENDERID'),
            'to' => (int) $to,
            'msg' => urlencode('Dear '.$name.', '.$otp.' is a SECRET '.$type.' OTP with Jag-Joyu Travel Solutions. OTP is valid for 5 mins. Please do not share OTP. Regards, Team - Jag-Joyu'),
            'templateid' =>(int)  env('SMS_TMPLID'),
            );
            //url-ify the data for the POST
            $fields_string = 'mobile='.$fields['mobile'].'&pass='.$fields['pass'].'&senderid='.$fields['senderid'].'&to='.$fields['to'].'&msg='.$fields['msg'].'&templateid='.$fields['templateid'];
            // dd($fields);
            
            $ch = curl_init();
            
            curl_setopt($ch,CURLOPT_URL, $url);
            curl_setopt($ch,CURLOPT_POST, count($fields));
            curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
            //execute post
            $result = curl_exec($ch);
            
            return $result;
            // curl_close($ch);

        }
        catch(Exception $e){
            return $e->getMessage();
        }
    }
}