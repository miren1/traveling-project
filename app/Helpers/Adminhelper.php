<?php
namespace App\Helpers;

use App\Setting;
use Auth;
 class Adminhelper
 {
       public static function sendEmailTo($body, $subject, $send_to)
       {
        // dd($send_to);
              $setting = Setting::first();
              // dd($setting);
              // $key = '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';
              
              $email = new \SendGrid\Mail\Mail(); 
              $email->setFrom($setting->contact_mail);
              $email->setSubject($subject);
              $email->addTo($send_to);
              $email->addContent("text/html", $body);
              $sendgrid = new \SendGrid($setting->sendgrid_key);
              try {
              
              $response = $sendgrid->send($email);
              // dd($response);
              return array('status' => $response->statusCode());
              } catch (Exception $e) {

              return array('status' => 'error' , 'error' => $e->getMessage());
              }
       }
       
       public static function setting(){
          return  $setting = Setting::first();
       }

       public static function generateSignature($postData){
    
            $secretKey = "394104d2e5fddcc87778c4b995504d7cd0f9a076";
            
            $postData = array( 
              "appId" => $postData['appId'],
              "orderId" => $postData['orderId'], 
              "orderAmount" => $postData['orderAmount'], 
              "orderCurrency" => $postData['orderCurrency'], 
              "orderNote" => $postData['orderNote'], 
              "customerName" => $postData['customerName'], 
              "customerEmail" => $postData['customerEmail'],
              "customerPhone" => $postData['customerPhone'], 
              "returnUrl" => $postData['returnUrl'], 
              "notifyUrl" => $postData['notifyUrl'],
            );

             ksort($postData);
             $signatureData = "";
             foreach ($postData as $key => $value){
                  $signatureData .= $key.$value;
             }
             $signature = hash_hmac('sha256', $signatureData, $secretKey,true);

             $signature = base64_encode($signature);
             return $signature;
        }

        public static function mandate_verification($merchant_id, $txt_id, $consumer_id){
          
            $ldate = date('d-m-Y');

            $curl = curl_init();

            curl_setopt_array($curl, array(
              CURLOPT_URL => 'https://www.paynimo.com/api/paynimoV2.req',
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => '',
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 0,
              CURLOPT_FOLLOWLOCATION => true,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => 'POST',
              CURLOPT_POSTFIELDS =>'{
              "merchant": {
                "identifier": '.$merchant_id.'
              },
              "payment": {
                "instruction": {}
              },
              "transaction": {
                "deviceIdentifier": "S",
                "type": "002",
                "currency": "INR",
                "identifier": '.$txt_id.',
                "dateTime": '.$ldate.',
                "subType": "002",
                "requestType": "TSI"
              },
              "consumer": {
                "identifier": '.$consumer_id.'
              }
            }',
              CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
              ),
            ));
            
            $response = curl_exec($curl);

            curl_close($curl);
            return json_decode($response,1);
        }


        public static function transaction_schedule($merchant_id, $txt_id, $bank_txn_id, $txn_amt, $mandate_reg_no, $consumer_id){
          
            $ldate = date('dmY');

            $time = time();
            $id = hexdec( uniqid(13) );
            //dd($merchant_id);
           
            $curl = curl_init();

                curl_setopt_array($curl, array(
                  CURLOPT_URL => 'https://www.paynimo.com/api/paynimoV2.req',
                  CURLOPT_RETURNTRANSFER => true,
                  CURLOPT_ENCODING => '',
                  CURLOPT_MAXREDIRS => 10,
                  CURLOPT_TIMEOUT => 0,
                  CURLOPT_FOLLOWLOCATION => true,
                  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                  CURLOPT_CUSTOMREQUEST => 'POST',
                  CURLOPT_POSTFIELDS =>'{
                  "merchant": {
                    "identifier": '.$merchant_id.'
                  },
                  "payment": {
                    "instrument": {
                      "identifier": "test"
                    },
                    "instruction": {
                      "amount": '.$txn_amt.',
                      "endDateTime": "19082021",
                      "identifier": '.$mandate_reg_no.'
                    }
                  },
                  "transaction": {
                    "deviceIdentifier": "S",
                    "type": "002",
                    "currency": "INR",
                    "identifier": '.$txt_id.',
                    "subType": "003",
                    "requestType": "TSI"
                  }
                }',
                  CURLOPT_HTTPHEADER => array(
                    'Content-Type: application/json'
                  ),
                ));

                $response = curl_exec($curl);

                curl_close($curl);
            
                return json_decode($response,1);
        }

      /*  public static function transaction_verification($merchant_id, $identifier_txn_id){
          
            //$ldate = date('d-m-Y');

            $curl = curl_init();

                curl_setopt_array($curl, array(
                  CURLOPT_URL => 'https://www.paynimo.com/api/paynimoV2.req',
                  CURLOPT_RETURNTRANSFER => true,
                  CURLOPT_ENCODING => '',
                  CURLOPT_MAXREDIRS => 10,
                  CURLOPT_TIMEOUT => 0,
                  CURLOPT_FOLLOWLOCATION => true,
                  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                  CURLOPT_CUSTOMREQUEST => 'POST',
                  CURLOPT_POSTFIELDS =>'{
                      "merchant": {
                        "identifier": '.$merchant_id.'
                      },
                      "payment": {
                        "instruction": {
                        }
                      },
                      "transaction": {
                        "deviceIdentifier": "S",
                        "type": "002",
                        "currency": "INR",
                        "identifier": '.$identifier_txn_id.',
                        "dateTime": "24-07-2021",
                        "subType": "004",
                        "requestType": "TSI"
                      }
                    }',
                  CURLOPT_HTTPHEADER => array(
                    'Content-Type: application/json'
                  ),
                ));

                $response = curl_exec($curl);

                curl_close($curl);
            
                return json_decode($response,1);
        } */
 }



