<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket_sub_category extends Model
{
    
    protected $fillable = [
        'ticket_cat_id', 'name', 'dec', 'is_delete','employee','agent','member','user'
    ];
    public function ticket_categories()
    {
       return $this->hasOne('App\Ticket_category','id','ticket_cat_id');
    }
}
