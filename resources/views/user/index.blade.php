<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->


<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <!-- Font Awesome CSS -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css"
    integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
  <!-- Google Font CSS -->
  <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700&display=swap" rel="stylesheet">
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css"
    integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
  <!-- Custom CSS -->
  <link rel="stylesheet" href="css/style.css">
  <title>Jag Joyu</title>
<style>
*{
    margin: 0;
    padding: 0;
    box-sizing: border-box;
}
body{
    font-family: 'Poppins', sans-serif;
    color: #555a60;
}
a{
    text-decoration: none;
    color: #555a60 !important;
}
h1{
    font-size: 48px;
    font-weight: 600;
    color: #ff6e6e;
}
h2{
    font-size: 28px;
    font-weight: 600;
}
h3{
    margin: 0;
    font-size: 28px;
    font-weight: 600;
}
.custom-menu ul li{
    margin: 5px;
}
.login{
    background-color: #ff6e6e;
    border-radius: 5px;
    margin: 5px;
    padding: 0 5px;
}
.signup{
    background-color: #64cbff;
    border-radius: 5px;
    margin: 5px;
    padding: 0 5px;
}
.signup a, .login a{
    color: #fff !important;
}
.bg{
    
    background-repeat: no-repeat;
    background-size: cover;
    background-position: center;
    padding-top: 50px;;
}
.count div{
    padding: 20px;
}
.count{
    display: flex;
    justify-content: center;
    margin-top: 20px;
}
.form{
    margin-top: 20px;;
}
.form input{
    margin-top: 10px;;
    width: 60%;
    padding: 5px 15px;
    border-radius: 20px;
    border: none;
    background-color: #c3d7e2;
    outline: none;
}
.form button{
    width: 30%;
    border: none;
    padding: 5px 15px;
    border-radius: 20px;
    background-color: #00c2f4;
    color: #fff;
    outline: none;
    cursor: pointer;
}
</style>
</head>

<body>
  <!-- START MAIN CONTENT -->
  <!-- ------------------------------------------------------------------------------------------------------------->
  <!-- Header Section  -->
  <header id="header">
    <div class="container">
     
    </div>
  </header>

  <!-- countdown-area -->
  <section class="countdown-area">
    <div class="container bg">
      <div class="row">
        <div class="col-md-6">
          <div class="text text-center">
            <h1 class="">SOMETHING AWESOME IS IN THE WORK
 </h1>
            <section>
            We are working on a new and exciting product that we think you'll really like.
Enter your email address below to be the first to know when we are launching it.
          </section>
            <div class="form">
            <form method='post' action='./sendmailuser'>
            @csrf
              <h2>Subscribe and Get Notify!</h2>
              <input type="email" placeholder="Enter Email" required name='email'>
              <button type="submit" name='submit'>Notify Me</button>
            </form>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="image">
            <img src="./images/Logo.jpg" alt="">
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- ------------------------------------------------------------------------------------------------------------->
  <!-- END MAIN CONTENT -->
  <!-- JQuery JS -->
  <script src="http://code.jquery.com/jquery-3.3.1.min.js"
    integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
  <!-- Popper JS -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
    integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
    crossorigin="anonymous"></script>
  <!-- Bootstrap JS -->
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"
    integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T"
    crossorigin="anonymous"></script>

  <script>
    // Get the current year for the copyright
    $('#year').text(new Date().getFullYear());
  </script>
</body>

</html>