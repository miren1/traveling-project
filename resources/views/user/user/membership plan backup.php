<br><br><br>
<hr>
<div class="container-fluid bg-gradient p-5">
  <div class='row'>
  <ul class="nav nav-pills mb-3 " id="pills-tab" role="tablist" style='margin-left:10rem'>
  <li class="nav-item">
    <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true"><b>Customer</b></a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false"><b>Member</b></a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false"><b>Agent</b></a>
  </li>

  <li class="nav-item">
    <a class="nav-link" id="pills-emp-tab" data-toggle="pill" href="#pills-emp" role="tab" aria-controls="pills-emp" aria-selected="false"><b>Employee</b></a>
  </li>

</ul>
  </div>
  <div class="tab-content" id="pills-tabContent">

  <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">

 
      <div class="row m-auto text-center w-75">

        <?php foreach ($plans as $value) {
          # code...
          if($value->role == 'customer'){
        ?>
        <div class="col-4 princing-item">
          <div class="pricing-divider ">
              <!-- <h3 class="text-light">START-UP</h3> -->
            <h4 class="my-0 display-2 text-light font-weight-normal mb-3"> <?php echo  $value->years ?><span class="h5">years</span></h4>
            <!-- // For Lines  -->
             <svg class='pricing-divider-img' enable-background='new 0 0 300 100' height='100px' id='Layer_1' preserveAspectRatio='none' version='1.1' viewBox='0 0 300 100' width='300px' x='0px' xml:space='preserve' xmlns:xlink='http://www.w3.org/1999/xlink' xmlns='http://www.w3.org/2000/svg' y='0px'>
          <path class='deco-layer deco-layer--1' d='M30.913,43.944c0,0,42.911-34.464,87.51-14.191c77.31,35.14,113.304-1.952,146.638-4.729
	c48.654-4.056,69.94,16.218,69.94,16.218v54.396H30.913V43.944z' fill='#FFFFFF' opacity='0.6'></path>
          <path class='deco-layer deco-layer--2' d='M-35.667,44.628c0,0,42.91-34.463,87.51-14.191c77.31,35.141,113.304-1.952,146.639-4.729
	c48.653-4.055,69.939,16.218,69.939,16.218v54.396H-35.667V44.628z' fill='#FFFFFF' opacity='0.6'></path>
          <path class='deco-layer deco-layer--3' d='M43.415,98.342c0,0,48.283-68.927,109.133-68.927c65.886,0,97.983,67.914,97.983,67.914v3.716
	H42.401L43.415,98.342z' fill='#FFFFFF' opacity='0.7'></path>
          <path class='deco-layer deco-layer--4' d='M-34.667,62.998c0,0,56-45.667,120.316-27.839C167.484,57.842,197,41.332,232.286,30.428
	c53.07-16.399,104.047,36.903,104.047,36.903l1.333,36.667l-372-2.954L-34.667,62.998z' fill='#FFFFFF'></path>
        </svg>
          </div>
          <div class="card-body bg-white mt-0 shadow">
            <ul class="list-unstyled mb-5 position-relative">
              <li><b>Fixed Amount</b> <?php echo $value->fixed_amt?></li><hr>
              <li><b>Mebership Fees</b> <?php echo $value->mebership_fees?></li><hr>
              <li><b>Free Nights Room </b><?php echo $value->free_nights_room?></li><hr>
              <li><b>Free Nights In Any Type Of Hotel </b><?php echo $value->free_night_in_any_type_of_hotel?></li><hr>
              <li><b>Monthly EMI</b><?php echo $value->monthly_emi?></li><hr>
              <li><b>Quarterly EMI</b><?php echo $value->quarterly_emi?></li><hr>
              <li><b>Half Yearly EMI</b><?php echo $value->half_yearly_emi?></li><hr>
              <li><b>Anual EMI </b><?php echo $value->annual_emi?></li><hr>
              <li><b>Emi Dafulter Charges </b><?php echo $value->emi_dafulter_charges?></li><hr>
              <li><b>Flexbility To Move Free Night To Next Year </b><?php echo $value->flexbility_to_move_free_night_to_next_year?></li><hr>
              <li><b>Upgrade Plans </b><?php echo $value->easy_upgrade_of_membership_plans?></li><hr>
              <li><b>Referral </b><?php echo $value->referral?></li><hr>
            </ul>
            <button type="button" class="btn btn-lg btn-block  btn-custom ">Sign up for free</button>
          </div>
        </div>
        <?php } ?><?php }?>
       
        </div>
     </div>

     <div class="tab-pane fade show" id="pills-emp" role="tabpanel" aria-labelledby="pills-emp-tab">

 
<div class="row m-auto text-center w-75">

  <?php foreach ($plans as $value) {
    # code...
    if($value->role == 'employee'){
  ?>
  <div class="col-4 princing-item">
    <div class="pricing-divider ">
        <!-- <h3 class="text-light">START-UP</h3> -->
      <h4 class="my-0 display-2 text-light font-weight-normal mb-3"> <?php echo  $value->years ?><span class="h5">years</span></h4>
      <!-- // For Lines  -->
       <svg class='pricing-divider-img' enable-background='new 0 0 300 100' height='100px' id='Layer_1' preserveAspectRatio='none' version='1.1' viewBox='0 0 300 100' width='300px' x='0px' xml:space='preserve' xmlns:xlink='http://www.w3.org/1999/xlink' xmlns='http://www.w3.org/2000/svg' y='0px'>
    <path class='deco-layer deco-layer--1' d='M30.913,43.944c0,0,42.911-34.464,87.51-14.191c77.31,35.14,113.304-1.952,146.638-4.729
c48.654-4.056,69.94,16.218,69.94,16.218v54.396H30.913V43.944z' fill='#FFFFFF' opacity='0.6'></path>
    <path class='deco-layer deco-layer--2' d='M-35.667,44.628c0,0,42.91-34.463,87.51-14.191c77.31,35.141,113.304-1.952,146.639-4.729
c48.653-4.055,69.939,16.218,69.939,16.218v54.396H-35.667V44.628z' fill='#FFFFFF' opacity='0.6'></path>
    <path class='deco-layer deco-layer--3' d='M43.415,98.342c0,0,48.283-68.927,109.133-68.927c65.886,0,97.983,67.914,97.983,67.914v3.716
H42.401L43.415,98.342z' fill='#FFFFFF' opacity='0.7'></path>
    <path class='deco-layer deco-layer--4' d='M-34.667,62.998c0,0,56-45.667,120.316-27.839C167.484,57.842,197,41.332,232.286,30.428
c53.07-16.399,104.047,36.903,104.047,36.903l1.333,36.667l-372-2.954L-34.667,62.998z' fill='#FFFFFF'></path>
  </svg>
    </div>
    <div class="card-body bg-white mt-0 shadow">
      <ul class="list-unstyled mb-5 position-relative">
        <li><b>Fixed Amount</b> <?php echo $value->fixed_amt?></li><hr>
        <li><b>Mebership Fees</b> <?php echo $value->mebership_fees?></li><hr>
        <li><b>Free Nights Room </b><?php echo $value->free_nights_room?></li><hr>
        <li><b>Free Nights In Any Type Of Hotel </b><?php echo $value->free_night_in_any_type_of_hotel?></li><hr>
        <li><b>Monthly EMI</b><?php echo $value->monthly_emi?></li><hr>
        <li><b>Quarterly EMI</b><?php echo $value->quarterly_emi?></li><hr>
        <li><b>Half Yearly EMI</b><?php echo $value->half_yearly_emi?></li><hr>
        <li><b>Anual EMI </b><?php echo $value->annual_emi?></li><hr>
        <li><b>Emi Dafulter Charges </b><?php echo $value->emi_dafulter_charges?></li><hr>
        <li><b>Flexbility To Move Free Night To Next Year </b><?php echo $value->flexbility_to_move_free_night_to_next_year?></li><hr>
        <li><b>Upgrade Plans </b><?php echo $value->easy_upgrade_of_membership_plans?></li><hr>
        <li><b>Referral </b><?php echo $value->referral?></li><hr>
      </ul>
      <button type="button" class="btn btn-lg btn-block  btn-custom ">Sign up for free</button>
    </div>
  </div>
  <?php } ?><?php }?>
 
  </div>
</div>



        <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
        
        <div class="row m-auto text-center w-75">

<?php foreach ($plans as $value) {
  # code...
  if($value->role == 'member'){
?>
<div class="col-4 princing-item">
  <div class="pricing-divider ">
      <!-- <h3 class="text-light">START-UP</h3> -->
    <h4 class="my-0 display-2 text-light font-weight-normal mb-3"> <?php echo  $value->years ?><span class="h5">years</span></h4>
    <!-- // For Lines  -->
     <svg class='pricing-divider-img' enable-background='new 0 0 300 100' height='100px' id='Layer_1' preserveAspectRatio='none' version='1.1' viewBox='0 0 300 100' width='300px' x='0px' xml:space='preserve' xmlns:xlink='http://www.w3.org/1999/xlink' xmlns='http://www.w3.org/2000/svg' y='0px'>
  <path class='deco-layer deco-layer--1' d='M30.913,43.944c0,0,42.911-34.464,87.51-14.191c77.31,35.14,113.304-1.952,146.638-4.729
c48.654-4.056,69.94,16.218,69.94,16.218v54.396H30.913V43.944z' fill='#FFFFFF' opacity='0.6'></path>
  <path class='deco-layer deco-layer--2' d='M-35.667,44.628c0,0,42.91-34.463,87.51-14.191c77.31,35.141,113.304-1.952,146.639-4.729
c48.653-4.055,69.939,16.218,69.939,16.218v54.396H-35.667V44.628z' fill='#FFFFFF' opacity='0.6'></path>
  <path class='deco-layer deco-layer--3' d='M43.415,98.342c0,0,48.283-68.927,109.133-68.927c65.886,0,97.983,67.914,97.983,67.914v3.716
H42.401L43.415,98.342z' fill='#FFFFFF' opacity='0.7'></path>
  <path class='deco-layer deco-layer--4' d='M-34.667,62.998c0,0,56-45.667,120.316-27.839C167.484,57.842,197,41.332,232.286,30.428
c53.07-16.399,104.047,36.903,104.047,36.903l1.333,36.667l-372-2.954L-34.667,62.998z' fill='#FFFFFF'></path>
</svg>
  </div>
  <div class="card-body bg-white mt-0 shadow">
    <ul class="list-unstyled mb-5 position-relative">
      <li><b>Fixed Amount</b> <?php echo $value->fixed_amt?></li><hr>
      <li><b>Mebership Fees</b> <?php echo $value->mebership_fees?></li><hr>
      <li><b>Free Nights Room </b><?php echo $value->free_nights_room?></li><hr>
      <li><b>Free Nights In Any Type Of Hotel </b><?php echo $value->free_night_in_any_type_of_hotel?></li><hr>
      <li><b>Monthly EMI</b><?php echo $value->monthly_emi?></li><hr>
      <li><b>Quarterly EMI</b><?php echo $value->quarterly_emi?></li><hr>
      <li><b>Half Yearly EMI</b><?php echo $value->half_yearly_emi?></li><hr>
      <li><b>Anual EMI </b><?php echo $value->annual_emi?></li><hr>
      <li><b>Emi Dafulter Charges </b><?php echo $value->emi_dafulter_charges?></li><hr>
      <li><b>Flexbility To Move Free Night To Next Year </b><?php echo $value->flexbility_to_move_free_night_to_next_year?></li><hr>
      <li><b>Upgrade Plans </b><?php echo $value->easy_upgrade_of_membership_plans?></li><hr>
      <li><b>Referral </b><?php echo $value->referral?></li><hr>
    </ul>
    <button type="button" class="btn btn-lg btn-block  btn-custom ">Sign up for free</button>
  </div>
</div>
<?php } ?><?php }?>

</div>
</div>

<div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
<div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
        
<div class="row m-auto text-center w-75">

<?php foreach ($plans as $value) {
  # code...
  if($value->role == 'agent'){
?>
<div class="col-4 princing-item">
  <div class="pricing-divider ">
      <!-- <h3 class="text-light">START-UP</h3> -->
    <h4 class="my-0 display-2 text-light font-weight-normal mb-3"> <?php echo  $value->years ?><span class="h5">years</span></h4>
    <!-- // For Lines  -->
     <svg class='pricing-divider-img' enable-background='new 0 0 300 100' height='100px' id='Layer_1' preserveAspectRatio='none' version='1.1' viewBox='0 0 300 100' width='300px' x='0px' xml:space='preserve' xmlns:xlink='http://www.w3.org/1999/xlink' xmlns='http://www.w3.org/2000/svg' y='0px'>
  <path class='deco-layer deco-layer--1' d='M30.913,43.944c0,0,42.911-34.464,87.51-14.191c77.31,35.14,113.304-1.952,146.638-4.729
c48.654-4.056,69.94,16.218,69.94,16.218v54.396H30.913V43.944z' fill='#FFFFFF' opacity='0.6'></path>
  <path class='deco-layer deco-layer--2' d='M-35.667,44.628c0,0,42.91-34.463,87.51-14.191c77.31,35.141,113.304-1.952,146.639-4.729
c48.653-4.055,69.939,16.218,69.939,16.218v54.396H-35.667V44.628z' fill='#FFFFFF' opacity='0.6'></path>
  <path class='deco-layer deco-layer--3' d='M43.415,98.342c0,0,48.283-68.927,109.133-68.927c65.886,0,97.983,67.914,97.983,67.914v3.716
H42.401L43.415,98.342z' fill='#FFFFFF' opacity='0.7'></path>
  <path class='deco-layer deco-layer--4' d='M-34.667,62.998c0,0,56-45.667,120.316-27.839C167.484,57.842,197,41.332,232.286,30.428
c53.07-16.399,104.047,36.903,104.047,36.903l1.333,36.667l-372-2.954L-34.667,62.998z' fill='#FFFFFF'></path>
</svg>
  </div>
  <div class="card-body bg-white mt-0 shadow">
    <ul class="list-unstyled mb-5 position-relative">
      <li><b>Fixed Amount</b> <?php echo $value->fixed_amt?></li><hr>
      <li><b>Mebership Fees</b> <?php echo $value->mebership_fees?></li><hr>
      <li><b>Free Nights Room </b><?php echo $value->free_nights_room?></li><hr>
      <li><b>Free Nights In Any Type Of Hotel </b><?php echo $value->free_night_in_any_type_of_hotel?></li><hr>
      <li><b>Monthly EMI</b><?php echo $value->monthly_emi?></li><hr>
      <li><b>Quarterly EMI</b><?php echo $value->quarterly_emi?></li><hr>
      <li><b>Half Yearly EMI</b><?php echo $value->half_yearly_emi?></li><hr>
      <li><b>Anual EMI </b><?php echo $value->annual_emi?></li><hr>
      <li><b>Emi Dafulter Charges </b><?php echo $value->emi_dafulter_charges?></li><hr>
      <li><b>Flexbility To Move Free Night To Next Year </b><?php echo $value->flexbility_to_move_free_night_to_next_year?></li><hr>
      <li><b>Upgrade Plans </b><?php echo $value->easy_upgrade_of_membership_plans?></li><hr>
      <li><b>Referral </b><?php echo $value->referral?></li><hr>
    </ul>
    <button type="button" class="btn btn-lg btn-block  btn-custom ">Sign up for free</button>
  </div>
</div>
<?php } ?><?php }?>

</div>
</div>


</div>

</div>
</div>
         
         
        
        
        
        
      </div>
    </div>