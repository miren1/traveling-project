@extends('user.master')
@section('bodyData')
<div class="breadcrumb-area style-two jarallax" style="background-image:url( {{ asset('uploads/plans_images/1604688713Jellyfish.jpg')}});">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb-inner">
                        <h1 class="page-title"><?php echo 'Inquiry' ?></h1>
                      
                    </div>
                </div>
            </div>
        </div>
        <div class='row'>
        
        </div>
    </div>
    <div class='container'>
    <div class='row'>
    <div class='col-sm-3'>
    </div>
    
    <form method='post' action='inquiryinsert'>
    @csrf
    <br><br> 
  <div class="form-row">
    <div class="form-group col-md-6">
      <label for="inputEmail4">First name</label>
      <input type="text" class="form-control" id="inputEmail4" placeholder="Enter First Name" name='first_name' pattern='[A-Za-z]{3,30}' required>
    </div>
    <div class="form-group col-md-6">
      <label for="inputPassword4">Last Name</label>
      <input type="text" class="form-control" id="inputPassword4" placeholder="Enter Last Name" name='last_name' required>
    </div>
  </div>
  <div class="form-group">
    <label for="inputAddress">Mobile Number</label>
    <input type="text" class="form-control" id="inputAddress" placeholder="Enter Mobile Number" required>
  </div>
  <div class="form-group">
    <label for="inputAddress2">What's App Number</label>
    <input type="text" class="form-control" id="inputAddress2" placeholder="Enter What's Up Number" name='whatsup_no' required>
  </div>
  <div class="form-row">
    <div class="form-group col-md-6">
      <label for="inputCity">Email</label>
      <input type="text" class="form-control" id="inputCity" name='email' required>
    </div>
    <div class="form-group col-md-4">
      <label for="inputState">Appointment Date</label>
      <input type="datetime-local" class="form-control" id="inputCity" name='appointment_date' required>
    </div>
   
  </div>
 
  <button type="submit" class="btn btn-primary">Send Inquiry</button>
</form>
    </div>
    <div class='col-sm-3'>
    </div>
    </div>
    @endsection
