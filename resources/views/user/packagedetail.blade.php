<!-- preloader area start -->
@extends('user.master')
@section('bodyData')
<style>
  .modal-dialog {
       max-width: 90% !important;
      width: 90% !important;
 }
 
</style>

<?php 
?>

    <!-- <div class="body-overlay" id="body-overlay"></div> -->
    
   <div class="breadcrumb-area style-two jarallax" style="background-image:url( {{ asset('uploads/plans_images/'.$tourpackage[0]->photo)}});">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb-inner">
                        <h1 class="page-title"><?php echo $tourpackage[0]->name ?></h1>
                       </div>
                </div>
            </div>
        </div>
    </div>
<br>   
    <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

    </div>
    </div>
  </div>    
</div>

<div class="destinations-details-page">
        <div class="container">
           
           <h4 class="single-page-small-title">Tour Packages</h4>
            
            <div class="destinations-client-review-slider tp-common-slider-style">
            <?php foreach($tourPlanDetails as $value){?>
                <div class="d-client-review-slider-item">
                    <div class="single-destination-grid text-center">
                        <div class="thumb">
                            <img src="<?php echo asset('media/').'/'.$value->banner_img?>">
                        </div>
                        <div class="details">
                            
                            <h4 class="title"><?php  echo $value->name?></h4>
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bd-example-modal-lg" data-whatever='<?php echo $value->id ?>' onclick="showDtails({{$value->id }})">View Details</button>
                            <!-- <button type="button"  data-toggle="modal" data-target="#myModal" >Get more weapon detials</button> -->


                        </div>
                    </div>
                </div>
            <?php } ?>
          </div>
          <script>
          function showDtails(postid){
            $.ajax({
                url : '{{ route("gettourspecificdata") }}',
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
                data:{postid:postid},
                success:function(data){
                //   //  alert("sucess");
                console.log(data);
                   $('.modal-content').html(data)

                },

            });
}
          </script>
           
            <div class="location-review-area">
                <div class="row">
                    <div class="col-lg-12">
                        <form class="tp-form-wrap bg-gray tp-form-wrap-one">
                            <div class="row">
                                <div class="col-md-6"><h4 class="single-page-small-title">Write A Review</h4></div>
                                <div class="col-md-6">
                                   
                                </div>
                                <div class="col-md-6">
                                    <label class="single-input-wrap">
                                        <span class="single-input-title">Name</span>
                                        <input type="text">
                                    </label>
                                </div>
                                <div class="col-md-6">
                                    <label class="single-input-wrap">
                                        <span class="single-input-title">Email</span>
                                        <input type="text">
                                    </label>
                                </div>
                                <div class="col-lg-12">
                                    <label class="single-input-wrap">
                                        <span class="single-input-title">comments</span>
                                        <textarea></textarea>
                                    </label>
                                </div>
                                <div class="col-12">
                                    <a class="btn btn-blue" href="#">+ Add Photo</a>
                                    <a class="btn btn-yellow float-right" href="#">Send</a>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-xl-3 col-lg-4 offset-xl-1 mt-5 mt-lg-0 hidden-md">
                       
                    </div>
                </div>
            </div>
            
        </div>
    </div>
   
    @endsection

   