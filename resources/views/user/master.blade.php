<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Jag Joyu - Travel, Tour Booking </title>
    <link rel=icon href="assets/img/favicon.png" sizes="20x20" type="image/png">
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/css/animate.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/css/magnific-popup.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/css/slick.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/css/swiper.min.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/css/nice-select.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/css/jquery-ui.min.css')}}">
    <!-- icons -->
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <link rel="stylesheet" href="{{ asset('assets/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/css/themify-icons.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/css/line-awesome.min.css')}}">
    <!-- main css -->
    <link rel="stylesheet" href="{{ asset('assets/css/style.css')}}">
    <!-- responsive css -->
    <link rel="stylesheet" href="{{ asset('assets/css/responsive.css')}}">
<style>
.loadicon
{
color:white;
}
    @media (min-width: 992px){
.navbar-area-fixed .nav-container .navbar-collapse .navbar-nav li a
  {
      color:#000;
  }
}
</style>
    <style>
   section {
    padding-top: 100px;
    padding-bottom: 100px;
}

.quote {
    color: rgba(0,0,0,.1);
    text-align: center;
    margin-bottom: 30px;
}

/*-------------------------------*/
/*    Carousel Fade Transition   */
/*-------------------------------*/

#fade-quote-carousel.carousel {
  padding-bottom: 60px;
}
#fade-quote-carousel.carousel .carousel-inner .item {
  opacity: 0;
  -webkit-transition-property: opacity;
      -ms-transition-property: opacity;
          transition-property: opacity;
}
#fade-quote-carousel.carousel .carousel-inner .active {
  opacity: 1;
  -webkit-transition-property: opacity;
      -ms-transition-property: opacity;
          transition-property: opacity;
}
#fade-quote-carousel.carousel .carousel-indicators {
  bottom: 10px;
}
#fade-quote-carousel.carousel .carousel-indicators > li {
  background-color: #e84a64;
  border: none;
}
#fade-quote-carousel blockquote {
    text-align: center;
    border: none;
}
#fade-quote-carousel .profile-circle {
    width: 100px;
    height: 100px;
    margin: 0 auto;
    border-radius: 100px;
}
    </style>

        <style>
        body {font-family: Arial, Helvetica, sans-serif;}
        * {box-sizing: border-box;}
        
        
        /* Button used to open the chat form - fixed at the bottom of the page */
        /* .icon1
        {

            color:#fff;
        } */
        .open-button {
          background-color:#f3941e;
          color: white;
          padding: 16px 20px;
          border: none;
          cursor: pointer;
          opacity: 0.8;
          position: fixed;
          bottom: 23px;
          right: 70px;
          width: 200px;
        }
        
        /* The popup chat - hidden by default */
        .chat-popup {
          display: none;
          position: fixed;
          bottom: 0;
          right: 15px;
          border: 3px solid #f1f1f1;
          z-index: 9;
        }
        
        /* Add styles to the form container */
        .form-container {
          max-width: 400px;
          padding: 10px;
          background-color: white;
          width:400px
        }
        
        /* Full-width textarea */
        .form-container textarea {
          width: 100%;
          padding: 15px;
          margin: 5px 0 22px 0;
          border: none;
          background: #f1f1f1;
          resize: none;
          min-height: 100px;
        }
        
        /* When the textarea gets focus, do something */
        .form-container textarea:focus {
          background-color: #ddd;
          outline: none;
        }
        
        /* Set a style for the submit/send button */
        .form-container .btn {
          background-color: #f3941e;
          color: white;
         /* // padding: 16px 20px; */
          border: none;
          cursor: pointer;
          width: 100%;
          margin-bottom:10px;
          opacity: 0.8;
        }
        
        /* Add a red background color to the cancel button */
        .form-container .cancel {
          background-color: #000;
        }
      
        
        /* Add some hover effects to buttons */
        .form-container .btn:hover, .open-button:hover {
          opacity: 1;
        }
        
        @media (min-width: 992px){
.icon1{
    line-height: 80px;
    color: #fff;

}


.nav-style-02 .dropdown-menu-btn .line {
    height: 2px;
    width: 21px;
    background: #ffffff;
    margin-bottom: 4px;
    display: block;
    -webkit-transition: all 0.3s ease;
    -moz-transition: all 0.3s ease;
    -o-transition: all 0.3s ease;
    transition: all 0.3s ease;
}
        }
        </style>
@notifyCss
</head>
<body>

    <!-- preloader area start -->
    <div class="preloader" id="preloader">
        <div class="preloader-inner">
            <div class="spinner">
                <div class="dot1"></div>
                <div class="dot2"></div>
            </div>
        </div>
    </div>

    <div class="body-overlay" id="body-overlay"></div>
    <div class="search-popup" id="search-popup">
        <form action="index.html" class="search-form">
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Search.....">
            </div>
            <button type="submit" class="submit-btn"><i class="fa fa-search"></i></button>
        </form>
    </div>

    <div class="signUp-popup login-register-popup" id="signUp-popup">
        <div class="login-register-popup-wrap">
            <div class="row no-gutters">
                <div class="col-lg-6">
                    <div class="thumb">
                        <img src="{{asset('assets/img/others/signup.png')}}" alt="img">
                    </div>
                </div>
                <div class="col-lg-6 align-self-center">
                    <div class="shape-thumb">
                        <img src="{{asset('assets/img/others/signup-shape.png')}}" alt="img">
                    </div>
                    <form class="login-form-wrap" method='post' action='/customerRegistration'>
                    @csrf
                        <h4 class="text-center">Sign Up</h4>
                        <div class="single-input-wrap style-two">
                            <input type="text" placeholder="Enter Name" pattern='[A-Za-z]{3,30}' name='first_name'>
                            <span class="single-input-title"><i class="fa fa-user"></i></span>
                        </div>
                        <div class="single-input-wrap style-two">
                            <input type="text" placeholder="Last name">
                            <span class="single-input-title"><i class="fa fa-user"></i></span>
                        </div>
                        <div class="single-input-wrap style-two">
                            <input type="text" placeholder="Mobile Number" pattern='[789][0-9]{9}' name='mobile_number'>
                            <span class="single-input-title"><i class="fa fa-phone"></i></span>
                        </div>
                        <div class="single-input-wrap style-two">
                            <input type="text" placeholder="Enter Whats App Number" name='whats_up' pattern='[789][0-9]{9}'>
                            <span class="single-input-title"><i class="fa fa-whatsapp "></i></span>
                        </div>
                        <div class="single-input-wrap style-two">
                            <input type="email" placeholder="Enter Email" pattern='[789][0-9]{9}' name='email'>
                            <span class="single-input-title"><i class="fa fa-envelope"></i></span>
                        </div>

                        <!-- <label class="checkbox">
                            <input type="checkbox">
                            <span>Remember me</span>
                        </label> -->
                        <div class="single-input-wrap style-two">
                            <button class="btn btn-yellow w-100">Sign Up</button>
                        </div>
                        <div class="sign-in-btn">I already have an account. <a href="#">Sign In</a></div> 
                        <div class="social-wrap">
                            <p>Or Continue With</p>
                            <ul class="social-icon">
                                <li>
                                    <a class="facebook" href="#" target="_blank"><i class="fa fa-facebook  "></i></a>
                                </li>
                                <li>
                                    <a class="twitter" href="#" target="_blank"><i class="fa fa-twitter  "></i></a>
                                </li>
                                <li>
                                    <a class="pinterest" href="#" target="_blank"><i class="fa fa-pinterest"></i></a>
                                </li>
                            </ul>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
   
    <nav class="navbar navbar-area navbar-expand-lg nav-style-01">
        <div class="container nav-container">
            <div class="responsive-mobile-menu">
                <div class="mobile-logo">
                    <a href="index.html">
                        <img src="{{asset('assets/img/sticky-Logo.jpg')}}" alt="logo">
                    </a>
                </div>
                <button class="navbar-toggler float-right" type="button" data-toggle="collapse" data-target="#tp_main_menu" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggle-icon">
                        <a href="javascript:void(0);" class="icon icon2" class="icon">&#9776;</a>
                    </span>
                </button>
                <div class="nav-right-content">
                    <ul class="pl-0">
                        <li class="top-bar-btn-booking">
                            <a class="btn btn-yellow" href="tour-details.html">Book Now <i class="fa fa-paper-plane"></i></a>
                        </li>
                     
                        <li class="notification">
                            <a class="signUp-btn" href="#">
                                <i class="fa fa-user-o"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="collapse navbar-collapse" id="tp_main_menu">
                <div class="logo-wrapper desktop-logo">
                    <a href="index.html" class="main-logo">
                        <img src="{{ asset('assets/img/Logo.jpg')}}" alt='logo'>
                    </a>
                    <a href="index.html" class="sticky-logo">
                        <img src="{{ asset('assets/img/sticky-Logo.jpg')}}" alt='logo'>
                    </a>
                </div>
                <ul class="dropdown-menu-btn">
                    <a href="javascript:void(0);" class="icon loadicon" style='font-size:24px'>&#9776;</a>
                </ul>
                <ul class="navbar-nav" style="display: none;">
                    <li class="menu-item-has-children">
                        <a href="/DEMO">Home</a>
                        <ul class="sub-menu">
                            
                        </ul>
                    </li>
                    <li>
                        <a href="/">About Us</a>
                    </li>
                
                    <li>
                        <a href="#">Contact</a>
                    </li>
                    <li>
                        <a href="/memberplans">Membership Plans</a>
                    </li>
                </ul>
            </div>
            <div class="nav-right-content">
                <ul>
                    <!-- <a class="btn btn-yellow" href="tour-details.html" tabindex="0">Become Member
                    </a> -->
                    <a class="btn btn-yellow" href="/inquiry" tabindex="0">Ask For Call
                    </a>
                    <script>
                    function demo()
                    {
                        alert("Hello working..")
                       $('#signUp-popup').show();
                    }

                    </script>
                    <li class="notification">
                        <a class="signUp-btn" role='button' onclick='demo()'>
                            <i class="fa fa-user-o"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    @yield('bodyData')
    <x:notify-messages />
    <br><br>
    <footer class="footer-area" style="background-image: url(asset('img/bg/2.png');">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <div class="footer-widget widget">
                        <div class="about_us_widget">
                            <a href="index.html" class="footer-logo"> 
                                <img src="assets/img/Logo.jpg" alt="footer logo">
                            </a>
                            <p>We believe brand interaction is key in commu- nication. Real innovations and a positive customer experience are the heart of successful communication.</p>
                            <ul class="social-icon">
                                <li>
                                    <a class="facebook" href="#" target="_blank"><i class="fa fa-facebook  "></i></a>
                                </li>
                                <li>
                                    <a class="twitter" href="#" target="_blank"><i class="fa fa-twitter  "></i></a>
                                </li>
                                <li>
                                    <a class="pinterest" href="#" target="_blank"><i class="fa fa-instagram"></i></a>
                                </li>
                            </ul>
                       </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="footer-widget widget ">
                        <div class="widget-contact">
                            <h4 class="widget-title">Contact us</h4>
                            <p>
                                <i class="fa fa-map-marker"></i> 
                                <span>Manama Tower, Badda Link Road, Badda Dhaka, Bangladesh</span>
                            </p>
                            <p class="location"> 
                                <i class="fa fa-envelope-o"></i>
                                <span>travelpoint@gmail.com</span>
                            </p>
                            <p class="telephone">
                                <i class="fa fa-phone base-color"></i> 
                                <span>
                                    +088 012121240
                                </span>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-6">
                    <div class="footer-widget widget">
                        <h4 class="widget-title">Quick Link</h4>
                        <ul class="widget_nav_menu">
                            <!-- <a href="#" data-target="#sidebar" data-toggle="collapse" aria-expanded="false" class="collapsed"><i class="text-dark fa fa-navicon fa-lg py-2 p-1"></i>
                            </a> -->

                            <!-- <li><a href="#">Abou Us</a></li> -->
                            <li><a href="#">About Us</a></li>
                            <li><a href="#">Blogs</a></li>
                            <li><a href="#">Career</a></li>
                            <li><a href="#">Events</a></li>
                            <li><a href="#">Gallery</a></li>
                            <li><a href="#">FAQ</a></li>
                            <li><a href="#">T & C</a></li>

                        </ul>
                    </div>
                </div>
               
            </div>
        </div>
        <div class="copyright-inner">
            <div class="copyright-text">
                &copy; Jag Joyu 2020 All rights reserved @Jag Joyu
            </div>
        </div>
    </footer>
    <!-- footer area end -->

    <!-- back to top area start -->
    <div class="back-to-top">
        <span class="back-top"><i class="fa fa-angle-up"></i></span>
    </div>
    <!-- back to top area end -->
    <script>
        function openForm() {
          document.getElementById("myForm").style.display = "block";
        }
        
        function closeForm() {
          document.getElementById("myForm").style.display = "none";
        }
        </script>

@notifyJs
    <!-- Additional plugin js -->
    <script src="{{ asset('assets/js/jquery-2.2.4.min.js') }} "></script>
    <script src=" {{ asset('assets/js/popper.min.js')}}"></script>
    <script src=" {{asset('assets/js/bootstrap.min.js')}}"></script>
    <script src=" {{asset('assets/js/jquery.magnific-popup.js')}}"></script>
    <script src=" {{asset('assets/js/owl.carousel.min.js')}}"></script>
    <script src=" {{asset('assets/js/wow.min.js')}}"></script>
    <script src="{{ asset('assets/js/slick.js')}}"></script>
    <script src=" {{asset('assets/js/waypoints.min.js')}}"></script>
    <script src="{{ asset('assets/js/jquery.counterup.min.js')}}"></script>
    <script src="{{ asset('assets/js/imagesloaded.pkgd.min.js')}}"></script>
    <script src="{{ asset('assets/js/isotope.pkgd.min.js')}}"></script>
    <script src="{{ asset('assets/js/swiper.min.js')}}"></script>
    <script src="{{ asset('assets/js/jquery.nice-select.min.js')}}"></script>
    <script src="{{ asset('assets/js/jquery-ui.min.js')}}"></script>

    <!-- main js -->
    <script src="{{asset('assets/js/main.js')}}"></script>
    <!-- codingeek -->
 <script src="{{ asset('assets/js/codingeek-link.js')}}"></script>
 <script src="{{asset('assets/js/codingeek.js')}}"></script>

 </html>