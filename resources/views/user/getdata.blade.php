      <div class="modal-header">
        
       
        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
  <li class="nav-item">
    <a class="nav-link active" id="pills-details-tab" data-toggle="pill" href="#pills-details" role="tab" aria-controls="pills-details" aria-selected="true">Details</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="pills-Itenary-tab" data-toggle="pill" href="#pills-Itenary" role="tab" aria-controls="pills-Itenary" aria-selected="false">Itenary</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="pills-photo-tab" data-toggle="pill" href="#pills-photo" role="tab" aria-controls="pills-photo" aria-selected="false">Photos</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="pills-video-tab" data-toggle="pill" href="#pills-video" role="tab" aria-controls="pills-video" aria-selected="false">Videos</a>
  </li>
</ul>
        
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        
      </div>
      <div class="modal-body">
      <div class="tab-content" id="pills-tabContent">
  <div class="tab-pane fade show active" id="pills-details" role="tabpanel" aria-labelledby="pills-details-tab">
  <!-- // Details Tags -->
  <div class="container">
		<div class="">
			<div class="container-fliud">
				<div class="wrapper row">
					<div class="preview col-md-3">
						
						<div class="preview-pic tab-content">
						  <div class="tab-pane active" id="pic-1"><img src="<?php echo asset('media/').'/'.$details[0]->banner_img?>" />
                         </div>
						
						</div>
						
						
					</div>
                    <div class="details col-md-9">
						<h3 class="product-title"><?php echo $details[0]->name . " ".$details[0]->price?></h3>
                        
						<div class="rating">
                            Days: <?php  echo $details[0]->days ;?>&nbsp;
                        <?php $small_detail_obj = json_decode($details[0]->small_info); 
                            $detail1 =  json_decode($details[0]->detail1); 
                            $detail2 =  json_decode($details[0]->detail2); 
                            $detail3 =  json_decode($details[0]->detail3); 
                            $detail4 =  json_decode($details[0]->detail4); 
                           
                            ?></div>
<table class="table table-bordered"><tbody>
  <?php  foreach ($small_detail_obj as $key => $value) {?>
<tr><td><?php echo $key ?></td>
      <td><?php echo $value ?></td></tr>
  <?php } ?>
    </tbody>
</table>
</div>
<div class='row'>
<div class='col-sm-12'>
<?php if($detail1) {?>
                        <?php foreach ($detail1 as $key1 => $value1) { ?>
                        <h6 class="price"><?php echo $key1 ?> </h6> 
                        <p class="product-description" style='text-align:justify'>
                        <?php 
                        print_r($value1);
                        ?>
                        </p><?php } }?>
                        <hr>
                        <?php foreach ($detail2 as $key1 => $value1) { ?>
                        <h6 class="price"><?php echo $key1 ?> </h6> 
                        <p class="product-description" style='text-align:justify'>
                        <?php 
                        print_r($value1);
                        ?>
                        </p><?php } ?>
                        <hr>
                        <?php foreach ($detail3 as $key1 => $value1) { ?>
                        <h6 class="price"><?php echo $key1 ?> </h6> 
                        <p class="product-description" style='text-align:justify'>
                        <?php 
                        print_r($value1);
                        ?>
                        </p><?php } ?>
                        <hr>
                        <?php foreach ($detail4 as $key1 => $value1) { ?>
                        <h6 class="price"><?php echo $key1 ?> </h6> 
                        <p class="product-description" style='text-align:justify'>
                        <?php 
                        print_r($value1);
                        ?>
                        </p><?php } ?>
                        <hr>
                        <?php if($details[0]->inclusion)
            { ?>
            <h6 class='price'>Inclusion:</h6>
            <?php
              print_r($details[0]->inclusion);
              ?><hr><?php
            } ?>

<?php if($details[0]->optional_charges)
            { ?>
            <h6 class='price'>Optional Charges:</h6>
            <?php
            print_r($details[0]->optional_charges);
              ?><hr><?php
            } ?>

        <?php if($details[0]->exclusion)
            { ?>
            <h6 class='price'>Exclusion:</h6>
            <?php
            print_r($details[0]->exclusion);
              ?><hr><?php
            } ?>

            
        <?php if($details[0]->essentials)
            { ?>
            <h6 class='price'>Basic Essentials:</h6>
            <?php
            print_r($details[0]->essentials);
            ?><hr><?php
            } ?>
            
            <?php if($details[0]->clothing_essential)
            { ?>
            <h6 class='price'>Clothing Essentials:</h6>
            <?php
            print_r($details[0]->clothing_essential);
            ?><hr><?php
            } ?>

</div>
</div>

                    </div></div></div></div>
  
  
  </div>
  <div class="tab-pane fade" id="pills-Itenary" role="tabpanel" aria-labelledby="pills-Itenary-tab">
  <?php 

foreach ($itinerary as $value) {
  ?>
  <p><b><?php echo "Day".$value->day_number;echo ": ".$value->caption  ?></b></p>
  <p><?php  print_r($value->details) ?>
  </p>
  <img src="<?php echo asset('itenary')."/".$value->images ?>" style='height:150px;width:70%'>
  <hr>
  <?php
}?>
  
  </div>
  <div class="tab-pane fade" id="pills-photo" role="tabpanel" aria-labelledby="pills-photo-tab">
  
 <div class='row'>
  <?php  foreach ($photos as $value) {
      # code...
  ?> 
  <div class='col-sm-3'>

<img src="<?php echo asset('media').'/'.$value->imagepath?>" alt="">
    <div class="desc">
    <?php echo  $value->caption?></div>
        </div> 
  <?php }?>
  </div>
 
  
  
  </div>
  <div class="tab-pane fade" id="pills-video" role="tabpanel" aria-labelledby="pills-video-tab">
  <div class='row'>
  <?php  foreach ($videos as $value) {
?>
  <div class='col-sm-4'>
	<iframe width="420" height="345" src="<?php echo $value->link?>">
    </iframe>
    <P> {{$value->caption}} 
    </P>
  </div>
  <?php } ?>
  </div>
  
  </div>

</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <!-- <button type="button" class="btn btn-primary">Send message</button> -->
      </div>
    </div>
 </div>
    </div>

    
