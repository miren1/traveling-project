<?php
$pendingleavs = DB::table('leaves')
->join('users', 'users.id', '=', 'leaves.employee_fk_id')
->select('*', 'users.id as user_id', 'leaves.id as leaveid')
->where('approve_status', 'pending')
->orderBy('leaves.id', 'desc')
->get();
$empcountDetail = DB::table('users')
->where('send_employment', 1)
->get();
$id = Session::get('aid');
$count = count($pendingleavs);
$empcounta = count($empcountDetail);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>Jag Joyu</title>
    <!-- Meta Tags -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="utf-8">
    <meta name="keywords" content="Jag Joyu" />
    <link rel="icon" type="image/png"
        href="<?php echo asset('images') . '/' . 'favicon.png'; ?>" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script>
        addEventListener("load", function() {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }

    </script>
    <script src="https://js.pusher.com/4.1/pusher.min.js"></script>


    <link href="{{ asset('admin/css/bootstrap.css') }} " rel="stylesheet" type="text/css" media="all" />
    <!-- Bootstrap Css -->
    <!-- Common Css -->

    <link href="{{ asset('admin/css/style.css') }}" rel="stylesheet" type="text/css" media="all" />
    <!--// Common Css -->
    <!-- Nav Css -->
    <link rel="stylesheet" href="{{ asset('admin/css/style4.css') }}">
    <!--// Nav Css -->
    <!-- Fontawesome Css -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <link href="{{ asset('admin/css/fontawesome-all.css') }}" rel="stylesheet" type='text/css'>
    <!--// Fontawesome Css -->
    <!--// Style-sheets -->


    <link href="https://use.fontawesome.com/releases/v5.0.4/css/all.css" rel="stylesheet">

    <!--web-fonts-->
    <link href="//fonts.googleapis.com/css?family=Poiret+One" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="http://fullcalendar.io/js/fullcalendar-2.1.1/lib/jquery-ui.custom.min.js"></script>

    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.js"></script>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.css" />
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" />
    <style>

        #sidebar {

            background: #083672;

        }

        #sidebar ul li.active>a,
        #sidebar a[aria-expanded="true"] {
            color: #fff;
            background: #083672;
        }

        #sidebar .sidebar-header {
            padding: 20px;
            background: #f3f3f3;
        }

        ul ul a {
            font-size: 0.9em !important;
            padding-left: 30px !important;
            background: #083672;
        }

        #sidebar ul li a:hover {
            color: #083672;
            background: #fff;
        }

        .btn-primary,
        .btn-primary:hover,
        .btn-primary:active,
        .btn-primary:visited {
            background-color: #083672 !important;
            border-color: #083672 !important;
        }

        .btn-success,
        .btn-success:hover,
        .btn-success:active,
        .btn-success:visited {
            background-color: #083672 !important;
            border-color: #083672 !important;
        }

        .btn-info,
        .btn-info:hover,
        .btn-info:active,
        .btn-info:visited {
            background-color: #f3941e !important;
            border-color: #f3941e !important;
        }

        .fa-info-circle,
        .fa-edit {
            color: #083672;
        }

        ul.top-icons-agileits-w3layouts a.nav-link i {
            display: inline-block;
            font-size: 17px;
            color: #083672;
        }
        small.help-block {
        color: red;
        }
        /* Chrome, Safari, Edge, Opera */
        input::-webkit-outer-spin-button,
        input::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
        }

        /* Firefox */
        input[type=number] {
        -moz-appearance: textfield;
        }

        .outer-w3-agile{
            width: 98%;
            margin-left: 1%;
        }
    </style>
    <style type="text/css">
        .b-close {
            color: #fff;
            background: #ff0000;
            padding: 5px 10px;
            position: absolute;
            right: 0;
            top: 0;
            cursor: pointer;
        }
        #toolbar{
            display: none;
        }
    </style>
    @stack('css')
   
    <link rel="stylesheet" href="{{url('vendor/mckenziearts/laravel-notify/css/notify.css')}}" />
    <!--//web-fonts-->
</head>

<body>
    <div class="wrapper">
        <!-- Sidebar Holder -->
        <nav id="sidebar">
            <div class="sidebar-header">
                <h1>
                   <!--  <a href="https://jagjoyu.xsquarestaging.in/admindashboard" style='color:white'>Jag Joyu</a> -->
                    <a href="{{ url()->current()}}"><img src="{{ url('/admin/images/JJ-dashboard.png')}}" style="margin-left: 20px;"></a>
                </h1>
                <span>J J</span>
            </div>
           
            <ul class="list-unstyled components">
                <li>
                    <a href="{{ url('admindashboard') }}">
                        <i class="fas fa-th-large"></i>
                        Dashboard
                    </a>
                </li>

                <li>
                    <a href="#reports" data-toggle="collapse" aria-expanded="false">
                        <i class="fas fa-table"></i>
                        Reports

                        <i class="fas fa-angle-down fa-pull-right"></i>
                    </a>
                    <ul class="collapse list-unstyled" id="reports">

                        <li>
                            <a href="cards.html">User Report</a>
                        </li>

                    </ul>
                </li>

                <li>
                    <a href="{{ url('groups') }}">
                        <i class="fas fa-th-large"></i>
                        Groups
                    </a>
                </li>

                <li>
                    <a href="{{ url('call-status') }}">
                        <i class="fas fa-th-large"></i>
                        Call Status
                    </a>
                </li>


                <li>
                    <a href="{{ url('/all-tickets') }}">
                        <i class="fas fa-clone"></i>
                        Inquiries
                    </a>
                </li>

                <li>
                    <a href="{{ url('/admin-customer') }}">
                         <i class="fa fa-users"></i>
                        Customers
                    </a>
                </li>

                
                {{--
                <li class="{{ request()->segment(1) == 'admin-customer' ? 'active' : '' }}">
                    <a href="#Customers" data-toggle="collapse" aria-expanded="{{ request()->segment(1) == 'admin-customer' ? 'true' : 'false' }}">
                        <i class="fa fa-users"></i>
                        Customers
                        <i class="fas fa-angle-down fa-pull-right"></i>
                    </a>
                    <ul class="collapse list-unstyled {{ request()->segment(1) == 'admin-customer' ? 'show' : '' }}" id="Customers">
                        <li>
                            <a href="{{ route('adminshowcustomer') }}"> Show Customers</a>
                        </li>
                        <li>
                            <a href="{{route('admin-customer.add')}}"> Add Customers</a>
                        </li>

                    </ul>
                </li>

                 <li class="">
                    <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false">
                        <i class="fa fa-user"></i>
                        Inquiry
                        <i class="fas fa-angle-down fa-pull-right"></i>
                    </a>
                    <ul class="collapse list-unstyled" id="homeSubmenu">
                        <li>
                            <a href="{{ url('adminInquiryForm') }}">Add Inquiry</a>
                        </li>
                        <li>
                            <a href="{{ url('allinquiry') }}">Show Inquiry</a>
                        </li>
                    </ul>
                </li>    --}}

                <li class="{{ request()->segment(1) == 'member-plans' ? 'active' : '' }}">
                    <a href="#member-plans" data-toggle="collapse" aria-expanded="false">
                        <i class="fa fa-users"></i>
                        Members
                        <i class="fas fa-angle-down fa-pull-right"></i>
                    </a>
                    <ul class="collapse list-unstyled {{ request()->segment(1) == 'member-plans' ? 'show' : '' }}" id="member-plans">
                        <li>
                            <!-- <a href="/employeemembershippackage">Show Plans</a> -->
                            <a href='{{url("/member-plans")}}'>Membership plans</a>
                        </li>
                        <li>
                            <!-- <a href="/employeemembershippackage">Show Plans</a> -->
                            <a href='{{url("/member-plans/memberlist")}}'>Members list</a>
                        </li>

                        {{-- <li>
                            <a href='{{url("/member-plans/invited_members")}}'>Invited members</a>
                        </li> --}}
                    
                        
                    </ul>
                </li> 


                <li>
                    <a href="{{ url('/adminshowemployee') }}">
                         <i class="fa fa-user"></i>
                        Employees
                    </a>
                </li>

                {{--
                <li class="{{ request()->segment(1) == 'adminshowemployee' ? 'active' : '' }}">
                    <a href="#employee" data-toggle="collapse" aria-expanded="{{ request()->segment(1) == 'adminshowemployee' ? 'true' : 'false' }}">
                        <i class="fa fa-user"></i>
                        Employees
                        <i class="fas fa-angle-down fa-pull-right"></i>
                    </a>
                    <ul class="collapse list-unstyled {{ request()->segment(1) == 'adminshowemployee' ? 'show' : '' }}" id="employee">
                        <li>
                            <a href="{{ url('admininsertemployee') }}">Add Employees</a>
                        </li>
                        <li>
                            <a href="{{ url('adminshowemployee') }}">Show Employees</a>
                        </li>

                    </ul>
                </li>
                --}}
                <li>
                    <a href="{{ url('/admin-agent') }}">
                         <i class="fa fa-users"></i>
                        Agents
                    </a>
                </li>

                {{-- <li class="{{ request()->segment(1) == 'admin-agent' ? 'active' : '' }}">
                    <a href="#agent" data-toggle="collapse" aria-expanded="{{ request()->segment(1) == 'admin-agent' ? 'true' : 'false' }}">
                        <i class="fa fa-users"></i>
                        Agents
                        <i class="fas fa-angle-down fa-pull-right"></i>
                    </a>
                    <ul class="collapse list-unstyled {{ request()->segment(1) == 'admin-agent' ? 'show' : '' }}" id="agent">
                        <li class="">
                            <a href="{{ url('/admin-agent') }}">Show Agent</a>
                        </li>
                        <li class="">
                            <a href="{{ url('/admin-agent/add') }}"> Add Agent</a>
                        </li> 

                    </ul>
                </li> --}}

                
                <li class="{{ request()->segment(1) == 'adminshowtours' ? 'active' : '' }} {{ request()->segment(1) == 'inserttour' ? 'active' : '' }} {{ request()->segment(1) == 'itenaryshow' ? 'active' : '' }}">
                    <a href="#tours" data-toggle="collapse" aria-expanded="{{ request()->segment(1) == 'adminshowtours' ? 'true' : 'false' }} {{ request()->segment(1) == 'inserttour' ? 'true' : 'false' }} {{ request()->segment(1) == 'itenaryshow' ? 'true' : 'false' }}">
                        <i class="fa fa-users"></i>
                        Tours
                        <i class="fas fa-angle-down fa-pull-right"></i>
                    </a>
                    <ul class="collapse list-unstyled {{ request()->segment(1) == 'adminshowtours' ? 'show' : '' }} {{ request()->segment(1) == 'inserttour' ? 'show' : '' }} {{ request()->segment(1) == 'itenaryshow' ? 'show' : '' }}" id="tours">
                        <li class="{{ request()->segment(1) == 'adminshowtours' ? 'active' : '' }}">
                            <a href="{{url('/adminshowtours')}}">Tours List</a>
                        </li>
                        <li class="{{ request()->segment(1) == 'itenaryshow' ? 'active' : '' }}">
                            <a href="{{url('/itenaryinfo')}}">Itinerary</a>
                        </li>
                        <li class="{{ request()->segment(1) == 'adminshowpackages' ? 'active' : '' }}">
                            <a href="{{url('/adminshowpackages')}}">Tours Category</a>
                        </li>
                        
                    </ul>
                </li>
                <li>
                    <a href="{{ url('/admin-banner') }}">
                         <i class="fa fa-image"></i>
                        Banner
                    </a>
                </li>

                {{-- <li class="{{ request()->segment(1) == 'admin-banner' ? 'active' : '' }}">
                    <a href="#Banner" data-toggle="collapse" aria-expanded="{{ request()->segment(1) == 'admin-banner' ? 'true' : 'false' }}">
                        <i class="fas fa-image"></i>
                        Banner
                        <i class="fas fa-angle-down fa-pull-right"></i>
                    </a>
                    <ul class="collapse list-unstyled {{ request()->segment(1) == 'admin-banner' ? 'show' : '' }}" id="Banner">
                        <li>
                            <a href="{{ route('admin-banner.index') }}"> Show Banner</a>
                        </li>
                        <li>
                            <a href="{{ route('admin-banner.add') }}"> Add Banner</a>
                        </li>

                    </ul>
                </li> --}}

                
                 {{-- <li class="{{ request()->segment(1) == 'admin-askforcal' ? 'active' : '' }}">
                    <a href="#admin-askforcal" data-toggle="collapse" aria-expanded="false">
                        <i class="fa fa-users"></i>
                        Ask for calls
                        <i class="fas fa-angle-down fa-pull-right"></i>
                    </a>
                    <ul class="collapse list-unstyled" id="admin-askforcal">
                        <li>
                            <!-- <a href="/employeemembershippackage">Show Plans</a> -->
                            <a href='{{url("/admin-askforcall")}}'>Show Ask for call</a>
                        </li>
                    
                        
                    </ul>
                </li>  
                 <li>
                    <a href="{{ url('salarygenerate') }}">
                        <i class="fas fa-clone"></i>
                        Salary Generation
                    </a>
                </li>

                <li>
                    <a href="#salarymanage" data-toggle="collapse" aria-expanded="false">
                        <i class="fas fa-dollar-sign"></i>

                        Salary Manage
                        <i class="fas fa-angle-down fa-pull-right"></i>
                    </a>
                    <ul class="collapse list-unstyled" id="salarymanage">
                        <li>
                            <a href="/showsalary">Show Salary</a>
                        </li>
                        <li>
                            <a href="/addsalary">Add Salary</a>
                        </li>


                    </ul>
                </li> --}}

                <li class="{{ request()->segment(1) == 'admin-category' ? 'active' : '' }}">
                    <a href="#Int_category" data-toggle="collapse" aria-expanded="{{ request()->segment(1) == 'admin-category' ? 'true' : 'false' }}">
                        <i class="fas fa-image"></i>
                        Interactive Category
                        <i class="fas fa-angle-down fa-pull-right"></i>
                    </a>
                    <ul class="collapse list-unstyled {{ request()->segment(1) == 'admin-category' ? 'show' : '' }}" id="Int_category">
                        <li>
                            <a href="{{ url('admin-category') }}"> Category</a>
                        </li>
                        <li>
                            <a href="{{ url('admin-category/sub_category') }}"> Sub Category</a>
                        </li>
                        <li>
                            <a href="{{ url('admin-category/category_assignment') }}">Category Assignment</a>
                        </li>

                    </ul>
                </li>

                <!-- <li class="{{ request()->segment(1) == 'adminshowpackages' ? 'active' : '' }} {{ request()->segment(1) == 'admininsertPackageForm' ? 'active' : '' }}">
                    <a href="#tplans" data-toggle="collapse" aria-expanded="false">
                        <i class="fas fa-table"></i>
                        Tours Category

                        <i class="fas fa-angle-down fa-pull-right"></i>
                    </a>
                    <ul class="collapse list-unstyled" id="tplans">
                        <li>

                            <a href="{{ url('adminshowpackages') }}">Show Tour Category</a>
                        </li>
                        <li>
                            <a href="{{ url('admininsertPackageForm') }}">Add Tour Category</a>
                        </li>

                    </ul>
                </li> -->

                <li class="{{ request()->segment(1) == 'adminLeaveForm' ? 'active' : '' }}">
                    <a href="#tleaves" data-toggle="collapse" aria-expanded="false">
                        <i class="fas fa-table"></i>
                        Leave Managment

                        <i class="fas fa-angle-down fa-pull-right"></i>
                    </a>
                    <ul class="collapse list-unstyled {{ request()->segment(1) == 'adminLeaveForm' ? 'show' : '' }}" id="tleaves">
                        <li>

                            <a href="{{ url('leaves_info') }}">Show Calender</a>
                        </li>
                        <li>
                            <a href="{{ url('adminLeaveForm') }}">Leaves</a>
                        </li>
                        <li>
                            <a href="{{ url('allrequest') }}">Leaves Request</a>
                        </li>
                    </ul>
                </li>
                {{-- <li>
                    <a>
                        <i class="fa fa-shopping-cart"></i>
                        Orders
                    </a>
                </li> 
                <li>
                    <a href="{{ url('empchat') }}">
                        <i class="fa fa-comments"></i>
                        Messenger
                    </a>
                </li>

                <li>
                    <a href="{{ url('allrequest') }}">
                        <i class="fas fa-clone"></i>
                        Leave Request Managment
                    </a>
                </li>

                <li>
                    <a href="{{ url('showevent') }}">
                        <i class="fas fa-clone"></i>
                        Events
                    </a>
                </li>

                <li>
                    <a href="{{ url('Review') }}">
                        <i class="fas fa-clone"></i>
                        Review
                    </a>
                </li> --}}
                <li class="{{ request()->segment(1) == 'ads' ? 'active' : '' }}">
                    <a href="{{ url('ads') }}">
                        <i class="fas fa-table"></i>
                        Ads
                    </a>
                </li>
                <li class="{{ request()->segment(1) == 'blogs' ? 'active' : '' }}">
                    <a href="{{ url('blogs') }}">
                        <i class="fas fa-table"></i>
                        Blog
                    </a>
                </li>
                <li class="{{ request()->segment(1) == 'review/record' ? 'active' : '' }}">
                    <a href="{{ url('review/record') }}">
                        <i class="fas fa-table"></i>
                        Review
                    </a>
                </li>
                <li class="{{request()->segment(1) == 'jj-career' ? 'active' : ''}}">
                    <a href="{{ url('jj-career') }}">
                        <i class="fas fa-table"></i>
                        Careers
                    </a>
                </li>
                <li class="{{request()->segment(1) == 'tour-gallery' ? 'active' : ''}}">
                    <a href="{{ url('tour-gallery') }}">
                        <i class="fas fa-table"></i>
                        Gallery
                    </a>
                </li>
                <li class="{{request()->segment(1) == 'announcements' ? 'active' : ''}}">
                    <a href="{{ url('announcements') }}">
                        <i class="fas fa-table"></i>
                        Announcements
                    </a>
                </li>
                <li class="{{request()->segment(1) == 'event' ? 'active' : ''}}">
                    <a href="{{ url('event') }}">
                        <i class="fas fa-clone"></i>
                        Events
                    </a>
                </li>
                <li class="{{ request()->segment(1) == 'campaign' ? 'active' : '' }}">
                    <a href="{{ url('campaign') }}">
                        <i class="fas fa-clone"></i>
                        Campaign
                    </a>
                </li>
            </ul>
        </nav>

        <!-- Page Content Holder -->
        <div id="content">
            <!-- top-bar -->
            <!--         mb-xl-5 -->
            <!-- <nav class="navbar navbar-default  mb-4"> -->
                <nav class="navbar navbar-default">
                <div class="container-fluid">

                    <div class="navbar-header">
                        <button type="button" id="sidebarCollapse" class="btn btn-dark navbar-btn">
                            <i class="fas fa-bars"></i>
                        </button>
                    </div>

                    <ul class="top-icons-agileits-w3layouts float-right">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="far fa fa-user"></i>
                                <span class="badge" id='empcounta'><?php echo $empcounta; ?></span>
                            </a>
                            <div class="dropdown-menu top-grid-scroll drop-1" style='position: absolute;
                                top: 100%;
                                left: -12rem;
                                width:20rem;
                                word-wrap: break-word;
                                z-index: 1000;
                                display: none;
                                float: left;
                                min-width: 10rem;
                                max-height: 350px;
                                overflow-y:auto;
                                padding: 0.5rem 0;
                                margin: 0.125rem 0 0;
                                font-size: 1rem;
                                color: #212529;
                                text-align: left;
                                list-style: none;
                                background-color: #fff;
                                background-clip: padding-box;
                                border: 1px solid rgba(0, 0, 0, 0.15);
                                border-radius: 0.25rem;'>
                                <h3 class="sub-title-w3-agileits"></h3>
                                <div id="parent1-div">
                                </div>
                                <?php
                                $i = 1;
                                foreach ($empcountDetail as $value) {
                                if ($i <= 3) { ?> <a href="#" class="dropdown-item">
                                    <!-- <div class="notif-img-agileinfo">
                                       
                                    </div> -->
                                    <div class="notif-content-wthree">
                                        <span class="text-diff"><?php echo $value->name; ?>
                                            <?php echo 'Need employeement Approval '; ?>
                                        </span><br>


                                        <buttongroup>
                                            <!--  <a href="{{ route('AdeminEmpResult', [$value->id, 'Approve']) }}" style='margin-left:1rem'>
                                         <button class='btn btn-success'>Approve</button></a>
                                         <a href="{{ route('AdeminEmpResult', [$value->id, 'Reject']) }}">
                                         <button class='btn btn-danger'>Reject</button></a> -->

                                            <a href="{{ route('employeedetails', base64_encode($value->id)) }}"> &nbsp;&nbsp
                                                <button class='btn btn-info'>Detail</button></a>

                                        </buttongroup>
                                    </div>
                                    <div class="dropdown-divider"></div>

                                    </a>
                                    <?php }
                                    $i++;
                                    }
                                    ?>
                                    <!-- /************ Close 1 Notification ************ */ -->

                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="far fa fa-user"></i>
                                <span class="badge" id='leavecount'><?php echo $count; ?></span>
                            </a>
                            <div class="dropdown-menu top-grid-scroll drop-1" style='position: absolute;
                                    top: 100%;
                                    left: -12rem;
                                    width:20rem;
                                    word-wrap: break-word;
                                    z-index: 1000;
                                    display: none;
                                    float: left;
                                    min-width: 10rem;
                                    max-height: 350px;
                                    overflow-y:auto;
                                    padding: 0.5rem 0;
                                    margin: 0.125rem 0 0;
                                    font-size: 1rem;
                                    color: #212529;
                                    text-align: left;
                                    list-style: none;
                                    background-color: #fff;
                                    background-clip: padding-box;
                                    border: 1px solid rgba(0, 0, 0, 0.15);
                                    border-radius: 0.25rem;' id='appendleave'>
                                <h3 class="sub-title-w3-agileits"></h3>
                                <div id="parent-div">
                                </div>
                                <?php
                                $i = 1;
                                foreach ($pendingleavs as $value) {
                                if ($i <= 3) { ?> <a href="#" class="dropdown-item">
                                    <!-- <div class="notif-img-agileinfo">
                                       
                                    </div> -->
                                    <div class="notif-content-wthree">
                                        <span class="text-diff"><?php echo $value->name; ?>
                                            <?php echo 'Request For ' . $value->leave_type . ' At ' .
                                            '<br>' . $value->leave_date_start . ' To ' . $value->leave_date_end; ?>
                                        </span><br>


                                        <buttongroup>
                                            <!--                                          <a href="{{ route('AdeminLeaveResult', [$value->leaveid, 'Approve']) }}" style='margin-left:1rem'>
                                         <button class='btn btn-success'>Approve</button></a>
                                         <a href="{{ route('AdeminLeaveResult', [$value->leaveid, 'Reject']) }}">
                                         <button class='btn btn-danger'>Reject</button></a> -->
                                            &nbsp;&nbsp
                                            <a href="{{ route('DetailLeaveRequest', $value->leaveid) }}">
                                                <button class='btn btn-info'
                                                    style='margin-left:20px;margin-bottom:20px'>Detail</button></a>

                                        </buttongroup>
                                    </div>
                                    <div class="dropdown-divider"></div>

                                    </a>
                                    <?php }
                                    $i++;
                                    }
                                    ?>
                                    <!-- /************ Close 1 Notification ************ */ -->
                                    <a class="dropdown-item" href="{{ url('allrequest') }}">View all
                                        Notifications</a>
                            </div>
                        </li>


                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown2" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="far fa-user"></i>
                            </a>
                            <div class="dropdown-menu drop-3" style='position: absolute;
                                top: 100%;
                                left: -12rem;
                                width:15rem;
                                word-wrap: break-word;
                                z-index: 1000;
                                display: none;
                                float: left;
                                min-width: 6rem;
                                max-height: 350px;
                                overflow-y:auto;
                                padding: 0.5rem 0;
                                margin: 0.125rem 0 0;
                                font-size: 1rem;
                                color: #212529;
                                text-align: left;
                                list-style: none;
                                background-color: #fff;
                                background-clip: padding-box;
                                border: 1px solid rgba(0, 0, 0, 0.15);
                                border-radius: 0.25rem;'>
                                <div class="profile d-flex mr-o">
                                    <!-- <div class="profile-l align-self-center">
                                        <img src="{{ asset('admin/images/profile.jpg') }}" class="img-fluid mb-3" alt="Responsive image">
                                    </div> -->
                                    <!-- profile-r align-self-center -->
                                    <div class="profile-r align-self-center">
                                        <center>
                                            <h5 class="dropdown-item mt-3">
                                                <b>Hello</b> {{ Session::get('admin_name') }}

                                            </h5>
                                        </center>

                                    </div>
                                </div>
                                <a href="{{ url('getAdminProfile') }}" class="dropdown-item mt-3">
                                    <h4>
                                        <i class="far fa-user mr-3"></i>Change Profile
                                    </h4>
                                </a>
                                <a href="{{ url('changePasword') }}" class="dropdown-item mt-3">
                                    <h4>
                                        <i class="far fa-user mr-3"></i>Change Password
                                    </h4>
                                </a>
                                <a href="{{ route('admin.setting') }}" class="dropdown-item mt-3">
                                    <h4>
                                        <i class="fa fa-cog mr-3"></i>Setting
                                    </h4>
                                </a>

                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="{{ url('adminlogout') }}">Logout</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>

            @yield('bodyData')
            <x:notify-messages />
            <style type="text/css">
                .new_footer{
                    position: fixed;
                    height: 58px !important;
                    bottom: 0px !important;
                    width: 100% !important;
                    display: block !important;
                    float: left !important;
                    background-color: #083672 !important;
                    border-radius: 0px 0px 0px 0px !important;
                    border-color: #083672 !important;
                    margin-left: -20px !important;
                }
            </style>
            <!-- <div class="copyright-w3layouts py-xl-3 py-2 mt-xl-5 mt-4 text-center"> -->
            <div class="new_footer py-xl-3 text-center mt-3">
                    <p style="color: #fff;">© {{ date('Y') }} All Rights Reserved | Design by Jag Joyu</p>
            </div>


            @stack('js')
           
             
             <script src="{{ url('vendor/mckenziearts/laravel-notify/js/notify.js') }} "></script>
            <script src="{{ asset('admin/js/modernizr.js') }} "></script>
           
            <script>
                $(document).ready(function() {
                    $('#sidebarCollapse').on('click', function() {
                        $('#sidebar').toggleClass('active');
                    });
                });

            </script>
            <!--// Sidebar-nav Js -->

            <!-- Graph -->
            <script src="{{ asset('admin/js/SimpleChart.js') }} "></script>
            <script>
                var graphdata4 = {
                    linecolor: "Random",
                    title: "Thursday",
                    values: [{
                            X: "6",
                            Y: 300.00
                        },
                        {
                            X: "7",
                            Y: 101.98
                        },
                        {
                            X: "8",
                            Y: 140.00
                        },
                        {
                            X: "9",
                            Y: 340.00
                        },
                        {
                            X: "10",
                            Y: 470.25
                        },
                        {
                            X: "11",
                            Y: 180.56
                        },
                        {
                            X: "12",
                            Y: 680.57
                        },
                        {
                            X: "13",
                            Y: 740.00
                        },
                        {
                            X: "14",
                            Y: 800.89
                        },
                        {
                            X: "15",
                            Y: 420.57
                        },
                        {
                            X: "16",
                            Y: 980.24
                        },
                        {
                            X: "17",
                            Y: 1080.00
                        },
                        {
                            X: "18",
                            Y: 140.24
                        },
                        {
                            X: "19",
                            Y: 140.58
                        },
                        {
                            X: "20",
                            Y: 110.54
                        },
                        {
                            X: "21",
                            Y: 480.00
                        },
                        {
                            X: "22",
                            Y: 580.00
                        },
                        {
                            X: "23",
                            Y: 340.89
                        },
                        {
                            X: "0",
                            Y: 100.26
                        },
                        {
                            X: "1",
                            Y: 1480.89
                        },
                        {
                            X: "2",
                            Y: 1380.87
                        },
                        {
                            X: "3",
                            Y: 1640.00
                        },
                        {
                            X: "4",
                            Y: 1700.00
                        }
                    ]
                };
                $(function() {
                    $("#Hybridgraph").SimpleChart({
                        ChartType: "Hybrid",
                        toolwidth: "50",
                        toolheight: "25",
                        axiscolor: "#E6E6E6",
                        textcolor: "#6E6E6E",
                        showlegends: false,
                        data: [graphdata4],
                        legendsize: "140",
                        legendposition: 'bottom',
                        xaxislabel: 'Hours',
                        title: 'Weekly Profit',
                        yaxislabel: 'Profit in $'
                    });
                });

            </script>
            <!--// Graph -->
            <!-- Bar-chart -->
            <script src="{{ asset('admin/js/rumcaJS.js') }}"></script>
            <script src="{{ asset('admin/js/example.js') }}"></script>
            <!--// Bar-chart -->
            <!-- Calender -->
            <script src="{{ asset('admin/js/moment.min.js') }}"></script>
            <script src="{{ asset('admin/js/pignose.calender.js') }}"></script>
            <script>
                //<![CDATA[
                $(function() {
                    $('.calender').pignoseCalender({
                        select: function(date, obj) {
                            obj.calender.parent().next().show().text('You selected ' +
                                (date[0] === null ? 'null' : date[0].format('YYYY-MM-DD')) +
                                '.');
                        }
                    });

                    $('.multi-select-calender').pignoseCalender({
                        multiple: true,
                        select: function(date, obj) {
                            obj.calender.parent().next().show().text('You selected ' +
                                (date[0] === null ? 'null' : date[0].format('YYYY-MM-DD')) +
                                '~' +
                                (date[1] === null ? 'null' : date[1].format('YYYY-MM-DD')) +
                                '.');
                        }
                    });
                });
                //]]>

            </script>
            <!--// Calender -->

            <!-- profile-widget-dropdown js-->
            <script src="{{ asset('admin/js/script.js') }}"></script>
            <!--// profile-widget-dropdown js-->

            <!-- Count-down -->
           {{--  <script src="{{ asset('admin/js/simplyCountdown.js') }}"></script> --}}
            <link href="{{ asset('admin/css/simplyCountdown.css') }}" rel='stylesheet' type='text/css' />
            <script>
                var d = new Date();
                simplyCountdown('simply-countdown-custom', {
                    year: d.getFullYear(),
                    month: d.getMonth() + 2,
                    day: 25
                });

            </script>
            <!--// Count-down -->

            <!-- pie-chart -->
            <script src='{{ asset('admin/js/amcharts.js') }}'></script>
            <script>
                var chart;
                var legend;

                var chartData = [{
                        country: "Lithuania",
                        value: 260
                    },
                    {
                        country: "Ireland",
                        value: 201
                    },
                    {
                        country: "Germany",
                        value: 65
                    },
                    {
                        country: "Australia",
                        value: 39
                    },
                    {
                        country: "UK",
                        value: 19
                    },
                    {
                        country: "Latvia",
                        value: 10
                    }
                ];

                AmCharts.ready(function() {
                    // PIE CHART
                    chart = new AmCharts.AmPieChart();
                    chart.dataProvider = chartData;
                    chart.titleField = "country";
                    chart.valueField = "value";
                    chart.outlineColor = "";
                    chart.outlineAlpha = 0.8;
                    chart.outlineThickness = 2;
                    // this makes the chart 3D
                    chart.depth3D = 20;
                    chart.angle = 30;

                    // WRITE
                    chart.write("chartdiv");
                });

            </script>
            <!--// pie-chart -->
            <!-- dropdown nav -->
            <script>
                $(document).ready(function() {
                    $(".dropdown").hover(
                        function() {
                            $('.dropdown-menu', this).stop(true, true).slideDown("fast");
                            $(this).toggleClass('open');
                        },
                        function() {
                            $('.dropdown-menu', this).stop(true, true).slideUp("fast");
                            $(this).toggleClass('open');
                        }
                    );
                });

            </script>
            <!-- //dropdown nav -->
            <!-- Js for bootstrap working-->
            <script src="{{ asset('admin/js/bootstrap.min.js') }} "></script>
            <script src="https://js.pusher.com/4.1/pusher.min.js"></script>

            <script>
                var pusher = new Pusher('1aa6ea5ecbe8b1e5cc4a', {
                    cluster: '{{ env('PUSHER_APP_CLUSTER') }}',
                    encrypted: true
                });
                var channel = pusher.subscribe('leave-channel');

                channel.bind('App\\Events\\Leave', function(data) {

                    document.getElementById('leavecount').innerHTML = data.count;

                });

            </script>
            <script>
                var receiver_id = '';
                var my_id = "{{ Auth::id() }}";
                $(document).ready(function() {
                    // ajax setup form csrf token
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    // Enable pusher logging - don't include this in production
                    Pusher.logToConsole = true;

                    var pusher = new Pusher('{{ env('MIX_PUSHER_APP_KEY') }}', {
                        cluster: 'ap2',
                        forceTLS: true
                    });

                    var channel = pusher.subscribe('my-channel');
                    channel.bind('my-event', function(data) {
                        // alert(JSON.stringify(data));
                        if (my_id == data.from) {
                            $('#' + data.to).click();
                        } else if (my_id == data.to) {
                            if (receiver_id == data.from) {
                                // if receiver is selected, reload the selected user ...
                                $('#' + data.from).click();
                            } else {
                                // if receiver is not seleted, add notification for that user
                                var pending = parseInt($('#' + data.from).find('.pending').html());

                                if (pending) {
                                    $('#' + data.from).find('.pending').html(pending + 1);
                                } else {
                                    $('#' + data.from).append('<span class="pending">1</span>');
                                }
                            }
                        }
                    });

                    $('.user').click(function() {
                        $('.user').removeClass('active');
                        $(this).addClass('active');
                        $(this).find('.pending').remove();

                        receiver_id = $(this).attr('id');
                        $.ajax({
                            type: "get",
                            url: "message/" + receiver_id, // need to create this route
                            data: "",
                            cache: false,
                            success: function(data) {
                                $('#messages').html(data);
                                scrollToBottomFunc();
                            }
                        });
                    });

                    $(document).on('keyup', '.input-text input', function(e) {
                        var message = $(this).val();

                        // check if enter key is pressed and message is not null also receiver is selected
                        if (e.keyCode == 13 && message != '' && receiver_id != '') {
                            $(this).val(''); // while pressed enter text box will be empty

                            var datastr = "receiver_id=" + receiver_id + "&message=" + message;
                            $.ajax({
                                type: "post",
                                url: "message", // need to create this post route
                                data: datastr,
                                cache: false,
                                success: function(data) {

                                },
                                error: function(jqXHR, status, err) {},
                                complete: function() {
                                    scrollToBottomFunc();
                                }
                            })
                        }
                    });
                });

                function scrollToBottomFunc() {
                    $('.message-wrapper').animate({
                        scrollTop: $('.message-wrapper').get(0).scrollHeight
                    }, 50);
                }
            </script>

            <script src="{{ asset('admin/js/bootstrap.min.js') }} "></script>
            <script>
                var pusher = new Pusher('{{ env('MIX_PUSHER_APP_KEY') }}', {
                    cluster: '{{ env('PUSHER_APP_CLUSTER') }}',
                    encrypted: true
                });
                var channel = pusher.subscribe('message-channel');
                channel.bind('App\\Events\\Message', function(data) {

                    if (data.to == "<?php echo $id; ?>") {
                        document.getElementById('leavecount').innerHTML = data.counts;
                    }
                });
            </script>
    <script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="https://www.ostraining.com/images/coding/bpopup/jquery.bpopup.min.js"></script>
    <script type="text/javascript">
     $(document).ready(function() {
        $('#ModelPop').bPopup();
        $('iframe #toolbar').css('display','none');
    });
    </script>
</body>

</html>
