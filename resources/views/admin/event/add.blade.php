@extends('admin.master')
@section('bodyData')
<section class="forms-section" style='min-height:500px'>
<div class="outer-w3-agile mt-3">
        <h4 class="tittle-w3-agileits mb-4">@if(isset($event))Edit @else Add @endif Event</h4>
       
        <form id="EventForm" method="POST"  action="{{ route('event-store')}}" enctype="multipart/form-data">
        @csrf
            <div class="row">
               @if(isset($event))
                    <input type="hidden" id="id" name="id" value="{{$event->id}}"/>
                @endif
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text" class="form-control" name="title" id="title" required data-bv-notempty-message="The Title is required" value="@if(isset($event)){{$event->title}}@endif">
                    </div>
                    <div class="form-group">
                        <label for="type">Event Start Date</label>
                        <input type="text" class="form-control date-picker" required id="startdate" name="startdate" placeholder="Start Date"  data-bv-notempty="true" data-bv-notempty-message="Start Date is required" onchange="Start_Date()" readonly value="@if(isset($event)){{$event->startdate}}@endif">
                    </div>
                    <div class="form-group">
                        <label for="type">Event End Date</label>
                        <input type="text" class="form-control end-date" required id="enddate" name="enddate" placeholder="End Date (You must have to select the first Start Date)"  data-bv-notempty="true" data-bv-notempty-message="End Date is required" readonly value="@if(isset($event)){{$event->enddate}}@endif">
                    </div>
                    <div class="form-group">
                        <label for="title">Duration</label>
                        <input type="text" class="form-control" name="duration" id="duration" required data-bv-notempty-message="The Duration is required" value="@if(isset($event)){{$event->duration}}@endif">
                    </div>
                    <div class="form-group">
                        <label for="title">Location & Route Link</label>
                        <input type="text" class="form-control" name="location_route" id="location_route" required data-bv-notempty-message="The Location & Route Link is required" value="@if(isset($event)){{$event->location_route}}@endif">
                    </div>
                    <div class="form-group">
                        <label for="title">Content</label>
                        <textarea rows="3" class="form-control" name="content" required data-bv-notempty-message="The Content is required">@if(isset($event)){{$event->content}}@endif</textarea>
                    </div>
                </div> 
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="category">Image</label>
                        <input data-browse-on-zone-click="true" class="form-control custom-file-input" id="input-id" type="file" name="image" onchange="SelectImage()">
                    </div>
                    <div class="form-group">
                        <label for="title">Price</label>
                        <input type="text" class="form-control" name="price" id="price" required data-bv-notempty-message="The Price is required" value="@if(isset($event)){{$event->price}}@endif" onkeypress="return isNumber(event)">
                    </div>
                    <div class="form-group">
                        <label for="title">Member Discount</label>
                        <input type="text" class="form-control" name="member_discount" id="member_discount" required data-bv-notempty-message="The Member Discount is required" value="@if(isset($event)){{$event->member_discount}}@endif" onkeypress="return isNumber(event)">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="title">Discount Offers End Date</label>
                        <input type="text" class="form-control date-picker" name="offer_end_date" id="offer_end_date" required data-bv-notempty-message="The Discount Offers End Date is required" value="@if(isset($event)){{$event->offer_end_date}}@endif">
                    </div>
                    <div class="form-group">
                        <label for="title">Agent Commission</label>
                        <input type="text" class="form-control" name="agent_discount" id="agent_discount" required data-bv-notempty-message="The Agent Discount is required" value="@if(isset($event)){{$event->agent_discount}}@endif" onkeypress="return isNumber(event)">
                    </div>
                    
                    @if(isset($event))
                    <small>
                        <img src="{{$event->url}}/{{$event->image}}" width="60%" style="margin-top: 50px;">
                    </small>
                    @endif
                </div>   
            </div>
            <button type="submit" id="submitbtn" class="btn btn-primary">@if(isset($event))Update @else Submit @endif</button>
        </form>
    </div>
</section>

@endsection
@push('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.min.css" crossorigin="anonymous">

<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.8/css/fileinput.min.css">
<link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel = "stylesheet">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css"/>
<style type="text/css">
    .img-container{
        width: 15%;
        display: flex;
    }
    .img-container img{
        justify-content: center;
        margin-left: 5px;
        margin-right: 5px;
        border-radius: 20px;
        height: 100px;
    }
    ul ul a {
        font-size: 0.9em !important;
        padding-left: 30px !important;
        background: #083672;
    }
    .ui-datepicker-month{
        color: #f7af38 !important;
    }

    .ui-datepicker-year{
        color: #f7af38 !important;
    }
</style>
@endpush

@push('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.js" integrity="sha512-n/4gHW3atM3QqRcbCn6ewmpxcLAHGaDjpEBu4xZd47N0W2oQ+6q7oc3PXstrJYXcbNU1OHdQ1T7pAP+gi5Yu8g==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src = "https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
{{-- <script src="https://cdn.jsdelivr.net/gh/kartik-v/bootstrap-fileinput@5.2.5/js/plugins/piexif.min.js" type="text/javascript"></script> 
<script src="https://cdn.jsdelivr.net/gh/kartik-v/bootstrap-fileinput@5.2.5/js/plugins/sortable.min.js" type="text/javascript"></script> --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.8/js/fileinput.min.js"></script>
{{-- <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.js"></script> --}}
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js"></script>
 <script>

    $( document ).ready(function() 
    {
        $('#EventForm').bootstrapValidator();  
    });

    $('.date-picker').on('changeDate show', function(e) {
        $('#EventForm').bootstrapValidator('revalidateField', 'startdate');
    });

    $(function() {
            $( "#startdate" ).datepicker({
            minDate: new Date(),
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true,
            yearRange: '-110:+29',
            language: "es",

        });

        $('#startdate').datepicker({
            }).on('change', function(e) {
            $('#EventForm').bootstrapValidator('revalidateField', 'startdate');
        });

        $('#offer_end_date').datepicker({
            }).on('change', function(e) {
            $('#EventForm').bootstrapValidator('revalidateField', 'offer_end_date');
        });
        
    });
    

    $(function() {
            $( "#offer_end_date" ).datepicker({
            minDate: new Date(),
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true,
            yearRange: '-110:+29',
            language: "es",

        });
    });
    
    function Start_Date()
    {
        var startDate = $('#startdate').val();
        $("#enddate").datepicker("destroy");
         $(function() {
            $( "#enddate" ).datepicker({
            minDate: new Date(startDate),
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true,
            yearRange: '-110:+29',
            language: "es",
        });

        $('#enddate').datepicker({
            }).on('change', function(e) {
            $('#EventForm').bootstrapValidator('revalidateField', 'enddate');
        });
    });
    }

@if(isset($event))
$("#input-id").fileinput({
      'showUpload':false, 
      'showCancel':false,
      'showClose':false,
      'showRemove':true,
      'allowedFileExtensions': ["jpg","png",'jpeg','mp4'],
       maxFileCount: 5,
       maxFileSize: 2000,
       
      
    });
@else
$("#input-id").fileinput({
      'showUpload':false, 
      'showCancel':false,
      'showClose':false,
      'showRemove':true,
      'allowedFileExtensions': ["jpg","png",'jpeg','mp4'],
       maxFileCount: 5,
       maxFileSize: 2000,
       required: true,
       
      
    });
@endif

function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

$(document).ready(function() {
    $('#EventForm')
        .find('[name="image"]')
            //.select()
            // Revalidate the color when it is changed
            .change(function(e) {
                $('#EventForm').bootstrapValidator('revalidateField', 'image');
            })
            .end()
        .bootstrapValidator({
            excluded: ':disabled',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                image: {
                    validators: {
                        callback: {
                            message: 'Please choose at least one image',
                            callback: function(value, validator) {
                                // Get the selected options
                                var options = validator.getFieldElements('image').val();
                                console.log(options);
                                return (options != null && options.length >= 2 && options.length <= 4);
                            }
                        }
                    }
                }
            }
        });
});

function SelectImage()
{
    $('#submitbtn').removeAttr('disabled');
    return false;
}
</script> 

@endpush
