@extends('admin.master')
@section('bodyData')
<div class="outer-w3-agile mt-3">
  <div class='row'>
    <div class='col-md-6'>
      <div class="btn btn-primary"><i class="fa fa-list mr-2"></i>Events list</div>
    </div>
    <div class='col-md-6 text-right'>
      <a class="btn btn-primary" href="{{ route('event-add')}}"><i class="fa fa-plus mr-2"></i>Add Event</a>
    </div>
  </div><br>
  <div class="table-responsive">
    <table class="table table-hover" style="width: 100%;" id="dataTable">
      <thead class="thead-dark">
        <tr>
          <th>#</th>
          <th>Title</th>
          <th>Start Date</th>
          <th>End Date</th>
          <th>Duration</th>
          <th>Content</th>
          <th>Price</th>
          <th>Discount Price</th>
          <th>Image</th>
          <th>Action</th>
        </tr>
      </thead>
    </table>
  </div>
</div>
@endsection
@push('css')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css">
@endpush
@push('js')
<script>
  $(function() {
    $("#dataTable").DataTable({
        'processing': true,
        'serverSide': true,
        'serverMethod': 'post',
        "aLengthMenu": [[10,25, 50,100], [10,25, 50,100]],
        order: [
            [0, 'desc']
        ],
        "ajax": {
            "url": "{{ route('event-list') }}",
            "data": {
                "_token": "{{ csrf_token() }}"
            },
        },
        "columns": [
            {
                data: "id",
                render: function(data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1
                }
            },
            {
                data: "title"
            },
            {
                data: "startdate",
                render: function(data, type, row){
                  /*if(type === "sort" || type === "type"){
                     return data;
                  }*/
                  return moment(data).format("MM-DD-YYYY");
                }
            },
            {
                data: "enddate",
                render: function(data, type, row){
                  /*if(type === "sort" || type === "type"){
                     return data;
                  }*/
                  return moment(data).format("MM-DD-YYYY");
                }
            },
            {
                data: "duration"
            },
            {
                data: "content"
            },
            {
                data: "price"
            },
            {
                data: "discount_offer"
            },
            {
                data: "image",
                render: function(data, type, row, alldata) {
                  var url = window.location.origin;
                  var image = url+'/'+data; 
                  
                  if(data!=null){
                    return '<img width="80px;" src="'+image+'"/>';
                  }else{
                    return "-";
                  }
                  
                }
            },
            {
                data: "id",orderable:false,
                render: function(data, row, alldata) {
                  var $id = btoa(alldata.id);
                    $html = `<a href="{{url('event/edit')}}/`+$id+`">
                         <!-- <button class="btn btn-info">Update</button> -->
                         <i class="fa fa-edit" aria-hidden="true" title='Edit' style='font-size:20px'></i>
                         </a>
                      
                      <i class="fa fa-trash delete" style='font-size:20px;color:red;cursor:pointer;' title='delete' data-id="`+alldata.id+`" >
                      </i>`;
                    return $html;
                }
            },
        ]
    });

    $(document).on("click", '.delete', function(event) { 
    var id =$(this).attr('data-id');
    //alert(id);
    swal({
      title: "Are you sure?",
      text: "You Want To Delete This Record ,You Will Not Be Able To Recover This Record!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {

        $.ajax({
          url: "{{ url('event/delete') }}/"+id,
          type: 'get',
          dataType: 'json',
        success:function($data){
          if($data == true)
          {
            swal("Record has been deleted!", {
              icon: "success",
            }).then((result) => {
                $('#dataTable').DataTable().ajax.reload(null,false);
            });
            //location.reload('#dataTable');
          }else{
            swal("Record has been not deleted!", {
              icon: "error",
            });
          }
    
        }
      });


      } else {
        swal("Your Record is Safe!");
      }
    });
  });

    $(document).on('click','.leave',function(){
      var id = $(this).data('id');
      // alert('true');
      $('#modal-myvalue').val(id);
    });
  });
</script>
@endpush
