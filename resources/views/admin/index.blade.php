@extends('admin.master')
@section('bodyData')
<section class="forms-section" style='min-height:500px'>
<div class="outer-w3-agile mt-3">
        <h6>In This Month We Get</h6>
                <div class="row">
                    <!-- Stats -->
                    
                    <div class='col-sm-6'>
                    <div class="outer-w3-agile col-xl">
                        <div class="stat-grid p-3 d-flex align-items-center justify-content-between bg-primary">
                            <div class="s-l">
                                <h5>Total Inquiry</h5>
                                <!-- <p class="paragraph-agileits-w3layouts text-white">Lorem Ipsum</p> -->
                            </div>
                            <div class="s-r">
                                <h6><?php echo $monthInquiry?>
                                    <!-- <i class="far fa-edit"></i> -->
                                </h6>
                            </div>
                        </div>
                        <div class="stat-grid p-3 mt-3 d-flex align-items-center justify-content-between bg-success">
                            <div class="s-l">
                                <h5>Total Agents</h5>
                                <!-- <p class="paragraph-agileits-w3layouts">Lorem Ipsum</p> -->
                            </div>
                            <div class="s-r">
                                <h6><?php echo $monthAgent?>
                                    <!-- <i class="far fa-smile"></i> -->
                                </h6>
                            </div>
                        </div>
                        <div class="stat-grid p-3 mt-3 d-flex align-items-center justify-content-between bg-danger">
                            <div class="s-l">
                                <h5>This Month Order</h5>
                                <!-- <p class="paragraph-agileits-w3layouts">Lorem Ipsum</p> -->
                            </div>
                            <div class="s-r">
                                <h6><?php echo $TotalOrder?>
                                    <!-- <i class="fas fa-tasks"></i> -->
                                </h6>
                            </div>
                        </div>
                        
                    </div>
                    </div>
                    
                    <div class='col-sm-6'>
                    <div class="outer-w3-agile col-xl">
                        <div class="stat-grid p-3 d-flex align-items-center justify-content-between bg-primary">
                            <div class="s-l">
                                <h5>Total Customers</h5>
                                <!-- <p class="paragraph-agileits-w3layouts text-white">Lorem Ipsum</p> -->
                            </div>
                            <div class="s-r">
                                <h6><?php echo $MonthCustomer?>
                                    <!-- <i class="far fa-edit"></i> -->
                                </h6>
                            </div>
                        </div>
                        <div class="stat-grid p-3 mt-3 d-flex align-items-center justify-content-between bg-success">
                            <div class="s-l">
                                <h5>Total Member</h5>
                                <!-- <p class="paragraph-agileits-w3layouts">Lorem Ipsum</p> -->
                            </div>
                            <div class="s-r">
                                <h6><?php echo $monthMember?>
                                    <!-- <i class="far fa-smile"></i> -->
                                </h6>
                            </div>
                        </div>
                        <div class="stat-grid p-3 mt-3 d-flex align-items-center justify-content-between bg-danger">
                            <div class="s-l">
                                <h5>Target Goal</h5>
                                <!-- <p class="paragraph-agileits-w3layouts">Lorem Ipsum</p> -->
                            </div>
                            <div class="s-r">
                                <h6><?php  echo $MonthTarget?>
                                    <!-- <i class="fas fa-tasks"></i> -->
                                </h6>
                            </div>
                        </div>
                        
                    </div>
                    </div>
                  
                </div>
            </div>
        </section>
            <div class='row'>
            <div class='col-sm-6'>
           
                  {{--   {!! $chart->html() !!} --}}
                
            </div>
            <div class='col-sm-6'>
           
         {{--    {!! $chart1->html() !!} --}}
            </div>
            </div>
           

{{-- {!! Charts::scripts() !!} --}}

{{-- {!! $chart->script() !!}
{!! $chart1->script() !!} --}}

@endsection 
