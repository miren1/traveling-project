@extends('admin.master')
@section('bodyData')

@if ($message = Session::get('Itenaryadded'))
<div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <strong>{{ $message }}</strong>
</div>
@endif

<section class="forms-section" style='min-height:500px'>
<div class="outer-w3-agile mt-3">
        

<h4 class="tittle-w3-agileits mb-4">Add Itinerary</h4>

@if(!isset($info))
<a href='{{url("/adminshowtours")}}' class='btn btn-primary'>Show Tours</a>
@endif
<div class="mt-4">
<form data-toggle="validator" method='post' id="adminshowtours" action='{{url("/additenary")}}' enctype="multipart/form-data" class="form-horizontal"
    data-bv-feedbackicons-valid="glyphicon glyphicon-ok"
    data-bv-feedbackicons-invalid="glyphicon glyphicon-remove"
    data-bv-feedbackicons-validating="glyphicon glyphicon-refresh">
    @csrf
     @if(isset($info))
        <input type="hidden" name="id" value="{{$info->id}}"/>
    @endif   
    <div class='row mt-4'>
        <div class='col-sm-6 form-group'>
            <label for="details">Enter Title:</label>
            <input type='text' class="form-control" name='details' id='details' placeholder='Enter Name'
                class='form-control' data-bv-notempty-message="The Name is required" required value="@if(isset($info)){{$info->details}}@endif">

        </div>
        <div class='col-sm-6 form-group'>
            <label for="tour_fk_id">Tour Name:</label>

            <select class='form-control' name='tour_fk_id' id="tour_fk_id" data-bv-notempty="true" data-bv-notempty-message="The Tour Id is required" >
                @foreach($tour as $value)
                    <option value='{{$value->id}}' @if(isset($info) && $value->id==$info->tour_fk_id) selected @endif />{{$value->name}}</option>
                @endforeach
            </select>

        </div>
    </div>
    <div class='row'>
        <div class='col-sm-12 form-group'>
            <label>Day:</label>
            <input type='number' name='day_number' data-bv-notempty-message="The Day is required" min="0" placeholder='Enter Day' class='form-control' required value="@if(isset($info)){{$info->day_number}}@endif">
        </div>
    </div>

    <div class='row'>
        <div class='col-sm-12 form-group'>
            <label for="caption">Description :</label>
<!-- 
            <input type='text' class="form-control" name='caption' id="caption" placeholder='Enter Description'
                class='form-control' data-bv-notempty-message="The Description is required" required>
 -->
            <textarea name='caption' id="caption" class="form-control" placeholder='Enter Description' data-bv-notempty="true" data-bv-notempty-message="The Description is required">@if(isset($info)){{$info->caption}}@endif</textarea>


        </div>
    </div>

    <div class='row'>
        <div class='col-sm-12 form-group '>
            <label>Image:</label>
            @if(!isset($info))
                <input type='file' class="form-control" name='images' class='form-control'required=""
                accept="image/*"
                data-bv-notempty-message="The Image is required"
                data-bv-file-extension="jpeg,jpg,png"
                data-bv-file-type="image/jpeg,image/jpg,image/png"

                data-bv-file-message="The selected file is not valid"
                data-dragdrop="true" data-btnClass="btn-primary" data-buttonBefore="true" />
            @endif
            @if(isset($info)) 
                <input type='file' class="form-control" name='images' value="@if(isset($info)){{$info->images}} @endif" class='form-control' data-btnClass="btn-primary" data-buttonBefore="true" />
            @endif
        </div>
        @if(isset($info))
            <img src="{{ url('itenary/'.$info->images)}}" style="width: 10%; margin-left: 20px;">
        @endif
    </div>

    <br>

    <button type="submit" class="btn btn-primary mr-5 right">@if(isset($info)) Update @else Submit @endif</button>
    
</form>
</div>

</div>
</section>
@endsection


@push('css')
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css"/>
@endpush
@push('js')
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js"></script>
<script>

  $(document).ready(function () {
    $('#adminshowtours').bootstrapValidator({
        fields: {
            images:{
                validators:{
                    file:{
                                   
                            extension: 'jpeg,png,jpg',
                            type: 'image/jpeg,image/jpg,image/png',
                            message: 'The selected file is not valid, it should be (jpeg,png).'
                        }
                }
            }
        }
    });
    
  });
</script>
@endpush