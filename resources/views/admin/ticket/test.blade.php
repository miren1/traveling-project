@extends('employee.master')
@section('bodyData')

    <style>
        /* width */
        ::-webkit-scrollbar {
            width: 7px;
        }

        /* Track */
        ::-webkit-scrollbar-track {
            background: #f1f1f1;
        }

        /* Handle */
        ::-webkit-scrollbar-thumb {
            background: #a7a7a7;
        }

        /* Handle on hover */
        ::-webkit-scrollbar-thumb:hover {
            background: #929292;
        }

        ul {
            margin: 0;
            padding: 0;
        }

        li {
            list-style: none;
        }

        .user-wrapper,
        .message-wrapper {
            border: 1px solid #dddddd;
            overflow-y: auto;
        }

        .user-wrapper {
            height: 600px;
            /* background:green; */
        }

        .user {
            cursor: pointer;
            padding: 5px 0;
            position: relative;
        }

        .user:hover {
            background: #eeeeee;
        }

        .user:last-child {
            margin-bottom: 0;
        }

        .pending {
            position: absolute;
            left: 13px;
            top: 9px;
            background: #b600ff;
            margin: 0;
            border-radius: 50%;
            width: 18px;
            height: 18px;
            line-height: 18px;
            padding-left: 5px;
            color: #ffffff;
            font-size: 12px;
        }

        .media-left {
            margin: 0 10px;
        }

        .media-left img {
            width: 64px;
            border-radius: 64px;
        }

        .media-body p {
            margin: 6px 0;
        }

        .message-wrapper {
            padding: 10px;
            height: 536px;
            background: #ffffff;
        }

        .messages .message {
            margin-bottom: 15px;
        }

        .messages .message:last-child {
            margin-bottom: 0;
        }

        .received,
        .sent {
            width: 45%;
            padding: 3px 10px;
            border-radius: 10px;
        }

        .received {
            background: #ffffff;
        }

        .sent {
            background: #3bebff;
            float: right;
            text-align: right;
        }

        .message p {
            margin: 5px 0;
        }

        .date {
            color: #777777;
            font-size: 12px;
        }

        .active {
            background: #eeeeee;
        }

        textarea#message_input {
            border: 1px solid #cccccc;
            background-color: #f6f6f6 !important;
        }

        input[type=text] {
            width: 100%;
            padding: 12px 20px;
            margin: 15px 0 0 0;
            display: inline-block;
            border-radius: 4px;
            box-sizing: border-box;
            outline: none;
            border: 1px solid #cccccc;
        }

        input[type=text]:focus {
            border: 1px solid #aaaaaa;
        }

        svg.feather.feather-paperclip.w-full.h-full {
            width: 50% !important;
        }

    </style>

    <div class="container-fluid">
        <section class="forms-section" style='min-height:200px'>
            <div class="outer-w3-agile mt-3">
                <a href='{{ url('/employee-ticket-generator') }}' class='btn btn-primary' style='color:white'>All
                    Ticket's</a>
                <h4 class="tittle-w3-agileits mb-2"> Ticket</h4>
            </div>




            <div class="row mt-4">
                <div class="col-md-4">
                    <div class="user-wrapper">
                        <ul class="users">

                            <li class="user" id="{{ $ticket->id }}">

                                @if ($ticket->status == 'active')
                                    <span class="pending"><span></span></span>
                                @endif

                                <div class="media">
                                    <div class="media-left">
                                        <img src="{{ url('public/images') }}/user.png" alt="" class="media-object">
                                    </div>

                                    <div class="media-body">
                                        <p class="name">Admin</p>
                                        <ul>
                                            @if (isset($ticket))
                                                @if (isset($ticket->ticket_categories->name))
                                                    <b>Categories:</b>
                                                    {{ $ticket->ticket_categories->name }}


                                                @endif
                                                @if (isset($ticket->ticket_sub_categories->name))
                                                    <br /><b>Sub Categories:
                                                    </b>{{ $ticket->ticket_sub_categories->name }}
                                                @endif

                                            @endif
                                        </ul>
                                    </div>
                                </div>
                            </li>

                        </ul>
                    </div>
                </div>

                <div class="col-md-8" id="messages">
                    <div class="message-wrapper">
                        <ul class="messages" id="media_list">


                        </ul>
                    </div>
                    <div id="inputmsg" class="">
                        {{-- <form action="javascript:void(0)" method="post" id="send_sms_form" enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-10 form-group">
                                    <textarea 
                                        class=" form-control"
                                        rows="5" placeholder="Type your message..."
                                        required aria-required="true"
                                        data-bv-notempty-message="Message is required"
                                        id="message_input"></textarea>
                                </div>
                                <input type="hidden" value="{{ $ticket->id }}" id="ticket_id" />

                                <div class="col-2 form-group" id="chat_media_body">

                            
                                    <input type="file" class=" w-full h-full top-2 left-0 absolute  chat-input"
                                        id="chat_file" name="media">
                                    


                                </div>
                                <div class="col-12"><button type="submit" class=" btn btn-primary  sms_send_button float-right"
                                    id="send_text_button">send</button></div>
                            </div>
                        </form> --}}
                    </div>
                    {{-- <div id="inputmsg" class="">
                        <form action="javascript:void(0)" method="post" id="send_sms_form" enctype="multipart/form-data">
                        <input type="hidden" value="{{ $ticket->id }}" id="ticket_id" />

                        <input type="text" name="message" class="submit">
                        <input type="file" name="anyfile" class="image" id="image_input_field" multiple
                            accept="image/x-png, image/gif, image/jpeg, image/jpg" />
                    </form>
                    </div> --}}
                </div>

            </div>
        </section>
    </div>

    <div class="container-fluid">
        <form class="defaultForm" action="{{ route('employee-ticket-generator-store') }}" method="POST"
        enctype="multipart/form-data" id="sendinfo">
        @csrf
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="body">Message</label>
                <textarea id="body" name="message" rows="8" class="form-control" required aria-required="true"
                    data-bv-notempty-message="Message is required"></textarea>
            </div>
            <div class="form-group col-md-6">
                <label for="inputEmail4">File <span style="color: red"></span></span></label>
                <input type="file" name="file" class="form-control dropify" required
                    data-bv-file-extension="jpg,jpeg,png,pdf" data-bv-file="true" />
            </div>
        </div>
        <button type="submit" name="save" class="btn btn-primary">Generator Ticket</button>
            </form>
    </div>
@endsection
@push('css')
<link rel="stylesheet"
href="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css" />
    <style type="text/css">
        div#example_filter input[type="search"] {
            box-sizing: border-box;
            border: 1px solid #d2d6dc;
        }

    </style>
@endpush
@push('js')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
   
<script type="text/javascript"
src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js"></script>
<script>
$(document).ready(function() {
   // $('.dropify').dropify();
    $('.defaultForm').bootstrapValidator();
      
});
</script>


@endpush
