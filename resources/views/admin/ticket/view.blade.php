@extends('admin.master')
@section('bodyData')
    <style>
        small.help-block {
            color: red;
        }
        /* width */
        ::-webkit-scrollbar {
            width: 7px;
        }

        /* Track */
        ::-webkit-scrollbar-track {
            background: #f1f1f1;
        }

        /* Handle */
        ::-webkit-scrollbar-thumb {
            background: #a7a7a7;
        }

        /* Handle on hover */
        ::-webkit-scrollbar-thumb:hover {
            background: #929292;
        }

        ul {
            margin: 0;
            padding: 0;
        }

        li {
            list-style: none;
        }

        .user-wrapper,
        .message-wrapper {
            border: 1px solid #dddddd;
            overflow-y: auto;
        }

        .user-wrapper {
            height: 620px;
            /* background:green; */
        }

        .user {
            cursor: pointer;
            padding: 5px 0;
            position: relative;
        }

        .user:hover {
            background: #eeeeee;
        }

        .user:last-child {
            margin-bottom: 0;
        }

        .pending {
            position: absolute;
            left: 13px;
            top: 9px;
            background: #b600ff;
            margin: 0;
            border-radius: 50%;
            width: 18px;
            height: 18px;
            line-height: 18px;
            padding-left: 5px;
            color: #ffffff;
            font-size: 12px;
        }

        .media-left {
            margin: 0 10px;
        }

        .media-left img {
            width: 64px;
            border-radius: 64px;
        }

        .media-body p {
            margin: 6px 0;
        }

        .message-wrapper {
            padding: 10px;
            height: 536px;
            background: #ffffff;
        }
        .message-wrapper {
        @if(isset($ticket)) 
        @if($ticket->status=='close') 
         height: 620px !important;
        @endif 
        @endif
        }

        .messages .message {
            margin-bottom: 15px;
        }

        .messages .message:last-child {
            margin-bottom: 0;
        }

        .received,
        .sent {
            width: 45%;
            padding: 3px 10px;
            border-radius: 10px;
        }

        .received {
            background: #d8e0ea;
        }

        .sent {
            background: #3bebff;
            float: right;
            text-align: right;
        }

        .message p {
            margin: 5px 0;
        }

        .date {
            color: #777777;
            font-size: 12px;
        }

        .active {
            background: #eeeeee;
        }

        textarea#message_input {
            border: 1px solid #cccccc;
            background-color: #f6f6f6 !important;
        }

        input[type=text] {
            width: 100%;
            padding: 12px 20px;
            margin: 15px 0 0 0;
            display: inline-block;
            border-radius: 4px;
            box-sizing: border-box;
            outline: none;
            border: 1px solid #cccccc;
        }

        input[type=text]:focus {
            border: 1px solid #aaaaaa;
        }

        svg.feather.feather-paperclip.w-full.h-full {
            width: 50% !important;
        }

        textarea#message_input {
            height: 80px;
        }

    </style>

    <div class="container-fluid">
        <section class="forms-section" style='min-height:200px'>
            <div class="outer-w3-agile mt-3">
                <a href='{{ url('/all-tickets') }}' class='btn btn-primary' style='color:white'>All
                    Inquiry</a>
                <h4 class="tittle-w3-agileits mb-2">Inquiry</h4>
            </div>




            <div class="row mt-4">
                <div class="col-md-4">
                    <div class="user-wrapper">
                        <ul class="users">

                            <li class="user" id="{{ $ticket->id }}">

                                @if ($ticket->status == 'active')
                                    <!-- <span class="pending"><span></span></span> -->
                                @endif

                                <div class="media">
                                    <div class="media-left">
                                        <img src="{{ url('public/images') }}/user.png" alt="" class="media-object">
                                    </div>

                                    <div class="media-body">
                                        @if(isset($ticket))
                                        @if($ticket->user)
                                        <p class="name">{{$ticket->user->name}}</p>
                                        @endif
                                        @endif
                                        <ul>
                                            @if (isset($ticket))
                                                @if (isset($ticket->ticket_categories->name))
                                                    <b>Categories:</b>
                                                    {{ $ticket->ticket_categories->name }}


                                                @endif
                                                @if (isset($ticket->ticket_sub_categories->name))
                                                    <br /><b>Sub Categories:
                                                    </b>{{ $ticket->ticket_sub_categories->name }}
                                                @endif

                                            @endif
                                        </ul>
                                    </div>
                                </div>
                            </li>

                        </ul>
                    </div>
                </div>

                <div class="col-md-8" id="messages">
                    <div class="message-wrapper">
                        <ul class="messages" id="media_list">


                        </ul>
                        <span id="img_show" class="float-right"></span>
                    </div>
                    <div id="inputmsg" class="" @if(isset($ticket)) @if($ticket->status=='close') style="display: none !important;" @endif @endif>
                        <form action="javascript:void(0)" method="post" enctype="multipart/form-data" id="sendinfo">
                            <div class="row">
                                <!-- <div class="col-10 form-group">
                                    <textarea name="message" id="message_input" rows="8" class="form-control" required
                                        aria-required="true" data-bv-notempty-message="Message is required"></textarea>
                                    <input type="hidden" value="{{ base64_encode($ticket->id) }}" id="ticket_id" />
                                </div>


                                <div class="col-1 form-group pt-4" id="chat_media_body">


                                    <div class=" relative text-gray-600  chat-inputbar" id="chat_media_body">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                            fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round"
                                            stroke-linejoin="round" class="feather feather-paperclip w-full h-full">
                                            <path
                                                d="M21.44 11.05l-9.19 9.19a6 6 0 0 1-8.49-8.49l9.19-9.19a4 4 0 0 1 5.66 5.66l-9.2 9.19a2 2 0 0 1-2.83-2.83l8.49-8.48">
                                            </path>
                                        </svg>
                                        <input type="file" class="w-full h-full top-0 left-0 absolute opacity-0 chat-input"
                                            id="chat_file" name="media">
                                    </div>



                                </div>
                                <div class="col-1 form-group pt-4"><button type="submit"
                                        class=" btn btn-primary  sms_send_button float-right"
                                        id="send_text_button">send</button></div> -->
                            </div>
                        </form>
                    </div>
                    <span id="msg_error" class="text-danger"></span>
                    <span id="img_error" class="text-danger"></span>

                </div>

            </div>
        </section>
    </div>

@endsection
@push('css')
    <link rel="stylesheet"
        href="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css" />
    <style type="text/css">
        div#example_filter input[type="search"] {
            box-sizing: border-box;
            border: 1px solid #d2d6dc;
        }
    </style>
@endpush
@push('js')
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <script type="text/javascript"
        src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js"></script>

    <script>
         $(document).on('keyup', '#message_input', function(e) {
            //console.log('keyup');
            if (e.keyCode == 13) {
                $("#send_text_button").trigger("click");
            }
         });
          setInterval(function(){ 
            var id ='{{ base64_encode($ticket->id) }}';
            var count ='{{$ticket_message_count}}';
            $.get('{{url('tickets/getcount')}}/'+id,function(data,status){
                    console.log(JSON.parse(data).msg_count);
                    if(count!=JSON.parse(data).msg_count){
                        getMessages(id);
                    }
                });
        }, 5000);
        function getMessages($id = null) {
            console.log($id);
            $.get("{{ url('/tickets/getlist') }}/" + $id, function(data, status) {
                // console.log(JSON.parse(data).ticket);
                var ticket = JSON.parse(data).ticket;
                var $html = '';

                if (ticket) {
                    var file = '';
                    if (ticket.file) {
                        if (ticket.file_type == "jpg" || ticket.file_type == "jpeg" || ticket.file_type == "png") {
                            file += '<a href="{{ url('public/ticket/') }}/' + ticket.file +
                                '" target="_blank"><img src="{{ url('public/ticket/') }}/' + ticket.file +
                                '" width="70px" height="50px"></a>';
                        } else {
                            file += '<a href="{{ url('public/ticket/') }}/' + ticket.file +
                                '" target="_blank">File</a>';
                        }
                    }

                    $html += '<li class="message clearfix">' +
                        '<div class="received">' +
                        '<p>' + ticket.message +
                        '<br/>' + file + '<p class="date">' + moment(ticket.created_at).format('DD-MM-YYYY,h:mm a') + '</p>' +
                        '</div></li>';
                }

                $.each(JSON.parse(data).message, function(key, value) {

                    var myfile = '';
                    if (value.file) {
                        if (value.file_type == "jpg" || value.file_type == "jpeg" || value.file_type ==
                            "png") {
                            myfile += '<a href="{{ url('public/ticket/') }}/' + value.file +
                                '" target="_blank"><img src="{{ url('public/ticket/') }}/' + value.file +
                                '" width="100px" height="100px"></a>';
                        } else {
                            myfile += '<a href="{{ url('public/ticket/') }}/' + value.file +
                                '" target="_blank">File</a>';
                        }
                    }

                    if (value.type == 'send') {
                        $html += '<li class="message clearfix">' +
                            '<div class="received">' +
                            '<p>' + value.message + '<br/>' + myfile + '</p>' +
                            '<p class="date">' + moment(value.created_at).format('DD-MM-YYYY,h:mm a') + '</p>' +
                            '</div></li>';
                    } else {

                        $html += '<li class="message clearfix">' +
                            '<div class="sent">' +
                            '<p>' + value.message + '<br/>' + myfile + '</p>' +
                            '<p class="date">' + moment(value.created_at).format('DD-MM-YYYY,h:mm a') + '</p>' +
                            '</div></li>';

                    }
                });
                $('#media_list').html($html);
                var $t = $('.message-wrapper');
                $t.animate({
                    "scrollTop": $('.message-wrapper')[0].scrollHeight
                }, "slow");
            });
            // $('.message-body').animate({scrollTop: 100}, 500);
        }




        var receiver_id = '';
        var file = null;
        $(document).ready(function() {

            @if (isset($ticket))
                var myid= '{{ base64_encode($ticket->id) }}';
                getMessages(myid)
            @else
                var myid= NULL;
            @endif

            $("#chat_file").on("change", function(e) {

                swal("File is Selected !", {
                    icon: "success",
                });

                file = $(this)[0].files[0];
                // console.log(file);
                if (this.files[0].size > 2000000) {
                    $("#img_error").html("Please upload file less than 2MB.!");
                    //alert("Please upload file less than 2MB. Thanks!!");
                    $('#send_sms_form').prop('disabled', true);
                    $(this).val('');
                } else {
                    $("#img_error").html('');
                    //console.log(file);
                    $("#img_show").show();
                    $("#img_show").html(file.name);

                }

            });

            $('#sendinfo').bootstrapValidator().on('success.form.bv', function(e) {


                //alert('test');
                // Prevent submit form
                $("#send_text_button").attr("disabled", true);
                e.preventDefault();
                //var $number = $('#send_sms_number').val();
                var $ticket_id = $('#ticket_id').val();
                var $message = $('#message_input').val();
                var $media = $('#chat_file').val();
                // console.log($media);
                //var token = $('#token').val();

                var from = new FormData();

                from.append('ticket_id', $ticket_id);
                from.append('message', $message);
                if (file) {
                    from.append('media', file, file.name);
                }
                from.append('_token', '{{ csrf_token() }}');
                //console.log(from);
                if ($message == '') {
                    //$("#msg_error").html("The Message is required");
                    $('#send_text_button').removeAttr("disabled");
                    return false;
                }

                $.ajax({
                    url: '{{ route('all-tickets.sendmsg') }}',
                    dataType: 'json',
                    data: from,
                    method: 'POST',
                    cache: false,
                    contentType: false,
                    processData: false,
                    timeout: 60000,
                    success: function(data) {
                        $("#img_show").html('');
                        file = null;
                        $('#message_input').val('');
                        $('#chat_file').val('');
                        //$('#charNum').text('160');
                        getMessages($ticket_id);
                        $('#send_sms_form').bootstrapValidator("resetForm", true);
                        $('#send_text_button').removeAttr("disabled");
                    },
                    error: function() {
                        $('#send_text_button').removeAttr("disabled");
                    }
                });
            });

        });

    </script>



@endpush
