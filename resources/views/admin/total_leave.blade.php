@extends('admin.master')
@section('bodyData')

<section class="forms-section" style='min-height:500px'>
<div class="outer-w3-agile mt-3">
@if ($message = Session::get('CurrentYearLeave'))
<div class="alert alert-success alert-block">
	<button type="button" class="close" data-dismiss="alert">×</button>	
        <strong>{{ $message }}</strong>
</div>
@endif
<a href='inserttotalLeave'><button class='btn btn-primary'>Add Total Leave For New Year</button></a>
@if ($message = Session::get('ToralLeaveError'))
<div class="alert alert-danger alert-block">
	<button type="button" class="close" data-dismiss="alert">×</button>	
        <strong>{{ $message }}</strong>
</div>
@endif
@if ($message = Session::get('ToralLeaveYear'))
<div class="alert alert-success alert-block">
	<button type="button" class="close" data-dismiss="alert">×</button>	
        <strong>{{ $message }}</strong>
</div>
@endif

        <h4 class="tittle-w3-agileits mb-4">Total Leave Update-<?php echo $data[0]->year ?></h4>
        
    <form action="{{url('updatetotalLeaves')}}" method="post">
        @csrf
      <div class="form-row">
               
                <div class="form-group col-md-6">
                    <label for="inputEmail4">Casual Leave</label>
                    <input type="text" class="form-control" id="inputEmail4" placeholder="Enter Casual Leave" required="" name='casual' value=<?php echo $data[0]->casual?>>
                </div>
                <input type='hidden' name='id' value='<?php echo $data[0]->id?>'>
                <div class="form-group col-md-6">
                   <label for="inputEmail4">Planned Leave</label>
                   <input type="text" class="form-control" id="inputEmail4" placeholder="Enter Planned Leave" required="" name='planned' value=<?php echo $data[0]->planned?>>
               </div>
               <!-- <div class="form-group col-md-6">
                   <label for="inputEmail4"> Year</label>
                   <input type="number" placeholder="YYYY" min="2020" max="2030" value='<?php  //echo$data[0]->year?>' name='year' disabled>

               </div> -->
               <script>
  document.querySelector("input[type=number]")
  .oninput = e => console.log(new Date(e.target.valueAsNumber, 0, 1))
</script>
           </div>

           <button type="submit" class="btn btn-primary" onclick='return confirm("Are you Sure You Want to Update Data");'>Update</button>


        
          
           </div>

          </div>
              
               
          </div>

        </form>
    </div>

</section>

@endsection
