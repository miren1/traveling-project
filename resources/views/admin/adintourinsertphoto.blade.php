@extends('admin.master')
@section('bodyData')
<?php
$segment = Request::segments();
$lastid =  $segment[1];
?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
    integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
</script>
<style>
#image_preview img {
    height:20%;
    width:20%; 
    margin-top: 12px;
    /* float:left; */
}
</style>
<script>
function demo(r) {
    var total_file = r.files.length;
    console.log(total_file);
    $('#image_preview').html('');
    for (var i = 0; i < total_file; i++) {
        $('#image_preview').append("<div class='row'><img src='" + URL.createObjectURL(r.files[i]) + "'></div>");
        var d = document.createElement('input');
        d.name = 'images[]';
        image_preview.appendChild(d);
        d.style.height = '30px';
        d.style.width = '200px';
        d.required = true
        d.placeholder = 'Enter Title';
    }
};
</script>

<a href='{{url("/tourdetail")}}/{{base64_encode($tour->id)}}' class='btn btn-primary'> Back To Tour Information</a>
<div class='row' id='demo'>
    <div class='col-sm-4'>
    </div>
    <div class='col-sm-6'>
        <form enctype="multipart/form-data" method='post' id="inserttouradmin" action='{{url("/photogallertinsert")}}'>
            @csrf 
            <div class='col-sm-12 form-group'>     
                <input id="browse" type="file" multiple name='upload_file[]' required accept="image/png, image/jpg, image/jpeg" onchange="demo(this)" data-bv-notempty-message="The Image is required" class="form-control" id='upload_file'>
               
                <input type='hidden' name='id' id="id" value="{{$tour->id}}">
                <input type='hidden' name='slug' name="slug" value="{{$tour->slug}}">
            </div>
            
            <div class='col-sm-12 form-group text-right' >
                <button type="submit" class='btn btn-success mt-4'>Upload</button>
                <br>
            </div>
        </form>
            <div class='col-md-12' >
                <div class="col-md-12" id="image_preview"></div>
            </div>
    </div>
</div>
<hr>

@endsection

@push('css')
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css"/>
@endpush
@push('js')
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js"></script>
<script>

  $(document).ready(function () {
    $('#inserttouradmin').bootstrapValidator({
        fields: {
            upload_file[]: {
                validators: {
                    file: {
                        // minFiles: 1,
                        extension: 'jpeg,jpg,png',
                        type: 'image/jpeg,image/jpg,image/png',
                        maxSize: 5120 * 5120 * 5120 * 5120,
                        message: 'The selected file is not valid, it should be (jpeg,jpg,png) and 4 MB at maximum.',
                    }
                }
            }
        }
    });
  });       
</script>


@endpush
