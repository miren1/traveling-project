@extends('admin.master')
@section('bodyData')

@if ($message = Session::get('deletereview'))
<div class="alert alert-success alert-block">
	<button type="button" class="close" data-dismiss="alert">×</button>	
        <strong>{{ $message }}</strong>
</div>
@endif
@if ($message = Session::get('error'))
<div class="alert alert-danger alert-block">
	<button type="button" class="close" data-dismiss="alert">×</button>	
        <strong>{{ $message }}</strong>
</div>
@endif

@if ($message = Session::get('Creatreview'))
<div class="alert alert-success alert-block">
	<button type="button" class="close" data-dismiss="alert">×</button>	
        <strong>{{ $message }}</strong>
</div>
@endif




<div class="outer-w3-agile mt-3">
<div class='row'>

<div class='col-sm-3'>
<a href="/adminReviewForm" style='text-align:left'><button class="btn btn-primary">Add Review</button></a>
</div>

                    <div class='col-sm-6'></div>
                    </div><br>
                    <!-- <h4 class="tittle-w3-agileits mb-4">
                    Table Head Options</h4> -->
                    <table class="table">
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col">Name</th>
                                <th scope="col">Content</th>
                                <th scope="col">Image </th>
                                <th scope="col">Actions</th>
                               </tr>
                        </thead>
                        <tbody id='tableid'>
                            @foreach($reviews as $value)
                            <tr>
                                
                                <td>{{ $value->name }}</td>
                                <td>{{ $value->content }}</td>
                                <td><img src='<?php echo asset("reviews")."/".$value->image?>' style='width:40px;height:40px'/></td>
                              <td>
                   <a href="/updatereview/{{$value->id}}">
                         <!-- <button class="btn btn-info">Update</button> -->
                         <i class="fa fa-edit" aria-hidden="true" title='Edit' style='font-size:20px'></i>
                         </a>

                        <a href="/reviewdelete/{{$value->id}}">
                        <i class="fa fa-trash" aria-hidden="true" onclick='return confirm("Are you Sure Delete this Inquiry?")' style='color:red;font-size:20px' title='Delete'></i>
                    </a>
                    </td>
                                
                                
                                
                              
                            </tr>
                           @endforeach
                        </tbody>
                    </table>
                    <br> <br><br>   <br> <br><br>



          
                </div>
@endsection 
