@extends('admin.master')
@section('bodyData')
<meta name="csrf-token" content="{{ csrf_token() }}">

<script>
function searchsalary(name)
{   
    
      var query= document.getElementById('sname').value;
      // var sdate =  document.getElementById('sdate').value;
      // var status = document.getElementById('status').value;
	console.log(query);
     $.ajax({
   url:"{{ route('salarylive') }}",
   method:'GET',
   data:{query:query},
   dataType:'json',
   success:function(data)
   {
      $('#tableid').empty();
    // $('tbody').html(data.table_data);
    $('#tableid').append(data.table_data);
   }
  })
}
</script>
<div class="outer-w3-agile mt-3" style='min-height:500px'>
 
@if ($message = Session::get('UpdateInquiey'))
<div class="alert alert-success alert-block">
	<button type="button" class="close" data-dismiss="alert">×</button>	
        <strong>{{ $message }}</strong>
</div>
@endif
@if ($message = Session::get('salarycreate'))
<div class="alert alert-success alert-block">
	<button type="button" class="close" data-dismiss="alert">×</button>	
        <strong>{{ $message }}</strong>
</div>
@endif
@if ($message = Session::get('DeleteInquiey'))
<div class="alert alert-success alert-block">
	<button type="button" class="close" data-dismiss="alert">×</button>	
        <strong>{{ $message }}</strong>
</div>
@endif
    <div class='row'>
    &nbsp;&nbsp;
      <a href="/showsalary"><button class="btn btn-primary">Show Salary</button></a>  &nbsp;&nbsp;
    <a href="/insertsalary"><button class="btn btn-primary">Add Salary</button></a>
 
      <div class='col-sm-8'>
            <div class="form-inline mx-auto search-form">
                <div class="col-sm-8">
                <input class="form-control mr-sm-2" type="search" placeholder="Search With Emp Id" aria-label="Search" name='sname' onkeypress='searchfun(this)' id='sname'>
                </div>
<!--                  <div class="col-sm-3">
        <input type="date" name='sdate' id='sdate' onchange='searchfun(this)'>
                  </div> -->
<!--                  <div class="col-sm-2">
                    <select onchange='searchfun(this)' id='status'>
                        <option value='done'>Done</option>
                        <option value='new' selected>New Inquiry</option>
                        <option value='follow'>Follow Up Inquiry</option>
<!--                         <option value='delete'>Delete Inquiry</option> 
                  //  </select>
                  </div> -->
                <!-- <button class="btn btn-style my-2 my-sm-0" type="submit">Search</button> -->
            </div>
        </div>
        <div class='col-sm-2'></div>
</div>
<br>

    <table class="table">
        <thead class="thead-dark"><tr>
           
            <th scope="col">Emp Id</th>
            <th scope="col">Emp Name</th>
            <th scope="col">Salary</th>
           <th scope="col">Follow Date</th>
           
             <th scope="col">Action</th>
            </tr>
    </thead>
    <tbody id='tableid'>

            <tr>
            <?php  foreach ($data as $value) {
          # code...
        ?>		<td scope="row"><?php echo "JJ-EMP-".$value->uid?></td>
                <td scope="row"><?php echo ucfirst($value->name)?></td>
               <td><?php echo $value->salary?></td>
                <td><?php echo $value->follow_date?></td>
            <td>
                     

                         <a href="/editsalary/{{$value->id}}">
                         <!-- <button class="btn btn-info">Update</button> -->
                         <i class="fa fa-edit" aria-hidden="true" title='Edit' style='font-size:20px'></i>
                         </a>
                        

                </td>
              
                 
                  
                    
              
            </tr>  
            <?php } ?>
             
            

           
        </tbody>
    </table>
    <center>
   <div class="container" style='margin-left:30%'>
   <?php //echo $data->render(); ?>

</div>

</center>
  
    


</div>
@endsection
