@extends('admin.master')
@section('bodyData')
<style>
.Portfolio {
    position: relative;
    margin: 5px;
    border: 2px solid black;
    float: left;
    width: 180px;
    transition-duration: 0.4s;
    border-radius: 5px;
    animation: winanim 0.5s ;
-webkit-backface-visibility:visible;
    backface-visibility:visible;
    box-shadow:0 3px 5px -1px rgba(0,0,0,.2),0 5px 8px 0 rgba(0,0,0,.14),0 1px 14px 0 rgba(0,0,0,.12)
}

.Portfolio:hover {
    box-shadow: 0 12px 16px 0 rgba(0,0,0,.24),0 17px 50px 0 rgba(0,0,0,.19);
}

.Portfolio img {
    width: 100%;
    height: auto;
    border-radius: 5px
}

.desc {
    padding: 5px;
    text-align: center;
    font-size: 90%;
    background:black;
    color:hotpink
}

.nav {
    padding:20px;
    margin-left:340px;
    margin-top:-30px;
}

.nav li a { 
    margin:5px;
    padding: 15px 50px; 
    font-size:16px; 
    color:hotpink; 
    background: #000;
    transition-duration: 0.4s;
}
.nav a:hover { 
    background:#333; 
}
.nav .active { 
    background-color:hotpink !important;
    color:#fff;
}

@keyframes winanim {
    0%{opacity:0;transform:scale3d(.3,.3,.3)}
    50%{opacity:1}
    
}
</style>

<!------ Include the above in your HEAD tag ---------->
<a href='{{url('/tourdetail')}}/{{ base64_encode($result[0]->tour_fk_id)}}'><button class='btn btn-primary'>Back To Tour Details</button></a>
<a href='{{url('/amintourinsertphoto')}}/{{base64_encode($result[0]->tour_fk_id)}}'><button class='btn btn-primary'>Add  Photos</button></a>
<a href='{{url('/amintourinsertvideo')}}/{{base64_encode($result[0]->tour_fk_id)}}'><button class='btn btn-primary'>Add Videos </button></a>


<div class="container-fluid" style="margin-top:20px;">
<div class="row">
</div><hr noshade style="margin-top:-20px;">
<div class="container">
<div class="tab-content" id="pills-tabContent">
  <div class="tab-pane fade show active" id="showall" role="tabpanel" aria-labelledby="showall-tab">
  <?php  foreach ($result as $value) {
      # code...
  ?>

    <div class="Portfolio"><a href="#">
    <img class="card-img" src="<?php echo asset('').'/'.$value->imagepath?>" alt=""></a>
    <div class="desc" style='color:white'><?php echo  $value->caption?></div>
    <center><a style='color:red' href='{{url('/deleteimage')}}/{{$value->id}}'>Delete</a></center>
    </div>
  <?php }?>
    
  </div>
  
  
  
</div>
</div>

</div>
</div>

</div>


@endsection
