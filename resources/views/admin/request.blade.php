@extends('admin.master')
@push('css')
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
@endpush
@section('bodyData')

<div class="outer-w3-agile mt-3" style='min-height:400px'>
    <div class='row'>
        <div class='col-sm-2'>
           <!--  <center><button class="btn btn-dark" onclick="window.location='{{ url(" admininsertemployee") }}'">Employee Request</button></center> -->
        </div>

        <div class='col-sm-8'>
            <!-- <form action="#" method="post" class="form-inline mx-auto search-form">
                <div class="col-sm-4">
                <input class="form-control mr-sm-2" type="search" placeholder="Search With Employee name" aria-label="Search" required="">
                </div>
                 <div class="col-sm-4">
                <input type="date">
                  </div>
                <button class="btn btn-style my-2 my-sm-0" type="submit">Search</button>
            </form> -->
        </div>
        <div class='col-sm-2'></div>


    </div>

    <h4 class="tittle-w3-agileits mb-4">All Notification</h4>
    
    <div class="table-responsive">
        <div class="row col-md-12">
            <div class="col-md-4 mb-2">
                <div class="single-widget-search-input">
                    <div id="reportrange"
                        style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                        <i class="fa fa-calendar"></i>&nbsp;
                        <span></span> <i class="fa fa-caret-down"></i>
                    </div>
                </div>
            </div>
            <div class="col-md-3 mb-2">
                <select name="employee" id="employee" class="form-control">
                    <option value=""> -- Select Employee -- </option>
                    @foreach ($employees as $employee)
                        <option value="{{$employee->id}}">{{$employee->name}} {{$employee->surname}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-2 mb-2">
                <select name="status" id="status" class="form-control">
                    <option value=""> -- Select Status -- </option>
                    <option value="Pending">Pending</option>
                    <option value="Approve">Approve</option>
                    <option value="Reject">Reject</option>
                </select>
            </div>
            <div class="col-md-3 mb-2">
                <select name="type" id="type" class="form-control">
                    <option value=""> -- Select Type -- </option>
                    <option value="WFH">Work from home</option>
                    <option value="PL">Planed Leave</option>
                    <option value="CL">Casual Leave</option>
                </select>
            </div>
        </div>
        <table class="table table-hover" style="width: 100%;" id="dataTable">
          <thead class="thead-dark">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Apply Date</th>
                <th scope="col">Leave Date</th>
                <th scope="col">Reason</th>
                <th scope="col">Status</th>
                <th scope="col">Action</th>
            </tr>
          </thead>
        </table>
    </div>

</div>
@endsection
@push('js')
<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
{{-- <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script> --}}
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script>
    var employee = '';
    var start_date = '';
    var end_date = '';
    var status = '';
    var type = '';
// console.log(start,end);
    $(function() {

        var start = moment().subtract(29, 'days');
        var end = moment();

        function cb(start, end)
        {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            start_date = start.format('YYYY-MM-DD');
            end_date = end.format('YYYY-MM-DD');
            LeaveData(employee,start_date,end_date,status,type);
        }

        $('#reportrange').daterangepicker({
            startDate: start,
            endDate: end,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1,
                    'month').endOf('month')]
            }
        }, cb);
        cb(start,end);
    });

    $(function() {

        $(document).on('change','#employee',function(){
            employee = $(this).val();
            LeaveData(employee,start_date,end_date,status,type);
        });

        $(document).on('change','#status',function(){
            status = $(this).val();
            LeaveData(employee,start_date,end_date,status,type);
        });

        $(document).on('change','#type',function(){
            type = $(this).val();
            LeaveData(employee,start_date,end_date,status,type);
        });
    });

    function LeaveData(employee,start,end,status,type)
    {
        $("#dataTable").DataTable().destroy();
        $("#dataTable").DataTable({
            'processing': true,
            'serverSide': true,
            'serverMethod': 'post',
            "aLengthMenu": [[10,25, 50,100], [10,25, 50,100]],
            order: [
                [0, 'desc']
            ],
            "ajax": {
                "url": "{{ route('leave.show') }}",
                "data": {
                    employee: employee,
                    date: {start:start,end:end},
                    status: status,
                    type: type,
                    "_token" : "{{csrf_token()}}"
                }
            },
            "columns": [
                {
                    data: "id",
                    render: function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1
                    }
                },
                {
                    data: "employee.name",orderable:false,
                    render: function(data, row, alldata) {
                        if(alldata.employee && alldata.employee.name)
                        {
                            return alldata.employee.name;
                        }
                        else
                        {
                            return '-';
                        }
                    }
                },
                {
                    data: "leave_date_start",orderable:false,
                    render: function(data, type, row, meta) {

                        return moment(data).format('DD-MM-YYYY,h:mm a');
                        // body...
                    }
                },
                {
                    data: "leave_date_end",orderable:false,
                    render: function(data, type, row, meta) {

                        return moment(data).format('DD-MM-YYYY,h:mm a');
                        // body...
                    }
                },
                {
                    data: "reason",orderable:false
                },
                {
                    data: "approve_status",orderable:false,
                    render: function(data, row, alldata) {
                        if(alldata.approve_status=="Approve")
                        {
                            return '<span class="badge badge-success p-2">Approved</span>'
                        }
                        else if(alldata.approve_status=="Reject")
                        {
                            return '<span class="badge badge-danger p-2">Rejected</span>';
                        }
                        else
                        {
                            return '<span class="badge badge-warning p-2">Pending</span>';
                        }
                    }
                },
                {
                    data: "id",orderable:false,
                    render: function(data, row, alldata) {
                    var $id = btoa(alldata.id);
                    $html = '';
                        $html += `<a href="{{url('/DetailLeaveRequest')}}/`+alldata.id+`">
                            <i class="fa fa-info-circle" aria-hidden="true" title="Detail View" style="font-size:20px"></i>
                            </a> `  

                            if(alldata.approve_status=="Pending")
                            {
                                $html +=`<a href="{{ url('/AdeminLeaveResult') }}/`+alldata.id+`/'Approve'">
                                <i class="fa fa-check" aria-hidden="true" style="font-size:20px;color:green"></i>
                                </a>
                                <a href="{{ url('/AdeminLeaveResult') }}/`+alldata.id+`/'Reject'">
                                <i class="fa fa-times" aria-hidden="true" style="font-size:20px;color:red"></i>
                                </a>`;
                            }
                        return $html;
                    }
                },
            ]
        });
    }
</script>
@endpush