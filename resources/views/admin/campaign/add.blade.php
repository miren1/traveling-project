@extends('admin.master')
@section('bodyData')
<meta name="csrf-token" content="{{ csrf_token() }}">
<section class="forms-section" style='min-height:500px'>
    <div class="outer-w3-agile mt-3">
        <h4 class="tittle-w3-agileits mb-4">Create Campaign</h4>
        <a href='{{url('/campaign')}}' class='btn btn-primary'>Back</a>
        <div class="mt-4">
          
            <form data-toggle="validator" id="Agentform" role="form" action="{{url('campaign/store')}}" method="post" enctype="multipart/form-data">
            @csrf
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" placeholder="Enter Name" required name='name' id="name" data-bv-notempty-message="The First Name is required"  pattern='[A-Za-z]{2,30}' style="width:610px">
                        </div>
                        <div class="form-group" style="width:610px">
                           <label for="message">Message</label>
                           <textarea  name='message' placeholder="Enter Message" rows="5"  id='message' name="message" class='form-control' required data-bv-notempty-message="The Message is required"></textarea>
                        </div>
                    </div>
                    <div class="form-group col-md-1"></div>

                    <div class="form-group col-md-4" style="margin-left: 200px;">
                        <a href="{{ url('/sample_email.csv') }}" class='btn btn-primary mb-2' download="" style="float: right;">sample file</a>
                       <label for="csv_file">CSV file</label> 
                       <input type="file" id="csv_file" name="csv_file" required data-bv-notempty-message="The csv file is required" data-allowed-file-extensions='["csv", "txt"]' />
                   </div>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
</section>

@endsection


@push('css')
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.min.css" integrity="sha512-EZSUkJWTjzDlspOoPSpUFR0o0Xy7jdzW//6qhUkoZ9c4StFkVsp9fbbd0O06p9ELS3H486m4wmrCELjza4JEog==" crossorigin="anonymous" />
@endpush
@push('js')
<script src="//cdn.ckeditor.com/4.16.2/standard/ckeditor.js"></script>
<script src="{{ url('js/ckfinder/ckfinder.js') }}"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/js/dropify.min.js" integrity="sha512-8QFTrG0oeOiyWo/VM9Y8kgxdlCryqhIxVeRpWSezdRRAvarxVtwLnGroJgnVW9/XBRduxO/z1GblzPrMQoeuew==" crossorigin="anonymous"></script>
<script>

   $( document ).ready(function() 
    {
        $('#Agentform').bootstrapValidator();
        var editor =  CKEDITOR.replace( 'message' );
       
        CKFinder.setupCKEditor( editor );
    });

    $(window).bind("pageshow", function() {
        var form = $('form'); 
        // let the browser natively reset defaults
        form[0].reset();
    });


    $("#CareerForm").submit( function(e) {
        var messageLength = CKEDITOR.instances['editorcontent'].getData().replace(/<[^>]*>/gi, '').length;
        if( !messageLength ) {
            alert( 'Please enter a content' );
            e.preventDefault();
        }
    });

  $(document).ready(function () {
    $('#Agentform').bootstrapValidator();
    $('#csv_file').dropify();
    
    @if(isset($agent))
    var state_fk_id ='{{$agent->state_fk_id}}';
    var city='{{$agent->city_fk_id}}';
    statechange(state_fk_id,city);
    @endif
    //console.log(state_fk_id);
    
   
    
  });


  $('#c_state_id').change(function() {
    var value= $(this).val();  
    var city='';
    statechange(value,city);
  });



    function statechange(value,city)
                { 
                    $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
              });
           
               var cat_id = value;
            

                 $.ajax({
                   
                       url:"{{ route('getCity') }}?id=" + cat_id ,
                       type:"POST",
                       data: {
                           cat_id: cat_id
                        },
                      
                       success:function (data,city) {
                        $('#c_city_id').empty();
                        // console.log(data.cities);

                        $.each(data.cities,function(index,cities){
                            if(city==''){
                                $('#c_city_id').append('<option value="'+index+'">'+cities+'</option>');
                            }else{
                                var selected='';
                                if(city==index){
                                 selected='selected'; 
                                }
                                $('#c_city_id').append('<option value="'+index+'" '+selected+'>'+cities+'</option>');
                            }
                            
                        
                        })
                       }
                   })
                }
               
</script>


@endpush
