@extends('admin.master')
@section('bodyData')

<div class="outer-w3-agile mt-3" style='min-height:400px'>
    <div class='row'>
        <div class='col-sm-2'>
            <a href="{{url('campaign')}}" style='text-align:left'><button class="btn btn-primary">Campaign</button></a>
        </div>
    </div>
   
    <br>              
    <div class="table-responsive">
        <table id="example" class="table" style="width: 100%">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">Name</th>
                    <th scope="col">Number</th>
                    <th scope="col">Email</th>
                    <th scope="col">Status</th>
                    <th scope="col">Created at</th>
                </tr>
            </thead> 
        </table>
    </div>
</div>
@endsection 
@push('js')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script type="text/javascript">
 
      $(document).on("click", '.agentdelete', function(e) {
            e.preventDefault();
            var id = $(this).attr('data-id');
            swal({
                    title: "Are you sure?",
                    text: "You want to delete this Email.",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {

                        $.ajax({
                            type: "POST",
                            url: "{{route('campaign-email-delete')}}",
                            data: {
                                "id": id,
                                "_token": "{{ csrf_token() }}"
                            },
                            success: function(response) {
                                if (response.status == "success") {
                                    swal(response.message, {
                                        icon: 'success',
                                    }).then((result) => {
                                         $('#example').DataTable().ajax.reload(null,false);
                                    });
                                } else {
                                    swal("Error","Something went wrong!",{icon:'error'});
                                }
                            }
                        });
                    }
                });
        });


    $(document).ready(function() {
  
    $table = $("#example").DataTable({
      "ajax": "{{ url('campaign/email_list') }}/{{base64_encode($campaign->id)}}",
      "processing": true,
      "serverSide": true,
      "ordering": false,
      "columns": [
            { "data": "name"},
            { "data": "number"},
            { "data": "to"},
            { "data": "status",render:function(data,type,row,meta) {
                    var status='';
                    //console.log(data);
                    if(data=='1'){
                        status='sent';
                    }else{
                        status='fail';
                    }
                    return status;
            }},
            { "data": "created_at",render:function(data,type,row,meta) {

                return moment(data.created_at).format('DD-MM-YYYY,h:mm a');
                // body...
            }},
            // { "data": "id", 
            //         render:function(data,type,row,meta){
                      
            //          var html= '<a  href="{{--url("campaign/email_view/")--}}/'+btoa(data)+'"><i class="fa fa-info-circle" aria-hidden="true"'+ 
            //                     'title="Detail View" style="font-size:20px"></i></a>'+
            //                     '<a href="#" class="ml-1 agentdelete"  data-id="'+btoa(data)+'"><i class="fa fa-trash"'+
            //                      'aria-hidden="true" style="color:red;font-size:20px" title="Delete"></i>'
            //                     '</a>';

            //       return html;          
                      
            //   }

            //},
        ]
    });


    
  } );
</script>

@endpush
