@extends('admin.master')
@section('bodyData')

<div class="outer-w3-agile mt-3">
    <div class='row'>
        <div class='col-sm-3'>
            <a href="/adminEventForm" style='text-align:left'><button class="btn btn-primary">Add Event</button></a>
        </div>
    </div><br>
    <table class="table">
        <thead class="thead-dark">
            <tr>  
                <th scope="col">Name</th>
                <th scope="col">Start Date</th>
                <th scope="col">End Date </th>
                <th scope="col">Title</th>
                <th scope="col">Description</th>
                <th scope="col">Image</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody id='tableid'>
            @foreach($data as $value)
            <tr>
                <td>{{ $value->name }}</td>
                <td>{{ $value->sdate }}</td>
                <td>{{ $value->edate }}</td>
                <td>{{ $value->title }}</td>
                <td>{{ $value->description }}</td>
                <td><img src='{{asset("uploads/events/$value->image")}}' style='height:50px;width:50px'></td>
                <td>
                    <a href="/editevent/{{$value->id}}">
                        <i class="fa fa-edit" aria-hidden="true" title='Edit' style='font-size:20px'></i>
                    </a>
                    <a href="/eventdelete/{{$value->id}}">
                        <i class="fa fa-trash" aria-hidden="true" onclick='return confirm("Are you Sure Delete this Event?")' style='color:red;font-size:20px' title='Delete'></i>
                    </a>
                </td>
            </tr>
           @endforeach
        </tbody>
    </table>
</div>
@endsection 
