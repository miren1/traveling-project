@extends('admin.master')
@section('bodyData')
<section class="forms-section" style='min-height:500px'>
<div class="outer-w3-agile mt-3">
    <div class='row'>
      <div class='col-md-6'>
        <div class="btn btn-primary"><i class="fa fa-list mr-2"></i>Gallery list</div>
      </div>
      <div class='col-md-6 text-right'>
        <a class="btn btn-primary" href="{{ route('gallery-add')}}"><i class="fa fa-plus mr-2"></i>Add Gallery</a>
      </div>
    </div><br>
    <div class="table-responsive">
      <table class="table table-hover" style="width: 100%;" id="dataTable">
        <thead class="thead-dark">
          <tr>
            <th>#</th>
            <th>Category</th>
            <th>Title</th>
            <th>Slug</th>
            <th>Start Date</th>
            <th>End Date</th>
            <th>Images</th>
            <th>Action</th>
          </tr>
        </thead>
      </table>
    </div>
  </div>
</section>
@endsection
@push('css')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css">
@endpush
@push('js')
<script>
  $(function() {
    $("#dataTable").DataTable({
        'processing': true,
        'serverSide': true,
        'serverMethod': 'post', 
        "aLengthMenu": [[10,25, 50,100], [10,25, 50,100]],
        "ajax": {
            "url": "{{ route('gallery-list') }}",
            "data": {
                "_token": "{{ csrf_token() }}"
            },
        },
        "columns": [
            {
                data: "id",
                render: function(data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1
                }

            },
            {
                data: "all_image[0].category"
            },
            {
                data: "all_image[0].title"
            },
            {
                data: "all_image[0].slug"
            },
            {
                data: "all_image[0].startdate"
            },
            {
                data: "all_image[0].enddate"
            },
            {
                data: "image",
               
                  render: function(data, type, row, alldata) {
                    
                    var h = 'https://';
                    var img = h+document.domain+'/'+data;
                    return "<img style='width:40px;' src="+img+">";
                  

                }
            },
            
            {
                data: "id",orderable:false,
                render: function(data, row, alldata) {
                  console.log(alldata);
                  var $id = btoa(alldata.id);
                    $html = `<a href="{{url('tour-gallery/edit')}}/`+$id+`">
                         <!-- <button class="btn btn-info">Update</button> -->
                         <i class="fa fa-edit" aria-hidden="true" title='Edit' style='font-size:20px'></i>
                         </a>
                      
                      <i class="fa fa-trash delete" style='font-size:20px;color:red;cursor:pointer;' title='delete' data-sec-id="`+alldata.gallery_id+`" data-id="`+alldata.id+`" >
                      </i>`;
                    return $html;
                }
            },
        ],
        "columnDefs": [ {
          'targets': [0,1,2,3,4,5,6,7], /* table column index */
          'orderable': false, /* true or false */
       }]
    });

    $(document).on("click", '.delete', function(event) { 
    var id =$(this).attr('data-id');
    var gallery_id =$(this).attr('data-sec-id');
    //alert(id);
    swal({
      title: "Are you sure?",
      text: "You Want To Delete This Record ,You Will Not Be Able To Recover This Record!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {

        $.ajax({
          url: "{{ url('tour-gallery/delete') }}/"+id+"/"+gallery_id,
          type: 'get',
          dataType: 'json',
        success:function($data){
          if($data == true)
          {
            swal("Record has been deleted!", {
              icon: "success",
            }).then((result) => {
                $('#dataTable').DataTable().ajax.reload(null,false);
            });
            //location.reload('#dataTable');
          }else{
            swal("Record has been not deleted!", {
              icon: "error",
            });
          }
    
        }
      });


      } else {
        swal("Your Record is Safe!");
      }
    });
  });

    $(document).on('click','.leave',function(){
      var id = $(this).data('id');
      // alert('true');
      $('#modal-myvalue').val(id);
    });
  });
</script>
@endpush
