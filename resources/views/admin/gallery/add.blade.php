@extends('admin.master')
@section('bodyData')
<section class="forms-section" style='min-height:500px'>
<div class="outer-w3-agile mt-3">
        <h4 class="tittle-w3-agileits mb-4">@if(isset($photos))Edit @else Add @endif Gallery</h4>
       
        <form action="{{ route('gallery-store')}}" id="AdsForm" method="post" enctype="multipart/form-data">
        @csrf
        {{-- dd($photos->id) --}}
            <div class="row">
                @if(isset($photos))
                    <input type="hidden" id="id" name="id" value="{{$photos->all_image[0]->id}}"/>
                @endif
                @if(isset($photos))
                    <input type="hidden" id="image_id" name="image_id" value="{{$photos->id}}"/>
                @endif
                <div class="col-md-6">

                    <div class="form-group">
                        <label for="category">Category</label>
                        <input type="text" class="form-control" name="category" id="category" value="@if(isset($photos)){{$photos->all_image[0]->category}}@endif" placeholder="Enter Category" data-bv-stringlength="true"
                        data-bv-stringlength-min="3"
                        data-bv-stringlength-max="20"
                        data-bv-stringlength-message="Category minimum length is 3 and maxmum length is 20"
                        data-bv-remote-url = "{{ url('tour-gallery/checkcategory') }}/@if(isset($photos)){{$photos->all_image[0]->id}}@endif"
                        data-bv-remote = "true"
                        data-bv-remote-type = "GET"   
                        data-bv-remote-message="Opps ! Category Already Used." placeholder="Enter Slug" required="" data-bv-notempty-message="The Category is required">
                    </div>
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text" placeholder="Enter Title" class="form-control" name="title" id="title" required data-bv-notempty-message="The Title is required" value="@if(isset($photos)){{$photos->all_image[0]->title}}@endif">
                    </div>
                    <div class="form-group">
                        <label for="title">Slug</label>
                        <input type="text"  class="form-control" name="slug" id="slug"  value="@if(isset($photos)){{$photos->all_image[0]->slug}}@endif" data-bv-stringlength="true"
                        data-bv-stringlength-min="3"
                        data-bv-stringlength-max="20"
                        data-bv-stringlength-message="Slug minimum length is 3 and maxmum length is 20"
                        data-bv-regexp="true"
                        data-bv-regexp-regexp="^[a-z0-9^-]*$"
                        data-bv-regexp-message="The Slug can consist of lower case  alphabetical characters and no spaces "
                        data-bv-remote-url = "{{ url('tour-gallery/checkslug') }}/@if(isset($photos)){{$photos->all_image[0]->id}}@endif"
                        data-bv-remote = "true"
                        data-bv-remote-type = "GET"   
                        data-bv-remote-message="Opps ! Slug Already Used." placeholder="Enter Slug" required="" data-bv-notempty-message="The Slug is required">
                    </div>
                    <div class="form-group">
                        <label for="tag">Tag</label>
                        <input type="text" placeholder="Enter Tag" class="form-control" name="tag" id="tag" required data-bv-notempty-message="The Tag is required" value="@if(isset($photos)){{$photos->all_image[0]->tag}}@endif">
                    </div>
                    <div class="form-group">
                        <label for="type">Start Date</label>
                        <input type="text" class="form-control date-picker" required id="startdate" name="startdate" value="@if(isset($photos)){{$photos->all_image[0]->startdate}}@endif" placeholder="Start Date"  data-bv-notempty="true" data-bv-notempty-message="Start Date is required" onchange="StartDate()" readonly>
                    </div>
                    <div class="form-group">
                        <label for="type">End Date</label>
                        <input type="text" class="form-control" required id="enddate" name="enddate" value="@if(isset($photos)){{$photos->all_image[0]->enddate}}@endif" placeholder="End Date (You must have to select the first Start Date)"  data-bv-notempty="true" data-bv-notempty-message="End Date is required" readonly>
                    </div>
                    <label><b>Details for Search Engine Optimization</b></label>
                    {{-- <div class="form-group">
                        <label for="seo_title">Title</label>
                        <input type="text" class="form-control" name="seo_title" id="seo_title" required data-bv-notempty-message="The Title is required" value="@if(isset($photos)){{$photos->all_image[0]->seo_title}}@endif">
                    </div> --}}
                    <div class="form-group">
                        <label for="title">Keywords</label>
                        <input type="text" class="form-control" name="seo_keywords" id="seo_keywords" required data-bv-notempty-message="The Keywords is required" value="@if(isset($photos)){{$photos->all_image[0]->seo_keywords}}@endif">
                    </div>
                    <div class="form-group">
                        <label for="title">Description</label>
                        <textarea class="form-control" name="seo_description" id="description" required data-bv-notempty-message="The Description is required" rows="5">@if(isset($photos)){{$photos->all_image[0]->seo_description}}@endif</textarea>
                    </div>
                </div> 
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="category">Images</label>
                        <input data-browse-on-zone-click="true" onchange="SelectImage()" class="form-control custom-file-input"  id="input-id" type="file" name="image[]"  @if(isset($photos))  @else multiple="multiple" @endif placeholder="Enter Your Image">
                    </div>

                    <div class="form-group">
                        @if(isset($photos))
                            <img class="img-container" src="{{$photos->url}}/{{$photos->image}}">
                        @endif
                    </div>
                </div>
                
            </div>
            <button type="submit" id="submitbtn" class="btn btn-primary">@if(isset($ads))Update @else Submit @endif</button>
        </form>
    </div>
</section>

@endsection
@push('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.min.css" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.8/css/fileinput.min.css">
<link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel = "stylesheet">
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css"/>


<style type="text/css">
    .img-container{
        width: 15%;
        display: initial;
    }
    .img-container img{
        justify-content: center;
        margin-left: 5px;
        margin-right: 5px;
        border-radius: 20px;
        height: 100px;
    }
    .ui-datepicker-month{
        color: #f7af38 !important;
    }

    .ui-datepicker-year{
        color: #f7af38 !important;
    }
</style>
@endpush

@push('js')
<script src = "https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="https://cdn.jsdelivr.net/npm/piexifjs@1.0.6/piexif.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.8/js/fileinput.min.js"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js"></script>
<script>

    $( document ).ready(function() 
    {
        $('#AdsForm').bootstrapValidator();     
    });

    $(window).bind("pageshow", function() {
        var form = $('form'); 
        // let the browser natively reset defaults
        form[0].reset();
    });

    $('.date-picker').on('changeDate show', function(e) {
        $('#AdsForm').bootstrapValidator('revalidateField', 'startdate');
    });

    $(function() {
            $( "#startdate" ).datepicker({
            minDate: new Date(),
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true,
            yearRange: '-110:+29',
            language: "es",

        });

        $('#startdate').datepicker({
            }).on('change', function(e) {
            $('#AdsForm').bootstrapValidator('revalidateField', 'startdate');
        });
        
    });
    
    function StartDate()
    {
        var startDate = $('#startdate').val();
        var sd = new Date(startDate);
        $("#enddate").datepicker("destroy");
         $(function() {
            $( "#enddate" ).datepicker({
            minDate: new Date(startDate),
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true,
            yearRange: '-110:+29',
            language: "es",
        });

        $('#enddate').datepicker({
            }).on('change', function(e) {
            $('#AdsForm').bootstrapValidator('revalidateField', 'enddate');
        });
    });
    }


@if(isset($photos))

$("#input-id").fileinput({
      
      'showUpload':false, 
      'showCancel':false,
      'showClose':false,
      'showRemove':false,
      'allowedFileExtensions': ["jpg","png",'jpeg','mp4'],
       // minFileCount: 2,
       maxFileCount: 6,
       maxFileSize: 2000,
       fileActionSettings: {showZoom: false}
      
    });
@else
$("#input-id").fileinput({
      
      'showUpload':false, 
      'showCancel':false,
      'showClose':false,
      'showRemove':false,
      'allowedFileExtensions': ["jpg","png",'jpeg','mp4'],
       // minFileCount: 2,
       maxFileCount: 6,
       maxFileSize: 2000,
       required: true,
       fileActionSettings: {showZoom: false}
      
    });
@endif

function checkslug() {
    var slug = $('#slug').val();
    var id = $('#id').val();
    
        if(id == undefined){
            var id = 'null'; 
        }
        $.ajax({
            url: '{{ url('tour-gallery/checkslug/') }}/'+slug +'/'+id,
            type: 'get',
            dataType: 'json',
            success:function(response){
                if(response.length <= 0){
                   $('#slug_error').hide();
                    $("#submitbtn").attr("disabled", false);
                }else{
                    $('#slug_error').show();
                    $("#submitbtn").attr("disabled", true);    
                    }
                }
        });
    }

    function SelectImage()
    {
        $('#submitbtn').removeAttr('disabled');
        return false;
    }

    $('#submitbtn').click(function() {
        if( document.getElementById("input-id").files.length == 0 ){
            console.log("no files selected");
            $('#submitbtn').removeClass('disabled'); 
            imagedata(false);
        }
        if (document.getElementById("input-id").files.length < 3) {
            $('.file-error-message').show();
            $('.file-error-message').html("You must select at least 3 file upload.");
            return false;
        }else{
            $('.file-error-message').hide();
            $('.file-error-message').html("");
        }
    });
</script> 

@endpush
