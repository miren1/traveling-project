@extends('admin.master')
@section('bodyData')

           
            <div class="container-fluid">
                <div class="row">
                <a href='/adminshowagent' class='btn btn-primary' style='color:white'>Show Agents</a>
                    <div class="col-sm-12">
                    
                     <ul class="nav nav-tabs">
       <li class="nav-item">
          <a class="nav-link active" data-toggle='tab' href="#profile">Profile</a>
           
       </li>
       <li class="nav-item">
          <a class="nav-link" data-toggle='tab' href="#Address">Address</a>
      </li>
      <li class="nav-item">
          <a class="nav-link" data-toggle='tab' href="#booking">Booking Detail</a>
           
       </li>
        
    </ul>
    
    <div class="tab-content">
      

        <!-- // Booking Detail -->
        <div id="booking" class="container tab-pane" style='min-height:400px'>
          <div class="row">
              <div class="col-sm-6">
                <br>
                 <ul class="list-group list-group-flush">
  <li class="list-group-item"><b>Order Number: </b>#1245</li>
  <li class="list-group-item"><b>Total Amount: </b>2007</li>
 
</ul>
</div>
             
</div>
        </div>

        <!-- // Address Inforamtion -->

         <div id="Address" class="container tab-pane" style='min-height:400px'>
          <div class="row">
              <div class="col-sm-6">
                <br>
                <ul class="list-group">
        
        <li class="list-group-item"><b>House_no: </b><?php echo $agentProfile[0]->house_no?></li>
      <li class="list-group-item"><b>Society Name: </b><?php echo $agentProfile[0]->society_name?></li>
      <li class="list-group-item"><b>Landmark: </b><?php echo $agentProfile[0]->landmark?></li>
    <li class="list-group-item"><b>City Name: </b><?php echo $agentProfile[0]->city?></li>
    <li class="list-group-item"><b>State Name: </b><?php echo $agentProfile[0]->state?></li>

    </ul>
</div>
             
</div>
        </div>
       
     
     <!-- Profile Detail -->
       
        <div id="profile" class="container tab-pane active">
          
          <div class="row">
              
              <div class="col-sm-8">
                              <br>

    <ul class="list-group">
        
    <li class="list-group-item"><b>Company Name: </b><?php echo $agentProfile[0]->company_email?></li>
  <li class="list-group-item"><b>First Name: </b><?php echo $agentProfile[0]->first_name?></li>
  <li class="list-group-item"><b>Last Name: </b><?php echo $agentProfile[0]->last_name?></li>
<li class="list-group-item"><b>Contact Details: </b><?php echo $agentProfile[0]->contact_details?></li>
  <li class="list-group-item"><b>Alternate Number: </b><?php echo $agentProfile[0]->alternate_number?></li>
<li class="list-group-item"><b>Company Pan number: </b><?php echo $agentProfile[0]->company_pan?></li>
   <li class="list-group-item"><b>Company GST Number: </b> <?php echo $agentProfile[0]->company_gst_no?></li>
   <li class="list-group-item"><b>Company Email Address: </b> <?php echo $agentProfile[0]->company_email?></li>

</ul>
              </div>
          </div>
          
           
       </div>
       

      
    

            
           
       </div>
       
</div>




  
                    </div>
                    
                 
                 
                    
                </div>
           

           
                
            
           @endsection
