@extends('admin.master')
@section('bodyData')

<div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                     <ul class="nav nav-tabs">
       <li class="nav-item">
          <a class="nav-link active" data-toggle='tab' href="#profile">Detail</a>   
       </li>        
    </ul>
    <div class="tab-content">
        <div id="profile" class="container tab-pane active">
          <div class="row">
              <div class="col-sm-8">
                <br>
                <ul class="list-group">
                <?php foreach($leaveinfo as $value) {?>
                <li class="list-group-item"><b>Leave Applied By : </b>{{ ucfirst($value->name)}}</li>
                <li class="list-group-item"><b>Leave Start Date : </b>{{ ucfirst($value->leave_date_start)}}</li>
                <li class="list-group-item"><b>Leave End Date : </b>{{ ucfirst($value->leave_date_end)}}</li>
                <li class="list-group-item"><b>Reason : </b>{{ ucfirst($value->reason)}}</li>
                <li class="list-group-item"><b>Leave Type : </b>{{ ucfirst($value->leave_type)}}</li>
                <li class="list-group-item"><b>Leave Mode : </b><?php  if($value->leave_mode == 0){echo 'Half' ;}
                else {echo 'Full';} ?></li>
                <li class="list-group-item"><b>Total Leave Days : </b>{{ ucfirst($value->total_leave_days)}}</li>
                
                <li class="list-group-item"><b>Leave Documents : </b>
                
                <?php  
                  if($value->leave_photos !=NULL) {                                   
                  $photo_array = explode(',',$value->leave_photos); 
                 $photo_array = explode(',',$value->leave_photos);
                 $count = count($photo_array);
                for($i=0;$i<$count;$i++){
                ?>
               <a href="{{ asset( './Leaves') .'/'.$photo_array[$i]}}">Document{{$i+1}}<br>
               </a>
                 <?php } }?>
               </li>
               @if($value->approve_status=="pending")
                <li class="list-group-item">
                  <buttongroup>
                    <a href="{{ route('AdeminLeaveResult',[$value->lid,'Approve']) }}" style='margin-left:1rem'>
                    <button class='btn btn-success'>Approve</button></a>
                    <a href="{{ route('AdeminLeaveResult',[$value->lid,'Reject']) }}">
                    <button class='btn btn-danger'>Reject</button></a>
                  
                  </buttongroup>
                </li>
                @endif
                                         

                
              
                <?php } ?>
            </ul>
              </div>
            
          </div>
        </div>

        <!-- /*--------------------------------- acooments-------------------------------*/ -->
       


    

     
       
    </div>
  
</div>




  
                    </div>
                    
                 
                 
                    
                </div>
            

           
                
            
           @endsection
          