@extends('admin.master')
@section('bodyData')
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
    integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
</script>
<script>
function membership(r) {
    // console.log("function called");
    var role = r.value;
    var company = $("#company").val();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: "{{ route('tourfill') }}",
        method: 'GET',
        data: {
            query: role,
            company: company
        },
        dataType: 'json',
        success: function(data) {
            //  alert("sucess");
            $('#demo').empty();
            // $('tbody').html(data.table_data);
            $('#demo').append(data.table_data);
            $('#page').hide();
        }
    })
}

function membershipplan(r) {
    
    var role = r.value;
   var company = $("#company").val();

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: "{{ route('tourfilter') }}",
        method: 'GET',
        data: {
            query: role,
            company: company
        },
        dataType: 'json',
        success: function(data) {
            //  alert("sucess");
            $('#demo').empty();
            // $('tbody').html(data.table_data);
            $('#demo').append(data.table_data);
            $('#page').hide();
        }
    })
}
</script>
@push('css')
<style type="text/css">
    .card-header {
        margin-bottom: 0 !important;
        background-color: rgb(158 204 243) !important;
        border-bottom: 1px solid rgb(158 204 243) !important;
        padding: 0px !important;
    }

    .page-item.active .page-link {
        z-index: 0 !important;
        color: #fff !important;
        background-color: #007bff !important;
        border-color: #007bff !important;
    }
    </style>
@endpush

<!-- Example single danger button -->
<section class="forms-section" style='min-height:500px'>
<div class="outer-w3-agile mt-3">
<div class="form-group">
    <label class="control-label col-sm-offset-2 col-sm-2" for="company" style="margin-top: 10px;">Package For </label>
    <div class="row"  style="margin-bottom: 110px;">
        <div class="col-md-4">
            <select id="company" class="form-control" onchange='membership(this)'>
                <option value='All'>All</option>
                <?php  foreach ($plancat as $value) { ?>
                <option value='<?php echo $value->id?>'><?php echo $value->name?></option>
                <?php } ?>
            </select>
        </div>

        <div class="col-md-4">
            <input type="text" name="tourname" class="form-control" id="tourname" onkeyup='membershipplan(this)' placeholder="Search Tour">
            {{-- <select id="company" class="form-control" onchange='membership(this)'>
                <option value='All'>All</option>
                <?php  foreach ($plancat as $value) { ?>
                <option value='<?php echo $value->id?>'><?php echo $value->name?></option>
                <?php } ?>
            </select> --}}
        </div>

        <div class='col-md-4 text-right'>
            <button class="btn btn-primary" onclick="window.location='{{ url("inserttour") }}'">
            <i class="fa fa-plus mr-2"></i>Add Tour</button>
        </div>
    </div>
</div>

<div class='row' id='demo'>
    @foreach($packages as $value)

    <div class="card box-shadow col-xl-4 col-md-6 mt-3">
        <div class="card-header">
            <h4 class="py-md-4 py-xl-3 py-2">
                <center><?php echo $value->name ?> </center>
            </h4>
        </div>

        <div class="card-body">

            <img src='<?php echo asset("media")."/".$value->banner_img?>' style='width:100%;height:300px' />
            <br><br>

            <a href="{{url('/tourdetail/')}}/{{base64_encode($value->id)}}">
                <button type="button" class="btn btn-sm btn-block py-2"
                    style='background:#f3941e;color:white'>Details</button>
            </a>
            <br>

            <a href="{{url('/updatetourform')}}/{{base64_encode($value->id)}}">
                <button type="button" class="btn btn-sm btn-block  py-2"
                    style='background:#f3941e;color:white'>Update</button>
            </a>
            <br>
            <a href="{{url('/tourdelete')}}/{{base64_encode($value->id)}}">
                <button type="button" class="btn btn-sm btn-block py-2"
                    style='background:#f3941e;color:white'>Delete</button>
            </a>

        </div>

    </div>
    @endforeach
</div>
<br>
<div style='margin-left:40%; margin-bottom:5%;' id='page'> {{ $packages->links() }}</div>
</div>
</section>

@endsection