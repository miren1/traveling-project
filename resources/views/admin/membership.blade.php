@extends('admin.master')
@section('bodyData')
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
    integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
</script>
<script>
function membership(r) {
    console.log("function called");
    var role = r.value;
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: "{{ route('filterole') }}",
        method: 'GET',
        data: {
            query: role
        },
        dataType: 'json',
        success: function(data) {
            //  alert("sucess");
            $('#demo').empty();
            // $('tbody').html(data.table_data);
            $('#demo').append(data.table_data);
        }
    })

}
</script>

<!-- Example single danger button -->
<div class="form-group">
    <label class="control-label col-sm-offset-2 col-sm-2" for="company">Package For </label>
    <div class="col-sm-6 col-md-4">
        <select id="company" class="form-control" onchange='membership(this)'>
            <option value='customer'>Customer</option>
            <option value='member'>Members</option>
            <option value='agent'>Agent</option>
            {{-- <option value='employee'>Employee</option> --}}
        </select>
    </div>
</div>
<br>

<div class='row' i
d='demo'>
    @foreach($packages as $value)

    <div class="card box-shadow col-xl-4 col-md-6 mt-3">
        <div class="card-header">
            <h4 class="py-md-4 py-xl-3 py-2">
                <center>{{$value->years." "."Years"}}</center>
            </h4>
        </div>

        <div class="card-body">
            <!-- <h5 class="card-title pricing-card-title">
                                <span class="align-top">$</span>59
                                <small class="text-muted">/ month</small>
                            </h5> -->

            <ul class="list-unstyled mt-3 mb-4">

                <li class="py-2 border-bottom"><b>Role: </b>{{ $value->role }}</li>

                <li class="py-2 border-bottom"><b>Fixed Amount: </b>{{ $value->fixed_amt }}</li>
                <li class="py-2 border-bottom" style='text-align: justify;'><b>Mebership Fees:
                        {{ $value->mebership_fees }}</b></li>

                <li class="py-2 border-bottom"><b>Fixed Amount:</b> {{ $value->fixed_amt }} </li>

                <li class="py-2 border-bottom"><b>Free Nights Room: {{ $value->free_nights_room }} </b></li>


                <li class="py-2 border-bottom"><b>Free Nights Avail In:</b> {{ $value->free_nights_to_avail_in	 }} </li>
                <li class="py-2 border-bottom"><b>Free Nights In Any Type Of Hotel: </b>
                    {{ $value->free_night_in_any_type_of_hotel }}</li>
                <li class="py-2 border-bottom"><b>Monthly EMI: </b> {{ $value->monthly_emi }} </li>
                <li class="py-2 border-bottom"><b>Quarterly EMI: </b> {{ $value->quarterly_emi }} </li>
                <li class="py-2 border-bottom"><b>Half Yearly EMI: </b> {{ $value->half_yearly_emi }} </li>
                <li class="py-2 border-bottom"><b>Anual EMI: </b>{{ $value->annual_emi }} </li>
                <li class="py-2 border-bottom"><b>Emi Dafulter Charges: </b> {{ $value->emi_dafulter_charges }} </li>
                <li class="py-2 border-bottom"><b>Flexbility To Move Free Night To Next Year: </b>
                    {{ $value->flexbility_to_move_free_night_to_next_year}} </li>
                <li class="py-2 border-bottom"><b>Upgrade Plans:</b>{{ $value->easy_upgrade_of_membership_plans	 }}
                </li>
                <li class="py-2 border-bottom"><b>Referral: </b>{{ $value->referral }} </li>

            </ul>

        </div>

    </div>
    @endforeach
</div>


<!--//  Pricing tables1 -->
<!-- Pricing tables2 -->
<!-- <section class="pricing2 py-5 my-5 border-top border-bottom">
                    <h4 class="tittle-w3-agileits mb-4">Pricing Tables Style2</h4>
                    <div class="card-deck text-center row">
                        <div class="card box-shadow col-xl-3 col-md-6">
                            <div class="card-header">
                                <h4 class="py-md-4 py-xl-3 py-2 text-white text-white">Basic</h4>
                                <h5 class="card-title pricing-card-title text-white font-weight-bold">
                                    <span class="align-top">$</span>25
                                    <small>/ month</small>
                                </h5>
                            </div>
                            <div class="card-body">
                                <ul class="list-unstyled mt-3 mb-4">
                                    <li class="py-2">Advertising</li>
                                    <li class="py-2 my-2">Branding Services</li>
                                    <li class="py-2">Online Marketing</li>
                                </ul>
                                <button type="button" class="btn btn-lg btn-outline-primary py-2 price2-btn" data-toggle="modal" data-target="#exampleModal">Get Started</button>
                            </div>
                        </div>
                        <div class="card box-shadow col-xl-3 col-md-6 my-lg-0 my-3">
                            <div class="card-header">
                                <h4 class="py-md-4 py-xl-3 py-2 text-white">Standard</h4>
                                <h5 class="card-title pricing-card-title text-white font-weight-bold">
                                    <span class="align-top">$</span>59
                                    <small>/ month</small>
                                </h5>
                            </div>
                            <div class="card-body">
                                <ul class="list-unstyled mt-3 mb-4">
                                    <li class="py-2">Advertising</li>
                                    <li class="py-2 my-2">Branding Services</li>
                                    <li class="py-2">Online Marketing</li>
                                </ul>
                                <button type="button" class="btn btn-lg btn-outline-primary py-2 price2-btn" data-toggle="modal" data-target="#exampleModal">Get Started</button>
                            </div>
                        </div>
                        <div class="card box-shadow col-xl-3 col-md-6 mt-lg-0 mt-3">
                            <div class="card-header">
                                <h4 class="py-md-4 py-xl-3 py-2 text-white">Expert</h4>
                                <h5 class="card-title pricing-card-title text-white font-weight-bold">
                                    <span class="align-top">$</span>75
                                    <small>/ month</small>
                                </h5>
                            </div>
                            <div class="card-body">
                                <ul class="list-unstyled mt-3 mb-4">
                                    <li class="py-2">Advertising</li>
                                    <li class="py-2 my-2">Branding Services</li>
                                    <li class="py-2">Online Marketing</li>
                                </ul>
                                <button type="button" class="btn btn-lg btn-outline-primary py-2 price2-btn" data-toggle="modal" data-target="#exampleModal">Get Started</button>
                            </div>
                        </div>
                        <div class="card box-shadow col-xl-3 col-md-6">
                            <div class="card-header">
                                <h4 class="py-md-4 py-xl-3 py-2 text-white">Premium</h4>
                                <h5 class="card-title pricing-card-title text-white font-weight-bold">
                                    <span class="align-top">$</span>90
                                    <small>/ month</small>
                                </h5>
                            </div>
                            <div class="card-body">
                                <ul class="list-unstyled mt-3 mb-4">
                                    <li class="py-2">Advertising</li>
                                    <li class="py-2 my-2">Branding Services</li>
                                    <li class="py-2">Online Marketing</li>
                                </ul>
                                <button type="button" class="btn btn-lg btn-outline-primary py-2 price2-btn" data-toggle="modal" data-target="#exampleModal">Get Started</button>
                            </div>
                        </div>
                    </div>
                </section> -->
<!--//  Pricing tables2 -->
<!-- Pricing tables3 -->
<!-- <section class="pricing3 mb-5">
                    <h4 class="tittle-w3-agileits mb-4">Pricing Tables Style3</h4>
                    <div class="card-deck text-center row">
                        <div class="card box-shadow col-xl-3 col-md-6">
                            <div class="card-header">
                                <h4 class="py-md-3 py-2 text-white text-white">Basic</h4>
                                <h5 class="card-title pricing-card-title text-white">
                                    $75
                                    <span class="d-block">Per Month</span>
                                </h5>
                            </div>
                            <div class="card-body">
                                <ul class="list-unstyled mt-3 mb-4">
                                    <li class="py-2 text-white">Advertising</li>
                                    <li class="py-2 my-2 text-white">Branding Services</li>
                                    <li class="py-2 text-white">Online Marketing</li>
                                </ul>
                                <button type="button" class="btn btn-lg btn-outline-primary py-2 price3-btn" data-toggle="modal" data-target="#exampleModal">Contact Us</button>
                            </div>
                        </div>
                        <div class="card box-shadow col-xl-3 col-md-6 my-lg-0 my-3">
                            <div class="card-header">
                                <h4 class="py-md-3 py-2 text-white">Standard</h4>
                                <h5 class="card-title pricing-card-title text-white">
                                    $150
                                    <span class="d-block">Per Month</span>
                                </h5>
                            </div>
                            <div class="card-body">
                                <ul class="list-unstyled mt-3 mb-4">
                                    <li class="py-2 text-white">Advertising</li>
                                    <li class="py-2 my-2 text-white">Branding Services</li>
                                    <li class="py-2 text-white">Online Marketing</li>
                                </ul>
                                <button type="button" class="btn btn-lg btn-outline-primary py-2 price3-btn" data-toggle="modal" data-target="#exampleModal">Contact Us</button>
                            </div>
                        </div>
                        <div class="card box-shadow col-xl-3 col-md-6 mt-lg-0 mt-3">
                            <div class="card-header">
                                <h4 class="py-md-3 py-2 text-white">Expert</h4>
                                <h5 class="card-title pricing-card-title text-white">
                                    $500
                                    <span class="d-block">Per Month</span>
                                </h5>
                            </div>
                            <div class="card-body">
                                <ul class="list-unstyled mt-3 mb-4">
                                    <li class="py-2 text-white">Advertising</li>
                                    <li class="py-2 my-2 text-white">Branding Services</li>
                                    <li class="py-2 text-white">Online Marketing</li>
                                </ul>
                                <button type="button" class="btn btn-lg btn-outline-primary py-2 price3-btn" data-toggle="modal" data-target="#exampleModal">Contact Us</button>
                            </div>
                        </div>
                        <div class="card box-shadow col-xl-3 col-md-6">
                            <div class="card-header">
                                <h4 class="py-md-3 py-2 text-white">Premium</h4>
                                <h5 class="card-title pricing-card-title text-white">
                                    $800
                                    <span class="d-block">Per Month</span>
                                </h5>
                            </div>
                            <div class="card-body">
                                <ul class="list-unstyled mt-3 mb-4">
                                    <li class="py-2 text-white">Advertising</li>
                                    <li class="py-2 my-2 text-white">Branding Services</li>
                                    <li class="py-2 text-white">Online Marketing</li>
                                </ul>
                                <button type="button" class="btn btn-lg btn-outline-primary py-2 price3-btn" data-toggle="modal" data-target="#exampleModal">Contact Us</button>
                            </div>
                        </div>
                    </div>
                </section> -->
<!--//  Pricing tables3 -->
</section>
<!-- Modal -->
<!-- <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p class="paragraph-agileits-w3layouts">Woohoo, you're reading this text in a modal!</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary">Save changes</button>
                        </div>
                    </div>
                </div>
            </div> -->
@endsection