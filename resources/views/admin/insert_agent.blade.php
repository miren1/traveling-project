@extends('admin.master')
@section('bodyData')
<meta name="csrf-token" content="{{ csrf_token() }}">

<script>
                   function statechange(r)
                { 
                    $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var cat_id = r.value;
           // console.log(cat_id);

                 $.ajax({
                   
                       url:"{{ route('getCity') }}?id=" + cat_id ,
                       type:"POST",
                       data: {
                           cat_id: cat_id
                        },
                      
                       success:function (data) {
                        $('#c_city_id').empty();
                        // console.log(data.cities);

                        $.each(data.cities,function(index,cities){
                            //console.log(cities);
                            
                            $('#c_city_id').append('<option value="'+index+'">'+cities+'</option>');
                        })
                       }
                   })
                }
               
                   </script>

<section class="forms-section" style='min-height:500px'>
<div class="outer-w3-agile mt-3">
        <h4 class="tittle-w3-agileits mb-4">Agent Creation</h4>
<a href='/adminshowagent' class='btn btn-primary'>Show Agent List</a>
        
        @if ($message = Session::get('AggentAdd'))
<div class="alert alert-success alert-block">
	<button type="button" class="close" data-dismiss="alert">×</button>	
        <strong>{{ $message }}</strong>
</div>
@endif
          
        <form action="{{url('agentCreation')}}" method="post">
        @csrf
      <div class="form-row">
               
                <div class="form-group col-md-6">
                    <label for="inputEmail4">First Name</label>
                    <input type="text" class="form-control" id="inputEmail4" placeholder="Enter Name" required="" name='first_name' pattern='[A-Za-z]{2,30}'>
                </div>
                <div class="form-group col-md-6">
                   <label for="inputEmail4">Last Name</label>
                   <input type="text" class="form-control" id="inputEmail4" placeholder="Enter Last Name" required="" name='last_name' pattern='[A-Za-z]{2,30}'>
               </div>
           </div>

          

          <div class="form-row">
               
               <div class="form-group col-md-6">
                   <label for="inputEmail4">Company Name</label>
                   <input type="text" class="form-control" id="inputEmail4" placeholder="Enter Company Name" required="" name='company_name'>
               </div>
               <div class="form-group col-md-6">
                   <label for="inputEmail4">Contatct Details</label>
                   <input type="text" class="form-control" id="inputEmail4" placeholder="Enter Contact Detail" required="" name='contact_details' pattern='[0-9]{10}'>
               </div>
          </div>

          
          <div class="form-row">
               
               <div class="form-group col-md-6">
                   <label for="inputEmail4">Alternate Number</label>
                   <input type="text" class="form-control" required="" name='alternate_number' Placeholder='Enter Alternate number' pattern='[0-9]{10}'>
               </div>

               <div class="form-group col-md-6">
                   <label for="inputEmail4">Company Pan</label>
                   <input type="text" class="form-control" required="" name='company_pan' placeholder='Enter Company Pan'>
               </div>
               
          </div>

          <div class="form-row">
               
               <div class="form-group col-md-6">
                   <label for="inputEmail4">Company GST</label>
                   <input type="text" class="form-control" required="" name='company_gst_no' placeholder='Enter Company GST '>
               </div>

               <div class="form-group col-md-6">
                   <label for="inputEmail4">Company Email</label>
                   <input type="email" class="form-control" required="" name='company_email' placeholder='Enter Email'>
               </div>
               
          </div>
          <div class="form-row">
               
               <div class="form-group col-md-6">
                   <label for="inputEmail4">House No</label>
                   <input type="text" class="form-control" required="" name='house_no' placeholder='Enter House No '>
               </div>

               <div class="form-group col-md-6">
                   <label for="inputEmail4">Society Name </label>
                   <input type="text" class="form-control" required="" name='society_name' placeholder='Enter Society Name '>
               </div>
               
          </div>

          <div class="form-row">
               
               <div class="form-group col-md-6">
                   <label for="inputEmail4">Land Mark</label>
                   <input type="text" class="form-control" required="" name='landmark' placeholder='Enter Landmark'>
               </div>

               <div class="form-group col-md-6">
                   <label for="inputEmail4">Pincode </label>
                   <input type="text" class="form-control" required="" name='pincode' placeholder='Enter Pincode Number '>
               </div>
               
          </div>
          

                  
          <div class="form-row">
          <div class="form-group col-md-6">
                   <label for="inputEmail4">State</label>
    <select class="form-control" name='state_fk_id' id='c_state_id' onchange='statechange(this)' required>
                    <option value=''>Select State</option>
                        <?php
                     
                        foreach ($state as $key => $s) { ?>
                           
            <option value='<?php echo $s->id?>' id='stateid'/>
                         <?php    print_r($s->name);
                            ?>
                            </option>
                         <?php
                        }
                      ?>
                   </select>
    
                   
                 </div>
                 <div class="form-group col-md-6">
                   <label for="inputEmail4">City</label>
                   <select id="c_city_id" class="form-control" name='city_fk_id' >
                   
                   </select>
               </div>
       
 	
               <div class="form-group col-md-6">
                  
                   <label for="inputEmail4">System Password </label>
                   <input type="password" class="form-control" required="" name='password' placeholder='Enter Password'>
               </div>
        
        	
               <div class="form-group col-md-6">
                <br>
              <a> <button type="submit" class="btn btn-primary mr-5" style='margin-top:13px'>Create Agent Pofile</button></a>
             
               </div>

            
        </form>
    </div>

</section>

@endsection
