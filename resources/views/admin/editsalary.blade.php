@extends('admin.master')
@section('bodyData')
<section class="forms-section" style='min-height:500px'>
<div class="outer-w3-agile mt-3">
        <h4 class="tittle-w3-agileits mb-4">Employee Salary Updation</h4>
<a href='/showsalary'><button class='btn btn-primary'>Show Salary</button></a>
        
        @if ($message = Session::get('updatesalary'))
<div class="alert alert-success alert-block">
	<button type="button" class="close" data-dismiss="alert">×</button>	
        <strong>{{ $message }}</strong>
</div>
@endif
          
        <form action="{{url('editsalarybyadmin')}}" method="post">
        @csrf
      <div class="form-row">
               
                <div class="form-group col-md-6">
                    <label for="inputEmail4">Salary</label>
         <input type="text" class="form-control" id="inputEmail4" placeholder="Enter Salary" required="" value='<?php echo $data[0]->salary?>' name='salary'>
                </div>
           </div>
           <div class="form-row">
               
               <div class="form-group col-md-6">
                   <label for="inputEmail4">Follow Date</label>
                   <input type="date" class="form-control" id="inputEmail4" placeholder="Enter Salary" required="" name='follow_date' value='<?php echo $data[0]->follow_date?>'>
               </div>
          </div>
        
        <input type='hidden' name='id' value='<?php echo $data[0]->id?>'>
        
         <div class="form-row">
               
               <div class="form-group col-md-6">
                   <label for="inputEmail4">Employee Name </label>
               <select name='fk_emp_id'>
               <?php $temp = $data[0]->fk_emp_id?>
               <?php foreach($users as $u){?>
               <option value=<?php  echo $u->id?> <?php if($temp == $u->id){?> "selected"<?php } ?>><?php echo $u->name ." ".$u->surname." "."JJ-EMP-".$u->id?></option>
               <?php } ?>
               </select>
               </div>
          </div>
          
          
          
            <button type="submit" class="btn btn-primary">Update Salary Data</button>
        </form>
    </div>

</section>

@endsection
