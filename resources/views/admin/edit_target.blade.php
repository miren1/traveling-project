@extends('admin.master')
@section('bodyData')

<section class="forms-section" style='min-height:500px'>
<div class="outer-w3-agile mt-3">
@if ($message = Session::get('targetInserted'))
<div class="alert alert-success alert-block">
	<button type="button" class="close" data-dismiss="alert">×</button>	
        <strong>{{ $message }}</strong>
</div>
@endif
        <h4 class="tittle-w3-agileits mb-4">Target Form</h4>
        
  <form action="{{url('updatetarget')}}" method="post">
        @csrf
      <div class="form-row">
               
                <div class="form-group col-md-6">
                    <label for="inputEmail4">Target Count</label>
    <input type="number" class="form-control" id="inputEmail4" placeholder="Enter Target Amount" required="" name='target' value='<?php echo $targets[0]->target?>'>
                </div>
                <input type='hidden' id='id' value='<?php echo $targets[0]->id?>' name='id'>
                <div class="form-group col-md-6">
                   <label for="inputEmail4">Month</label>
                   <input type="month" class="form-control" id="inputEmail4" name='month_year' value='<?php echo $targets[0]->month_year?>'> 
               </div>
           </div>
            <div class="form-group col-md-6">
                   <label for="inputEmail4">Employee</label>
                   <select class="form-control" id='employee_id_fk' name='employee_id_fk'>
                    <option value=''>Select Employee</option>
                        <?php
                     
                        foreach ($users as $key => $s) { ?>
                           
            <option value='<?php echo $s->id?>' <?php if($s->id == $targets[0]->employee_id_fk) echo 'selected'?>/>
                         <?php    print_r($s->name);
                            ?>
                            </option>
                         <?php
                        }
                      ?>
                   </select>
               </div>
               <button type="submit" class="btn btn-primary">Update Target</button>

            </div>
         
         </div>

              
               
          </div>

           
        </form>
    </div>

</section>

@endsection
