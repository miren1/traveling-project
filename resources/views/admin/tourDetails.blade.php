@extends('admin.master')
@section('bodyData')
<style>
/*****************globals*************/
body {
    font-family: 'open sans';
    overflow-x: hidden;
}

img {
    max-width: 100%;
}

.preview {
    display: -webkit-box;
    display: -webkit-flex;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
    -webkit-flex-direction: column;
    -ms-flex-direction: column;
    flex-direction: column;
}

@media screen and (max-width: 996px) {
    .preview {
        margin-bottom: 20px;
    }
}

.preview-pic {
    -webkit-box-flex: 1;
    -webkit-flex-grow: 1;
    -ms-flex-positive: 1;
    flex-grow: 1;
}

.preview-thumbnail.nav-tabs {
    border: none;
    margin-top: 15px;
}

.preview-thumbnail.nav-tabs li {
    width: 18%;
    margin-right: 2.5%;
}

.preview-thumbnail.nav-tabs li img {
    max-width: 100%;
    display: block;
}

.preview-thumbnail.nav-tabs li a {
    padding: 0;
    margin: 0;
}

.preview-thumbnail.nav-tabs li:last-of-type {
    margin-right: 0;
}

.tab-content {
    overflow: hidden;
}

.tab-content img {
    width: 100%;
    -webkit-animation-name: opacity;
    animation-name: opacity;
    -webkit-animation-duration: .3s;
    animation-duration: .3s;
}

.card {
    margin-top: 50px;
    background: #eee;
    padding: 3em;
    line-height: 1.5em;
}

@media screen and (min-width: 997px) {
    .wrapper {
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
    }
}

.details {
    display: -webkit-box;
    display: -webkit-flex;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
    -webkit-flex-direction: column;
    -ms-flex-direction: column;
    flex-direction: column;
}

.colors {
    -webkit-box-flex: 1;
    -webkit-flex-grow: 1;
    -ms-flex-positive: 1;
    flex-grow: 1;
}

.product-title,
.price,
.sizes,
.colors {
    text-transform: UPPERCASE;
    font-weight: bold;
}

.checked,
.price span {
    color: #ff9f1a;
}

.product-title,
.rating,
.product-description,
.price,
.vote,
.sizes {
    margin-bottom: 15px;
}

.product-title {
    margin-top: 0;
}

.size {
    margin-right: 10px;
}

.size:first-of-type {
    margin-left: 40px;
}

.color {
    display: inline-block;
    vertical-align: middle;
    margin-right: 10px;
    height: 2em;
    width: 2em;
    border-radius: 2px;
}

.color:first-of-type {
    margin-left: 20px;
}

.add-to-cart,
.like {
    background: #ff9f1a;
    padding: 1.2em 1.5em;
    border: none;
    text-transform: UPPERCASE;
    font-weight: bold;
    color: #fff;
    -webkit-transition: background .3s ease;
    transition: background .3s ease;
}

.add-to-cart:hover,
.like:hover {
    background: #b36800;
    color: #fff;
}

.not-available {
    text-align: center;
    line-height: 2em;
}

.not-available:before {
    font-family: fontawesome;
    content: "\f00d";
    color: #fff;
}

.orange {
    background: #ff9f1a;
}

.green {
    background: #85ad00;
}

.blue {
    background: #0076ad;
}

.tooltip-inner {
    padding: 1.3em;
}

@-webkit-keyframes opacity {
    0% {
        opacity: 0;
        -webkit-transform: scale(3);
        transform: scale(3);
    }

    100% {
        opacity: 1;
        -webkit-transform: scale(1);
        transform: scale(1);
    }
}

@keyframes opacity {
    0% {
        opacity: 0;
        -webkit-transform: scale(3);
        transform: scale(3);
    }

    100% {
        opacity: 1;
        -webkit-transform: scale(1);
        transform: scale(1);
    }
}

/*# sourceMappingURL=style.css.map */
.modal-body {
    height: 600px;
    overflow-y: auto;
}

@media (min-height: 500px) {
    .modal-body {
        height: 400px;
    }
}

@media (min-height: 800px) {
    .modal-body {
        height: 600px;
    }
}
</style>
<div class="container">
    <div class="card">
        <div class="container-fliud">
            <div class="wrapper row">
                <div class="preview col-md-3">

                    <div class="preview-pic tab-content">
                        <div class="tab-pane active" id="pic-1"><img
                                src="<?php echo asset('media/').'/'.$result[0]->banner_img?>" /></div>

                    </div>


                </div>
                <div class="details col-md-9">
                    <span class="product-title"><?php echo "JJ-TOUR-".$result[0]->id ?></span>
                    <h3 class="product-title"><?php echo $result[0]->name?></h3>

                    <div class="rating">
                        Days: <?php  echo $result[0]->days ;?>&nbsp;
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                        Itinerary
                        </button>&nbsp;
                        <a href='{{url("/photogallery/")}}/{{ base64_encode($result[0]->id)}}'><button class="btn btn-primary"
                                type="button">Photo Gallery</button></a>
                        <a href='{{url("/updatetourform/")}}/{{base64_encode($result[0]->id) }}'><button class="btn btn-primary"
                                type="button">Update Tour</button></a>
                        <a href='{{url("/adminshowtours/")}}'><button class="btn btn-primary" type="button">All Tours</button></a>

                        <!-- Modal -->
                        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
                            aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel"><?php echo $result[0]->name ?>
                                        </h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <?php 

        foreach ($itenary as $value) {
          ?>
                                        <p><b><?php echo "Day".$value->day_number;echo ": ".$value->caption  ?></b></p>
                                        <p><?php  print_r($value->details) ?>
                                        </p>
                                        <img src="<?php echo asset('itenary')."/".$value->images ?>"
                                            style='height:150px;width:70%'>
                                        <hr>
                                        <?php
        }?>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary"
                                            data-dismiss="modal">Close</button>
                                        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
                                    </div>
                                </div>
                            </div>
                        </div><?php $small_detail_obj = json_decode($result[0]->small_info); 
                            $detail1 =  json_decode($result[0]->detail1); 
                            $detail2 =  json_decode($result[0]->detail2); 
                            $detail3 =  json_decode($result[0]->detail3); 
                            $detail4 =  json_decode($result[0]->detail4); 
                           
                            ?>
                    </div>
                    <table class="table table-bordered">
                        <tbody>
                            <?php  foreach ($small_detail_obj as $key => $value) {?>
                            <tr>
                                <td><?php echo $key ?></td>
                                <td><?php echo $value ?></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                <div class='row'>
                    <div class='col-sm-12'>
                        <?php if($detail1) {?>
                        <?php foreach ($detail1 as $key1 => $value1) { ?>
                        <h6 class="price"><?php echo $key1 ?> </h6>
                        <p class="product-description" style='text-align:justify'>
                            <?php 
                        print_r($value1);
                        ?>
                        </p><?php } }?>
                        <hr>
                        <?php foreach ($detail2 as $key1 => $value1) { ?>
                        <h6 class="price"><?php echo $key1 ?> </h6>
                        <p class="product-description" style='text-align:justify'>
                            <?php 
                        print_r($value1);
                        ?>
                        </p><?php } ?>
                        <hr>
                        <?php foreach ($detail3 as $key1 => $value1) { ?>
                        <h6 class="price"><?php echo $key1 ?> </h6>
                        <p class="product-description" style='text-align:justify'>
                            <?php 
                        print_r($value1);
                        ?>
                        </p><?php } ?>
                        <hr>
                        <?php foreach ($detail4 as $key1 => $value1) { ?>
                        <h6 class="price"><?php echo $key1 ?> </h6>
                        <p class="product-description" style='text-align:justify'>
                            <?php 
                        print_r($value1);
                        ?>
                        </p><?php } ?>
                        <hr>

                        <?php if($result[0]->inclusion)
            { ?>
                        <h6 class='price'>Inclusion:</h6>
                        <?php
              print_r($result[0]->inclusion);
              ?>
                        <hr><?php
            } ?>


                        <?php if($result[0]->optional_charges)
            { ?>
                        <h6 class='price'>Optional Charges:</h6>
                        <?php
            print_r($result[0]->optional_charges);
              ?>
                        <hr><?php
            } ?>

                        <?php if($result[0]->exclusion)
            { ?>
                        <h6 class='price'>Exclusion:</h6>
                        <?php
            print_r($result[0]->exclusion);
              ?>
                        <hr><?php
            } ?>


                        <?php if($result[0]->essentials)
            { ?>
                        <h6 class='price'>Basic Essentials:</h6>
                        <?php
            print_r($result[0]->essentials);
            ?>
                        <hr><?php
            } ?>

                        <?php if($result[0]->clothing_essential)
            { ?>
                        <h6 class='price'>Clothing Essentials:</h6>
                        <?php
            print_r($result[0]->clothing_essential);
            ?>
                        <hr><?php
            } ?>



                        <div class="action">
                            <a href='{{url("/photogallery/")}}/{{ base64_encode($result[0]->id)}}'>

                                <button class="btn btn-primary" type="button">Photo Gallery</button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection