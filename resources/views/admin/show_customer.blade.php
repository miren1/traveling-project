@extends('admin.master')
@section('bodyData')
<meta name="csrf-token" content="{{ csrf_token() }}">

<div class="outer-w3-agile mt-3">

    <div class='row'>
      <div class='col-md-6'>
        <div class="btn btn-primary"><i class="fa fa-list mr-2"></i>Customer list</div>
      </div>
      <div class='col-md-6 text-right'>
        <button class="btn btn-primary" onclick="window.location='{{ route("admin-customer.add") }}'">
          <i class="fa fa-plus mr-2"></i>Add Customer</button>
      </div>
    </div>
    <br>
   <!-- 
    <div class='row mb-2 ml-2'>
        <a href="{{route('adminshowcustomer')}}"><button class="btn btn-primary">Show Customer</button></a> &nbsp;&nbsp;
        <a href="{{route('admin-customer.add')}}"><button class="btn btn-primary">Add Customer</button></a>

    </div> -->
        
    <div class="table-responsive">
        <table class="table table-hover" style="width: 100%;" id="dataTable">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">Id</th>
                    <th scope="col">First Name</th>
                    <th scope="col">Last Name </th>
                    <th scope="col">Mobile Number</th>
                    <th scope="col">Email</th>
                    <th>WhatsApp</th>
                    <th scope="col">Registration Date</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
@endsection 
@push('js')
<script>
  $(function() {
    $("#dataTable").DataTable({
        'processing': true,
        'serverSide': true,
        'serverMethod': 'post',
        "aLengthMenu": [[10,25, 50,100], [10,25, 50,100]],
        order: [
            [0, 'desc']
        ],
        "ajax": {
            "url": "{{ route('admin-customer.show') }}",
            "data": {
                "_token": "{{ csrf_token() }}"
            },
        },
        "columns": [
            {
                data: "id",
                render: function(data, row, alldata) {
                    return 'JJ-USER-'+alldata.id;
                }
            },
            {
                data: "first_name",orderable:false,
            },
            {
                data: "last_name",orderable:false,
            },
            {
                data: "mobile_number",orderable:false
            },
            {
                data: "email",orderable:false
            },
            {
                data: "whats_up",orderable:false
            },
            {
                data: "created_at",orderable:false,
                render: function(data, row, alldata) {
                    return moment(alldata.created_at).format('DD-MM-YYYY h:mm a');
                }
            },
            {
                data: "id",orderable:false,
                render: function(data, row, alldata) {
                  var $id = btoa(alldata.id);
                    $html = `<a href="{{url('/admin-customer/detail')}}/`+$id+`">
                         <i class="fa fa-info-circle" aria-hidden="true" title='Detail' style='font-size:20px'></i>
                         </a>
                         <a href="{{url('/admin-customer/edit')}}/`+$id+`">
                         <i class="fa fa-edit" aria-hidden="true" title='Edit' style='font-size:20px'></i>
                         </a>
                      
                      <i class="fa fa-trash delete" style='font-size:20px;color:red;cursor:pointer;' title='delete Customer' data-id="`+alldata.id+`" >
                      </i>`;
                    return $html;
                }
            },
        ]
    });

    $(document).on('click','.delete',function(){
      var id = $(this).data('id');
      swal({
        title: "Are you sure?",
        text: "You want to delete this Customer.",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      }).then((willDelete) => {
        if(willDelete)
        {
          $.ajax({
            method:'post',
            url: "{{route('admin-customer.destroy')}}",
            data: {id:id,
              '_token':"{{csrf_token()}}"
            },
            success : function(result){
              swal({title:result.title,icon:result.icon,text:result.message});
              $('#dataTable').DataTable().ajax.reload();
            }
          });
        }
      });
    });

    $(document).on('click','.leave',function(){
      var id = $(this).data('id');
      // alert('true');
      $('#modal-myvalue').val(id);
    });
  });
</script>
@endpush
