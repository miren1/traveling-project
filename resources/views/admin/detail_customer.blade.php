@extends('admin.master')
@section('bodyData')

<div class="container-fluid">                 
  <div class="row">

      <div class='col-md-12 text-right'>
         <br>
        <div class="btn btn-primary" onclick="window.location='{{ route("adminshowcustomer") }}'"><i class="fa fa-list mr-2"></i>Customer list</div>
        <br>
      </div>
     
    <br>
    
    <div class="col-sm-12">        
    
      <ul class="nav nav-tabs">
        <li class="nav-item">
          <a class="nav-link active" data-toggle='tab' href="#profile">Profile</a>  
        </li>
        <li class="nav-item">
          <a class="nav-link" data-toggle='tab' href="#detail">Booking Details</a>   
        </li>
      </ul>
      <div class="tab-content">
        <div id="profile" class="container tab-pane active">
          <div class="row">
            <div class="col-sm-8"><br>
              <ul class="list-group">
                <li class="list-group-item"><b>First Name: </b>{{ ucfirst($customer->first_name)}}</li>
                <li class="list-group-item"><b>Last Name: </b>{{ ucfirst($customer->last_name)}}</li>
                <li class="list-group-item"><b>Mobile Number: </b>{{ $customer->mobile_number}}</li>
                <li class="list-group-item"><b>Whatsapp Number: </b>{{ $customer->whats_up}}</li>
                <li class="list-group-item"><b>Email : </b>{{$customer->email}}</li>
              </ul>
            </div> 
          </div>
        </div>
        <!-- /*--------------------------------- acooments-------------------------------*/ -->
        <!-- /**********************************Detail comments********************** */ -->
        <div id="detail" class="container tab-pane" style='height:400px'>
        <table class="table table-striped">
          <thead>
            <tr>
              <th scope="col">Comments</th>
              <th scope="col">By Employee</th>
              <th scope="col">By Date</th>
            </tr>
          </thead>
          <tbody>
            <!-- // By Booking Details -->
          </tbody>
        </table>
 </div>

 <!-- /***************************a comments ******************** */  -->


        </div>
        </div>
     
       
    </div>
  
</div>
</div>
</div>
@endsection
          