@extends('admin.master')
@section('bodyData')
<meta name="csrf-token" content="{{ csrf_token() }}">

<div class="outer-w3-agile mt-3">
 
@if ($message = Session::get('LeaveUpdates'))
<div class="alert alert-success alert-block">
	<button type="button" class="close" data-dismiss="alert">×</button>	
        <strong>{{ $message }}</strong>
</div>
@endif
@if ($message = Session::get('DeleteLeave'))
<div class="alert alert-success alert-block">
	<button type="button" class="close" data-dismiss="alert">×</button>	
        <strong>{{ $message }}</strong>
</div>
@endif

    <div class='row'>
      <div class='col-md-6'>
        <div class="btn btn-primary"><i class="fa fa-list mr-2"></i>Leave list</div>
      </div>
      <div class='col-md-6 text-right'>
<a href="/adminLeaveForm"><button class='btn btn-primary'>Add Leave</button></a>
</div>
    </div>
<br>

    <table class="table table-hover" style="width: 100%;" id="dataTable">
        <thead class="thead-dark">
          <tr>
          <th scope="col">Sr. No</th>
          <th scope="col">Title</th>
          <th scope="col">Start Date</th>
          <th scope="col">Actions</th>
            </tr>
    </thead>
    {{-- <tbody id='tableid'>
            <tr>
            <?php  foreach ($data as $value) { ?>
                <td scope="row"><?php echo ucfirst($value->title)?></td>
               <td><?php echo $value->start_date?></td>
               <td><?php echo $value->created_at?></td>
               <td>
                      <a href="/editleave/{{$value->id}}">
                         <!-- <button class="btn btn-info">Update</button> -->
                         <i class="fa fa-edit" aria-hidden="true" title='Edit' style='font-size:20px'></i>
                         </a>
                        <a href="/leavedelete/{{$value->id}}">
                        <i class="fa fa-trash" aria-hidden="true" onclick='return confirm("Are you Sure Delete this Leave?")' style='color:red;font-size:20px' title='Delete'></i>
                    </a>
                </td>
            </tr>  
            <?php } ?>
        </tbody>--}}
    </table> 
  </div>

    <center>
  </div>
  </div>
</div>
</center>
</div>
@endsection

@push('js')
<script>
  $(function() {
    $("#dataTable").DataTable({
        "pageLength": 5,
        "autoWidth": true,
        "lengthChange": true,
        "searching": true,
        "aLengthMenu": [[10,25, 50,100], [10,25, 50,100]],
        order: [
            [0, 'desc']
        ],
        "ajax": {
            "url": "{{ route('FestivalLeave') }}",
            "data": {
                "_token": "{{ csrf_token() }}"
            },
        },
        "columns": [
            {
                data: "id",
                render: function(data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1
                }
            },
            {
                data: "title",orderable:false,
            },
            {
                data: "start_date",orderable:false,
            },
            {
                data: "id",orderable:false,
                render: function(data, row, alldata) {
                  var $id = btoa(alldata.id);
                    $html = `<a href="{{url('/admin-banner/edit')}}/`+$id+`">
                         <!-- <button class="btn btn-info">Update</button> -->
                         <i class="fa fa-edit" aria-hidden="true" title='Edit' style='font-size:20px'></i>
                         </a>
                      
                      <i class="fa fa-trash delete" style='font-size:20px;color:red;cursor:pointer;' title='delete banner' data-id="`+alldata.id+`" >
                      </i>`;
                    return $html;
                }
            },
        ]
    });

    $(document).on('click','.delete',function(){
      var id = $(this).data('id');
      swal({
        title: "Are you sure?",
        text: "You want to delete this Banner.",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      }).then((willDelete) => {
        if(willDelete)
        {
          $.ajax({
            method:'post',
            url: "{{route('admin-banner.destroy')}}",
            data: {id:id,
              '_token':"{{csrf_token()}}"
            },
            success : function(result){
              swal({title:result.title,icon:result.icon,text:result.message});
              $('#dataTable').DataTable().ajax.reload();
            }
          });
        }
      });
    });

    $(document).on('click','.leave',function(){
      var id = $(this).data('id');
      // alert('true');
      $('#modal-myvalue').val(id);
    });
  });
</script>
@endpush
