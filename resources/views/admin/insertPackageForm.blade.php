@extends('admin.master')
@section('bodyData')

<section class="forms-section" style='min-height:500px'>
    <div class="outer-w3-agile mt-3">
    <a class='btn btn-primary' href='{{url('/adminshowpackages')}}'>Back</a>
        <h4 class="tittle-w3-agileits mb-4">Add Tour Category</h4>    
       
        <form action="{{url('createplan')}}" id="Agentform" method="post" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="form-group col-md-4">
                    <label for="inputEmail4">Name</label>
                    <input type="text" class="form-control" id="name" placeholder="Enter Name" required="" name='name' >
                    <span style="color:red; display: none"  id="namespan">The Name  is required</span><br>
                    <label for="inputEmail4">Slug</label>
                    <input type="text" class="form-control" 
                    data-bv-stringlength="true"
                    data-bv-stringlength-min="3"
                    data-bv-stringlength-max="20"
                    data-bv-stringlength-message="Slug minimum length is 3 and maxmum length is 20"
                    data-bv-regexp="true"
                    data-bv-regexp-regexp="^[a-z0-9]*$"
                    data-bv-regexp-message="The Slug can consist of lower case  alphabetical characters and no spaces "
                    data-bv-remote-url = "{{ url('/catcheckSlug') }}"
                    data-bv-remote = "true"
                    data-bv-remote-type = "GET"   
                    data-bv-remote-message="Opps ! Slug Already Used." id="slug" placeholder="Enter slug" required="" name='slug' >
                    <span style="color:red; display: none" id="slugspan">The slug is required</span><br>
                   <label for="inputEmail4">Tag Line</label>
                   <input type="text" class="form-control" id="tagline" placeholder="Enter Tagline" name='tag_line' >
                    <span style="color:red; display: none" id="taglinespan">The Tag Line  is required</span><br>
                   <label for="inputEmail4">Details</label>
                    <textarea class="form-control" id="details" placeholder="Enter Plan Detail" name='details' required="" ></textarea>
                    <span style="color:red; display: none" id="detailspan">The Details  is required</span><br>
                   <label for="inputEmail4">Image</label>
                   <br>
                   <input type='file' class="form-control" id="photo" name='photo' required=''  >
                    <span style="color:red; display: none" id="imagespan">The Image  is required</span>
               </div>
            </div>
               <button type="submit" id="submit" class="btn btn-primary">Submit</button>
          </div>

          
         
              
          

          </div>
          </div>

              
               
          </div>

            
        </form>
    </div>

</section>

@endsection



@push('css')

<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css"/>

@endpush
@push('js')


<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js"></script>
<script>

$(document).ready(function() {
    $('#submit').click(function() {
        if (!$('#name').val()) {
           $( "#namespan" ).show();

        }
         if (!$('#slug').val()) {
           $( "#slugspan" ).show();
            
        }
         if (!$('#tagline').val()) {
           $( "#taglinespan" ).show();
            
        }
         if (!$('#details').val()) {
           $( "#detailspan" ).show();
            
        }
         if (!$('#photo').val()) {
           $( "#imagespan" ).show();
            
        }
    })
});
</script>
@endpush