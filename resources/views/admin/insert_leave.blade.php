@extends('admin.master')
@section('bodyData')

<section class="forms-section" style='min-height:500px'>
  <div class="outer-w3-agile mt-3">
    @if ($message = Session::get('LeaveCreated'))
    <div class="alert alert-success alert-block">
      <button type="button" class="close" data-dismiss="alert">×</button> 
        <strong>{{ $message }}</strong>
    </div>
    @endif
    <h4 class="tittle-w3-agileits mb-4">Leave Creation</h4>
      <form action="{{url('createLeaveAdmin')}}" method="post">
      @csrf
      <input type="hidden" id="id" name='id' value="@if(isset($leave_data)){{$leave_data->id}}@endif">
        <div class="form-row">
          <div class="form-group col-md-4">
            <label for="inputEmail4">Title</label>
            <input type="text" class="form-control" id="inputEmail4" placeholder="Enter Leave Title" required="" name='title' value="@if(isset($leave_data)){{$leave_data->title}}@endif">
            <br>
            <label for="inputEmail4">Leave Date</label>
            <!-- <input type="date" class="form-control" placeholder="Enter Employee's Last Name" required="" name='start_date' value="@if(isset($leave_data)){{$leave_data->start_date}}@endif">
             -->
            <input class="form-control" type="text" name='start_date' id="leave_date" placeholder="dd/mm/yyyy" value="@if(isset($leave_data)){{$leave_data->start_date}}@endif">
            
          </div>
        </div>
        <button type="submit" class="btn btn-primary">@if(isset($leave_data)) Update @else Save @endif</button>
      </form>
  </div>

  <!-- ================Show Leave Start=====================-->

    <button class="btn btn-primary" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample" style="margin-left: 20px">
      Show Leave
    </button>
  
  <div class="collapse" id="collapseExample">
    <div class="outer-w3-agile mt-3" style="margin-top: 10px">
    <table class="table table-hover" style="width: 100%;" id="dataTable">
        <thead class="thead-dark">
          <tr>
          <th scope="col">Sr. No</th>
          <th scope="col">Title</th>
          <th scope="col">Start Date</th>
          <th scope="col">Actions</th>
            </tr>
    </thead>
      <tbody id='tableid'>
        
      </tbody>
    </table>
    </div> 
  </div>


  <!-- ================Show Leave Start=====================-->



  <div class="outer-w3-agile mt-3">
    {{-- <h4 class="tittle-w3-agileits mb-4">Total Leave Update-<?php echo $data[0]->year ?></h4> --}}
    @if ($message = Session::get('CurrentYearLeave'))
    <div class="alert alert-success alert-block">
      <button type="button" class="close" data-dismiss="alert">×</button> 
      <strong>{{ $message }}</strong>
    </div>
    @endif
    <a href='inserttotalLeave'><button class='btn btn-primary'>Add Total Leave For New Year</button></a>
    @if ($message = Session::get('ToralLeaveError'))
    <div class="alert alert-danger alert-block">
      <button type="button" class="close" data-dismiss="alert">×</button> 
      <strong>{{ $message }}</strong>
    </div>
    @endif
    @if ($message = Session::get('ToralLeaveYear'))
    <div class="alert alert-success alert-block">
      <button type="button" class="close" data-dismiss="alert">×</button> 
      <strong>{{ $message }}</strong>
    </div>
    @endif
 
    <form action="{{url('updatetotalLeaves')}}" method="post">
        @csrf
      <div class="form-row">
        <div class="form-group col-md-4">
            <label for="inputEmail4">Total Casual Leave</label>
            <input type="text" class="form-control" id="inputEmail4" placeholder="Enter Casual Leave" required="" name='casual' value="@if(isset($data)){{$data->casual}}@endif">
            <br>
           <label for="inputEmail4">Total Planned Leave</label>
           <input type="text" class="form-control" id="inputEmail4" placeholder="Enter Planned Leave" required="" name='planned' value="@if(isset($data)){{$data->planned}}@endif">
        </div>
        <script>
          document.querySelector("input[type=number]")
          .oninput = e => console.log(new Date(e.target.valueAsNumber, 0, 1))
        </script>
      </div>
      <button type="submit" class="btn btn-primary" onclick='return confirm("Are you Sure You Want to Update Data");'>Update</button>
    </form>
  </div>
</section>
@endsection
@push('css')
 
<link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel = "stylesheet">
<style type="text/css">
  .outer-w3-agile {
    margin-bottom: 1% !important;
  }

ul ul a {
    font-size: 0.9em !important;
    padding-left: 30px !important;
    background: #083672;
}
#sidebar ul li a:hover {
    color: #083672;
    background: #fff;
}
.btn-primary, .btn-primary:hover, .btn-primary:active, .btn-primary:visited {
    background-color: #083672 !important;
border-color: #083672 !important;
}
.btn-info, .btn-info:hover, .btn-info:active, .btn-info:visited {
    background-color: #f3941e !important;
  border-color: #f3941e !important;
}
.bg-primary
{
 background-color: #f3941e !important;
  border-color: #f3941e !important;
}

.fa-info-circle,.fa-edit
{
color:#083672;
}
ul.top-icons-agileits-w3layouts a.nav-link i {
    display: inline-block;
    font-size: 17px;
    color: #083672;
}
.nav-pills .nav-link.active, .nav-pills .show>.nav-link {
    color: #fff;
    background-color: #083672;
}
a {
    color: #083672;
    text-decoration: none;
    background-color: transparent;
    -webkit-text-decoration-skip: objects;
}
a:hover {
    color: #083672;
    text-decoration: underline;
}
.ui-datepicker-month{
    color: #f7af38 !important;
}

.ui-datepicker-year{
    color: #f7af38 !important;
}
/* Chrome, Safari, Edge, Opera */
input::-webkit-outer-spin-button,
        input::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
        }

        /* Firefox */
        input[type=number] {
        -moz-appearance: textfield;
        }

</style>
@endpush
@push('js')
<script src = "https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script>
  $(function() {
    $("#dataTable").DataTable({
        'processing': true,
        'serverSide': true,
        'serverMethod': 'post',
        "aLengthMenu": [[10,25, 50,100], [10,25, 50,100]],
        order: [
            [0, 'desc']
        ],
        "ajax": {
            "url": "{{ route('FestivalLeave') }}",
            "data": {
                "_token": "{{ csrf_token() }}"
            },
        },
        "columns": [
            {
                data: "id",
                render: function(data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1
                }
            },
            {
                data: "title",orderable:false,
            },
            {
                data: "start_date",orderable:false,
            },
            {
                data: "id",orderable:false,
                render: function(data, row, alldata) {
                  var $id = btoa(alldata.id);
                    $html = `<a href="{{url('/editleave')}}/`+$id+`">
                         <!-- <button class="btn btn-info">Update</button> -->
                         <i class="fa fa-edit" aria-hidden="true" title='Edit' style='font-size:20px'></i>
                         </a>
                      
                      <i class="fa fa-trash delete" style='font-size:20px;color:red;cursor:pointer;' title='delete banner' data-id="`+alldata.id+`" >
                      </i>`;
                    return $html;
                }
            },
        ]
    });

    $(document).on('click','.delete',function(){
      var id = $(this).data('id');
      
      swal({
        title: "Are you sure?",
        text: "You want to delete this Leave.",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      }).then((willDelete) => {
        if(willDelete)
        {
          $.ajax({
            method:'post',
            url: "{{url('/leavedelete/')}}/"+id,
            data: {id:id,
              '_token':"{{csrf_token()}}"
            },
            success : function(result){
              console.log(result);
              if(result)
              {
                swal("Leave has been deleted!", {
                  icon: "success",
                });
                $('#dataTable').DataTable().ajax.reload();
              }else{
                swal("Leave has been not deleted!", {
                  icon: "error",
                });
              }
            }
          });
          
          } else {
            swal("Your Leave is Safe!",
              {
                icon: "success",
              });
          }
        //}
      });
    });

    $(document).on('click','.leave',function(){
      var id = $(this).data('id');
      // alert('true');
      $('#modal-myvalue').val(id);
    });
  });

  $(function() {
        $( "#leave_date" ).datepicker({
        minDate: new Date(),
        dateFormat: 'dd/mm/yy',
        changeMonth: true,
        changeYear: true,
        yearRange: '-110:+29',
        language: "es",
    });
  });
</script>
@endpush


