@extends('admin.master')
@section('bodyData')
    <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="Stylesheet" type="text/css" />
	<script src="//code.jquery.com/jquery-latest.min.js"></script>
<style>
  .timepicker_div{ text-align:center; }

</style>



<script>
  $(document).ready(function(){
    $('.timepicker').mdtimepicker();
  	
  });
</script>
<style>

@import url(https://fonts.googleapis.com/css?famil	y=Roboto);
.mdtp__wrapper,
body[mdtimepicker-display="on"] {
  overflow: hidden;
}
.mdtimepicker {
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  font-family: Roboto, sans-serif;
  font-size: 14px;
/*   background-color: rgba(10, 10, 10, 0.7); */
  transition: background-color 0.28s ease;
  z-index: 100001;
}
.mdtimepicker.hidden {
  display: none;
}
.mdtimepicker.animate {
  background-color: transparent;
}
.mdtp__wrapper {
  position: absolute;
  display: flex;
  flex-direction: column;
  left: 50%;
  bottom: 24px;
  min-width: 280px;
  opacity: 1;
  user-select: none;
  border-radius: 2px;
  transform: translateX(-50%) scale(1);
  box-shadow: 0 11px 15px -7px rgba(0, 0, 0, 0.2),
    0 24px 38px 3px rgba(0, 0, 0, 0.14), 0 9px 46px 8px rgba(0, 0, 0, 0.12);
  transition: transform 0.28s ease, opacity 0.28s ease;
}
.mdtp__wrapper.animate {
  transform: translateX(-50%) scale(1.05);
  opacity: 0;
}
.mdtp__time_holder {
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  font-size: 46px;
  padding: 20px 24px;
  color: rgba(255, 255, 255, 0.5);
  text-align: center;
  background-color: #1565c0;
}
.mdtp__time_holder > span {
  display: inline-block;
  line-height: 48px;
  cursor: default;
}
.mdtp__time_holder > span:not(.mdtp__timedots):not(.mdtp__ampm) {
  cursor: pointer;
  margin: 0 4px;
}
.mdtp__time_holder .mdtp__time_h.active,
.mdtp__time_holder .mdtp__time_m.active {
  color: #fafafa;
}
.mdtp__time_holder .mdtp__ampm {
  font-size: 18px;
}
.mdtp__clock_holder {
  position: relative;
  padding: 20px;
  background-color: #fff;
}
.mdtp__clock_holder .mdtp__clock {
  position: relative;
  width: 250px;
  height: 250px;
  margin-bottom: 20px;
  border-radius: 50%;
  background-color: #eee;
}
.mdtp__clock .mdtp__am,
.mdtp__clock .mdtp__pm {
  display: block;
  position: absolute;
  bottom: -8px;
  width: 36px;
  height: 36px;
  line-height: 36px;
  text-align: center;
  cursor: pointer;
  border-radius: 50%;
  border: 1px solid rgba(0, 0, 0, 0.1);
  background: rgba(0, 0, 0, 0.05);
  transition: background-color 0.2s ease, color 0.2s;
  z-index: 3;
}
.mdtp__clock .mdtp__am {
  left: -8px;
}
.mdtp__clock .mdtp__pm {
  right: -8px;
}
.mdtp__clock .mdtp__am:hover,
.mdtp__clock .mdtp__pm:hover {
  background-color: rgba(0, 0, 0, 0.1);
}
.mdtp__clock .mdtp__am.active,
.mdtp__clock .mdtp__pm.active {
  color: #fafafa;
  background-color: #1565c0;
}
.mdtp__clock .mdtp__clock_dot {
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  padding: 4px;
  background-color: #1565c0;
  border-radius: 50%;
}
.mdtp__clock .mdtp__hour_holder,
.mdtp__clock .mdtp__minute_holder {
  position: absolute;
  top: 0;
  width: 100%;
  height: 100%;
  opacity: 1;
  transform: scale(1);
  transition: transform 0.35s cubic-bezier(0.4, 0, 0.2, 1), opacity 0.35s ease;
  overflow: hidden;
}
.mdtp__clock .mdtp__hour_holder.animate {
  transform: scale(1.2);
  opacity: 0;
}
.mdtp__clock .mdtp__minute_holder.animate {
  transform: scale(0.8);
  opacity: 0;
}
.mdtp__clock .mdtp__hour_holder.hidden,
.mdtp__clock .mdtp__minute_holder.hidden {
  display: none;
}
.mdtp__clock .mdtp__digit {
  position: absolute;
  width: 50%;
  top: 50%;
  left: 0;
  margin-top: -16px;
  transform-origin: right center;
  z-index: 1;
}
.mdtp__clock .mdtp__digit span {
  display: inline-block;
  width: 32px;
  height: 32px;
  line-height: 32px;
  margin-left: 8px;
  text-align: center;
  border-radius: 50%;
  cursor: pointer;
  transition: background-color 0.28s, color 0.14s;
}
.mdtp__clock .mdtp__digit span:hover,
.mdtp__digit.active span {
  background-color: #1565c0 !important;
  color: #fff;
  z-index: 2;
}
.mdtp__button,
.mdtp__wrapper[data-theme="blue"] .mdtp__button {
  color: #1565c0;
}
.mdtp__digit.active:before {
  content: "";
  display: block;
  position: absolute;
  top: calc(50% - 1px);
  right: 0;
  height: 2px;
  width: calc(100% - 40px);
  background-color: #1565c0;
}
.mdtp__clock .mdtp__minute_holder .mdtp__digit {
  font-size: 13px;
}
.mdtp__clock .mdtp__minute_holder .mdtp__digit:not(.marker) {
  margin-top: -6px;
  height: 12px;
}
.mdtp__clock .mdtp__minute_holder .mdtp__digit:not(.marker).active:before {
  width: calc(100% - 26px);
}
.mdtp__clock .mdtp__minute_holder .mdtp__digit:not(.marker) span {
  width: 12px;
  height: 12px;
  line-height: 12px;
  margin-left: 14px;
}
.mdtp__clock .mdtp__minute_holder .mdtp__digit.marker {
  margin-top: -12px;
}
.mdtp__clock .mdtp__minute_holder .mdtp__digit.marker.active:before {
  width: calc(100% - 34px);
}
.mdtp__clock .mdtp__minute_holder .mdtp__digit.marker span {
  width: 24px;
  height: 24px;
  line-height: 24px;
  margin-left: 10px;
}
.mdtp__digit.rotate-6 {
  transform: rotate(6deg);
}
.mdtp__digit.rotate-6 span {
  transform: rotate(-6deg);
}
.mdtp__digit.rotate-12 {
  transform: rotate(12deg);
}
.mdtp__digit.rotate-12 span {
  transform: rotate(-12deg);
}
.mdtp__digit.rotate-18 {
  transform: rotate(18deg);
}
.mdtp__digit.rotate-18 span {
  transform: rotate(-18deg);
}
.mdtp__digit.rotate-24 {
  transform: rotate(24deg);
}
.mdtp__digit.rotate-24 span {
  transform: rotate(-24deg);
}
.mdtp__digit.rotate-30 {
  transform: rotate(30deg);
}
.mdtp__digit.rotate-30 span {
  transform: rotate(-30deg);
}
.mdtp__digit.rotate-36 {
  transform: rotate(36deg);
}
.mdtp__digit.rotate-36 span {
  transform: rotate(-36deg);
}
.mdtp__digit.rotate-42 {
  transform: rotate(42deg);
}
.mdtp__digit.rotate-42 span {
  transform: rotate(-42deg);
}
.mdtp__digit.rotate-48 {
  transform: rotate(48deg);
}
.mdtp__digit.rotate-48 span {
  transform: rotate(-48deg);
}
.mdtp__digit.rotate-54 {
  transform: rotate(54deg);
}
.mdtp__digit.rotate-54 span {
  transform: rotate(-54deg);
}
.mdtp__digit.rotate-60 {
  transform: rotate(60deg);
}
.mdtp__digit.rotate-60 span {
  transform: rotate(-60deg);
}
.mdtp__digit.rotate-66 {
  transform: rotate(66deg);
}
.mdtp__digit.rotate-66 span {
  transform: rotate(-66deg);
}
.mdtp__digit.rotate-72 {
  transform: rotate(72deg);
}
.mdtp__digit.rotate-72 span {
  transform: rotate(-72deg);
}
.mdtp__digit.rotate-78 {
  transform: rotate(78deg);
}
.mdtp__digit.rotate-78 span {
  transform: rotate(-78deg);
}
.mdtp__digit.rotate-84 {
  transform: rotate(84deg);
}
.mdtp__digit.rotate-84 span {
  transform: rotate(-84deg);
}
.mdtp__digit.rotate-90 {
  transform: rotate(90deg);
}
.mdtp__digit.rotate-90 span {
  transform: rotate(-90deg);
}
.mdtp__digit.rotate-96 {
  transform: rotate(96deg);
}
.mdtp__digit.rotate-96 span {
  transform: rotate(-96deg);
}
.mdtp__digit.rotate-102 {
  transform: rotate(102deg);
}
.mdtp__digit.rotate-102 span {
  transform: rotate(-102deg);
}
.mdtp__digit.rotate-108 {
  transform: rotate(108deg);
}
.mdtp__digit.rotate-108 span {
  transform: rotate(-108deg);
}
.mdtp__digit.rotate-114 {
  transform: rotate(114deg);
}
.mdtp__digit.rotate-114 span {
  transform: rotate(-114deg);
}
.mdtp__digit.rotate-120 {
  transform: rotate(120deg);
}
.mdtp__digit.rotate-120 span {
  transform: rotate(-120deg);
}
.mdtp__digit.rotate-126 {
  transform: rotate(126deg);
}
.mdtp__digit.rotate-126 span {
  transform: rotate(-126deg);
}
.mdtp__digit.rotate-132 {
  transform: rotate(132deg);
}
.mdtp__digit.rotate-132 span {
  transform: rotate(-132deg);
}
.mdtp__digit.rotate-138 {
  transform: rotate(138deg);
}
.mdtp__digit.rotate-138 span {
  transform: rotate(-138deg);
}
.mdtp__digit.rotate-144 {
  transform: rotate(144deg);
}
.mdtp__digit.rotate-144 span {
  transform: rotate(-144deg);
}
.mdtp__digit.rotate-150 {
  transform: rotate(150deg);
}
.mdtp__digit.rotate-150 span {
  transform: rotate(-150deg);
}
.mdtp__digit.rotate-156 {
  transform: rotate(156deg);
}
.mdtp__digit.rotate-156 span {
  transform: rotate(-156deg);
}
.mdtp__digit.rotate-162 {
  transform: rotate(162deg);
}
.mdtp__digit.rotate-162 span {
  transform: rotate(-162deg);
}
.mdtp__digit.rotate-168 {
  transform: rotate(168deg);
}
.mdtp__digit.rotate-168 span {
  transform: rotate(-168deg);
}
.mdtp__digit.rotate-174 {
  transform: rotate(174deg);
}
.mdtp__digit.rotate-174 span {
  transform: rotate(-174deg);
}
.mdtp__digit.rotate-180 {
  transform: rotate(180deg);
}
.mdtp__digit.rotate-180 span {
  transform: rotate(-180deg);
}
.mdtp__digit.rotate-186 {
  transform: rotate(186deg);
}
.mdtp__digit.rotate-186 span {
  transform: rotate(-186deg);
}
.mdtp__digit.rotate-192 {
  transform: rotate(192deg);
}
.mdtp__digit.rotate-192 span {
  transform: rotate(-192deg);
}
.mdtp__digit.rotate-198 {
  transform: rotate(198deg);
}
.mdtp__digit.rotate-198 span {
  transform: rotate(-198deg);
}
.mdtp__digit.rotate-204 {
  transform: rotate(204deg);
}
.mdtp__digit.rotate-204 span {
  transform: rotate(-204deg);
}
.mdtp__digit.rotate-210 {
  transform: rotate(210deg);
}
.mdtp__digit.rotate-210 span {
  transform: rotate(-210deg);
}
.mdtp__digit.rotate-216 {
  transform: rotate(216deg);
}
.mdtp__digit.rotate-216 span {
  transform: rotate(-216deg);
}
.mdtp__digit.rotate-222 {
  transform: rotate(222deg);
}
.mdtp__digit.rotate-222 span {
  transform: rotate(-222deg);
}
.mdtp__digit.rotate-228 {
  transform: rotate(228deg);
}
.mdtp__digit.rotate-228 span {
  transform: rotate(-228deg);
}
.mdtp__digit.rotate-234 {
  transform: rotate(234deg);
}
.mdtp__digit.rotate-234 span {
  transform: rotate(-234deg);
}
.mdtp__digit.rotate-240 {
  transform: rotate(240deg);
}
.mdtp__digit.rotate-240 span {
  transform: rotate(-240deg);
}
.mdtp__digit.rotate-246 {
  transform: rotate(246deg);
}
.mdtp__digit.rotate-246 span {
  transform: rotate(-246deg);
}
.mdtp__digit.rotate-252 {
  transform: rotate(252deg);
}
.mdtp__digit.rotate-252 span {
  transform: rotate(-252deg);
}
.mdtp__digit.rotate-258 {
  transform: rotate(258deg);
}
.mdtp__digit.rotate-258 span {
  transform: rotate(-258deg);
}
.mdtp__digit.rotate-264 {
  transform: rotate(264deg);
}
.mdtp__digit.rotate-264 span {
  transform: rotate(-264deg);
}
.mdtp__digit.rotate-270 {
  transform: rotate(270deg);
}
.mdtp__digit.rotate-270 span {
  transform: rotate(-270deg);
}
.mdtp__digit.rotate-276 {
  transform: rotate(276deg);
}
.mdtp__digit.rotate-276 span {
  transform: rotate(-276deg);
}
.mdtp__digit.rotate-282 {
  transform: rotate(282deg);
}
.mdtp__digit.rotate-282 span {
  transform: rotate(-282deg);
}
.mdtp__digit.rotate-288 {
  transform: rotate(288deg);
}
.mdtp__digit.rotate-288 span {
  transform: rotate(-288deg);
}
.mdtp__digit.rotate-294 {
  transform: rotate(294deg);
}
.mdtp__digit.rotate-294 span {
  transform: rotate(-294deg);
}
.mdtp__digit.rotate-300 {
  transform: rotate(300deg);
}
.mdtp__digit.rotate-300 span {
  transform: rotate(-300deg);
}
.mdtp__digit.rotate-306 {
  transform: rotate(306deg);
}
.mdtp__digit.rotate-306 span {
  transform: rotate(-306deg);
}
.mdtp__digit.rotate-312 {
  transform: rotate(312deg);
}
.mdtp__digit.rotate-312 span {
  transform: rotate(-312deg);
}
.mdtp__digit.rotate-318 {
  transform: rotate(318deg);
}
.mdtp__digit.rotate-318 span {
  transform: rotate(-318deg);
}
.mdtp__digit.rotate-324 {
  transform: rotate(324deg);
}
.mdtp__digit.rotate-324 span {
  transform: rotate(-324deg);
}
.mdtp__digit.rotate-330 {
  transform: rotate(330deg);
}
.mdtp__digit.rotate-330 span {
  transform: rotate(-330deg);
}
.mdtp__digit.rotate-336 {
  transform: rotate(336deg);
}
.mdtp__digit.rotate-336 span {
  transform: rotate(-336deg);
}
.mdtp__digit.rotate-342 {
  transform: rotate(342deg);
}
.mdtp__digit.rotate-342 span {
  transform: rotate(-342deg);
}
.mdtp__digit.rotate-348 {
  transform: rotate(348deg);
}
.mdtp__digit.rotate-348 span {
  transform: rotate(-348deg);
}
.mdtp__digit.rotate-354 {
  transform: rotate(354deg);
}
.mdtp__digit.rotate-354 span {
  transform: rotate(-354deg);
}
.mdtp__digit.rotate-360 {
  transform: rotate(360deg);
}
.mdtp__digit.rotate-360 span {
  transform: rotate(-360deg);
}
.mdtp__buttons {
  margin: 0 -10px -10px;
  text-align: right;
}
.mdtp__button {
  display: inline-block;
  padding: 0 16px;
  min-width: 50px;
  text-align: center;
  text-transform: uppercase;
  line-height: 32px;
  font-weight: 500;
  cursor: pointer;
}
.mdtp__button:hover {
  background-color: #e0e0e0;
}
.mdtp__wrapper[data-theme="blue"] .mdtp__clock .mdtp__am.active,
.mdtp__wrapper[data-theme="blue"] .mdtp__clock .mdtp__clock_dot,
.mdtp__wrapper[data-theme="blue"] .mdtp__clock .mdtp__pm.active,
.mdtp__wrapper[data-theme="blue"] .mdtp__time_holder {
  background-color: #1565c0;
}
.mdtp__wrapper[data-theme="blue"] .mdtp__clock .mdtp__digit span:hover,
.mdtp__wrapper[data-theme="blue"] .mdtp__digit.active span {
  background-color: #1565c0 !important;
}
.mdtp__wrapper[data-theme="blue"] .mdtp__digit.active:before {
  background-color: #1565c0;
}
.mdtp__wrapper[data-theme="red"] .mdtp__clock .mdtp__am.active,
.mdtp__wrapper[data-theme="red"] .mdtp__clock .mdtp__clock_dot,
.mdtp__wrapper[data-theme="red"] .mdtp__clock .mdtp__pm.active,
.mdtp__wrapper[data-theme="red"] .mdtp__time_holder {
  background-color: #c62828;
}
.mdtp__wrapper[data-theme="red"] .mdtp__clock .mdtp__digit span:hover,
.mdtp__wrapper[data-theme="red"] .mdtp__digit.active span {
  background-color: #c62828 !important;
}
.mdtp__wrapper[data-theme="red"] .mdtp__digit.active:before {
  background-color: #c62828;
}
.mdtp__wrapper[data-theme="red"] .mdtp__button {
  color: #c62828;
}
.mdtp__wrapper[data-theme="purple"] .mdtp__clock .mdtp__am.active,
.mdtp__wrapper[data-theme="purple"] .mdtp__clock .mdtp__clock_dot,
.mdtp__wrapper[data-theme="purple"] .mdtp__clock .mdtp__pm.active,
.mdtp__wrapper[data-theme="purple"] .mdtp__time_holder {
  background-color: #6a1b9a;
}
.mdtp__wrapper[data-theme="purple"] .mdtp__clock .mdtp__digit span:hover,
.mdtp__wrapper[data-theme="purple"] .mdtp__digit.active span {
  background-color: #6a1b9a !important;
}
.mdtp__wrapper[data-theme="purple"] .mdtp__digit.active:before {
  background-color: #6a1b9a;
}
.mdtp__wrapper[data-theme="purple"] .mdtp__button {
  color: #6a1b9a;
}
.mdtp__wrapper[data-theme="indigo"] .mdtp__clock .mdtp__am.active,
.mdtp__wrapper[data-theme="indigo"] .mdtp__clock .mdtp__clock_dot,
.mdtp__wrapper[data-theme="indigo"] .mdtp__clock .mdtp__pm.active,
.mdtp__wrapper[data-theme="indigo"] .mdtp__time_holder {
  background-color: #283593;
}
.mdtp__wrapper[data-theme="indigo"] .mdtp__clock .mdtp__digit span:hover,
.mdtp__wrapper[data-theme="indigo"] .mdtp__digit.active span {
  background-color: #283593 !important;
}
.mdtp__wrapper[data-theme="indigo"] .mdtp__digit.active:before {
  background-color: #283593;
}
.mdtp__wrapper[data-theme="indigo"] .mdtp__button {
  color: #283593;
}
.mdtp__wrapper[data-theme="teal"] .mdtp__clock .mdtp__am.active,
.mdtp__wrapper[data-theme="teal"] .mdtp__clock .mdtp__clock_dot,
.mdtp__wrapper[data-theme="teal"] .mdtp__clock .mdtp__pm.active,
.mdtp__wrapper[data-theme="teal"] .mdtp__time_holder {
  background-color: #00695c;
}
.mdtp__wrapper[data-theme="teal"] .mdtp__clock .mdtp__digit span:hover,
.mdtp__wrapper[data-theme="teal"] .mdtp__digit.active span {
  background-color: #00695c !important;
}
.mdtp__wrapper[data-theme="teal"] .mdtp__digit.active:before {
  background-color: #00695c;
}
.mdtp__wrapper[data-theme="teal"] .mdtp__button {
  color: #00695c;
}
.mdtp__wrapper[data-theme="green"] .mdtp__clock .mdtp__am.active,
.mdtp__wrapper[data-theme="green"] .mdtp__clock .mdtp__clock_dot,
.mdtp__wrapper[data-theme="green"] .mdtp__clock .mdtp__pm.active,
.mdtp__wrapper[data-theme="green"] .mdtp__time_holder {
  background-color: #2e7d32;
}
.mdtp__wrapper[data-theme="green"] .mdtp__clock .mdtp__digit span:hover,
.mdtp__wrapper[data-theme="green"] .mdtp__digit.active span {
  background-color: #2e7d32 !important;
}
.mdtp__wrapper[data-theme="green"] .mdtp__digit.active:before {
  background-color: #2e7d32;
}
.mdtp__wrapper[data-theme="green"] .mdtp__button {
  color: #2e7d32;
}
@media (max-height: 360px) {
  .mdtp__wrapper {
    flex-direction: row;
    bottom: 8px;
  }
  .mdtp__time_holder {
    width: 160px;
    padding: 20px;
  }
  .mdtp__clock_holder {
    padding: 16px;
  }
  .mdtp__clock .mdtp__am,
  .mdtp__clock .mdtp__pm {
    bottom: -4px;
  }
  .mdtp__clock .mdtp__am {
    left: -4px;
  }
  .mdtp__clock .mdtp__pm {
    right: -4px;
  }
}
@media (max-height: 320px) {
  .mdtp__wrapper {
    bottom: 0;
  }
}

</style>
<script>
if("undefined"==typeof jQuery)throw new Error("MDTimePicker: This plugin requires jQuery");+function(e){var t="mdtimepicker",i=120,s=90,n=360,r=30,a=6,o=[9,112,113,114,115,116,117,118,119,120,121,122,123],c=function(t,i){this.hour=t,this.minute=i,this.format=function(t,i){var s=this,n=(t.match(/h/g)||[]).length>1;return e.trim(t.replace(/(hh|h|mm|ss|tt|t)/g,function(e){switch(e.toLowerCase()){case"h":var t=s.getHour(!0);return i&&10>t?"0"+t:t;case"hh":return s.hour<10?"0"+s.hour:s.hour;case"mm":return s.minute<10?"0"+s.minute:s.minute;case"ss":return"00";case"t":return n?"":s.getT().toLowerCase();case"tt":return n?"":s.getT()}}))},this.setHour=function(e){this.hour=e},this.getHour=function(e){return e?0===this.hour||12===this.hour?12:this.hour%12:this.hour},this.invert=function(){"AM"===this.getT()?this.setHour(this.getHour()+12):this.setHour(this.getHour()-12)},this.setMinutes=function(e){this.minute=e},this.getMinutes=function(){return this.minute},this.getT=function(){return this.hour<12?"AM":"PM"}},d=function(t,i){var s=this;this.visible=!1,this.activeView="hours",this.hTimeout=null,this.mTimeout=null,this.input=e(t),this.config=i,this.time=new c(0,0),this.selected=new c(0,0),this.timepicker={overlay:e('<div class="mdtimepicker hidden"></div>'),wrapper:e('<div class="mdtp__wrapper"></div>'),timeHolder:{wrapper:e('<section class="mdtp__time_holder"></section>'),hour:e('<span class="mdtp__time_h">12</span>'),dots:e('<span class="mdtp__timedots">:</span>'),minute:e('<span class="mdtp__time_m">00</span>'),am_pm:e('<span class="mdtp__ampm">AM</span>')},clockHolder:{wrapper:e('<section class="mdtp__clock_holder"></section>'),am:e('<span class="mdtp__am">AM</span>'),pm:e('<span class="mdtp__pm">PM</span>'),clock:{wrapper:e('<div class="mdtp__clock"></div>'),dot:e('<span class="mdtp__clock_dot"></span>'),hours:e('<div class="mdtp__hour_holder"></div>'),minutes:e('<div class="mdtp__minute_holder"></div>')},buttonsHolder:{wrapper:e('<div class="mdtp__buttons">'),btnOk:e('<span class="mdtp__button ok">Ok</span>'),btnCancel:e('<span class="mdtp__button cancel">Cancel</span>')}}};var n=s.timepicker;if(s.setup(n).appendTo("body"),n.clockHolder.am.click(function(){"AM"!==s.selected.getT()&&s.setT("am")}),n.clockHolder.pm.click(function(){"PM"!==s.selected.getT()&&s.setT("pm")}),n.timeHolder.hour.click(function(){"hours"!==s.activeView&&s.switchView("hours")}),n.timeHolder.minute.click(function(){"minutes"!==s.activeView&&s.switchView("minutes")}),n.clockHolder.buttonsHolder.btnOk.click(function(){s.setValue(s.selected);var t=s.getFormattedTime();s.input.trigger(e.Event("timechanged",{time:t.time,value:t.value})).trigger("onchange"),s.hide()}),n.clockHolder.buttonsHolder.btnCancel.click(function(){s.hide()}),s.input.on("keydown",function(e){return 13===e.keyCode&&s.show(),!(o.indexOf(e.which)<0&&s.config.readOnly)}).on("click",function(){s.show()}).prop("readonly",s.config.readOnly),""!==s.input.val()){var r=s.parseTime(s.input.val(),s.config.format);s.setValue(r)}else{var r=s.getSystemTime();s.time=new c(r.hour,r.minute)}s.resetSelected(),s.switchView(s.activeView)};d.prototype={constructor:d,setup:function(t){if("undefined"==typeof t)throw new Error("Expecting a value.");var o=this,c=t.overlay,d=t.wrapper,u=t.timeHolder,m=t.clockHolder;u.wrapper.append(u.hour).append(u.dots).append(u.minute).append(u.am_pm).appendTo(d);for(var l=0;12>l;l++){var p=l+1,h=(i+l*r)%n,f=e('<div class="mdtp__digit rotate-'+h+'" data-hour="'+p+'"><span>'+p+"</span></div>");f.find("span").click(function(){var t=parseInt(e(this).parent().data("hour")),i=o.selected.getT(),s=(t+("PM"===i&&12>t||"AM"===i&&12===t?12:0))%24;o.setHour(s),o.switchView("minutes")}),m.clock.hours.append(f)}for(var l=0;60>l;l++){var v=10>l?"0"+l:l,h=(s+l*a)%n,k=e('<div class="mdtp__digit rotate-'+h+'" data-minute="'+l+'"></div>');l%5===0?k.addClass("marker").html("<span>"+v+"</span>"):k.html("<span></span>"),k.find("span").click(function(){o.setMinute(e(this).parent().data("minute"))}),m.clock.minutes.append(k)}switch(m.clock.wrapper.append(m.am).append(m.pm).append(m.clock.dot).append(m.clock.hours).append(m.clock.minutes).appendTo(m.wrapper),m.buttonsHolder.wrapper.append(m.buttonsHolder.btnCancel).append(m.buttonsHolder.btnOk).appendTo(m.wrapper),m.wrapper.appendTo(d),o.config.theme){case"red":case"blue":case"green":case"purple":case"indigo":case"teal":d.attr("data-theme",o.config.theme);break;default:d.attr("data-theme",e.fn.mdtimepicker.defaults.theme)}return d.appendTo(c),c},setHour:function(t){if("undefined"==typeof t)throw new Error("Expecting a value.");var i=this;this.selected.setHour(t),this.timepicker.timeHolder.hour.text(this.selected.getHour(!0)),this.timepicker.clockHolder.clock.hours.children("div").each(function(t,s){var n=e(s),r=n.data("hour");n[r===i.selected.getHour(!0)?"addClass":"removeClass"]("active")})},setMinute:function(t){if("undefined"==typeof t)throw new Error("Expecting a value.");this.selected.setMinutes(t),this.timepicker.timeHolder.minute.text(10>t?"0"+t:t),this.timepicker.clockHolder.clock.minutes.children("div").each(function(i,s){var n=e(s),r=n.data("minute");n[r===t?"addClass":"removeClass"]("active")})},setT:function(e){if("undefined"==typeof e)throw new Error("Expecting a value.");this.selected.getT()!==e.toUpperCase()&&this.selected.invert();var t=this.selected.getT();this.timepicker.timeHolder.am_pm.text(t),this.timepicker.clockHolder.am["AM"===t?"addClass":"removeClass"]("active"),this.timepicker.clockHolder.pm["PM"===t?"addClass":"removeClass"]("active")},setValue:function(e){if("undefined"==typeof e)throw new Error("Expecting a value.");var t="string"==typeof e?this.parseTime(e,this.config.format):e;this.time=new c(t.hour,t.minute);var i=this.getFormattedTime();this.input.val(i.value).attr("data-time",i.time).attr("value",i.value)},resetSelected:function(){this.setHour(this.time.hour),this.setMinute(this.time.minute),this.setT(this.time.getT())},getFormattedTime:function(){var e=this.time.format(this.config.timeFormat,!1),t=this.time.format(this.config.format,this.config.hourPadding);return{time:e,value:t}},getSystemTime:function(){var e=new Date;return new c(e.getHours(),e.getMinutes())},parseTime:function(e,t){var i=this,s="undefined"==typeof t?i.config.format:t,n=(s.match(/h/g)||[]).length,r=n>1,a=((s.match(/m/g)||[]).length,(s.match(/t/g)||[]).length),o=e.length,d=s.indexOf("h"),u=s.lastIndexOf("h"),m="",l="",p="";if(i.config.hourPadding||r)m=e.substr(d,2);else{var h=s.substring(d-1,d),f=s.substring(u+1,u+2);m=u===s.length-1?e.substring(e.indexOf(h,d-1)+1,o):0===d?e.substring(0,e.indexOf(f,d)):e.substring(e.indexOf(h,d-1)+1,e.indexOf(f,d+1))}s=s.replace(/(hh|h)/g,m);var v=s.indexOf("m"),k=s.lastIndexOf("m"),g=s.indexOf("t"),w=s.substring(v-1,v);s.substring(k+1,k+2);l=k===s.length-1?e.substring(e.indexOf(w,v-1)+1,o):0===v?e.substring(0,2):e.substr(v,2),p=r?parseInt(m)>11?a>1?"PM":"pm":a>1?"AM":"am":e.substr(g,2);var H="pm"===p.toLowerCase(),_=new c(parseInt(m),parseInt(l));return(H&&parseInt(m)<12||!H&&12===parseInt(m))&&_.invert(),_},switchView:function(e){var t=this,i=this.timepicker,s=350;"hours"!==e&&"minutes"!==e||(t.activeView=e,i.timeHolder.hour["hours"===e?"addClass":"removeClass"]("active"),i.timeHolder.minute["hours"===e?"removeClass":"addClass"]("active"),i.clockHolder.clock.hours.addClass("animate"),"hours"===e&&i.clockHolder.clock.hours.removeClass("hidden"),clearTimeout(t.hTimeout),t.hTimeout=setTimeout(function(){"hours"!==e&&i.clockHolder.clock.hours.addClass("hidden"),i.clockHolder.clock.hours.removeClass("animate")},"hours"===e?20:s),i.clockHolder.clock.minutes.addClass("animate"),"minutes"===e&&i.clockHolder.clock.minutes.removeClass("hidden"),clearTimeout(t.mTimeout),t.mTimeout=setTimeout(function(){"minutes"!==e&&i.clockHolder.clock.minutes.addClass("hidden"),i.clockHolder.clock.minutes.removeClass("animate")},"minutes"===e?20:s))},show:function(){var t=this;if(""===t.input.val()){var i=t.getSystemTime();this.time=new c(i.hour,i.minute)}t.resetSelected(),e("body").attr("mdtimepicker-display","on"),t.timepicker.wrapper.addClass("animate"),t.timepicker.overlay.removeClass("hidden").addClass("animate"),setTimeout(function(){t.timepicker.overlay.removeClass("animate"),t.timepicker.wrapper.removeClass("animate"),t.visible=!0,t.input.blur()},10)},hide:function(){var t=this;t.timepicker.overlay.addClass("animate"),t.timepicker.wrapper.addClass("animate"),setTimeout(function(){t.switchView("hours"),t.timepicker.overlay.addClass("hidden").removeClass("animate"),t.timepicker.wrapper.removeClass("animate"),e("body").removeAttr("mdtimepicker-display"),t.visible=!1,t.input.focus()},300)},destroy:function(){var e=this;e.input.removeData(t).unbind("keydown").unbind("click").removeProp("readonly"),e.timepicker.overlay.remove()}},e.fn.mdtimepicker=function(i){return e(this).each(function(){var s=this,n=e(this),r=e(this).data(t);options=e.extend({},e.fn.mdtimepicker.defaults,n.data(),"object"==typeof i&&i),r||n.data(t,r=new d(s,options)),"string"==typeof i&&r[i](),e(document).on("keydown",function(e){27===e.keyCode&&r.visible&&r.hide()})})},e.fn.mdtimepicker.defaults={timeFormat:"hh:mm:ss.000",format:"h:mm tt",theme:"blue",readOnly:!0,hourPadding:!1}}(jQuery);
</script>
<script>

function countrychange(r)
                { 
                    $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var cat_id = r.value;
            //console.log(cat_id);

                 $.ajax({
                   
                       url:"{{ route('getState') }}?id=" + cat_id ,
                       type:"POST",
                       data: {
                           cat_id: cat_id
                        },
                      
                       success:function (data) {
                       alert(data);
                        $('#state_id').empty();
                        // console.log(data.cities);

                        $.each(data.states,function(index,states){
                          //  console.log(states);
                    
                            $('#state_id').append('<option value="'+index+'">'+states+'</option>');
                        })

                       }
                   })
             }
 function statechange(r)
                { 
                    $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var cat_id = r.value;
            //console.log(cat_id);

                 $.ajax({
                   
                       url:"{{ route('getCity') }}?id=" + cat_id ,
                       type:"POST",
                       data: {
                           cat_id: cat_id
                        },
                      
                       success:function (data) {
                       // console.log("i m in sucess");
                        $('#city_id').empty();
                        // console.log(data.cities);

                        $.each(data.cities,function(index,cities){
                            // console.log(cities);
                            
                            $('#city_id').append('<option value="'+index+'">'+cities+'</option>');
                        })

                       }
                   })
             }
                </script>


<section class="forms-section" style='min-height:500px'>
<div class="outer-w3-agile mt-3">
<a href='/allinquiry'><button class='btn btn-primary'>Show Inquiry</button></a>
        <h4 class="tittle-w3-agileits mb-4">Inquiry Update</h4>
        
  
          
        <form action="{{url('adminEditInquiry')}}" method="post">
        @csrf
       <div class="form-row">
               
                <div class="form-group col-md-6">
                    <label for="inputEmail4">First Name</label>
    <input type="text" class="form-control" pattern='[A-za-z]{2,30}' id="inputEmail4" placeholder="Enter First Name" required="" name='first_name' value='<?php ucfirst(print_r($inquiryData[0]->first_name))?>'>
                </div>
                <div class="form-group col-md-6">
                   <label for="inputEmail4">Last Name</label>
                   <input type="text" class="form-control" pattern='[A-za-z]{2,30}' id="inputEmail4" placeholder="Enter  Last Name" required="" name='last_name' value='<?php ucfirst(print_r($inquiryData[0]->last_name))?>'>
               </div>
           </div>

    <input type="hidden" class="form-control" name='id' value='<?php echo $inquiryData[0]->id?>'>
          

          <div class="form-row">
               
               <div class="form-group col-md-6">
                   <label for="inputEmail4">Mobile Number</label>
                   <input type="text" class="form-control" id="inputEmail4" placeholder="Enter Mobile Number" required="" name='mobile_no' value='<?php print_r($inquiryData[0]->mobile_no)?>' pattern='[0-9]{10}'>
               </div>
               <div class="form-group col-md-6">
                   <label for="inputEmail4">Whats App Number</label>
                   <input type="text" class="form-control" id="inputEmail4" placeholder="Enter Whats Up Number" required="" name='whatsup_no' value='<?php print_r($inquiryData[0]->whatsup_no)?>' pattern='[0-9]{10}'>
               </div>
          </div>
          <div class='form-row'>
           <div class="form-group col-md-6">
                   <label for="inputEmail4">Inquiry Status</label>
            <?php $temp =$inquiryData[0]->inq_status;echo $temp;?>
                   <select id="inputState" class="form-control" name='inq_status' required>
                  
                   
                   <option value='new' <?php if($temp == 'new') {?>selected<?php }?>>New</option>
                   <option value='follow' <?php if($temp == 'follow') {?>selected<?php }?>>Follow</option>
                   <option value='done' <?php if($temp == 'done') {?>selected<?php }?>>Done</option>
                   </select>
          </div>
       
       
           
         
        
             
               <div class="form-group col-md-6">
                   <label for="inputEmail4">Email</label>
                   <input type="email" class="form-control" required="" name='email' placeholder='Enter Name' value='<?php echo $inquiryData[0]->email  ?>'>
               </div>
               
               </div>
        <div class='form-row'>
               <div class="form-group col-md-6">
                   <label for="inputEmail4">Inquiry Handle By</label>
                   <select id="inputState" class="form-control" name='emp_handle_by_fk'>
                   <?php $tempselected = $inquiryData[0]->emp_handle_by_fk?>
                   <?php foreach ($users as $u) {
                      
                       $temp = $u->id;

                   ?>
                       
        <option value='<?php echo $temp;?>' <?php if ($temp == $tempselected) echo ' selected="selected"';?>>
        <?php echo $u->name." ".$u->surname ?>
                      </option>

        
                   
                   <?php } ?>
                   </select>
               </div></div>
               
            <div class="form-row">
               
               <div class="form-group col-md-6">
                   <label for="inputEmail4">Appointment Date</label>
                
                		<input type="date" id="datetimepicker" name='appointment_date' class="form-control" value='<?php echo date("Y-m-d", strtotime($inquiryData[0]->appointment_date))?>' />
               </div>
               
            <div class="form-group col-md-6">
            <?php //data-time="06:36:00.000" value="6:36 AM" 
           
             $trmpt =  date("H:i a", strtotime($inquiryData[0]->appointment_date));
             //echo $trmpt;
            		
            ?>
             <label for="inputEmail4">Appointment Time</label>
          	<div class="timepicker_div"> 
    <input type="text"  class="form-control timepicker" placeholder="Time" name='app_time' required='' value='<?php echo $trmpt?>'>
</div></div>
            
        </div>
        
        <div class="form-group col-md-6">
                   <label for="inputEmail4">Country</label>
                   <select id="inputState" class="form-control" name='country_id' id='country_id' required onchange='countrychange(this)'>
                   <?php $sel = $inquiryData[0]->country_id ;?>
                   <?php  foreach($country as $c) {?>
                   <option id='<?php echo $c->id?>' <?php if($c->id == $sel) { ?> selected <?php } ?> value='<?php echo $c->id?>'><?php echo $c->name ?></option>
                   <?php } ?>
                   </select>
               </div>

             
            <div class="form-row">
               
              <?php 
              		$sname = DB::table('states')->where('id',$inquiryData[0]->state_id)->get();
                    $sname = $sname[0]->name;
              ?>
            
            
             <div class="form-group col-md-6">
                   <label for="inputEmail4">State</label>
                    <?php $sel1 = $inquiryData[0]->state_id ;?>
                   <select id='state_id'  class="form-control" name='state_id' required onchange='statechange(this)'>
                 		<option id='<?php echo $sel1?>' selected value='<?php echo $sel1?>'><?php echo $sname ?></option>
                   
                   </select>
               </div>
            
            
            	<?php 
              		$cname = DB::table('cities')->where('id',$inquiryData[0]->city_id)->get();
                    $cname = $cname[0]->name;
              ?>
             <div class="form-group col-md-6">
                   <label for="inputEmail4">City</label>
                   <?php $sel1 = $inquiryData[0]->city_id ;?>
                   <select class="form-control" name='city_id' id='city_id' required >
                 	  <option id='<?php echo $sel1?>' selected value='<?php echo $sel1?>'><?php echo $cname ?></option>

                   </select>
               </div>

             </div>

         
            <button type="submit" class="btn btn-primary">Update Inquiry</button>
        </form>
    </div>

</section>

@endsection
