@extends('admin.master')
@section('bodyData')

@if ($message = Session::get('EmployeeLeaveUpdated'))
<div class="alert alert-success alert-block">
	<button type="button" class="close" data-dismiss="alert">×</button>	
        <strong>{{ $message }}</strong>
</div>
@endif


<div class="outer-w3-agile mt-3">
    <div class='row'>
      <div class='col-md-6'>
        <div class="btn btn-primary"><i class="fa fa-list mr-2"></i>Blog list</div>
      </div>
      <div class='col-md-6 text-right'>
        <a class="btn btn-primary" href="{{ route('blog-add')}}"><i class="fa fa-plus mr-2"></i>Add Blog</a>
      </div>
    </div>
<br>
{{-- <div class="row"> --}}
  <div class="table-responsive">
    <table class="table table-hover" style="width: 100%;" id="dataTable">
      <thead class="thead-dark">
        <tr>
            <th>#</th>
            <th>Title</th>
            <th>Content</th>
            <th>Action</th>
        </tr>
      </thead>
    </table>
  </div>
    
    </div>
@endsection
@push('css')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css">
@endpush
@push('js')
<script>
  $(function() {
    $("#dataTable").DataTable({
        'processing': true,
        'serverSide': true,
        'serverMethod': 'post',
        "aLengthMenu": [[10,25, 50,100], [10,25, 50,100]],
        order: [
            [0, 'desc']
        ],
        "ajax": {
            "url": "{{ route('blog-list') }}",
            "data": {
                "_token": "{{ csrf_token() }}"
            },
        },
        "columns": [
            {
                data: "id",
                render: function(data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1
                }

            },
            {
                data: "title"
            },
            {
                data: "content",
                render: function(data, type, row, meta) {
                  if(data){
                      return (data.length > 200)?data.substring(0, 300)+'...':data;
                  } else {
                      return '';
                  }
                }
            },
            {
                data: "id",orderable:false,
                render: function(data, row, alldata) {
                  var $id = btoa(alldata.id);
                    $html = `<a href="{{url('blogs/edit')}}/`+$id+`">
                         <!-- <button class="btn btn-info">Update</button> -->
                         <i class="fa fa-edit" aria-hidden="true" title='Edit' style='font-size:20px'></i>
                         </a>
                      
                      <i class="fa fa-trash delete" style='font-size:20px;color:red;cursor:pointer;' title='delete' data-id="`+alldata.id+`" >
                      </i>`;
                    return $html;
                }
            },
        ],
        "columnDefs": [ {
          'targets': [0,1,2,3], /* table column index */
          'orderable': false, /* true or false */
       }]
    });

    $(document).on("click", '.delete', function(event) { 
    var id =$(this).attr('data-id');
    //alert(id);
    swal({
      title: "Are you sure?",
      text: "You Want To Delete This Record ,You Will Not Be Able To Recover This Record!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {

        $.ajax({
          url: "{{ url('blogs/delete') }}/"+id,
          type: 'get',
          dataType: 'json',
        success:function($data){
          if($data == true)
          {
            swal("Record has been deleted!", {
              icon: "success",
            }).then((result) => {
                $('#dataTable').DataTable().ajax.reload(null,false);
            });
            //location.reload('#dataTable');
          }else{
            swal("Record has been not deleted!", {
              icon: "error",
            });
          }
    
        }
      });


      } else {
        swal("Your Record is Safe!");
      }
    });
  });

    $(document).on('click','.leave',function(){
      var id = $(this).data('id');
      // alert('true');
      $('#modal-myvalue').val(id);
    });
  });
</script>
@endpush
