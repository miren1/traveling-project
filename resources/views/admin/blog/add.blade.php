@extends('admin.master')
@section('bodyData')
<section class="forms-section" style='min-height:500px'>
<div class="outer-w3-agile mt-3">
        <h4 class="tittle-w3-agileits mb-4">@if(isset($blog))Edit @else Add @endif Blog</h4>
       
        <form id="BlogForm" method="POST"  action="{{ route('blog-store')}}" enctype="multipart/form-data">
        @csrf
            <div class="row">
               @if(isset($blog))
                    <input type="hidden" id="id" name="id" value="@if(isset($blog)){{$blog->id}} @endif"/>
                @endif
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text" class="form-control" placeholder="Enter Title" name="title" id="title" required data-bv-notempty-message="The Title is required" value="@if(isset($blog)){{$blog->title}}@endif">
                    </div>
                    <div class="form-group">
                        <label for="title">Tag</label>
                        <input type="text" placeholder="Enter Tag" class="form-control" name="tag" id="tag" required data-bv-notempty-message="The Tag is required" value="@if(isset($blog)){{$blog->tag}}@endif">
                    </div>
                    <div class="form-group">
                        <label for="title">Slug</label>
                        <input type="text" class="form-control" name="slug" id="slug"  value="@if(isset($blog)){{$blog->slug}}@endif" data-bv-stringlength="true"
                        data-bv-stringlength-min="3"
                        data-bv-stringlength-max="20"
                        data-bv-stringlength-message="Slug minimum length is 3 and maxmum length is 20"
                        data-bv-regexp="true"
                        data-bv-regexp-regexp="^[a-z0-9^-]*$"
                        data-bv-regexp-message="The Slug can consist of lower case  alphabetical characters and no spaces "
                        data-bv-remote-url = "{{ url('blogs/checkslug') }}/@if(isset($blog)){{$blog->id}}@endif"
                        data-bv-remote = "true"
                        data-bv-remote-type = "GET"   
                        data-bv-remote-message="Opps ! Slug Already Used." placeholder="Enter Slug" required="" data-bv-notempty-message="The Slug is required">
                    </div>
                    <div class="form-group">
                        <label for="type">Select Ads</label><br>
                        <select class="form-control" name="ads[]" id="ads" required data-bv-notempty-message="The Ads is required" multiple="multiple">
                            <option value="">Select ads</option>
                            @foreach($ads_details as $value)
                                @if(isset($blog))
                                    <option value="{{$value->id}}" @foreach($blog->blogs_ads as $key) @if(isset($blog) && $value->id==$key->ads_id) selected @endif  @endforeach/>{{$value->title}}</option>
                                @else
                                    <option value="{{$value->id}}"/>{{$value->title}}</option>
                                @endif 
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        @if(isset($blog->thumbnail_image))
                        <small>
                            <img src="{{$blog->url}}/{{$blog->thumbnail_image}}" style="margin-top: 50px; width:200px;">
                        </small>
                        @endif
                    </div>
                    <label><b>Details for Search Engine Optimization</b></label>
                    {{-- <div class="form-group">
                        <label for="seo_title">Title</label>
                        <input type="text" class="form-control" name="seo_title" id="seo_title" required data-bv-notempty-message="The Title is required" value="@if(isset($blog)){{$blog->seo_title}}@endif">
                    </div> --}}
                    <div class="form-group">
                        <label for="title">Keywords</label>
                        <input type="text" class="form-control" name="seo_keywords" id="seo_keywords" required data-bv-notempty-message="The Keywords is required" value="@if(isset($blog)){{$blog->seo_keywords}}@endif">
                    </div>
                    <div class="form-group">
                        <label for="title">Description</label>
                        <textarea class="form-control" name="seo_description" id="description" required data-bv-notempty-message="The Description is required" rows="5">@if(isset($blog)){{$blog->seo_description}}@endif</textarea>
                    </div>
                </div> 
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="thumbnail_image">Thumbnail Image</label>
                        <input type="file" id="input-id" data-browse-on-zone-click="true" id="thumbnail_image" name="thumbnail_image" accept="image/jpg, image/png, image/jpeg" class="form-control custom-file-input"value="@if(isset($blog->thumbnail_image)) {{asset($blog->thumbnail_image)}}@endif">
                    </div>
                    <div class="form-group">
                        <label for="thumbnail_image">Content</label>
                        <textarea name="editorcontent">@if(isset($blog->content)){{$blog->content}}@endif</textarea>
                    </div>
                    <div class="row">
                        @if(isset($blog->blogs_media))
                            @foreach($blog->blogs_media as $key)
                            <div class="column">
                                <img src="{{$blog->url}}/{{$key->image}}" alt="Snow" style="width:100%">
                            </div>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
            <button type="submit" id="submitbtn" class="btn btn-primary">@if(isset($blog))Update @else Submit @endif</button>
        </form>
    </div>
</section>

@endsection
@push('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.8/css/fileinput.min.css">
<link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel = "stylesheet">
<link href="{{ url('css/select2.min.css') }}" rel="stylesheet"/>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css"/>
<style type="text/css">
    .img-container{
        width: 15%;
        display: flex;
    }
    .img-container img{
        justify-content: center;
        margin-left: 5px;
        margin-right: 5px;
        border-radius: 20px;
        height: 100px;
    }

    /*.select2 {
        width:100%!important;
    }*/

    .select2-search__field{
        width: 418px !important;
    }

@media only screen and (max-width: 768px){
    .select2-search__field{
        width: 320px !important;
    }
}

@media only screen and (max-width: 1680px) {
    .select2-search__field{
        width: 208px !important;
    }
}

@media only screen and (max-width: 1280px) {
    .select2-search__field{
        width: 210px !important;
    }
}

@media only screen and (min-width: 992px) {

    .select2-search__field{
        width: 418px !important;
    }
} 

@media only screen and (min-width: 1366px) {

    .select2-search__field{
        width: 322px !important;
    }
}

* {
  box-sizing: border-box;
}

.column {
  float: left;
  width: 33.33%;
  padding: 5px;
}

/* Clearfix (clear floats) */
.row::after {
  content: "";
  clear: both;
  display: table;
}

</style>
@endpush

@push('js')
<script src = "https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="https://cdn.jsdelivr.net/npm/piexifjs@1.0.6/piexif.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.8/js/fileinput.min.js"></script>

<script src="//cdn.ckeditor.com/4.16.2/standard/ckeditor.js"></script>
<script src="{{ url('js/ckfinder/ckfinder.js') }}"></script>

<script src="{{ url('js/select2.min.js') }}"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js"></script>
 <script>

    $( document ).ready(function() 
    { 
        // $('.ckeditor').ckeditor();
        $('#BlogForm').bootstrapValidator();  
        var editor =  CKEDITOR.replace( 'editorcontent' );
        CKFinder.setupCKEditor( editor );

    });

    $(window).bind("pageshow", function() {
        var form = $('form'); 
        // let the browser natively reset defaults
        form[0].reset();
    });

    
    //CKEDITOR.replace( 'editor' );
    $("#BlogForm").submit( function(e) {
        var messageLength = CKEDITOR.instances['editorcontent'].getData().replace(/<[^>]*>/gi, '').length;
        if( !messageLength ) {
            alert( 'Please enter a content' );
            e.preventDefault();
        }
    });

    $('#ads').select2({
        width: '400px',
        placeholder: "Select an Option",
        allowClear: true,
    });

@if(isset($blog))

    $("#input-id").fileinput({
      'showUpload':false, 
      'showCancel':false,
      'showClose':false,
      'showRemove':false,
      'allowedFileExtensions': ["png","jpeg","jpg"],
       maxFileCount: 5,
       maxFileSize: 2000,
       fileActionSettings: {showZoom: false}
    });


    $("#input-id2").fileinput({
      'showUpload':false, 
      'showCancel':false,
      'showClose':false,
      'showRemove':false,
      'allowedFileExtensions': ["png","jpeg","jpg"],
       maxFileCount: 5,
       maxFileSize: 2000,
      fileActionSettings: {showZoom: false}
    });


@else
    $("#input-id").fileinput({
      'showUpload':false, 
      'showCancel':false,
      'showClose':false,
      'showRemove':false,
      'allowedFileExtensions': ["png","jpeg","jpg"],
       maxFileCount: 5,
       maxFileSize: 2000,
       required: true,
      fileActionSettings: {showZoom: false}
    });

    $("#input-id2").fileinput({
      'showUpload':false, 
      'showCancel':false,
      'showClose':false,
      'showRemove':false,
      'allowedFileExtensions': ["png","jpeg","jpg"],
       maxFileCount: 5,
       maxFileSize: 2000,
      fileActionSettings: {showZoom: false}
    });

@endif

/*function checkslug() {
    var slug = $('#slug').val();
    var id = $('#id').val();
    
        if(id == undefined){
            var id = 'null'; 
        }
        $.ajax({
            url: '{{ url('blogs/checkslug/') }}/'+slug +'/'+id,
            type: 'get',
            dataType: 'json',
            success:function(response){
                if(response.length <= 0){
                   $('#error').hide();
                    $("#submitbtn").attr("disabled", false);
                }else{
                    $('#error').show();
                    $("#submitbtn").attr("disabled", true);    
                    }
                }
        });
    }*/
</script> 

@endpush
