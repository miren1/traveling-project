@extends('admin.master')
@section('bodyData')


    <div class="container-fluid">
        <div class="row">
            <a href='{{url('/admin-agent')}}' class='btn btn-primary' style='color:white'>Show Agents</a>
            <div class="col-sm-12 mt-4">

                <ul class="nav nav-tabs">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle='tab' href="#profile">Profile</a>

                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle='tab' href="#Address">Address</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle='tab' href="#booking">Booking Detail</a>

                    </li>

                </ul>

                <div class="tab-content">


                    <!-- // Booking Detail -->
                    <div id="booking" class="container tab-pane" style='min-height:400px'>
                        <div class="row">
                            <div class="col-sm-6">
                                <br>
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item"><b>Order Number: </b>#1245</li>
                                    <li class="list-group-item"><b>Total Amount: </b>2007</li>

                                </ul>
                            </div>

                        </div>
                    </div>

                    <!-- // Address Inforamtion -->

                    <div id="Address" class="container tab-pane" style='min-height:400px'>
                        <div class="row">
                            <div class="col-sm-6">
                                <br>
                                <ul class="list-group">

                                    <li class="list-group-item"><b>House_no: </b> 
                                      @if(isset($agent)){{$agent->house_no}}@endif
                                    </li>
                                    <li class="list-group-item"><b>Society Name: </b>
                                      @if(isset($agent)){{$agent->society_name}}@endif
                                   </li>
                                    <li class="list-group-item"><b>Landmark: </b>
                                      @if(isset($agent)){{$agent->landmark}}@endif
                                   </li>
                                    <li class="list-group-item"><b>City Name: </b>
                                      @if(isset($city)){{$city->name}}@endif
                                    </li>
                                    <li class="list-group-item"><b>State Name: </b>
                                      @if(isset($state)){{$state->name}}@endif
                                    </li>

                                </ul>
                            </div>

                        </div>
                    </div>


                    <!-- Profile Detail -->

                    <div id="profile" class="container tab-pane active">

                        <div class="row">

                            <div class="col-sm-8">
                                <br>

                                <ul class="list-group">

                                    <li class="list-group-item"><b>Company Name: </b>
                                      @if(isset($agent))
                                       {{ $agent->company_name }} 
                                      @endif
                                    </li>
                                    <li class="list-group-item"><b>First Name: </b>
                                      @if(isset($agent))
                                      {{  $agent->first_name }} 
                                      @endif  
                                      </li>
                                    <li class="list-group-item"><b>Last Name: </b>
                                      @if(isset($agent))
                                      {{  $agent->last_name }} 
                                      @endif 
                                    </li>
                                    <li class="list-group-item"><b>Contact Details: </b>
                                      @if(isset($agent))
                                      {{  $agent->contact_details }} 
                                      @endif 
                                    </li>
                                    <li class="list-group-item"><b>Alternate Number: </b>
                                      @if(isset($agent))
                                      {{  $agent->alternate_number }} 
                                      @endif
                                    </li>
                                    <li class="list-group-item"><b>Company Pan number: </b>
                                      @if(isset($agent))
                                      {{  $agent->company_pan }} 
                                      @endif
                                    </li>
                                    <li class="list-group-item"><b>Company GST Number: </b> 
                                      @if(isset($agent))
                                      {{  $agent->company_gst_no }} 
                                      @endif
                                      </li>
                                    <li class="list-group-item"><b>Company Email Address: </b> 
                                      @if(isset($agent))
                                      {{  $agent->company_email }} 
                                      @endif
                                    </li>

                                </ul>
                            </div>
                        </div>


                    </div>







                </div>

            </div>





        </div>




    </div>



    



@endsection
