@extends('admin.master')
@section('bodyData')

<div class="outer-w3-agile mt-3" style='min-height:400px'>
<div class='row'>

{{-- <div class='col-sm-2'>
<a href="{{url('admin-agent/add')}}" style='text-align:left'><button class="btn btn-primary">Add Agent</button></a>
</div> --}}
</div><br>
                  
                    <div class="table-responsive">
                    <table id="example" class="table" style="width: 100%">
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col">First Name</th>
                                <th scope="col">Last Name</th>
                                <th scope="col">Company Name </th>
                                <th scope="col">Contact Details </th>
                                <th scope="col">Company Email </th>
                                <th scope="col">Company GST Number</th>
                                <th>Actions</th>
                 
                               
                            </tr>
                        </thead>
                       
                    </table>
                  </div>
                    <br> <br><br>   <br> <br><br>



          
                </div>
@endsection 


@push('js')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script type="text/javascript">
 
      $(document).on("click", '.agentdelete', function(e) {
            e.preventDefault();
            var id = $(this).attr('data-id');
            swal({
                    title: "Are you sure?",
                    text: "You want to delete this Agent.",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {

                        $.ajax({
                            type: "POST",
                            url: "{{route('admin-agent-delete')}}",
                            data: {
                                "id": id,
                                "_token": "{{ csrf_token() }}"
                            },
                            success: function(response) {
                                if (response.status == "success") {
                                    swal(response.message, {
                                        icon: 'success',
                                    }).then((result) => {
                                         $('#example').DataTable().ajax.reload(null,false);
                                    });
                                } else {
                                    swal("Error","Something went wrong!",{icon:'error'});
                                }
                            }
                        });
                    }
                });
        });


    $(document).ready(function() {
  
    $table = $("#example").DataTable({
      "ajax": "{{ url('admin-agent/list') }}",
      "processing": true,
      "serverSide": true,
      "ordering": false,
      "columns": [
            { "data": "first_name"},
            { "data": "last_name"},
            { "data": "company_name"},
            { "data": "contact_details"},
            { "data": "company_email"},
            { "data": "company_gst_no"},
            { "data": "id", 
                    render:function(data,type,row,meta){
                      
                     var html= '<a  href="{{url("admin-agent/view/")}}/'+btoa(data)+'"><i class="fa fa-info-circle" aria-hidden="true"'+ 
                                'title="Detail View" style="font-size:20px"></i></a>'+
                                '<a class="ml-1" href="{{url("admin-agent/add/")}}/'+btoa(data)+'"><i class="fa fa-edit" aria-hidden="true"'+
                                'title="Edit" style="font-size:20px"></i></a>'+
                                '<a href="#" class="ml-1 agentdelete"  data-id="'+btoa(data)+'"><i class="fa fa-trash"'+
                                 'aria-hidden="true" style="color:red;font-size:20px" title="Delete"></i>'
                                '</a>';

                  return html;          
                      
              }

            },
        ]
    });


    
  } );
</script>

@endpush
