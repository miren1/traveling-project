@extends('admin.master')
@section('bodyData')
<meta name="csrf-token" content="{{ csrf_token() }}">



<section class="forms-section" style='min-height:500px'>
<div class="outer-w3-agile mt-3">
        <h4 class="tittle-w3-agileits mb-4">Edit Agent</h4>
        <a href='{{url('/admin-agent')}}' class='btn btn-primary'>Show Agent List</a>
        
        <div class="mt-4">
          
        <form data-toggle="validator" id="Agentform" role="form" action="{{url('admin-agent/store')}}" method="post">
        @csrf
        @if(isset($agent))
        <input type="hidden" name="id" value="{{$agent->id}}"/>
        @endif
        <div class="form-row">
               
                <div class="form-group col-md-6">
                    <label for="inputEmail4">First Name</label>
                    <input type="text" class="form-control" id="inputEmail4" placeholder="Enter Name" required="" name='first_name'  @if(isset($agent)) value="{{$agent->first_name}}" @endif  data-bv-notempty-message="The First Name is required"  pattern='[A-Za-z]' onkeydown="return alphaOnly(event);" autocomplete="off">
                </div>
                <div class="form-group col-md-6">
                   <label for="inputEmail4">Last Name</label>
                   <input type="text" class="form-control" id="inputEmail4" placeholder="Enter Last Name" required="" name='last_name'  @if(isset($agent)) value="{{$agent->last_name}}" @endif  data-bv-notempty-message="The Last Name is required" pattern='[A-Za-z]' onkeydown="return alphaOnly(event);" autocomplete="off">
               </div>
           </div>

          <div class="form-row">
               
               <div class="form-group col-md-6">
                   <label for="inputEmail4">Company Name</label>
                   <input type="text" class="form-control" id="inputEmail4" placeholder="Enter Company Name" required="" name='company_name' @if(isset($agent)) value="{{$agent->company_name}}" @endif data-bv-notempty-message="The Company Name is required">
               </div>
               <div class="form-group col-md-6">
                   <label for="inputEmail4">Contact Details</label>
                   <input type="number" class="form-control" id="inputEmail4" placeholder="Enter Contact Detail" required="" name='contact_details' minlength="10" maxlength="10" min="0" pattern='[0-9]{10}' @if(isset($agent)) value="{{$agent->contact_details}}" @endif  data-bv-notempty-message="The Contact Detail is required">
               </div>
          </div>

          
          <div class="form-row">
               
               <div class="form-group col-md-6">
                   <label for="inputEmail4">Alternate Number</label>
                   <input type="number" class="form-control" required="" name='alternate_number' @if(isset($agent)) value="{{$agent->alternate_number}}" @endif  minlength="10" maxlength="10" min="0" Placeholder='Enter Alternate number' data-bv-notempty-message="The Alternate Number is required" pattern='[0-9]{10}'>
               </div>

               <div class="form-group col-md-6">
                   <label for="inputEmail4">Company Pan</label>
                   <input type="text" id="pan" class="form-control"  name='company_pan'   @if(isset($agent)) value="{{$agent->company_pan}}" @endif placeholder='Enter Company Pan'  style="text-transform:uppercase" onchange="validatePanNumber(this)">
                   <span id="panError" style="color: red; display: none">Enter valid pan number</span>
               </div>
               
          </div>

          <div class="form-row">
               
               <div class="form-group col-md-6">
                   <label for="inputEmail4">Company GST</label>
                   <input type="text" class="form-control" required="" name='company_gst_no' placeholder='Enter Company GST ' @if(isset($agent)) value="{{$agent->company_gst_no}}" @endif data-bv-notempty-message="The Company GST is required" style="text-transform:uppercase">
               </div>

               <div class="form-group col-md-6">
                   <label for="inputEmail4">Company Email</label>
                   <input type="email" class="form-control" required="" name='company_email' placeholder='Enter Email' @if(isset($agent)) value="{{$agent->company_email}}" @endif data-bv-notempty-message="The Company Email is required">
               </div>
               
          </div>
          <div class="form-row">
               
               <div class="form-group col-md-6">
                   <label for="inputEmail4">House No</label>
                   <input type="number" class="form-control" required="" name='house_no' placeholder='Enter House No ' @if(isset($agent)) value="{{$agent->house_no}}" @endif data-bv-notempty-message="The House No is required">
               </div>

               <div class="form-group col-md-6">
                   <label for="inputEmail4">Society Name </label>
                   <input type="text" class="form-control" required="" name='society_name' placeholder='Enter Society Name ' @if(isset($agent)) value="{{$agent->society_name}}" @endif data-bv-notempty-message="The Society Name  is required" onkeydown="return alphaOnly(event);">
               </div>
               
          </div>

          <div class="form-row">
               
               <div class="form-group col-md-6">
                   <label for="inputEmail4">Land Mark</label>
                   <input type="text" class="form-control" required="" name='landmark' placeholder='Enter Landmark' @if(isset($agent)) value="{{$agent->landmark}}" @endif   data-bv-notempty-message="The Land Mark is required">
               </div>

               <div class="form-group col-md-6">
                   <label for="inputEmail4">Pincode </label>
                   <input type="number" class="form-control" required="" name='pincode' placeholder='Enter Pincode Number'  minlength="6" maxlength="6" min="0" @if(isset($agent)) value="{{$agent->pincode}}" @endif onkeypress="return isNumberKey(event)">
               </div>
               
          </div>
          

                  
          <div class="form-row">
          <div class="form-group col-md-6">
                   <label for="inputEmail4">State </label>
              <select class="form-control" name='state_fk_id' id='c_state_id'  required data-bv-notempty-message="The State is required">
                    <option value=''>Select State</option>
                                           
                        @foreach ($state as $key => $s) { ?>
                          <option value='{{$s->id}}' @if(isset($agent)) @if($s->id==$agent->state_fk_id) selected @endif @endif />
                           {{$s->name}}
                          </option>
                         @endforeach
                   </select>
    
                   
                 </div>
                 <div class="form-group col-md-6">
                   <label for="inputEmail4">City</label>
                   <select id="c_city_id" class="form-control" name='city_fk_id' required=""   data-bv-notempty-message="The City is required" >
                   
                   </select>
               </div>
       
               @if(!isset($agent))
               <div class="form-group col-md-6">
                  
                   <label for="inputEmail4">System Password </label>
                   <input type="password" class="form-control" required="" name='password'  placeholder='Enter Password' data-bv-notempty-message="The System Password is required" >
               </div>
               @endif

               <div class="form-group col-md-6">
                <label for="officeaddress">Official Address</label>
                <textarea  class="form-control" required="" name='official_address' placeholder='Enter Official Address Number '  data-bv-notempty-message="The Official Address is required">@if(isset($agent)){{$agent->official_address}}@endif</textarea>
                
              </div>
        	
               <div class="form-group col-md-12">
                <br>
              <a> <button type="submit" id="submit" class="btn btn-primary mr-5" style='margin-top:13px'>Update</button></a>
             
               </div>

            
        </form>
      </div>
    </div>

</section>

@endsection


@push('css')
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css"/>
@endpush
@push('js')
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js"></script>
<script>
  $(document).ready(function () {
        
    $("#pan").on('keyup',function(){
            // alert();
           var regex = /[A-Z]{5}[0-9]{4}[A-Z]{1}$/;
          if (!regex.test($("#pan").val())) 
          {
            $('#panError').show();
          }else{
            $('#panError').hide();
          }
        })
    });



  function alphaOnly(event) {
        var key = event.keyCode;
        return ((key >= 65 && key <= 90) || key == 8);
    };

    function isNumberKey(evt)
{
  var charCode = (evt.which) ? evt.which : event.keyCode
  if (charCode > 31 && (charCode < 48 || charCode > 57))
  return false;

  return true;
}

  $(document).ready(function () {
    $('#Agentform').bootstrapValidator();
    
    @if(isset($agent))
    var state_fk_id ='{{$agent->state_fk_id}}';
    var city='{{$agent->city_fk_id}}';
    statechange(state_fk_id,city);
    @endif
    //console.log(state_fk_id);
    
  });

  $(window).bind("pageshow", function() {
        var form = $('form'); 
        // let the browser natively reset defaults
        form[0].reset();
    });



  $('#c_state_id').change(function() {
    var value= $(this).val();  
    var city='';
    statechange(value,city);
  });



    function statechange(value,city)
                { 
                    $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
              });
           
               var cat_id = value;
            

                 $.ajax({
                   
                       url:"{{ route('getCity') }}?id=" + cat_id ,
                       type:"POST",
                       data: {
                           cat_id: cat_id
                        },
                      
                       success:function (data,city) {
                        $('#c_city_id').empty();
                        // console.log(data.cities);

                        $.each(data.cities,function(index,cities){
                            if(city==''){
                                $('#c_city_id').append('<option value="'+index+'">'+cities+'</option>');
                            }else{
                                var selected='';
                                if(city==index){
                                 selected='selected'; 
                                }
                                $('#c_city_id').append('<option value="'+index+'" '+selected+'>'+cities+'</option>');
                            }
                            
                        
                        })
                       }
                   })
                }


               
</script>


@endpush
