@extends('admin.master')
@push('css')
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css"/>
@endpush
@push('js')
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js"></script>
<script>
/*
$(document).ready(function () {
    $('#add_membreship').bootstrapValidator({
    });
});
*/

$( document ).ready(function() 
    {
        $('.defaultForm').bootstrapValidator();
         
    });
               
</script>


@endpush
@section('bodyData')
<section class="forms-section">
<div class="outer-w3-agile mt-3">
        <h4 class="tittle-w3-agileits mb-4">Add Membership Plan</h4>
  		
             <form class="defaultForm" action="{{url('create-membership-plan')}}" method="POST" enctype="multipart/form-data" >
            @csrf
  
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputAmount">Fix Amount</label>
                    <input type="text" required="" class="form-control" id="inputAmount" data-bv-notempty-message="The Fix Amount is required" name="fixed_amt" placeholder="Enter Fix Amount">
                </div>
                <div class="form-group col-md-6">
                    <label for="inputYears">Years</label>
                    <input type="number" class="form-control" id="inputYears" name="years" placeholder="Enter Years" required="" data-bv-notempty-message="The Years is required">
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                   <label for="inputRole">Role</label>
                    <select id="inputRole" name="role" class="form-control" required aria-required="true" data-bv-notempty-message="Role is required">
                        <option value="">Select Role</option>
                        <option value="customer">Customer</option>
                        <option value="member">Member</option>
                        <option value="agent">Agent</option>
                      	<option value="employee">Employee</option>
                    </select>
                </div>
                <div class="form-group col-md-6">
                    <label for="mebership_fees">Mebership Fees</label>
                    <input type="text" class="form-control" id="mebership_fees" name="mebership_fees" placeholder="Enter Mebership Fees" required="" data-bv-notempty-message="The Mebership Fees is required">
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="free_nights_room">Free Nights Room</label>
                    <input type="text" class="form-control" id="free_nights_room" name="free_nights_room" placeholder="Enter Free Nights Room" required="" data-bv-notempty-message="The Free Nights Room is required">
                </div>
                <div class="form-group col-md-6">
                    <label for="free_nights_to_avail_in">Free Nights To Avail In</label>
                    <input type="text" class="form-control" id="free_nights_to_avail_in" name="free_nights_to_avail_in" placeholder="Enter Free Nights To Avail In" required="" data-bv-notempty-message="The Free Nights To Avial In is required">
                </div>
            </div>
            
             <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="free_night_in_any_type_of_hotel">Free Night In Any Type Of Hotel</label>
                    <input type="text" class="form-control" id="free_night_in_any_type_of_hotel" name="free_night_in_any_type_of_hotel" placeholder="Enter Free Night In Any Type Of Hotel" required="" data-bv-notempty-message="The Free Night In Any Type Of Hotel is required">
                </div>
                <div class="form-group col-md-6">
                    <label for="monthly_emi">Monthly EMI</label>
                    <input type="text" class="form-control" id="monthly_emi" name="monthly_emi" placeholder="Monthly EMI" required="" data-bv-notempty-message="The Monthly EMI is required">
                </div>
            </div>
          
            <div class="form-row">
                 <div class="form-group col-md-6">
                    <label for="quarterly_emi">Quarterly EMI</label>
                    <input type="text" class="form-control" name="quarterly_emi" id="quarterly_emi" placeholder="Enter Quarterly EMI" required="" data-bv-notempty-message="The Quarterly EMI is required">
                </div>
                <div class="form-group col-md-6">
                    <label for="half_yearly_emi" id="reflab">Half Yearly EMI</label>
                    <input name="half_yearly_emi" id="half_yearly_emi" placeholder="Enter Half Yearly EMI" class="form-control" type="text" required="" data-bv-notempty-message="The Half Yearly EMI is required">
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="annual_emi">Annual EMI</label>
                      <input type="text" name="annual_emi" placeholder="Annual EMI" class="form-control" id="annual_emi" required="" data-bv-notempty-message="The Annual EMI is required">
                </div>
                <div class="form-group col-md-6">
                    <label for="emi_dafulter_charges">EMI Dafulter Charges</label>
                      <input type="text" class="form-control" placeholder="EMI Dafulter Charges" name="emi_dafulter_charges" id="emi_dafulter_charges" required="" data-bv-notempty-message="The EMI Dafulter Charges is required">
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="flexbility_to_move_free_night_to_next_year">Flexbility To Move Free Night To Next Year</label>
                     <input name="flexbility_to_move_free_night_to_next_year" placeholder="Enter Flexbility To Move Free Night To Next Year " class="form-control" type="text" id="flexbility_to_move_free_night_to_next_year" required="" data-bv-notempty-message="The Flexbility To Move Free Night To Next Year is required">
                </div>
                <div class="form-group col-md-4">
                    <label for="easy_upgrade_of_membership_plans" >Easy Upgrade Of Membership Plans</label>
                    <input name="easy_upgrade_of_membership_plans" placeholder="Enter Easy Upgrade Of Membership Plans" class="form-control" type="text" id="easy_upgrade_of_membership_plans" required="" data-bv-notempty-message="The Easy Upgrade Of Membership Plans is required">
                </div>
                <div class="form-group col-md-2">
                    <label for="referral">Referral</label>
                    <input type="text" class="form-control" id="referral" name="referral" required="" placeholder="Enter Referral" data-bv-notempty-message="The Referral is required">
                </div>
            </div>
         
            <button type="submit" name="save" class="btn btn-primary">Submit</button>
        </form>
    </div>
</section> 

@endsection

