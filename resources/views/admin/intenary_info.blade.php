@extends('admin.master')
@section('bodyData')
    
    <div class="outer-w3-agile mt-3" style='min-height:400px'>
        <div class='row'>
            <div class='col-sm-6'>
                <div class="btn btn-primary"><i class="fa fa-list mr-2"></i>Itinerary list</div>
            </div>
            <div class='col-sm-6 text-right'>
                <a href="{{url('/itenaryshow')}}" style='text-align:right'><button class="btn btn-primary">+ Add Itinerary</button></a>
            </div>
            <div class='col-sm-6'></div>
        </div><br>

        <!-- <h4 class="tittle-w3-agileits mb-4">
                        Table Head Options</h4> -->
        <table class="table table-hover" style="width: 100%;" id="dataTable">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">Sr.No</th>
                    <th scope="col">Caption</th>
                    <th scope="col">Day Number</th>
                    <th scope="col">Details</th>
                    <th scope="col">images</th>
                    <th>Actions</th>
                </tr>
            </thead>
        </table>
    </div>
@endsection
@push('js')
<script>
  $(function() {
    $("#dataTable").DataTable({
        'processing': true,
        'serverSide': true,
        "pageLength": 5,
        'serverMethod': 'post',
        "aLengthMenu": [[10,25, 50,100], [10,25, 50,100]],
        order: [
            [0, 'desc']
        ],
        "ajax": {
            "url": "{{ route('allitenaryinfo') }}",
            "data": {
                "_token": "{{ csrf_token() }}"
            },
        },
        "columns": [
            {
                data: "id",
                render: function(data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1
                }
            },
            {
                data: "details",orderable:false,
            },
            {
                data: "day_number",orderable:false,
            },
            {
                data: "caption",orderable:false,
            },
            {
                data: "image",orderable:false,
                render: function(data, row, alldata) {
                    return `<img src="{{url('itenary')}}/`+alldata.images+`" hieght="100" width="100" />`;
                }
            },
            {
                data: "id",orderable:false,
                render: function(data, row, alldata) {
                  var $id = btoa(alldata.id);
                  //console.log($id);
                    $html = `<a href="{{url('/itenaryEdit')}}/`+$id+`">
                         <i class="fa fa-edit" aria-hidden="true" title='Edit' style='font-size:20px'></i>
                         </a>
                      
                      <i class="fa fa-trash delete" style='font-size:20px;color:red;cursor:pointer;' title='delete banner' data-id="`+alldata.id+`" >
                      </i>`;
                    return $html;
                }
            },
        ]
    });

    $(document).on('click','.delete',function(){
      var id = $(this).data('id');
      
      swal({
        title: "Are you sure?",
        text: "You want to delete this Itenary.",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      }).then((willDelete) => {
        if(willDelete)
        {

        $.ajax({
          url: '{{ url('deleteItenary') }}/'+id,
          type: 'get',
          dataType: 'json',
        success:function($data){
            console.log($data);
          if($data == true)
          {
            swal("Itenary has been deleted!", {
              icon: "success",
            });
            //$('#dataTable').DataTable().ajax.reload();
            //$('#dataTable').DataTable().ajax.reload();
            location.reload('#dataTable');
          }else{
            swal("Itenary has been not deleted!", {
              icon: "error",
            });
          }
    
        }
      });


      } else {
        swal("Your Itenary is Safe!");
      }
      });
    });

    $(document).on('click','.leave',function(){
      var id = $(this).data('id');
      // alert('true');
      $('#modal-myvalue').val(id);
    });
  });
</script>
@endpush
