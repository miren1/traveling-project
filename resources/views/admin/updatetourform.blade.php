@extends('admin.master')
@section('bodyData')

<section class="forms-section" style='min-height:500px'>
    <div class="outer-w3-agile mt-3">
        <h4 class="tittle-w3-agileits mb-4">Tour Update</h4>
        <a href='{{url("/tourdetail")}}/{{base64_encode($data->id)}}' class='btn btn-primary'>Detail Tour
            Information</a>
        <a href='{{url("/adminitenary")}}/{{base64_encode($data->id)}}' class='btn btn-primary'>Manage Itinerary</a>

        <form action="{{url('updatetouradmin')}}" method="post" enctype='multipart/form-data' id="inserttouradmin">
        @csrf
        <input type='hidden' name='id' id='id' value='@if(isset($data)){{$data->id}}@endif'>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="inputEmail4"> Tour Name</label>
                        <input type="text" class="form-control" id="inputEmail4" placeholder="Enter Tour Name" 
                            name='name' required="" data-bv-notempty-message="The Tour Name is required" @if(isset($data)) value="{{$data->name}}" @endif pattern='[A-Za-z]{2,30}'>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail03">Price</label>
                        <input type="number" class="form-control" id="inputEmail03" placeholder="Enter Price" name='price' min="0" @if(isset($data)) value="{{$data->price}}" @endif >
                    </div>
                    <div class="form-group">
                        <label for="inputEmail4">Days</label>
                        <input type="Number" class="form-control" min="0"  id="inputEmail4" @if(isset($data)) value="{{$data->days}}" @endif name='days'>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="inputEmail4">Slug</label>
                        <input type="text" class="form-control" 
                        data-bv-stringlength="true"
                        data-bv-stringlength-min="3"
                        data-bv-stringlength-max="20"
                        data-bv-stringlength-message="Slug minimum length is 3 and maxmum length is 20"
                        data-bv-regexp="true"
                        data-bv-regexp-regexp="^[a-z0-9]*$"
                        data-bv-regexp-message="The Slug can consist of lower case  alphabetical characters and no spaces "
                        data-bv-remote-url = "{{ url('/tourcheckSlug') }}/{{$data->id}}"
                        data-bv-remote = "true"
                        data-bv-remote-type = "GET"
                        @if(isset($data)) value="{{$data->slug}}" @endif 
                        data-bv-remote-message="Opps ! Slug Already Used." id="inputEmail6" placeholder="Enter slug" required="" name='slug' data-bv-notempty-message="The slug is required">
                    </div>
                     <div class="form-group">
                        <label for="inputEmail0Person3">Person</label>
                        <input type="text" class="form-control" id="inputEmail0Person3" @if(isset($data)) value="{{$data->person}}" @endif placeholder="Enter Person" 
                            name='person'>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail4">Banner Image</label>
                        <input type="file" class="form-control" name='banner_img'>
                        
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="inputEmail4">Package Type</label>
                        <br>
                        <select class='form-control' name='package_type'  required="" data-bv-notempty-message="The Package Type is required">
                        <option value="">Select Package Type</option>
                            @foreach($result as $value)
                        <option  id='{{$value->id}}' value='{{$value->id}}' @if(isset($data)) @if($data->package_type==$value->id){{'selected'}}  @endif @endif >{{$value->name}}</option>
                        @endforeach    
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="inputEmail4">Previous Image </label>
                        <input type="hidden" name="imagebanner2" @if(isset($data)) value="{{$data->banner_img}}" @endif/>
                        <img src='{{ asset("media") }}/{{$data->banner_img}}' style='width:100%;height:100px'/>
                    </div>
                </div>        
            </div> 
            <div class='row'>
                <div class="form-group col-md-12">
                    <hr>
                    <label for="inputEmail4">Small Information About Trip(Fill If It Neccessary Information)</label>
                    <div class='row'>
                        <div class='form-group col-sm-4'>
                            <label for="region">Region:</label>
                            <input type='text' name='region' id='region' class='form-control'  @if(isset($small_info)) @if(isset($small_info['Region'])) value="{{$small_info['Region']}}" @endif @endif  >

                        </div>
                        <div class='form-group col-sm-4'>
                            <label for="duration">Duration</label>
                            <input type='text' name='duration' id='duration' class='form-control'   @if(isset($small_info)) @if(isset($small_info['Duration'])) value="{{$small_info['Duration']}}" @endif @endif>
                        </div>
                        <div class='form-group col-sm-4'>
                            <label for="start">Start :&nbsp;&nbsp;</label>
                            <input type='text' name='start' id='start' class='form-control'  @if(isset($small_info)) @if(isset($small_info['StartPoint'])) value="{{$small_info['StartPoint']}}" @endif @endif>
                        </div>
                        <div class='form-group col-sm-3'>
                            <label>End:&nbsp;&nbsp;</label>
                            <input type='text' name='end' id='End' class='form-control'  @if(isset($small_info)) @if(isset($small_info['EndPoint'])) value="{{$small_info['EndPoint']}}" @endif @endif>
                        </div>
                        <div class='form-group col-sm-3'>
                            <label>Highest Altitude:</label>
                            <input type='text' name='altitute' id='count' class='form-control'  @if(isset($small_info)) @if(isset($small_info['HighestAltitute'])) value="{{$small_info['HighestAltitute']}}" @endif @endif>
                        </div>
                        <div class='form-group col-sm-3'>
                            <label>Approx km:&nbsp;&nbsp;</label>
                            <input type='text' id='count' class='form-control' name='approx'  @if(isset($small_info)) @if(isset($small_info['ApproxKm'])) value="{{$small_info['ApproxKm']}}" @endif @endif>
                        </div>
                        <div class='form-group col-sm-3'>
                            <label>Grade:&nbsp;&nbsp;</label>
                            <input type='text' name='grade' id='Grade' class='form-control'  @if(isset($small_info)) @if(isset($small_info['Grade'])) value="{{$small_info['Grade']}}" @endif @endif>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="col-md-12">
                    <hr>
                    <label for="Detail1">Detail 1</label>
                    <div class="row">
                        <div class='form-group col-sm-6'>
                            <label for="detaile1label">Enter Label Title:&nbsp;&nbsp;</label>
                            <input id='detaile1label' class='form-control' type='text' name='detaile1label'
                            @if(isset($detail['detail1'])) value="{{key($detail['detail1'])}}" @endif>
                        </div>
                        <div class='form-group col-sm-6'>
                            <label for="detail1value">Enter Detail 1:&nbsp;&nbsp;</label>
                            <textarea name='detail1value' id='detail1value' class='form-control'>@if(isset($detail['detail1'][key($detail['detail1'])])){{trim($detail['detail1'][key($detail['detail1'])])}}@endif</textarea>
                        </div>
                    </div>
                </div>
                <div class="form-group col-md-12">
                    <hr>
                    <label for="Detail2">Detail 2</label>
                    <div class="row">
                        <div class='form-group col-sm-6'>
                            <label for="detaile1label">Enter Label Title 2:&nbsp;&nbsp;</label>
                            <input id='detaile2label' class='form-control' type='text' name='detaile2label'
                           @if(isset($detail['detail2'])) value="{{key($detail['detail2'])}}" @endif>
                        </div>
                        <div class='form-group col-sm-6'>
                            <label for="detail2value">Enter Detail2:&nbsp;&nbsp;</label>
                            <textarea name='detail2value' id='detail2value' class='form-control'>@if(isset($detail['detail2'][key($detail['detail2'])])){{trim($detail['detail2'][key($detail['detail2'])])}} @endif</textarea>
                        </div>
                    </div>
                </div>
                <div class="form-group col-md-12">
                    <hr>
                    <label for="Detail3">Detail 3</label>
                    <div class="row">
                        <div class='form-group col-sm-6'>
                            <label for="detaile3label">Enter Label Title 3:&nbsp;&nbsp;</label>
                            <input id='detaile3label' class='form-control' type='text' name='detaile3label'
                             @if(isset($detail['detail3'])) value="{{key($detail['detail3'])}}" @endif>
                        </div>
                        <div class='form-group col-sm-6'>
                            <label for="detail3value">Enter Detail 3:&nbsp;&nbsp;</label>
                            <textarea name='detail3value' id='detail3value' class='form-control'>@if(isset($detail['detail3'][key($detail['detail3'])])){{trim($detail['detail3'][key($detail['detail3'])])}} @endif</textarea>
                        </div>
                    </div>
                </div>
                <div class="form-group col-md-12">
                    <hr>
                    <label for="Detail4">Detail 4</label>
                    <div class="row">
                        <div class='form-group col-sm-6'>
                            <label for="detaile4label">Enter Label Title 4:&nbsp;&nbsp;</label>
                            <input id='detaile4label' class='form-control' type='text' name='detaile4label'
                            @if(isset($detail['detail4'])) value="{{key($detail['detail4'])}}" @endif>
                        </div>
                        <div class='form-group col-sm-6'>
                            <label for="detail4value">Enter Detail 4:&nbsp;&nbsp;</label>
                            <textarea name='detail4value' id='detail4value' class='form-control'>@if(isset($detail['detail4'][key($detail['detail4'])])) {{trim($detail['detail4'][key($detail['detail4'])])}} @endif</textarea>
                        </div>
                    </div>
                </div>
                <div class="form-group col-md-6">
                    <hr>
                    <label for="inclusion">Inclusion</label>
                    <textarea name='inclusion' id='inclusion' rows="4" class='form-control'>@if(isset($data)){{$data->inclusion}}@endif</textarea>
                </div>
                <div class="form-group col-md-6">
                    <hr>
                    <label for="optional_charges">Optional Charges</label>
                    <textarea name='optional_charges' rows="4" id='optional_charges' class='form-control'>@if(isset($data)){{$data->optional_charges}}@endif</textarea>
                </div>
                <div class="form-group col-md-6">
                    <hr>
                    <label for="exclusion">Exclusion</label>
                    <textarea name='exclusion' rows="4" id='exclusion' class='form-control'>@if(isset($data)){{$data->exclusion}}@endif</textarea>
                </div>
                <div class="form-group col-md-6">
                    <hr>
                    <label for="essentials">Essentials</label>
                    <textarea name='essentials' rows="4" id='essentials' class='form-control'>@if(isset($data)){{$data->essentials}}@endif</textarea>
                </div>
                <div class="form-group col-md-12">
                    <hr>

                    <label for="clothing_essential">Clothing Essential</label>
                    <textarea name='clothing_essential' id='clothing_essential' class='form-control'>@if(isset($data)){{$data->clothing_essential}}@endif</textarea>
                </div>
            </div>
            <button type="submit" class="btn  btn-primary">Update Tour</button>
        </form>
    </div>
</section>
@endsection
@push('css')
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css"/>
@endpush
@push('js')
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js"></script>
<script>

  $(document).ready(function () {
    $('#inserttouradmin').bootstrapValidator();
  });


function demo() {
    var c = document.getElementById('count').value;

    for (var i = 1; i <= c; i++) {
        var node = document.createElement('tr');
        var node1 = document.createElement('td');

        var label1 = document.createElement('input');
        label1.id = 'keys';
        label1.name = 'keys[]';
        label1.placeholder = 'Enter Label';
        node1.appendChild(label1);
        node.appendChild(node1);


        var node1 = document.createElement('td');
        var label1 = document.createElement('input');
        label1.id = 'values';
        label1.name = 'values[]';
        label1.placeholder = 'Enter Value';
        node1.appendChild(label1);
        node.appendChild(node1);
        node.appendChild(node1);
        tby.appendChild(node);
    }

}
          
</script>


@endpush
