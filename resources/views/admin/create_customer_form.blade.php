@extends('admin.master')
@section('bodyData')
<section class="forms-section">
    <div class="outer-w3-agile mt-3">
        <a href="{{route('adminshowcustomer')}}" class='btn btn-primary'>Back</a>
        <h4 class="tittle-w3-agileits mb-4">@if(isset($customer)) Edit @else Add @endif Customer</h4>
        <form action="{{route('admin-customer.store')}}" id="customerForm" method="post">
            @csrf
            @if(isset($customer))
                <input type="hidden" name="id" value="{{$customer->id}}">
            @endif
            <div class="row">
                <div class="form-group col-md-3">
                    <label for="inputEmail4">First Name</label>
                    <input type="text" class="form-control" id="inputEmail4" placeholder="Enter Name" required name='first_name' value="@if(isset($customer)){{$customer->first_name}}@endif" data-bv-notempty="true" data-bv-notempty-message="First Name is required">
                </div>
                <div class="form-group col-md-3">
                    <label for="inputPassword4">Last Name</label>
                    <input type="text" class="form-control" id="inputPassword4" placeholder="Last Name" required name='last_name' value="@if(isset($customer)){{$customer->last_name}}@endif" data-bv-notempty="true" data-bv-notempty-message="Last Name is required">
                </div>
            
                <div class="form-group col-md-2">
                    <label for="inputEmail4">Contact Number</label>
                    <input type="number" min="0" minlength="10" maxlength="10" class="form-control" id="contactnumber" placeholder="Enter Contact Number" value="@if(isset($customer)){{$customer->mobile_number}}@endif" required="" name='mobile_number' data-bv-notempty="true" data-bv-notempty-message="Contact Number is required" @if(isset($customer)) readonly @endif>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-4">
                    <label for="inputPassword4">WhatsApp Number</label>
                    <input type="number" min="0" minlength="10" maxlength="10" class="form-control" id="wanumber" placeholder="Enter WhatsApp  Number" required="" name='whats_up' value="@if(isset($customer)){{$customer->whats_up}}@endif" data-bv-notempty="true" data-bv-notempty-message="WhatsApp Number is required">
                </div>
                <div class="form-group col-md-4">
                    <label for="inputEmail4">Email</label>
                    <input type="email" class="form-control" id="inputEmail4" pattern="[a-zA-Z0-9._%+-]+@[a-z0-9.-]+\.[a-zA-Z]{2,4}" placeholder="Email" required="" name='email' value="@if(isset($customer)){{$customer->email}}@endif" data-bv-notempty="true" data-bv-notempty-message="E-mail is required" data-bv-remote="true" data-bv-remote-type="GET"
                    data-bv-remote-url="{{ url('customer/Editemail') }}/@if(isset($customer)){{$customer->id}}@else All @endif"
                    data-bv-remote-message="Opps ! Email Already Exist">
                </div>
            </div>
            @if(!isset($customer))
            <div class="row">
                <div class="form-group col-md-4">
                    <label for="inputEmail4">Password</label>
                    <input type="password" class="form-control" id="inputEmail4" placeholder="Password" minlength="6" required="" name='password' data-bv-notempty="true" data-bv-notempty-message="password is required" data-bv-identical="true"
                    data-bv-identical-field="c_password"
                    data-bv-identical-message="The password and its confirm password does not match. ">
                </div>
                <div class="form-group col-md-4">
                    <label for="inputEmail4">Confirm Password</label>
                    <input type="password" minlength="6" class="form-control" id="inputEmail4" placeholder="Confirm Password" required="" name='c_password' data-bv-notempty="true" data-bv-notempty-message="confirm password is required" data-bv-identical="true"
                    data-bv-identical-field="password"
                    data-bv-identical-message="The password and its confirm password does not match. ">
                </div>
            </div>
            @endif
            <button type="submit" class="btn btn-primary">@if(isset($customer)) Update @else Submit @endif </button>
        </form>
    </div>
</section>
@endsection
@push('css')
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css"/>
@endpush
@push('js')
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js"></script>

<script>
  $(document).ready(function () {
    $('#customerForm').bootstrapValidator();
  });
  
</script>
@endpush