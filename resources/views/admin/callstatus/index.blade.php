@extends('admin.master')
@section('bodyData')

    <div class="outer-w3-agile mt-3" style='min-height:400px'>
       
        <div class='row'>
            <div class='col-md-6'>
                <div class="btn btn-primary"><i class="fa fa-list mr-2"></i>Call Status list</div>
            </div>
            <div class='col-md-6 text-right'>
             
              <button id="add-group" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal"
                    ><i class="fa fa-plus mr-2"></i>Add Call Status</button>
            </div>
        </div><br>
        <div class="table-responsive">
            <table id="example" class="table" style="width: 100%">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">Name</th>
                        <th scope="col">Created at</th>
                        <th>Actions</th>


                    </tr>
                </thead>

            </table>
        </div>
        <br> <br><br> <br> <br><br>




    </div>



    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="status-head">
                        Add Call Status
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="defaultForm" class="defaultForm" action="{{ route('call-status-store') }}" method="POST"
                    enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="id" id="lead_id" value=""  />
                    <div class="modal-body">


                        <div class="form-row">



                            <div class="form-group col-md-12" id="groupnew" >
                                <label for="inputYears">Name</label>
                                <input type="text" class="form-control" id="inputYears" name="name" placeholder="Enter Name"
                                    required="" data-bv-notempty-message="The Name is required" 
                                    minlength="2" maxlength="50"
                                            >
                            </div>


                        </div>


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary" id="status-add">Add</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.css"
        integrity="sha512-In/+MILhf6UMDJU4ZhDL0R0fEpsp4D3Le23m6+ujDWXwl3whwpucJG1PEmI3B07nyJx+875ccs+yX2CqQJUxUw=="
        crossorigin="anonymous" />
    <link rel="stylesheet"
        href="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css" />
    <style type="text/css">
        div#example_filter input[type="search"] {
            box-sizing: border-box;
            border: 1px solid #d2d6dc;
        }

    </style>
@endpush
@push('js')
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/js/dropify.min.js"
        integrity="sha512-8QFTrG0oeOiyWo/VM9Y8kgxdlCryqhIxVeRpWSezdRRAvarxVtwLnGroJgnVW9/XBRduxO/z1GblzPrMQoeuew=="
        crossorigin="anonymous"></script>
    <script type="text/javascript"
        src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js"></script>
    <script type="text/javascript">
        $(document).on("click", '.agentdelete', function(e) {
            e.preventDefault();
            var id = $(this).attr('data-id');
            swal({
                    title: "Are you sure?",
                    text: "You want to delete this Call Status.",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {

                        $.ajax({
                            type: "POST",
                            url: "{{ route('call-status-delete') }}",
                            data: {
                                "id": id,
                                "_token": "{{ csrf_token() }}"
                            },
                            success: function(response) {
                                if (response.status == "success") {
                                    swal(response.message, {
                                        icon: 'success',
                                    }).then((result) => {
                                        $('#example').DataTable().ajax.reload(null, false);
                                    });
                                } else {
                                    swal("Error", "Something went wrong!", {
                                        icon: 'error'
                                    });
                                }
                            }
                        });
                    }
                });
        });


        $(document).ready(function() {


            $table = $("#example").DataTable({
                "ajax": "{{ url('call-status/list') }}",
                "processing": true,
                "serverSide": true,
                "ordering": false,
                "columns": [{
                        "data": "name"
                    },
                    {
                        "data": "created_at",
                        render: function(data, type, row, meta) {

                            return moment(data).format('DD-MM-YYYY,h:mm a');
                            // body...
                        }
                    },
                    {
                        "data": "id",
                        render: function(data, type, row, meta) {

                            var html = '<button class="ml-1 edit_status" data-id="' + row.id +
                                '" data-name="' + row.name +
                                '"><i class="fa fa-edit" aria-hidden="true"' +
                                'title="Edit" style="font-size:20px"></i></button>' +
                                '<a href="#" class="ml-1 agentdelete"  data-id="' + btoa(data) +
                                '"><i class="fa fa-trash"' +
                                'aria-hidden="true" style="color:red;font-size:20px" title="Delete"></i></a>';

                            return html;

                        }

                    },
                ]
            });

            //create

            $('#defaultForm').bootstrapValidator({
                message: 'This value is not valid',
                fields: {
                    name: {
                        validators: {
                        remote: {
                            type: 'POST',
                            url: '{{ url('call-status/checkname') }}',
                            dataType: 'json',
                            data: function(validator) {
                                console.log(validator.getFieldElements('id').val());
                                return {
                                    id: btoa(validator.getFieldElements('id').val()),
                                     _token: "{{ csrf_token() }}",
                                };
                            },
                            message: 'Opps! Name Already Used!',

                        }
                        }
                    },

                }
            }).on('success.form.bv', function(e) {
                e.preventDefault();
                var $form = $(e.target);
                var bv = $form.data('bootstrapValidator');

                $.post($form.attr('action'), $form.serialize(), function(result) {

                    var data = JSON.parse(JSON.stringify(result));
                    $('#example-select-all').removeAttr('checked');
                    if (data.status == 'success') {
                        swal(data.message, {
                            icon: 'success',
                        }).then((result) => {
                            var sform2 = $('#defaultForm')[0];
                            $(sform2).removeClass('was-validated');
                            sform2.reset();

                            $('#exampleModal').modal('hide');
                            $('#example').DataTable().ajax.reload();
                        });
                    } else {
                        swal("Error", data.message, {
                            icon: 'error'
                        });
                        var form2 = $('#defaultForm')[0];
                        $(form2).removeClass('was-validated');
                        form2.reset();

                        $("#lead_id").val();
                        $('#exampleModal').modal('hide');
                        $('#example').DataTable().ajax.reload();
                    }
                }, 'json');
                //End Update
            });


            $("#exampleModal").on('hide.bs.modal', function() {
                $("#defaultForm")[0].reset();
                $('#defaultForm').bootstrapValidator('resetForm', true);
                $('#lead_id').val('');
                $('#status-callstatus option[value=""]').attr('selected', 'selected');
                $('#status-head').text('Add Call Status');
                $('#status-add').text('Add');

            });



        });


        $(document).on("click", '.edit_status', function(e) {
             
            var id = $(this).attr('data-id');
            var name = $(this).attr('data-name');
           
            $('#lead_id').val('');
            $('#status-head').text('Edit Call Status');
            $('#status-add').text('Update');
            $('#lead_id').val(id);
            $('#inputYears').val(name);
            //$('#inputYears').attr('data-bv-remote-url',url);
            var info= urlName(id);
            $('#exampleModal').modal('show');
        });


        function urlName(id=null) {
            var url ='{{ url('call-status/checkname') }}';
            if(id!=null){
                var url ='{{ url('call-status/checkname') }}/'+btoa(id);
            }
            return url;
        }

    </script>

@endpush
