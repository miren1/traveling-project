@extends('admin.master')
@section('bodyData')
<section class="forms-section" style=''>
   
<div class="outer-w3-agile mt-3">
    <a href='{{url('/groups')}}' class='btn btn-primary' style='color:white'>Back to group list</a>
        <h4 class="tittle-w3-agileits mb-4">@if(isset($group)) {{$group->name}}@endif</h4>
</div>

    <div class="container-fluid">
        <div class="row">
           
            <div class="col-sm-12 mt-4">

                <ul class="nav nav-tabs">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle='tab' href="#profile">Leads</a>

                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle='tab' href="#Address">Assign lead</a>
                    </li>
                  

                </ul>

                <div class="tab-content">


                    <!-- // Booking Detail -->
                    <div id="profile" class="container-fluid tab-pane active " >
                    <form id="frm-example" action="{{route('groups-assign-employee')}}" method="POST">
                      @csrf  
                      <input type="hidden" name="group_id" value="{{$group->id}}"/>
                     <div class="row mt-2">
                         
                        <div class="col-12 mt-4">

                            <div class="form-row">
                                <div class="form-group col-md-3">
                                    {{-- <label for="fname">First Name</label> --}}
                                    <select class="form-control" name="employee_id" id="employee_id">
                                        <option value="">Select User</option>
                                        @if(isset($user))
                                        @foreach($user as $u)
                                        @if($u->name)
                                        <option value="{{$u->id}}">{{$u->name}}</option>
                                        @endif
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group col-md-3">
                                    <button type="submit" name="save" id="Assign_emp" class="btn btn-primary" disabled="true">Assign</button>
                                </div>
                                <div class="form-group col-md-6">

                                </div>
                            </div>
                            
                        </div>
                          <div class="col-12">
                            <div class="table-responsive">
                                <table id="Leads" class="table" style="width: 100%">
                                    <thead class="thead-dark">
                                        <tr>
                                            <th scope="col"><input name="select_all" value="1" id="example-select-all" type="checkbox" /></th>
                                            <th scope="col">First Name</th>
                                            <th scope="col">Last Name</th>
                                            <th scope="col">Number</th>
                                            <th scope="col">Whatsapp Number</th>
                                            <th scope="col">Created at</th>
                                            <th scope="col">Actions</th>
                                        </tr>
                                    </thead>
                                   
                                </table>
                              </div>
                              <br><br><br>
                          </div>
                      </div>  
                     
                        
                    </div>

                    <!-- // Address Inforamtion -->

                    <div id="Address" class="container-fluid tab-pane" >
                        <div class="row mt-2">
                            <div class="col-12 mt-4">

                            </div>
                            <div class="col-12">  
                                <div class="table-responsive">
                                    <table id="EmployeeLeads" class="table" style="width: 100%">
                                        <thead class="thead-dark">
                                            <tr>
                                                <th scope="col">Employee</th>
                                                {{-- <th scope="col">Lead Count</th> --}}
                                                {{-- <th scope="col">Created at</th> --}}
                                                <th scope="col">Actions</th>
                                
                                            
                                            </tr>
                                        </thead>
                                    
                                    </table>
                                </div>
                                <br><br><br>
                            </div></div>
                    </div>

                </form>

                </div>

            </div>





        </div>




    </div>

</section>

    



@endsection


@push('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.css" integrity="sha512-In/+MILhf6UMDJU4ZhDL0R0fEpsp4D3Le23m6+ujDWXwl3whwpucJG1PEmI3B07nyJx+875ccs+yX2CqQJUxUw==" crossorigin="anonymous" />
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css"/>
<style type="text/css">
    div#example_filter input[type="search"] {
    box-sizing: border-box;
    border: 1px solid #d2d6dc;
}

input.CheckLead {
    text-align: left;
    float: left;
    margin-left: 8px;
}
</style>
@endpush


@push('js')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/js/dropify.min.js" integrity="sha512-8QFTrG0oeOiyWo/VM9Y8kgxdlCryqhIxVeRpWSezdRRAvarxVtwLnGroJgnVW9/XBRduxO/z1GblzPrMQoeuew==" crossorigin="anonymous"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js"></script>
<script type="text/javascript">
 
      $(document).on("click", '.Leadsdelete', function(e) {
            e.preventDefault();
            var id = $(this).attr('data-id');
            swal({
                    title: "Are you sure?",
                    text: "You want to delete this lead.",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {

                        $.ajax({
                            type: "POST",
                            url: "{{route('groups-lead-delete')}}",
                            data: {
                                "id": id,
                                "_token": "{{ csrf_token() }}"
                            },
                            success: function(response) {
                                if (response.status == "success") {
                                    swal(response.message, {
                                        icon: 'success',
                                    }).then((result) => {
                                         $('#Leads').DataTable().ajax.reload(null,false);
                                    });
                                } else {
                                    swal("Error","Something went wrong!",{icon:'error'});
                                }
                            }
                        });
                    }
                });
    });
        
        
    $(document).ready(function() {

  
    var table = $("#Leads").DataTable({
      "ajax": "{{ url('groups/lead-list') }}/{{base64_encode($group->id)}}",
      "processing": true,
      "serverSide": true,
      "ordering": false,
      'columnDefs': [{
         'targets': 0,
         'searchable':false,
         'orderable':false,
         'className': 'dt-body-left',
         }],
      "columns": [
            {"data":"id",'render': function (data, type, full, meta){
                return '<input type="checkbox" name="id[]" class="CheckLead " value="' 
                +data+'">';
            }},
            { "data": "fname"},
            { "data": "lname"},
            { "data": "number"},
            { "data": "wnumber"},
            { "data": "created_at",render:function(data,type,row,meta) {

                return moment(data.created_at).format('DD-MM-YYYY,h:mm a');
                
            }},
            { "data": "id", 
                    render:function(data,type,row,meta){
                      
                     var html= '<a  href="{{url("groups/number-view/")}}/'+btoa(data)+'"><i class="fa fa-eye" aria-hidden="true"'+ 
                                'title="Detail View" style="font-size:20px"></i></a>'+
                                '<a class="ml-1" href="{{url("groups/lead-edit/")}}/'+btoa(data)+'"><i class="fa fa-edit" aria-hidden="true"'+
                                'title="Edit" style="font-size:20px"></i></a>'+
                                '<a href="#" class="ml-1 Leadsdelete"  data-id="'+btoa(data)+'"><i class="fa fa-trash"'+
                                 'aria-hidden="true" style="color:red;font-size:20px" title="Delete"></i></a>';

                  return html;          
                      
              }

            },
        ]
    });

    //create

     // Handle click on "Select all" control
   $('#example-select-all').on('click', function(){
      // Check/uncheck all checkboxes in the table
      var rows = table.rows({ 'search': 'applied' }).nodes();
      $('input[type="checkbox"]', rows).prop('checked', this.checked);
      
      if($('#example-select-all:checked').length == 1)
      {
          $("#Assign_emp").attr("disabled", false); 
      }else{
          $("#Assign_emp").attr("disabled", true); 
      }  

   });

   // Handle click on checkbox to set state of "Select all" control
   $('#Leads tbody').on('change', 'input[type="checkbox"]', function(){
        var $fields = $('.CheckLead:checked').length;
        if($fields==0){
            $("#Assign_emp").attr("disabled", true); 
        }else{
            $("#Assign_emp").attr("disabled", false); 
        }
   });
   $('#Leads tbody').on('change', 'input[type="checkbox"]', function(){
      // If checkbox is not checked
      if(!this.checked){
        
         var el = $('#example-select-all').get(0);
         // If "Select all" control is checked and has 'indeterminate' property
         if(el && el.checked && ('indeterminate' in el)){
            // Set visual state of "Select all" control 
            // as 'indeterminate'
            el.indeterminate = true;
            
         }
      }else{
        $("#Assign_emp").attr("disabled", false); 
      }
   });

   $('#Leads').on( 'page.dt', function () {
    //var info = table.page.info();
         $('#frm-example').bootstrapValidator('resetForm', true);
        $('input[type="checkbox"]').removeAttr('checked');
        $('#example-select-all').removeAttr('checked');
        $("#Assign_emp").attr("disabled", true); 
    
    });
    




$('#frm-example').bootstrapValidator({
        message: 'This value is not valid',
        fields: {
            'employee_id': {
                validators: {
                    notEmpty: {
                        message: 'Please Select Employee.'
                    }
                }
            },
            'id[]': {
                validators: {
                    choice: {
                        min: 1,
                        message: 'Please choose 1 record you are good at'
                    }
                }
            },
     }
    }).on('success.form.bv', function(e) {
        e.preventDefault();
        var $form = $(e.target);
        var bv = $form.data('bootstrapValidator');
        var matches = [];
        var checkedcollection = table.$(".CheckLead:checked", { "page": "all" });
        checkedcollection.each(function (index, elem) {
            matches.push($(elem).val());
        });

        console.log(matches.length);
        var AccountsJsonString = JSON.stringify(matches);
        
        if(matches.length!=0){

        $.post($form.attr('action'), $form.serialize(), function(result) {
            //$('#loaderbg').hide();
             
             $("#Assign_emp").attr("disabled", true); 
                var data=JSON.parse(JSON.stringify(result));
                $('#example-select-all').removeAttr('checked');
                if(data.status=='success')
                {
                    swal(data.message, {
                    icon: 'success',
                    }).then((result) => {
                        $('#Leads').DataTable().ajax.reload(null,false);
                        $('#EmployeeLeads').DataTable().ajax.reload(null,false);
                        
                        $('#frm-example').bootstrapValidator('resetForm', true);
                    });
                }else 
                {
                swal("Error",data.message,{icon:'error'});
                    $('#Leads').DataTable().ajax.reload(null,false);
                    $('#frm-example').bootstrapValidator('resetForm', true);
                }

        },'json');

        }else{
            swal("You Have not selected any records of lead so please try again!!")
            .then((value) => {
                location.reload();
            });
        }

    });  


    $("#EmployeeLeads").DataTable({
      "ajax": "{{ url('groups/list_employee') }}/{{base64_encode($group->id)}}",
      "processing": true,
      "serverSide": true,
      "ordering": false,
      "columns": [
            { "data": "name"},
            // { "data": "id",render:function(data,type,row,meta) {
            //     return "<span class='badge badge-info ml-1'>"+row.mainleads_count+"</span>";
            // } },
            // { "data": "created_at",render:function(data,type,row,meta) {
            //     return moment(data.created_at).format('DD-MM-YYYY,h:mm a'); 
            // }},
            { "data": "id", 
                    render:function(data,type,row,meta){
                        var group_id ='';
                        @if(isset($group)) group_id ='{{$group->id}}';@endif
                        
                     var html= '<a  href="{{url("groups/employee-leads/")}}/'+btoa(row.id)+'/'+btoa(group_id)+'"><i class="fa fa-eye" aria-hidden="true"'+ 
                                'title="View All Leads" style="font-size:20px"></i></a>'+
                                '<a href="#" class="ml-1 Unassign"  data-id="'+btoa(row.id)+'" data-group="'+btoa(group_id)+'"><i class="fa fa-minus-circle"'+
                                 'aria-hidden="true" style="color:red;font-size:20px" title="Unassign All Leads"></i></a>';

                  return html;          
                      
              }

            },
        ]
    }); 



    $(document).on("click", '.Unassign', function(e) {
            e.preventDefault();
            var id = $(this).attr('data-id');
            var group_id =$(this).attr('data-group');
            swal({
                    title: "Are you sure?",
                    text: "You want to Unassign All Leads from this Employee.",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {

                        $.ajax({
                            type: "POST",
                            url: "{{route('groups-unassign')}}",
                            data: {
                                "id": id,
                                'group_id':group_id,
                                "_token": "{{ csrf_token() }}"
                            },
                            success: function(response) {
                                if (response.status == "success") {
                                    swal(response.message, {
                                        icon: 'success',
                                    }).then((result) => {
                                         $('#Leads').DataTable().ajax.reload(null,false);
                                         $('#EmployeeLeads').DataTable().ajax.reload(null,false);
                                    });
                                } else {
                                    swal("Error","Something went wrong!",{icon:'error'});
                                }
                            }
                        });
                    }
        });
    });


    
  } );
</script>

@endpush
