@extends('admin.master')
@section('bodyData')
<div class="outer-w3-agile mt-3" style='min-height:400px'>
    <div class='row'>
        <div class='col-md-6'>
            <div class="btn btn-primary"><i class="fa fa-list mr-2"></i>Group list</div>
        </div>
        <div class='col-md-6 text-right'>
          <button id="add-group" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" style='text-align:left'><i class="fa fa-plus mr-2"></i>Add Group</button>
        </div>
    </div><br>
    <div class="table-responsive">
        <table id="example" class="table" style="width: 100%">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">Name</th>
                    <th scope="col">Lead Count</th>
                    <th scope="col">Created at</th>
                    <th>Actions</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    Add Gourp 
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="defaultForm" class="defaultForm" action="{{route('groups-store')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="inputAmount">Group</label>
                            <select class="form-control"  name="group_id" id="group_select" required="" data-bv-notempty-message="The Group is required" >
                             <option value="">Select Group</option>
                             <option value="addnew">Add New</option>
                             @if(isset($group))
                             @foreach($group as $g)
                             <option value="{{$g->id}}">{{$g->name}}</option> 
                             @endforeach  
                             @endif
                            </select>
                        </div>
                        <div class="form-group col-md-12" id="groupnew" style="display: none;">
                            <label for="inputYears">Group Name</label>
                            <input type="text" class="form-control" id="inputYears" 
                                name="name" 
                                placeholder="Enter Group Name"
                                required="" 
                                data-bv-notempty-message="The Group Name is required"
                                data-bv-remote-url = "{{ url('groups/checkname') }}"
                                data-bv-remote = "true"
                                data-bv-remote-type = "GET"   
                                data-bv-remote-message="Opps ! Group Name Already Used">
                        </div>    
                        <div class="form-group col-md-12">
                            <label for="inputEmail4">CSV File <span style="color: red">*please upload proper formatted csv file.</span></label>  <a href="{{url('demo/group_sample.csv')}}" class="btn btn-primary mb-2" download="" style="float: right;">sample file</a>
                            <input type="file" name="csv_file" data-bv-file-extension="csv" data-bv-file="true" class="form-control dropify" required="" data-bv-notempty-message="The CSV File is required"/>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary"
                        data-dismiss="modal">Close</button>
                    <button  type="submit" class="btn btn-primary">Add</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection 

@push('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.css" integrity="sha512-In/+MILhf6UMDJU4ZhDL0R0fEpsp4D3Le23m6+ujDWXwl3whwpucJG1PEmI3B07nyJx+875ccs+yX2CqQJUxUw==" crossorigin="anonymous" />
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css"/>
<style type="text/css">
    div#example_filter input[type="search"] {
    box-sizing: border-box;
    border: 1px solid #d2d6dc;
}
</style>
@endpush
@push('js')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/js/dropify.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js"></script>
<script type="text/javascript">
 
    $(document).on("click", '.agentdelete', function(e) {
        e.preventDefault();
        var id = $(this).attr('data-id');
        swal({
            title: "Are you sure?",
            text: "You want to delete this Group.",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {

                $.ajax({
                    type: "POST",
                    url: "{{route('groups-delete')}}",
                    data: {
                        "id": id,
                        "_token": "{{ csrf_token() }}"
                    },
                    success: function(response) {
                        if (response.status == "success") {
                            swal(response.message, {
                                icon: 'success',
                            }).then((result) => {
                                    $('#example').DataTable().ajax.reload(null,false);
                            });
                        } else {
                            swal("Error","Something went wrong!",{icon:'error'});
                        }
                    }
                });
            }
        });
    });
        
        
    $(document).ready(function() {

    $('.dropify').dropify();
  
    $('#defaultForm').bootstrapValidator();

    $("#group_select").change(function() {
        var val= $(this).val();
        if(val=='addnew'){
            $('#groupnew').show();
        }else{
            $('#groupnew').hide();
        }
    });
  
    $table = $("#example").DataTable({
      "ajax": "{{ url('groups/list') }}",
      "processing": true,
      "serverSide": true,
      "ordering": false,
      "columns": [
            { "data": "name"},
            {"data":"id",render:function(data,type,row,meta) {

            return "<span class='badge badge-info ml-1'>"+row.grouplead_count+"</span>";

            } },
            { "data": "created_at",render: function (data, type, row) {
          return moment(new Date(data).toString()).format('DD/MM/YYYY');
        }},
            { "data": "id", 
                    render:function(data,type,row,meta){
                      
                     var html= '<a  href="{{url("groups/lead-view/")}}/'+btoa(data)+'"><i class="fa fa-eye" aria-hidden="true"'+ 
                                'title="Detail View" style="font-size:20px"></i></a>'+
                                '<a href="#" class="ml-1 agentdelete"  data-id="'+btoa(data)+'"><i class="fa fa-trash"'+
                                 'aria-hidden="true" style="color:red;font-size:20px" title="Delete"></i></a>';

                  return html;          
                      
              }

            },
        ]
    });

    //create

  
    $("#add-group").click(function(){
        $('#defaultForm').bootstrapValidator('resetForm', true);
    });

    $("#exampleModal").on('hide.bs.modal', function(){
    $("#defaultForm")[0].reset();
    $('#defaultForm').bootstrapValidator('resetForm', true);
    });
    
  } );

   
</script>

@endpush
