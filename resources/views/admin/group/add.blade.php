@extends('admin.master')
@push('css')
<link rel="stylesheet"
    href="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css" />
@endpush

@section('bodyData')
<section class="forms-section">
    <a href='{{url('/groups/lead-view/')}}/{{base64_encode($group->group_id)}}' class='btn btn-primary' style='color:white'>Show Lead</a>
    <div class="outer-w3-agile mt-3">
        <h4 class="tittle-w3-agileits mb-4"> Edit Lead</h4>
        
        <form class="defaultForm" action="{{route('groups-lead-store')}}" method="POST" enctype="multipart/form-data">
            @csrf
            @if(isset($group))
            <input type="hidden" value="{{$group->id}}" name="id" />
            <input type="hidden" value="{{base64_encode($group->group_id)}}" name="group_id" />
            @endif
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="fname">First Name</label>
                    <input type="text" required="" class="form-control" id="inputAmount"
                        data-bv-notempty-message="The First Name is required" name="fname"
                        placeholder="Enter First Name" @if(isset($group)) value="{{$group->fname}}" @endif >
                </div>
                <div class="form-group col-md-6">
                    <label for="lname">Last Name</label>
                    <input type="text" class="form-control" id="lname" name="lname" placeholder="Enter Last Name"
                        required="" data-bv-notempty-message="The Last Name is required" @if(isset($group)) value="{{$group->lname}}" @endif>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="Email">Email</label>
                    <input type="text" required="" class="form-control" id="Email"
                        data-bv-notempty-message="The Email is required" name="email"
                        placeholder="Enter Email" @if(isset($group)) value="{{$group->email}}" @endif >
                </div>
                <div class="form-group col-md-6">
                    <label for="number">Number</label>
                    <input type="text" class="form-control" maxlength="10" id="number" name="number" placeholder="Enter Number"
                        required="" data-bv-notempty-message="The Number is required" @if(isset($group)) value="{{$group->number}}" @endif>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="wnumber">Whatsapp Number</label>
                    <input type="text" required="" class="form-control" id="wnumber" maxlength="10"
                        data-bv-notempty-message="The Whatsapp Number is required" name="wnumber"
                        placeholder="Enter Whatsapp Number" @if(isset($group)) value="{{$group->wnumber}}" @endif >
                </div>
                <div class="form-group col-md-6">
                    <label for="city">City </label>
                    <input type="text" class="form-control"  id="city" name="city" placeholder="Enter city"
                        required="" data-bv-notempty-message="The city is required" @if(isset($group)) value="{{$group->city}}" @endif>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="state">State</label>
                    <input type="text" required="" class="form-control" id="state"
                        data-bv-notempty-message="The State is required" name="state"
                        placeholder="Enter State" @if(isset($group)) value="{{$group->state}}" @endif >
                </div>
                <div class="form-group col-md-6">
                    <label for="country">Country </label>
                    <input type="text" class="form-control"  id="country" name="country" placeholder="Enter Country"
                        required="" data-bv-notempty-message="The Country is required" @if(isset($group)) value="{{$group->country}}" @endif>
                </div>
            </div>

            <div class="form-row">
               
                <div class="form-group col-md-6">
                    <label for="area">Area location </label>
                    <textarea type="text" class="form-control"  id="area" name="area" placeholder="Enter Area location"
                    required="" data-bv-notempty-message="The Area location is required">@if(isset($group)){{$group->area}}@endif</textarea>
                    
                </div>
            </div>

            <button type="submit" name="save" class="btn btn-primary"> @if(isset($group)) Update Lead @else Submit Lead @endif</button>
        </form>
    </div>
</section>
@endsection

@push('js')
<script type="text/javascript"
    src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js"></script>
<script>
$(document).ready(function() {
    $('.defaultForm').bootstrapValidator();

});
</script>

@endpush
