@extends('admin.master')
@section('bodyData')

    <div class="outer-w3-agile mt-3" style='min-height:400px'>
        <a href="{{ url('groups/lead-view') }}/{{$group_id}}" style='text-align:left'><button class="btn btn-primary">Back</button></a>
        <h4 class="tittle-w3-agileits mb-4">Employee: @if(isset($user)) {{$user->name}}@endif</h4>
        
        <div class="table-responsive">
            <table id="example" class="table" style="width: 100%">
                <thead class="thead-dark">
                    <tr>
                        {{-- <th scope="col"><input name="select_all" value="1" id="example-select-all" type="checkbox" /></th> --}}
                        <th scope="col">First Name</th>
                        <th scope="col">Last Name</th>
                        <th scope="col">Number</th>
                        <th scope="col">Whatsapp Number</th>
                        <th scope="col">Activity Count</th>
                        <th scope="col">Created at</th>
                        <th scope="col">Actions</th>
                    </tr>
                </thead>

            </table>
        </div>
        <br> <br><br> <br> <br><br>




    </div>



   
@endsection

@push('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.css"
        integrity="sha512-In/+MILhf6UMDJU4ZhDL0R0fEpsp4D3Le23m6+ujDWXwl3whwpucJG1PEmI3B07nyJx+875ccs+yX2CqQJUxUw=="
        crossorigin="anonymous" />
    <link rel="stylesheet"
        href="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css" />
    <style type="text/css">
        div#example_filter input[type="search"] {
            box-sizing: border-box;
            border: 1px solid #d2d6dc;
        }

    </style>
@endpush
@push('js')
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/js/dropify.min.js"
        ></script>
  
    <script type="text/javascript"
        src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js"></script>
    
    
    <script type="text/javascript">
         $(document).on("click", '.Leadsdelete', function(e) {
            e.preventDefault();
            var id = $(this).attr('data-id');
            swal({
                    title: "Are you sure?",
                    text: "You want to delete this lead.",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {

                        $.ajax({
                            type: "POST",
                            url: "{{route('groups-lead-delete')}}",
                            data: {
                                "id": id,
                                "_token": "{{ csrf_token() }}"
                            },
                            success: function(response) {
                                if (response.status == "success") {
                                    swal(response.message, {
                                        icon: 'success',
                                    }).then((result) => {
                                         $('#Leads').DataTable().ajax.reload(null,false);
                                    });
                                } else {
                                    swal("Error","Something went wrong!",{icon:'error'});
                                }
                            }
                        });
                    }
                });
    });


        $(document).ready(function() {

            $("#example").DataTable({
            "ajax": "{{ url('groups/employee-leads-list') }}/{{$id}}/{{$group_id}}",
            "processing": true,
            "serverSide": true,
            "ordering": false,
            'columnDefs': [{
            'targets': 0,
            'searchable':false,
            'orderable':false,
            'className': 'dt-body-left',
         }],
        "columns": [
            { "data": "fname"},
            { "data": "lname"},
            { "data": "number"},
            { "data": "wnumber"},
            {"data":"id" ,render:function(data,type,row,meta) {

            return "<span class='badge badge-info ml-1'>"+row.activity_count+"</span>";

            }},
            { "data": "created_at",render:function(data,type,row,meta) {

                return moment(data.created_at).format('DD-MM-YYYY,h:mm a');
                
            }},
            { "data": "id", 
                    render:function(data,type,row,meta){
                      
                     var html='<a  href="{{url("groups/view_status/")}}/'+btoa(data)+'/{{$id}}"><i class="fa fa-eye" aria-hidden="true"'+ 
                                'title="Detail View" style="font-size:20px"></i></a>'+
                              '<a href="#" class="ml-1 Unassign"  data-id="'+btoa(data)+'"><i class="fa fa-minus-circle"'+
                                'aria-hidden="true" style="color:red;font-size:20px" title="Unassign All Leads"></i></a>';

                  return html;
                //   '<a  href="{{url("groups/number-view/")}}/'+btoa(data)+'"><i class="fa fa-eye" aria-hidden="true"'+ 
                //     'title="Detail View" style="font-size:20px"></i></a>'+
                //      '<a class="ml-1" href="{{url("groups/lead-edit/")}}/'+btoa(data)+'"><i class="fa fa-edit" aria-hidden="true"'+
                //  'title="Edit" style="font-size:20px"></i></a>'+
                //  '<a href="#" class="ml-1 Leadsdelete"  data-id="'+btoa(data)+'"><i class="fa fa-trash"'+
                //   'aria-hidden="true" style="color:red;font-size:20px" title="Delete"></i></a>'+          
                      
              }

            },
        ]
    });

    $(document).on("click", '.Unassign', function(e) {
            e.preventDefault();
            var id = $(this).attr('data-id');
            swal({
                    title: "Are you sure?",
                    text: "You want to Unassign Lead from this Employee.",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {

                        $.ajax({
                            type: "POST",
                            url: "{{route('lead-groups-unassign')}}",
                            data: {
                                "id": id,
                                "_token": "{{ csrf_token() }}"
                                @if(isset($user))
                                ,"employee_id":"{{base64_encode($user->id)}}",
                                @endif
                            },
                            success: function(response) {
                                if (response.status == "success") {
                                    swal(response.message, {
                                        icon: 'success',
                                    }).then((result) => {
                                         $('#example').DataTable().ajax.reload(null,false);
        
                                    });
                                } else {
                                    swal("Error","Something went wrong!",{icon:'error'});
                                }
                            }
                        });
                    }
        });
    });

        });

    </script>

@endpush
