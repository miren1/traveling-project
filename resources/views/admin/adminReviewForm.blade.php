@extends('admin.master')
@section('bodyData')
<meta name="csrf-token" content="{{ csrf_token() }}">
<section class="forms-section" style='min-height:500px'>
<div class="outer-w3-agile mt-3">
        <h4 class="tittle-w3-agileits mb-4">Review Creation</h4>
        
    
          
        <form action="{{url('ReviewCreation')}}" method="post" enctype="multipart/form-data">
        @csrf
      <div class="form-row">
               
                <div class="form-group col-md-6">
                    <label for="inputEmail4">Name</label>
                    <input type="text" class="form-control" id="inputEmail4" placeholder="Enter  Name" required="" name='name'>
                </div>
                <div class="form-group col-md-6">
                   <label for="inputEmail4">Content</label>
                   <input type="text" class="form-control" id="inputEmail4" required="" name='content' style='height:50px'>
               </div>
           </div>

           <div class="form-row">
               
               <div class="form-group col-md-6">
                   <label for="inputEmail4">Image</label>
                   <input type="file" class="form-control" id="inputEmail4"  required="" name='image'>
               </div>
             
          </div>

          
         

          <div class="form-row">
               
               <div class="form-group col-md-6">
                   <button class='btn btn-success'>Create Review</button>
               </div>
            </div>
        

         
         

          
          

                  
          
               
              
              
              

            
        </form>
    </div>

</section>

@endsection
