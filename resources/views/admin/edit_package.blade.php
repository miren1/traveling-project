@extends('admin.master')
@section('bodyData')

<script>
  var loadFile = function(event) {
    var output = document.getElementById('output');
    document.getElementById('output').setAttribute("style","width:100px");
    document.getElementById('output').setAttribute("style","height:100px");
  output.src = URL.createObjectURL(event.target.files[0]);
    output.onload = function() {
      URL.revokeObjectURL(output.src) ;
    
      // free memory
     

    }
  };
</script>

<section class="forms-section" style='min-height:500px'>
  <div class="outer-w3-agile mt-3">
    <a class='btn btn-primary' href='{{url('/adminshowpackages')}}'>Back</a>
    <h4 class="tittle-w3-agileits mb-4">Category Update</h4>      
    <form action="{{url('updateeplan')}}" id="Agentform" method="post" enctype="multipart/form-data">
    @csrf

    <input type='hidden' name='id' value='{{$package[0]->id}}'>
      <div class="row">       
        <div class="form-group col-md-4">
          <label for="inputEmail4">Name</label>
          <input type="text" class="form-control" id="inputEmail4" placeholder="Enter Name" required="" data-bv-notempty-message="The Name  is required" name='name' value='{{$package[0]->name}}'>
      
          <label for="inputEmail4">Slug</label>
          <input type="text" class="form-control"
          value="{{$package[0]->slug}}" 
          data-bv-stringlength="true"
          data-bv-stringlength-min="3"
          data-bv-stringlength-max="20"
          data-bv-stringlength-message="Slug minimum length is 3 and maxmum length is 20"
          data-bv-regexp="true"
          data-bv-regexp-regexp="^[a-z0-9]*$"
          data-bv-regexp-message="The Slug can consist of lower case  alphabetical characters and no spaces "
          data-bv-remote-url = "{{ url('/catcheckSlug') }}/{{$package[0]->id}}"
          data-bv-remote = "true"
          data-bv-remote-type = "GET"   
          data-bv-remote-message="Opps ! Slug Already Used." id="inputEmail6" placeholder="Enter slug" required="" name='slug' data-bv-notempty-message="The slug is required">
        
          <label for="inputEmail4">Tag Line</label>
          <input type="text" class="form-control" id="inputEmail4" placeholder="Enter Tagline" data-bv-notempty-message="The Tag Line  is required" name='tag_line' value='{{$package[0]->tag_line}}'>

          <label for="inputEmail4">Details</label>
          <textarea class="form-control" id="inputEmail4" placeholder="Enter Plan Detail" name='details' required="" data-bv-notempty-message="The Details  is required">{{$package[0]->details}}</textarea>

          <label for="inputEmail4">Image</label>
          <input type='file' class="form-control" name='photo' id="imgInp" onchange="loadFile(event)" >
        </div>
        <div class="form-group col-md-2"></div>
        <div class="form-group col-md-4">
          <label for="inputEmail4">Curent  Image</label>
          <img id="output"/>
          <br>       
          
          <img src="{{ asset('uploads/plans_images/'.$package[0]->photo)}}" alt="Image" style='height:250px; ;width:250px'/>        
        </div>
      </div>
      <button type="submit" class="btn btn-primary">Update</button>
    </form>
  </div>
</section>

@endsection


@push('css')

<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css"/>

@endpush
@push('js')


<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js"></script>
<script>
$(document).ready(function () {
  $('#Agentform').bootstrapValidator();
});
</script>
@endpush
