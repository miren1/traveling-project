@extends('admin.master')
@section('bodyData')
<section class="forms-section" style='min-height:500px'>
<div class="outer-w3-agile mt-3">
    <h4 class="tittle-w3-agileits mb-4">Add Employee</h4>
    
    @if ($message = Session::get('admincreation'))
    <div class="alert alert-{{Session::get('class')}} alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>	
            <strong>{{ $message }}</strong>
    </div>
    @endif
          
    <form action="{{url('createEmployee')}}" id="employeeForm" method="post">
    @csrf

        <div class="row">       
            <div class="form-group col-md-4">
                <label for="inputEmail4">Email</label>
                <input type="email" class="form-control emailVerify" id="inputEmail4" placeholder="Enter Employee's Email Address" required name='email' pattern="[a-zA-Z0-9._%+-]+@[a-z0-9.-]+\.[a-zA-Z]{2,4}" style="text-transform: lowercase" value="{{ old('email') }}" data-bv-notempty="true" data-bv-notempty-message="E-mail is required">
                <span id="emailError" style="color: red; font-size: 15px;"></span>
            </div>
        </div> 
        <div class="row">          
            <div class="form-group col-md-4">
                <label for="inputEmail4">Mobile No.</label>
                <input type="number" min="0" minlength="10" maxlength="10" class="form-control verifyMobile" id="inputEmail4" placeholder="Enter Employee's Contact Number" required name='contact_number' value="{{ old('contact_number') }}" data-bv-notempty="true" data-bv-notempty-message="Mobile No. is required">
                <span id="mobileError" style="color: red; font-size: 15px;"></span>
            </div>
        </div> 
        <div class="row">
           <div class="form-group col-md-4">
               <label for="inputEmail4">Salary</label>
               <input type="number" class="form-control" id="inputEmail4" placeholder="Enter Salary" required="" min="0" minlength="4" maxlength="8" name='salary' value="{{ old('salary') }}" data-bv-notempty="true" data-bv-notempty-message="Salary is required">
           </div>
        </div> 
        <div class="row">
           <div class="form-group col-md-4">
               <label for="inputEmail4">On Board (Salary Started From)</label>
               <input type="text" class="form-control" id="StartDate"  name='follow_date' value="{{ old('follow_date') }}"   placeholder="DD/MM/YYYY" autocomplete="off" >
               <span id="dateError" style="color:red; display: none; font-size: 13px">Date is required</span>
           </div>
        </div> 
        <div class="row">
           <div class="form-group col-md-4">
               <label for="inputEmail4">Designation</label>
               <input type="text" class="form-control" onkeydown="return alphaOnly(event);" placeholder="Enter Designation" required="" name='designation' value="{{ old('designation') }}" data-bv-notempty="true" data-bv-notempty-message="designation is required">
           </div>
        </div>
        <button type="submit" id="submit" class="btn btn-primary">Submit</button>
    </form>
</div>
</section>

@endsection
@push('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.css" integrity="sha512-In/+MILhf6UMDJU4ZhDL0R0fEpsp4D3Le23m6+ujDWXwl3whwpucJG1PEmI3B07nyJx+875ccs+yX2CqQJUxUw==" crossorigin="anonymous" />
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css"/>
 
<link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel = "stylesheet">
<style type="text/css">
  .outer-w3-agile {
    margin-bottom: 1% !important;
  }

ul ul a {
    font-size: 0.9em !important;
    padding-left: 30px !important;
    background: #083672;
}
#sidebar ul li a:hover {
    color: #083672;
    background: #fff;
}
.btn-primary, .btn-primary:hover, .btn-primary:active, .btn-primary:visited {
    background-color: #083672 !important;
border-color: #083672 !important;
}
.btn-info, .btn-info:hover, .btn-info:active, .btn-info:visited {
    background-color: #f3941e !important;
  border-color: #f3941e !important;
}
.bg-primary
{
 background-color: #f3941e !important;
  border-color: #f3941e !important;
}

.fa-info-circle,.fa-edit
{
color:#083672;
}
ul.top-icons-agileits-w3layouts a.nav-link i {
    display: inline-block;
    font-size: 17px;
    color: #083672;
}
.nav-pills .nav-link.active, .nav-pills .show>.nav-link {
    color: #fff;
    background-color: #083672;
}
a {
    color: #083672;
    text-decoration: none;
    background-color: transparent;
    -webkit-text-decoration-skip: objects;
}
a:hover {
    color: #083672;
    text-decoration: underline;
}
.ui-datepicker-month{
    color: #f7af38 !important;
}

.ui-datepicker-year{
    color: #f7af38 !important;
}
/* Chrome, Safari, Edge, Opera */
input::-webkit-outer-spin-button,
        input::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
        }

        /* Firefox */
        input[type=number] {
        -moz-appearance: textfield;
        }

</style>
@push('js')
<script src = "https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/js/dropify.min.js" integrity="sha512-8QFTrG0oeOiyWo/VM9Y8kgxdlCryqhIxVeRpWSezdRRAvarxVtwLnGroJgnVW9/XBRduxO/z1GblzPrMQoeuew==" crossorigin="anonymous"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js"></script>
<script>

   
  $(document).ready(function() {
            $('#submit').click(function() 
        {
            if (!$('#StartDate').val()) 
            {
                $( "#dateError" ).show();
            }
       })
    });
    $(document).ready(function () {
        $('#employeeForm').bootstrapValidator();
    });
    function alphaOnly(event) {
        var key = event.keyCode;
        return ((key >= 65 && key <= 90) || key == 8);
    };

    

    $(function() {
        $( "#StartDate" ).datepicker({
        minDate: new Date(),
        dateFormat: 'dd/mm/yy',
        changeMonth: true,
        changeYear: true,
        yearRange: '-110:+29',
        language: "es",
    });
        $("#StartDate").on('change',function(){
            // alert();
            $( "#dateError" ).hide();
        })
  });

$(document).on('keyup','.emailVerify',function(){
    var data = $('.emailVerify').val();
    $.ajax({
        type: "post",
        url: '/emailVerifyEmp',   
        data: {'email':data},
        success: function(response){
            if (response.status == 'error') {
                $('#emailError').html('Email already exists!');
            }else{
                $('#emailError').html('');
            }
        }    
    });
});
$(document).on('keyup','.verifyMobile',function(){
    var data = $('.verifyMobile').val();
    $.ajax({
        type: "post",
        url: '/verifyMobileEmp',   
        data: {'mobileNo':data},
        success: function(response){
            if (response.status == 'error') {
                $('#mobileError').html('mobile no already exists!');
            }else{
                $('#mobileError').html('');
            }
        }    
    });
});
</script>
@endpush