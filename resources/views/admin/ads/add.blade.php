@extends('admin.master')
@section('bodyData')
<section class="forms-section" style='min-height:500px'>
<div class="outer-w3-agile mt-3">
        <h4 class="tittle-w3-agileits mb-4">@if(isset($ads))Edit @else Add @endif Ads</h4>
        <form action="{{ route('ads-store')}}" id="AdsForm" method="post" enctype="multipart/form-data">
        @csrf
            <div class="row">
               @if(isset($ads))
                    <input type="hidden" id="id" name="id" value="{{$ads->id}}"/>
                @endif
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text" class="form-control" name="title" id="title" value="@if(isset($ads)){{$ads->title}}@endif"
                        data-bv-stringlength="true"
                        data-bv-remote-url = "{{ url('ads/checktitle') }}/@if(isset($ads)){{$ads->id}}@endif"
                        data-bv-remote = "true"
                        data-bv-remote-type = "GET"   
                        data-bv-remote-message="Opps ! Title Already Used." id="inputEmail6" placeholder="Enter Title" required="" name='slug' data-bv-notempty-message="The Title is required"
                        >
                        <span class="error" id="error" style="color:red; display:none;">This Title is already exist.</span>
                    </div>
                    <div class="form-group">
                        <label for="ads_type">Ads Type</label>
                        <select class="form-control" name="ads_type" id="ads_type" required data-bv-notempty-message="The Ads type is required" onchange="adstype()">
                            <option value="">Select type of ads</option>
                            <option value="image" @if(isset($ads) && $ads->ads_type == 'image') selected @endif>Image</option>
                            <option value="video" @if(isset($ads) && $ads->ads_type == 'video') selected @endif>Video</option>
                            <option value="script" @if(isset($ads) && $ads->ads_type == 'script') selected @endif>Script</option>
                        </select>
                    </div>
                    <div class="form-group" @if(isset($ads->ads_image)) style="display: block;" @else style="display:none;" @endif id="ads_image_div">
                        <label for="ads_id">Image</label>
                        <input type="file" id="input-id" onchange="SelectImage()" data-browse-on-zone-click="true" id="ads_id" name="ads_image" accept="image/jpg, image/png, image/jpeg" class="form-control custom-file-input" @if(isset($ads->ads_image)) value="{{$ads->ads_image}}" @endif >
                    </div>

                    <div class="form-group" @if(isset($ads->ads_video_url)) style="display: block;" @else style="display:none;" @endif id="ads_url_div">
                        <label for="ads_video_url">Video URL</label>
                        <input type="url" class="form-control" name="ads_video_url" id="ads_video_url" required data-bv-notempty-message="The Video URL is required" value="@if(isset($ads)){{$ads->ads_video_url}}@endif">
                    </div>
                     <div class="form-group" @if(isset($ads->affiliate_url)) style="display: block;" @else style="display:none;" @endif id="affiliate_url_div">
                        <label for="affiliate_url">Affiliate URL</label>
                        <input type="url" class="form-control" name="affiliate_url" id="affiliate_url" required data-bv-notempty-message="The Affiliate URL is required" value="@if(isset($ads)){{$ads->affiliate_url}}@endif">
                    </div>
                     <div class="form-group">
                        <label for="ads_position">Ads Position</label>
                        <select class="form-control" name="ads_position" id="ads_position" required data-bv-notempty-message="The Ads Position is required">
                            <option value="">Select position of ads</option>
                            <option value="right" @if(isset($ads) && $ads->position == 'right') selected @endif>Right</option>
                            <option value="footer" @if(isset($ads) && $ads->position == 'footer') selected @endif>Footer</option>
                            <option value="between" @if(isset($ads) && $ads->position == 'between') selected @endif>Between Paragraphs</option>
                        </select>
                    </div>
                </div> 
                <div class="col-md-2"></div>
                <div class="col-md-4">

                    @if(isset($ads->ads_image))
                    <small>
                        <img src="{{$ads->url}}/{{$ads->ads_image}}" style="margin-top: 50px;">
                        <!-- <span>{{$ads->ads_image}}</span> -->
                    </small>
                    @endif
                </div>  
            </div>
            <button type="submit" id="submitbtn" class="btn btn-primary">@if(isset($ads))Update @else Submit @endif</button>
        </form>
    </div>
</section>

@endsection
@push('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.min.css" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.8/css/fileinput.min.css">
<link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel = "stylesheet">
<link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel = "stylesheet">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css"/>
@endpush

@push('js')
<script src = "https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="https://cdn.jsdelivr.net/npm/piexifjs@1.0.6/piexif.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.8/js/fileinput.min.js"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js"></script>
 <script>

    $( document ).ready(function() 
    {
        $('#AdsForm').bootstrapValidator();  
    });

    $(window).bind("pageshow", function() {
        var form = $('form'); 
        // let the browser natively reset defaults
        form[0].reset();
    });

@if(isset($ads))
    $("#input-id").fileinput({
      'showUpload':false, 
      'showCancel':false,
      'showClose':false,
      'showRemove':false,
      'allowedFileExtensions': ["png","jpeg","jpg"],
       maxFileCount: 5,
       maxFileSize: 2000,
      fileActionSettings: {showZoom: false}
    });

@endif

    function imagedata(data)
    {
        $("#input-id").fileinput({
          'showUpload':false, 
          'showCancel':false,
          'showClose':false,
          'showRemove':false,
          'allowedFileExtensions': ["png","jpeg","jpg"],
           maxFileCount: 5,
           maxFileSize: 2000,
           required: data,
          fileActionSettings: {showZoom: false}
        });
    }

    function adstype(){
        var type = $("#ads_type").val();
        if(type == "image"){
            $("#ads_image_div").show();
            $("#ads_url_div").hide();
            $("#affiliate_url_div").hide();

            imagedata(true);
        }else if(type == "video"){
            $("#ads_image_div").hide();
            $("#ads_url_div").show(); 
            $("#affiliate_url_div").hide();

            imagedata(false);
            
        }else{
            $("#ads_image_div").show();
            $("#ads_url_div").hide();
            $("#affiliate_url_div").show();
            imagedata(true);
        }
    }

    function SelectImage()
    {
        $('#submitbtn').removeAttr('disabled');
        return false;
    }

    $('#submitbtn').click(function() {
        if( document.getElementById("input-id").files.length == 0 ){
            console.log("no files selected");
            $('#submitbtn').removeClass('disabled'); 
            imagedata(false);
        }
    });

    function checktitle(){

        var title = $('#title').val();
        var id = $('#id').val();
        
        if(id == undefined){
            var id = 'null'; 
        }
            
        $.ajax({
            url: '{{ url('ads/checktitle/') }}/'+title +'/'+id,
            type: 'get',
            dataType: 'json',
            success:function(response){
                if(response.length <= 0){
                    $('#error').hide();
                    $("#submitbtn").attr("disabled", false);
                }else{
                    $('#error').show();
                    $("#submitbtn").attr("disabled", true);
                }
            }
        });
    }

</script> 

@endpush
