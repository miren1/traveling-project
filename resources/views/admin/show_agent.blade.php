@extends('admin.master')
@section('bodyData')
    <script>
        function searchAgent(name) {

            var query = name.value;
            console.log(query);
            $.ajax({
                url: "{{ route('agentlive') }}",
                method: 'GET',
                data: {
                    query: query
                },
                dataType: 'json',
                success: function(data) {
                    $('#tableid').empty();
                    // $('tbody').html(data.table_data);
                    $('#tableid').append(data.table_data);
                }
            })
        }

    </script>
    @if ($message = Session::get('AgentDelete'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $message }}</strong>
        </div>
    @endif
    @if ($message = Session::get('error'))
        <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $message }}</strong>
        </div>
    @endif

    @if ($message = Session::get('agenrCreate'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $message }}</strong>
        </div>
    @endif




    <div class="outer-w3-agile mt-3" style='min-height:400px'>
        <div class='row'>
            <div class='col-sm-2'>
                <a href="/adminshowagent" style='text-align:left'><button class="btn btn-primary">Show Agent</button></a>
            </div>
            <div class='col-sm-2'>
                <a href="/adminAgentForm" style='text-align:left'><button class="btn btn-primary">Add Agent</button></a>
            </div>
            <div class='col-sm-4'>

                <input class="form-control mr-sm-2" type="search" placeholder="Search With Name ,Id" aria-label="Search"
                    onkeypress='searchAgent(this)'>

            </div>
            <div class='col-sm-6'></div>
        </div><br>

        <!-- <h4 class="tittle-w3-agileits mb-4">
                        Table Head Options</h4> -->
        <table class="table">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">First Name</th>
                    <th scope="col">First Name</th>
                    <th scope="col">Last Name</th>
                    <th scope="col">Company Name </th>
                    <th scope="col">Contact Details </th>
                    <th scope="col">Company Email </th>
                    <th scope="col">Company GST Number</th>
                    <th>Actions</th>



                </tr>
            </thead>
            <tbody id='tableid'>
                @foreach ($agents as $value)
                    <tr>
                        <td>{{ 'JJ-AG-' . $value->id }}</td>
                        <td>{{ $value->first_name }}</td>
                        <td>{{ $value->last_name }}</td>
                        <td>{{ $value->company_name }}</td>
                        <td>{{ $value->contact_details }}</td>
                        <td>{{ $value->company_email }}</td>
                        <td>{{ $value->company_gst_no }}</td>
                        <td>
                            <a href="/agentdetails/{{ $value->id }}">
                                <i class="fa fa-info-circle" aria-hidden="true" title='Detail View'
                                    style='font-size:20px'></i>
                            </a>

                            <a href="/editagent/{{ $value->id }}">
                                <!-- <button class="btn btn-info">Update</button> -->
                                <i class="fa fa-edit" aria-hidden="true" title='Edit' style='font-size:20px'></i>
                            </a>

                            <a href="/agentdelete/{{ $value->id }}">
                                <i class="fa fa-trash" aria-hidden="true"
                                    onclick='return confirm("Are you Sure Delete this Agent?")'
                                    style='color:red;font-size:20px' title='Delete'></i>
                            </a>

                        </td>




                    </tr>
                @endforeach
            </tbody>
        </table>
        <br> <br><br> <br> <br><br>




    </div>
@endsection
