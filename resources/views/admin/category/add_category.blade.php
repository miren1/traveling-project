@extends('admin.master')
@section('bodyData')
<section class="forms-section" style='min-height:500px'>
<div class="outer-w3-agile mt-3">
        <h4 class="tittle-w3-agileits mb-4">@if(isset($category))Edit @else Add @endif Inquiry Category</h4>
        
        @if ($message = Session::get('admincreation'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>	
                <strong>{{ $message }}</strong>
        </div>
        @endif
       
        <form action="{{ url('admin-category/store')}}" id="catogeryForm" method="post" enctype="multipart/form-data">
        @csrf

            <div class="row">
                @if(isset($category))
                    <input type="hidden" id="id" name="id" value="{{$category->id}}"/>
                @endif   
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="category_name">Inquiry Category Name</label>
                        <input type="text" class="form-control" id="category_name" value="@if(isset($category)){{$category->name}}@endif" placeholder="Inquiry Category Name" name='ticket_category_name' pattern="[a-zA-Z][a-zA-Z0-9\s]*" onkeyup="checkcategory()">
                         
                        <span class="error" id="error" style="color:red; display:none;">This Category is already exist.</span>
                        
                    </div>
                    <div class="form-group">
                        <label for="description">Description(Optional)</label>
                        <textarea id="description"  class="form-control description" rows="4" maxlength="200" onkeyup="countChar()"  placeholder="Inquiry Category Description" name='description'>@if(isset($category)){{$category->dec}}@endif</textarea>
                        <p class="help-block float-right h-margin-0-4">
                            {{-- <strong class="msg-count">1</strong> Text --}} (<strong id="msg-char">0</strong>/200 Characters)
                        </p>
                    </div>  
                </div>   
            </div>
            <button type="submit" id="submitbtn" class="btn btn-primary">@if(isset($category))Update @else Submit @endif</button>

        </form>
    </div>

</section>

@endsection
@push('css')
<style type="text/css" media="screen">
.error {
  color: red; // you're using display!
}    
</style>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.css" >
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css"/>
@endpush
@push('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/js/dropify.min.js" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>
{{-- <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js"></script> --}}
 <script>

    $( document ).ready(function() 
    {
        //$('#catogeryForm').bootstrapValidator();  
        $('#catogeryForm').validate({
            rules:{
               ticket_category_name:{
                required: true
               },
                /*description: {
                    required: true
                },*/
            }
           
        });  
    });
    //=================Category Check==============//
    function checkcategory() {

          var category_name = $('#category_name').val();
          var id = $('#id').val();
        
            if(id == undefined){
                var id = 'null';
                
            }
            
            $.ajax({
              url: '{{ url('admin-category/checkcategory/') }}/'+category_name +'/'+id,
              type: 'get',
              dataType: 'json',
            success:function(response){
                
            if(response.length <= 0){
               $('#error').hide();
                $("#submitbtn").attr("disabled", false);
            }else{
                $('#error').show();
                $("#submitbtn").attr("disabled", true);
        
                }

            }
            });
        }

        function countChar(){

         var len = $('.description').val().length;
         var str= $('.description').val();
         
         var text=1;
         if (len >= 200) {
                  text=parseInt(len)/200;
                  $('.msg-count').text(Math.ceil(text));
                  $('.sms-count').text(Math.ceil(text));
                  $('#msg-char').text(len);
                  $('.description').val(str.replace(/[^\x00-\x7F]/g, ""));
                  //$('.description').val($('.description').val().substring(0, 160)) ;
         } else {
                  $('.sms-count').text('1');   
                  $('.msg-count').text('1');   
                  $('.description').val(str.replace(/[^\x00-\x7F]/g, ""));
                  $('#msg-char').text(len);

         }
         //countcontant();

    };
   /*
            $('#category_name').keypress(function (e) {
            var regex = new RegExp("^[a-zA-Z]+$");
            var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
            if (regex.test(str)) {
                return true;
            }
            else
            {
            e.preventDefault();
            $('.error').show();
            $('.error').text('Please Enter Alphabate');
            return false;
            }
        });*/
</script> 

@endpush
