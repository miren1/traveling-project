@extends('admin.master')
@section('bodyData')
<section class="forms-section" style='min-height:500px'>
<div class="outer-w3-agile mt-3">
        <h4 class="tittle-w3-agileits mb-4">@if(isset($ticket_assign))Edit @else @endif Category Assignment</h4>
        
        @if ($message = Session::get('admincreation'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>	
                <strong>{{ $message }}</strong>
        </div>
        @endif
          
        <form action="{{ url('admin-category/store_category_assignment')}}" id="catogeryForm" method="post" enctype="multipart/form-data">
        @csrf
            <div class="row">
                @if(isset($ticket_assign))
                    <input type="hidden" name="id" value="{{$ticket_assign->id}}"/>
                @endif   

                <div class="col-md-4">
                    <div class="form-group">
                        <label for="inputEmail4">Employee Name </label>
                        <select class="form-control" name='employee_id' id='employee_id' required data-bv-notempty-message="The Employee is required" onchange="Emp_details()">
                            <option value=''>Select Employee</option>

                            @foreach($user as $key)
                            <option value="{{$key->id}}" @if(isset($ticket_assign) && $key->id==$ticket_assign->user_id) selected @endif />
                                {{$key->name}} {{$key->surname}}
                            </option>
                             @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail4">Category </label>
                        <select class="form-control" name='category_id' id='category_id' required data-bv-notempty-message="The Category is required" onchange="get_sub_category()">
                            <option value=''>Select Category</option>
                            
                                @foreach($ticket_categoies as $key)
                                <option value="{{$key->id}}" @if(isset($ticket_assign) && $key->id==$ticket_assign->ticket_cat_id) selected @endif />
                                    {{$key->name}}
                                </option>
                                 @endforeach
                             
                        </select>
                    </div>
                    <div class="form-group">
                         <label for="inputEmail4">Sub Category </label>
                        @if(isset($ticket_assign))
                        <select class="form-control" required data-bv-notempty-message="The Sub Category is required" name='sub_category_id' id='sub_category_id'>
                        @else
                            <select class="form-control" name='sub_category_id' id='sub_category_id'>
                        @endif
                            <option value=''>Select Sub Category</option>
                            @if(isset($ticket_assign))
                                @foreach($ticket_sub_category as $key)
                                <option value="{{$key->id}}" @if(isset($ticket_assign) && $key->id==$ticket_assign->ticket_sub_cat_id) selected @endif />
                                    {{$key->name}}
                                </option>
                                 @endforeach
                            @endif
                        </select>
                    </div>
                
                </div>   
            </div>
            <button type="submit" class="btn btn-primary btnSubmit">@if(isset($ticket_assign))Update @else Submit @endif.</button>
        </form>
    </div>

</section>

@endsection
@push('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.css" integrity="sha512-In/+MILhf6UMDJU4ZhDL0R0fEpsp4D3Le23m6+ujDWXwl3whwpucJG1PEmI3B07nyJx+875ccs+yX2CqQJUxUw==" crossorigin="anonymous" />
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css"/>
@endpush
@push('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/js/dropify.min.js" integrity="sha512-8QFTrG0oeOiyWo/VM9Y8kgxdlCryqhIxVeRpWSezdRRAvarxVtwLnGroJgnVW9/XBRduxO/z1GblzPrMQoeuew==" crossorigin="anonymous"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js"></script>
<script>

$( document ).ready(function() 
    {
        $('#catogeryForm').bootstrapValidator();
         
    });

    function get_sub_category() {
          var category_id = $('#category_id').val();
          var emp_id = $('#employee_id').val();
            $.ajax({
              url: '{{ url('admin-category/get_subCategory/') }}/'+category_id+'/'+emp_id,
              type: 'get',
              dataType: 'json',
            success:function(response){
                console.log(response);
                var len = response.length;

                //if(response.length >=1 ){

                    //$('#sub').hide();
                    $("#sub_category_id").empty();
                    $("#sub_category_id").append("<option value=''>Select Sub Category</option>");
                    for( var i = 0; i<len; i++){
                        var id = response[i]['id'];
                        var name = response[i]['name'];
                        
                        $("#sub_category_id").append("<option value='"+id+"'>"+name+"</option>");

                    }
                /*}else{
                    $('#sub').show();
                    $(".btnSubmit").attr("disabled", true);
                }*/
            }
            });
        }

    function Emp_details(){

        var emp_id = $('#employee_id').val();
        
        $.ajax({
              url: '{{ url('admin-category/get_Category/') }}/'+emp_id,
              type: 'get',
              dataType: 'json',
            success:function(response){
                console.log(response);
                var len = response.length;

                $("#category_id").empty();
                $("#category_id").append("<option value=''>Select Category</option>");
                for( var i = 0; i<len; i++){
                    var id = response[i]['id'];
                    var name = response[i]['name'];
                    
                    $("#category_id").append("<option value='"+id+"'>"+name+"</option>");

                }
            }
            }); 
    }
/*    
for( var i = 0; i<len; i++){
    var id = response[i]['id'];
    var name = response[i]['name'];
    
    $("#sel_user").append("<option value='"+id+"'>"+name+"</option>");

}*/

</script>

@endpush