@extends('admin.master')
@section('bodyData')

@if ($message = Session::get('EmployeeLeaveUpdated'))
<div class="alert alert-success alert-block">
	<button type="button" class="close" data-dismiss="alert">×</button>	
        <strong>{{ $message }}</strong>
</div>
@endif


<div class="outer-w3-agile mt-3">
    <div class='row'>
      <div class='col-md-6'>
        <div class="btn btn-primary"><i class="fa fa-list mr-2"></i>Banner list</div>
      </div>
      <div class='col-md-6 text-right'>
        <button class="btn btn-primary" onclick="window.location='{{ route("admin-banner.add") }}'">
          <i class="fa fa-plus mr-2"></i>Add Banner</button>
      </div>
    </div>
<br>
{{-- <div class="row"> --}}
  <div class="table-responsive">
    <table class="table table-hover" style="width: 100%;" id="dataTable">
      <thead class="thead-dark">
        <tr>
            <th>#</th>
            <th>Title</th>
            <th>Description</th>
            <th>Image</th>
            <th>Action</th>
        </tr>
      </thead>
    </table>
  </div>
    
    </div>
@endsection
@push('js')
<script>
  $(function() {
    $("#dataTable").DataTable({
        'processing': true,
        'serverSide': true,
        'serverMethod': 'post',
        "aLengthMenu": [[10,25, 50,100], [10,25, 50,100]],
        order: [
            [0, 'desc']
        ],
        "ajax": {
            "url": "{{ url('admin-category/get_assign_data') }}",
            "data": {
                "_token": "{{ csrf_token() }}"
            },
        },
        "columns": [
            {
                data: "id",
                render: function(data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1
                }

            },

            {
                data: "name",
                render: function(data, type, row, alldata) {
                  //console.log(row.users.name);
                  return row.users.name+' '+row.users.surname;
                  
                }

            },

            {
                data: "name",
                render: function(data, type, row, alldata) {
                  var cat = '';
                  if(row.ticket_categories!=null){
                    cat = row.ticket_categories.name;
                  }
                  console.log(cat);
                  return cat;
                  
                }
            },
            {
                data: "name",
                render: function(data, type, row, alldata) {
                  var str ='';
                  if(row.ticket_sub_categories!=null){
                    str =row.ticket_sub_categories.name;
                  }
                  //console.log(row.ticket_sub_categories.name);
                  return str;
                  
                }
            },
            
            {
                data: "id",orderable:false,
                render: function(data, row, alldata) {
                  var $id = btoa(alldata.id);
                    $html = `<a href="{{url('/admin-category/edit_category_assign')}}/`+$id+`">
                         <!-- <button class="btn btn-info">Update</button> -->
                         <i class="fa fa-edit" aria-hidden="true" title='Edit' style='font-size:20px'></i>
                         </a>
                      
                      <i class="fa fa-trash delete" style='font-size:20px;color:red;cursor:pointer;' title='delete' data-id="`+alldata.id+`" >
                      </i>`;
                    return $html;
                }
            },
        ]
    });

    $(document).on("click", '.delete', function(event) { 
    var id =$(this).attr('data-id');
    //alert(id);
    swal({
      title: "Are you sure?",
      text: "You Want To Delete This Ticket Sub Category ,You Will Not Be Able To Recover This Ticket Sub Category!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {

        $.ajax({
          url: '{{ url('admin-category/destroy_assign_category') }}/'+id,
          type: 'get',
          dataType: 'json',
        success:function($data){
          if($data == true)
          {
            swal("Ticket Sub Category has been deleted!", {
              icon: "success",
            });
            //$('#dataTable').DataTable().ajax.reload();
            //$('#dataTable').DataTable().ajax.reload();
            location.reload('#dataTable');
          }else{
            swal("Ticket Sub Category has been not deleted!", {
              icon: "error",
            });
          }
    
        }
      });


      } else {
        swal("Your Ticket Sub Category is Safe!");
      }
    });
  });

    $(document).on('click','.leave',function(){
      var id = $(this).data('id');
      // alert('true');
      $('#modal-myvalue').val(id);
    });
  });
</script>
@endpush
