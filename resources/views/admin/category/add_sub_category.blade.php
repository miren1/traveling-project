@extends('admin.master')
@section('bodyData')
<section class="forms-section" style='min-height:500px'>
<div class="outer-w3-agile mt-3">
        <h4 class="tittle-w3-agileits mb-4">@if(isset($ticket_sub_category))Edit @else Add @endif Inquiry Sub Category</h4>
        
        @if ($message = Session::get('admincreation'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>	
                <strong>{{ $message }}</strong>
        </div>
        @endif
          
        <form action="{{ url('admin-category/sub_cat_store')}}" id="subcatogeryForm" method="post" class="commentForm" enctype="multipart/form-data">
        @csrf
            <div class="row">
                @if(isset($ticket_sub_category))
                    <input type="hidden" name="id" value="{{$ticket_sub_category->id}}"/>
                @endif   

                <div class="col-md-4">
                    <div class="form-group">
                        <label for="inputEmail4">Category </label>
                        <select class="form-control" required data-bv-notempty-message="The Category is required" name='category_id' id='category_id'>
                            <option value=''>Select Category</option>
                            @foreach($ticket_categoies as $key)
                            <option value="{{$key->id}}" @if(isset($ticket_sub_category) && $key->id==$ticket_sub_category->ticket_cat_id) selected @endif />
                                {{$key->name}}
                            </option>
                             @endforeach
                        </select>
                    </div>
                    <label for="inputEmail4">Inquiry Sub Category Name</label>
                     <div class="input-group">
                        <input type="hidden" id="counter" value="1">
                        <div class="input-group-append">
                            <button type="button" class="btn btn-primary input-group-text" id="add_fields" style="margin-bottom: 15px;"><span style="color:#fff;">+</span></button>
                        </div>
                        <input type="text" class="form-control name_input" placeholder="Inquiry Sub Category" name="ticket_sub_category_name[0]" id="sub_category_name" value="@if(isset($ticket_sub_category)){{$ticket_sub_category->name}}@endif"  style="margin-bottom: 15px; width: 90%;" onkeyup="check_Subcategory()">
                        
                    </div>
                    <div id="add_people_div" class="input-group"></div>
                    
                    <div class="errorTxt"></div>
                        <span class="error" id="error" style="color:red; display:none;">This Sub Category is already exist.</span>
                    <div class="form-group">
                        <label for="inputEmail4">Description(Optional)</label>
                        <textarea class="form-control description" rows="4" maxlength="200" id="description" placeholder="Inquiry Sub Category Description" onkeyup="countChar()" name='description'>@if(isset($ticket_sub_category)){{$ticket_sub_category->dec}}@endif</textarea>
                        <p class="help-block float-right h-margin-0-4">
                            (<strong id="msg-char">0</strong>/200 Characters)
                        </p>
                    </div>
                    
                    <div class="form-group">
                        <label for="inputEmail2">Role:</label>
                        <div class="form-row">&nbsp;&nbsp;   
                            <div class="checkbox"><label>
                            <input type="checkbox" value="1" name='employee' @if(isset($ticket_sub_category)) @if($ticket_sub_category->employee==1) checked @endif @endif>&nbsp;Employee</label>
                            </div> &nbsp;&nbsp;
                            <div class="checkbox">
                                <label><input type="checkbox" value="1" name='agent' @if(isset($ticket_sub_category)) @if($ticket_sub_category->agent=='1') checked @endif @endif>&nbsp;Agent</label>
                            </div>&nbsp;&nbsp;
                            <div class="checkbox">
                                <label><input type="checkbox" value="1" name='member' @if(isset($ticket_sub_category)) @if($ticket_sub_category->member=='1') checked @endif @endif>&nbsp;Member</label>
                            </div>&nbsp;&nbsp;
                            <div class="checkbox">
                                <label><input type="checkbox" value="1" name='user' @if(isset($ticket_sub_category)) @if($ticket_sub_category->user=='1') checked @endif @endif>&nbsp;User</label>
                            </div>
                        </div>  
                      </div> 
                        
                </div>   
            </div>
            <button type="submit" id="submitbtn" class="btn btn-primary">@if(isset($ticket_sub_category))Update @else Submit @endif</button>
        </form>
    </div>

</section>

@endsection
@push('css')
<style type="text/css" media="screen">
.error {
   float: none; color: red; 
   padding-left: .5em;
   vertical-align: top; 
   display: block;
} 


</style>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.css"/>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css"/>
@endpush
@push('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/js/dropify.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>
{{-- <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js"></script> --}}
<script>
$( document ).ready(function() 
{
    $('#add_fields').click( function(){
        add_inputs()
    });
    $(document).on('click', '.remove_fields', function() {
        $(this).closest('.record').remove();
    });
    function add_inputs(){
        var counter = parseInt($('#counter').val());
        var html = '<div class="record input-group" style="margin-bottom: 15px;"><div class="input-group-append"><button type="button" class="remove_fields btn btn-danger" style="margin-bottom: 15px;">-</button></div><input type="text" placeholder="Enter Sub Category Name" name="ticket_sub_category_name['+ counter +']" class="form-control sub_cat name_input" onkeyup="check_on_Subcategory()" style="margin-bottom: 15px; width: 90%;"></div><div id="error_agree"></div>';
        
        $('#add_people_div').append(html);
        $('#counter').val( counter + 1 );
    }
        
    $('#subcatogeryForm').on('submit', function(event) {
        $('.name_input').each(function() {
            $(this).rules("add", 
                {
                    required: true,
                    messages: {
                        required: "This field is required",
                    }
                });
        }); 
    });

    $("#subcatogeryForm").validate();
});

//=================End Multiple Textbox================//

//=================Sub Category Check==============//
function check_Subcategory() {
    
    var subCategory_name = $('#sub_category_name').val();
    var category_id = $('#category_id').val();
     
    $.ajax({
        url: '{{ url('admin-category/checkSubcategory/') }}/'+subCategory_name+ '/' +category_id,
        type: 'get',
        dataType: 'json',
        success:function(response){
            if(response == ""){
                $('#error').hide();
                $("#submitbtn").attr("disabled", false); 
            }else{
               $('#error').show();
                $("#submitbtn").attr("disabled", true);
                $("#container").prop("disabled", true);
                //$("#container").children().attr("disabled","disabled");   
            }
        }
    });
    }

function check_on_Subcategory() {
    var counter = parseInt($('#counter').val());
    
    /*for(i=1;i<counter;i++){
        alert("Hey");
    }*/

    var subCategory = $('.sub_cat').val();
    
    var category_id = $('#category_id').val();

    $.ajax({
        url: '{{ url('admin-category/check_on_Subcategory/') }}/'+subCategory+ '/' +category_id,
        type: 'get',
        dataType: 'json',
        success:function(response){
            if(response == ""){
                $('#error').hide();
                $("#submitbtn").attr("disabled", false); 
            }else{
               $('#error').show();
                $("#submitbtn").attr("disabled", true);
                $("#container").prop("disabled", true);                    
            }

        }
    });

}

function countChar(){

    var len = $('.description').val().length;
    var str= $('.description').val();
    var text=1;
        if (len >= 200) {
            text=parseInt(len)/200;
            $('.msg-count').text(Math.ceil(text));
            $('.sms-count').text(Math.ceil(text));
            $('#msg-char').text(len);
            $('.description').val(str.replace(/[^\x00-\x7F]/g, ""));
        } else {
            $('.sms-count').text('1');   
            $('.msg-count').text('1');   
            $('.description').val(str.replace(/[^\x00-\x7F]/g, ""));
            $('#msg-char').text(len);
        }
 
};    
</script>

@endpush