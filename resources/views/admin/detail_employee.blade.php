@extends('admin.master')
@section('bodyData')
<div class="container-fluid">
  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
        <h6 class="modal-title" id="myModalLabel">Salary Details</h6>
          <button type="button" class="close closeModal" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body">
          <form method='post' id="salaryDetails" action='{{ route('AdeminEmpResult') }}' class="from-approve" style="display: none;">
            @csrf
              <input type='hidden' id="id" name='id' value="{{base64_encode($employeeProfile->id)}}">
              <input type='hidden' id="status" name='status'>
                <div class="col-md-12">
                  <label>Offered Salary</label>
                  <div class="row">
                    <div class="col-md-6 form-group">
                      <input type="number" class="form-control" name="basic" min="0" placeholder="Basic" required />
                    </div>
                    <div class="col-md-6 form-group">
                      <input type="number" class="form-control" name="hra" min="0" placeholder="HRA" required />
                    </div>
                    <div class="col-md-6 form-group">
                      <input type="number" class="form-control" name="other_allowance" min="0" placeholder="Other Allowance" required />
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <label>Variable Pay</label>
                  <div class="row">
                    <div class="col-md-12 form-group">
                      <input type="number" class="form-control" name="variable_pay" min="0" placeholder="Variable Pay" required />
                    </div>
                  </div>
                </div>
              <button type='submit' class='btn btn-primary'>Approve</button>
          </form>
          <form method='post' action='{{ route('AdeminEmpResult') }}' class="from-reject" style="display: none;">
            @csrf
              <input type='hidden' id="id" name='id' value="{{base64_encode($employeeProfile->id)}}">
              <input type='hidden' id="status_rej" name='status'>
              <button type='submit' class='btn btn-primary'>Reject</button>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default closeModal" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  <br>
  <a href="{{url('/adminshowemployee')}}"><button class='btn btn-primary'>Show All Employees</button></a>
  @if($employeeProfile->send_employment == 1)
    <a href="#" data-toggle="modal" data-backdrop="false" data-target="#myModal">
      <button class='btn btn-success approve'>Approve</button></a>
    <a href="#" data-toggle="modal" data-backdrop="false" data-target="#myModal"><button class='btn btn-danger reject'>Reject</button></a>
  @endif
  <br/>
  <br>
  <div class="row" style="width: 1200px;">
    <div class="col-lg-12">
      <ul class="nav nav-tabs">
       <li class="nav-item">
        <a class="nav-link active" data-toggle='tab' href="#profile">Profile</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-toggle='tab' href="#salary">Address</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-toggle='tab' href="#Documents">Documents</a>

      </li>
      <li class="nav-item">
        <a class="nav-link" data-toggle='tab' href="#family">Family Detail</a>

      </li>
      
      <li class="nav-item">
        <a class="nav-link" data-toggle='tab' href="#emp">Employment Details</a>
      </li>
    </ul>
    
      <div class="tab-content">
        <div id="salary" class="container tab-pane">
          <div class="row">
            <div class="col-md-12">
              <br>
              @if(isset($employeeProfile) && isset($p_cityname) && isset($p_statename) && isset($c_cityname) && isset($c_statename))
              <div class="container">
                <div class="card-deck row">
                  <div class="card">
                    <div class="card-body">
                      <div class="row">
                        <div class="col-md-6">
                          <h4 style="font-weight: bold; margin-left: 28%;">Current Address</h4>
                          <br>
                          <div class="form-group row">
                            <label for="staticEmail" class="col-md-6 col-form-label" style="font-weight: bold;">House No:</label>
                            <div class="col-md-6">
                              <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="{{$employeeProfile->c_house_no}}">
                            </div>
                          </div>
                          <div class="form-group row">
                            <label for="staticEmail" class="col-md-6 col-form-label" style="font-weight: bold;">Society Name:</label>
                            <div class="col-md-6">
                              <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="{{$employeeProfile->c_society_name}}">
                            </div>
                          </div>
                          <div class="form-group row">
                            <label for="staticEmail" class="col-md-6 col-form-label" style="font-weight: bold;">Land Mark:</label>
                            <div class="col-md-6">
                              <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="{{$employeeProfile->c_landmark}}">
                            </div>
                          </div>
                          <div class="form-group row">
                            <label for="staticEmail" class="col-md-6 col-form-label" style="font-weight: bold;">City:</label>
                            <div class="col-md-6">
                              <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="{{$c_cityname->name}}">
                            </div>
                          </div>
                          <div class="form-group row">
                            <label for="staticEmail" class="col-md-6 col-form-label" style="font-weight: bold;">State:</label>
                            <div class="col-md-6">
                              <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="{{$c_statename->name}}">
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <h4 style="font-weight: bold; margin-left: 28%">Permenant Address</h4>
                          <br>
                          <div class="form-group row">
                            <label for="staticEmail" class="col-md-6 col-form-label" style="font-weight: bold;">House Number:</label>
                            <div class="col-md-6">
                              <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="{{$employeeProfile->p_house_no}}">
                            </div>
                          </div>
                          <div class="form-group row">
                            <label for="staticEmail" class="col-md-6 col-form-label" style="font-weight: bold;">Society_name:</label>
                            <div class="col-md-6">
                              <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="{{$employeeProfile->p_society_name}}">
                            </div>
                          </div>
                          <div class="form-group row">
                            <label for="staticEmail" class="col-md-6 col-form-label" style="font-weight: bold;">Landmark:</label>
                            <div class="col-md-6">
                              <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="{{$employeeProfile->p_landmark}}">
                            </div>
                          </div>
                          <div class="form-group row">
                            <label for="staticEmail" class="col-md-6 col-form-label" style="font-weight: bold;">City:</label>
                            <div class="col-md-6">
                              <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="{{$p_cityname->name}}">
                            </div>
                          </div>
                          <div class="form-group row">
                            <label for="staticEmail" class="col-md-6 col-form-label" style="font-weight: bold;">State:</label>
                            <div class="col-md-6">
                              <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="{{$p_statename->name}}">
                            </div>
                          </div>
                          <div class="form-group row">
                            <label for="staticEmail" class="col-md-6 col-form-label" style="font-weight: bold;">Pincode:</label>
                            <div class="col-md-6">
                              <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="{{$employeeProfile->p_pincode}}">
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div> 
              </div>
              @else
                <div>No data filled</div>
              @endif
            </div>
          </div>
        </div>

        <div id="profile" class="container tab-pane active">
          <div class="row" style="width: 1350px;">
            <div class="col-lg-12">
              <br>
              @if($employeeProfile)
              <div class="container">
                <div class="card-deck row" style="width: 1250px;">
                  <div class="card">
                    <div class="card-body">
                      <div class="row">
                        <div class="col-md-3">
                          <div class="form-group ">
                            <label for="staticEmail" class="form-label" style="font-weight: bold;">Employee Photo:</label><br>
                            <img style='height:220px;width:250px' src='{{url("/avatar/",$employeeProfile->avatar)}}'>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label for="staticEmail" class="col-md-6 col-form-label" style="font-weight: bold;">Employee Id:</label>
                            <div class="col-md-6">
                              <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="JJ-EMP-{{$employeeProfile->id}}" style="outline: none;">
                            </div>
                          </div>
                          <div class="form-group row">
                            <label for="staticEmail" class="col-md-6 col-form-label" style="font-weight: bold;">Designation:</label>
                            <div class="col-md-6">
                              <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="{{$employeeProfile->designation}}" style="outline: none;">
                            </div>
                          </div>
                          <div class="form-group row">
                            <label for="staticEmail" class="col-md-6 col-form-label" style="font-weight: bold;">Prefix:</label>
                            <div class="col-md-6">
                              <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="{{ucfirst($employeeProfile->prefix)}}" style="outline: none;">
                            </div>
                          </div>
                          <div class="form-group row">
                            <label for="staticEmail" class="col-md-6 col-form-label" style="font-weight: bold;">Name:</label>
                            <div class="col-md-6">
                              <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="{{$employeeProfile->name}}{{$employeeProfile->middlename}}{{$employeeProfile->surname}}" style="outline: none;">
                            </div>
                          </div>
                          <div class="form-group row">
                            <label for="staticEmail" class="col-md-6 col-form-label" style="font-weight: bold;">Contact Number:</label>
                            <div class="col-md-6">
                              <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="{{$employeeProfile->contact_number}}" style="outline: none;">
                            </div>
                          </div>
                          <div class="form-group row">
                            <label for="staticEmail" class="col-md-6 col-form-label" style="font-weight: bold;">Alternate Number:</label>
                            <div class="col-md-6">
                              <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="{{$employeeProfile->contact_number_alt}}" style="outline: none;">
                            </div>
                          </div>
                          <div class="form-group row">
                            <label for="staticEmail" class="col-md-6 col-form-label" style="font-weight: bold;">Whatsapp Number:</label>
                            <div class="col-md-6">
                              <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="{{$employeeProfile->whatsup_number}}" style="outline: none;">
                            </div>
                          </div>
                          <div class="form-group row">
                            <label for="staticEmail" class="col-md-6 col-form-label" style="font-weight: bold;">Email:</label>
                            <div class="col-md-6">
                              <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="{{$employeeProfile->email}}" style="outline: none;">
                            </div>
                          </div>
                          <div class="form-group row">
                            <label for="staticEmail" class="col-md-6 col-form-label" style="font-weight: bold;">Where did you get to know about vacancy:</label>
                            <div class="col-md-6">
                              <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="{{$employeeProfile->know_jagjoyu}}" style="outline: none;">
                            </div>
                          </div>
                          <div class="form-group row">
                            <label for="staticEmail" class="col-md-6 col-form-label" style="font-weight: bold;">Reference Name:</label>
                            <div class="col-md-6">
                              <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="{{$employeeProfile->reference_name}}" style="outline: none;">
                            </div>
                          </div>
                          <div class="form-group row">
                            <label for="staticEmail" class="col-md-6 col-form-label" style="font-weight: bold;">Employee Status:</label>
                            <div class="col-md-6">
                              <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="@if($employeeProfile->employee_approved == 0) Pending @elseif($employeeProfile->employee_approved == 1) Approved @elseif($employeeProfile->employee_approved == 2) Rejected @endif" style="outline: none;">
                            </div>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group row">
                            <label for="staticEmail" class="col-md-6 col-form-label" style="font-weight: bold;">Bank Name:</label>
                            <div class="col-md-6">
                              <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="{{$employeeProfile->bank_name}}" style="outline: none;">
                            </div>
                          </div>
                          <div class="form-group row">
                            <label for="staticEmail" class="col-md-6 col-form-label" style="font-weight: bold;">Account Number:</label>
                            <div class="col-md-6">
                              <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="{{$employeeProfile->acc_no}}" style="outline: none;">
                            </div>
                          </div>
                          <div class="form-group row">
                            <label for="staticEmail" class="col-md-6 col-form-label" style="font-weight: bold;">IFSC Code:</label>
                            <div class="col-md-6">
                              <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="{{$employeeProfile->ifsc_code}}" style="outline: none;">
                            </div>
                          </div>
                          <div class="form-group row">
                            <label for="staticEmail" class="col-md-6 col-form-label" style="font-weight: bold;">Salary:</label>
                            <div class="col-md-6">
                              <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="{{$employeeProfile->salary}}" style="outline: none;">
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div> 
              </div>
              @else
                <div>No data filled</div>
              @endif
            </div>
          </div>
        </div>
        <!-- /*************Family Detail ***************** */ -->
        <div id="family" class="container tab-pane" style='min-height:400px'>
          <div class="row">
            <div class="col-sm-8">
              @if(isset($employeeProfile))
                <table class="table table-striped">
                  <thead>
                    <tr>
                      <th scope="col">Prefix</th>
                      <th scope="col">Name</th>
                      <th scope="col">Mobile</th>
                      <th scope="col">Email</th>
                      <th scope="col">Address</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                    if(isset($employeeProfile)){$result = $employeeProfile->family_details; }else{$result = [];}
                    $family_data = json_decode($result,true);
                    ?>
                    @if(isset($family_data))
                    <?php  foreach ($family_data as $value) {
                      ?>
                      <tr>

                       <td><?php echo $value['prefix'];?></td>
                       <td><?php echo $value['name'];?></td>
                       <td><?php echo $value['mobile'];?></td>
                       <td><?php echo $value['email'];?></td>
                       <td><?php echo $value['address'];?></td>

                     </tr>
                     <?php } ?>
                    @endif

                   </tbody>
                 </table>
              @else
                <div>No data filled</div>
              @endif
            </div>
          </div>	
        </div>
        <!-- /*******************EmployeementDetails********************* */ -->
        <div id="emp" class="container tab-pane" style='min-height:400px'>
          @if($employeeProfile)
            <div class="row">
              <div class="col-sm-8">
                <table class="table table-striped">
                  <thead>
                    <!-- [{"companyname":"Version Up","sdate":"2020-11-03","edate":"2020-11-21","designation":"PHP Developer","salary":"20000","reason":"Finance Level "}] -->
                    <tr>
                      <th scope="col">Companyname</th>
                      <th scope="col">Sdate</th>
                      <th scope="col">Edate</th>
                      <th scope="col">Designation</th>
                      <th scope="col">Salary</th>
                      <th scope="col">Reason</th>
                    </tr>
                  </thead>
                  @if(isset($employeeProfile))
                    <tbody>
                      <?php 
                      if(isset($employeeProfile)){$result = $employeeProfile->employeement_details;}else{
                        $result = [];
                      }
                      $employeement_details = json_decode($result,true);
                      ?>
                      @if(isset($employeement_details))
                      <?php  foreach ($employeement_details as $value) {
                        ?>
                        <tr>
                          <td><?php echo $value['companyname'];?></td>
                          <td><?php echo $value['sdate'];?></td>
                          <td><?php echo $value['edate'];?></td>
                          <td><?php echo $value['designation'];?></td>
                          <td><?php echo $value['salary'];?></td>
                          <td><?php echo $value['reason'];?></td>


                        </tr>
                        <?php } ?>
                      @endif
                      </tbody>
                    </table>
                  @else
                  <div>No data filled</div>
                  @endif
                </div>
            </div>
            
          @else
            <div>No data filled</div>
          @endif
        </div>

        <!-- ********************Documents ************************ -->

        <div id="Documents" class="container tab-pane" name='tab' style='min-height:400px'>
          @if($employeeProfile)
            <table class="table table-striped">
              <thead>
                <tr>
                  <th scope="col">Name</th>
                  <th scope="col">Id</th>
                  <th scope="col">Document</th>
                  
                </tr>
              </thead>
              <tbody>
                      @if($employeeProfile->aadhar_card_pic)
                      <tr>
                        <td>Adhar Card</td>
                         <td><?php echo $employeeProfile->aadhar_card_id?></td>
                        <td><a target="_blank" href="{{url('/documentemp?id=')}}{{$employeeProfile->id}}&document=aadhar_card_pic"><button class="btn btn-primary btn-sm">Adhar Card</button></a></td>
                      </tr>
                      @endif
                       @if($employeeProfile->driving_photo)
                      <tr>
                        <td>driving Card</td>
                        <td><?php echo $employeeProfile->driving_id?></td>
                        <td><a target="_blank" href="{{url('/documentemp?id=')}}{{$employeeProfile->id}}&document=driving_photo"><button class="btn btn-primary btn-sm">driving Card</button></a></td>
                      </tr>
                      @endif
                      @if($employeeProfile->pancard_photo)
                      <tr>
                        <td>Pan Card</td>
                        <td><?php echo $employeeProfile->pancard_id?></td>
                        <td><a target="_blank" href="{{url('/documentemp?id=')}}{{$employeeProfile->id}}&document=pancard_photo"><button class="btn btn-primary btn-sm">Pan Card</button></a></td>
                      </tr>
                      @endif
                      @if($employeeProfile->passport_photo)
                      <tr>
                        <td>Passport</td>
                         <td><?php echo $employeeProfile->passport_id?></td>
                        <td><a target="_blank" href="{{url('/documentemp?id=')}}{{$employeeProfile->id}}&document=passport_photo"><button class="btn btn-primary btn-sm">Passport</button></a></td>
                      </tr>
                      @endif
                      @if($employeeProfile->marksheet_10th_photo)
                      <tr>
                        <td>10Th Marksheet</td>
                        <td></td>
                        <td><a target="_blank" href="{{url('/documentemp?id=')}}{{$employeeProfile->id}}&document=marksheet_10th_photo"><button class="btn btn-primary btn-sm">10Th Marksheet</button></a></td>
                      </tr>
                      @endif
                      @if($employeeProfile->marksheet_12th_photo)
                      <tr>
                        <td>12Th Marksheet</td>
                        <td></td>
                        <td><a target="_blank" href="{{url('/documentemp?id=')}}{{$employeeProfile->id}}&document=marksheet_12th_photo"><button class="btn btn-primary btn-sm">12Th Marksheet</button></a></td>
                      </tr>
                      @endif
                      @if($employeeProfile->marksheet_bachelors_photo)
                      <tr>
                        <td>Bachelors Degree Certificate</td>
                        <td></td>
                        <td><a target="_blank" href="{{url('/documentemp?id=')}}{{$employeeProfile->id}}&document=marksheet_bachelors_photo"><button class="btn btn-primary btn-sm">Bachelors Degree Certificate</button></a></td>
                      </tr>
                      @endif
                      @if($employeeProfile->marksheet_masters_photo)
                      <tr>
                        <td>Master Degree Certificate</td>
                        <td></td>
                        <td><a target="_blank" href="{{url('/documentemp?id=')}}{{$employeeProfile->id}}&document=marksheet_masters_photo"><button class="btn btn-primary btn-sm">Master Degree Certificate</button></a></td>
                      </tr>
                      @endif
                      @if($employeeProfile->appointment)
                      <tr>
                        <td>Appointment Letter</td>
                        <td></td>
                        <td><a target="_blank" href="{{url('/documentemp?id=')}}{{$employeeProfile->id}}&document=appointment"><button class="btn btn-primary btn-sm">Appointment Letter</button></a></td>
                      </tr>
                      @endif
                      @if($employeeProfile->offerLetter)
                      <tr>
                        <td>Offer Letter</td>
                        <td></td>
                        <td><a target="_blank" href="{{url('/documentemp?id=')}}{{$employeeProfile->id}}&document=appointment"><button class="btn btn-primary btn-sm">Offer Letter</button></a></td>
                      </tr>
                      @endif
                      @if($employeeProfile->experience)
                      <tr>
                        <td>Experience Letter</td>
                        <td></td>
                        <td><a target="_blank" href="{{url('/documentemp?id=')}}{{$employeeProfile->id}}&document=appointment"><button class="btn btn-primary btn-sm">Experience Letter</button></a></td>
                      </tr>
                      @endif
                      @if($employeeProfile->resignation)
                      <tr>
                        <td>Resignation Letter</td>
                        <td></td>
                        <td><a target="_blank" href="{{url('/documentemp?id=')}}{{$employeeProfile->id}}&document=appointment"><button class="btn btn-primary btn-sm">Resignation Letter</button></a></td>
                      </tr>
                      @endif
                      @if($employeeProfile->increment)
                      <tr>
                        <td>Last Increment Letter</td>
                        <td></td>
                        <td><a target="_blank" href="{{url('/documentemp?id=')}}{{$employeeProfile->id}}&document=appointment"><button class="btn btn-primary btn-sm">Last Increment Letter</button></a></td>
                      </tr>
                      @endif
                      @if($employeeProfile->statment)
                      <tr>
                        <td>Bank Statement</td>
                        <td></td>
                        <td><a target="_blank" href="{{url('/documentemp?id=')}}{{$employeeProfile->id}}&document=appointment"><button class="btn btn-primary btn-sm">Bank Statment</button></a></td>
                      </tr>
                      @endif
                      <?php if($employeeProfile->salaryslips){
                        $temp = explode(",",$employeeProfile->salaryslips);
                        $i=1;
                        foreach($temp as $t)
                         { ?>
                         <tr>
                          <td>Salary Slip{{$i}}</td>
                          <td></td>
                          <td><a target="_blank" href="{{url('/documentemp?id=')}}{{$employeeProfile->id}}&document=appointment"><button class="btn btn-primary btn-sm">Salary Slip{{$i}}</button></a></td>
                        </tr>
                          <?php $i++;} 
                        } ?>
                        <?php if($employeeProfile->any_other_certificates){
                          $temp = explode(",",$employeeProfile->any_other_certificates);
                          foreach($temp as $t)
                           { $i=1; ?>
                           <tr>
                            <td>Certificate{{$i}}</td>
                            <td></td>
                            <td><a target="_blank" href="{{url('/documentemp?id=')}}{{$employeeProfile->id}}&document=any_other_certificates"><button class="btn btn-primary btn-sm">Certificate{{$i}}</button></a></td>
                          </tr>
                             <?php $i++; } 
                           } ?>
              </tbody>
            </table>
          @else
            <div>No data filled</div>
          @endif
        </div>
      </div>
    </div>
  </div>
 @endsection
@push('css')
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css"/>
@endpush
@push('js')
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js"></script>

    <script>
      $(function(){
        $('#salaryDetails').bootstrapValidator({
          fields: {
              basic: {
                  message: 'Basic Amount is required',
              },
              hra: {
                  message: 'HRA is required',
              },
              other_allowance: {
                  message: 'Other Allowance is required',
              },
              variable_pay: {
                  message: 'Variable Pay is required',
              },
          }
        });
      });

      $(document).on('click','.approve',function(){
        $('#status').val('Approve');
        $('#myModalLabel').html('Salary Details');
        $('.from-reject').hide();
        $('.from-approve').show();
        $('#submit').html('Approve');
      });

      $(document).on('click','.reject',function(){
        $('#status_rej').val('Reject');
        $('#myModalLabel').html('Are You Sure ?');
        $('.from-approve').hide();
        $('.from-reject').show();
        $('#submit').html('Reject');
      });

      $(document).on('click','.closeModal',function(){
        $('#salaryDetails').bootstrapValidator('resetForm', true);
      });
    </script>
@endpush