@extends('admin.master')
@section('bodyData')
<meta name="csrf-token" content="{{ csrf_token() }}">

<script>
                   function statechange(r)
                { 
                    $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var cat_id = r.value;
            console.log(cat_id);

                 $.ajax({
                   
                       url:"{{ route('getCity') }}?id=" + cat_id ,
                       type:"POST",
                       data: {
                           cat_id: cat_id
                        },
                      
                       success:function (data) {
                        $('#c_city_id').empty();
                        // console.log(data.cities);

                        $.each(data.cities,function(index,cities){
                            //console.log(cities);
                            
                            $('#c_city_id').append('<option value="'+index+'">'+cities+'</option>');
                        })
                       }
                   })
                }
               
                   </script>

<section class="forms-section" style='min-height:500px'>
<div class="outer-w3-agile mt-3">
<a class='btn btn-primary' href='/adminshowagent'>Show Agents</a>
        <h4 class="tittle-w3-agileits mb-4">Agent Updation</h4>
        <form action="{{url('updateagent')}}" method="post">
        @csrf
      <div class="form-row">
               
                <div class="form-group col-md-6">
                    <label for="inputEmail4">First Name</label>
                    <input type="text" class="form-control" id="inputEmail4" placeholder="Enter First Name" required="" name='first_name' value='<?php echo $agentData[0]->first_name?>' pattern='[A-Za-z]{2,30}'>
                </div>
                <div class="form-group col-md-6">
                   <label for="inputEmail4">Last Name</label>
                   <input type="text" class="form-control" id="inputEmail4" placeholder="Enter  Last Name" required="" name='last_name' value='<?php echo $agentData[0]->last_name?>' pattern='[A-Za-z]{2,30}'>
               </div>
           </div>
           <input type='hidden' name='id' value='<?php echo $agentData[0]->aid?>'>
                <div class="form-row">
               <div class="form-group col-md-6">
                   <label for="inputEmail4">Company Name</label>
                   <input type="text" class="form-control" id="inputEmail4" placeholder="Enter Company Name" required="" name='company_name' value='<?php echo $agentData[0]->company_name?>'>
               </div>
               <div class="form-group col-md-6">
                   <label for="inputEmail4">Contatct Details</label>
                   <input type="text" class="form-control" id="inputEmail4" placeholder="Enter Contact Detail" required="" name='contact_details' value='<?php echo $agentData[0]->contact_details?>'>
               </div>
          </div>

          
          <div class="form-row">
               
               <div class="form-group col-md-6">
                   <label for="inputEmail4">Alternate Number</label>
                   <input type="text" class="form-control" required="" name='alternate_number' Placeholder='Enter Alternate number' value='<?php echo $agentData[0]->alternate_number?>' pattern='[0-9]{10}'>
               </div>

               <div class="form-group col-md-6">
                   <label for="inputEmail4">Company Pan</label>
                   <input type="text" class="form-control" required="" name='company_pan' placeholder='Enter Company Pan' value='<?php echo $agentData[0]->company_pan ?>'>
               </div>
               
          </div>

          <div class="form-row">
               
               <div class="form-group col-md-6">
                   <label for="inputEmail4">Company GST</label>
                   <input type="text" class="form-control" required="" name='company_gst_no' placeholder='Enter Company GST ' value='<?php echo $agentData[0]->company_gst_no  ?>'>
               </div>

               <div class="form-group col-md-6">
                   <label for="inputEmail4">Company Email</label>
                   <input type="email" class="form-control" required="" name='company_email' placeholder='Enter Email' value='<?php echo  $agentData[0]->company_email?>'>
               </div>
               
          </div>
          <div class="form-row">
               
               <div class="form-group col-md-6">
                   <label for="inputEmail4">House No</label>
                   <input type="text" class="form-control" required="" name='house_no' placeholder='Enter House No ' value='<?php echo $agentData[0]->house_no?>'>
               </div>

               <div class="form-group col-md-6">
                   <label for="inputEmail4">Society Name </label>
                   <input type="text" class="form-control" required="" name='society_name' placeholder='Enter Society Name ' value='<?php echo $agentData[0]->society_name ?>'>
               </div>
               
          </div>

          <div class="form-row">
               
               <div class="form-group col-md-6">
                   <label for="inputEmail4">Land Mark</label>
                   <input type="text" class="form-control" required="" name='landmark' placeholder='Enter Landmark' value='<?php  echo $agentData[0]->landmark ?>'>
               </div>

               <div class="form-group col-md-6">
                   <label for="inputEmail4">Pincode </label>
                   <input type="text" class="form-control" required="" name='pincode' placeholder='Enter Pincode Number ' value='<?php echo $agentData[0]->pincode ?>'>
               </div>
               
          </div>
          

                  
          <div class="form-row">
          <div class="form-group col-md-6">
                   <label for="inputEmail4">State</label>
    <select class="form-control" name='state_fk_id' id='c_state_id' onchange='statechange(this)'>
                    <option value=''>Select State</option>
                        <?php
                     
                        foreach ($state as $key => $s) { ?>
                           
        <option value='<?php echo $s->id?>' id='stateid' <?php if($s->id == $agentData[0]->state_fk_id)echo 'selected'?>/>
                         <?php    print_r($s->name);
                            ?>
                            </option>
                         <?php
                        }
                      ?>
                   </select>
    
                   
                 </div>
                 <div class="form-group col-md-6">
                   <label for="inputEmail4">City</label>
                   <select id="c_city_id" class="form-control" name='city_fk_id' required>
                  
                   <option value='<?php echo $agentData[0]->city_fk_id?>'><?php echo $agentData[0]->cityname?></option>
                   
                   </select>
               </div>
               <!-- <div class="form-group col-md-6">
                  
                   <label for="inputEmail4">System Password </label>
                   <input type="text" class="form-control" required="" name='password' placeholder='Enter Password' value='<?php echo $agentData[0]->password ?>'>
               </div> -->
               <div class="form-group col-md-6">
                 
              <a> <button type="submit" class="btn btn-primary" style='margin-top:13px'>Update Agent Pofile</button></a>
               </div>
              

            
        </form>
    </div>

</section>

@endsection
