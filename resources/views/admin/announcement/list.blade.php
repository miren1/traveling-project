@extends('admin.master')
@section('bodyData')
<section class="forms-section" style='min-height:500px'>
<div class="outer-w3-agile mt-3">
    <div class='row'>
      <div class='col-md-6'>
        <div class="btn btn-primary"><i class="fa fa-list mr-2"></i>Announcement list</div>
      </div>
      <div class='col-md-6 text-right'>
        <a class="btn btn-primary" href="{{ route('announcements-add')}}"><i class="fa fa-plus mr-2"></i>Add Announcement</a>
      </div>
    </div><br>
    <div class="table-responsive">
      <table class="table table-hover" style="width: 100%;" id="dataTable">
        <thead class="thead-dark">
          <tr>
            <th>#</th>
            <th>Content</th>
            <th>Image</th>
            <th>Assign</th>
            <th>Action</th>
          </tr>
        </thead>
      </table>
    </div>
  </div>
</section>
@endsection
@push('css')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css">
@endpush
@push('js')
<script>
  $(function() {
    $("#dataTable").DataTable({
        'processing': true,
        'serverSide': true,
        'serverMethod': 'post',
        "aLengthMenu": [[10,25, 50,100], [10,25, 50,100]],
        order: [
            [0, 'desc']
        ],
        "ajax": {
            "url": "{{ route('announcements-list') }}",
            "data": {
                "_token": "{{ csrf_token() }}"
            },
        },
        "columns": [
            {
                data: "id",
                render: function(data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1
                }

            },
            {
                data: "content"
            },
            {
                data: "image",
               
                  render: function(data, type, row, alldata) {
                    //console.log(pathinfo(data));
                    var h = 'https://';
                    var img = h+document.domain+'/'+data;
                    return "<img style='width:40px;' src="+img+">";
                    //return img;
                  

                }
            },
            {
                data: "id",orderable:false,
                render: function(data, type, row, alldata) {
                  var mytag = '';
                  //console.log(row.employee=="1");
                  if(row.employee=="1"){
                    mytag +="<span class='badge badge-info ml-1'>Employee</span>";
                  }
                  if(row.agent=="1"){
                    mytag +="<span class='badge badge-info ml-1'>Agent</span>";
                  }
                  if(row.member=="1"){
                    mytag +="<span class='badge badge-info ml-1'>Member</span>";
                  }
                  if(row.user=="1"){
                    mytag +="<span class='badge badge-info ml-1'>User</span>";
                  }
                  if(row.all_user=="1"){
                    mytag +="<span class='badge badge-info ml-1'>All User</span>";
                  }
                  return mytag;
                  
                }
            },
            {
                data: "id",orderable:false,
                render: function(data, row, alldata) {
                 // console.log(data);
                  var $id = btoa(data);
                    $html = `<a href="{{url('announcements/edit')}}/`+$id+`">
                         <!-- <button class="btn btn-info">Update</button> -->
                         <i class="fa fa-edit" aria-hidden="true" title='Edit' style='font-size:20px'></i>
                         </a>
                      
                      <i class="fa fa-trash delete" style='font-size:20px;color:red;cursor:pointer;' title='delete' data-id="`+data+`" >
                      </i>`;
                    return $html;
                }
            },
        ],
        "columnDefs": [ {
          'targets': [0,1,2,3], /* table column index */
          'orderable': false, /* true or false */
       }]
    });

    $(document).on("click", '.delete', function(event) { 
    var id =$(this).attr('data-id');
    //alert(id);
    swal({
      title: "Are you sure?",
      text: "You Want To Delete This Record ,You Will Not Be Able To Recover This Record!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {

        $.ajax({
          url: "{{ url('announcements/delete') }}/"+id,
          type: 'get',
          dataType: 'json',
        success:function($data){
          if($data == true)
          {
            swal("Record has been deleted!", {
              icon: "success",
            }).then((result) => {
                $('#dataTable').DataTable().ajax.reload(null,false);
            });
            //location.reload('#dataTable');
          }else{
            swal("Record has been not deleted!", {
              icon: "error",
            });
          }
    
        }
      });


      } else {
        swal("Your Record is Safe!");
      }
    });
  });

    $(document).on('click','.leave',function(){
      var id = $(this).data('id');
      // alert('true');
      $('#modal-myvalue').val(id);
    });
  });
</script>
@endpush
