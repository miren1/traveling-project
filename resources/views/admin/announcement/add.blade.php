@extends('admin.master')
@section('bodyData')
<section class="forms-section" style='min-height:500px'>
<div class="outer-w3-agile mt-3">
        <h4 class="tittle-w3-agileits mb-4">@if(isset($announce))Edit @else Add @endif Announcement</h4>
       
        <form action="{{ route('announcements-store')}}" id="AnnouncementsForm" method="post" enctype="multipart/form-data">
        @csrf
        {{-- dd($photos->id) --}}
            <div class="row">
                @if(isset($announce))
                    <input type="hidden" id="id" name="id" value="{{$announce->id}}"/>
                @endif
                <div class="col-md-6">
                    
                    <div class="form-group">
                        <label for="title">Content</label>
                        <textarea  class="form-control" name="Content_area" id="ContentText" rows="12" >@if(isset($announce)){{$announce->content}}@endif</textarea>
                        <p><span style="color:red; display: none" id="warning">* Please add some content</span></p>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail2">Select for visibility:</label>
                        <div class="form-row">&nbsp;&nbsp;
                         
                            <div class="checkbox">
                                <label><input class="ads_Checkbox" type="checkbox" value="1" id="all_user" onclick="checkalluser(this)" name='all_user' @if(isset($announce)) @if($announce->all_user==1) checked @endif @endif>&nbsp;Home</label>
                            </div> &nbsp;&nbsp; 
                            <div class="checkbox">
                                <label><input class="ads_Checkbox" type="checkbox" id="employee" onclick="checkemployee(this)" name='employee' @if(isset($announce)) @if($announce->employee==1) checked @endif @endif>&nbsp;Employee</label>
                                <!-- {{ (is_array(old('hobby')) && in_array(1, old('hobby'))) ? ' checked' : '' }} -->
                            </div> &nbsp;&nbsp;
                            <div class="checkbox">
                                <label><input class="ads_Checkbox" type="checkbox" id="agent" onclick="checkagent(this)" value="1" name='agent' @if(isset($announce)) @if($announce->agent=='1') checked @endif @endif>&nbsp;Agent</label>
                            </div>&nbsp;&nbsp;
                            <div class="checkbox">
                                <label><input class="ads_Checkbox" type="checkbox" id="member" onclick="checkmember(this)" value="1" name='member' @if(isset($announce)) @if($announce->member=='1') checked @endif @endif>&nbsp;Member</label>
                            </div>&nbsp;&nbsp;
                            <div class="checkbox">
                                <label><input class="ads_Checkbox" type="checkbox" id="user" onclick="checkuser(this)" value="1" name='user' @if(isset($announce)) @if($announce->user=='1') checked @endif @endif>&nbsp;User</label>
                            </div>
                            <!-- <span class="error" id="error" style="color:red; display:none;">This Announcement is already exist.</span> -->
                            
                        </div>  
                        <span id="emp" style="color: red;"></span>
                      </div> 
                </div> 
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="category">Images & Pdf</label>
                        <input data-browse-on-zone-click="true" onchange="SelectImage()"      class="form-control custom-file-input" id="input-id" type="file" name="image"  >
                    </div>

                    <div class="form-group">
                        @if(isset($announce))
                            <img class="img-container" src="{{$url}}/{{$announce->image}}">
                        @endif
                    </div> 
                </div>
            </div>
            <button type="submit" id="submitbtn" class="btn btn-primary">@if(isset($announce))Update @else Submit @endif</button>
        </form>
    </div>
</section>

@endsection
@push('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.min.css" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.8/css/fileinput.min.css">
<link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel = "stylesheet">
<link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel = "stylesheet">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css"/>
<style type="text/css">
    .img-container{
        width: 15%;
        display: initial;
    }
    .img-container img{
        justify-content: center;
        margin-left: 5px;
        margin-right: 5px;
        border-radius: 20px;
        height: 100px;
    }
</style>
@endpush

@push('js')
<script src="//cdn.ckeditor.com/4.16.2/standard/ckeditor.js"></script>
<script src="{{ url('js/ckfinder/ckfinder.js') }}"></script>
<script src = "https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="https://cdn.jsdelivr.net/npm/piexifjs@1.0.6/piexif.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.8/js/fileinput.min.js"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js"></script>
<script>
    $( document ).ready(function() 
    {
        $('#AnnouncementsForm').bootstrapValidator();
        var editor =  CKEDITOR.replace( 'Content_area' );
        CKFinder.setupCKEditor( editor );
    });

    $(window).bind("pageshow", function() {
        var form = $('form'); 
        // let the browser natively reset defaults
        form[0].reset();
    });


    $("#AnnouncementsForm").submit( function(e) {
        var messageLength = CKEDITOR.instances['ContentText'].getData().replace(/<[^>]*>/gi, '').length;
        if( !messageLength ) {
            // alert( 'Please enter a content' );
            $( "#warning" ).show();

            e.preventDefault();
        }
    });

@if(isset($announce))
$("#input-id").fileinput({
      'showUpload':false, 
      'showCancel':false,
      'showClose':false,
      'showRemove':false,
      'allowedFileExtensions': ["pdf","jpeg","jpg"],
       maxFileCount: 5,
       maxFileSize: 2000,
      fileActionSettings: {showZoom: false}
    });
@else
$("#input-id").fileinput({
      'showUpload':false, 
      'showCancel':false,
      'showClose':false,
      'showRemove':false,
      'allowedFileExtensions': ["pdf","jpeg","jpg"],
       maxFileCount: 5,
       maxFileSize: 2000,
       required: true,
      fileActionSettings: {showZoom: false}
    });
@endif

function SelectImage()
{

    // $('#submitbtn').removeAttr('disabled');
    return false;
}
$('#submitbtn').click(function() {
        if( document.getElementById("input-id").files.length == 0 ){
            console.log("no files selected");
            $('#submitbtn').removeClass('disabled'); 
            imagedata(false);
        }
        
    });
// function checkemployee(el){
//     console.log(el);
// }
function chekboxcheck()
{
    $.ajax({
        url: "{{ url('announcements/checkboxCheck') }}",
        type: 'get',
        dataType: 'json',
        success:function(data){
            if (data == true) {
                  $("#submitbtn").attr("disabled", true);
                  $('#emp').html('This announcement already exits!');
            }
        }
    });
}

function checkalluser(el){
    var user = el.checked;
    if(el.checked == true) {
      $.ajax({
            url: "{{ url('announcements/check') }}/"+user,
            type: 'get',
            dataType: 'json',
            success:function(data){
                if(data == true)
                { 
                   $('#emp').html('This announcement already exits!');
                   $("#submitbtn").attr("disabled", true);
                }
            }
        });
    }
    $('#emp').html('');
    $('#submitbtn').removeAttr('disabled');
    
}
// function Error()
// {
//     $('#emp').html('This announcement already exits!');
//     $("#submitbtn").attr("disabled", true);
//     return false;
// }

function checkemployee(el){
    var emp = el.checked;
    if(el.checked == true) {
      $.ajax({
            url: "{{ url('announcements/checkemployee') }}/"+emp,
            type: 'get',
            dataType: 'json',
            success:function(data){
                if(data == true)
                {
                   $('#emp').html('This announcement already exits!');
                   $("#submitbtn").attr("disabled", true);
                }
            }
        });
    }
        // SelectImage();
    $('#emp').html('');
    $('#submitbtn').removeAttr('disabled');
}

function checkagent(el){
    var emp = el.checked;
    if(el.checked == true) {
      $.ajax({
            url: "{{ url('announcements/checkagent') }}/"+emp,
            type: 'get',
            dataType: 'json',
            success:function(data){
                if(data == true)
                {
                   $('#emp').html('This announcement already exits!');
                   $("#submitbtn").attr("disabled", true);
                }
            }
        });
    }
    $('#emp').html('');
    $('#submitbtn').removeAttr('disabled');
}


function checkmember(el){
    var emp = el.checked;
    if(el.checked == true) {
      $.ajax({
            url: "{{ url('announcements/checkmember') }}/"+emp,
            type: 'get',
            dataType: 'json',
            success:function(data){
                if(data == true)
                {
                   $('#emp').html('This announcement already exits!');
                   $("#submitbtn").attr("disabled", true);
                }
            }
        });
    }
    $('#emp').html('');
    $('#submitbtn').removeAttr('disabled');
}

function checkuser(el){
    var emp = el.checked;
    if(el.checked == true) {
      $.ajax({
            url: "{{ url('announcements/checkuser') }}/"+emp,
            type: 'get',
            dataType: 'json',
            success:function(data){
                if(data == true)
                {
                   $('#emp').html('This announcement already exits!');
                   $("#submitbtn").attr("disabled", true);
                }
            }
        });
    }
    $('#emp').html('');
    $('#submitbtn').removeAttr('disabled');
}

</script> 

@endpush
