@extends('admin.master')
@section('bodyData')
<meta name="csrf-token" content="{{ csrf_token() }}">
<section class="forms-section" style='min-height:500px'>
<div class="outer-w3-agile mt-3">
        <h4 class="tittle-w3-agileits mb-4">Event Updation</h4>
        
        @if ($message = Session::get('AggentAdd'))
<div class="alert alert-success alert-block">
	<button type="button" class="close" data-dismiss="alert">×</button>	
        <strong>{{ $message }}</strong>
</div>
@endif
          
        <form action="{{url('updateevent')}}" method="post" enctype="multipart/form-data">
        @csrf
      <div class="form-row">
               
                <div class="form-group col-md-6">
                    <label for="inputEmail4">Name</label>
                    <input type="text" class="form-control" id="inputEmail4" placeholder="Enter Event Name" required="" name='name' value='<?php echo $data[0]->name?>'>
                </div>
                <div class="form-group col-md-6">
                   <label for="inputEmail4">Start Date</label>
                   <input type="date" class="form-control" id="inputEmail4" required="" name='sdate' value='<?php echo $data[0]->sdate?>'>
               </div>
           </div>
    <input type='hidden' name='id' value='<?php echo $data[0]->id ?>'>

          

          <div class="form-row">
               
               <div class="form-group col-md-6">
                   <label for="inputEmail4">End Date</label>
                   <input type="date" class="form-control" id="inputEmail4" placeholder="Enter Company Name" required="" name='edate' value='<?php echo $data[0]->edate?>'>
               </div>
               <div class="form-group col-md-6">
                   <label for="inputEmail4">Tagline</label>
                   <input type="text" class="form-control" id="inputEmail4" placeholder="Enter Contact Detail" required="" name='title' value='<?php echo $data[0]->title?>'>
               </div>
          </div>

          
          <div class="form-row">
               
               <div class="form-group col-md-6">
                   <label for="inputEmail4">Description</label>
                   <input type="text" class="form-control" required="" name='description' Placeholder='Enter Event Description' value='<?php echo $data[0]->description?>'>
               </div>

               <div class="form-group col-md-4">
                   <label for="inputEmail4">Image</label>
                   <input type="file" class="form-control" name='image' >
               </div>
               <div class="form-group col-md-2">
                   <label for="inputEmail4">Previous Image</label>
                   <br>
                   <img src='{{asset("uploads/events/"."/".$data[0]->image)}}' style='height:50px;width:50px'>
               </div>
               
          </div>

          <div class="form-row">
               
               <div class="form-group col-md-6">
                   <button class='btn btn-success'>Update Event</button>
               </div>
            </div>
        

         
         

          
          

                  
          
               
              
              
              

            
        </form>
    </div>

</section>

@endsection
