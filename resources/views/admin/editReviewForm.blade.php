@extends('admin.master')
@section('bodyData')
<meta name="csrf-token" content="{{ csrf_token() }}">
<section class="forms-section" style='min-height:500px'>
<div class="outer-w3-agile mt-3">
        <h4 class="tittle-w3-agileits mb-4">Review Edit</h4>
        <?php 



?>
    
          
        <form action="{{url('editreview')}}" method="post" enctype="multipart/form-data">
        @csrf
      <div class="form-row">
               
                <div class="form-group col-md-6">
                    <label for="inputEmail4">Name</label>
                    <input type="text" class="form-control" id="inputEmail4" placeholder="Enter  Name" required="" name='name' value='{{$data[0]->name}}'>
                </div>
                <div class="form-group col-md-6">
                   <label for="inputEmail4">Content</label>
                   <input type="text" class="form-control" id="inputEmail4" name='content' style='height:50px' value='{{$data[0]->content}}'>
               </div>
           </div>

           <input type='hidden' name='id' value='{{$data[0]->id}}'>
           <div class="form-row">
           <div class="form-group col-md-6">
                   <label for="inputEmail4">Previous Image</label>
                   <img src='<?php echo asset("reviews")."/".$data[0]->image?>' style='width:100px;height:100px'/>
               </div>
               <div class="form-group col-md-6">
                   <label for="inputEmail4">Image</label>
                   <input type="file" class="form-control" id="inputEmail4" name='image'>
               </div>
             
          </div>

          
         

          <div class="form-row">
               
               <div class="form-group col-md-6">
                   <button class='btn btn-success'>Edit Review</button>
               </div>
            </div>
        

         
         

          
          

                  
          
               
              
              
              

            
        </form>
    </div>

</section>

@endsection
