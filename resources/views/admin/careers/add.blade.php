@extends('admin.master')
@section('bodyData')
<section class="forms-section" style='min-height:500px'>
<div class="outer-w3-agile mt-3">
        <h4 class="tittle-w3-agileits mb-4">@if(isset($career))Edit @else Add @endif Career</h4>
       
        <form action="{{ route('career-store')}}" id="CareerForm" name="CareerForm" method="post" enctype="multipart/form-data">
        @csrf

            <div class="row">
               @if(isset($career))
                    <input type="hidden" id="id" name="id" value="{{$career->id}}"/>
                @endif
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="title">Department Name</label>
                        <input type="text" class="form-control" name="department" id="department" required data-bv-notempty-message="The Department Name is required" value="@if(isset($career)){{$career->department}}@endif">
                    </div>
                    <div class="form-group">
                        <label for="title">Opening Title</label>
                        <input type="text" class="form-control" name="opening_title" id="opening_title" required data-bv-notempty-message="The Opening Title is required" value="@if(isset($career)){{$career->opening_title}}@endif">
                    </div>
                    <div class="form-group">
                        <label for="title">Number of Openings</label>
                        <input type="text" class="form-control" name="number_of_openings" id="number_of_openings" required data-bv-notempty-message="The Number of Openings is required" value="@if(isset($career)){{$career->number_of_openings}}@endif">
                    </div>
                    <div class="form-group">
                        <label for="title">Relevant Experience Required</label>
                        <input type="text" class="form-control" name="relevant_experience_required" id="relevant_experience_required" required data-bv-notempty-message="The Relevant Experience Required is required" value="@if(isset($career)){{$career->relevant_experience_required}}@endif">
                    </div>
                    <div class="form-group">
                        <label for="title">Base Location</label>
                        <input type="text" class="form-control" name="base_location" id="base_location" required data-bv-notempty-message="The Base Location is required" value="@if(isset($career)){{$career->base_location}}@endif">
                    </div>
                    <div class="form-group">
                        <label for="thumbnail_image">Primary skills</label>
                        <textarea name="primary_skills">@if(isset($career)){{$career->primary_skills}}@endif</textarea>
                         <span style="color:red; display:none " id="warning">* Please add some content</span>
                    </div>
                </div>
                <div class="col-md-4">
                    <!-- <div class="form-group">
                        <label for="title">Roles and Responsibilities</label>
                        <textarea type="text" rows="5" class="form-control" name="roles_responsibilities" id="roles_responsibilities" required data-bv-notempty-message="The Roles and Responsibilities is required" value="@if(isset($career)) {{$career->roles_responsibilities}}@endif">@if(isset($career)){{$career->roles_responsibilities}}@endif</textarea>
                    </div> -->
                    <div class="form-group">
                        <label for="thumbnail_image">Roles and Responsibilities</label>
                        <textarea id="roles_responsibilities" name="roles_responsibilities">@if(isset($career)){{$career->roles_responsibilities}}@endif</textarea>
                         <span style="color:red; display: none" id="warning1">* Please add some content</span>
                    </div>
                    <!-- <div class="form-group">
                        <label for="title">Primary skills</label>
                        <input type="text" class="form-control" name="primary_skills" id="primary_skills" required data-bv-notempty-message="The Primary skills is required" value="@if(isset($career)){{$career->primary_skills}}@endif">
                    </div> -->
                    
                    <!-- <div class="form-group">
                        <label for="title">Pre Requisite</label>
                        <input type="text" class="form-control" name="pre_requiesite" id="pre_requiesite" required data-bv-notempty-message="The Pre Requisite is required" value="@if(isset($career)){{$career->pre_requiesite}}@endif">
                    </div> -->
                    <div class="form-group">
                        <label for="thumbnail_image">Pre Requisite</label>
                        <textarea name="pre_requiesite">@if(isset($career)){{$career->pre_requiesite}}@endif</textarea>
                         <span style="color:red; display: none" id="warning2">* Please add some content</span>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="type">Flexibility To Do Work From Home</label><br>

                        <input class="form-group" type="radio" name="work_flexibility" value="no" checked>
                        <label for="inlineRadio2">No</label>

                        <input class="form-group" type="radio" name="work_flexibility"value="yes" @if(isset($career) && $career->work_flexibility == 'yes') checked @endif>
                        <label for="inlineRadio1">Yes</label>
                    
                    </div>
                    <div class="form-group">
                        <label for="type">Salary Range</label><br>
                        <input class="form-group" type="radio" name="salary_range" value="no" checked>
                        <label for="inlineRadio2">No</label>

                        <input class="form-group" type="radio" name="salary_range" value="yes" @if(isset($career) && $career->salary_range == 'yes') checked @endif>
                        <label for="inlineRadio1">Yes</label>
                    </div>
                    <div class="form-group">
                        <label for="type">Incentives</label><br>
                        <input class="form-group" type="radio" name="incentives" value="no" checked>
                        <label for="inlineRadio2">No</label>

                        <input class="form-group" type="radio" name="incentives" value="yes" @if(isset($career) && $career->incentives == 'yes') checked @endif>
                        <label for="inlineRadio1">Yes</label>
                    </div>
                    <div class="form-group">
                        <label for="type">Benefits</label><br>
                        <input class="form-group" type="radio" name="benefits" value="no" checked>
                        <label for="inlineRadio2">No</label>

                        <input class="form-group" type="radio" name="benefits" value="yes" @if(isset($career) && $career->benefits == 'yes') checked @endif>
                        <label for="inlineRadio1">Yes</label>
                    </div>
                    <!-- <div class="form-group">
                        <label for="title">Advantage</label>
                        <input type="text" class="form-control" name="advantage" id="advantage" required data-bv-notempty-message="The Advantage is required" value="@if(isset($career)){{$career->advantage}}@endif">
                    </div> -->
                    <br><br><br><br>
                    <div class="form-group">
                        <label for="thumbnail_image">Advantage</label>
                        <textarea name="advantage">@if(isset($career)){{$career->advantage}}@endif</textarea>
                         <p><span style="color:red; display:none " id="warning3">* Please add some content</span></p>
                    </div>
                </div>
            </div>
            <button type="submit" id="submitbtn" class="btn btn-primary">@if(isset($ads))Update @else Submit @endif</button>
        </form>
    </div>
</section>
@endsection
@push('css')
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css"/>
@endpush
@push('js')
<script src="//cdn.ckeditor.com/4.16.2/standard/ckeditor.js"></script>
<script src="{{ url('js/ckfinder/ckfinder.js') }}"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js"></script>
<script>
    $( document ).ready(function() 
    {
        $('#CareerForm').bootstrapValidator();
        var editor =  CKEDITOR.replace( 'primary_skills' );
        var editor =  CKEDITOR.replace( 'roles_responsibilities' );
        var editor =  CKEDITOR.replace( 'pre_requiesite' );
        var editor =  CKEDITOR.replace( 'advantage' );
        CKFinder.setupCKEditor( editor );
    });


   $("#CareerForm").submit( function(e) {

    var messageLength = CKEDITOR.instances['primary_skills'].getData().replace(/<[^>]*>/gi, '').length;
        if( !messageLength ) {
            // alert( 'Please enter a content' );
            $( "#warning" ).show();
            e.preventDefault();    
        }

        var messageLength = CKEDITOR.instances['roles_responsibilities'].getData().replace(/<[^>]*>/gi, '').length;
        if( !messageLength ) {
            // alert( 'Please enter a content' );
            $( "#warning1" ).show();
            e.preventDefault();
            
        }
         
        var messageLength = CKEDITOR.instances['pre_requiesite'].getData().replace(/<[^>]*>/gi, '').length;
        if( !messageLength ) {
            // alert( 'Please enter a content' );
            $( "#warning2" ).show();
            e.preventDefault();
            
        }
        var messageLength = CKEDITOR.instances['advantage'].getData().replace(/<[^>]*>/gi, '').length;
        if( !messageLength ) {
            // alert( 'Please enter a content' );
            $( "#warning3" ).show();
            e.preventDefault();
            
        }
    });

    $( document ).ready(function() 
    {
        $('#CareerForm').bootstrapValidator();
        var editor =  CKEDITOR.replace( 'primary_skills' );
        var editor =  CKEDITOR.replace( 'roles_responsibilities' );
        var editor =  CKEDITOR.replace( 'pre_requiesite' );
        var editor =  CKEDITOR.replace( 'advantage' );
        CKFinder.setupCKEditor( editor );
    });

    $(window).bind("pageshow", function() {
        var form = $('form'); 
        // let the browser natively reset defaults
        form[0].reset();
    });

   

    

    $(function () {
        $("input[id*='number_of_openings']").keydown(function (event) {


            if (event.shiftKey == true) {
                event.preventDefault();
            }

            if ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105) || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190) {

            } else {
                event.preventDefault();
            }
            
            if($(this).val().indexOf('.') !== -1 && event.keyCode == 190)
                event.preventDefault();

        });
    });

    $('#department').keypress(function (e) {
        var regex = new RegExp("^[a-zA-Z ]+$");
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (regex.test(str)) {
            return true;
        }
        else
        {
        e.preventDefault();
        alert('Please Enter Alphabate and space only');
        return false;
        }
    });

    $('#opening_title').keypress(function (e) {
        var regex = new RegExp("^[a-zA-Z ]+$");
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (regex.test(str)) {
            return true;
        }
        else
        {
        e.preventDefault();
        alert('Please Enter Alphabate and space only');
        return false;
        }
    });

    $('#base_location').keypress(function (e) {
        var regex = new RegExp("^[a-zA-Z ]+$");
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (regex.test(str)) {
            return true;
        }
        else
        {
        e.preventDefault();
        alert('Please Enter Alphabate and space only');
        return false;
        }
    });
</script>
@endpush
