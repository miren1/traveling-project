@extends('admin.master')
@section('bodyData')
<meta name="csrf-token" content="{{ csrf_token() }}">
<section class="forms-section" style='min-height:500px'>
<div class="outer-w3-agile mt-3">
        <h4 class="tittle-w3-agileits mb-4">Event Creation</h4>
        
        @if ($message = Session::get('AggentAdd'))
<div class="alert alert-success alert-block">
	<button type="button" class="close" data-dismiss="alert">×</button>	
        <strong>{{ $message }}</strong>
</div>
@endif
          
        <form action="{{url('eventCreation')}}" method="post" enctype="multipart/form-data">
        @csrf
      <div class="form-row">
               
                <div class="form-group col-md-6">
                    <label for="inputEmail4">Name</label>
                    <input type="text" class="form-control" id="inputEmail4" placeholder="Enter Event Name" required="" name='name'>
                </div>
                <div class="form-group col-md-6">
                   <label for="inputEmail4">Start Date</label>
                   <input type="date" class="form-control" id="inputEmail4" required="" name='sdate'>
               </div>
           </div>

           <div class="form-row">
               
               <div class="form-group col-md-6">
                   <label for="inputEmail4">End Date</label>
                   <input type="date" class="form-control" id="inputEmail4" placeholder="Enter Company Name" required="" name='edate'>
               </div>
               <div class="form-group col-md-6">
                   <label for="inputEmail4">Tagline</label>
                   <input type="text" class="form-control" id="inputEmail4" placeholder="Enter Contact Detail" required="" name='title'>
               </div>
          </div>

          
          <div class="form-row">
               
               <div class="form-group col-md-6">
                   <label for="inputEmail4">Description</label>
                   <input type="text" class="form-control" required="" name='description' Placeholder='Enter Event Description'>
               </div>

               <div class="form-group col-md-6">
                   <label for="inputEmail4">Image</label>
                   <input type="file" class="form-control" required="" name='image' >
               </div>
               
          </div>

          <div class="form-row">
               
               <div class="form-group col-md-6">
                   <button class='btn btn-success'>Create Event</button>
               </div>
            </div>
        

         
         

          
          

                  
          
               
              
              
              

            
        </form>
    </div>

</section>

@endsection
