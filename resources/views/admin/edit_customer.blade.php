@extends('admin.master')
@section('bodyData')

<section class="forms-section" style='min-height:500px'>
<div class="outer-w3-agile mt-3">
   <a href='/adminshowcustomer' class='btn btn-primary'>Show Customer</a>
        <h4 class="tittle-w3-agileits mb-4">Customer Updation</h4>
        
  
          
        <form action="{{url('adminEditCustomer')}}" method="post">
        @csrf
      <div class="form-row">
               
                <div class="form-group col-md-6">
                    <label for="inputEmail4">First Name</label>
    <input type="text" class="form-control" id="inputEmail4" placeholder="Enter First Name" required="" name='first_name' value='<?php ucfirst(print_r($customer[0]->first_name))?>' pattern='[A-Za-z]{2,30}'>
                </div>
                <div class="form-group col-md-6">
                   <label for="inputEmail4">Last Name</label>
                   <input type="text" class="form-control" id="inputEmail4" placeholder="Enter Last Name" required="" name='last_name' value='<?php ucfirst(print_r($customer[0]->last_name))?>' pattern='[A-Za-z]{2,30}'>
               </div>
           </div>

    <input type="hidden" class="form-control" name='id' value='<?php echo $customer[0]->id?>'>
          

          <div class="form-row">
               
               <div class="form-group col-md-6">
                   <label for="inputEmail4">Mobile Number</label>
                   <input type="text" class="form-control" id="inputEmail4" placeholder="Enter Mobile Number" required="" name='mobile_number' value='<?php print_r($customer[0]->mobile_number)?>' pattern='[0-9]{10}'>
               </div>
               <div class="form-group col-md-6">
                   <label for="inputEmail4">Whats App Number</label>
                   <input type="text" class="form-control" id="inputEmail4"placeholder="Enter Whats App Number" required="" name='whats_up' value='<?php print_r($customer[0]->whats_up)?>' pattern='[0-9]{10}'>
               </div>
          </div>
        
               
             
               <div class="form-group col-md-6">
                   <label for="inputEmail4">Email</label>
                   <input type="email" class="form-control" required="" name='email' placeholder='Enter Email' value='<?php echo $customer[0]->email  ?>'>
               </div>
               <button type="submit" class="btn btn-primary">Update Customer</button>

            </div>
         
         </div>

              
               
          </div>

           
        </form>
    </div>

</section>

@endsection
