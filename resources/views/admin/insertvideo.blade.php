@extends('admin.master')
@section('bodyData')


<!------ Include the above in your HEAD tag ---------->
<a href='{{url('/tourdetail')}}/{{base64_encode($result[0]->fk_tour)}}'><button class='btn btn-primary'>Back To Tour Details</button></a>
<a href='{{url('/amintourinsertphoto')}}/{{base64_encode($result[0]->fk_tour)}}'><button class='btn btn-primary'>Add  Photos</button></a>
<a href='{{url('/amintourinsertvideo')}}/{{base64_encode($result[0]->fk_tour)}}'><button class='btn btn-primary'>Add Videos </button></a>


<div class="container-fluid" style="margin-top:20px;">
<div class="row">
</div><hr noshade style="margin-top:-20px;">
<div class="container">
<div class="tab-content" id="pills-tabContent">

  <div class="tab-pane fade show active" id="showall" role="tabpanel" aria-labelledby="showall-tab">
  <form method='post' action="{{url('/insertadminvideo')}}/{{$result[0]->fk_tour}}">
  @csrf
  <label>Enter Caption:</label>
  <input type='text' name='caption' placeholder='Enter Caption'>
  <label>Link:</label>
  <input type='text' name='link' placeholder='Enter Link'>
  <input type='submit' name='submit' value='Submit' class='btn btn-success'>
    </form>
  <br>
  <?php  foreach ($result as $value) {
      # code...
  ?>

    <div class="Portfolio">
    <iframe width="420" height="345" src="<?php echo $value->link?>">
    </iframe>
    <p><?php echo $value->caption?></p>
    <a style='' href='{{url('/deletevideo')}}/{{$value->id}}' role='button' class='btn btn-danger'>Delete</a>
    <hr>
    </div>
    </div>
  <?php }?>
    
  </div>
  
  
  
</div>
</div>

</div>
</div>

</div>


@endsection
