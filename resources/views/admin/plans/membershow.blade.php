@extends('admin.master')
@section('bodyData')
<div class="outer-w3-agile mt-3" style='min-height:400px'>
                <div class='row'>
                   
                </div>
                <br>


                  
                    <div class="table-responsive">
                    <table id="example" class="table" style="width: 100%">
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col">First name</th>
                                <th scope="col">Last name</th>
                                <th scope="col">Emalil</th>
                                <th scope="col">Phone number</th>
                                <th scope="col">Status</th>
                                <th scope="col">Created at</th>
                                
                 
                               
                            </tr>
                        </thead>
                       
                    </table>
                    </div>
                    <br> <br><br>   <br> <br><br>



</div>
@endsection 

@push('css')
<style type="text/css">
    div#example_filter input[type="search"] {
    box-sizing: border-box;
    border: 1px solid #d2d6dc;
}
</style>
@endpush
@push('js')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script type="text/javascript">
 
      $(document).on("click", '.agentdelete', function(e) {
            e.preventDefault();
            var id = $(this).attr('data-id');
            swal({
                    title: "Are you sure?",
                    text: "You want to delete this Plan.",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {

                        $.ajax({
                            type: "POST",
                            url: "{{route('member-plans-delete')}}",
                            data: {
                                "id": id,
                                "_token": "{{ csrf_token() }}"
                            },
                            success: function(response) {
                                if (response.status == "success") {
                                    swal(response.message, {
                                        icon: 'success',
                                    }).then((result) => {
                                         $('#example').DataTable().ajax.reload(null,false);
                                    });
                                } else {
                                    swal("Error","Something went wrong!",{icon:'error'});
                                }
                            }
                        });
                    }
                });
        });


    $(document).ready(function() {
  
    $table = $("#example").DataTable({
      "ajax": "{{ url('member-plans/list_member_req') }}",
      "processing": true,
      "serverSide": true,
      "ordering": false,
      "columns": [
            { "data": "first_name"},
            { "data": "last_name"},
            { "data": "email"},
            { "data": "mobile_number"},
            { "data": "status",render:function(data,type,row,meta) {
                    if(data=='0'||data==0){
                        return '<span class="badge badge-danger ml-1">pending</span>';
                    }else{  
                        return '<span class="badge badge-success ml-1">success</span>';
                    }
            },},
            { "data": "created_at",render:function(data,type,row,meta) {

                return moment(data).format('DD-MM-YYYY,h:mm a');
                // body...
            }}
        ]
    });


    
  } );
</script>

@endpush
