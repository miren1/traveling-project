@extends('admin.master')
@section('bodyData')

<div class="outer-w3-agile mt-3" style='min-height:400px'>
    <div class='row'>
    @if(Auth::user()->role=='admin')
        <!-- <div class='col-sm-2'>
            <a href="{{url('member-plans/add')}}" style='text-align:right'>
                <button class="btn btn-primary">+ Add Plan</button>
            </a>
        </div>  
 -->    <div class='col-md-6'>
            <div class="btn btn-primary"><i class="fa fa-list mr-2"></i>Plan list</div>
          </div>
        <div class='col-md-6 text-right'>
            <button class="btn btn-primary" onclick="window.location='{{ url("member-plans/add") }}'">
            <i class="fa fa-plus mr-2"></i>Add Plan</button>
        </div>
    @endif
    </div><br>    
    <div class="table-responsive">
        <table id="example" class="table" style="width: 100%">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">TIP Plans Year</th>
                    <th scope="col">Plan Type</th>
                    <th scope="col">Fixed Amount</th>
                    <th scope="col">Created at</th>
                    <th>Actions</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
@endsection 

@push('css')
<style type="text/css">
    div#example_filter input[type="search"] {
    box-sizing: border-box;
    border: 1px solid #d2d6dc;
}
</style>
@endpush
@push('js')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script type="text/javascript">
 
      $(document).on("click", '.agentdelete', function(e) {
            e.preventDefault();
            var id = $(this).attr('data-id');
            swal({
                    title: "Are you sure?",
                    text: "You want to delete this Plan.",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {

                        $.ajax({
                            type: "POST",
                            url: "{{route('member-plans-delete')}}",
                            data: {
                                "id": id,
                                "_token": "{{ csrf_token() }}"
                            },
                            success: function(response) {
                                if (response.status == "success") {
                                    swal(response.message, {
                                        icon: 'success',
                                    }).then((result) => {
                                         $('#example').DataTable().ajax.reload(null,false);
                                    });
                                } else {
                                    swal("Error","Something went wrong!",{icon:'error'});
                                }
                            }
                        });
                    }
                });
        });


    $(document).ready(function() {
  
    $table = $("#example").DataTable({
      "ajax": "{{ url('member-plans/list') }}",
      "processing": true,
      "serverSide": true,
      "ordering": false,
      "columns": [
            { "data": "years"},
            { "data": "type"},
            { "data": "fixed_amt"},
            { "data": "created_at",render: function (data, type, row) {
          return moment(new Date(data).toString()).format('DD/MM/YYYY');
        }},
            { "data": "id", 
                    render:function(data,type,row,meta){
                      
                     var html= '<a  href="{{url("member-plans/add_member")}}/'+btoa(data)+'"><i class="fa fa-plus" aria-hidden="true"'+ 
                                'title="Add member" style="font-size:20px;margin-right: 2px;"></i></a>'+
                                '<a  href="{{url("member-plans/view/")}}/'+btoa(data)+'"><i class="fa fa-info-circle" aria-hidden="true"'+ 
                                'title="Detail View" style="font-size:20px"></i></a>'+
                                @if(Auth::user()->role=='admin')
                                '<a class="ml-1" href="{{url("member-plans/add/")}}/'+btoa(data)+'"><i class="fa fa-edit" aria-hidden="true"'+
                                'title="Edit" style="font-size:20px"></i></a>'+
                                
                                '<a href="#" class="ml-1 agentdelete"  data-id="'+btoa(data)+'"><i class="fa fa-trash"'+
                                 'aria-hidden="true" style="color:red;font-size:20px" title="Delete"></i>'
                                '</a>'+
                                @endif
                                '';

                  return html;          
                      
              }

            },
        ]
    });


    
  } );
</script>

@endpush
