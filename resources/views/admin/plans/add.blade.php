@extends('admin.master')
@push('css')
<link rel="stylesheet"
    href="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css" />
@endpush

@section('bodyData')
<section class="forms-section">
    <div class="outer-w3-agile mt-3">
        <h4 class="tittle-w3-agileits mb-4"> @if(isset($plan)) Edit Plan @else Add Plan @endif</h4>

        <form class="defaultForm" action="{{route('member-plans-store')}}" method="POST" enctype="multipart/form-data">
            @csrf
            @if(isset($plan))
            <input type="hidden" value="{{$plan->id}}" name="id" />
            @endif
            <div class="row">
                <div class="form-group col-md-3">
                    <label for="inputAmount">Fixed Amount</label>
                    <input type="number" required="" class="form-control" id="inputAmount"
                        data-bv-notempty-message="The Fixed Amount is required" name="fixed_amt"
                        placeholder="Enter Fixed Amount" @if(isset($plan)) value="{{$plan->fixed_amt}}" @endif >
                </div>
                <div class="form-group col-md-3">
                    <label for="inputYears">TIP Plan Year</label>
                    <input type="number" class="form-control" id="inputYears" name="years" placeholder="Enter TIP Plan Year"
                        required="" data-bv-notempty-message="The TIP Plan Year is required" @if(isset($plan)) value="{{$plan->years}}" @endif>
                </div>
                <div class="form-group col-md-3">
                    <label for="inputRole">Role</label>
                    <select id="inputRole" name="role" class="form-control" required aria-required="true"
                        data-bv-notempty-message="Role is required">
                        <option value="">Select Role</option>
                        <option value="customer" @if(isset($plan)) @if($plan->role=="customer") {{'selected'}} @endif  @endif>Customer</option>
                        <option value="member" @if(isset($plan)) @if($plan->role=="member") {{'selected'}} @endif  @endif>Member</option>
                        <option value="agent" @if(isset($plan)) @if($plan->role=="agent") {{'selected'}} @endif  @endif>Agent</option>
                        <option value="employee" @if(isset($plan)) @if($plan->role=="employee") {{'selected'}} @endif  @endif>Employee</option>
                    </select>
                </div>
            
                <div class="form-group col-md-3">
                    <label for="type">Type</label>
                    <select id="type" name="type" class="form-control" required aria-required="true"
                        data-bv-notempty-message="Type is required">
                        <option value="">Select Type</option>
                        <option value="silver" @if(isset($plan)) @if($plan->type=="silver") {{'selected'}} @endif  @endif>silver</option>
                        <option value="gold" @if(isset($plan)) @if($plan->type=="gold") {{'selected'}} @endif  @endif>gold</option>
                        <option value="platinum" @if(isset($plan)) @if($plan->type=="platinum") {{'selected'}} @endif  @endif>platinum</option>
                    </select>
                </div>
                <div class="form-group col-md-3">
                    <label for="mebership_fees">Membership Fees</label>
                    <input type="number" min="0" class="form-control" id="mebership_fees" name="mebership_fees"
                        placeholder="Enter Membership Fees" required=""
                        data-bv-notempty-message="The Membership Fees is required" @if(isset($plan)) value="{{$plan->mebership_fees}}" @endif>
                </div>
                <div class="form-group col-md-3">
                    <label for="free_nights_room">Free Nights Room</label>
                    <input type="number" class="form-control" id="free_nights_room" name="free_nights_room"
                        placeholder="Enter Free Nights Room" required=""
                        data-bv-notempty-message="The Free Nights Room is required" @if(isset($plan)) value="{{$plan->free_nights_room}}" @endif>
                </div>
           
                <div class="form-group col-md-6">
                    <label for="free_nights_to_avail_in">Free Nights To Avail In</label>
                    <input type="text"  class="form-control" id="free_nights_to_avail_in" name="free_nights_to_avail_in"
                        placeholder="Enter Free Nights To Avail In" required=""
                        data-bv-notempty-message="The Free Nights To Avial In is required" @if(isset($plan)) value="{{$plan->free_nights_to_avail_in}}" @endif>
                </div>
                <div class="form-group col-md-3">
                    <label for="free_night_in_any_type_of_hotel">Free Night In Any Type Of Hotel</label>
                    <input type="text"  class="form-control" id="free_night_in_any_type_of_hotel"
                        name="free_night_in_any_type_of_hotel" placeholder="Enter Free Night In Any Type Of Hotel"
                        required="" data-bv-notempty-message="The Free Night In Any Type Of Hotel is required"  @if(isset($plan)) value="{{$plan->free_night_in_any_type_of_hotel}}" @endif>
                </div>
                 <div class="form-group col-md-3">
                    <label for="monthly_emi">Monthly EMI</label>
                    <input type="number" min="0" class="form-control" id="monthly_emi" name="monthly_emi"
                        placeholder="Monthly EMI" required="" data-bv-notempty-message="The Monthly EMI is required" @if(isset($plan)) value="{{$plan->monthly_emi}}" @endif>
                </div>
            
                <div class="form-group col-md-2">
                    <label for="quarterly_emi">Quarterly EMI</label>
                    <input type="number" min="0" class="form-control" name="quarterly_emi" id="quarterly_emi"
                        placeholder="Enter Quarterly EMI" required=""
                        data-bv-notempty-message="The Quarterly EMI is required" @if(isset($plan)) value="{{$plan->quarterly_emi}}" @endif>
                </div>
                <div class="form-group col-md-2">
                    <label for="half_yearly_emi" id="reflab">Half Yearly EMI</label>
                    <input name="half_yearly_emi" id="half_yearly_emi" placeholder="Enter Half Yearly EMI"
                        class="form-control" type="number" min="0" required=""
                        data-bv-notempty-message="The Half Yearly EMI is required" @if(isset($plan)) value="{{$plan->half_yearly_emi}}" @endif>
                </div>
                <div class="form-group col-md-2">
                    <label for="annual_emi">Annual EMI</label>
                    <input type="number" min="0" name="annual_emi" placeholder="Annual EMI" class="form-control" id="annual_emi"
                        required="" data-bv-notempty-message="The Annual EMI is required" @if(isset($plan)) value="{{$plan->annual_emi}}" @endif>
                </div>
            
                <div class="form-group col-md-2">
                    <label for="emi_dafulter_charges">EMI Default Charges</label>
                    <input type="number" min="0" class="form-control" placeholder="EMI Default Charges"
                        name="emi_dafulter_charges" id="emi_dafulter_charges" required=""
                        data-bv-notempty-message="The EMI Default Charges is required" @if(isset($plan)) value="{{$plan->emi_dafulter_charges}}" @endif>
                </div>
                <div class="form-group col-md-2">
                    <label for="referral">Referral</label>
                    <input type="number" min="0" class="form-control" id="referral" name="referral" required=""
                        placeholder="Enter Referral" data-bv-notempty-message="The Referral is required" @if(isset($plan)) value="{{$plan->referral}}" @endif>
                </div>
                <div class="form-group col-md-3">
                    <label for="flexbility_to_move_free_night_to_next_year">Flexibility To Move Free Night To Next Year</label>
                    <select id="inputRole" name="flexbility_to_move_free_night_to_next_year" class="form-control"
                        required aria-required="true"
                        data-bv-notempty-message="The Flexibility To Move Free Night To Next Year is required">
                        <option value="">Select Role</option>
                        <option value="true" @if(isset($plan)) @if($plan->flexbility_to_move_free_night_to_next_year=="true") {{'selected'}} @endif  @endif>Yes</option>
                        <option value="false" @if(isset($plan)) @if($plan->flexbility_to_move_free_night_to_next_year=="false") {{'selected'}} @endif  @endif>No</option>
                    </select>
                </div>
                <div class="form-group col-md-3">
                    <label for="easy_upgrade_of_membership_plans">Easy Upgrade Of Membership Plans</label>
                    <input name="easy_upgrade_of_membership_plans" placeholder="Enter Easy Upgrade Of Membership Plans"
                        class="form-control" type="text" id="easy_upgrade_of_membership_plans" required=""
                        data-bv-notempty-message="The Easy Upgrade Of Membership Plans is required" @if(isset($plan)) value="{{$plan->easy_upgrade_of_membership_plans}}" @endif>
                </div>
                <div class="form-group col-md-2">
                    <label for="agent_target">Target for Agents </label>
                    <input type="number" min="0" class="form-control" placeholder="Target for Agents"
                        name="agent_target" id="agent_target" required=""
                        data-bv-notempty-message="The Target for Agents is required" @if(isset($plan)) value="{{$plan->agent_target}}" @endif>
                </div>
                <div class="form-group col-md-3">
                    <label for="commission_am">Commission (Agent / Members)</label>
                    <input type="number" min="0" name="commission_am" placeholder="Commission (Agent / Members)" class="form-control" id="commission_am"
                        required="" data-bv-notempty-message="The Commission (Agent / Members) is required" @if(isset($plan)) value="{{$plan->commission_am}}" @endif>
                </div>
                <div class="form-group col-md-5">
                    <label for="commission_first_payment">Commission 1st Payment <span>(in the month of Membership Registration done)</span> </label>
                    <input type="number" min="0" class="form-control" placeholder="EMI Commission 1st Payment"
                        name="commission_first_payment" id="commission_first_payment" required=""
                        data-bv-notempty-message="The Commission 1st Payment is required" @if(isset($plan)) value="{{$plan->commission_first_payment}}" @endif>
                </div>
                <div class="form-group col-md-4">
                    <label for="commission_employee">Commission (Employee) </label>
                    <input type="number" min="0" name="commission_employee" placeholder="Commission (Employee)" class="form-control" id="commission_employee"
                        required="" data-bv-notempty-message="The Commission (Employee) is required" @if(isset($plan)) value="{{$plan->commission_employee}}" @endif>
                </div>
                <div class="form-group col-md-6">
                    <label for="commission_second_payment">Commission 2nd Payment <span>(in the 7th month post Membership Registration done)</span> </label>
                    <input type="number" min="0" name="commission_second_payment" placeholder="Commission 2nd Payment" class="form-control" id="commission_second_payment"
                        required="" data-bv-notempty-message="The Commission 2nd Payment is required"  @if(isset($plan)) value="{{$plan->commission_second_payment}}" @endif>
                </div>
                
                <div class="form-group col-md-6">
                    <label for="commission_payment">Commission Payment (in the 4th month Salary post Membership Registration done) </label>
                    <input type="number" min="0" class="form-control" placeholder="Commission Payment"
                        name="commission_payment" id="commission_payment" required=""
                        data-bv-notempty-message="The Commission Payment is required" @if(isset($plan)) value="{{$plan->commission_payment}}" @endif>
                </div>

            </div>


            <button type="submit" name="save" class="btn btn-primary"> @if(isset($plan)) Update Plan @else Submit Plan @endif</button>
        </form>
    </div>
</section>

@endsection


@push('js')
<script type="text/javascript"
    src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js"></script>
<script>
$(document).ready(function() {
    $('.defaultForm').bootstrapValidator();

});
</script>


@endpush