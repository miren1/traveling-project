@extends('admin.master')
@section('bodyData')


    <div class="container-fluid">
        <div class="row">
            <a href='{{url('/member-plans')}}' class='btn btn-primary' style='color:white'>Show Plan</a>
            <div class="col-sm-12 mt-4">

                <ul class="nav nav-tabs">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle='tab' href="#profile">Plans</a>

                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle='tab' href="#Address">Commission (Agent / Members)</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle='tab' href="#booking">Commission (Employee)</a>

                    </li>

                </ul>

                <div class="tab-content">


                    <!-- // Booking Detail -->
                    <div id="booking" class="container tab-pane" style='min-height:400px'>
                        <div class="row">
                            <div class="col-sm-6">
                                <br>
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item"><b>Commission (Employee): </b> 
                                      @if(isset($plan)){{$plan->commission_employee}}@endif
                                    </li>
                                    <li class="list-group-item"><b>Commission Payment (in the 4th month Salary post Membership Registration done): </b> 
                                      @if(isset($plan)){{$plan->commission_payment}}@endif
                                    </li>


                                </ul>
                            </div>

                        </div>
                    </div>

                    <!-- // Address Inforamtion -->

                    <div id="Address" class="container tab-pane" style='min-height:400px'>
                        <div class="row">
                            <div class="col-sm-6">
                                <br>
                                <ul class="list-group">

                                    <li class="list-group-item"><b>Commission (Agent / Members): </b> 
                                      @if(isset($plan)){{$plan->commission_am}}@endif
                                    </li>
                                    <li class="list-group-item"><b>Commission 1st Payment (in the month of Membership Registration done): </b>
                                      @if(isset($plan)){{$plan->commission_first_payment}}@endif
                                   </li>
                                    <li class="list-group-item"><b>Commission 2nd Payment (in the 7th month post Membership Registration done): </b>
                                      @if(isset($plan)){{$plan->commission_second_payment}}@endif
                                   </li>
                                    <li class="list-group-item"><b>Target for Agents: </b>
                                      @if(isset($plan)){{$plan->agent_target}}@endif
                                    </li>
                                    

                                </ul>
                            </div>

                        </div>
                    </div>


                    <!-- Profile Detail -->

                    <div id="profile" class="container tab-pane active">

                        <div class="row">

                            <div class="col-sm-8">
                                <br>

                                <ul class="list-group">

                                    <li class="list-group-item"><b>TIP Plan Year: </b>
                                      @if(isset($plan))
                                       {{ $plan->years }} 
                                      @endif
                                    </li>
                                    <li class="list-group-item"><b>Fixed Amount: </b>
                                      @if(isset($plan))
                                       {{ $plan->fixed_amt }} 
                                      @endif
                                    </li>
                                    <li class="list-group-item"><b>Membership Fees: </b>
                                      @if(isset($plan))
                                       {{ $plan->mebership_fees }} 
                                      @endif
                                    </li>
                                    <li class="list-group-item"><b>Type: </b>
                                      @if(isset($plan))
                                       {{ $plan->type }} 
                                      @endif
                                    </li>
                                    <li class="list-group-item"><b>Free Nights (1 Room): </b>
                                      @if(isset($plan))
                                      {{  $plan->free_nights_room }} 
                                      @endif  
                                      </li>
                                    <li class="list-group-item"><b>Free Nights to avail in : </b>
                                      @if(isset($plan))
                                      {{  $plan->free_nights_to_avail_in }} 
                                      @endif 
                                    </li>
                                    <li class="list-group-item"><b>Free Night in Type of Hotel: </b>
                                      @if(isset($plan))
                                      {{  $plan->free_night_in_any_type_of_hotel }} 
                                      @endif 
                                    </li>
                                    <li class="list-group-item"><b>Monthly EMI: </b>
                                      @if(isset($plan))
                                      {{  $plan->monthly_emi }} 
                                      @endif
                                    </li>
                                    <li class="list-group-item"><b>Quarterly EMI: </b>
                                      @if(isset($plan))
                                      {{  $plan->quarterly_emi }} 
                                      @endif
                                    </li>
                                    <li class="list-group-item"><b>Half-Yearly EMI: </b> 
                                      @if(isset($plan))
                                      {{  $plan->half_yearly_emi }} 
                                      @endif
                                      </li>
                                    <li class="list-group-item"><b>Annual EMI: </b> 
                                      @if(isset($plan))
                                      {{  $plan->annual_emi }} 
                                      @endif
                                    </li>
                                     <li class="list-group-item"><b>EMI Default Charges per month: </b> 
                                      @if(isset($plan))
                                      {{  $plan->emi_dafulter_charges }} 
                                      @endif
                                    </li>
                                    <li class="list-group-item"><b>Flexibility To Move Free Night To Next Year: </b> 
                                      @if(isset($plan))
                                      @if($plan->flexbility_to_move_free_night_to_next_year=="true")
                                      {{ 'yes' }}
                                      @else
                                      {{ 'no' }} 
                                      @endif
                                      @endif
                                    </li>

                                </ul>
                            </div>
                        </div>


                    </div>







                </div>

            </div>





        </div>




    </div>



    



@endsection
