@extends('admin.master')
@section('bodyData')
<meta name="csrf-token" content="{{ csrf_token() }}">
<script>
function padToTwo(number) {
  if (number <= 9999) {
    number = ("0" + number).slice(-2);
  }
  return number;
}

(function($) {
  $.fn.monthly = function(options) {
    var months = options.months || [
        "January",
        "February",
        "March",
        "April",
        "May",
        "June",
        "July",
        "August",
        "September",
        "October",
        "November",
        "December"
      ],
      Monthly = function(el) {
        this._el = $(el);
        this._init();
        this._render();
        this._renderYears();
        this._renderMonths();
        this._bind();
      };

    Monthly.prototype = {
      _init: function() {
        this._el.html(months[0] + "-" + options.years[0]);
      },

      _render: function() {
        var linkPosition = this._el.offset(),
          cssOptions = {
            display: "none",
            position: "absolute",
            top:
              linkPosition.top + this._el.height() + (options.topOffset || 0),
            left: linkPosition.left
          };
        this._container = $('<div class="monthly-wrp">')
          .css(cssOptions)
          .appendTo($("body"));
      },

      _bind: function() {
        var self = this;
        this._el.on("click", $.proxy(this._show, this));
        $(document).on("click", $.proxy(this._hide, this));
        this._yearsSelect.on("click", function(e) {
          e.stopPropagation();
        });
        this._container.on("click", "button", $.proxy(this._selectMonth, this));
      },

      _show: function(e) {
        e.preventDefault();
        e.stopPropagation();
        this._container.css("display", "inline-block");
      },

      _hide: function() {
        this._container.css("display", "none");
      },

      _selectMonth: function(e) {
        var monthIndex = $(e.target).data("value"),
          month = monthIndex+1,
          year = this._yearsSelect.val();
        this._el.html(month + "," + year);
        if (options.onMonthSelect) {
          options.onMonthSelect(monthIndex, month, year);
        }
      },

      _renderYears: function() {
        var markup = $.map(options.years, function(year) {
          return "<option>" + year + "</option>";
        });
        var yearsWrap = $('<div class="years">').appendTo(this._container);
        this._yearsSelect = $("<select>")
          .html(markup.join(""))
          .appendTo(yearsWrap);
      },

      _renderMonths: function() {
        var markup = ["<table>", "<tr>"];
        $.each(months, function(i, month) {
          if (i > 0 && i % 4 === 0) {
            markup.push("</tr>");
            markup.push("<tr>");
          }
          markup.push(
            '<td><button data-value="' + i + '">' + month + "</button></td>"
          );
        });
        markup.push("</tr>");
        markup.push("</table>");
        this._container.append(markup.join(""));
      }
    };

    return this.each(function() {
      return new Monthly(this);
    });
  };
})(jQuery);

$(function() {
  $("#selection").monthly({
    years: [2020,2021,2022,2023,2024,2025,2026,2027,2028,2029,2030,2031,2032,2032,2033,2034],
    topOffset: 28,
    onMonthSelect: function(mi, m, y) {
      mi = padToTwo(mi);
      $("#selection").val(m+"-" + y);
      

     }
  });
});
</script>
<style>
input,
select,
button {
  font-family: inherit;
  font-size: inherit;
  padding: 8px;
}
select {
  padding: 8px;
}

.monthly-wrp {
  padding: 1em;
  top: 6px;
  z-index: 1000;
  border-radius: 3px;
  background-color: #2C3E50;
}

.monthly-wrp:before {
  content: "";
  border-bottom: 6px solid #2C3E50;
  border-left: 6px solid transparent;
  border-right: 6px solid transparent;
  position: absolute;
  top: -6px;
  left: 6px;
  z-index: 1002;
}


.monthly-wrp .years {
  margin-bottom: 0.8em;
  text-align: center;
}

.monthly-wrp .years select {
  border: 0;
  border-radius: 3px;
  width: 100%;
}

.monthly-wrp .years select:focus {
  outline: none;
}

.monthly-wrp table {
  border-collapse: collapse;
  table-layout: fixed;
}

.monthly-wrp td {
  padding: 1px;
}

.monthly-wrp table button {
  width: 100%;
  border: none;
  background-color:#083672;
  color: #FFFFFF;
  font-size: 14px;
  padding: 0.6em;
  cursor: pointer;
  border-radius: 3px;
}

.monthly-wrp table button:hover {
  background-color: #083672;
}

.monthly-wrp table button:focus {
  outline: none;
}

</style>
<section class="forms-section" style='min-height:500px'>
<div class="outer-w3-agile mt-3">
        <h4 class="tittle-w3-agileits mb-4">Salary Generate</h4>
        
         @if ($message = Session::get('userPayment'))
<div class="alert alert-success alert-block">
	<button type="button" class="close" data-dismiss="alert">×</button>	
        <strong>{{ $message }}</strong></div>
@endif

         @if ($message = Session::get('salarygenerate'))
<div class="alert alert-success alert-block">
	<button type="button" class="close" data-dismiss="alert">×</button>	
        <strong>{{ $message }}</strong></div>
@endif


<form action='allsalarygenerate' method='post'>
@csrf
<label>Salary Month Generation</label>

<!-- <input type='date' name='monthsalary' max='30-11-2020'>
 -->
<input type="text" id="selection" name="monthsalary">&nbsp;&nbsp;


<input type='submit' name='submit' value='Generate' class='btn btn-success'>
<br><br>
</form>
</div></section>
@endsection
