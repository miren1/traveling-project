@extends('admin.master')
@section('bodyData')
@push('css')
<style type="text/css">
    .card-header {
        margin-bottom: 0 !important;
        background-color: rgb(158 204 243) !important;
        border-bottom: 1px solid rgb(158 204 243) !important;
        padding: 0px !important;
    }

    .page-item.active .page-link {
        z-index: 0 !important;
        color: #fff !important;
        background-color: #007bff !important;
        border-color: #007bff !important;
    }
    .bt-margin{
        margin-bottom: 4%;
    }
    </style>
@endpush
<section class="forms-section" style='min-height:500px'>
<div class="outer-w3-agile mt-3">
<section class="pricing-tables"><br>
    <div class='col-md-12 text-right'>
        <button class="btn btn-primary" onclick="window.location='{{ url("admininsertPackageForm") }}'">
        <i class="fa fa-plus mr-2"></i>Add New Tour Category</button>
    </div>
    <div class='row'>
        @foreach($packages as $value)
        <div class="card box-shadow col-xl-4 col-md-6 mt-3 bt-margin">
            <div class="card-header">
                <center><h4 class="py-md-4 py-xl-3 py-2">{{ $value->name }} </h4></center>
            </div>
            <div class="card-body">
                <ul class="list-unstyled mt-3 mb-4">
                    <li class="py-2 border-bottom">
                        <img src="{{ asset('uploads/plans_images/'.$value->photo)}}" alt="Image" style='height:200px;width:100%'/>
                    </li>
                    <li class="py-2 border-bottom">{{ $value->tag_line }}</li>
                    <li class="py-2 border-bottom" style='text-align: justify;'>{{ $value->details }}</li>
                    <li class="py-2 border-bottom">{{ $value->created_at }} </li>
                </ul>
                <a href="{{url('/updatepackageform')}}/{{base64_encode($value->id)}}">
                    <button type="button" class="btn btn-sm btn-block btn btn-primary py-2 mt-4">Update</button>
                </a>
                <a href="{{url('/packagedelete')}}/{{base64_encode($value->id)}}">
                    <button type="button" class="btn btn-sm btn-block btn btn-primary mt-4 py-2" onclick='return confirm("Are you Sure Delete this Package?")'>Delete</button>
                </a>
            </div>
        </div>
        @endforeach
    </div>    
</section>
</div>
</section>
@endsection 
