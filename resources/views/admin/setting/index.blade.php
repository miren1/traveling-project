@extends('admin.master')
@section('bodyData')
@if ($message = Session::get('profile_sucess'))
<div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>	
    <strong>{{ $message }}</strong>
</div>
@endif
<section class="forms-section" style='height:400px'>

    <div class="outer-w3-agile mt-3">
        <h4 class="tittle-w3-agileits mb-4">Settings </h4>
        <form action="{{route('admin-setting.store')}}" id="settingForm" method="post">
        @csrf
        @if(isset($setting))
            <input type="hidden" value="{{$setting->id}}" name="id">
        @endif
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputEmail4">Sendgrid Key</label>
                    <input type="text" class="form-control" id="inputEmail4" name = 'sendgrid_key' placeholder="Enter Sendgrid Key" required value="@if(isset($setting)){{$setting->sendgrid_key}}@endif" data-bv-notempty="true" data-bv-notempty-message="Sendgrid Key is required">
                </div>
                <div class="form-group col-md-6">
                    <label for="inputPassword4">Contact E-mail</label>
                    <input type="email" class="form-control" id="inputPassword4" name='contact_mail'placeholder="Contact E-mail" pattern="[a-zA-Z0-9._%+-]+@[a-z0-9.-]+\.[a-zA-Z]{2,4}" style="text-transform: lowercase" required value="@if(isset($setting)){{$setting->contact_mail}}@endif" data-bv-notempty="true" data-bv-notempty-message="Contact E-mail is required">
                </div>
               <!--  <div class="form-group col-md-6">
                    <label for="inputPassword4">Executive E-mail</label>
                    <input type="email" class="form-control" id="inputPassword4" name='executive_mail'placeholder="Executive E-mail" pattern="[a-zA-Z0-9._%+-]+@[a-z0-9.-]+\.[a-zA-Z]{2,4}" style="text-transform: lowercase" value="@if(isset($setting)){{$setting->executive_mail}}@endif">
                </div>
                <div class="form-group col-md-6">
                    <label for="inputPassword4">Help Desk E-mail</label>
                    <input type="email" class="form-control" id="inputPassword4" name='help_desk_mail'placeholder="Help Desk E-mail" pattern="[a-zA-Z0-9._%+-]+@[a-z0-9.-]+\.[a-zA-Z]{2,4}" style="text-transform: lowercase" value="@if(isset($setting)){{$setting->help_desk_mail}}@endif">
                </div> -->

                 <div class="form-group col-md-6">
                    <label for="inputEmail4">Assign Ask For Call Employee</label>
                        <select class="form-control" name='askforcall_user_id' id='askforcall_user_id' required data-bv-notempty-message="The Assign Ask For Call Employee is required">
                            <option value=''>Select Ask For Call Employee</option>
                            @if(@isset($users))
                            @foreach ($users as $item)
                               @if($item->name)
                               <option value="{{$item->id}}" @if(isset($setting)) @if($item->id==$setting->askforcall_user_id) selected @endif @endif>{{$item->name}}</option> 
                               @endif
                            @endforeach
                              
                            @endif
                        </select>
                          
                </div>

                <div class="form-group col-md-6">
                    <label for="inputEmail4">Assign Ask For Group</label>
                        <select class="form-control" name='askforcall_group_id' id='askforcall_group_id' required data-bv-notempty-message="The Assign Ask For Group is required">
                            <option value=''>Select Ask For Group</option>
                            @if(@isset($groups))
                            @foreach ($groups as $group)
                               @if($group->name)
                               <option value="{{$group->id}}" @if(isset($group)) @if($group->id==$setting->askforcall_group_id) selected @endif @endif>{{$group->name}}</option> 
                               @endif
                            @endforeach
                              
                            @endif
                        </select>
                          
                </div>
            </div>

            <button type="submit" class="btn btn-primary">Save</button>
        </form>
    </div>

</section>

@endsection
@push('css')
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css"/>
@endpush
@push('js')
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js"></script>

<script>

  $(document).ready(function () {
    $('#settingForm').bootstrapValidator();
  });
</script>
@endpush