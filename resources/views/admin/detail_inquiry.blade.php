@extends('admin.master')
@section('bodyData')

<div class="container-fluid">
                <div class="row">
              
                <a class='btn btn-primary' href='/allinquiry' style='color:white'>All Inquiry</a>
                &nbsp
                <a class='btn btn-primary' href='/editinquiry/<?php echo $inquries[0]->inqid?>' style='color:white'>Update Inquiry</a>
                    <div class="col-sm-12">
                    
                     <ul class="nav nav-tabs">
       <li class="nav-item">
          <a class="nav-link active" data-toggle='tab' href="#profile">Profile</a>
           
       </li>

       <li class="nav-item">
          <a class="nav-link" data-toggle='tab' href="#detail">Comments Details</a>
           
       </li>
       <li class="nav-item">
          <a class="nav-link" data-toggle='tab' href="#acomments">Admin Comments</a>
           
       </li>
        
        
    </ul>
    
    <div class="tab-content">
       
        <div id="profile" class="container tab-pane active">
          <div class="row">
           
              <div class="col-sm-8">
                <br>
                <ul class="list-group">
                <?php foreach($inquries as $value) {?>
                 <li class="list-group-item"><b>Inquiry ID </b>{{ ucfirst("JJ-INQ-".$value->inqid)}}</li>
                <li class="list-group-item"><b>First Name: </b>{{ ucfirst($value->first_name)}}</li>
                <li class="list-group-item"><b>Last Name: </b>{{ ucfirst($value->last_name)}}</li>
               <li class="list-group-item"><b>Mobile Number: </b>{{ $value->mobile_no}}</li>
               <li class="list-group-item"><b>Whats App Number: </b>{{ $value->whatsup_no}}</li>

               <li class="list-group-item"><b>Appointment Date: </b>{{ $value->appointment_date}}</li>

               <li class="list-group-item"><b>Employee Handle By : </b>{{ucfirst($value->name)}}</li>
               <li class="list-group-item"><b>Inquiry Status : </b>{{ucfirst($value->inq_status)}}</li>
               <li class="list-group-item"><b>Email : </b>{{$value->email}}</li>
                <?php } ?>
            </ul>
              </div>
            
          </div>
        </div>

        <!-- /*--------------------------------- acooments-------------------------------*/ -->
       


    <!-- /**********************************Detail comments********************** */ -->
        <div id="detail" class="container tab-pane" style='min-height:300px'>
        <table class="table table-striped">
  <thead>
    <tr>
      
      <th scope="col">Comments</th>
      <th scope="col">By Employee</th>
      <th scope="col">By Date</th>
    </tr>
  </thead>
  <tbody>
  <?php foreach ($datacom as  $value) {
    # code...
 ?>
    <tr>
     
      <td><?php print_r($value->comments)?></td>
      <td><?php print_r($value->name)?></td>
      <td><?php print_r($value->created_at);?></td>

    
    </tr>
  <?php } ?>
  </tbody>
</table>
 </div>

 <!-- /***************************a comments ******************** */  -->

 <div id="acomments" class="container tab-pane" style='min-height:300px'>
          <div class="row">
          <br>
          <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
 Add Comments For this Inquiry
</button>
<br>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><?php echo $inquries[0]->first_name ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form method='post' action="{{url('adminaddComments')}}">
      @csrf
    <div class="form-group">
      <input type='hidden' name='id' value='<?php echo $inquries[0]->inqid?>'>
      <label for="comment">Comment:</label>
      <textarea class="form-control" rows="5" id="comment" name='comment'></textarea>
    </div>
    <input type="submit" class="btn btn-primary" value='Add Comment'>
  </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
       
      </div>
    </div>
  </div>
</div>
    <table class="table table-striped">
  <thead>
    <tr>
      
      <th scope="col">Comments</th>
     
      <th scope="col">By Date</th>
    </tr>
  </thead>
  <tbody>
 
  <?php foreach ($admincomments as  $value1) {
   
 ?>
    <tr>
     
      <td><?php print_r($value1->comments)?></td>
      
      <td><?php print_r($value1->created_at);?></td>

    
    </tr>
  <?php } ?>
  </tbody>
</table>
            
          </div>
        </div>
     
       
    </div>
  
</div>




  
                    </div>
                    
                 
                 
                    
                </div>
            

           
                
            
           @endsection
          