@extends('admin.master')
@section('bodyData')

<section class="forms-section" style='min-height:500px'>
    <div class="outer-w3-agile mt-3">
        <a href='{{url("/adminshowtours")}}' class='btn btn-primary'>Back</a>
        <h4 class="tittle-w3-agileits mb-4">Tour Creation</h4>

        @if ($message = Session::get('inquiryAdd'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $message }}</strong>
        </div>
        @endif

        <form id="inserttouradmin" action="{{url('inserttouradmin')}}" method="post" enctype='multipart/form-data'>
            @csrf
            <div class="row">
                <div  class="col-md-4">
                    <div class="form-group">
                        <label for="inputEmail4"> Tour Name</label>
                        <input type="text" class="form-control" id="inputEmail4" placeholder="Enter Tour Name" 
                            name='name' required="" data-bv-notempty-message="The Tour Name is required"  pattern='[A-Za-z]{2,30}'>
                    </div>
                </div>
                <div  class="col-md-4">
                    <div class="form-group">
                        <label for="inputEmail4">Slug</label>
                        <input type="text" class="form-control" 
                        data-bv-stringlength="true"
                        data-bv-stringlength-min="3"
                        data-bv-stringlength-max="20"
                        data-bv-stringlength-message="Slug minimum length is 3 and maxmum length is 20"
                        data-bv-regexp="true"
                        data-bv-regexp-regexp="^[a-z0-9]*$"
                        data-bv-regexp-message="The Slug can consist of lower case  alphabetical characters and no spaces "
                        data-bv-remote-url = "{{ url('/tourcheckSlug') }}"
                        data-bv-remote = "true"
                        data-bv-remote-type = "GET"   
                        data-bv-remote-message="Opps ! Slug Already Used." id="inputEmail6" placeholder="Enter slug" required="" name='slug' data-bv-notempty-message="The slug is required">
                    </div>
                </div>
                <div  class="col-md-4">
                    <div class="form-group">
                        <label for="inputEmail4">Package Type</label>
                        <select class='form-control' name='package_type'  required="" data-bv-notempty-message="The Package Type is required">
                            <option value="">Select Package Type</option>
                        @foreach($result as $value)
                        <option  id='{{$value->id}}' value='{{$value->id}}'>{{$value->name}}</option>
                        @endforeach
                        </select>
                    </div>
                </div>
                <div  class="col-md-4">
                    <div class="form-group">
                        <label for="inputEmail03">Price</label>
                        <input type="number" class="form-control" id="inputEmail03" placeholder="Enter Price" 
                            name='price' min="0">
                    </div>
                </div>
                <div  class="col-md-4">
                    <div class="form-group">
                        <label for="inputEmail0Person3">Person</label>
                        <input type="text" class="form-control" id="inputEmail0Person3" placeholder="Enter Person" 
                            name='person'>
                    </div>
                </div>
                <div  class="col-md-4">
                    <div class="form-group">
                        <label for="inputEmail4">Banner Image</label>
                        <input type="file" name="banner_img" class="form-control" id="inputEmail4" required
                        data-bv-notempty-message="Banner Image is required."
                        data-text="Select Image" data-dragdrop="true" data-btnClass="btn-primary" data-buttonBefore="true">
                    </div>
                </div>
            </div><br>
            <hr> <hr> <hr><br>
            <div class="row">
                <div  class="col-md-3">
                    <div class="form-group">
                        <label for="region">Region:</label>
                        <input type='text' name='region' id='region' class='form-control'>

                    </div>
                </div>
                <div  class="col-md-3">
                    <div class="form-group">
                        <label for="duration">Duration</label>
                        <input type='text' name='duration' id='duration' class='form-control' value=''>
                    </div>
                </div>

                <div  class="col-md-3">
                    <div class="form-group">
                        <label for="start">Start Point :&nbsp;&nbsp;</label>
                        <input type='text' name='start' id='start' class='form-control'  value=''>
                    </div>
                </div>
                <div  class="col-md-3">
                    <div class="form-group">
                        <label>End Point :&nbsp;&nbsp;</label>
                        <input type='text' name='end' id='End' class='form-control'   value=''>
                    </div>
                </div>
                <div  class="col-md-3">
                    <div class="form-group">
                        <label>Highest Altitude:</label>
                        <input type='text' name='altitute' id='count' class='form-control'  value=''>
                    </div>
                </div>
                <div  class="col-md-3">
                    <div class="form-group">
                        <label>Approx km:&nbsp;&nbsp;</label>
                        <input type='text' id='count' class='form-control' name='approx'>
                    </div>
                </div>
                <div  class="col-md-3">
                    <div class="form-group">
                        <label>Grade:&nbsp;&nbsp;</label>
                        <input type='text' name='grade' id='Grade' class='form-control'>
                    </div>
                </div>
            </div><br>
            <hr><hr><hr><br>
            <div class="row">
                <div  class="col-md-3">
                    <div class="form-group">
                        <label for="Detail1">Detail1</label>
                        <label for="detaile1label">Enter Label Title:&nbsp;&nbsp;</label>
                        <input id='detaile1label' class='form-control' type='text' name='detaile1label'
                            value=''>
                    </div>
                </div>
                <div  class="col-md-3">
                    <div class="form-group">
                        <label for="detail1value">Enter Detail1:&nbsp;&nbsp;</label>
                        <textarea name='detail1value' id='detail1value' class='form-control'></textarea>
                    </div>
                </div>

                <div  class="col-md-3">
                    <div class="form-group">
                        <label for="Detail2">Detail2</label>
                        <label for="detaile1label">Enter Label Title2:&nbsp;&nbsp;</label>
                        <input id='detaile2label' class='form-control' type='text' name='detaile2label'
                            value=''>
                    </div>
                </div>
                <div  class="col-md-3">
                    <div class="form-group">
                        <label for="detail2value">Enter Detail2:&nbsp;&nbsp;</label>
                        <textarea name='detail2value' id='detail2value' class='form-control'></textarea>
                    </div>
                </div>
                 <div  class="col-md-3">
                    <div class="form-group">
                        <label for="Detail3">Detail 3</label>
                        <label for="detaile3label">Enter Label Title 3:&nbsp;&nbsp;</label>
                        <input id='detaile3label' class='form-control' type='text' name='detaile3label'
                            value=''>
                    </div>
                </div>
                <div  class="col-md-3">
                    <div class="form-group">
                        <label for="detail3value">Enter Detail 3:&nbsp;&nbsp;</label>
                        <textarea name='detail3value' id='detail3value' class='form-control'></textarea>
                    </div>
                </div>

                <div  class="col-md-3">
                    <div class="form-group">
                        <label for="Detail3">Detail 4</label>
                        <label for="detaile3label">Enter Label Title 4:&nbsp;&nbsp;</label>
                        <input id='detaile4label' class='form-control' type='text' name='detaile4label'
                                value=''>
                    </div>
                </div>
                <div  class="col-md-3">
                    <div class="form-group">
                        <label for="detail3value">Enter Detail 4:&nbsp;&nbsp;</label>
                        <textarea name='detail4value' id='detail4value' class='form-control'></textarea>
                    </div>
                </div>
                <div  class="col-md-6">
                    <div class="form-group">
                        <label for="inclusion">1. Inclusion</label>
                        <textarea name='inclusion' id='inclusion' class='form-control'></textarea>
                    </div>
                </div>
                <div  class="col-md-6">
                    <div class="form-group">
                        <label for="optional_charges">2. Cancellation Charges</label>
                        <textarea name='optional_charges' id='optional_charges' class='form-control'></textarea>
                    </div>
                </div>
                <div  class="col-md-6">
                    <div class="form-group">
                        <label for="exclusion">3. Exclusion</label>
                        <textarea name='exclusion' id='exclusion' class='form-control'></textarea>
                    </div>
                </div>
                <div  class="col-md-6">
                    <div class="form-group">
                        <label for="essentials">4. Basic Essentials</label>
                        <textarea name='essentials' id='essentials' class='form-control'></textarea>
                    </div>
                </div>
                <div  class="col-md-6">
                    <div class="form-group">
                        <label for="clothing_essential">5. Clothing Essential</label>
                        <textarea name='clothing_essential' id='clothing_essential' class='form-control'></textarea>
                    </div>
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>

</section>

@endsection


@push('css')
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css"/>
@endpush
@push('js')
<script src="//cdn.ckeditor.com/4.16.2/standard/ckeditor.js"></script>
<script src="{{ url('js/ckfinder/ckfinder.js') }}"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js"></script>
<script>

 
  $(document).ready(function () {
    $('#inserttouradmin').bootstrapValidator({
        fields: {
            banner_img: {
                validators: {
                    file: {
                        minFiles: 1,
                        extension: 'jpeg,png,jpg',
                        type: 'image/jpeg,image/png/jpg',
                        maxSize: 5120 * 5120 * 5120 * 5120 * 5120, 
                        message: 'The selected file is not valid, it should be (jpeg,png,jpg) and 5 MB at maximum.'
                    }
                }
            }
        }
    });

     var editor =  CKEDITOR.replace( 'inclusion' );
     var editor =  CKEDITOR.replace( 'optional_charges' );
     var editor =  CKEDITOR.replace( 'exclusion' );
     var editor =  CKEDITOR.replace( 'essentials' );
     var editor =  CKEDITOR.replace( 'clothing_essential' );      
        CKFinder.setupCKEditor( editor );
  });
   
    function demo() {
        var c = document.getElementById('count').value;

        for (var i = 1; i <= c; i++) {
            var node = document.createElement('tr');
            var node1 = document.createElement('td');

            var label1 = document.createElement('input');
            label1.id = 'keys';
            label1.name = 'keys[]';
            label1.placeholder = 'Enter Label';
            node1.appendChild(label1);
            node.appendChild(node1);


            var node1 = document.createElement('td');
            var label1 = document.createElement('input');
            label1.id = 'values';
            label1.name = 'values[]';
            label1.placeholder = 'Enter Value';
            node1.appendChild(label1);
            node.appendChild(node1);
            node.appendChild(node1);
            tby.appendChild(node);
        }

    }
</script>

@endpush
