@extends('admin.master')
@section('bodyData')
@if ($message = Session::get('deleteTarget'))
<div class="alert alert-success alert-block">
	<button type="button" class="close" data-dismiss="alert">×</button>	
        <strong>{{ $message }}</strong>
</div>
@endif
@if ($message = Session::get('updateTarget'))
<div class="alert alert-success alert-block">
	<button type="button" class="close" data-dismiss="alert">×</button>	
        <strong>{{ $message }}</strong>
</div>
@endif

<section class="forms-section" style='min-height:500px'>
<div class="outer-w3-agile mt-3">
<div class='row'>
    &nbsp;&nbsp;
    <a href="{{url('targetCreation')}}"><button class="btn btn-primary">Add Target</button></a>
<div class='col-sm-2'></div>
</div>
<br>

    <table class="table">
        <thead class="thead-dark"><tr>
            <th scope="col">Name</th>
            <th scope="col">Month</th>
            <th scope="col">Target</th>
           
           
             <th scope="col">Action</th>
            </tr>
    </thead>
    <tbody id='tableid'>

            <tr>
            <?php  foreach ($data as $value) {
          # code...
        ?>
                <td scope="row"><?php echo ucfirst($value->name)?></td>
               <td><?php echo $value->month_year?></td> 
               <td><?php echo $value->target?></td>

               
                    <td>
                     

                         <a href="{{url('edittarget')}}/{{$value->id}}">
                         <!-- <button class="btn btn-info">Update</button> -->
                         <i class="fa fa-edit" aria-hidden="true" title='Edit' style='font-size:20px'></i>
                         </a>
                        <a href="{{url('deletetarget')}}/{{$value->id}}">
                        <i class="fa fa-trash" aria-hidden="true" onclick='return confirm("Are you Sure Delete this Target?")' style='color:red;font-size:20px' title='Delete'></i>
                    </a>

                </td>
              
            </tr>  
            <?php } ?>
             
            

           
        </tbody>
    </table>
   

    <center>
   <div class="container" style='margin-left:30%'>
   <?php echo $data->render(); ?>

</div>

</center>
  
    


</div>
@endsection

