@extends('admin.master')
@section('bodyData')
@push('css')
<link rel="stylesheet"
    href="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css" />
@endpush

<section class="forms-section">
    <div class="outer-w3-agile mt-3">
        <h4 class="tittle-w3-agileits mb-4">Total Leave Creation</h4>
        <form action="{{url('creationtotaLeave')}}" method="post" id="year" class="needs-validation" novalidate>
            @csrf
            <div class="form-row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Casual Leave</label>
                        <input type="text" class="form-control"placeholder="No. of Casual Leave" required="" name='casual' data-bv-notempty-message="The Casual Leave is required">
                    </div>
                    <div class="form-group">
                        <label >Planned Leave</label>
                        <input type="text" class="form-control" placeholder="No. of Planned Leave" name='planned' required="" data-bv-notempty-message="The Planned Leave is required">
                    </div>
                    <div class="form-group">
                       <label>Apply Year</label>
                       <input type="number" class="form-control" placeholder="YYYY" min="2020" max="2050" name='year' required="" data-bv-notempty-message="The Apply Year is required">
                    </div>
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</section>

@endsection
@push('js')
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js"></script>
<script>
$(document).ready(function() {
    $('#year').bootstrapValidator();

});

    document.querySelector("input[type=number]")
    .oninput = e => console.log(new Date(e.target.valueAsNumber, 0, 1))
</script>


@endpush

