@extends('admin.master')
@section('bodyData')
<meta name="csrf-token" content="{{ csrf_token() }}">
<section class="forms-section" style='min-height:500px'>
<div class="outer-w3-agile mt-3">
        <h4 class="tittle-w3-agileits mb-4">Salary Generate</h4>
<br>
<p style='color:red'>For Month : <?php echo $monthsalary?></p>
<table class="table">
  <thead>
  </thead>
  <tbody>
<table class="table">
  <thead>
 
    <tr>
    <th scope="col">Employee Name</th>
    <th>Amount</th>
    <th>Actions</th>

     </tr>
  </thead>
  <tbody>
  <?php foreach ($usersal as $value) {
          # code...
  ?>
    <tr>
     <td>{{$value->name}} {{$value->surname}}</td>
     <td>{{$value->total_amount}}</td>
     <input type='hidden' name='id' value='{{$value->id}}'>
      
      <?php
      $modelData = array(

        'name'=>$value->name." ".$value->surname,
        'id'=>$value->id,
        'amount'=>$value->total_amount,
      'salaryid'=>$value->salaryid
      ) ;
	 $modelData2 = array(

        'name'=>$value->name." ".$value->surname,
        'id'=>$value->id,
        'amount'=>$value->total_amount,
     	'pf'=>$value->pf,
     	'otherallowance'=>$value->otherallowance,
     	'grratuti'=>$value->grratuti,
     	'total_leave_days'=>$value->total_leave_days,
     	'house_rent'=>$value->house_rent,
     	'basic_salary'=>$value->basic_salary,
     	'other_charges'=>$value->other_charges,
     	'esic'=>$value->esic,
     	'tax'=>$value->tax,
     	'remark'=>$value->remark,
     	'salary_leave_deducation'=>$value->salary_leave_deducation,
        'emp_esic'=>$value->emp_esic,
        'salaryid'=>$value->salaryid
      ) ;?>
    <td>
     <button class='btn btn-success' data-toggle="modal" data-target="#exampleModal" data-whatever='<?php print_r($modelData['name']) ?>' data-whatever2='<?php echo $modelData['id']?>' 
        data-whatever3='<?php echo $modelData['amount']?>' data-whatever4='<?php echo $modelData2['pf']?>' data-whatever5='<?php echo $modelData2['otherallowance']?>'
        data-whatever6='<?php echo $modelData2['grratuti']?>' data-whatever7='<?php echo $modelData2['total_leave_days']?>'  data-whatever8='<?php echo $modelData2['basic_salary']?>'
        data-whatever9='<?php echo $modelData2['other_charges']?>' data-whatever10='<?php echo $modelData2['remark']?>' data-whatever11='<?php echo $modelData2['house_rent']?>'
             data-whatever12='<?php echo $modelData2['esic']?>' data-whatever13='<?php echo $modelData2['tax']?>' data-whatever14='<?php echo $modelData2['salary_leave_deducation']?>'
             data-whatever15='<?php echo $modelData2['emp_esic']?>' data-whatever16='<?php echo $modelData2['salaryid']?>'>Detail</button>
   
     <button class='btn btn-success'>Payment Gateway</button>
      
       <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCash" data-whatever='<?php echo $modelData['salaryid']?>' data-whateverdate='<?php echo $monthsalary?>'>Cash</button>
<!--          <button class='btn btn-success' data-toggle="modal" data-target="#exampleModal" data-whatever='<?php print_r($modelData['name']) ?>' data-whatever2='<?php echo $modelData['id']?>' data-whatever3='<?php echo $modelData['amount']?>' data-whatever4='<?php echo $monthsalary?>'>Cash</button>
 -->
     
    <button class='btn btn-success' data-toggle="modal" data-target="#exampleModal2" data-whatever='<?php print_r($modelData['name']) ?>' data-whatever2='<?php echo $modelData['id']?>' data-whatever3='<?php echo $modelData['amount']?>' data-whatever4='<?php echo $monthsalary?>'>Change Payment</button>

    </td>
    <td>
   
   <?php if($value->pay_done == 'yes'){?>
    <span style='color:red'><?php  echo "Payment Alredy Pay" ?></span>
   
   <?php } } ?>
    </td>
</tr>
 </tbody>
</table>
  <div class="modal fade" id="exampleModalCash" tabindex="-1" role="dialog" aria-labelledby="exampleModalCashLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
<!--         <h5 class="modal-title" id="exampleModalCashLabel">New message</h5> -->
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action='cashpaymentfinal' method='post'>
        @csrf
          <div class="form-group">
          <input type='hidden' name='payid' id='payid'>
            <label for="recipient-name" class="col-form-label">Remark:</label>
          	<input type='hidden' name='mdate' id='mdate1'>
            <input type="text" class="form-control" id="recipient-name" name='cashremarkdone' required>
          </div>
          <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <input class="btn btn-primary" type='submit' value='Done'>
      </div>
         
        </form>
      </div>
    
    </div>
  </div>
</div>
  
  
<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModal2Label" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method='post' action='cashpayment'>
        @csrf
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Employee Name:</label>
            <input type="text" class="form-control" id="recipient-name" disabled>
          </div>
          <input type='hidden' id='id' name='id'>
          <input type='hidden' id='month_year' name='month_year' value='<?php echo $monthsalary ?>'>
          <span><?php echo $monthsalary?></span>
         
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Amount:</label>
            <input type="text" class="form-control" id="amount" name='amount' disabled>
          <input type="hidden" class="form-control" id="amount1" name='amount'>
          	
          </div>
        
         <div class="form-group">
            <label for="message-text" class="col-form-label">Label For Salary Slip:</label>
           <input type='text' name='label' id='label' required>
          </div>
        
          <div class="form-group">
            <label for="message-text" class="col-form-label">Remark:</label>
            <textarea class="form-control" id="message-text" name='remark' id='remark' required></textarea>
          </div>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Other Charges:</label>
            <input type="number" class="form-control" id="amount" name='other_charges' value=0 required>
          </div>
      
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <input type="submit" class="btn btn-primary" onclick='confirm("Sure,You Want to Change Payment ???")' value='Change Payment'>
      </div>
      </form>
    </div>
  </div>
</div>
 <!--   /******************************************************Salary Information ---------------------------****************************** -->
  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
      <span>Salary Detail Of Employee</span>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method='post' action='editsalaryslip'>
        @csrf
          <div class="form-group">

            <label for="recipient-name" class="col-form-label">Employee Name:</label>
            <span id="recipient-name"></span>
          </div>
          <input type='hidden' id='id' name='id'>
          <input type='hidden' id='month_year' name='month_year' value='<?php echo $monthsalary ?>'>
        
         
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Total Amount:</label>
            <input type="text" class="form-control" id="amount" name='amount' required>
          </div>
          <div class="form-group">
            <label for="message-text" class="col-form-label">PF:</label>
            <input typr='text' class="form-control" id="pf" name='pf' id='remark' required>
          </div>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Other Charges:</label>
            <input type="text" class="form-control" id="other_charges" name='other_charges' required>
          </div>
           <div class="form-group">
            <label for="recipient-name" class="col-form-label">Other Allowance:</label>
            <input type="text" class="form-control" id="otherallowance" name='otherallowance' required>
          </div>
           <div class="form-group">
            <label for="recipient-name" class="col-form-label">Basic Salary:</label>
            <input type="text" class="form-control" id="basic_salary" name='basic_salary' required>
          </div>
          <input type='hidden' name='salaryid' id='salaryid'>
           <div class="form-group">
            <label for="recipient-name" class="col-form-label">House Rent:</label>
            <input type="text" class="form-control" id="houserent" name='houserent' required>
          </div>
       
         <div class="form-group">
            <label for="recipient-name" class="col-form-label">Grratuti:</label>
            <input type="text" class="form-control" id="grratuti" name='grratuti' required>
          </div>
        
        	<div class="form-group">
            <label for="recipient-name" class="col-form-label">Total leave Days:</label>
            <input type="text" class="form-control" id="total_leave_days" name='total_leave_days' required>
          </div>
        
        	<div class="form-group">
            <label for="recipient-name" class="col-form-label">ESIC:</label>
            <input type="text" class="form-control" id="esic" name='esic' required>
          </div>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Employer ESIC:</label>
            <input type="text" class="form-control" id="emp_esic" name='emp_esic' required>
          </div>
        <div class="form-group">
            <label for="recipient-name" class="col-form-label">Tax:</label>
            <input type="text" class="form-control" id="tax" name='tax' required>
          </div>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Leave Deducation:</label>
            <input type="text" class="form-control" id="salary_leave_deducation" name='salary_leave_deducation' required>
          </div>
        
        
<!--         	<div class="form-group">
            <label for="recipient-name" class="col-form-label">Total leave Days:</label>
            <input type="text" class="form-control" id="total_leave_days" name='total_leave_days' required>
          </div> -->
        
        	<div class="form-group">
            <label for="recipient-name" class="col-form-label">Remark:</label>
            <input type="text" class="form-control" id="remark" name='remark' required>
          </div>
      </div>
      <div class="modal-footer">
       <button type="submit" class="btn btn-primary" onclick='return confirm("Sure You Want To Change Salary Data?")'>Update Salary Data</button>
       <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>

      </div>
      </form>
    </div>
  </div>
</div>
<script>
$('#exampleModal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) // Button that triggered the modal
  var recipient = button.data('whatever') // Extract info from data-* attributes
  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
  var id = button.data('whatever2')
  var amount = button.data('whatever3')
  var pf = button.data("whatever4");
  var otherallowance = button.data("whatever5");
  var gratuti = button.data("whatever6");
var total_leave_days = button.data("whatever7");
var basicsalary = button.data("whatever8");
var other_charges = button.data("whatever9");
var remark = button.data("whatever10");
var houserent = button.data("whatever11");
var emp_esic = button.data("whatever15");
var salaryid = button.data("whatever16");




var esic = button.data("whatever12");
var tax = button.data("whatever13");
var salary_leave_deducation = button.data('whatever14');
  
  var modal = $(this)
 // console.log(recipient);
  modal.find('#modal-title1').text('Salary Details');
  modal.find('#recipient-name').text(recipient);
  modal.find('#pf').val(pf);
  modal.find('#otherallowance').val(otherallowance);
modal.find('#other_charges').val(other_charges); 
modal.find('#grratuti').val(gratuti);
modal.find('#total_leave_days').val(total_leave_days);
modal.find('#basic_salary').val(basicsalary);
modal.find('#remark').val(remark);
modal.find('#id').val(id);
modal.find('#amount').val(amount);
modal.find('#houserent').val(houserent);
modal.find('#esic').val(esic);
modal.find('#tax').val(tax);
modal.find('#emp_esic').val(emp_esic);
modal.find('#salary_leave_deducation').val(salary_leave_deducation);
modal.find('#salaryid').val(salaryid);

})
</script>
  <script>
$('#exampleModal2').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) // Button that triggered the modal
  var recipient = button.data('whatever') // Extract info from data-* attributes
  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
  var id = button.data('whatever2')
  var amount = button.data('whatever3')
  var modal = $(this)
 
  modal.find('.modal-title').text('New message to ' + recipient);
  modal.find('#recipient-name').val(recipient);
  modal.find('#pf').val(pf);
  modal.find('#id').val(id);
	modal.find('#amount').val(amount);
	modal.find('#amount1').val(amount);
})
  
  $('#exampleModalCash').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) // Button that triggered the modal
  var recipient = button.data('whatever')
  var mdate = button.data('whateverdate')
  alert(mdate);
  // Extract info from data-* attributes
  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
  //alert(recipient);
  var modal = $(this)
  // modal.find('.modal-title').text('New message to ' + recipient)
  modal.find('#payid').val(recipient)
  modal.find('#mdate1').val(mdate);
  })
</script></tbody>
</table>
</div></section>
@endsection
