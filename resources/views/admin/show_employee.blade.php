@extends('admin.master')
@section('bodyData')
{{-- <script>
function searchEmployee(name)
{   
   
      var query= name.value;
      // console.log(query);
    $.ajax({
   url:"{{ route('emplive') }}",
   method:'GET',
   data:{query:query},
   dataType:'json',
   success:function(data)
   {
   console.log(data.table_data);
      $('#tableid').empty();
    // $('tbody').html(data.table_data);
    $('#tableid').append(data.table_data);
   }
  })
}
</script> --}}
@if ($message = Session::get('EmployeeLeaveUpdated'))
<div class="alert alert-success alert-block">
	<button type="button" class="close" data-dismiss="alert">×</button>	
        <strong>{{ $message }}</strong>
</div>
@endif
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
      <h6 class="modal-title" id="myModalLabel">Are You sure To Relive This Employee?</h6>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
        
      </div>
      <div class="modal-body">
      <form method='post' action='{{ url("empleave") }}'>
      @csrf
        <input type='hidden' id="modal-myvalue" name='id'>
        <input type='date' id='leave_date' name='leave_date' min="{{date('Y-m-d')}}">
        <br><br>
        <button type='submit' class='btn btn-primary' name='submit'>Submit</button>
       </form>
         
      
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
       
      </div>
    </div>
  </div>
</div>
<div class="outer-w3-agile mt-3">
    <div class='row'>
      <div class='col-md-6'>
        <div class="btn btn-primary"><i class="fa fa-list mr-2"></i>Employee list</div>
      </div>
      <div class='col-md-6 text-right'>
        <button class="btn btn-primary" onclick="window.location='{{ url("admininsertemployee") }}'">
          <i class="fa fa-plus mr-2"></i>Employee</button>
      </div>
    </div>
<br>
{{-- <div class="row"> --}}
  <div class="table-responsive">
    <table class="table table-hover" style="width: 100%;" id="dataTable">
      <thead class="thead-dark">
        <tr>
           <th>#</th>
           <th>ID</th>
            <th>Name</th>
            <th>Contact Number</th>
            <th>Email</th>
            <th>Joining Date</th>
            <th>Salary</th>
            <th>Status</th>
            <th>Action</th>
        </tr>
      </thead>
    </table>
  </div>
    
    </div>
@endsection
@push('js')
<script>
  $(function() {
    $("#dataTable").DataTable({
        'processing': true,
        'serverSide': true,
        'serverMethod': 'post',
        "aLengthMenu": [[10,25, 50,100], [10,25, 50,100]],
        order: [
            [0, 'desc']
        ],
        "ajax": {
            "url": "{{ route('employee.show') }}",
            "data": {
                "_token": "{{ csrf_token() }}"
            },
        },
        "columns": [
            {
                data: "id",
                render: function(data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1
                }
            },
            {
                data: "id",
                render: function(data, row, alldata) {
                   return (data)?'JJ-EMP-'+alldata.id:'-';
                }
            },
            {
                data: "name",
                render: function(data, row, alldata) {
                    $html = (data)?data+" "+alldata.surname:'-';
                    return $html;
                }
            },
            {
                data: "contact_number",orderable:false,
            },
            {
                data: "email",orderable:false,
            },
            {
                data: "joining_date",orderable:false,
                render: function(data, row, alldata) {
                    $html = moment(alldata.joining_date).format('DD-MM-YYYY');
                    return $html;
                }
            },
            {
                data: "salary",orderable:false
            },
            {
                data: "employee_approved",orderable:false,
                render: function(data, row, alldata) {
                    if(data==2)
                    {
                      return '<h6 style="font-size:15px;"><span class="badge badge-danger p-2">Rejected</span></h6>';
                    }
                    else if(data==1)
                    {
                      return '<h6 style="font-size:15px;"><span class="badge badge-success p-2">Approved</span></h6>';
                    }
                    else
                    {
                      return '<h6 style="font-size:15px;" class="emp_status" data-id="'+alldata.id+'"><span class="badge badge-warning p-2">Pending</span></h6>';
                    }
                }
            },
            {
                data: "id",orderable:false,
                render: function(data, row, alldata) {
                  var $id = btoa(alldata.id);
                    $html = `<a href="{{url('/employeedetails')}}/`+$id+`">
                      
                      <i class="fa fa-info-circle" aria-hidden="true" title='Detail View' style='font-size:20px'></i>
                        </a>   

                         <a href="{{url('/employeeedit')}}/`+$id+`">
                         <!-- <button class="btn btn-info">Update</button> -->
                         <i class="fa fa-edit" aria-hidden="true" title='Edit' style='font-size:20px'></i>
                         </a>
                      
                      <i class="fa fa-stop-circle leave" aria-hidden="true" data-toggle="modal" data-target="#myModal" style='font-size:20px;color:red' title='Leave' data-id="`+alldata.id+`" >
                      </i>`;
                    return $html;
                }
            },
        ]
    });

    // $(document).on('click','.emp_status',function(){
    //   var emp_id = $(this).data('id');
    //   swal({
    //     title: 'Are you sure ?',
    //     icon:'warning',
    //     text: 'you want to change employee status',
    //     buttons: {
    //       Reject: true,
    //       Approve: true         
    //     },
    //   }).then((result) => {
    //     // console.log(result);
    //     if (result=="Approve") {
    //       var status = 1;
    //     } else if (result=="Reject") {
    //       var status = 2;
    //     }
    //     if(status)
    //     {
    //       $.ajax({
    //         method:'post',
    //         url: "{{route('employee.status')}}",
    //         data: {status:status,id:emp_id,
    //           '_token':"{{csrf_token()}}"
    //         },
    //         success : function(result){
    //           swal({title:result.title,icon:result.icon,text:result.message});
    //           $('#dataTable').DataTable().ajax.reload();
    //         }
    //       });
    //     }
    //   })
    // });

    $(document).on('click','.leave',function(){
      var id = $(this).data('id');
      // alert('true');
      $('#modal-myvalue').val(id);
    });
  });
</script>
@endpush
