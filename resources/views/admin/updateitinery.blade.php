@extends('admin.master')
@section('bodyData')

@if ($message = Session::get('Itenaryadded'))
<div class="alert alert-success alert-block">
	<button type="button" class="close" data-dismiss="alert">×</button>	
        <strong>{{ $message }}</strong>
</div>
@endif
<h6 styla='color:red'>Update Itinary</h6>
<a href='/adminshowtours'><button class='btn btn-primary'>Show Tour</button></a>
<form method='post' action='/edititi' enctype="multipart/form-data">
  @csrf
  
  <div class='row'>
  <div class='col-sm-6'>
  <label>Enter Details</label>
  
  <input type='text' name='details' placeholder='Enter Details' style='height:200px;width:400px' value='<?php echo $data[0]->details?>'>
  </div>
  <div class='col-sm-6'>
  <label>Tour Id</label>
  <input type='hidden' name='id' value='<?php echo $data[0]->id?>'>
  <select name='tour_fk_id'>
  <?php  $tmp =  $data[0]->tour_fk_id;?>
  <?php foreach($toures as $t){ ?>
  <option value='<?php echo $t->id?>' <?php if($t->id == $tmp){?>selected <?php } ?>>
  {{$t->name}}
  </option>
  <?php } ?>
  </select>
  
	</div>
  </div>
  <div class='row'>
  <div class='col-sm-12'>
  <label>Day</label>
  
    <input type='text' name='day_number' placeholder='Enter Day' class='form-control' value='<?php echo $data[0]->day_number?>'>
	

  </div>
   </div>

   <div class='row'>
  <div class='col-sm-12'>
  <label>Caption</label>
  
    <input type='text' name='caption' placeholder='Enter Caption' class='form-control' value='<?php echo $data[0]->caption?>'>


  </div>
   </div>

   <div class='row'>
  <div class='col-sm-12'>
  <label>Image</label>
  
    <input type='file' name='images' class='form-control'>
    <img src='<?php echo asset("itenary")."/".$data[0]->images?>' style='width:40px;height:40px' name='images'/>


  </div>
   </div>

   <br>
  <input type='submit' value='Update' class='btn btn-success'>
    </form>
    @endsection