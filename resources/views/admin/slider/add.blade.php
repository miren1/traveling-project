@extends('admin.master')
@section('bodyData')
<section>
    <div class="outer-w3-agile mt-3">
        <h4 class="tittle-w3-agileits mb-4">@if(isset($slider))Edit @else Add @endif Banner</h4>
        
        @if ($message = Session::get('admincreation'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>	
                <strong>{{ $message }}</strong>
        </div>
        @endif
          
        <form action="{{route('admin-banner.store')}}" id="bannerForm" method="post" enctype="multipart/form-data">
        @csrf
            <div class="row">
                @if(isset($slider))
                    <input type="hidden" name="id" value="{{$slider->id}}"/>
                @endif   
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="inputEmail4">Title</label>
                        <input type="text" class="form-control" id="title" value="@if(isset($slider)){{$slider->title}}@endif" placeholder="Banner Title" minlength="3" maxlength="40" required name='title' data-bv-notempty="true" data-bv-notempty-message="The Title is required">
                    </div>
                    <div class="form-group">
                        <label for="inputEmail4">Description</label>
                        <textarea class="form-control" rows="4" maxlength="255" id="desc" placeholder="Banner Description" name='description'>@if(isset($slider)){{$slider->description}}@endif</textarea>
                    </div>  
                
                    @if(isset($slider))
                        <div class="form-group">
                            <label for="inputEmail4">Image <span style="color: red">*please upload 1920 * 1280 resolution image.</span></label>
                            <input type="file" name="image" data-bv-file-extension="jpg,jpeg,png" data-bv-file="true" class="form-control dropify" data-default-file="{{url('public/banner').'/'.$slider->image}}" />
                        </div>
                    @else
                        <div class="form-group">
                            <label for="inputEmail4">Image <span style="color: red">*please upload 1920 * 1280 resolution image</span></label>
                            <input type="file" name="image" class="form-control dropify" required
                            data-bv-file-extension="jpg,jpeg,png" data-bv-file="true" data-bv-notempty="true"
                             data-bv-notempty-message="The Image is required"/>
                        </div>
                    @endif
                
                    <div class="form-group">
                        <label for="inputEmail4">Video URL</label>
                        <input type="url" class="form-control" id="video_url" value="@if(isset($slider)){{$slider->video_url}}@endif" placeholder="Banner Video Url" name='video_url' data-bv-uri-message="The website address is not valid">
                    </div>
                </div>
            </div>
            <button  type="submit" class="btn btn-primary">@if(isset($slider))Update @else Submit @endif</button>
        </form>
    </div>

</section>

@endsection
@push('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.css" integrity="sha512-In/+MILhf6UMDJU4ZhDL0R0fEpsp4D3Le23m6+ujDWXwl3whwpucJG1PEmI3B07nyJx+875ccs+yX2CqQJUxUw==" crossorigin="anonymous" />
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css"/>
@endpush
@push('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/js/dropify.min.js" integrity="sha512-8QFTrG0oeOiyWo/VM9Y8kgxdlCryqhIxVeRpWSezdRRAvarxVtwLnGroJgnVW9/XBRduxO/z1GblzPrMQoeuew==" crossorigin="anonymous"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js"></script>

<script>

  $(document).ready(function () {
    $('.dropify').dropify();
  
    $('#bannerForm').bootstrapValidator({
        fields: {
            image: {
                validators: {
                    file: {
                        extension: 'jpeg,png',
                        type: 'image/jpeg,image/png',
                        maxSize: 1940 * 1390,
                        message: 'The selected file size is not valid'
                    }
                }
            }
        }
    });
  });
</script>
@endpush