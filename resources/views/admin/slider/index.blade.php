@extends('admin.master')
@section('bodyData')

@if ($message = Session::get('EmployeeLeaveUpdated'))
<div class="alert alert-success alert-block">
	<button type="button" class="close" data-dismiss="alert">×</button>	
        <strong>{{ $message }}</strong>
</div>
@endif


<div class="outer-w3-agile mt-3">
    <div class='row'>
      <div class='col-md-6'>
        <div class="btn btn-primary"><i class="fa fa-list mr-2"></i>Banner list</div>
      </div>
      <div class='col-md-6 text-right'>
        <button class="btn btn-primary" onclick="window.location='{{ route("admin-banner.add") }}'">
          <i class="fa fa-plus mr-2"></i>Add Banner</button>
      </div>
    </div>
<br>
{{-- <div class="row"> --}}
  <div class="table-responsive">
    <table class="table table-hover" style="width: 100%;" id="dataTable">
      <thead class="thead-dark">
        <tr>
            <th>#</th>
            <th>Title</th>
            <th>Description</th>
            <th>Image</th>
            <th>Action</th>
        </tr>
      </thead>
    </table>
  </div>
    
    </div>
@endsection
@push('js')
<script>
  $(function() {
    $("#dataTable").DataTable({
        'processing': true,
        'serverSide': true,
        'serverMethod': 'post',
        "aLengthMenu": [[10,25, 50,100], [10,25, 50,100]],
        order: [
            [0, 'desc']
        ],
        "ajax": {
            "url": "{{ route('admin-banner.show') }}",
            "data": {
                "_token": "{{ csrf_token() }}"
            },
        },
        "columns": [
            {
                data: "id",
                render: function(data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1
                }
            },
            {
                data: "title",orderable:false,
            },
            {
                data: "description",orderable:false,
            },
            {
                data: "image",orderable:false,
                render: function(data, row, alldata) {
                    return `<img src="{{url('/banner')}}/`+alldata.image+`" hieght="100" width="100" />`;
                }
            },
            {
                data: "id",orderable:false,
                render: function(data, row, alldata) {
                  var $id = btoa(alldata.id);
                    $html = `<a href="{{url('/admin-banner/edit')}}/`+$id+`">
                         <!-- <button class="btn btn-info">Update</button> -->
                         <i class="fa fa-edit" aria-hidden="true" title='Edit' style='font-size:20px'></i>
                         </a>
                      
                      <i class="fa fa-trash delete" style='font-size:20px;color:red;cursor:pointer;' title='delete banner' data-id="`+alldata.id+`" >
                      </i>`;
                    return $html;
                }
            },
        ]
    });

    $(document).on('click','.delete',function(){
      var id = $(this).data('id');
      swal({
        title: "Are you sure?",
        text: "You want to delete this Banner.",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      }).then((willDelete) => {
        if(willDelete)
        {
          $.ajax({
            method:'post',
            url: "{{route('admin-banner.destroy')}}",
            data: {id:id,
              '_token':"{{csrf_token()}}"
            },
            success : function(result){
              swal({title:result.title,icon:result.icon,text:result.message});
              $('#dataTable').DataTable().ajax.reload();
            }
          });
        }
      });
    });

    $(document).on('click','.leave',function(){
      var id = $(this).data('id');
      // alert('true');
      $('#modal-myvalue').val(id);
    });
  });
</script>
@endpush
