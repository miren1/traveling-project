@extends('admin.master')
@section('bodyData')
@push('css')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css">
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.0/css/bootstrap-toggle.min.css" rel="stylesheet"/>

@endpush
<div class="outer-w3-agile mt-3">
    <div class='row'>
      <div class='col-md-6'>
        <div class="btn btn-primary"><i class="fa fa-list mr-2"></i>Review list</div>
      </div>
      <!-- <div class='col-md-6 text-right'>
        <a class="btn btn-primary" href="{{ route('ads-add')}}"><i class="fa fa-plus mr-2"></i>Review Ads</a>
      </div> -->
    </div>
<br>
{{-- <div class="row"> --}}
  <div class="table-responsive">
    <table class="table table-hover" style="width: 100%;" id="dataTable">
      <thead class="thead-dark">
        <tr>
            <th>#</th>
            <th>User</th>
            <th>Content</th>
            <th>Permission</th>
            <th>Action</th>
        </tr>
      </thead>
    </table>
  </div>
    
    </div>
@endsection

@push('js')

    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.0/js/bootstrap-toggle.min.js"></script>
<script>
  $(function() {
    $("#dataTable").DataTable({
        'processing': true,
        'serverSide': true,
        'serverMethod': 'post',
        "aaSorting": [[ 1, "desc" ]],
        "aLengthMenu": [[10,25, 50,100], [10,25, 50,100]],
        order: [
            [0, 'desc']
        ],
        "ajax": {
            "url": "{{ route('review-list') }}",
            "data": {
                "_token": "{{ csrf_token() }}"
            },
        },
        "columns": [
            {
                data: "id",
                render: function(data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1
                }

            },
            {
                data: "content",
                render: function(data, type, row, meta) {
                  var firstname = '';
                  if(row.member[0]){
                      var fname = row.member[0];
                      firstname = fname.name;  
                    return firstname+' (member)';
                  }
                  
                  if(row.customer){  
                    // console.log('cutomer',row.customer[0].first_name);
                    // return false;
                    try{
                     var fname =  row.customer[0].first_name;

                    }catch(error)
                    {
                      var fname = '';
                    }
                    // if(row.customer[0].first_name != undefined){

                     
                    // }else{
                       
                    // }       
                    try{
                     var mname = row.customer[0].last_name;

                    }catch(error)
                    {
                      var mname = '';
                    }
                    
                       
                      return fname + ' ' + mname +' (user)';
                  }
                }
            },
            {
                data: "content"
            },

            {
                data: "status",
                render: function(data, row, alldata) {
                  var $id = btoa(alldata.id);
                    if(data == 'hide'){
                      $html1 = `<a href="{{ url('review/show/') }}/`+$id+`/show" style="font-size:15px;"><span class="badge badge-danger p-2">Hide</span></a>`;
                    }else{
                      $html1 = `<a href="{{ url('review/show/') }}/`+$id+`/hide" style="font-size:15px;"><span class="badge badge-success p-2">Show</span></a>`;
                    }
                    
                    return $html1;
                }
            },
            
            {
                data: "id",
                render: function(data, row, alldata) {
                  var $id = btoa(row.id);
                    $html = `<a  href="{{ url('review/image_list/') }}/`+btoa(data)+`"><i class="fa fa-eye" aria-hidden="true"' +
                                'title="Detail View" style="font-size:20px"></i></a>

                         <!--       <a href="{{url('ads/edit')}}/`+$id+`">
                          <i class="fa fa-edit" aria-hidden="true" title='Edit' style='font-size:20px'></i>
                         </a> -->
                      
                      <i class="fa fa-trash delete" style='font-size:20px;color:red;cursor:pointer;' title='delete' data-id="`+alldata.id+`" >
                      </i>`;
                    return $html;
                }
            },
        ]
    });

    $(document).on("click", '.delete', function(event) { 
    var id =$(this).attr('data-id');
    //alert(id);
    swal({
      title: "Are you sure?",
      text: "You Want To Delete This Record ,You Will Not Be Able To Recover This Record!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {

        $.ajax({
          url: "{{ url('review/delete') }}/"+id,
          type: 'get',
          dataType: 'json',
        success:function($data){
          if($data == true)
          {
            swal("Record has been deleted!", {
              icon: "success",
            }).then((result) => {
                $('#dataTable').DataTable().ajax.reload(null,false);
            });
            //location.reload('#dataTable');
          }else{
            swal("Record has been not deleted!", {
              icon: "error",
            });
          }
    
        }
      });


      } else {
        swal("Your Record is Safe!");
      }
    });
  });

    $(document).on('click','.leave',function(){
      var id = $(this).data('id');
      // alert('true');
      $('#modal-myvalue').val(id);
    });
  });
</script>
@endpush
