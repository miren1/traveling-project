@extends('admin.master')
@section('bodyData')
<section class="forms-section" style='min-height:500px'>
    <div class="outer-w3-agile mt-3">
        <h4 class="tittle-w3-agileits mb-4">Select Image</h4>
        <form action="{{ route('select-image') }}" id="SelectForm" method="post" enctype="multipart/form-data">
        @csrf
            <div class="row">
                <div class="col-md-12">  
                    <ul>
                        @php($count=1)
                        @if(isset($image))
                        @foreach($image as $key)

                        <input type="hidden" id="id" name="id" value="{{$key->review_id}}"/>
                        <li>
                            <div class="form-group">
                                <input type="radio" name="image" id="cb{{$count}}" value="{{$key->id}}" @if($key->image_select) == 'selected' checked @else @endif>
                                <label for="cb{{$count++}}"><img src="{{$url}}/{{$key->image}}" /></label>
                                <p id="checked" style="color: red; font-size: 80%; margin-left: 20px;"></p>
                            </div>
                        </li>
                        @endforeach
                        @php($count++)
                    </ul>
                @endif
                </div>  
            </div>
            <button type="submit" id="submitbtn" class="btn btn-primary">@if(isset($data))Update @else Submit @endif</button>
        </form>
    </div>
</section>
@endsection
@push('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.min.css" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.8/css/fileinput.min.css">
<link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel = "stylesheet">
<link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel = "stylesheet">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css"/>
<style type="text/css">
ul {
  list-style-type: none;
}

li {
  display: inline-block;
}

input[type="radio"][id^="cb"] {
  display: none;
}

label {
  border: 1px solid #fff;
  padding: 10px;
  display: block;
  position: relative;
  margin: 10px;
  cursor: pointer;
  border-radius: 25px;
}

/*label:before {
  background-color: white;
  color: white;
  content: " ";
  display: block;
  border-radius: 50%;
  border: 1px solid grey;
  position: absolute;
  top: -5px;
  left: -5px;
  width: 25px;
  height: 25px;
  text-align: center;
  line-height: 28px;
  transition-duration: 0.4s;
  transform: scale(0);
}*/
label:before {
    background-color: white;
    color: white;
    content: " ";
    display: block;
    border-radius: 50%;
    border: 1px solid grey;
    position: absolute;
    top: 0px;
    font-size: 25px;
    left: 0px;
    width: 40px;
    height: 40px;
    text-align: center;
    line-height: 36px;
    transition-duration: 0.4s;
    transform: scale(0);
    z-index: 1;
}

label img {
  height: 200px;
  width: 200px;
  transition-duration: 0.2s;
  transform-origin: 50% 50%;
  border-radius: 15px;
}

:checked + label {
  border-color: #ddd;
}

:checked + label:before {
  content: "✓";
  background-color: #083672;
  transform: scale(1);
}

:checked + label img {
  transform: scale(0.9);
  box-shadow: 0 0 5px #333;
  z-index: -1;
}
</style>
@endpush
@push('js')
<script src = "https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="https://cdn.jsdelivr.net/npm/piexifjs@1.0.6/piexif.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.8/js/fileinput.min.js"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js"></script>
<script>
    $( document ).ready(function() 
    {
        $('#SelectForm').bootstrapValidator();  
    });

    $(window).bind("pageshow", function() {
        var form = $('form'); 
        // let the browser natively reset defaults
        form[0].reset();
    });


    $(function() {
        $("#submitbtn").click(function() {   
          if($('input[type=radio][name=image]:checked').length == 0)
          {
             // alert("Please select atleast one");
             $("#checked").text("Please select atleast one");
             return false;
          }
          /*else
          {
              alert("radio button selected value: ");
          }  */    
        });
    });

    
    /*$(function() {
    $("#submitbtn").click(function() {
        var atLeastOneChecked = false;

        $("input[type=radio][name=image]").each(function() {
            if ($(this).attr("checked") == "checked") {

                atLeastOneChecked = true;
            }
        });
        console.log($(this).attr("checked"));
        if (!atLeastOneChecked) {
            $("#checked").text("Check one");
            return false;
        } else {
            $("#checked").text("One is checked");
            return true;
        }
    });
});*/
</script> 

@endpush
