@extends('front.layouts.master')
@section('title','Tours-Plan')
@push('style')
<style>
    .breadcrumb-area.style-two {
        padding: 100px 0 90px !important;
    }
    .single-package-card .details {
        padding: 20px;
        height: 100px;
    }

    .package-meta {
        padding: 0;
        position: absolute;
        bottom: 0;
    }

    .section-title {
        margin-bottom: 63px;
        margin-top: -30px;
    }

    .package-area {
        overflow-y: hidden;
    }

    #ui-datepicker-div {
        display: none;
    }

    .mg-bottom-92 {
        margin-bottom: -30px;
    }

    @media only screen and (max-width: 1680px) {
        .container-bg {
            padding: 31px 0 31px 0;
            /* margin: 0 30px; */
        }
    }

    @media only screen and (max-width: 1280px) {
        .mg-top--70 {
            margin-top: 0px !important;
        }
    }
    .single-package-card .thumb{
        height: 210px;
    }
    .single-package-card .thumb img {
    width: 100%;
    height: 100%;
 }
</style>
@endpush

@section('content')


 <!-- breadcrumb area start -->
 <div class="breadcrumb-area style-two jarallax" style="background-image:url(/assets/img/bg/1.png);">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-inner">
                    @if(isset($packagtype))
                    <h1 class="page-title">{{strtoupper($packagtype['name'])}}</h1>
                     <ul class="page-list">
                        <li><a href="{{url('/')}}">Home</a></li>
                        <li>{{strtoupper($packagtype['name'])}}</li>
                    </ul> 
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb area End -->

<!-- destination area End -->
<div class="destination-area">
    <div class="container-bg mg-top--70 mb-4">
        <div class="container">
            <div class="row justify-content-center">
                <!--Start Package Tour List-->
                <div class="package-area pd-top-108 mg-bottom-92">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-xl-6 col-lg-8">
                                <div class="section-title text-center">
                                    <h2 class="title wow  fadeInUp animated" data-wow-duration="0.6s"
                                        data-wow-delay="0.1s"
                                        style="visibility: visible; animation-duration: 0.6s; animation-delay: 0.1s; animation-name: fadeInUp;">
                                        Best Packages For You</h2>
                                        
                                        <p class="wow  fadeInUp animated" data-wow-duration="0.6s" data-wow-delay="0.2s"
                                        style="visibility: visible; animation-duration: 0.6s; animation-delay: 0.2s; animation-name: fadeInUp;">Experience the best of world with most affordable tour packages with best of services. 
                                        Give us the chance to make your journey be worth remembering.</p>  
                                </div>
                            </div>
                        </div>
                        <div class="row" id="tour-box">
                        </div>
                    </div>
                </div>
                <!--End Package Tour List-->
            </div>
        </div>
    </div>
</div>
<!-- destination area End -->



@endsection

@push('script')

<script>
     $(document).ready(function() {
        var inof= {};
        @if(isset($packagtype))
        inof['id']='{{$packagtype->id}}';
        @endif
        getData(inof);
     });

     $("button#mysearch").on( "click", function() {
      var pname=  $('#pname').val();
      console.log(pname);
      var inof= {};
      @if(isset($packagtype))
        inof['id']='{{$packagtype->id}}';
      @endif
      if(pname!=''){
       
        inof['name']=pname;
        getData(inof);
      }
        
     });

    function getData(array){

        $.ajax({
            url: "{{route('tour.getTours')}}", 
            type: "POST",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(array),
           // data:,
            success: function (result) {
                console.log(result.length);
                $('#tour-box').html('');
                if(result.length!=0){
                    var $html = '';
                    $.each(result, function(key, value) {
                        var obj = jQuery.parseJSON(value.small_info);
                        var Region='';
                        var Duration='';
                        if(obj.Region){
                            Region=obj.Region;
                        }
                        if(obj.Duration){
                            Duration=obj.Duration;
                        }
                        if(value.days == null){
                            days='';
                        }else{
                            days=value.days+'Days';
                        }

                        if(value.person == null){
                            person='';
                        }else{
                            person=value.person;
                        }

                        if(value.price == null){
                            price='';
                        }else{
                           price=value.price;
                           // price='1000';
                        }
                        

                        $html+='<div class="col-xl-3 col-sm-6">'+
                                '<a href="{{url('packages/tour')}}/'+value.slug+'">'+
                                '<div class="single-package-card wow  fadeInUp animated" data-wow-duration="0.4s"'+
                                'data-wow-delay="0.1s"style="visibility: visible; animation-duration: 0.4s; animation-delay: 0.1s; animation-name: fadeInUp;">'+
                                '<div class="thumb"><img src="{{url('media/')}}/'+value.banner_img+'" alt="img"></div>'+
                                '<div class="details">'+
                                 '<div class="location">'+
                                 '<span class="location-name"><img src="{{url('/assets/img/icons/1.png')}}" alt="img" style="display: inherit;">'+value.name+'</span>'+
                                 '<span class="tp-review-meta float-right">'+
                                  '</span></div>'+
                                    '</div></div></a></div>';
                                    
                    
                    });
                    $('#tour-box').html($html);
                }else{

                }
            },
            error: function (err) {
            // check the err for error details
            }
        }); // ajax call closing

    }

</script>


@endpush