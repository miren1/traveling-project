<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>India's No. 1 - Travel Investment Membership Plans  - Jag Joyu</title>
    <!-- favicon -->
    <link rel=icon href="{{url('/front')}}/assets/img/favicon.png" sizes="20x20" type="image/png">

    <!-- Additional plugin css -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/animate.css">
    <link rel="stylesheet" href="assets/css/magnific-popup.css">
    <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="assets/css/slick.css">
    <link rel="stylesheet" href="assets/css/swiper.min.css">
    <link rel="stylesheet" href="assets/css/nice-select.css">
    <link rel="stylesheet" href="assets/css/jquery-ui.min.css">
    <!-- icons -->
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/themify-icons.css">
    <link rel="stylesheet" href="assets/css/line-awesome.min.css">
    <!-- main css -->
    <link rel="stylesheet" href="assets/css/style.css">
    <!-- responsive css -->
    <link rel="stylesheet" href="assets/css/responsive.css">

</head>
<body>
    <!-- preloader area start -->
    <div class="preloader" id="preloader">
        <div class="preloader-inner">
            <div class="spinner">
                <div class="dot1"></div>
                <div class="dot2"></div>
            </div>
        </div>
    </div>
    <!-- preloader area end -->

    <!-- search popup start -->
    <div class="body-overlay" id="body-overlay"></div>
    <div class="search-popup" id="search-popup">
        <form action="index.html" class="search-form">
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Search.....">
            </div>
            <button type="submit" class="submit-btn"><i class="fa fa-search"></i></button>
        </form>
    </div>
    <!-- search popup End -->

    <!-- //. sign up Popup -->
    <div class="signUp-popup login-register-popup" id="signUp-popup">
        <div class="login-register-popup-wrap">
            <div class="row no-gutters">
                <div class="col-lg-6">
                    <div class="thumb">
                        <img src="assets/img/others/signup.png" alt="img">
                    </div>
                </div>
                <div class="col-lg-6 align-self-center">
                    <div class="shape-thumb">
                        <img src="assets/img/others/signup-shape.png" alt="img">
                    </div>
                    <form class="login-form-wrap">
                        <h4 class="text-center">Sign Up</h4>
                        <div class="single-input-wrap style-two">
                            <input type="text" placeholder="Name">
                            <span class="single-input-title"><i class="fa fa-user"></i></span>
                        </div>
                        <div class="single-input-wrap style-two">
                            <input type="text" placeholder="Email">
                            <span class="single-input-title"><i class="fa fa-envelope"></i></span>
                        </div>
                        <div class="single-input-wrap style-two">
                            <input type="text" placeholder="Password">
                            <span class="single-input-title"><i class="fa fa-lock"></i></span>
                        </div>
                        <label class="checkbox">
                            <input type="checkbox">
                            <span>Remember me</span>
                        </label>
                        <div class="single-input-wrap style-two">
                            <button class="btn btn-yellow w-100">Sign Up</button>
                        </div>
                        <div class="sign-in-btn">I already have an account. <a href="#">Sign In</a></div> 
                        <div class="social-wrap">
                            <p>Or Continue With</p>
                            <ul class="social-icon">
                                <li>
                                    <a class="facebook" href="#" target="_blank"><i class="fa fa-facebook  "></i></a>
                                </li>
                                <li>
                                    <a class="twitter" href="#" target="_blank"><i class="fa fa-twitter  "></i></a>
                                </li>
                                <li>
                                    <a class="pinterest" href="#" target="_blank"><i class="fa fa-pinterest"></i></a>
                                </li>
                            </ul>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- //. sign up Popup End -->

    <!-- navbar area start -->
    <nav class="navbar navbar-area navbar-expand-lg nav-style-01">
        <div class="container nav-container">
            <div class="responsive-mobile-menu">
                <div class="mobile-logo">
                    <a href="index.html">
                        <img src="assets/img/sticky-logo.png" alt="logo">
                    </a>
                </div>
                <button class="navbar-toggler float-right" type="button" data-toggle="collapse" data-target="#tp_main_menu" 
                    aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggle-icon">
                        <span class="line"></span>
                        <span class="line"></span>
                        <span class="line"></span>
                    </span>
                </button>
                <div class="nav-right-content">
                    <ul class="pl-0">
                        <li class="top-bar-btn-booking">
                            <a class="btn btn-yellow" href="tour-details.html">Book Now <i class="fa fa-paper-plane"></i></a>
                        </li>
                        <li class="tp-lang">
                            <div class="tp-lang-wrap">
                                <select class="select single-select">
                                  <option value="1">ENG</option>
                                  <option value="2">BAN</option>
                                  <option value="3">Chinese</option>
                                  <option value="3">Spanish</option>
                                </select>
                            </div>
                        </li>
                        <li class="search">
                            <i class="ti-search"></i>
                        </li>
                        <li class="notification">
                            <a class="signUp-btn" href="#">
                                <i class="fa fa-user-o"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="collapse navbar-collapse" id="tp_main_menu">
                <div class="logo-wrapper desktop-logo">
                    <a href="{{route('index-demo')}}" class="main-logo">
                        <img src="{{url('/images/jj-logo-white.png')}}" alt="logo">
                    </a>
                    <a href="{{route('index-demo')}}" class="sticky-logo">
                        <img src="{{url('/images/jj-logo.png')}}" alt="logo">
                    </a>
                </div>
                <!-- <ul class="navbar-nav">
                    <li>
                        <a href="{{ route('packages-demo') }}">International Tours</a>
                    </li>
                    <li>
                        <a href="{{ route('packages-demo') }}">Domestic Tours</a>
                    </li>
                    <li>
                        <a href="{{ route('about-us-demo') }}">About Us</a>
                    </li>
                   
                    <li>
                        <a href="#">Ask for call</a>
                    </li>
                </ul> -->
            </div>
            <div class="nav-right-content">
                <ul>
                    <li>
                        <a href="{{ route('packages-demo') }}">International Tours</a>
                    </li>
                    <li>
                        <a href="{{ route('packages-demo') }}">Domestic Tours</a>
                    </li>
                    <li>
                        <a href="{{ route('about-us-demo') }}">About Us</a>
                    </li>
                    <li>
                        <a href="#">Ask for call</a>
                    </li>
                    <li>
                        <a class="btn btn-yellow" href="{{route('membership-demo')}}">Become Member</a>
                    </li>
                    <li class="notification">
                        <a class="signUp-btn" href="#">
                            <i class="fa fa-user-o"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- navbar area end -->
    <!-- breadcrumb area start -->
    <div class="breadcrumb-area style-two jarallax" style="background-image:url(assets/img/bg/1.png);">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb-inner">
                        <h1 class="page-title">Dazzling Dubai</h1>
                        <ul class="page-list">
                            <li><a href="#">Home</a></li>
                            <li>Tour Details</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- breadcrumb area End -->
    
    <!-- tour details area End -->
    <div class="tour-details-area mg-top--70">
         <div class="tour-details-gallery">
            <div class="container-bg bg-dark-blue">
                <div class="container">
                    <div class="gallery-filter-area row">
                        <div class="gallery-sizer col-1"></div>
                        <!-- gallery-item -->
                        <div class="tp-gallery-item col-md-5 col-sm-6 mb-10">
                            <div class="tp-gallery-item-img">
                                <div class="thumbnails">
                                    <img src="assets/img/tour-details/1.png" alt="image">
                                    <div class="video-popup-btn">
                                        <a href="https://www.youtube.com/watch?v=c7XEhXZ_rsk" class="video-play-btn mfp-iframe" tabindex="0"><i class="fa fa-play"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- gallery-item -->
                        <div class="tp-gallery-item col-md-3 col-sm-6">
                            <div class="tp-gallery-item-img">
                                <a href="#" data-effect="mfp-zoom-in">
                                    <img src="assets/img/tour-details/2.png" alt="image">
                                </a>
                            </div>
                        </div>
                        <!-- gallery-item -->
                        <div class="tp-gallery-item col-lg-2 col-md-4 col-sm-6">
                            <div class="tp-gallery-item-img">
                                <a href="#" data-effect="mfp-zoom-in">
                                    <img src="assets/img/tour-details/3.png" alt="image">
                                </a>
                            </div>
                        </div>
                        <!-- gallery-item -->
                        <div class="tp-gallery-item col-lg-2 col-md-4 col-sm-6">
                            <div class="tp-gallery-item-img">
                                <a href="#" data-effect="mfp-zoom-in">
                                    <img src="assets/img/tour-details/4.png" alt="image">
                                </a>
                            </div>
                        </div>
                        <!-- gallery-item -->
                        <div class="tp-gallery-item col-lg-2 col-md-4 col-sm-6">
                            <div class="tp-gallery-item-img">
                                <a href="#" data-effect="mfp-zoom-in">
                                    <img src="assets/img/tour-details/5.png" alt="image">
                                </a>
                            </div>
                        </div>
                        <!-- gallery-item -->
                        <div class="tp-gallery-item col-lg-2 col-md-4 col-sm-6">
                            <div class="tp-gallery-item-img">
                                <a href="#" data-effect="mfp-zoom-in">
                                    <img src="assets/img/tour-details/6.png" alt="image">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="tour-details-wrap">
                        <!-- <h4 class="single-page-small-title">Write A Review</h4>
                        <p>From its distinct half-hour time zone to its occasional June snowshower, Newfoundland runs on its own time. By August, the summer crowds have dwindled, berries hang ripe and heavy on their stems, and the landscape is ablaze with wildflowers. Join us at the peak of Newfoundland’s late summer season as we wind our way through the famously Celtic stretch of coastline known as the Irish Loop, exploring its unique history, folklore, cuisine, and breathtaking seaside scenery. We’ll enjoy dinners made from freshly foraged ingredients on a quiet dock, chat with a boat-builder in the midst of making a vessel, and learn how to craft heritage cheese from local experts while surrounded by an adorable, bleating tribe of tiny baby goats. As we make our way along the Loop, we’ll encounter countless characters, places, and stories that give this corner of the island its charm, tenacity, and unique flair.</p>
                        <p> This trip is offered by Atlas Obscura. Once you've reserved your spot, our team will be in touch to help you prepare for the trip. Please note that flights to and from St. John's are not included in the trip cost. This trip is limited by 12 travelers.</p> -->
                        <div class="package-included-area">
                            <h4 class="single-page-small-title">Included</h4>
                            <div class="row">
                                <div class="col-xl-4 col-sm-6">
                                    <div class="single-package-included">
                                        <img src="assets/img/icons/15.png" alt="icons">
                                        <h6>Food</h6>
                                        <p>3 breakfasts, 3 dinners</p>
                                    </div>
                                </div>
                                <div class="col-xl-4 col-sm-6">
                                    <div class="single-package-included">
                                        <img src="assets/img/icons/16.png" alt="icons">
                                        <h6>Accommodations</h6>
                                        <p>3 nights in a hotel</p>
                                    </div>
                                </div>
                                <div class="col-xl-4 col-sm-6">
                                    <div class="single-package-included">
                                        <img src="assets/img/icons/17.png" alt="icons">
                                        <h6>Transportation</h6>
                                        <p>2 boat rides, 1 car ride</p>
                                    </div>
                                </div>
                                <div class="col-xl-4 col-sm-6">
                                    <div class="single-package-included">
                                        <img src="assets/img/icons/18.png" alt="icons">
                                        <h6>Drinks</h6>
                                        <p>Water, tea, coffee, beer</p>
                                    </div>
                                </div>
                                <div class="col-xl-4 col-sm-6">
                                    <div class="single-package-included">
                                        <img src="assets/img/icons/19.png" alt="icons">
                                        <h6>Tickets</h6>
                                        <p>Entrance fee</p>
                                    </div>
                                </div>
                                <div class="col-xl-4 col-sm-6">
                                    <div class="single-package-included">
                                        <img src="assets/img/icons/20.png" alt="icons">
                                        <h6>Equipment</h6>
                                        <p>Outdoor gear, safety</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="package-included-location">
                            <h4 class="single-page-small-title">Your Itinerary</h4>
                            <div class="row">
                                <div class="col-lg-3 col-md-3">
                                    <div class="single-blog single-blog-after-none">
                                        <div class="p-list">
                                            <div class="list">1</div>
                                            <p>Day 1</p>
                                        </div>
                                        <div class="thumb">
                                            <img src="assets/img/blog/8.png" alt="blog">
                                        </div>
                                        <div class="single-blog-details">
                                            <h4 class="title">Welcome to St. John's</h4>
                                            <p class="content">After a welcome drink, we'll stroll into town and get to know each other over a hyper-local “nose-to-tail” dinner. Show more</p>
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3">
                                    <div class="single-blog single-blog-after-none">
                                        <div class="p-list">
                                            <div class="list">2</div>
                                            <p>Day 2</p>
                                        </div>
                                        <div class="thumb">
                                            <img src="assets/img/blog/1.png" alt="blog">
                                        </div>
                                        <div class="single-blog-details">
                                            <h4 class="title">Relaxation & Exploration</h4>
                                            <p class="content">After a welcome drink, we'll stroll into town and get to know each other over a hyper-local “nose-to-tail” dinner. Show more</p>
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3">
                                    <div class="single-blog single-blog-after-none">
                                        <div class="p-list">
                                            <div class="list">3</div>
                                            <p>Day 3</p>
                                        </div>
                                        <div class="thumb">
                                            <img src="assets/img/blog/9.png" alt="blog">
                                        </div>
                                        <div class="single-blog-details">
                                            <h4 class="title">Farewell & Departure</h4>
                                            <p class="content">After a welcome drink, we'll stroll into town and get to know each other over a hyper-local “nose-to-tail” dinner. Show more</p>
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3">
                                    <div class="single-blog single-blog-after-none">
                                        <div class="p-list">
                                            <div class="list">4</div>
                                            <p>Day 4</p>
                                        </div>
                                        <div class="thumb">
                                            <img src="assets/img/blog/9.png" alt="blog">
                                        </div>
                                        <div class="single-blog-details">
                                            <h4 class="title">Farewell & Departure</h4>
                                            <p class="content">After a welcome drink, we'll stroll into town and get to know each other over a hyper-local “nose-to-tail” dinner. Show more</p>
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3">
                                    <div class="single-blog single-blog-after-none">
                                        <div class="p-list">
                                            <div class="list">5</div>
                                            <p>Day 5</p>
                                        </div>
                                        <div class="thumb">
                                            <img src="assets/img/blog/9.png" alt="blog">
                                        </div>
                                        <div class="single-blog-details">
                                            <h4 class="title">Farewell & Departure</h4>
                                            <p class="content">After a welcome drink, we'll stroll into town and get to know each other over a hyper-local “nose-to-tail” dinner. Show more</p>
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3">
                                    <div class="single-blog single-blog-after-none">
                                        <div class="p-list">
                                            <div class="list">6</div>
                                            <p>Day 6</p>
                                        </div>
                                        <div class="thumb">
                                            <img src="assets/img/blog/9.png" alt="blog">
                                        </div>
                                        <div class="single-blog-details">
                                            <h4 class="title">Farewell & Departure</h4>
                                            <p class="content">After a welcome drink, we'll stroll into town and get to know each other over a hyper-local “nose-to-tail” dinner. Show more</p>
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3">
                                    <div class="single-blog single-blog-after-none">
                                        <div class="p-list">
                                            <div class="list">7</div>
                                            <p>Day 7</p>
                                        </div>
                                        <div class="thumb">
                                            <img src="assets/img/blog/9.png" alt="blog">
                                        </div>
                                        <div class="single-blog-details">
                                            <h4 class="title">Farewell & Departure</h4>
                                            <p class="content">After a welcome drink, we'll stroll into town and get to know each other over a hyper-local “nose-to-tail” dinner. Show more</p>
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3">
                                    <div class="single-blog single-blog-after-none">
                                        <div class="p-list">
                                            <div class="list">8</div>
                                            <p>Day 8</p>
                                        </div>
                                        <div class="thumb">
                                            <img src="assets/img/blog/9.png" alt="blog">
                                        </div>
                                        <div class="single-blog-details">
                                            <h4 class="title">Farewell & Departure</h4>
                                            <p class="content">After a welcome drink, we'll stroll into town and get to know each other over a hyper-local “nose-to-tail” dinner. Show more</p>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- tour details area End -->

    <!-- footer area start -->
    <footer class="footer-area" style="background-image: url(assets/img/bg/2.png);">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <div class="footer-widget widget">
                        <div class="about_us_widget">
                            <a href="index.html" class="footer-logo"> 
                                <img src="assets/img/logo.png" alt="footer logo">
                            </a>
                            <p>We believe brand interaction is key in commu- nication. Real innovations and a positive customer experience are the heart of successful communication.</p>
                            <ul class="social-icon">
                                <li>
                                    <a class="facebook" href="#" target="_blank"><i class="fa fa-facebook  "></i></a>
                                </li>
                                <li>
                                    <a class="twitter" href="#" target="_blank"><i class="fa fa-twitter  "></i></a>
                                </li>
                                <li>
                                    <a class="pinterest" href="#" target="_blank"><i class="fa fa-instagram"></i></a>
                                </li>
                            </ul>
                       </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="footer-widget widget ">
                        <div class="widget-contact">
                            <h4 class="widget-title">Contact us</h4>
                            <p>
                                <i class="fa fa-map-marker"></i> 
                                <span>Manama Tower, Badda Link Road, Badda Dhaka, Bangladesh</span>
                            </p>
                            <p class="location"> 
                                <i class="fa fa-envelope-o"></i>
                                <span>travelpoint@gmail.com</span>
                            </p>
                            <p class="telephone">
                                <i class="fa fa-phone base-color"></i> 
                                <span>
                                    +088 012121240
                                </span>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-6">
                    <div class="footer-widget widget">
                        <h4 class="widget-title">Quick Link</h4>
                        <ul class="widget_nav_menu">
                            <li><a href="{{ route('about-us-demo') }}">About Us</a></li>
                            <li><a href="#">Career</a></li>
                            <li><a href="{{ route('blog-demo') }}">Blog</a></li>
                            <li><a href="#">Contact Us</a></li>
                            <li><a href="#">T&C</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="footer-widget widget">
                        <h4 class="widget-title">Instagram Gallery</h4>
                        <ul class="widget-instagram-feed">
                            <li><a href="#"><img src="assets/img/instagram/1.png" alt="img"></a></li>
                            <li><a href="#"><img src="assets/img/instagram/2.png" alt="img"></a></li>
                            <li><a href="#"><img src="assets/img/instagram/3.png" alt="img"></a></li>
                            <li><a href="#"><img src="assets/img/instagram/4.png" alt="img"></a></li>
                            <li><a href="#"><img src="assets/img/instagram/5.png" alt="img"></a></li>
                            <li><a href="#"><img src="assets/img/instagram/6.png" alt="img"></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="copyright-inner">
            <div class="copyright-text">
                &copy; Viaje 2019 All rights reserved. Powered with <a href="http://zwin.io/" target="_blank"><i class="fa fa-heart"></i> </a> by <a href="http://zwin.io/" target="_blank"><span>Zwin.</span></a>
            </div>
        </div>
    </footer>
    <!-- footer area end -->

    <!-- back to top area start -->
    <div class="back-to-top">
        <span class="back-top"><i class="fa fa-angle-up"></i></span>
    </div>
    <!-- back to top area end -->

    <!-- Additional plugin js -->
    <script src="assets/js/jquery-2.2.4.min.js"></script>
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/jquery.magnific-popup.js"></script>
    <script src="assets/js/owl.carousel.min.js"></script>
    <script src="assets/js/wow.min.js"></script>
    <script src="assets/js/slick.js"></script>
    <script src="assets/js/waypoints.min.js"></script>
    <script src="assets/js/jquery.counterup.min.js"></script>
    <script src="assets/js/imagesloaded.pkgd.min.js"></script>
    <script src="assets/js/isotope.pkgd.min.js"></script>
    <script src="assets/js/swiper.min.js"></script>
    <script src="assets/js/jquery.nice-select.min.js"></script>
    <script src="assets/js/jquery-ui.min.js"></script>
    <script src="assets/js/jarallax.min.js"></script>

    <!-- main js -->
    <script src="assets/js/main.js"></script>

</body>
</html>