@extends('front.layouts.master')
@section('title','Tours-Plan')
@push('style')
<style>
.breadcrumb-area.style-two {
    padding: 130px 0 73px !important;
}
    .gallery-box {
            margin-bottom: 30px !important;
            height: 170px;
            width: 170px;
            margin: 10px;
        }

        .gallery-box img {
            border-radius: 10px;
            height: 100%;
            width: 100%;
        }



        /*This CSS Compulsary Add And Use*/
        .banner-slider-item .margin-top {
            margin-top: 80px;
        }

        @media only screen and (max-width: 1100px) {
            .banner-slider .banner-slider-item {
                height: 700px !important;
                padding: 170px 0 240px 0 !important;
                overflow: hidden !important;
            }
        }

        .custom_tab .tab-content {
            box-shadow: 0px 3px 13px #23397421 !important;
        }

        .tp-main-search {
            box-shadow: none;
        }

        .tp-holiday-plan-area.mg-top-96 {
            margin-top: 0 !important;
            padding-top: 72px !important;
        }

        .pd-top-110 {
            padding: 50px 0px !important;
        }

        .copyright-inner .privacypolicy {
            text-align: center;
            font-size: 14px;
            letter-spacing: 0.28px;
            color: #ffffff;
            line-height: 44px;
        }

        .banner-tour-package {
            visibility: hidden !important;
        }

        small.help-block {
            color: red;
        }

        .fixed {
            z-index: 9999;
        }

    .section-title {
        margin-bottom: 18px;
    }

    .popup-btn a {
        top: 200px;
        position: fixed;
        right: -57px;
        z-index: 1000;
        transform: rotate(90deg);
        background-color: #f3941e !important;
        padding: 10px 20px 35px;
        height: 0px;
        background-color: #000;
        color: #fff;
        border-bottom-left-radius: 15px;
        border-bottom-right-radius: 15px;
    }

    .popup-btn a:hover {
        text-decoration: none;
        color: #fff;
    }

    .heading-tab p {
        /*position: absolute;*/
        /*right: 60%;*/
        padding-left: 50px;
        font-size: 29px;
        font-weight: bold;
        padding-top: 20px;
        color: #071c55;
}
    }

    .collapsible .single-intro {
        height: 50px;
    }

    .note-details h5 {
        padding-top: 30px;
        font-weight: bold;
        color: #f3941e;
    }

    .icon-tab {
        height: 50px;
        width: 50px;
    }

    .icon-tab img {
        height: 100%;
        width: 100%;
    }

    /*Start Accordion Css*/

    .collapsible {
        background-color: white;
        cursor: pointer;
        padding: 18px;
        width: 100%;
        border: none;
        text-align: left;
        outline: none;
        font-size: 15px;
        border: 1px solid lightgray;
    }

    .active,
    .collapsible:hover {
        background-color: #f9cf9b;
    }

    .collapsible:after {
        content: '\002B';
        font-weight: bold;
        float: right;
        margin-left: 5px;
    }

    .active:after {
        content: "\2212";
    }

    .contentbox {
        padding: 0 18px;
        max-height: 0;
        overflow: hidden;
        transition: max-height 0.2s ease-out;
        background-color: #f1f1f1;
    }

    /*End Accordion Css*/
    .tour-details-area .package-included-location {
        margin-bottom: 15px;
    }

    .tour-details-area .tour-details-gallery {
        margin-bottom: 27px;
    }

    .single-page-small-title {
        margin-bottom: -3px;
    }

    .tour-details-area .package-included-area {
        margin-top: 0;
    }

    .tp-price-meta h4 {
        color: orange;
    }
    .p-align{
        text-align: justify;
    }
    .align-caption{
        line-height: 25px !important;
        margin-top: 15px !important;
    }

    .container-bg {
          padding: 10px 0 10px 0 !important;
        }


    @media only screen and (max-width: 1680px) {
        .container-bg {
            padding: 7px 0 0 0;
            margin: 0 30px;
        }
        .contentbox{
            max-height: 0px;
        }

    }

    @media screen and (max-width: 1024px) {
        .single-destinations-list.style-three .thumb {
            flex: 100%;
        }

        .single-destinations-list.style-three .details {
            flex: 100%;
        }

        .tour-details-gallery {
            background: #071C55;
            padding: 16px 0px;
        }
        .contentbox{
            max-height: 0px;
        }

    }

    @media screen and (max-width: 991px) {
        .heading-tab p {
            right: 29%;
        }
        .contentbox{
            max-height: 0px;
        }

    }

    @media only screen and (max-width: 767px) {
        .tour-details-gallery {
            background: #071C55;
            padding: 16px 0px;
        }
        .contentbox{
            max-height: 0px;
        }
    }

    @media only screen and (max-width: 575px) {
        .tour-details-gallery {
            background: #071C55;
            padding: 16px 0px;
        }
        .contentbox{
            max-height: 0px;
        }

    }
</style>
@endpush

@section('content')
 <!-- breadcrumb area start -->
 <div class="breadcrumb-area style-two jarallax" style="background-image:url(/assets/img/bg/8.png);">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-inner">
                    @if(isset($toure))
                    <h1 class="page-title">{{strtoupper($toure->name)}}</h1>
                    <ul class="page-list">
                        @if(isset($toure->pack))
                        <li><a href="{{url('/packages')}}/{{$toure->pack->slug}}">{{$toure->pack->name}}</a></li>
                        @endif
                        <li>{{strtoupper($toure->name)}}</li>
                    </ul>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb area End -->

<!-- tour details area End -->
<div class="tour-details-area mg-top--70">
    <div class="tour-details-gallery">
        <div class="container-bg bg-dark-blue">
            <div class="container">
                <div class=" row mt-4 justify-content-center">
                    <!-- <div class="gallery-sizer col-1"></div> -->
                   
                    @if(isset($gallery))
                        @foreach ($gallery as $item)
                            <div class="gallery-box">
                                <a href="#" data-effect="mfp-zoom-in">
                                    <img src="{{url('')}}/{{$item->imagepath}}" alt="image">
                                </a>
                            </div>
                        @endforeach
                    @endif
                </div>

            </div>
        </div>
    </div>
    <div class="container">
        
        <div class="row">
            <div class="col-lg-12">
                <div class="tour-details-wrap">
                    <div class="package-included-area">
                        @if(isset($overview))
                        <div class="package-included-location">
                            <div class="section-title text-lg-center text-left">
                                <h2 class="title"><span>Overview</span></h2>
                            </div>
                        </div>
                        <div class="row">
                        {{-- @foreach($overview as $key =>  $over)
                            <div class="col-xl-4 col-sm-6">
                                <div class="single-package-included">
                                    <!-- <img src="assets/img/icons/15.png" alt="icons"> -->
                                    <h6>{{$key}}</h6>
                                    <p>{{$over}}</p>
                                </div>
                            </div>
                            @endforeach --}}
                           
                           @if($overview['Region'] != null)
                            <div class="col-xl-4 col-sm-6">
                                <div class="single-package-included">
                                    <!-- <img src="assets/img/icons/16.png" alt="icons"> -->
                                    <h6>Region</h6>
                                    <p>{{$overview['Region']}}</p>
                                </div>
                            </div>
                            @endif
                           @if($overview['Duration'] != null)
                            <div class="col-xl-4 col-sm-6">
                                <div class="single-package-included">
                                    <!-- <img src="assets/img/icons/16.png" alt="icons"> -->
                                    <h6>Duration</h6>
                                    <p>{{$overview['Duration']}}</p>
                                </div>
                            </div>
                            @endif
                            @if($overview['StartPoint'] != null)
                            <div class="col-xl-4 col-sm-6">
                                <div class="single-package-included">
                                    <!-- <img src="assets/img/icons/17.png" alt="icons"> -->
                                    <h6>Start Point</h6>
                                    <p>{{$overview['StartPoint']}}</p>
                                </div>
                            </div>
                            @endif
                            @if($overview['EndPoint'] != null)
                            <div class="col-xl-4 col-sm-6">
                                <div class="single-package-included">
                                    <!-- <img src="assets/img/icons/18.png" alt="icons"> -->
                                    <h6>End Point</h6>
                                    <p>{{$overview['EndPoint']}}</p>
                                </div>
                            </div>
                            @endif
                            @if($overview['HighestAltitute'] != null)
                            <div class="col-xl-4 col-sm-6">
                                <div class="single-package-included">
                                    <!-- <img src="assets/img/icons/19.png" alt="icons"> -->
                                    <h6>Highest Altitude</h6>
                                    <p>{{$overview['HighestAltitute']}}</p>
                                </div>
                            </div>
                            @endif
                            @if($overview['ApproxKm'] != null)
                            <div class="col-xl-4 col-sm-6">
                                <div class="single-package-included">
                                    <!-- <img src="assets/img/icons/20.png" alt="icons"> -->
                                    <h6>ApproxKm</h6>
                                    <p>{{$overview['ApproxKm']}}</p>
                                </div>
                            </div>
                            @endif
                            @if($overview['Grade'] != null)
                            <div class="col-xl-4 col-sm-6">
                                <div class="single-package-included">
                                    <!-- <img src="assets/img/icons/20.png" alt="icons"> -->
                                    <h6>Grade</h6>
                                    <p>{{$overview['Grade']}}</p>
                                </div>
                            </div>
                            @endif
                            <!-- <div class="col-12">
                                @if(isset($detail1))
                                 @foreach ($detail1 as $key=> $item)
                                 <p class="text-justify"> {{$item}}</p>
                                 @endforeach
                                 @endif
                            </div> -->
                        </div>
                        @endif
                    </div>
                    @if(isset($detail1))
                        @foreach ($detail1 as $key=> $item)
                        <h4 class="single-page-small-title">{{$key}}</h4>
                        <p class="text-justify"> {{$item}}</p>
                        @endforeach
                    @endif
                    @if(isset($detail2))
                        @foreach ($detail2 as $key=> $item)
                        <h4 class="single-page-small-title">{{$key}}</h4>
                        <p class="text-justify"> {{$item}}</p>
                        @endforeach
                    @endif

                    @if(isset($detail3))
                    @foreach ($detail3 as $key=> $item)
                    <h4 class="single-page-small-title">{{$key}}</h4>
                    <p class="text-justify"> {{$item}}</p>
                    @endforeach
                    @endif

                    @if(isset($detail4))
                    @foreach ($detail4 as $key=> $item)
                    <h4 class="single-page-small-title">{{$key}}</h4>
                    <p class="text-justify"> {{$item}}</p>
                    @endforeach
                    @endif

                   
                </div>
            </div>
        </div>
    </div>
</div>
<!-- tour details area End -->
@if(isset($toure->itinerary))
<div class="container">
    <div class="row">
        <div class="col-12">
            <div class="package-included-location">
                <div class="section-title text-lg-center text-left">
                    <h2 class="title"><span>Your&nbsp;</span>Itinerary</h2>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            
                @foreach ($toure->itinerary as $index => $item)
                
                <button class="collapsible @if($index == 0) active @endif ">
                    <div class="single-intro d-inline-flex m-0">
                        <div class="icon-tab mr-lg-5 mr-3">
                            <img src="{{url('/itenary')}}/{{$item->images}}" class="rounded">
                        </div>
                        <h4 class="intro-title">
                            <span class="intro-count">{{$item->day_number}}</span>
                            <a class="intro-cat" href="#">Day</a>
                        </h4>
                        <div class="heading-tab d-md-block d-none">
                            <p>{{$item->details}}</p>
                        </div>
                    </div>
                </button>
                <div class="contentbox" @if($index == 0) style="max-height: 362px;" @endif >
    
                    <div class="single-destinations-list style-three row mt-3 border-0">
                        <div class="thumb">
                            <img src="{{url('/itenary')}}/{{$item->images}}" alt="list">
                        </div>
                        <div class="details">
    
                            <p class="location"><i class="fa fa-clock-o"></i>&nbsp;Day {{$item->day_number}}</p>
                            <!-- <p class="content text-justify">{{$item->details}}</p> -->
                            <h4 class="title">{{$item->details}}</h4>
                            <p class="content text-justify align-caption"><a href="#">{{$item->caption}}</a></p>
                            
                            
                            {{-- <div class="list-price-meta">
                                <div class="tp-price-meta d-inline-block">
                                    <h4>Accommodation</h4>
                                    <p>Hotel</p>
    
                                </div>
                                <div class="tp-price-meta d-inline-block">
                                    <h4>Meals</h4>
                                    <p>Evening Snacks, Dinner</p>
                                </div>
                            </div> --}}
    
    
                        </div>
                    </div>
                </div>
                @endforeach
                @if(isset($toure->itinerary))
                <div class="note-details my-5">
                    <h5 class="font-weight-bold d-inline-block">Note*:</h5><span>Above
                        Itinerary
                        is just
                        indicative. Do not consider it as a Final Schedule.</span>
                </div>
            @endif

        </div>
    </div>
</div>
@endif
<!--Start Jag Joyu Details-->
<div class="intro-area mt-5">
    <div class="container">

        <div class="row">
            @if(isset($toure->inclusion))
            <div class="col-lg-6 col-md-6">
                <div class="single-intro bullet-icon style-two">

                    <h4 class="intro-title">INCLUSION</h4>
                    <ul class="mt-4 p-align">
                        {!!$toure->inclusion!!}
                        
                    </ul>
                </div>
            </div>
            @endif

            @if(isset($toure->optional_charges))
            
            <div class="col-lg-6 col-md-6">
                <div class="single-intro bullet-icon style-two">

                    <h4 class="intro-title">OPTIONAL CHARGES</h4>
                    <ul class="p-align"> 
                        {!!$toure->optional_charges!!}
                    </ul>
                </div>
            </div>
            @endif
            @if(isset($toure->exclusion))
            
            <div class="col-lg-6 col-md-6">
                <div class="single-intro bullet-icon style-two">

                    <h4 class="intro-title">EXCLUSION</h4>
                    <ul class="p-align">
                        {!!$toure->exclusion!!}

                    </ul>
                </div>
            </div>
            @endif

            @if(isset($toure->essentials))
            <div class="col-lg-6 col-md-6">
                <div class="single-intro bullet-icon style-two">

                    <h4 class="intro-title">BASIC ESSENTIALS</h4>
                    <ul class="p-align">
                        {!!$toure->essentials!!}
                    </ul>
                </div>
            </div>
            @endif
            @if(isset($toure->clothing_essential))
            <div class="col-lg-6 col-md-6">
                <div class="single-intro bullet-icon style-two">

                    <h4 class="intro-title">CLOTHING ESSENTIALS</h4>
                    <ul class="p-align">
                        {!!$toure->clothing_essential!!}
                    </ul>
                </div>
            </div>
            @endif

        </div>
    </div>

</div>

<!--End Jag Joyu Details-->

<!-- back to top area start -->
<div class="back-to-top">
    <span class="back-top"><i class="fa fa-angle-up"></i></span>
</div>
<!-- back to top area end -->



<div class="popup-btn" onclick="document.getElementById('id01').style.display='block'">
    <a href="https://jagjoyu.com/login" target="_blank" class="border-bottom-left-radius">Get Quatation</a>
</div>

@endsection

@push('script')
<script>
    var coll = document.getElementsByClassName("collapsible");
    var i;

    for (i = 0; i < coll.length; i++) {
        coll[i].addEventListener("click", function () {
            this.classList.toggle("active");
            var content = this.nextElementSibling;
            if (content.style.maxHeight) {
                content.style.maxHeight = null;
            } else {
                content.style.maxHeight = content.scrollHeight + "px";
            }
        });
    }
</script>
@endpush