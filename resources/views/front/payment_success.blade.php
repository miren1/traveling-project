<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Payment Successfull</title>
    <!-- favicon -->
    <link rel=icon href="{{url('/front')}}/assets/img/favicon.png" sizes="20x20" type="image/png">

    <!-- Additional plugin css -->
    <link rel="stylesheet" href="{{url('/front')}}/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{url('/front')}}/assets/css/animate.css">
    <link rel="stylesheet" href="{{url('/front')}}/assets/css/magnific-popup.css">
    <link rel="stylesheet" href="{{url('/front')}}/assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="{{url('/front')}}/assets/css/slick.css">
    <link rel="stylesheet" href="{{url('/front')}}/assets/css/swiper.min.css">
    <link rel="stylesheet" href="{{url('/front')}}/assets/css/nice-select.css">
    <!-- icons -->
    <link rel="stylesheet" href="{{url('/front')}}/assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{url('/front')}}/assets/css/themify-icons.css">
    <link rel="stylesheet" href="{{url('/front')}}/assets/css/line-awesome.min.css">
    <!-- main css -->
    <link rel="stylesheet" href="{{url('/front')}}/assets/css/style.css">
    <!-- responsive css -->
    <link rel="stylesheet" href="{{url('/front')}}/assets/css/responsive.css">
    <style>
        .navbar {
            padding: 3.5rem 1rem;
        }

        #ui-datepicker-div {
            display: none;
        }

        .mg-bottom-92 {
            margin-bottom: -30px;
        }

        .copyright-inner {
            margin-top: 30px;
        }

        @media only screen and (max-width: 991px) {
            .navbar {
                padding: 12px 1rem;
            }

            .copyright-inner {
                margin-top: 35px;
            }
        }
      


        .Payment-heading a {
            text-decoration: underline;
        }

        .Payment-heading h3 {
            color: #5d5d5d;
            font-family: var(--body-font);
        }

        .Payment-heading h6 {
            color: #b3b3b3;
            font-weight: 500;
            font-family: var(--body-font);
        }
        .new-footer{
            padding-top: 0!important;
            position: fixed !important;
            float: left !important;
            height: 58px !important;
            bottom: 30px !important;
            display: block !important;
            width: 100%;
        }
    </style>
</head>

<body>
    <!-- preloader area start -->
    <div class="preloader" id="preloader">
        <div class="preloader-inner">
            <div class="spinner">
                <div class="dot1"></div>
                <div class="dot2"></div>
            </div>
        </div>
    </div>
    <!-- preloader area end -->

    <!-- navbar area start -->
    <nav class="navbar navbar-expand-lg nav-style-02">
        <div class="container nav-container">
            <div class="responsive-mobile-menu">
                <div class="mobile-logo">
                    <a href="https://jagjoyu.xsquarestaging.in">
                        <img src="https://jagjoyu.xsquarestaging.in/public/images/jj-logo.png" alt="logo">
                    </a>
                </div>
            </div>

            <div class="collapse navbar-collapse" id="tp_main_menu">
                <div class="logo-wrapper desktop-logo">
                    <a href="https://jagjoyu.xsquarestaging.in" class="main-logo">
                        <img src="https://jagjoyu.xsquarestaging.in/public/images/jj-logo.png" alt="logo">
                    </a>

                </div>

            </div>



        </div>
    </nav>
    <!-- navbar area end -->

    <!--Start Payment Detials Area-->
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="header-img text-center">
                    <img src="{{url('/front')}}/assets/img/payment_png2.png" class="img-fluid" alt="">
                </div>
                <div class="Payment-heading text-center">
                    <h3 class="font-weight-bold">Your Payment is Successfull</h3>
                    <h6>Thank you for being with us. Payment receipt will be sent to your registered email.</h6>
                    <a href="https://jagjoyu.xsquarestaging.in/login" class="text-danger mx-2">Back to Login</a>
                    <a href="https://jagjoyu.xsquarestaging.in/" class="text-danger mx-2">Back to Home</a>
                </div>

            </div>
        </div>
    </div>
    <!--End Payment Details Area-->

    <!--Start Footer Area-->
    <footer class="pt-0 new-footer">
        <div class="copyright-inner">
            <div class="copyright-text col-lg-8 mx-auto">
                © Jag Joyu 2021 All rights reserved. Powered with <a href="http://zwin.io/" target="_blank"><i
                        class="fa fa-heart"></i> </a> by <a href="http://zwin.io/"
                    target="_blank"><span>X-Square</span></a>
            </div>
        </div>
    </footer>
    <!--End Footer Area-->

    <!-- back to top area start -->
    <div class="back-to-top">
        <span class="back-top"><i class="fa fa-angle-up"></i></span>
    </div>
    <!-- back to top area end -->

    <!-- Additional plugin js -->
    <script src="{{url('/front')}}/assets/js/jquery-2.2.4.min.js"></script>
    <script src="{{url('/front')}}/assets/js/popper.min.js"></script>
    <script src="{{url('/front')}}/assets/js/bootstrap.min.js"></script>
    <script src="{{url('/front')}}/assets/js/jquery.magnific-popup.js"></script>
    <script src="{{url('/front')}}/assets/js/owl.carousel.min.js"></script>
    <script src="{{url('/front')}}/assets/js/wow.min.js"></script>
    <script src="{{url('/front')}}/assets/js/slick.js"></script>
    <script src="{{url('/front')}}/assets/js/waypoints.min.js"></script>
    <script src="{{url('/front')}}/assets/js/jquery.counterup.min.js"></script>
    <script src="{{url('/front')}}/assets/js/imagesloaded.pkgd.min.js"></script>
    <script src="{{url('/front')}}/assets/js/isotope.pkgd.min.js"></script>
    <script src="{{url('/front')}}/assets/js/swiper.min.js"></script>
    <script src="{{url('/front')}}/assets/js/jquery.nice-select.min.js"></script>
    <script src="{{url('/front')}}/assets/js/jquery-ui.min.js"></script>
    <script src="{{url('/front')}}/assets/js/jarallax.min.js"></script>
    <!-- main js -->
    <script src="{{url('/front')}}/assets/js/main.js"></script>


</body>

</html>