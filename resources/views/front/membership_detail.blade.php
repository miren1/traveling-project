@extends('front.layouts.master')
@section('title', 'Membership-Detail')
@push('style')
    <link rel="stylesheet"
    href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css" />    

    <style type="text/css">
        select{
            border: 1px solid #ccc;
        }
        .table-responsive{
            margin-top: 20px !important;
        }
    </style>    
@endpush
@section('content')
    <!-- breadcrumb area start style-two-->
    <div class="breadcrumb-area  jarallax" style="background-image:url({{ url('//front/assets/img/bg/1.jpg') }});">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb-inner">
                        <h1 class="page-title">Welcome @if(Auth::guard('webagent')->user())
                                {{ Auth::guard('webagent')->user()->first_name}} {{Auth::guard('webagent')->user()->last_name }}
                            @elseif(Auth::guard('webcustomer')->user())
                                 {{ Auth::guard('webcustomer')->user()->first_name}} {{Auth::guard('webcustomer')->user()->last_name }}
                            @elseif(Auth::guard('webmember')->user())
                                 {{ Auth::guard('webmember')->user()->name}}
                            @endif</h1>
                        
                        @if(Auth::guard('webagent')->user())
                        <ul class="page-list">
                            <li>Balance : <span>{{ Auth::guard('webagent')->user()->wallet}}</li>
                            <button type="button" class="btn btn-yellow btn-sm" data-toggle="modal" data-target="#topupModel"
                            id="btnexampleModalLong">
                            Charge Wallet
                            </button>
                        </ul>
                        @elseif(Auth::guard('webmember')->user())
                        <div class="col-lg-12">
                            <p class="my-1 text-color mb-0 font-weight-light">Welcome {{ Auth::guard('webmember')->user()->first_name}} {{Auth::guard('webmember')->user()->last_name }}</p>
                        </div>
                        @elseif(Auth::guard('webcustomer')->user())
                        <div class="col-lg-12">
                            <p class="my-1 text-color mb-0 font-weight-light">Welcome {{ Auth::guard('webcustomer')->user()->first_name}} {{Auth::guard('webcustomer')->user()->last_name }}</p>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- breadcrumb area End -->
    @include('front.layouts.newmenu')
<!--Start Payment Detials Area-->
    <div class="container mb-5">
        <div class="row">
            <div class="table-responsive">
                <table id="example" class="display">
                    <thead>
                        <tr>
                            <th scope="col">Sr.No.</th>
                            <th scope="col" class="select-filter">Primary Member Name</th>
                            <th scope="col" class="select-filter">Contact No</th>
                            <th scope="col" class="select-filter">Membership Plan</th>
                            <th scope="col" class="select-filter">Term of TIP</th>
                            <th scope="col" >Year of Enrollment</th>
                            <th scope="col" class="select-filter">Status</th>
                            <th scope="col" class="select-filter">Commission</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <!-- <tfoot>
                        <tr>
                            <th scope="col">Sr.No.</th>
                            <th scope="col" class="select-filter">Primary Member Name</th>
                            <th scope="col" class="select-filter">Contact No</th>
                            <th scope="col" class="select-filter">Membership Plan</th>
                            <th scope="col" class="select-filter">Term of TIP</th>
                            <th scope="col" >Year of Enrollment</th>
                            <th scope="col" class="select-filter">Status</th>
                            <th scope="col" class="select-filter">Commission</th>
                        </tr>
                    </tfoot> -->
                </table>
            </div>
        </div>
    </div>
@endsection


@push('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>

<script type="text/javascript">
    $("#example").DataTable({
        // 'processing': true,
        // 'serverSide': true,
        'searching': true,
        "bSort": false,
         'pageLength': 5, 
        
            initComplete: function () {
            this.api().columns('.select-filter').every( function () {
                var column = this;
                var select = $('<br><select><option value=""></option></select>')
                    .appendTo( $(column.header()) )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
     
                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );
     
                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );
        },
        //ordering: false,
        order: [
            [5, 'desc']
        ],
        "ajax": {
            "url": "{{ route('getall.member') }}",
            "data": {
                "_token": "{{ csrf_token() }}"
            },
        },
        "columns": [
            {
                data: "id",
                render: function(data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1
                }

            },

            {
                data: "name",
               
            },

            {
                data: "primary_contact",
               
            },
            {
                data: 'plan.type',
                render: function(data, type, row, alldata) {
                  //console.log(row.plan.type);
                  return row.plan.type;
                  
                }

               
            },
            {
                data: "plan.years",

                render: function(data, type, row, alldata) {
                  //console.log(row.plan.type);
                  return row.plan.years;
                  
                }
               
            },

            {
                data: "created_at",
                render: function(data, type, row, alldata){
                   
                  return moment(data).format("DD-MM-YYYY");
                }
               
            },

            {
                data: "status",
                render: function(data, type, row, alldata){
                  //console.log(row.status); 
                  if(row.status == 'invite'){
                    return '<span class="badge rounded-pill bg-primary" style="color:#fff;">'+row.status+'</span>'
                  }else if(row.status == 'member'){
                    return '<span class="badge rounded-pill bg-warning text-dark">'+row.status+'</span>'
                  }else{
                    return '<span class="badge rounded-pill bg-success" style="color:#fff;">Active</span>'
                  }
                  //return moment(data).format("DD-MM-YYYY");
                }
               
            },

            {
                data: "id",
                render: function(data, type, row, alldata){
                 
                    return row.plan.commission_payment;
                }
               
            },

        ]
    });

</script>
@endpush
