@extends('front.layouts.master')
@section('title', 'View Ticket')
    @push('style')
         <link rel="stylesheet"
            href="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css" />
        <style type="text/css">
            .description {
                margin-top: 45px;
            }

            .description h3.title {
                margin-top: 10px !important;
            }

            .messaging {
                width: 100%;
            }

            /*#style-5::-webkit-scrollbar-track
                {
                    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
                    background-color: #F5F5F5;
                }

                #style-5::-webkit-scrollbar
                {
                    width: 10px;
                    background-color: #F5F5F5;
                }

                #style-5::-webkit-scrollbar-thumb
                {
                    background-color: #0ae;
                    
                    background-image: -webkit-gradient(linear, 0 0, 0 100%,
                                       color-stop(.5, rgba(255, 255, 255, .2)),
                                       color-stop(.5, transparent), to(transparent));
                }*/
                #style-6::-webkit-scrollbar-track
                {
                    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
                    background-color: #F5F5F5;
                }

                #style-6::-webkit-scrollbar
                {
                    width: 10px;
                    background-color: #F5F5F5;
                }

                #style-6::-webkit-scrollbar-thumb
                {
                    background-color: #fdb74d;
                    /*background-image: 
                    -webkit-linear-gradient(45deg,
                                              rgba(255, 255, 255, .2) 25%,
                                              transparent 25%,
                                              transparent 50%,
                                              rgba(255, 255, 255, .2) 50%,
                                              rgba(255, 255, 255, .2) 75%,
                                              transparent 75%,
                                              transparent)*/
                }

            div#chat_media_body {
                /*background: #05728f none repeat scroll 0 0;
                border: none;
                border-radius: 50%;
                color: #fff;
                cursor: pointer;
                font-size: 15px;
                height: 33px;
                position: absolute;
                right: 34px;
                top: 13px;
                width: 30px;

                background: #fdb74d none repeat scroll 0 0; */
                border: none;
                border-radius: 50%;
                color: #fff;
                cursor: pointer;
                font-size: 10px;
                height: 35px;
                position: absolute;
                right: 34px;
                color:  #000;
                top: 13px;
                width: 32px;
                margin-right: 12px;
            }

            div#chat_media_body svg.feather.feather-paperclip.w-full.h-full {
                margin: 4px 0;
            }

            .type_msg {
                display: inline-block;
                width: 100%;
                margin: auto;
            }

            .msgimage {
                width: 100%;
                display: block !important;
                margin: auto;
                height: auto;
            }

        </style>
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.24/css/dataTables.bootstrap4.min.css" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.css"
            integrity="sha512-In/+MILhf6UMDJU4ZhDL0R0fEpsp4D3Le23m6+ujDWXwl3whwpucJG1PEmI3B07nyJx+875ccs+yX2CqQJUxUw=="
            crossorigin="anonymous" />
        <style type="text/css">
            .body {
                font-family: 'Poppins', sans-serif;
            }

            .submitbutton {
                margin-bottom: 75px;
                display: flex;
                align-items: center;
            }

            .chat-list {
                display: none;
            }

            .single-destinations-list.style-two .details {
                padding: 1px 0px;
            }

            /* Chat CSS */
            .inbox_people {
                background: #fff;
                float: left;
                overflow: hidden;
                width: 40%;
                border-right: 1px solid #ddd;
            }

            @media only screen and (max-width : 320px) {
                .inbox_people {
                    width: 100%;
                }

                .chat_ib p {
                    white-space: inherit !important;
                }

                .mesgs {
                    width: 100% !important;

                }
            }

            /* Extra Small Devices, Phones */
            @media only screen and (max-width : 480px) {
                .inbox_people {
                    width: 100%;
                }

                .chat_ib p {
                    white-space: inherit !important;
                }

                .mesgs {
                    width: 100% !important;
                }
            }

            /* Small Devices, Tablets */
            @media only screen and (max-width : 768px) {
                .inbox_people {
                    width: 100%;
                }

                .chat_ib p {
                    white-space: inherit !important;
                }

                .mesgs {
                    width: 100% !important;
                }
            }

            .inbox_msg {
                border: 1px solid #ddd;
                clear: both;
                overflow: hidden;
            }

            .top_spac {
                margin: 20px 0 0;
            }

            .recent_heading {
                float: left;
                width: 40%;
            }

            .srch_bar {
                display: inline-block;
                text-align: right;
                width: 60%;
                padding:
            }

            .headind_srch {
                padding: 10px 29px 10px 20px;
                overflow: hidden;
                border-bottom: 1px solid #c4c4c4;
            }

            .recent_heading h4 {
                color: #0465ac;
                font-size: 16px;
                margin: auto;
                line-height: 29px;
            }

            .srch_bar input {
                outline: none;
                border: 1px solid #cdcdcd;
                border-width: 0 0 1px 0;
                width: 80%;
                padding: 2px 0 4px 6px;
                background: none;
            }

            .srch_bar .input-group-addon button {
                background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
                border: medium none;
                padding: 0;
                color: #707070;
                font-size: 18px;
            }

            .srch_bar .input-group-addon {
                margin: 0 0 0 -27px;
            }

            .chat_ib h5 {
                font-size: 15px;
                color: #464646;
                margin: 0 0 8px 0;
            }

            .chat_ib h5 span {
                font-size: 13px;
                float: right;
            }

            .chat_ib p {
                font-size: 12px;
                color: #989898;
                margin: auto;
                display: inline-block;
                white-space: nowrap;
                overflow: hidden;
                text-overflow: ellipsis;
            }

            .chat_img {
                float: left;
                width: 11%;
            }

            .chat_img img {
                width: 100%
            }

            .chat_ib {
                float: left;
                padding: 0 0 0 15px;
                width: 88%;
            }

            .chat_people {
                overflow: hidden;
                clear: both;
            }

            .chat_list {
                border-bottom: 1px solid #ddd;
                margin: 0;
                padding: 18px 16px 10px;
            }

            .inbox_chat {
                height: 550px;
                overflow-y: scroll;
            }

            .active_chat {
                background: #e8f6ff;
            }

            .incoming_msg_img {
                display: inline-block;
                width: 6%;
            }

            .incoming_msg_img img {
                width: 100%;
            }

            .received_msg {
                display: inline-block;
                padding: 0 0 0 10px;
                vertical-align: top;
                width: 92%;
            }

            .received_withd_msg p {
                background: #ebebeb none repeat scroll 0 0;
                border-radius: 0 15px 15px 15px;
                color: #646464;
                font-size: 14px;
                margin: 0;
                padding: 5px 10px 5px 12px;
                width: 100%;
            }

            .time_date {
                color: #747474;
                display: block;
                font-size: 12px;
                margin: -2px 0 0;
            }

            .received_withd_msg {
                width: 57%;
            }

            .mesgs {
                float: left;
                padding: 30px 15px 0 25px;
                width: 100%;
                overflow-y: scroll;
                scroll-behavior: smooth;
                height: 374px;
            }

            .sent_msg p {
                /*background: #071C55;*/
                background-color: #35b1d6;
                    /*background-image: 
                    -webkit-linear-gradient(45deg,
                                              rgba(255, 255, 255, .2) 25%,
                                              transparent 25%,
                                              transparent 50%,
                                              rgba(255, 255, 255, .2) 50%,
                                              rgba(255, 255, 255, .2) 75%,
                                              transparent 75%,
                                              transparent)*/
                border-radius: 12px 15px 15px 0;
                font-size: 14px;
                margin: 0;
                color: #fff;
                padding: 5px 10px 5px 12px;
                width: 100%;
            }

            .outgoing_msg {
                overflow: hidden;
                margin: 10px 0 10px;
            }

            .sent_msg {
                float: right;
                width: 46%;
            }

            .input_msg_write input {
                background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
                border: medium none;
                color: #4c4c4c;
                font-size: 15px;
                min-height: 48px;
                width: 100%;
                outline: none;
            }

            .type_msg {
                border-top: 1px solid #c4c4c4;
                position: relative;
            }

            .msg_send_btn {
                /*background: #05728f none repeat scroll 0 0;
                border: none;
                border-radius: 50%;
                color: #fff;
                cursor: pointer;
                font-size: 15px;
                height: 33px;
                position: absolute;
                right: 0;
                top: 11px;
                width: 33px;
                background: #fdb74d none repeat scroll 0 0; */
                border: none;
                border-radius: 50%;
                color: #fff;
                cursor: pointer;
                font-size: 18px;
                height: 36px;
                position: absolute;
                right: 0;
                color:  #000;
                top: 11px;
                width: 33px;
                margin-right: 10px;
            }

            .messaging {
                padding: 0 0 50px 0;
            }

            .msg_history {
                height: 516px;
                overflow-y: auto;
            }

        </style>
   
@endpush
@section('content')
    <!-- breadcrumb area start -->
    <div class="breadcrumb-area jarallax"
        style="background-image:url({{ url('//front/assets/img/bg/1.jpg') }});">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb-inner">
                        <h1 class="page-title">Inquiry & Issue</h1>
                    <ul class="page-title">
                        
                       
                        </ul>
                    </div>
                </div>
                @if(Auth::guard('webagent')->user())
                <div class="col-lg-12">
                    <!-- <p class="my-1 text-color mb-0 font-weight-light">Welcome Akshay Patel</p>
                    <p class="text-color m-0 font-weight-light">Balance : <span>2651.00</span></p> -->
                </div>
                @endif
            </div>
        </div>
    </div>
    <!-- breadcrumb area End -->
    <!-- tour list area End -->
    <div class="tour-list-area pd-top-120 pd-bottom-120">
        <div class="container">
            <div class="row">
                <div class="col-xl-4 col-lg-4 order-lg-1">
                    <div class="sidebar-area">
                        <div class="widget tour-list-widget">
                            <div class="widget-tour-list-meta">
                                <div class="single-widget-search-input-title"><i class="fa fa-list-alt"></i> Categories Type
                                </div>
                                @if (isset($ticket))
                                    <div class="single-widget-search-input">
                                        {{ $ticket->ticket_categories->name }}
                                    </div>
                                @endif
                                <div class="single-widget-search-input-title"><i class="fa fa-list-alt"></i> Sub Categories
                                    Type</div>
                                @if (isset($ticket))
                                    <div class="single-widget-search-input">
                                        {{ $ticket->ticket_sub_categories->name }}
                                    </div>
                                @endif
                                <div class="single-widget-search-input-title"><i class="fa fa-list-alt"></i> Ticket Status
                                </div>
                                <div class="single-widget-search-input">
                                    @if (isset($ticket))
                                        @if ($ticket->status == 'active')
                                            <span class="badge badge-success">Active</span>
                                        @else
                                            <span class="badge badge-danger">Close</span>
                                        @endif
                                    @endif
                                </div>
                                <div class="single-widget-search-input-title"><i class="fa fa-calendar-minus-o"></i>
                                    Returning</div>
                                <div class="single-widget-search-input">
                                    @if (isset($ticket))
                                        {{ $ticket->created_at }}
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-8 col-lg-8 order-lg-12">
                   <!--  <div class="tp-tour-list-search-area">
                        <div class="row">
                            <div class="col-xl-3 col-sm-6">
                                <a type="button" class="btn btn-yellow" href="{{ url('tickets') }}">
                                    Back
                                </a>
                            </div>
                            <div class="col-xl-3 col-sm-6">
                            </div>
                            <div class="col-xl-3 col-sm-6">
                            </div>
                            <div class="col-xl-3 col-sm-6">
                            </div>
                        </div>
                    </div> -->
                    <div class="tour-list-area">
                        <div class="row">
                            <!-- Chat boot Start -->
                            <div class="messaging">
                                <div class="inbox_msg">
                                    <div class="mesgs" id="style-6">
                                        <div class="msg_history" id="media_list">
                                        </div>
                                        <div class="msgimage">
                                            <span id="img_show" class="float-right"></span>
                                        </div>
                                        
                                    </div>
                                    @if($ticket->status=='active')
                                        <div class="type_msg">
                                            <form action="javascript:void(0)" method="post" enctype="multipart/form-data"
                                                id="sendinfo">
                                                <input type="hidden" value="{{ base64_encode($ticket->id) }}"
                                                    id="ticket_id" />
                                                   
                                                <div class="input_msg_write">
                                                    <input type="text" class="form-control write_msg"
                                                        placeholder="Type a message" name="message" id="message_input"
                                                        required aria-required="true"
                                                        data-bv-notempty-message="Message is required" />
                                                    <div class=" relative text-gray-600  chat-inputbar"
                                                        id="chat_media_body">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                            viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                            stroke-width="1.5" stroke-linecap="round"
                                                            stroke-linejoin="round"
                                                            class="feather feather-paperclip w-full h-full">
                                                            <path
                                                                d="M21.44 11.05l-9.19 9.19a6 6 0 0 1-8.49-8.49l9.19-9.19a4 4 0 0 1 5.66 5.66l-9.2 9.19a2 2 0 0 1-2.83-2.83l8.49-8.48">
                                                            </path>
                                                        </svg>
                                                        <input type="file"
                                                            class="w-full h-full top-0 left-0 absolute opacity-0 chat-input"
                                                            id="chat_file" name="media">
                                                    </div>
                                                    <button class="msg_send_btn sms_send_button" type="submit"
                                                        id="send_text_button"><i class="fa fa-paper-plane"
                                                            aria-hidden="true"></i></button>
                                                </div>
                                            </form>
                                        </div>
                                        @endif
                                </div>
                            </div>
                            <span id="msg_error" class="text-danger"></span>
                            <span id="img_error" class="text-danger"></span>
                            <!-- Chat Boot End -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- tour list area End -->
@endsection
@push('script')
    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.24/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/js/dropify.min.js"
        integrity="sha512-8QFTrG0oeOiyWo/VM9Y8kgxdlCryqhIxVeRpWSezdRRAvarxVtwLnGroJgnVW9/XBRduxO/z1GblzPrMQoeuew=="
        crossorigin="anonymous"></script>
    <script type="text/javascript"
        src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js"></script>
    <script>
        $(document).on('keyup', '#message_input', function(e) {
            //console.log('keyup');
            if (e.keyCode == 13) {
                $("#send_text_button").trigger("click");
            }
        });
        setInterval(function() {
            var id = '{{ base64_encode($ticket->id) }}';
            var count = '{{ $ticket_message_count }}';
            $.get('{{ url('tickets/getcount') }}/' + id, function(data, status) {
                //console.log(JSON.parse(data).msg_count);
                if (count != JSON.parse(data).msg_count) {
                    getMessages(id);
                }
            });
        }, 5000);

        function getMessages($id = null) {
            console.log($id);
            $.get("{{ url('/tickets/getlist') }}/" + $id, function(data, status) {

                var ticket = JSON.parse(data).ticket;
                var $html = '';

                if (ticket) {
                    var file = '';
                    if (ticket.file) {
                        if (ticket.file_type == "jpg" || ticket.file_type == "jpeg" || ticket.file_type == "png") {
                            file += '<a href="{{ url('public/ticket/') }}/' + ticket.file +
                                '" target="_blank"><img src="{{ url('public/ticket/') }}/' + ticket.file +
                                '" width="70px" height="50px"></a>';
                        } else {
                            file += '<a href="{{ url('public/ticket/') }}/' + ticket.file +
                                '" target="_blank">File</a>';
                        }
                    }

                    $html += '<div class="outgoing_msg">' +
                        '<div class="sent_msg">' +
                        '<p>' + ticket.message + '<br/>' + file + '</p>' +
                        '<span class="time_date">' + moment(ticket.created_at).format('DD-MM-YYYY,h:mm a') +
                        '</span>' +
                        '</div></div>';
                }

                $.each(JSON.parse(data).message, function(key, value) {

                    var myfile = '';
                    if (value.file) {
                        if (value.file_type == "jpg" || value.file_type == "jpeg" || value.file_type ==
                            "png") {
                            myfile += '<a href="{{ url('public/ticket/') }}/' + value.file +
                                '" target="_blank"><img src="{{ url('public/ticket/') }}/' + value.file +
                                '" width="100px" height="100px"></a>';
                        } else {
                            myfile += '<a href="{{ url('public/ticket/') }}/' + value.file +
                                '" target="_blank">File</a>';
                        }
                    }

                    if (value.type == 'send') {

                        $html += '<div class="outgoing_msg">' +
                            '<div class="sent_msg">' +
                            '<p>' + value.message + '<br/>' + myfile + '</p>' +
                            '<span class="time_date">' + moment(value.created_at).format(
                                'DD-MM-YYYY,h:mm a') +
                            '</span>' +
                            '</div></div>';

                    } else {

                        $html += '<div class="incoming_msg">' +
                            '<div class="received_msg">' +
                            '<div class="received_withd_msg">' +
                            '<p>' + value.message + '<br/>' + myfile + '</p>' +
                            '<span class="time_date">' + moment(value.created_at).format(
                                'DD-MM-YYYY,h:mm a') +
                            '</span></div></div></div>';




                    }
                });
                $('#media_list').html($html);
                var $t = $('.msg_history');
                $t.animate({
                    "scrollTop": $('.msg_history')[0].scrollHeight
                }, "slow");
            });
            // $('.message-body').animate({scrollTop: 100}, 500);
        }




        var receiver_id = '';
        var file = null;
        $(document).ready(function() {

            @if (isset($ticket))
                var myid= '{{ base64_encode($ticket->id) }}';
                getMessages(myid)
            @else
                var myid= NULL;
            @endif

            var reader  = new FileReader();
            $("#chat_file").on("change", function(e) {

              
                file = $(this)[0].files[0];
                console.log(file);

                  swal("File is Selected !", {
                    text: "Image Selected done",
                    icon: "success",
                });

                if (this.files[0].size > 2000000) {
                    $("#img_error").html("Please upload file less than 2MB.!");
                    //alert("Please upload file less than 2MB. Thanks!!");
                    $('#send_sms_form').prop('disabled', true);
                    $(this).val('');
                } else {
                    $("#img_error").html('');
                    $("#img_show").show();
                    var img = file.name;
                    img.src ="https://ssl.gstatic.com/onebox/media/olympics/photos/o16/archive/MTZSPDEC74T805NJ_213x120.JPG"  
                    $('#img_show').append(img);
                    //$("#img_show").html(file.name);
                }

            });
            reader.readAsDataURL(thefile);


            $("#send_text_button").click(function() {
                if (file) {
                    var $message_new = $('#message_input').val();
                    if ($message_new == '') {
                        $('#sendinfo').bootstrapValidator('removeField', $message_new);
                    }

                }
            });




            $('#sendinfo').bootstrapValidator().on('success.form.bv', function(e) {


                //alert('test');
                // Prevent submit form
                $("#send_text_button").attr("disabled", true);
                e.preventDefault();
                //var $number = $('#send_sms_number').val();
                var $ticket_id = $('#ticket_id').val();
                var $message = $('#message_input').val();
                var $media = $('#chat_file').val();


                var from = new FormData();

                from.append('ticket_id', $ticket_id);
                from.append('message', $message);
                if (file) {
                    from.append('media', file, file.name);
                }
                from.append('_token', '{{ csrf_token() }}');
                //console.log(from);
                // if ($message == '') {
                //     //$("#msg_error").html("The Message is required");
                //     $('#send_text_button').removeAttr("disabled");
                //     return false;
                // }

                $.ajax({
                    url: '{{ route('front.tickets.sendmsg') }}',
                    dataType: 'json',
                    data: from,
                    method: 'POST',
                    cache: false,
                    contentType: false,
                    processData: false,
                    timeout: 60000,
                    success: function(data) {
                        $("#img_show").html('');
                        file = null;
                        $('#message_input').val('');
                        $('#chat_file').val('');
                        //$('#charNum').text('160');
                        getMessages($ticket_id);
                        $('#send_sms_form').bootstrapValidator("resetForm", true);
                        $('#send_text_button').removeAttr("disabled");
                    },
                    error: function() {
                        $('#send_text_button').removeAttr("disabled");
                    }
                });
            });

        });

    </script>
@endpush
