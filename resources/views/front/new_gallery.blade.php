@extends('front.new-layouts.master')
@section('title','gallery')
@section('description',$image[0]->seo_description)
@section('keywords',$image[0]->seo_keywords)
@section('content')
    <!-- breadcrumb area start -->
    <div class="breadcrumb-area jarallax" style="background-image:url(assets/img/bg/1.png);">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb-inner">
                        <h1 class="page-title">Gallery</h1>
                        <ul class="page-list">
                            <li><a href="{{url('/')}}">Home</a></li>
                            <li>Gallery</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- breadcrumb area End -->

    <!-- blog area start -->
    <div class="blog-list-area pd-top-120">
        <div class="container">
            <div class="row justify-content-center">
                @if(isset($image))
                    @foreach($image as $key => $value)
                        <div class="col-lg-4 col-md-6">
                            <div class="single-blog">
                                <a href="{{url('all-image')}}/{{$value->slug}}">
                                    <div class="thumb">
                                        <img src="{{$url}}/{{$value->gallery_image[0]->image}}" alt="blog" height="350" width="520">
                                    </div>
                                </a>
                                <div class="single-blog-details">
                                        
                                    <h4 class="title"><a>{{$value->category}}</a></h4>
                                    <a class="btn-read-more" href="{{url('all-image')}}/{{$value->slug}}"><span>View More<i class="la la-arrow-right"></i></span></a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div> 
    <!-- blog area End -->
@endsection