@push('style')
 <style>
        /*This CSS Compulsary Add And Use*/
        .banner-slider-item .margin-top {
            margin-top: 80px;
        }

        @media only screen and (max-width: 1100px) {
            .banner-slider .banner-slider-item {
                height: 700px !important;
                padding: 170px 0 240px 0 !important;
                overflow: hidden !important;
            }
        }

        .custom_tab .tab-content {
            box-shadow: 0px 3px 13px #23397421 !important;
        }

        .tp-main-search {
            box-shadow: none;
        }

        .tp-holiday-plan-area.mg-top-96 {
            margin-top: 0 !important;
            padding-top: 72px !important;
        }

        .pd-top-110 {
            padding: 50px 0px !important;
        }

        .copyright-inner .privacypolicy {
            text-align: center;
            font-size: 14px;
            letter-spacing: 0.28px;
            color: #ffffff;
            line-height: 44px;
        }

        .banner-tour-package {
            visibility: hidden !important;
        }

        small.help-block {
            color: red;
        }

        .fixed {
            z-index: 9999;
        }

        .bg-dash-blue {
            color: white;
            background-color: #061847;
        }

        .single-input-title {
            color: #0d1c55 !important;
        }
        .active {
            color: #f3941e !important;
        }

        .tp-main-search-area {
            margin-top: -40px;
        }

        .tp-main-search .nav_link:hover {
            color: #f3941e;
            transition: all 0.5s;
        }

        .breadcrumb-area .page-title {
            font-size: 30px;
        }
    </style>
@endpush
<div class="search-area tp-main-search-area">
    <div class="container">
        <div class="tp-main-search py-0 px-0">
           


            <div class="row bg-dash-blue rounded d-flex justify-content-center">
                
                <div class="col-lg-6 col-sm-6 col-xl-1 col-12 nav_link text-center py-xl-4 py-lg-3 py-2">
                    <a href="{{url('/dashboard')}}" class=" {{ request()->segment(1) == 'dashboard' ? 'active' : '' }}">My profile</a>
                </div>
                @if(Auth::guard('webagent')->user() || Auth::guard('webmember')->user())
                <div class="col-lg-6 col-sm-6 col-xl-2 col-12 nav_link text-center py-xl-4 py-lg-3 py-2">
                    <a href="{{route('membership.detail')}}" class="{{ request()->segment(1) == 'membership-detail' ? 'active' : '' }}">Membership details
                    </a>
                </div>                   
                @endif
                <div class="col-lg-6 col-sm-6 col-xl-2 col-12 nav_link text-center py-xl-4 py-lg-3 py-2">
                    <a href="#">Booking History</a>
                </div>
                <div class="col-lg-6 col-sm-6 col-xl-2 col-12 nav_link text-center py-xl-4 py-lg-3 py-2">
                    <a href="{{url('/tickets')}}" class="{{ request()->segment(1) == 'tickets' ? 'active' : '' }}">Help & Support</a>
                </div>
                @if(Auth::guard('webagent')->user() || Auth::guard('webmember')->user())
                <div class="col-lg-5 col-sm-5 col-xl-2 col-12 nav_link text-center py-xl-4 py-lg-3 py-2">
                    <a href="{{url('/membership-plans')}}" {{ request()->segment(1) == 'membership-plans' ? 'active' : '' }}>Add Members</a>
                </div>
                @endif

                @if(Auth::user() || Auth::guard('webmember')->user())
                <div class="col-lg-6 col-sm-6 col-xl-2 col-12 nav_link text-center py-xl-4 py-lg-3 py-2">
                    <a href="{{ url('review')}}" {{ request()->segment(1) == 'review' ? 'active' : '' }}>Review</a>
                </div>
                @endif
                
                <div class="col-lg-6 col-sm-6 col-xl-1 col-12 nav_link text-center py-xl-4 py-lg-3 py-2">
                    <a href="{{url('/privacy-policy')}}" class="{{ request()->segment(1) == 'privacy-policy' ? 'active' : '' }}">Policy</a>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="topupModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="single-page-small-title text-center modal-title my-auto" id="exampleModalLabel">Add Wallet Payment</h4>
                <button type="button" class="close my-auto" data-dismiss="modal" aria-label="Close ">
                    <!-- <span aria-hidden="true">&times;</span> --><i class="fa fa-window-close" aria-hidden="true"></i>
                </button>
            </div>
            <div class="modal-body p-0">
                <div class="container p-0">
                    <div class="row">
                        <div class="col-lg-12 mx-auto">
                            <div class="blog-comment-area">
                                <form action="{{url('tickets/topup')}}" id="topupform" method="post" class="tp-form-wrap bg-gray tp-form-wrap-one p-3">
                                    @csrf
                                    @if(Auth::guard('webagent')->user())
                                        <input type="hidden" name="agent_name" value="{{ Auth::guard('webagent')->user()->first_name}} {{Auth::guard('webagent')->user()->last_name }}">
                                        <input type="hidden" name="phone_number" value="{{ Auth::guard('webagent')->user()->contact_details}}">
                                        <input type="hidden" name="email" value="{{ Auth::guard('webagent')->user()->company_email}}">
                                        <input type="hidden" name="agent_id" value="{{ Auth::guard('webagent')->user()->id}}">
                                    @endif
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="single-input-wrap">
                                                <span class="single-input-title"><i class="fa fa-money" aria-hidden="true"></i>&nbsp;&nbsp;Amount</span>
                                                <input type="text" onkeyup="checkamount()" id="amount" class="form-control" name="amount" required="" data-bv-notempty-message="The Recharge Amount is required" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" />
                                                <span class="error" id="error" style="color:red; display:none; font-size: 85%;">Please Enter Minimum amount 1.</span>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="single-input-wrap">
                                                <span class="single-input-title"><i class="fa fa-sticky-note-o"
                                                        aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Note</span>
                                                <textarea name="note" cols="30" rows="10"></textarea>
                                            </div>
                                        </div>
                                        <div class="col-12 text-right">
                                            <button type="submit" id="addWallet" class="btn btn-yellow" href="#">Add to Wallet</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@push('script')
<script type="text/javascript">
    function checkamount(){
    var amount = $('#amount').val();
    if(amount == 0){
        $('#error').show();
        $("#addWallet").attr("disabled", true);
       
    }else{
        $('#error').hide();
    }
}
</script>
@endpush