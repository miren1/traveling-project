 <!-- preloader area start -->
 <div class="preloader" id="preloader">
    <div class="preloader-inner">
        <div class="spinner">
            <div class="dot1"></div>
            <div class="dot2"></div>
        </div>
    </div>
</div>
<!-- preloader area end -->

<!-- search popup start -->
<div class="body-overlay" id="body-overlay"></div>
<div class="search-popup" id="search-popup">
    <form action="{{route('home')}}" class="search-form">
        <div class="form-group">
            <input type="text" class="form-control" placeholder="Search.....">
        </div>
        <button type="submit" class="submit-btn"><i class="fa fa-search"></i></button>
    </form>
</div>
<!-- search popup End -->

<!-- navbar area start -->
<nav class="navbar navbar-area navbar-expand-lg nav-style-02">
    <div class="container nav-container">
        <div class="responsive-mobile-menu">
            <div class="mobile-logo">
                <a href="{{route('home')}}">
                    <img src="{{url('assets/img/sticky-logo.png')}}" alt="logo">
                </a>
            </div>
            <button class="navbar-toggler float-right" type="button" data-toggle="collapse" data-target="#tp_main_menu" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggle-icon">
                    <span class="line"></span>
                    <span class="line"></span>
                    <span class="line"></span>
                </span>
            </button>
            <div class="nav-right-content">
                <ul class="pl-0">
                    <!-- <li class="top-bar-btn-booking">
                        <a class="btn btn-yellow" href="tour-details.html">Book Now <i class="fa fa-paper-plane"></i></a>
                    </li>
                    <li class="tp-lang">
                        <div class="tp-lang-wrap">
                            <select class="select single-select" style="display: none;">
                              <option value="1">ENG</option>
                              <option value="2">BAN</option>
                              <option value="3">Chinese</option>
                              <option value="3">Spanish</option>
                            </select><div class="nice-select select single-select" tabindex="0"><span class="current">ENG</span><ul class="list"><li data-value="1" class="option selected">ENG</li><li data-value="2" class="option">BAN</li><li data-value="3" class="option">Chinese</li><li data-value="3" class="option">Spanish</li></ul></div>
                        </div>
                    </li>
                    <li class="search">
                        <i class="ti-search"></i>
                    </li> -->
                    <li class="notification mt-2">
                        <a class="signUp-btn" href="{{route('login')}}">
                            <i class="fa fa-user-o"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        
        <div class="collapse navbar-collapse" id="tp_main_menu">
            <div class="logo-wrapper desktop-logo">
                <a href="{{route('home')}}" class="main-logo">
                    <img src="{{url('assets/img/logo.png')}}" alt="logo">
                </a>
                <a href="{{route('home')}}" class="sticky-logo">
                    <img src="{{url('assets/img/sticky-logo.png')}}" alt="logo">
                </a>
            </div>
            <ul class="dropdown-menu-btn">
                <li class="line"></li>
                <li class="line"></li>
                <li class="line"></li>
            </ul>
            <ul class="navbar-nav">                   
                <li class="tp-lang d-lg-none">
                    <a href="{{url('/membership-plans')}}">Membership Plan</a>
                </li>
                <li class="search d-lg-none">
                    <a href="{{url('/askforcall')}}">Ask for call</a>
                </li>
                <li class="menu-item-has-children">
                    <a href="{{route('home')}}">Home</a>
                </li>
                <li class="">
                    <a href="{{url('/tickets')}}">Tickets</a>
                </li>
                <!-- <li class="menu-item-has-children">
                    <a href="#">Events</a>
                </li> -->
                <li class="menu-item-has-children">
                    <a href="#">Gallery</a>
                </li>
                <li>
                    <a href="#">Contact</a>
                </li>
            </ul>
        </div>
        <div class="nav-right-content">
            <ul>
                <li class="tp-lang">
                    <a href="{{url('/membership-plans')}}">Membership Plan</a>
                </li>
                <li class="search">
                    <a href="{{url('/askforcall')}}">Ask for call</a>
                </li>
                <li class="notification">
                    <a class="" href="{{route('login')}}">
                        <i class="fa fa-user-o"></i>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>
<!-- navbar area end -->