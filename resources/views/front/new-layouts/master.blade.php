<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- For Search Engin Optimization -->
    <meta name="description" content="@yield('description')">
    <meta name="keywords" content="@yield('keywords')">
    <title>@if(View::hasSection('title')) @yield('title') @else India's No. 1 - Travel Investment Membership Plans - Jag Joyu @endif</title>
    
    <!-- For Search Engin Optimization -->

    <!-- favicon -->
    <link rel=icon href="{{url('/front')}}/assets/img/favicon.png" sizes="20x20" type="image/png">

    <!-- Additional plugin css -->
    <link rel="stylesheet" href="{{ url('assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ url('assets/css/animate.css') }}">
    <link rel="stylesheet" href="{{ url('assets/css/magnific-popup.css') }}">
    <link rel="stylesheet" href="{{ url('assets/css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ url('assets/css/slick.css') }}">
    <link rel="stylesheet" href="{{ url('assets/css/swiper.min.css') }}">
    <link rel="stylesheet" href="{{ url('assets/css/nice-select.css') }}">
    <link rel="stylesheet" href="{{ url('assets/css/jquery-ui.min.css') }}">
    <!-- icons -->
    <link rel="stylesheet" href="{{ url('assets/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ url('assets/css/themify-icons.css') }}">
    <link rel="stylesheet" href="{{ url('assets/css/line-awesome.min.css') }}">
    <!-- main css -->
    <link rel="stylesheet" href="{{ url('assets/css/style.css') }}">
    <!-- responsive css -->
    <link rel="stylesheet" href="{{ url('assets/css/responsive.css') }}">
    <style type="text/css">
         .popup-button a {
        top: 200px;
        position: fixed;
        right: -57px;
        z-index: 1000;
        transform: rotate(90deg);
        background-color: #f3941e !important;
        padding: 10px 20px 35px;
        height: 0px;
        background-color: #000;
        color: #fff;
        border-bottom-left-radius: 15px;
        border-bottom-right-radius: 15px;
    }

    .popup-button a:hover {
        text-decoration: none;
        color: #fff;
    }
    #mypopup{
        position: fixed; !important;
        z-index: 9999 !important;
        opacity: 1 !important;
        background-color: #fff !important; 
        text-align: center !important;
        background: #fff; 
        top: 20% !important;
        padding: 2px !important;
        position: fixed  !important;
        width: 40%  !important;
        height: 50%  !important;
        border-radius: 10px;
    }
    .b-close{
        color: #fff;
        background: #818181;
        padding: 0px 11px;
        position: absolute;
        right: -20px;
        top: -25px;
        cursor: pointer;
        border-radius: 20px;
        
    }
    .slick-arrow{
        display: none !important;
    }
    .swiper-slide-active{
        width: 300px;
        height: 300px;
    }
    .responsive {
      width: 100%;
      height: auto;
    }
    .dropdown-toggle::after{
            display: none !important;
        }
    #dropdownHover a:hover{
        color: black !important;
    }

   /* @media (min-width: 1024px) { 
        .navbar-area .nav-container .navbar-collapse .navbar-nav{
            margin-top: 33px !important;
            display: block !important;
        }
        .navbar-area .nav-container .desktop-logo img{
            width: 84px !important;
            margin-left: -20px !important;
            margin-right: -50px !important;
        }
        .navbar-area .nav-container .navbar-collapse .navbar-nav li a{
            font-size: 12px !important;
        }
    }*/


    @media only screen and (max-width:1366px)  {
        #popupads {
            position: absolute !important;
            z-index: 9999 !important;
            opacity: 1 !important;
            background-color: #fff !important;
            text-align: center !important;
            background: #fff !important;
            padding: 2px !important;
            position: fixed !important;
            width: 50% !important;
            height: 60% !important;
            border-radius: 10px;
        }
        .b-close {
            color: #fff;
            background: #818181;
            padding: 2px 10px;
            position: absolute;
            right: -17px;
            top: -18px;
            cursor: pointer;
            border-radius: 20px;
        }
    }

    @media only screen and (max-width:1280px)  {
        #popupads {
            position: absolute !important;
            z-index: 9999 !important;
            opacity: 1 !important;
            background-color: #fff !important;
            text-align: center !important;
            background: #fff !important;
            padding: 2px !important;
            position: fixed !important;
            width: 50% !important;
            height: 19% !important;
            border-radius: 10px;
        }
        .b-close {
            color: #fff;
            background: #818181;
            padding: 2px 10px;
            position: absolute;
            right: -17px;
            top: -18px;
            cursor: pointer;
            border-radius: 20px;
        }
         .navbar-area .nav-container .navbar-collapse .navbar-nav{
            margin-top: 33px !important;
            display: block !important;
        }
        .navbar-area .nav-container .desktop-logo img{
            max-width: 100px !important;
            margin-left: -55px !important;
            margin-right: -58px !important;
        }
        .navbar-area .nav-container .navbar-collapse .navbar-nav li{
            margin: 0 9px !important;
        }
        .navbar-area .nav-container .navbar-collapse .navbar-nav li.menu-item-has-children .sub-menu{
            left: -14px !important;
            min-width: 154px !important;
        }
        .navbar-area .nav-container .navbar-collapse .navbar-nav li a{
            font-size: 15px !important;
        }
        .btn {
            font-size: 15px !important;
            height: 50px !important;
            padding: 0 11px !important;
        }
    }

    @media only screen and (max-width:1024px){
        #popupads {
            position: absolute !important;
            z-index: 9999 !important;
            opacity: 1 !important;
            background-color: #fff !important;
            text-align: center !important;
            background: #fff !important;
            padding: 2px !important;
            position: fixed !important;
            width: 50% !important;
            height: 25% !important;
            border-radius: 10px;
        }
        .b-close {
            color: #fff;
            background: #818181;
            padding: 2px 10px;
            position: absolute;
            right: -17px;
            top: -18px;
            cursor: pointer;
            border-radius: 20px;
        }   
        .navbar-area .nav-container .navbar-collapse .navbar-nav{
            margin-top: 33px !important;
            display: block !important;
        }
        .navbar-area .nav-container .desktop-logo img{
            width: 84px !important;
            margin-left: -20px !important;
            margin-right: -50px !important;
        }
        .navbar-area .nav-container .navbar-collapse .navbar-nav li a{
            font-size: 12px !important;
        }
    }

    @media only screen and (max-width: 768px){
        #popupads {
            position: absolute !important;
            z-index: 9999 !important;
            opacity: 1 !important;
            background-color: #fff !important;
            text-align: center !important;
            background: #fff !important;
            padding: 2px !important;
            position: fixed !important;
            top: 20% !important;
            width: 50% !important;
            height: 13% !important;
            border-radius: 10px;
        }
        .b-close {
            color: #fff;
            background: #818181;
            padding: 2px 10px;
            position: absolute;
            right: -17px;
            top: -18px;
            cursor: pointer;
            border-radius: 20px;
        }   
    }

    @media only screen and (max-width: 365px){
        #popupads {
            position: absolute !important;
            z-index: 9999 !important;
            opacity: 1 !important;
            background-color: #fff !important;
            text-align: center !important;
            background: #fff !important;
            padding: 2px !important;
            position: fixed !important;
            width: 50% !important;
            height: 16% !important;
            border-radius: 10px;
        }
        .b-close {
            color: #fff;
            background: #818181;
            padding: 2px 10px;
            position: absolute;
            right: -17px;
            top: -18px;
            cursor: pointer;
            border-radius: 20px;
        }   
    }

    @media only screen and (max-width: 360px){
        #popupads {
            position: absolute !important;
            z-index: 9999 !important;
            opacity: 1 !important;
            background-color: #fff !important;
            text-align: center !important;
            background: #fff !important;
            padding: 2px !important;
            position: fixed !important;
            width: 50% !important;
            height: 19% !important;
            border-radius: 10px;
        }
        .b-close {
            color: #fff;
            background: #818181;
            padding: 2px 10px;
            position: absolute;
            right: -17px;
            top: -18px;
            cursor: pointer;
            border-radius: 20px;
        }   
    }

    @media only screen and (max-width: 280px){
        #popupads {
            position: absolute !important;
            z-index: 9999 !important;
            opacity: 1 !important;
            background-color: #fff !important;
            text-align: center !important;
            background: #fff !important;
            padding: 2px !important;
            position: fixed !important;
            width: 50% !important;
            height: 14.5% !important;
            border-radius: 10px;
        }
        .b-close {
            color: #fff;
            background: #818181;
            padding: 2px 10px;
            position: absolute;
            right: -17px;
            top: -18px;
            cursor: pointer;
            border-radius: 20px;
        }   
    }
    </style>
    @stack('css')
</head>
<body>
    @include('front.new-layouts.header')
    @yield('content')
    <x:notify-messages />   
    
    @include('front.new-layouts.footer')   
     <!-- Additional plugin js -->
    <script src="{{ url('assets/js/jquery-2.2.4.min.js') }}"></script>
    <script src="{{ url('assets/js/popper.min.js') }}"></script>
    <script src="{{ url('assets/js/bootstrap.min.js') }}"></script>
    <script src="{{ url('assets/js/jquery.magnific-popup.js') }}"></script>
    <script src="{{ url('assets/js/owl.carousel.min.js') }}"></script>
    <script src="{{ url('assets/js/wow.min.js') }}"></script>
    <script src="{{ url('assets/js/slick.js') }}"></script>
    <script src="{{ url('assets/js/waypoints.min.js') }}"></script>
    <script src="{{ url('assets/js/jquery.counterup.min.js') }}"></script>
    <script src="{{ url('assets/js/imagesloaded.pkgd.min.js') }}"></script>
    <script src="{{ url('assets/js/isotope.pkgd.min.js') }}"></script>
    <script src="{{ url('assets/js/swiper.min.js') }}"></script>
    <script src="{{ url('assets/js/jquery.nice-select.min.js') }}"></script>
    <script src="{{ url('assets/js/jquery-ui.min.js') }}"></script>
    <script src="{{ url('assets/js/jarallax.min.js') }}"></script>
    <!-- main js -->
    <script src="{{ url('assets/js/main.js') }}"></script>
    <!--Start Equal Height Reviews Portion-->
    <script>
    //This Jquery Use Compulsary
        $(document).ready(function () {
            (function () {
                equalHeight(false);
            })();
            window.onresize = function () {
                equalHeight(true);
            }
            function equalHeight(resize) {
                var elements = document.getElementsByClassName("equalHeight"),
                    allHeights = [],
                    i = 0;
                if (resize === true) {
                    for (i = 0; i < elements.length; i++) {
                        elements[i].style.height = 'auto';
                    }
                }
                for (i = 0; i < elements.length; i++) {
                    var elementHeight = elements[i].clientHeight;
                    allHeights.push(elementHeight);
                }
                for (i = 0; i < elements.length; i++) {
                    elements[i].style.height = Math.max.apply(Math, allHeights) + 'px';
                    if (resize === false) {
                        elements[i].className = elements[i].className + " show";
                    }
                }
            }
        });

        //This Jquery Use Compulsary
</script>
<script src="{{ asset('vendor/mckenziearts/laravel-notify/js/notify.js') }} "></script>
<!--End Equal Height Reviews Portion-->
@stack('script')      
</body>
</html>