 <!-- preloader area start -->
 <div class="preloader" id="preloader">
        <div class="preloader-inner">
            <div class="spinner">
                <div class="dot1"></div>
                <div class="dot2"></div>
            </div>
        </div>
</div>
    <!-- preloader area end -->

    <!-- search popup start -->
    <div class="body-overlay" id="body-overlay"></div>
    <div class="search-popup" id="search-popup">
        <form action="{{route('home')}}" class="search-form">
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Search.....">
            </div>
            <button type="submit" class="submit-btn"><i class="fa fa-search"></i></button>
        </form>
    </div>
    <!-- search popup End -->

    <!-- //. sign up Popup -->
    <div class="signUp-popup login-register-popup" id="signUp-popup">
        <div class="login-register-popup-wrap">
            <div class="row no-gutters">
                <div class="col-lg-6">
                    <div class="thumb">
                        <img src="assets/img/loginleftimg.jpg" alt="img">
                    </div>
                </div>
                <div class="col-lg-6 align-self-center">
                    <div class="shape-thumb">
                        <img src="assets/img/others/signup-shape.png" alt="img">
                    </div>
                    <form class="login-form-wrap">
                        <h4 class="text-center">Sign Up</h4>
                        <div class="single-input-wrap style-two">
                            <input type="text" placeholder="Name">
                            <span class="single-input-title"><i class="fa fa-user"></i></span>
                        </div>
                        <div class="single-input-wrap style-two">
                            <input type="text" placeholder="Email">
                            <span class="single-input-title"><i class="fa fa-envelope"></i></span>
                        </div>
                        <div class="single-input-wrap style-two">
                            <input type="text" placeholder="Password">
                            <span class="single-input-title"><i class="fa fa-lock"></i></span>
                        </div>
                        <label class="checkbox">
                            <input type="checkbox">
                            <span>Remember me</span>
                        </label>
                        <div class="single-input-wrap style-two">
                            <button class="btn btn-yellow w-100">Sign Up</button>
                        </div>
                        <div class="sign-in-btn">I already have an account. <a href="#">Sign In</a></div>
                        <div class="social-wrap">
                            <p>Or Continue With</p>
                            <ul class="social-icon">
                                <li>
                                    <a class="facebook" href="#" target="_blank"><i class="fa fa-facebook  "></i></a>
                                </li>
                                <li>
                                    <a class="twitter" href="#" target="_blank"><i class="fa fa-twitter  "></i></a>
                                </li>
                                <li>
                                    <a class="pinterest" href="#" target="_blank"><i class="fa fa-pinterest"></i></a>
                                </li>
                            </ul>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- //. sign up Popup End -->


    


    <!-- navbar area start -->
<nav class="navbar navbar-area navbar-expand-lg nav-style-02">
    <div class="container nav-container">
        <div class="responsive-mobile-menu">
            <div class="mobile-logo">
                <a href="{{route('home')}}">
                    <img src="{{url('assets/img/sticky-logo.png')}}" alt="logo">
                </a>
            </div>
            <button class="navbar-toggler float-right" type="button" data-toggle="collapse" data-target="#tp_main_menu" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggle-icon">
                    <span class="line"></span>
                    <span class="line"></span>
                    <span class="line"></span>
                </span>
            </button>
            <div class="nav-right-content">
                <ul class="pl-0">
                    
                    <li class="notification mt-2">
                        <a class="signUp-btn" href="{{route('login')}}">
                            <i class="fa fa-user-o"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        
        <div class="collapse navbar-collapse" id="tp_main_menu">
            <div class="logo-wrapper desktop-logo">
                <a href="{{route('home')}}" class="main-logo">
                    <img src="{{url('assets/img/logo.png')}}" alt="logo">
                </a>
                <a href="{{route('home')}}" class="sticky-logo">
                    <img src="{{url('assets/img/sticky-logo.png')}}" alt="logo">
                </a>
            </div>
            <ul class="dropdown-menu-btn">
                <li class="line"></li>
                <li class="line"></li>
                <li class="line"></li>
            </ul>
            <ul class="navbar-nav">                   
                <li class="tp-lang d-lg-none">
                    <a href="{{url('/membership-plans')}}">Membership Plan</a>
                </li>
                <li class="search d-lg-none">
                    <a href="{{url('/askforcall')}}">Ask for call</a>
                </li>
                <li class="menu-item-has-children">
                    <a href="{{route('home')}}">Home</a>
                </li>
                <li class="">
                    <a href="{{url('/tickets')}}">Tickets</a>
                </li>
                <!-- <li class="menu-item-has-children">
                    <a href="#">Events</a>
                </li> -->
                <li class="menu-item-has-children">
                    <a href="#">Gallery</a>
                </li>
                <li>
                    <a href="#">Contact</a>
                </li>
            </ul>
        </div>
        <div class="nav-right-content">
            <ul>
                <li class="tp-lang">
                    <a href="{{url('/membership-plans')}}">Membership Plan</a>
                </li>
                <li class="search">
                    <a href="{{url('/askforcall')}}">Ask for call</a>
                </li>
                <li class="notification">
                    <a class="" href="{{route('login')}}">
                        <i class="fa fa-user-o"></i>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>
<!-- navbar area end -->