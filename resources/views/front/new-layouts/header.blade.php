    <!-- preloader area start -->
    <div class="preloader" id="preloader">
        <div class="preloader-inner">
            <div class="spinner">
                <div class="dot1"></div>
                <div class="dot2"></div>
            </div>
        </div>
    </div>
    <!-- preloader area end -->

    <!-- search popup start -->
    <!-- <div class="body-overlay" id="body-overlay"></div>
    <div class="search-popup" id="search-popup">
        <form action="index.html" class="search-form">
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Search.....">
            </div>
            <button type="submit" class="submit-btn"><i class="fa fa-search"></i></button>
        </form>
    </div> -->
    <!-- search popup End -->

    <!-- //. sign up Popup -->
    <!-- <div class="signUp-popup login-register-popup" id="signUp-popup">
        <div class="login-register-popup-wrap">
            <div class="row no-gutters">
                <div class="col-lg-6">
                    <div class="thumb">
                        <img src="{{ url('assets/img/others/signup.png') }}" alt="img">
                    </div>
                </div>
                <div class="col-lg-6 align-self-center">
                    <div class="shape-thumb">
                        <img src="{{ url('assets/img/others/signup-shape.png') }}" alt="img">
                    </div>
                    <form class="login-form-wrap">
                        <h4 class="text-center">Sign Up</h4>
                        <div class="single-input-wrap style-two">
                            <input type="text" placeholder="Name">
                            <span class="single-input-title"><i class="fa fa-user"></i></span>
                        </div>
                        <div class="single-input-wrap style-two">
                            <input type="text" placeholder="Email">
                            <span class="single-input-title"><i class="fa fa-envelope"></i></span>
                        </div>
                        <div class="single-input-wrap style-two">
                            <input type="text" placeholder="Password">
                            <span class="single-input-title"><i class="fa fa-lock"></i></span>
                        </div>
                        <label class="checkbox">
                            <input type="checkbox">
                            <span>Remember me</span>
                        </label>
                        <div class="single-input-wrap style-two">
                            <button class="btn btn-yellow w-100">Sign Up</button>
                        </div>
                        <div class="sign-in-btn">I already have an account. <a href="#">Sign In</a></div> 
                        <div class="social-wrap">
                            <p>Or Continue With</p>
                            <ul class="social-icon">
                                <li>
                                    <a class="facebook" href="#" target="_blank"><i class="fa fa-facebook  "></i></a>
                                </li>
                                <li>
                                    <a class="twitter" href="#" target="_blank"><i class="fa fa-twitter  "></i></a>
                                </li>
                                <li>
                                    <a class="pinterest" href="#" target="_blank"><i class="fa fa-pinterest"></i></a>
                                </li>
                            </ul>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div> -->
    <!-- //. sign up Popup End -->

    <!-- navbar area start -->
    <nav class="navbar navbar-area navbar-expand-lg nav-style-01">
        <div class="container nav-container">
            <div class="responsive-mobile-menu">
                <div class="mobile-logo">
                    <a href="{{route('home')}}">
                        <img src="{{url('/images/jj-logo.png')}}" alt="logo">
                    </a>
                </div>
                <button class="navbar-toggler float-right" type="button" data-toggle="collapse" data-target="#tp_main_menu" 
                    aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggle-icon">
                        <span class="line"></span>
                        <span class="line"></span>
                        <span class="line"></span>
                    </span>
                </button>
                
                <!-- <div class="nav-right-content">
                    <ul class="pl-0">
                        <li class="top-bar-btn-booking">
                            <a class="btn btn-yellow" href="tour-details.html">Book Now <i class="fa fa-paper-plane"></i></a>
                        </li>
                        <li class="tp-lang">
                            <div class="tp-lang-wrap">
                                <select class="select single-select">
                                  <option value="1">ENG</option>
                                  <option value="2">BAN</option>
                                  <option value="3">Chinese</option>
                                  <option value="3">Spanish</option>
                                </select>
                            </div>
                        </li> -->
                        <!-- <li class="search">
                            <i class="ti-search"></i>
                        </li>
                        <li class="notification">
                            <a class="" href="{{route('login')}}">
                                <i class="fa fa-user-o"></i>
                            </a>
                        </li> -->
                    <!-- </ul> -->
                <!-- </div> -->
            </div>
            <div class="collapse navbar-collapse" id="tp_main_menu">
                <div class="logo-wrapper desktop-logo" style="margin-top: 30px;">
                    <a href="{{route('home')}}" class="main-logo">
                        <img src="{{url('/images/jj-logo-white.png')}}" alt="logo">
                    </a>
                    <a href="{{route('home')}}" class="sticky-logo">
                        <img src="{{url('/images/jj-logo.png')}}" alt="logo">
                    </a>
                </div>
                <ul class="navbar-nav">
                    <li>
                <div class="dropdown show quickmenu">
                  <a class=" dropdown-toggle quickmenutag" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-menu-button-wide" viewBox="0 0 16 16">
                      <path d="M0 1.5A1.5 1.5 0 0 1 1.5 0h13A1.5 1.5 0 0 1 16 1.5v2A1.5 1.5 0 0 1 14.5 5h-13A1.5 1.5 0 0 1 0 3.5v-2zM1.5 1a.5.5 0 0 0-.5.5v2a.5.5 0 0 0 .5.5h13a.5.5 0 0 0 .5-.5v-2a.5.5 0 0 0-.5-.5h-13z"/>
                      <path d="M2 2.5a.5.5 0 0 1 .5-.5h3a.5.5 0 0 1 0 1h-3a.5.5 0 0 1-.5-.5zm10.823.323-.396-.396A.25.25 0 0 1 12.604 2h.792a.25.25 0 0 1 .177.427l-.396.396a.25.25 0 0 1-.354 0zM0 8a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v5a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V8zm1 3v2a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1v-2H1zm14-1V8a1 1 0 0 0-1-1H2a1 1 0 0 0-1 1v2h14zM2 8.5a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zm0 4a.5.5 0 0 1 .5-.5h6a.5.5 0 0 1 0 1h-6a.5.5 0 0 1-.5-.5z"/>
                    </svg>
                  </a>
                  <div class="dropdown-menu" id="dropdownHover" aria-labelledby="dropdownMenuLink">
                    <a class="dropdown-item" style="color: black;" href="{{route('AboutUsDemo')}}">About us</a>
                    <a class="dropdown-item" style="color: black;" href="{{route('home')}}/#newEvents">Events</a>
                    <a class="dropdown-item" style="color: black;" href="{{ route('blogAll') }}">Blogs</a>
                    <a class="dropdown-item" style="color: black;" href="{{route('Careers')}}">Careers</a>
                    <a class="dropdown-item" style="color: black;" href="{{route('home')}}/#contactUs">Contact us</a>

                  </div>
                </div>
                    </li>
                    
                    @foreach((App\Packagtype::where('deleted_at','1')->get()) as $PackageTypes) 
                        <li>
                            <a href="{{ url('packages') }}/{{$PackageTypes->slug}}">{{$PackageTypes->name}} Tours</a>
                        </li>
                    @endforeach
                    <li class="menu-item-has-children">
                        <a href="#">Ticket</a>
                        <ul class="sub-menu">
                            <li><a href="{{route('home')}}/#home">Flight</a></li>
                            <li><a href="{{route('home')}}/#menu1">Bus</a></li>
                            <li><a href="{{route('home')}}/#menu2">Train</a></li>
                            <li><a href="#">Group Booking</a></li>
                        </ul>
                    </li>
                    <li>
                       <a href="{{route('membership-demo')}}"><button class="btn btn-yellow">Become Member</button></a>
                        <!-- <a href="{{route('membership-demo')}}">Become Member</a> -->
                    </li>
                     @if(Auth::guard('webagent')->user() || Auth::guard('webcustomer')->user() || Auth::guard('webmember')->user())
                    <li class="menu-item-has-children">
                        <a href="#"><i class="fa fa-bars"></i></a>
                        <ul class="sub-menu">
                            <li><a href="{{route('home')}}">Home</a></li>
                            @if(Auth::guard('webcustomer')->user() || Auth::guard('webmember')->user())
                            <li><a href="{{route('front.dashboard')}}">Dashboard</a></li>
                            @elseif(Auth::guard('webagent')->user())
                            <li><a href="{{route('front.tickets')}}">Dashboard</a></li>
                            @endif
                            <li><a href="{{route('front.logout')}}">logout</a></li>
                        </ul>
                    </li>
                    @else
                    <li class="notification">
                        <a class="" href="{{route('login')}}">
                            <i class="fa fa-user" aria-hidden="true"></i>
                        </a>
                    </li>
                    @endif
                </ul>
<!--            <ul class="navbar-nav">

                    <li class="menu-item-has-children">
                        <a href="#">Home</a>
                    </li>
                    <li>
                        <a href="about.html">About Us</a>
                    </li>
                    <li class="menu-item-has-children">
                        <a href="#">Pages</a>
                    </li>
                    <li class="menu-item-has-children">
                        <a href="#">Blog</a>
                    </li>
                    <li>
                        <a href="contact.html">Contact</a>
                    </li>
                </ul> -->
            </div>
        
        </div>
    </nav>
    <!-- navbar area end -->

   