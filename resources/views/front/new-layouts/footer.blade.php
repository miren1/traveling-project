<!-- footer area start -->
    <footer class="footer-area" style="background-image: url(assets/img/bg/2.png);">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="footer-widget widget">
                        <div class="about_us_widget">
                            <div class="footer-logo"> 
                                <img src="{{ url('/images/jj-logo-white.png')}}" alt="footer logo">
                            </div>
                            <p>We believe brand interaction is key in commu- nication. Real innovations and a positive customer experience are the heart of successful communication.</p>
                            <ul class="social-icon">
                                <li>
                                    <a class="facebook" href="https://www.facebook.com/JagJoyu" target="_blank"><i class="fa fa-facebook  "></i></a>
                                </li>
                                <li>
                                    <a class="twitter" href="https://twitter.com/JagJoyu" target="_blank"><i class="fa fa-twitter  "></i></a>
                                </li>
                                <li>
                                    <a class="instagram" href="https://www.instagram.com/jagjoyuts/" target="_blank"><i class="fa fa-instagram"></i></a>
                                </li>
                                <li>
                                    <a class="youtube" href="https://www.youtube.com/channel/UCciZJ7X7weZiRIG6KOHOj9w" target="_blank"><i class="fa fa-youtube"></i></a>
                                </li>
                            </ul> 
                       </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6" id="contactUs">
                    <div class="footer-widget widget ">
                        <div class="widget-contact">
                            <h4 class="widget-title">Contact us</h4>
                            <p>
                                <i class="fa fa-map-marker"></i> 
                                <span>35 First Floor, 4D Square, Visat to Gandhinagar Highway,<br> Motera, Ahmedabad, 380005</span>
                            </p>
                            <p class="location"> 
                                <i class="fa fa-envelope-o"></i>
                                <span>business@jagjoyu.com</span>
                            </p>
                            <p class="telephone">
                                <i class="fa fa-phone base-color"></i> 
                                <span>
                                    97258 76529
                                </span>
                            </p>
                        </div>
                        <a href="{{route('front.askforcall')}}">
                        <h4 class="widget-title">Ask For Call</h4>
                        </a>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6">
                    <div class="footer-widget widget">
                        <div class="widget-contact">    
                                <iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d1834.9357260943166!2d72.59291860818233!3d23.101801113349914!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1s35%201st%20Floor%2C%20First%20Floor%2C%204D%20Square%2C%20Visat%2C%20to%2C%20Sarkhej%20-%20Gandhinagar%20Hwy%2C%20Motera%2C%20Ahmedabad%2C%20Gujarat%20380005!5e0!3m2!1sen!2sin!4v1630659313955!5m2!1sen!2sin" width="100%" height="300" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                        </div>
                    </div>
                </div>

                <div class="col-lg-2 col-md-6">
                    <div class="footer-widget widget" style="margin-left: 80px;">
                        <h4 class="widget-title">Quick Link</h4>
                        <ul class="widget_nav_menu">
                            <li><a href="{{route('AboutUsDemo')}}">About Us</a></li>
                            <li><a href="{{ route('blogAll') }}">Blog</a></li>
                            <li><a href="{{route('Careers')}}">Career</a></li>
                            <li><a href="{{route('gallery_design')}}">Gallery</a></li>
                            <li><a href="{{route('NewReview')}}">Review</a></li>
                            <li><a href="{{ route('FAQ') }}">FAQ</a></li>
                            <li><a href="{{route('terms-condition')}}" target="_blank">T&C</a></li>
                            <li><a href="{{route('privacy-policy')}}">Privacy Policy</a></li>
                        </ul>
                    </div>
                </div>
            <!--     <div class="col-lg-4 col-md-6">
                    <div class="footer-widget widget">
                        <h4 class="widget-title">Gallery</h4>
                        <ul class="widget-instagram-feed">
                            <li><a href="#"><img src="{{ url('assets/img/instagram/1.png') }}" alt="img"></a></li>
                            <li><a href="#"><img src="{{ url('assets/img/instagram/2.png') }}" alt="img"></a></li>
                            <li><a href="#"><img src="{{ url('assets/img/instagram/3.png') }}" alt="img"></a></li>
                            <li><a href="#"><img src="{{ url('assets/img/instagram/4.png') }}" alt="img"></a></li>
                            <li><a href="#"><img src="{{ url('assets/img/instagram/5.png') }}" alt="img"></a></li>
                            <li><a href="#"><img src="{{ url('assets/img/instagram/6.png') }}" alt="img"></a></li>
                            <li><a href="#"><img src="{{ url('assets/img/instagram/1.png') }}" alt="img"></a></li>
                            <li><a href="#"><img src="{{ url('assets/img/instagram/2.png') }}" alt="img"></a></li>
                            <li><a href="#"><img src="{{ url('assets/img/instagram/3.png') }}" alt="img"></a></li>
                            <li><a href="#"><img src="{{ url('assets/img/instagram/4.png') }}" alt="img"></a></li>
                        </ul>
                    </div>
                </div> -->
                
            </div>
        </div>
        <!-- <div class="copyright-inner">
            <div class="copyright-text">
                &copy; Viaje 2019 All rights reserved. Powered with <a href="http://zwin.io/" target="_blank"><i class="fa fa-heart"></i> </a> by <a href="http://zwin.io/" target="_blank"><span>Zwin.</span></a>
            </div>
        </div> -->
        <div class="copyright-inner">
        <div class="container">
            <div class="row">
                <div class="copyright-text col-lg-8">
                    &copy; Jag Joyu 2021 All rights reserved
                </div>
            </div>
        </div>
    </div>
    </footer>
    <!-- footer area end -->

    <!-- back to top area start -->
    <div class="back-to-top">
        <span class="back-top"><i class="fa fa-angle-up"></i></span>
    </div>
    <!-- back to top area end -->