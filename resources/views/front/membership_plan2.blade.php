<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Membership Plan</title>
    <!-- favicon -->
    <link rel=icon href="assets/img/favicon.png" sizes="20x20" type="image/png">

    <!-- Additional plugin css -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/animate.css">
    <link rel="stylesheet" href="assets/css/magnific-popup.css">
    <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="assets/css/slick.css">
    <link rel="stylesheet" href="assets/css/swiper.min.css">
    <link rel="stylesheet" href="assets/css/nice-select.css">
    <!-- icons -->
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/themify-icons.css">
    <link rel="stylesheet" href="assets/css/line-awesome.min.css">
    <!-- main css -->
    <link rel="stylesheet" href="assets/css/style.css">
    <!-- responsive css -->
    <link rel="stylesheet" href="assets/css/responsive.css">
    <style>   
        .client-slider-two .swiper-slide-active .client-slider-item {
            background-color: #000c2d;
            border-radius: 35px;
            margin-left: 0px;
            margin-right: 0px;
        }

        .details .single-intro .intro-title .intro-count {
            font-size: 50px;
            font-weight: 700;
        }

        .single-intro {
            box-shadow: rgb(17 12 46 / 15%) 0px 5px 30px 0px;
            border-radius: 35px;
            padding: 30px 10px;
        }

        .slider-box {
            width: 30%;
            padding: 10px;
            cursor: pointer;
            transform: scale(0.9);
            border-radius: 35px;
            position: relative;
            height: 310px;
        }

        .single-intro .intro-title .intro-cat {
            margin-left: -12px;
            margin-top: 4px;
        }

        .slider-box span {
            font-weight: 500;
            color: #071c55;
        }

        .box-slider-position {
            position: relative;
            overflow: hidden;
        }

        .slider-box-active p {
            color: lightgray;
        }

        .slider-box-active span {
            color: white;
            font-size: 20px;
        }

        .slider-box-active {
            background-color: #000c2d;
            transform: scale(1);
            padding: 5px;
            box-shadow: rgb(17 12 46 / 15%) 0px 5px 30px 0px;
            width: 40%;
            /* z-index: 999; */
            height: 410px;
        }

        .slider-box-active .single-intro .intro-title .intro-count {
            font-size: 50px;
            color: #f3941e;
        }

        .single-intro .intro-title .intro-count {
            font-size: 30px;
        }

        .single-intro .intro-title .intro-cat {
            background: transparent;
            font-family: var(--body-font);
        }

        .slider-box-active .single-intro .intro-title a {
            color: white !important;
        }

        .slider-box .btn {
            font-size: 12px;
            height: 40px;
            line-height: 40px;
            padding: 0 22px;
        }

        .slider-box-active .btn {
            font-size: 14px;
            height: 50px;
            line-height: 50px;
            padding: 0 22px;
        }

        .margin-space {
            margin-top: 100px;
            margin-bottom: 100px;
        }

        .single-intro p {
            padding-top: 10px !important;
        }

        .section-title h2 {
            color: #f3941e !important;
        }

        .section-title span {
            color: #071c55 !important;
        }

        .section-title p {
            color: #666 !important;
        }

        #ui-datepicker-div {
            display: none;
        }

        .mg-bottom-92 {
            margin-bottom: -30px;
        }

        .breadcrumb-area {
            padding: 130px 0 50px;
        }

        @media only screen and (max-width: 1680px) {
            .container-bg {
                padding: 31px 0 31px 0;
                /* margin: 0 30px; */
            }
        }

        @media only screen and (max-width: 1280px) {
            .mg-top--70 {
                margin-top: 0px !important;
            }
        }

        @media only screen and (min-width: 992px) {

            .slider-box:nth-child(1).slider-box-active+.slider-box {
                left: -40%;
            }

            .slider-box:nth-child(1).slider-box-active {
                left: 30%;
            }

            .slider-box:nth-child(3).slider-box-active {
                left: -30%;
            }

            .slider-box.alignment {
                left: 40%;
            }

            .slider-box-active {
                margin-bottom: 30px;
            }  

            .margin-space {
                margin-top: 100px;
                margin-bottom: 75px;
            }
        } 
        
        @media only screen and (max-width: 991px) {
            .slider-box {
                width: 100%;
                height: auto;
            }

            .margin-space {
                margin-top: 40px;
                margin-bottom: 0px;
            }

            .slider-box-active {
                background-color: #000c2d;
                transform: scale(0.9) !important;
                padding: 5px;
                box-shadow: rgb(17 12 46 / 15%) 0px 5px 30px 0px;
                width: 100%;
                /* z-index: 999; */
                height: 400px;
            }
        }

        @media only screen and (min-width: 576px) and (max-width:991px) {
            .breadcrumb-area {
                padding: 160px 0 50px;
            }
        }
    </style>
</head>
 
<body>
    <!-- preloader area start -->
    <div class="preloader" id="preloader">
        <div class="preloader-inner">
            <div class="spinner">
                <div class="dot1"></div>
                <div class="dot2"></div>
            </div>
        </div>
    </div>
    <!-- preloader area end -->
    <!-- search popup start -->
    <div class="body-overlay" id="body-overlay"></div>
    <div class="search-popup" id="search-popup">
        <form action="index.html" class="search-form">
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Search.....">
            </div>
            <button type="submit" class="submit-btn"><i class="fa fa-search"></i></button>
        </form>
    </div>
    <!-- search popup End -->
    <!-- navbar area start -->
    <nav class="navbar navbar-area navbar-expand-lg nav-style-02">
        <div class="container nav-container">
            <div class="responsive-mobile-menu">
                <div class="mobile-logo">
                    <a href="https://jagjoyu.xsquarestaging.in">
                        <img src="https://jagjoyu.xsquarestaging.in/public/images/jj-logo.png" alt="logo">
                    </a>
                </div>
                <button class="navbar-toggler float-right" type="button" data-toggle="collapse"
                    data-target="#tp_main_menu" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggle-icon">
                        <span class="line"></span>
                        <span class="line"></span>
                        <span class="line"></span>
                    </span>
                </button>
                <div class="nav-right-content">
                    <ul class="pl-0">
                        <li class="notification">
                            <a class="" href="https://jagjoyu.xsquarestaging.in/login">
                                <i class="fa fa-user-o fa-lg"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="collapse navbar-collapse" id="tp_main_menu">
                <div class="logo-wrapper desktop-logo">
                    <a href="https://jagjoyu.xsquarestaging.in" class="main-logo">
                        <img src="https://jagjoyu.xsquarestaging.in/public/images/jj-logo.png" alt="logo">
                    </a>
                    <a href="https://jagjoyu.xsquarestaging.in" class="sticky-logo">
                        <img src="https://jagjoyu.xsquarestaging.in/public/images/jj-logo.png" alt="logo">
                    </a>
                </div>
                <ul class="dropdown-menu-btn">
                    <li class="line"></li>
                    <li class="line"></li>
                    <li class="line"></li>
                </ul>
                <ul class="navbar-nav">
                    <li class="tp-lang d-lg-none">
                        <a href="https://jagjoyu.xsquarestaging.in/membership-plans">Membership Plan</a>
                    </li>
                    <li class="search d-lg-none">
                        <a href="https://jagjoyu.xsquarestaging.in/askforcall">Ask for call</a>
                    </li>

                    <li class="menu-item">
                        <a href="https://jagjoyu.xsquarestaging.in">Home</a>
                    </li>
                    <li class="menu-item">
                        <a href="#">Events</a>
                    </li>
                    <li class="menu-item">
                        <a href="https://jagjoyu.xsquarestaging.in/gallery">Gallery</a>
                    </li>
                </ul>
            </div>
            <div class="nav-right-content">
                <ul>
                    <li class="tp-lang">
                        <a href="https://jagjoyu.xsquarestaging.in/membership-plans">Membership Plan</a>
                    </li>
                    <li class="">
                        <a href="https://jagjoyu.xsquarestaging.in/askforcall">Ask for call</a>
                    </li>
                    <li class="notification">
                        <a class="" href="https://jagjoyu.xsquarestaging.in/login">
                            <i class="fa fa-user-o fa-lg"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- breadcrumb area start -->
    <div class="breadcrumb-area jarallax" style="background-image: none; z-index: 0;"
        data-jarallax-original-styles="background-image:url(assets/img/bg/1.png);">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb-inner">
                        <div class="row justify-content-center">
                            <div class="col-xl-6 col-lg-8">
                                <div class="section-title mb-2 text-center style-two">
                                    <h2 class="title">Membership&nbsp;<span>Plans</span></h2>
                                    <p>Choose a Plan that works best for you</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- breadcrumb area End -->
    <!--Start Membership Plan Slider Area--> 
    <div class="container margin-space">
        <div class="row box-slider-position">
            <div class="slider-box child1">
                <div class="single-intro">
                    <h4 class="intro-title text-center">
                        <span class="intro-count">Silver</span>
                        <a class="intro-cat" href="#">Plan</a>
                    </h4>
                    <p class="content s-animate-3 border border-top border-left-0 border-right-0 border-bottom-0 ">
                        Navagio Beach, or Shipwreck Beach, is an
                        exposed cove, sometimes
                        referred to as.</p>
                    <div class="row">
                        <div class="col-6 text-center">
                            <p>1 Years</p>
                            <span>Rs.12000</span>
                        </div>
                        <div class="col-6 text-center">
                            <p>2 Years</p>
                            <span>Rs.43</span>
                        </div>
                        <div class="col-6 text-center">
                            <p>2 Years</p>
                            <span>Rs.2250000</span>
                        </div>
                        <div class="col-6 text-center">
                            <p>3 Years</p>
                            <span>Rs.43</span>
                        </div>
                    </div>
                    <div class="text-center mt-4">
                        <a class="btn btn-yellow rounded" href="https://jagjoyu.xsquarestaging.in/login">Know More</a>
                    </div>
                </div>
            </div>
            <div class="slider-box child2 slider-box-active">
                <div class="single-intro wow">
                    <h4 class="intro-title text-center">
                        <span class="intro-count">Gold</span>
                        <a class="intro-cat" href="#">Plan</a>
                    </h4>
                    <p class="content s-animate-3 border border-top border-left-0 border-right-0 border-bottom-0 ">
                        Navagio Beach, or Shipwreck Beach, is an
                        exposed cove, sometimes
                        referred to as.</p>
                    <div class="row">
                        <div class="col-6 text-center">
                            <p>2 Years</p>
                            <span>Rs.1200000</span>
                        </div>
                        <div class="col-6 text-center">
                            <p>3 Years</p>
                            <span>Rs.15000</span>
                        </div>
                        <div class="col-6 text-center">
                            <p>4 Years</p>
                            <span>Rs.240000</span>
                        </div>
                        <div class="col-6 text-center">
                            <p>5 Years</p>
                            <span>Rs.8350</span>
                        </div>
                    </div>
                    <div class="text-center mt-4">
                        <a class="btn btn-yellow rounded" href="https://jagjoyu.xsquarestaging.in/login">Know More</a>
                    </div>
                </div>
            </div>
            <div class="slider-box child3">
                <div class="single-intro wow">
                    <h4 class="intro-title text-center">
                        <span class="intro-count">Platinum</span>
                        <a class="intro-cat" href="#">Plan</a>
                    </h4>
                    <p class="content s-animate-3 border border-top border-left-0 border-right-0 border-bottom-0 ">
                        Navagio Beach, or Shipwreck Beach, is an
                        exposed cove, sometimes
                        referred to as.</p>
                    <div class="row">
                        <div class="col-6 text-center">
                            <p>2 Years</p>
                            <span>Rs.2250000</span>
                        </div>
                        <div class="col-6 text-center">
                            <p>3 Years</p>
                            <span>Rs.6500</span>
                        </div>
                        <div class="col-6 text-center">
                            <p>4 Years</p> 
                            <span>Rs.7500</span>
                        </div>
                        <div class="col-6 text-center">
                            <p>5 Years</p>
                            <span>Rs.8500</span>
                        </div>
                    </div> 

                    <div class="text-center mt-4">
                        <a class="btn btn-yellow rounded" href="https://jagjoyu.xsquarestaging.in/login">Know More</a>
                    </div>
                </div>
            </div>
        </div>
    </div> 
    <!--End Membership Plan Slider Area-->

    <!-- footer area start -->
    <footer class="footer-area" 
        style="background-image: url(https://jagjoyu.xsquarestaging.in/public/front//img/bg/2.png);">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="footer-widget widget">
                        <div class="about_us_widget">
                            <!-- <a href="index.html" class="footer-logo">
                                    <img src="https://jagjoyu.xsquarestaging.in/public/front//img/logo.png" alt="footer logo">
                                </a> -->
                            <h4 class="widget-title">Contact us</h4>
                            <ul class="widget_nav_menu">
                                <li><a href="#">Address</a></li>
                                <li><a href="https://jagjoyu.xsquarestaging.in/askforcall">Ask for a call
                                    </a></li>
                            </ul>
                            <ul class="social-icon mt-5">
                                <li>
                                    <a class="facebook" href="#" target="_blank"><i class="fa fa-facebook  "></i></a>
                                </li>
                                <li>
                                    <a class="twitter" href="#" target="_blank"><i class="fa fa-twitter  "></i></a>
                                </li>
                                <li>
                                    <a class="pinterest" href="#" target="_blank"><i class="fa fa-instagram"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div> 
                </div>
                <div class="col-lg-7 col-md-6">
                    <div class="footer-widget widget">
                        <div class="widget-contact">
                           
                            <iframe
                                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d26360909.888257876!2d-113.74875964478716!3d36.242299409623534!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x54eab584e432360b%3A0x1c3bb99243deb742!2sUnited%20States!5e0!3m2!1sen!2sin!4v1618391824916!5m2!1sen!2sin"
                                width="100%" height="300" style="border:0;" class="rounded" allowfullscreen=""
                                loading="lazy"></iframe>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-6 pl-lg-4">
                    <div class="footer-widget widget">
                        <h4 class="widget-title">Quick Link</h4>
                        <ul class="widget_nav_menu">
                            <li><a href="https://jagjoyu.xsquarestaging.in/about-us">About us</a></li>
                            <li><a href="https://jagjoyu.xsquarestaging.in/blog">Blogs</a></li>
                            <li><a href="#">Careers</a></li>
                            <li><a href="#">Events</a></li>
                            <li><a href="https://jagjoyu.xsquarestaging.in/gallery">Gallery</a></li>
                            <li><a href="https://jagjoyu.xsquarestaging.in/FAQ">FAQ</a></li>
                            <li><a href="https://jagjoyu.xsquarestaging.in/terms-condition">T&amp;C</a></li>
                        </ul>
                    </div>
                </div> 

            </div>
        </div>
        <div class="copyright-inner">
            <div class="container">
                <div class="row">
                    <div class="privacypolicy col-lg-2 text-lg-left">
                        <a href="https://jagjoyu.xsquarestaging.in/privacy-policy">Privacy Policy</a>
                    </div>
                    <div class="copyright-text col-lg-8">
                        © Jag Joyu 2021 All rights reserved. Powered with <a href="http://zwin.io/" target="_blank"><i
                                class="fa fa-heart"></i> </a> by <a href="http://zwin.io/"
                            target="_blank"><span>X-Square</span></a>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- footer area end -->

    <!-- back to top area start -->
    <div class="back-to-top">
        <span class="back-top"><i class="fa fa-angle-up"></i></span>
    </div>
    <!-- back to top area end -->

    <!-- Additional plugin js -->
    <script src="assets/js/jquery-2.2.4.min.js"></script>
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/jquery.magnific-popup.js"></script>
    <script src="assets/js/owl.carousel.min.js"></script>
    <script src="assets/js/wow.min.js"></script>
    <script src="assets/js/slick.js"></script>
    <script src="assets/js/waypoints.min.js"></script>
    <script src="assets/js/jquery.counterup.min.js"></script>
    <script src="assets/js/imagesloaded.pkgd.min.js"></script>
    <script src="assets/js/isotope.pkgd.min.js"></script>
    <script src="assets/js/swiper.min.js"></script>
    <script src="assets/js/jquery.nice-select.min.js"></script>
    <script src="assets/js/jquery-ui.min.js"></script>
    <script src="assets/js/jarallax.min.js"></script>
    <!-- main js -->
    <script src="assets/js/main.js"></script>

    <script> 
        $(document).ready(function () { 
            var selector = '.slider-box';

            $(selector).on('click', function () {
                $(selector).removeClass('slider-box-active');
                $(this).addClass('slider-box-active');
            });
        });

        $(".child3").click(function () { 
            $(".child2").addClass("alignment");
        });
        $(".child1, .child2").click(function () {
            $(".child2").removeClass("alignment");
        });
    </script> 
</body>

</html>  

