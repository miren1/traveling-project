@extends('front.new-layouts.master')
@section('title',$blog->title)
@section('description',$blog->seo_description)
@section('keywords',$blog->seo_keywords)
@section('content')
<!-- breadcrumb area start -->
<div class="breadcrumb-area jarallax" style="background-image:url(assets/img/bg/1.png);">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-inner">
                    <h1 class="page-title a" style="text-transform: uppercase;">{{$blog->title}}</h1>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb area End -->
<!-- blog area start -->
<div class="blog-details-area pd-top-120">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="single-blog mb-0">
                    <div class="single-blog-details">
                        <h3 class="title">{{$blog->title}}</h3>
                    </div>
                    <div class="thumb">
                        @if(isset($blog))
                            <img src="{{$url}}/{{$blog->thumbnail_image}}" alt="blog">
                        @else
                            <img src="assets/img/blog-details/1.png" alt="blog">
                        @endif
                    </div>
                    <div class="single-blog-details">
                        {!!$blog->content!!}
                    </div>
                </div>
                <div class="blog-list-area pd-top-10">
                    <div class="container">
                        <div class="row justify-content-center">
                            @if($ads)
                            @foreach($ads as $value) 
                                @foreach($value->allads as $ads_value) 
                                    @if($ads_value->position == 'between')
                                    
                                        @if($ads_value->ads_video_url)
                                        <div class="col-lg-4 col-md-6">
                                            <div class="single-blog">
                                                <a href="{{$ads_value->ads_video_url}}">
                                                    <div class="thumb">
                                                        <iframe width="420" height="315" src="{{$ads_value->ads_video_url}}" frameborder="0" allowfullscreen></iframe>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                        @endif
                                        @if($ads_value->ads_image)
                                        <div class="col-lg-4 col-md-6">
                                            <div class="single-blog">
                                                @if($ads_value->affiliate_url)
                                                <a href="{{$ads_value->affiliate_url}}" target="_blank">
                                                @else
                                                <a href="{{$url}}/{{$ads_value->ads_image}}" target="_blank">
                                                @endif
                                                    <div class="thumb">
                                                            <img class="w-100" src="{{$url}}/{{$ads_value->ads_image}}" alt="img">
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                        @endif
                                    @endif
                                @endforeach
                            @endforeach
                            @endif
                        </div>
                    </div>
                </div> 
                <div class="blog-list-area pd-top-10">
                    <div class="container">
                        <div class="row">
                            <div class="slider">
                                @if($ads)
                                    @foreach($ads as $key) 
                                        @foreach($key->allads as $ads_value) 
                                            @if($ads_value->position == 'footer')
                                                @if($ads_value->ads_image)
                                                    <div class="slide" style="margin-left: 90px;">
                                                        <div class="single-blog">
                                                            @if($ads_value->affiliate_url)
                                                            <a href="{{$ads_value->affiliate_url}}" target="_blank">
                                                            @else
                                                            <a href="{{$url}}/{{$ads_value->ads_image}}" target="_blank">
                                                            @endif    
                                                                
                                                                    <img style="height: 310px; width: 300px" src="{{$url}}/{{$ads_value->ads_image}}" alt="blog">
                                                                
                                                            </a>
                                                        </div>
                                                    </div>
                                                @endif
                                                @if($ads_value->ads_video_url)
                                                    <div class="slide" style="margin-left:70px;">
                                                        <div class="single-blog">
                                                            <a href="{{$ads_value->ads_video_url}}">
                                                                <div class="thumb">
                                                                    <iframe width="420" height="315" src="{{$ads_value->ads_video_url}}" frameborder="0" allowfullscreen></iframe>
                                                                </div>
                                                            </a>
                                                        </div>
                                                    </div>
                                                @endif
                                            @endif
                                        @endforeach
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                </div> 
            </div>
            <div class="col-lg-3">
                <aside class="sidebar-area sidebar-area-4" style="margin-top: 90px;">
                    @if($ads)
                        @foreach($ads as $key) 
                            @foreach($key->allads as $ads_value) 
                                @if($ads_value->position == 'right')
                                    
                                    @if($ads_value->ads_image)    
                                    <div class="widget_ads">
                                        @if($ads_value->affiliate_url)
                                        <a href="{{$ads_value->affiliate_url}}" target="_blank">
                                        @else
                                        <a href="{{$url}}/{{$ads_value->ads_image}}" target="_blank">
                                        @endif 
                                            <img class="w-100" src="{{$url}}/{{$ads_value->ads_image}}" alt="img">
                                        </a>
                                    </div> <br>
                                    @endif
                                    @if($ads_value->ads_video_url)
                                    <div class="widget_ads">
                                        <iframe width="420" height="315" src="{{$ads_value->ads_video_url}}" frameborder="0" allowfullscreen></iframe>
                                    </div> <br>
                                    @endif
                                @endif
                                @endforeach
                        @endforeach
                    @endif
                </aside>

            </div>    
        </div>
    </div>
</div>


<!-- blog area End -->
@endsection
@push('css')
<style type="text/css">
       .slider{
          width:1200px;
          text-align: center;
          color:white;
          .parent-slide{padding:15px;}
          img{display: block;margin:auto;}
        }
        .draggable{
            width: 1500px;
        }
</style>
@endpush
@push('script')
<script src="https://web.archive.org/web/20140208040316js_/http://mediaelementjs.com/js/mejs-2.13.2/mediaelement-and-player.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.slider').slick({
          autoplay:true,
          autoplaySpeed:1500,
          arrows:true,
          prevArrow:'<button type="button" class="slick-prev"></button>',
          nextArrow:'<button type="button" class="slick-next"></button>',
          centerMode:true,
          slidesToShow:3,
          slidesToScroll:1,
          responsive: [
            {
              breakpoint: 1024,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 2,
                adaptiveHeight: true,
              },
            },
            {
              breakpoint: 600,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
              },
            },
          ],
        });
    });
</script>
@endpush