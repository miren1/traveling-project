@extends('front.layouts.master')
@section('title','Membership Registration')
@push('style')
<link rel="stylesheet" href="{{url('/front')}}/assets/css/bootstrap-datepicker.min.css"/>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css"/>
<style type="text/css">
    @import url('https://fonts.googleapis.com/css?family=Roboto');

/*------------------------*/
input:focus,
button:focus,
.form-control:focus{
    outline: none;
    box-shadow: none;
}
/*.form-control:disabled, .form-control[readonly]{
    background-color: #fff;
}*/
/*----------step-wizard------------*/
.d-flex{
    display: flex;
}
.justify-content-center{
    justify-content: center;
}
.align-items-center{
    align-items: center;
}

/*---------signup-step-------------*/
.bg-color{
    background-color: #333;
}
.signup-step-container{
    padding: 150px 0px;
    padding-bottom: 60px;
}
    .wizard .nav-tabs {
        position: relative;
        margin-bottom: 0;
        border-bottom-color: transparent;
    }

    .wizard > div.wizard-inner {
    position: relative;
    margin-bottom: 50px;
    text-align: center;
    }

.connecting-line {
    height: 2px;
    background: #f3941e;
    position: absolute;
    width: 75%;
    margin: 0 auto;
    left: 0;
    right: 0;
    top: 15px;
    z-index: 1;
}

.wizard .nav-tabs > li.active > a, .wizard .nav-tabs > li.active > a:hover, .wizard .nav-tabs > li.active > a:focus {
    color: #555555;
    cursor: default;
    border: 0;
    border-bottom-color: transparent;
}

span.round-tab {
    width: 30px;
    height: 30px;
    line-height: 30px;
    display: inline-block;
    border-radius: 50%;
    background: #fff;
    z-index: 2;
    position: absolute;
    left: 0;
    text-align: center;
    font-size: 16px;
    color: #0e214b;
    font-weight: 500;
    border: 1px solid #ddd;
}
span.round-tab i{
    color:#555555;
}
.wizard li.active span.round-tab {
    background: #f3941e !important;
    color: #fff;
    border-color: #f3941e !important;
}
.wizard li.active span.round-tab i{
    color: #5bc0de;
}
.wizard .nav-tabs > li.active > a i{
    color: #f3941e;
    font-size: 15px !important;
}

.wizard .nav-tabs > li {
    width: 25%;
}

.wizard li:after {
    content: " ";
    position: absolute;
    left: 46%;
    opacity: 0;
    margin: 0 auto;
    bottom: 0px;
    border: 5px solid transparent;
    border-bottom-color: red;
    transition: 0.1s ease-in-out;
}



.wizard .nav-tabs > li a {
    width: 30px;
    height: 30px;
    margin: 20px auto;
    border-radius: 100%;
    padding: 0;
    background-color: transparent;
    position: relative;
    top: 0;
}
.wizard .nav-tabs > li a i{
    position: absolute;
    top: -15px;
    font-style: normal;
    font-weight: 400;
    white-space: nowrap;
    left: 50%;
    transform: translate(-50%, -50%);
    font-size: 12px;
    font-weight: 700;
    color: #000;
}

    .wizard .nav-tabs > li a:hover {
        background: transparent;
    }

.wizard .tab-pane {
    position: relative;
    padding-top: 20px;
}

.next-btn:hover {
    background-color: #f3941e;
    color: white;
    }


.wizard h3 {
    margin-top: 0;
}
/*.prev-step,
.next-step{
    font-size: 13px;
    padding: 8px 24px;
    border: none;
    border-radius: 4px;
    margin-top: 30px;
}*/
/*.next-step{
    background-color: #f3941e;
}*/
.skip-btn{
    background-color: #cec12d;
}
.step-head{
    font-size: 20px;
    text-align: center;
    font-weight: 500;
    margin-bottom: 20px;
}
.term-check{
    font-size: 14px;
    font-weight: 400;
}
.custom-file {
    position: relative;
    display: inline-block;
    width: 100%;
    height: 40px;
    margin-bottom: 0;
}
.custom-file-input {
    position: relative;
    z-index: 2;
    width: 100%;
    height: 40px;
    margin: 0;
    opacity: 0;
}
.custom-file-label {
    position: absolute;
    top: 0;
    right: 0;
    left: 0;
    z-index: 1;
    height: 40px;
    padding: .375rem .75rem;
    font-weight: 400;
    line-height: 2;
    color: #495057;
    background-color: #fff;
    border: 1px solid #ced4da;
    border-radius: .25rem;
}
.custom-file-label::after {
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    z-index: 3;
    display: block;
    height: 38px;
    padding: .375rem .75rem;
    line-height: 2;
    color: #495057;
    content: "Browse";
    background-color: #e9ecef;
    border-left: inherit;
    border-radius: 0 .25rem .25rem 0;
}
.footer-link{
    margin-top: 30px;
}
.all-info-container{

}
.list-content{
    margin-bottom: 10px;
}
.list-content a{
    padding: 10px 15px;
    width: 100%;
    display: inline-block;
    background-color: #f5f5f5;
    position: relative;
    color: #565656;
    font-weight: 400;
    border-radius: 4px;
}
.list-content a[aria-expanded="true"] i{
    transform: rotate(180deg);
}
.list-content a i{
    text-align: right;
    position: absolute;
    top: 15px;
    right: 10px;
    transition: 0.5s;
}
/*.form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
    background-color: #fdfdfd;
}*/
.list-box{
    padding: 10px;
}
.signup-logo-header .logo_area{
    width: 200px;
}
.signup-logo-header .nav > li{
    padding: 0;
}
.signup-logo-header .header-flex{
    display: flex;
    justify-content: center;
    align-items: center;
}
.list-inline li{
    display: inline-block;
}
.pull-right{
    float: right;
}

.icon-plus {
    position: relative;
    }

    .icon-plus button {
    position: absolute;
    left: 16px;
    top: 36px;
    height: 38px;
    width: 35px;
    background-color: #071c55;
    border: none;
    color: white;
    border-radius: 4px;
    transition: 0.7s;
    }

    .icon-plus button:hover {
    background-color: #f3941e;
    color: white;
    }

    .icon-minus
    {
    width: 30px;
    height: 37px;
    border-radius: 1px 5px 5px 0px;
    }

/*-----------custom-checkbox-----------*/
/*----------Custom-Checkbox---------*/
input[type="checkbox"]{
    position: relative;
    display: inline-block;
    margin-right: 5px;
}
input[type="checkbox"]::before,
input[type="checkbox"]::after {
    position: absolute;
    content: "";
    display: inline-block;   
}
input[type="checkbox"]::before{
    height: 16px;
    width: 16px;
    border: 1px solid #999;
    left: 0px;
    top: 0px;
    background-color: #fff;
    border-radius: 2px;
}
input[type="checkbox"]::after{
    height: 5px;
    width: 9px;
    left: 4px;
    top: 4px;
}
input[type="checkbox"]:checked::after{
    content: "";
    border-left: 1px solid #fff;
    border-bottom: 1px solid #fff;
    transform: rotate(-45deg);
}
input[type="checkbox"]:checked::before{
    background-color: #18ba60;
    border-color: #18ba60;
}

@media (max-width: 767px){
    .sign-content h3{
        font-size: 40px;
    }
    .wizard .nav-tabs > li a i{
        display: none;
    }
    .signup-logo-header .navbar-toggle{
        margin: 0;
        margin-top: 8px;
    }
    .signup-logo-header .logo_area{
        margin-top: 0;
    }
    .signup-logo-header .header-flex{
        display: block;
    }
}

</style>
@endpush
@section('content')

<!-- breadcrumb area start -->
<div class="breadcrumb-area jarallax" style="background-image:url(/assets/img/bg/1.png); z-index: 0;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-inner">
                    <div class="row justify-content-center">
                        <div class="col-xl-6 col-lg-8">
                            <div class="section-title mb-2 text-center style-two">
                                <h2 class="title">Mandate <span>Registration</span></h2>
                                <p>Press Confirm button to register</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb area End -->
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="tour-details-wrap">
                <div class="package-included-area"> 
                    <div class="package-included-location">
                        <div class="section-title text-lg-center text-left my-3">
                            <!-- <h2 class="title"><label>Member&nbsp;</label>Details</h2> -->
                        </div>
                    </div> 
                        <div class="tp-form-wrap bg-gray tp-form-wrap-one">
                        
                            <div class="single-intro bullet-icon style-two"> 
                                <h4 class="intro-title text-center">Mandate Registration</h4>
                                <hr><br>
                                <form action="#" id="topupform" method="post" class="tp-form-wrap bg-gray tp-form-wrap-one p-3">
                                    @csrf
                               <div class="row">
                                    <div class="col-md-4">
                                        <label class="mt-md-3 mb-md-3 mt-3">Select EMI Option</label>
                                        <select name="emi" id="emi" onchange="generate_token()" class="form-control" required="" data-bv-notempty-message="The EMI option is required">
                                            <option value="null" selected>Select EMI type</option>
                                            <option value="monthly">Monthly</option>
                                            <option value="quarterly">Quarterly</option>
                                            <option value="half-yearly">Half Yearly</option>
                                            <option value="annual">Annual</option>
                                        </select>
                                        <span style="display: none; color:red;" id="error">Please Select EMI option</span>
                                    </div>
                                    <div class="col-md-4">
                                        <label class="mt-md-3 mb-md-3 mt-3">EMI Amount</label>
                                        <input class="form-control" id="emi_amount" type="text" readonly value="">
                                    </div>
                                    <div class="col-md-4">
                                        <label class="mt-md-3 mb-md-3 mt-3">Total Plan Amount</label>
                                        <input class="form-control" type="text" readonly value="{{$member_plan->fixed_amt}}">
                                    </div>
                                    <div class="col-md-4">
                                        <label class="mt-md-3 mb-md-3 mt-3">Plan Type</label>
                                        <input class="form-control" type="text" readonly value="{{$member_plan->type}}">
                                    </div>
                                    <div class="col-md-4">
                                        <label class="mt-md-3 mb-md-3 mt-3">Plan Year</label>
                                        <input class="form-control" type="text" readonly value="{{$member_plan->years}}">
                                    </div>
                                    <div class="col-md-4">
                                        <label class="mt-md-3 mb-md-3 mt-3">EMI Defaulter Charges</label>
                                        <input class="form-control" type="text" readonly value="{{$member_plan->emi_dafulter_charges}}">
                                    </div>
                                    <div class="col-md-4">
                                        <label class="mt-md-3 mb-md-3 mt-3">Debit Start Date</label>
                                        <input type="text" disabled class="form-control" name="debit_start_date" id="debit_start_date" required data-bv-notempty="true" data-bv-notempty-message="Please Select date">
                                    </div>  
                                    <!-- <div class="col-md-4">
                                        <label class="mt-md-3 mb-md-3 mt-3">Mobile Number</label>
                                        <input class="form-control" name="primary_contact" id="primary_contact" type="text">
                                    </div>
                                    <div class="col-md-4">
                                        <label class="mt-md-3 mb-md-3 mt-3">Email</label>
                                        <input class="form-control" name="email" id="email" type="text">
                                    </div> -->
                                </div>
                            </form>
                            </div>
                            <div class="col-md-12 bg-light text-right">
                                <button type="button" class="btn btn-primary confirm" id="btnSubmit" disabled="">Confirm</button>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@push('script')
<!-- <script src="//code.jquery.com/jquery-1.10.2.min.js"></script>
 <script src="https://code.jquery.com/jquery-3.4.0.slim.min.js"></script> 
<script src="{{url('/front')}}/assets/js/sha.js"></script>
 -->
<script src="https://www.paynimo.com/Paynimocheckout/client/lib/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript" src="https://www.paynimo.com/Paynimocheckout/server/lib/checkout.js"></script>
<script src="{{url('/front')}}/assets/js/sha.js"></script>
<script src="//cdn.rawgit.com/placemarker/jQuery-MD5/master/jquery.md5.js"></script>
<!-- <script src="{{url('/front')}}/assets/js/bootstrap-datepicker.min.js"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js"></script>
<script type="text/javascript">


$(document).ready( function() {
    $('#topupform').bootstrapValidator();
    var debit_start_date = $("#debit_start_date").datepicker("setDate", new Date());

});


function selectEMI(){
    var emi = $('#emi').val();

        if(emi == 'null'){
            $("#error").show();
            //$('#btnSubmit').prop('disabled', true);
            $(".confirm").attr("disabled", true);
        }else{
            $("#error").hide();
            $('#btnSubmit"]').removeAttr('disabled', false);
        }

    
}

function generate_token(){

    var emi = $('#emi').val();

    if(emi == 'null'){

        //console.log(emi);
            $("#error").show();
            $('#btnSubmit').prop('disabled', true);
        }else{
            //console.log(emi);
            $("#error").hide();
            $('#btnSubmit').prop('disabled', false);
        }

    var start_debit_date = $('#debit_start_date').val();
    
    var date = new Date(start_debit_date);

    var newdate = new Date(date);
    
    var dd = newdate.getDate();

    //var mm = newdate.getMonth() + 1;
    var mm = ("0" + (newdate.getMonth() + 1)).slice(-2);
    var y = newdate.getFullYear() + {{$member_plan->years}};

    var end_Date = dd + '-' + mm + '-' + y;

    var dd_s = newdate.getDate();
    var mm_s = ("0" + (newdate.getMonth() + 1)).slice(-2);
    var y_s = newdate.getFullYear();

    var start_date = dd_s + '-' + mm_s + '-' + y_s;
    
    if(emi  == 'monthly'){
        var totalamount = {{$member_plan->monthly_emi}};
        $('#emi_amount').val(totalamount);
    }else if(emi  == 'quarterly'){
        var totalamount = {{$member_plan->quarterly_emi}};
        $('#emi_amount').val(totalamount);
    }else if(emi  == 'half-yearly'){
        var totalamount = {{$member_plan->half_yearly_emi}};
        $('#emi_amount').val(totalamount);
    }else if(emi  == 'annual'){
        var totalamount = {{$member_plan->annual_emi}};
        $('#emi_amount').val(totalamount);
    }else{
        var totalamount = 'null';
        $('#emi_amount').val(totalamount);
    }
    //alert(totalamount);
    $('#total_amount').val(totalamount);

    var fix_amount = {{$member_plan->fixed_amt}};

    var primary_contact = $('#primary_contact').val();
    var email = $('#email').val();

    let consumer_Id = Math.random().toString(36).substring(5);
    //alert(consumer_Id);
    var merchantId = 'L658345';
    var txnId = Date.now();
    var totalamount = totalamount;
    var consumerId = consumer_Id;
    //var consumerId = 'c200024';
    var consumerMobileNo = '';
    var consumerEmailId = '';
    var debitStartDate = start_date;
    //var debitStartDate = '09-08-2021';
    var debitEndDate = end_Date;
   // var debitEndDate = '19-08-2021';
    var maxAmount = totalamount;
    var amountType = 'M';
    var frequency = 'ADHO';
    var salt = '9114050656MYYEJX';
   // alert(totalamount);
    var membership_id = btoa({{$member_request_details->id}});
    var url = window.location.origin+'/mandate_response/'+membership_id+'/'+merchantId+'/'+txnId+'/'+consumerId;

    var token = `${merchantId}|${txnId}|${totalamount}||${consumerId}|${consumerMobileNo}|${consumerEmailId}|${debitStartDate}|${debitEndDate}|${maxAmount}|${amountType}|${frequency}|||||${salt}`; 

    var token = $.md5(token);
    //console.log(token);
    function handleResponse(res) {

        if (typeof res != 'undefined' && typeof res.paymentMethod != 'undefined' && typeof res.paymentMethod.paymentTransaction != 'undefined' && typeof res.paymentMethod.paymentTransaction.statusCode != 'undefined' && res.paymentMethod.paymentTransaction.statusCode == '0300') {
            // success block
        } else if (typeof res != 'undefined' && typeof res.paymentMethod != 'undefined' && typeof res.paymentMethod.paymentTransaction != 'undefined' && typeof res.paymentMethod.paymentTransaction.statusCode != 'undefined' && res.paymentMethod.paymentTransaction.statusCode == '0398') {
            // initiated block
        } else {
            // error block
        }
    };

    $(document).off('click', '#btnSubmit').on('click', '#btnSubmit', function(e) {

        e.preventDefault();

        var configJson = {
            'tarCall': false,
            'features': {
                'showPGResponseMsg': true,
                'enableAbortResponse': true,
                'enableNewWindowFlow': true,
                'enableExpressPay':true,
                'siDetailsAtMerchantEnd':true,
                'enableSI':true
            },
            'consumerData': {
                'deviceId': 'WEBMD5', //possible values 'WEBSH1', 'WEBSH2' and 'WEBMD5'
                'token': token,
                'returnUrl': url,
                'responseHandler': handleResponse,
                'paymentMode': 'netBanking',
                'merchantLogoUrl': "{{url('/front')}}/assets/img/mandate_logo/JJ.png",
                'merchantId': merchantId,
                'currency': 'INR',
                'consumerId': consumerId,
                'consumerMobileNo': consumerMobileNo,
                'consumerEmailId': consumerEmailId,
                'txnId': txnId,
                'bankCode': '470',
                'items': [{ 'itemId' : 'FIRST', 'amount' : totalamount, 'comAmt':'0'}],
                                                                                        
                'customStyle': {
                    'PRIMARY_COLOR_CODE': '#3977b7',
                    'SECONDARY_COLOR_CODE': '#FFFFFF',
                    'BUTTON_COLOR_CODE_1': '#1969bb',
                    'BUTTON_COLOR_CODE_2': '#FFFFFF'
                },                                                                   
                'accountType': 'Saving',
                'debitStartDate': debitStartDate,
                'debitEndDate': debitEndDate,
                'maxAmount': maxAmount,
                'amountType': amountType,
                'frequency': frequency
            }
        };

        $.pnCheckout(configJson);
        if(configJson.features.enableNewWindowFlow){
            
            pnCheckoutShared.openNewWindow();
            
        }
    });
   
}

</script>
@endpush