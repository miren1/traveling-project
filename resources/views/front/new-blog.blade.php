@extends('front.new-layouts.master')
@section('title',"Jag Joyu Blog")
@section('description',"Blog Description of Jag Joyu")
@section('keywords',"jagjoyu, travel, travel company in ahmedabad")
@section('content')
    <!-- navbar area end -->
    <!-- breadcrumb area start -->
    <div class="breadcrumb-area jarallax" style="background-image:url(assets/img/bg/1.png);">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb-inner">
                        <h1 class="page-title">Blogs</h1>
                        <ul class="page-list">
                            <li><a href="{{url('/')}}">Home</a></li>
                            <li>Blogs</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- breadcrumb area End -->

    <!-- blog area start -->
    <div class="blog-list-area pd-top-120">
        <div class="container">
            <div class="row justify-content-center">
                @if(isset($blog))
                    @foreach($blog as $key)
                    <div class="col-lg-4 col-md-6">
                        <div class="single-blog">
                            <a href="{{url('blog')}}/{{$key->slug}}">
                                <div class="thumb">
                                    <img src="{{$key->thumbnail_image}}" alt="blog" height="300px" width="450px">
                                </div>
                            </a>
                            <div class="single-blog-details">
                                <p class="date">{{Carbon::parse($key->created_at)->format('d-m-y')}}</p>
                                <h4 class="title">{{$key->title}}</h4>
                                <p class="content">{{$key->seo_description}}</p>
                                <a class="btn-read-more" href="{{url('blog')}}/{{$key->slug}}"><span>Read More<i class="la la-arrow-right"></i></span></a>
                            </div>
                        </div>
                    </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
    <!-- blog area End -->
@endsection