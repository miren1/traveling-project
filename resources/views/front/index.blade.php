@extends('front.layouts.master')
@section('title',"India's No. 1 - Travel Investment Membership Plans  - Jag Joyu")
@push('style')

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>


    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>


    <link href="{{ asset('admin/css/fontawesome-all.css') }}" rel="stylesheet" type='text/css'>
   

<link href="https://use.fontawesome.com/releases/v5.0.4/css/all.css" rel="stylesheet">
<link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel = "stylesheet">
    
    <link href="//fonts.googleapis.com/css?family=Poiret+One" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
<link rel="stylesheet"
    href="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css" />
<style>
    .holiday-plan-area .thumb img {
    height: 350px;
    }
    .package-margin{
        padding-top: 20px !important;
    }

    .video-popup-btn-package {
        position: absolute !important;
        top: 195% !important;
        left: 50% !important;
        margin-top: -53px !important;
        margin-left: -53px !important;
        border: 2px solid var(--main-color-one) !important;
        border-radius: 50% !important;
        padding: 6px !important;
    }
    .video-popup-btn-package .video-play-btn-package {
        height: 78px !important;
        width: 78px !important;
        background: var(--main-color-one) !important;
        display: inline-block !important;
        border-radius: 50% !important;
        position: relative !important;
        font-size: 26px !important;
        text-align: center !important;
        line-height: 80px !important;
        padding-left: 4px !important;
        color: #ffffff !important;
        -webkit-animation: ripple-white3 2.5s linear infinite !important;
        animation: ripple-white3 2.5s linear infinite !important;
    }

    @media only screen and (max-width: 600px) {
          .video-popup-btn-package {
            display: none;
        }
    }
    @media only screen and (max-width: 575px){
        .banner-slider .video-popup-btn {
            margin-top: 0;
            margin-left: 15px;
            left: 0;
            top: 0;
            margin-left: 70%;
        }
    }

    .video-popup-btn-package-footer {
        position: absolute !important;
        top: 90% !important;
        left: 50% !important;
        margin-top: -53px !important;
        margin-left: -53px !important;
        border: 2px solid var(--main-color-one) !important;
        border-radius: 50% !important;
        padding: 6px !important;
    }
    .video-popup-btn-package-footer .video-play-btn-package-footer {
        height: 65px !important;
        width: 65px !important;
        background: var(--main-color-one) !important;
        display: inline-block !important;
        border-radius: 50% !important;
        position: relative !important;
        font-size: 20px !important;
        text-align: center !important;
        line-height: 65px !important;
        padding-left: 6px !important;
        color: #ffffff !important;
        -webkit-animation: ripple-white3 2.5s linear infinite !important;
        animation: ripple-white3 2.5s linear infinite !important;
    }
    .ui-datepicker-month{
    color: #f7af38 !important;
}

.ui-datepicker-year{
    color: #f7af38 !important;
}

</style>
@endpush
@section('content')
@if(isset($sliders))
@include('front.layouts.banner')
@endif
<!--Start Search area Tab Here-->
<div class="container" style="display:block;">
    <div class="row">
        <div class="col-12 pt-5 custom_tab">
            <ul class="nav nav-tabs justify-content-center">
                <li class="nav-item">
                    <a class="nav-link active font-weight-bold" data-toggle="tab" href="#home">Flights</a>
                </li>
                <li class="nav-item font-weight-bold">
                    <a class="nav-link" data-toggle="tab" href="#menu1">Bus</a>
                </li>
                <li class="nav-item font-weight-bold">
                    <a class="nav-link" data-toggle="tab" href="#menu2">Trains</a>
                </li>
            </ul>
            <div class="tab-content">
                <!-- search area start -->
                <div id="home" class="container tab-pane active">
                    <div class="container">
                        <div class="tp-main-search">
							<form class="tp-form-wrap" id="flightbooking" action="{{ route('flight-availability') }}" method="POST" >
                            @csrf
                            <div class="row">
                                <div class="col-lg-2 col-md-3">
                                    <div class="tp-search-single-wrap">
                                        <input class="w-100" type="text" name="from" placeholder="Delhi" required="" data-bv-notempty-message="The From name is required">
                                        <i class="ti-location-pin"></i>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-3">
                                    <div class="tp-search-single-wrap">
                                        <input class="w-100" type="text" name="to" placeholder="Where From?" required="" data-bv-notempty-message="The To name is required">
                                        <i class="fa fa-dot-circle-o"></i>
                                    </div>
                                </div>
                                <div class="col-lg-1 col-md-1 order-lg-9">
                                    <div class="tp-search-single-wrap float-left w-100">
                                        <select class="select w-100" style="padding: 0 15px;" required="" data-bv-notempty-message="The Adult is required" name="adult">
                                            <option value="0">Adult</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                        </select>
                                        <!-- <i class="fa fa-plus-circle"></i> -->
                                    </div>
                                </div>
                                <div class="col-lg-1 col-md-1 order-lg-9">
                                    <div class="tp-search-single-wrap float-left w-100">
                                        <!-- <input class="w-100" type="text" name="child" placeholder="Child" style="padding: 0 15px;"> -->
                                         <select class="select w-100" style="padding: 0 15px;" required="" data-bv-notempty-message="The Child is required" name="child">
                                            <option value="">Child</option>
                                            <option value="0">0</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-2 order-lg-9">
                                    <div class="tp-search-single-wrap float-left w-100">
                                         <select class="select w-100" style="padding: 0 0px;" required="" data-bv-notempty-message="The Fare Types is required" name="fare_type">
                                            <option value="">Fare Types</option>
                                            <option value="AN">Normal Fare</option>
                                            <option value="AC">Coupon Fare</option>
                                            <option value="ACO">Corporate Fare</option>
                                            <option value="AR">Return Fare</option>
                                            <option value="AS">Sale Fare</option>
                                            <option value="AF">Family Fare</option>
                                            <option value="AFF">Flexi Fare</option>
                                            <option value="AT">Tactical Fare</option>
                                            <option value="AM">SME Fare</option>
                                            <option value="ASR">Special Round Trip</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-8 order-lg-6">
                                    <div class="tp-search-single-wrap float-left">
                                        <div class="tp-search-date tp-departing-date-wrap w-50 float-left">
                                            <input type="text" name="departing" id="departing" class="departing-date" placeholder="Departing" required="" data-bv-notempty-message="The Departing is required">

                                            <i class="fa fa-calendar-minus-o"></i> 
                                        </div>
                                        <div class="tp-search-date tp-returning-date-wrap w-50 float-left">
                                            <input type="text" name="returning" class="returning-date" id="returning" placeholder="Returning" >
                                            <img src="assets/img/icons/2.png" alt="icons">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-1 col-md-3 order-12">
                                    
                                    <button type="submit" class="btn btn-yellow"><i class="ti-search"></i></button>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- search area end -->
                <div id="menu1" class="container tab-pane fade"><br>
                    <div class="tp-main-search">
						<center><h3>Coming Soon</h3></center>
                        <div class="row" style="display:none;">
                            <div class="col-lg-3 col-md-4">
                                <div class="tp-search-single-wrap">
                                    <input class="w-100" type="text" placeholder="Bangladesh,Dhaka">
                                    <i class="ti-location-pin"></i>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-4">
                                <div class="tp-search-single-wrap">
                                    <input class="w-100" type="text" placeholder="Where From?">
                                    <i class="fa fa-dot-circle-o"></i>
                                </div>
                            </div>
                            
                            <div class="col-lg-3 col-md-8 order-lg-6">
                                <div class="tp-search-single-wrap float-left">
                                    <div class="tp-search-date tp-departing-date-wrap w-50 float-left">
                                        <input type="text"class="departing-date hasDatepicker" placeholder="Departing"
                                            id="dp1618317966176">
                                        <i class="fa fa-calendar-minus-o"></i>
                                    </div>
                                    <div class="tp-search-date tp-returning-date-wrap w-50 float-left">
                                        <input type="text" class="returning-date hasDatepicker" placeholder="Returning"
                                            id="dp1618317966177">
                                        <img src="{{url('/front')}}/assets/img/icons/2.png" alt="icons">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-4 order-12">
                                <button class="btn btn-yellow" ><i class="ti-search"></i> Search</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="menu2" class="container tab-pane fade"><br>
					<div class="tp-main-search">
                    <center><h3>Coming Soon</h3></center>
					</div>
                    <!-- <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque
                            laudantium, totam rem aperiam.</p> -->
                </div>
            </div>
        </div>
    </div>
</div>
<!--End Search area Tab Here-->

<!-- holiday plan area start -->
<div class="holiday-plan-area tp-holiday-plan-area mg-top-96" style="background-image: url({{url('/front')}}/assets/img/bg/8.png);">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-6 col-lg-9">
                <div class="section-title text-center">
                    <h2 class="title wow animated fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.1s">Tour Packages</h2>
                </div>
            </div>
        </div>
        <div class="row" id="tour-type"></div>
    </div>
</div>

@endsection
@push('script')
<script type="text/javascript"
    src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js"></script>
<script>
    $(document).ready(function() {
        $('#flightbooking').bootstrapValidator();

    });

    $(document).ready(function() {
       var inof= {};
       
       getData(inof);
    });

    $(function() {
            $( "#departing" ).datepicker({
            minDate: new Date(),
            dateFormat: 'dd/mm/yy',
            changeMonth: true,
            changeYear: true,
            yearRange: '-110:+29',
            language: "es",
        });
    });

    $(function() {
            $( "#returning" ).datepicker({
            minDate: new Date(),
            dateFormat: 'dd/mm/yy',
            changeMonth: true,
            changeYear: true,
            yearRange: '-110:+29',
            language: "es",
        });
    });

    

    function getData(array){
       $.ajax({
           url: "{{route('tour.getTourType')}}", 
           type: "POST",
           dataType: "json",
           contentType: "application/json; charset=utf-8",
           data: JSON.stringify(array),
           success: function (result) {
               console.log(result.length);
               if(result.length!=0){
                   var $html = '';
                   var $video_button = '';
                   $('#tour-type').html($html);
                   $.each(result, function(key, value) {
                       $html+='<div class="col-lg-4 col-sm-6">'+
                                '<a href="{{url("/packages")}}/'+value.slug+'">'+
                                '<div class="single-destinations-list style-two wow animated fadeInUp" data-wow-duration="0.4s"'+
                                ' data-wow-delay="0.1s">'+
                                '<div class="thumb">'+
                                        '<img src="{{url("uploads/plans_images")}}/'+value.photo+'" alt="'+value.name+'">'+
                                '</div>'+
                                '<div class="details">'+
                                    '<h4 class="title">'+value.name+'</h4>'+
                                '</div>'+
                                '</div></a></div>';
                        $html+= `<div class="single-upconing-card">
                                <div class="shadow" style="background-image: url({{url('demo/assets/img/tour/01.png')}})">
                                    <img src="{{url("uploads/plans_images")}}/`+value.photo+`" alt="`+value.name+`">
                                </div>
                                <div class="tp-price-meta">
                                    <h2>`+value.name+`</h2>
                                </div>
                            </div>`;
                   });
                   $('#tour-type').html($html);
               }else{
                $('#tour-type').html('');
               }
           },
           error: function (err) {
           }
        }); 
    }

</script>



@endpush