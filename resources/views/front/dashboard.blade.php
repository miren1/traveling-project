@extends('front.layouts.master')
@section('title','Dashboard')
@section('content')

    @if(isset($announcement->image))
         <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
            <!-- Modal content-->
                <div class="modal-content" style="width: 500px;height:500px;margin-top: 100px;">
                    <div class="modal-header" style="border-color: black;">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                            @if(isset($announcement->image))
                                <img src="{{$announcement->image}}" style="border-radius:10px; width:500px;margin-top: 35px;">
                            @endif           
                    @if(isset($announcement->content))
                    <div class="modal-footer" style="background-color:white;border-color:black;">
                        <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
                    <p>{!!$announcement->content!!}</p>
                     </div>
                    @endif
                </div>
            </div>
        </div>
    @endif
 <!--  @if(isset($announcement->image))
        <div id="mypopup" class="adspopup">
            <span class="button b-close">
                <span>X</span>
            </span>
            @if (pathinfo($announcement->image, PATHINFO_EXTENSION) == 'pdf')
                <embed
                    src="{{$announcement->image}}#toolbar=0&navpanes=0&scrollbar=0"
                    type="application/pdf"
                    frameBorder="0"
                    scrolling="auto"
                    height="100%"
                    width="100%"
                ></embed>
            @else
                @if(isset($announcement->image))
                    <img src="{{$announcement->image}}" width="100%" style="border-radius: 10px;">
                @endif
            @endif       
        </div>
        @endif -->
<!-- breadcrumb area start style-two-->
<!-- <div class="breadcrumb-area jarallax" style="background-image:url({{ url('/front/assets/img/bg/1.jpg')}});"> -->
<div class="breadcrumb-area jarallax">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-inner">
                    @if(Auth::guard('webagent')->user())
                        <h1 class="page-title">Welcome {{ Auth::guard('webagent')->user()->first_name}} {{Auth::guard('webagent')->user()->last_name }}</h1>
                    @elseif(Auth::guard('webmember')->user())
                        <h1 class="page-title">Welcome {{ Auth::guard('webmember')->user()->name}}</h1>
                    @elseif(Auth::guard('webcustomer')->user())
                        <h1 class="page-title">Welcome {{ Auth::guard('webcustomer')->user()->first_name}} {{Auth::guard('webcustomer')->user()->last_name }}</h1>
                    @endif

                    @if(Auth::guard('webagent')->user())
                    <ul class="page-list">
                        <li>Balance : {{ Auth::guard('webagent')->user()->wallet}}</li>
                        <button type="button" class="btn btn-yellow btn-sm" data-toggle="modal" data-target="#topupModel"
                        id="btnexampleModalLong">
                        Charge Wallet
                        </button>
                    </ul>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb area End -->
 
@include('front.layouts.newmenu')
 <!--Start User Profile Area-->
 <div class="user-profile-area pd-top-120 mb-5 ">
    <div class="container">
        <div class="row">
            <div class="col-xl-10 col-lg-12 mx-auto">
                <div class="row">
                    <div class="col-md-5 mx-auto" style="border-right: 2px solid #ccc !important;">
                        <form class="tp-form-wrap" id="profile"  action="{{url('dashboard/update_profile')}}" method="post" enctype="multipart/form-data">
                        @csrf
                            <div class="tp-img-upload">
                                <div class="tp-avatar-preview">
                                    <div id="tp_imagePreview" style="background-image: url({{$user->profile}});">
                                    </div>
                                </div>
                                <div class="tp-avatar-edit">
                                    <input type="hidden" name="id" id="id" value="@if(Auth::guard('webagent')->user()){{ Auth::guard('webagent')->user()->id}}@elseif(Auth::guard('webmember')->user()){{Auth::guard('webmember')->user()->id}}@else{{ Auth::guard('webcustomer')->user()->id}} @endif">

                                    @if(Auth::guard('webagent')->user())
                                        <input type="hidden" name="type" value="agent">
                                    @elseif(Auth::guard('webcustomer')->user())
                                        <input type="hidden" name="type" value="customer">
                                    @elseif(Auth::guard('webmember')->user())
                                        <input type="hidden" name="type" value="member">
                                    @endif

                                    <input type="file" name="profile" id="tp_imageUpload" accept="image/png, image/jpeg" required="" data-bv-notempty-message="Image is required" value="{{$user->profile}}">
                                    <label class="btn btn-transparent" for="tp_imageUpload">
                                        <i class="fa fa-picture-o"></i>Change Photo
                                    </label>
                                    <h4 class="name">
                                    @if(Auth::guard('webagent')->user())
                                        {{ Auth::guard('webagent')->user()->first_name}} {{Auth::guard('webagent')->user()->last_name }}
                                    @elseif(Auth::guard('webcustomer')->user())
                                         {{ Auth::guard('webcustomer')->user()->first_name}} {{Auth::guard('webcustomer')->user()->last_name }}
                                    @endif
                                    </h4>
                                </div>
                            </div>
                            <div class="form-group col-md-12 text-right">
                                <button type="submit" class="btn btn-yellow mt-3 text-center">Update</button>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-7 mx-auto">
                        <div class="tab-content user-tab-content">
                            <div class="tab-pane fade show active" id="tabs_1">
                                <div class="user-details">
                                    <!-- <h3 class="user-details-title">Profile</h3> -->
                                    
                                    <form class="tp-form-wrap" id="profile"  action="{{url('dashboard/update_profile')}}" method="post">
                                        @csrf
                                        <input type="hidden" id="id" name="id" value="@if(Auth::guard('webagent')->user()){{ Auth::guard('webagent')->user()->id}}@elseif(Auth::guard('webmember')->user()){{Auth::guard('webmember')->user()->id}}@else{{ Auth::guard('webcustomer')->user()->id}} @endif">

                                        <input type="hidden" id="role" name="role" value="@if(Auth::guard('webagent')->user()){{ Auth::guard('webagent')->user()->role}}@elseif(Auth::guard('webmember')->user()){{Auth::guard('webmember')->user()->role}}@else{{ Auth::guard('webcustomer')->user()->role}} @endif">
                                        <div class="row">
                                            @if(Auth::guard('webmember')->user())
                                            <div class="col-md-6">
                                                <label class="single-input-wrap style-two">
                                                    <span class="single-input-title">Name</span>
                                                    <input type="text" name="first_name" readonly value="{{ Auth::guard('webmember')->user()->name}}" required="" data-bv-notempty-message="The First Name is required">
                                                </label>
                                            </div>
                                           
                                            @else
                                            <div class="col-md-6">
                                                <label class="single-input-wrap style-two">
                                                    <span class="single-input-title">First Name</span>
                                                    <input type="text" name="first_name" readonly value="@if(Auth::guard('webagent')->user()){{ Auth::guard('webagent')->user()->first_name}}@elseif(Auth::guard('webmember')->user()){{Auth::guard('webmember')->user()->first_name}}@else{{ Auth::guard('webcustomer')->user()->first_name}}@endif" required="" data-bv-notempty-message="The First Name is required">
                                                </label>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="single-input-wrap style-two">
                                                    <span class="single-input-title">Last Name</span>
                                                    <input type="text" readonly name="last_name" value="@if(Auth::guard('webagent')->user()){{ Auth::guard('webagent')->user()->last_name}}@elseif(Auth::guard('webmember')->user()){{Auth::guard('webmember')->user()->last_name}}@else{{ Auth::guard('webcustomer')->user()->last_name}} @endif" required="" data-bv-notempty-message="The Last Name is required">
                                                </label>
                                            </div>
                                            @endif
                                            <div class="col-md-6">
                                                <label class="single-input-wrap style-two">
                                                    <span class="single-input-title">Country</span>
                                                    <input type="text" readonly value="India" required="" data-bv-notempty-message="The Country is required">
                                                </label>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="single-input-wrap style-two">
                                                    <span class="single-input-title">Contact</span>
                                                    <input type="text" readonly name="mobile" value="@if(Auth::guard('webagent')->user()){{ Auth::guard('webagent')->user()->contact_details}}@elseif(Auth::guard('webmember')->user()){{Auth::guard('webmember')->user()->primary_contact}}@else{{ Auth::guard('webcustomer')->user()->mobile_number}} @endif" readonly required="" data-bv-notempty-message="The Contact is required">
                                                </label>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="single-input-wrap style-two">
                                                    <span class="single-input-title">Email Address</span>
                                                    <input type="text" readonly name="email" value="@if(Auth::guard('webagent')->user()){{ Auth::guard('webagent')->user()->company_email}}@elseif(Auth::guard('webmember')->user()){{Auth::guard('webmember')->user()->email}}@else{{ Auth::guard('webcustomer')->user()->email}} @endif" required="" data-bv-notempty-message="The Email Address is required">
                                                </label>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="single-input-wrap style-two">
                                                    <span class="single-input-title">Other Phone</span>
                                                    <input type="text" readonly name="other_phone" value="@if(Auth::guard('webagent')->user()){{ Auth::guard('webagent')->user()->alternate_number}}@elseif(Auth::guard('webmember')->user()){{Auth::guard('webmember')->user()->secondary_contact}}@else{{ Auth::guard('webcustomer')->user()->mobile_number}} @endif" >
                                                </label>
                                            </div>
                                           
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--End User Profile Area-->
@endsection
@push('style')

<link rel="stylesheet"
    href="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css" />
@endpush
@push('css')
<style>
.b-close {
color: #fff;
background: #ff0000;
padding: 5px 10px;
position: absolute;
right: 0;
top: 0;
cursor: pointer;
}
#toolbar{
display: none;
}
.btn-outline-warning {
    color: #212529 !important;
    background-color: #ffc107 !important;
    border-color: #ffc107 !important;
}

.bg-dash-blue {
    color: white;
    background-color: #061847;
    /*height: 67px !important;*/
}
</style>
@endpush
@push('script')
<script src="https://www.ostraining.com/images/coding/bpopup/jquery.bpopup.min.js"></script>
<script type="text/javascript"
    src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js"></script>
<script>
// $( document ).ready(function() {
//     $('#mypopup').bPopup();
//     $('iframe #toolbar').css('display','none');

// }
 $(window).load(function(){        
   $('#myModal').modal('show');
    }); 
$(document).ready(function() {
    $('#mypopup').bPopup();
    $('#profile').bootstrapValidator();

});

$(document).ready(function() {
    $('#topupform').bootstrapValidator();

});
function checkamount(){
    var amount = $('#amount').val();
    
    if(amount == 0){
        $('#error').show();
        $("#addWallet").attr("disabled", true);
       
    }else{
        $('#error').hide();
    }
}
</script>
<!-- <script type="text/javascript">
    $(document).ready(function(){
        $('#mypopup').bpopup();
        $('iframe #toolbar').css('display','none');
    });
</script> -->
@endpush