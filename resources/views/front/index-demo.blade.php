@extends('front.new-layouts.master')
@section('title',"India's No. 1 - Travel Investment Membership Plans  - Jag Joyu")
@section('content')
@if(isset($announcement->all_user))
        @if(isset($announcement->image))
         <div id="myModal" class="modal fade" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
            <!-- Modal content-->
                <div class="modal-content modal-lg">
                    
                        
                    <div class="model-body">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        @if(isset($announcement->image))
                            <img src="{{$url}}/{{$announcement->image}}" class="img-fluid image-center" alt="Responsive image" style="border-radius:10px;object-fit: cover; display: block; margin: 0 auto;">
                        @endif           
                    </div>
                    @if(isset($announcement->content))
                    <div class="modal-footer">
                        <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
                    <p>{!!$announcement->content!!}</p>
                     </div>
                    @endif
                </div>
            </div>
        </div>
        @endif
    @endif


    <!-- banner area start -->
    <!-- <div class="banner-area">
        <div class="banner-slider">
            <div class="banner-slider-item banner-bg-1">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-8 col-lg-9 offset-xl-2 offset-lg-1">
                            <div class="row">
                                <div class="col-lg-9 col-sm-8">
                                    <div class="banner-inner">
                                        <p class="banner-cat s-animate-1">Hot Places</p>
                                        <h2 class="banner-title s-animate-2">Navagio <br> Beach</h2>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-sm-4">
                                    <div class="video-popup-btn s-animate-video">
                                        <a href="https://www.youtube.com/watch?v=c7XEhXZ_rsk" class="video-play-btn mfp-iframe"><i class="fa fa-play"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="banner-slider-item banner-bg-2">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-8 col-lg-9 offset-xl-2 offset-lg-1">
                            <div class="row">
                                <div class="col-lg-9 col-sm-8">
                                    <div class="banner-inner">
                                        <p class="banner-cat s-animate-1">Hot Places</p>
                                        <h2 class="banner-title s-animate-2">Mykonos <br> island</h2>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-sm-4">
                                    <div class="video-popup-btn s-animate-video">
                                        <a href="https://www.youtube.com/watch?v=c7XEhXZ_rsk" class="video-play-btn mfp-iframe"><i class="fa fa-play"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="banner-slider-item banner-bg-3">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-8 col-lg-9 offset-xl-2 offset-lg-1">
                            <div class="row">
                                <div class="col-lg-9 col-sm-8">
                                    <div class="banner-inner">
                                        <p class="banner-cat s-animate-1">Hot Places</p>
                                        <h2 class="banner-title s-animate-2">Navagio <br> Beach</h2>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-sm-4">
                                    <div class="video-popup-btn s-animate-video">
                                        <a href="https://www.youtube.com/watch?v=c7XEhXZ_rsk" class="video-play-btn mfp-iframe"><i class="fa fa-play"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="banner-slider-controls">
                <div class="slider-nav tp-control-nav"></div>
                <div class="tp-slider-extra slider-extra">
                    <div class="text">
                        <span class="first">01</span>
                        <span class="devider">/</span>
                        <span class="last"></span>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    @if(isset($sliders))
    @include('front.layouts.banner')
    @endif
    
    <!-- banner area end -->

    <!-- search area start -->
    <!-- <div class="search-area tp-main-search-area">
        <div class="container">
            <div class="tp-main-search">
                <div class="row">
                    <div class="col-lg-3 col-md-4">
                        <div class="tp-search-single-wrap">
                            <input class="w-100" type="text" placeholder="Bangladesh,Dhaka">
                            <i class="ti-location-pin"></i>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-4">
                        <div class="tp-search-single-wrap">
                            <input class="w-100" type="text" placeholder="Where From?">
                            <i class="fa fa-dot-circle-o"></i>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-4 order-lg-9">
                        <div class="tp-search-single-wrap float-left w-100">
                            <select class="select w-100">
                                <option value="1">Travel Type</option>
                                <option value="2">Event Travel</option>
                                <option value="3">Weekend Break</option>
                                <option value="4">Package Holiday</option>
                                <option value="5">Business Travel</option>
                            </select>
                            <i class="fa fa-plus-circle"></i>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-8 order-lg-6">
                        <div class="tp-search-single-wrap float-left">
                            <div class="tp-search-date tp-departing-date-wrap w-50 float-left">
                                <input type="text" class="departing-date" placeholder="Departing">
                                <i class="fa fa-calendar-minus-o"></i>
                            </div>
                            <div class="tp-search-date tp-returning-date-wrap w-50 float-left">
                                <input type="text" class="returning-date" placeholder="Returning">
                                <img src="assets/img/icons/2.png" alt="icons">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-4 order-12">
                        <a class="btn btn-yellow" href="#"><i class="ti-search"></i> Search</a>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <!-- search area end -->

    <!-- flight api -->
    <div class="container" style="display:block;">
        <div class="row">
            <div class="col-12 pt-5 custom_tab">
                <ul class="nav nav-tabs justify-content-center">
                    <li class="nav-item">
                        <a class="nav-link active font-weight-bold" data-toggle="tab" href="#home">Flights</a>
                    </li>
                    <li class="nav-item font-weight-bold">
                        <a class="nav-link" data-toggle="tab" href="#menu1">Bus</a>
                    </li>
                    <li class="nav-item font-weight-bold">
                        <a class="nav-link" data-toggle="tab" href="#menu2">Trains</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <!-- search area start -->
                    <div id="home" class="container tab-pane active">
                        <div class="container">
                            <div class="tp-main-search">
                                <!-- <form class="tp-form-wrap" id="flightbooking" action="{{ route('flight-availability') }}" method="POST" >
                                @csrf
                                <div class="row">
                                    <div class="col-lg-2 col-md-3">
                                        <div class="tp-search-single-wrap">
                                            <input class="w-100" type="text" name="from" placeholder="Delhi" required="" data-bv-notempty-message="The From name is required">
                                            <i class="ti-location-pin"></i>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-md-3">
                                        <div class="tp-search-single-wrap">
                                            <input class="w-100" type="text" name="to" placeholder="Where From?" required="" data-bv-notempty-message="The To name is required">
                                            <i class="fa fa-dot-circle-o"></i>
                                        </div>
                                    </div>
                                    <div class="col-lg-1 col-md-1 order-lg-9">
                                        <div class="tp-search-single-wrap float-left w-100">
                                            <select class="select w-100" style="padding: 0 15px;" required="" data-bv-notempty-message="The Adult is required" name="adult">
                                                <option value="0">Adult</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-1 col-md-1 order-lg-9">
                                        <div class="tp-search-single-wrap float-left w-100">
                                             <select class="select w-100" style="padding: 0 15px;" required="" data-bv-notempty-message="The Child is required" name="child">
                                                <option value="">Child</option>
                                                <option value="0">0</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-md-2 order-lg-9">
                                        <div class="tp-search-single-wrap float-left w-100">
                                             <select class="select w-100" style="padding: 0 0px;" required="" data-bv-notempty-message="The Fare Types is required" name="fare_type">
                                                <option value="">Fare Types</option>
                                                <option value="AN">Normal Fare</option>
                                                <option value="AC">Coupon Fare</option>
                                                <option value="ACO">Corporate Fare</option>
                                                <option value="AR">Return Fare</option>
                                                <option value="AS">Sale Fare</option>
                                                <option value="AF">Family Fare</option>
                                                <option value="AFF">Flexi Fare</option>
                                                <option value="AT">Tactical Fare</option>
                                                <option value="AM">SME Fare</option>
                                                <option value="ASR">Special Round Trip</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-8 order-lg-6">
                                        <div class="tp-search-single-wrap float-left">
                                            <div class="tp-search-date tp-departing-date-wrap w-50 float-left">
                                                <input type="text" name="departing" id="departing" class="departing-date" placeholder="Departing" required="" data-bv-notempty-message="The Departing is required">

                                                <i class="fa fa-calendar-minus-o"></i> 
                                            </div>
                                            <div class="tp-search-date tp-returning-date-wrap w-50 float-left">
                                                <input type="text" name="returning" class="returning-date" id="returning" placeholder="Returning" >
                                                <img src="assets/img/icons/2.png" alt="icons">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-1 col-md-3 order-12">  
                                        <button type="submit" class="btn btn-yellow"><i class="ti-search"></i></button>
                                    </div>
                                </div>
                                </form> -->
                        <center><h3>Coming Soon</h3></center>        
                            </div>
                        </div>
                    </div>
                    <!-- search area end -->
                    <div id="menu1" class="container tab-pane fade"><br>
                         <div class="tp-main-search">
                                <!-- <form class="tp-form-wrap" id="flightbooking" action="{{ route('flight-availability') }}" method="POST" >
                                @csrf
                                <div class="row">
                                    <div class="col-lg-2 col-md-3">
                                        <div class="tp-search-single-wrap">
                                            <input class="w-100" type="text" name="from" placeholder="Delhi" required="" data-bv-notempty-message="The From name is required">
                                            <i class="ti-location-pin"></i>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-md-3">
                                        <div class="tp-search-single-wrap">
                                            <input class="w-100" type="text" name="to" placeholder="Where From?" required="" data-bv-notempty-message="The To name is required">
                                            <i class="fa fa-dot-circle-o"></i>
                                        </div>
                                    </div>
                                    <div class="col-lg-1 col-md-1 order-lg-9">
                                        <div class="tp-search-single-wrap float-left w-100">
                                            <select class="select w-100" style="padding: 0 15px;" required="" data-bv-notempty-message="The Adult is required" name="adult">
                                                <option value="0">Adult</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-1 col-md-1 order-lg-9">
                                        <div class="tp-search-single-wrap float-left w-100">
                                             <select class="select w-100" style="padding: 0 15px;" required="" data-bv-notempty-message="The Child is required" name="child">
                                                <option value="">Child</option>
                                                <option value="0">0</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-md-2 order-lg-9">
                                        <div class="tp-search-single-wrap float-left w-100">
                                             <select class="select w-100" style="padding: 0 0px;" required="" data-bv-notempty-message="The Fare Types is required" name="fare_type">
                                                <option value="">Fare Types</option>
                                                <option value="AN">Normal Fare</option>
                                                <option value="AC">Coupon Fare</option>
                                                <option value="ACO">Corporate Fare</option>
                                                <option value="AR">Return Fare</option>
                                                <option value="AS">Sale Fare</option>
                                                <option value="AF">Family Fare</option>
                                                <option value="AFF">Flexi Fare</option>
                                                <option value="AT">Tactical Fare</option>
                                                <option value="AM">SME Fare</option>
                                                <option value="ASR">Special Round Trip</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-8 order-lg-6">
                                        <div class="tp-search-single-wrap float-left">
                                            <div class="tp-search-date tp-departing-date-wrap w-50 float-left">
                                                <input type="text" name="departing" id="departing" class="departing-date" placeholder="Departing" required="" data-bv-notempty-message="The Departing is required">

                                                <i class="fa fa-calendar-minus-o"></i> 
                                            </div>
                                            <div class="tp-search-date tp-returning-date-wrap w-50 float-left">
                                                <input type="text" name="returning" class="returning-date" id="returning" placeholder="Returning" >
                                                <img src="assets/img/icons/2.png" alt="icons">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-1 col-md-3 order-12"> 
                                        <button type="submit" class="btn btn-yellow">
                                            <i class="ti-search"></i>
                                        </button>
                                    </div>
                                </div> -->
                        <center><h3>Coming Soon</h3></center>
                            </div>
                    </div>
                    <div id="menu2" class="container tab-pane fade"><br>
                        <div class="tp-main-search">
                        <center><h3>Coming Soon</h3></center>
                        </div>
                        <!-- <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque
                                laudantium, totam rem aperiam.</p> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end flight api -->
    

    <!-- video area start -->
    <div class="video-area tp-video-area pd-top-110" style="background-image: url(assets/img/bg/7.png);">
        <div class="container">
            <div class="row">
                <div class="col-xl-5 col-lg-6 align-self-center wow animated fadeInRight" data-wow-duration="1s" data-wow-delay="0.3s">
                    <div class="section-title mb-lg-0 mb-4 text-center text-lg-left">
                        <h2 class="title">What is JagJoyu's TIP?</h2>
                        <p>TIP stands for Travel Investment Plan. A unique Travel Investment Plan, where you can invest your money based on different Membership Plans and make customized tour package or join a group tour package of your choice, as per your holiday plan in near future.</p>
                        <a class="btn btn-yellow" href="{{ route('membership-demo')}}"><span>Read More<i class="la la-arrow-right"></i></span></a>
                    </div>
                </div>
                <div class="col-xl-5 col-lg-6 offset-xl-1 wow animated fadeInLeft" data-wow-duration="1s" data-wow-delay="0.3s">
                    <div class="video-popup-wrap">
                        <div class="thumb">
                            <img src="assets/img/video.png" alt="video">
                        </div>
                        <div class="video-popup-btn">
                            <a href="https://www.youtube.com/watch?v=dASGYXYIrlM" class="video-play-btn mfp-iframe"><i class="fa fa-play"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><br>
    <!-- video area end -->
    

    <!-- upcomming tour start -->
    <div class="upcomming-tour upcomming-tour-bg pd-top-75 pd-bottom-120" style="background-image: url(assets/img/bg/11.png);">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12">
                    <div class="section-title">
                        <center><h2 class="title">Tour Packages</h2></center>
                        <center>
                            <p>Jag Joyu is Travel Solution with well organized packages for you. Select your vacation destination and days to pick your packages. Get some off days from your routine, visit some Sea shore places of Goa, some snow- covered of Manali, some remarkable place in Mumbai, hill station of Saputara-Gujarat, historical place like palaces of Jaipur- Rajasthan, and many other adventure places are there for you. Checkout the most reliable packages to travel.</p>
                        </center>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="sliderTourPackages">
                        @foreach($packagType as $packagTypes)
                        <div class="slide">
                            <a href="{{url('/packages',$packagTypes->slug)}}">
                                <div class="single-upconing-card">
                                    <div class="shadow" style="background-image: url({{url('uploads/plans_images',$packagTypes->photo)}})">
                                        <img src="{{url('uploads/plans_images',$packagTypes->photo)}}" height="350" alt="{{$packagTypes->name}}">
                                    </div>
                                    <div class="tp-price-meta">
                                        <h2>{{$packagTypes->name}}</h2>
                                    </div>
                                </div>
                            </a>
                        </div>
                        @endforeach
                    </div>
                   <!--  <div class="upcomming-card-slider upcomming-card-slider-2 tp-common-slider-style">
                        @foreach($packagType as $packagTypes)
                            <a href="{{url('/packages',$packagTypes->slug)}}">
                                <div class="single-upconing-card">
                                    <div class="shadow" style="background-image: url({{url('uploads/plans_images',$packagTypes->photo)}})">
                                        <img src="{{url('uploads/plans_images',$packagTypes->photo)}}" height="350" alt="{{$packagTypes->name}}">
                                    </div>
                                    <div class="tp-price-meta">
                                        <h2>{{$packagTypes->name}}</h2>
                                    </div>
                                </div>
                            </a>
                        @endforeach
                    </div> -->
                </div>
            </div>
        </div>
    </div>
    <!-- upcomming tour end -->

    <!-- suggested blog for you -->
    <div class="offer-area pd-top-0">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-6 col-lg-8">
                    <div class="section-title text-center">
                        <h2 class="title">Suggested Blogs for you</h2>
                        <p>Searching to travel some beautiful destination in India? You are at right place. Below mention are a most amazing traveling destination for you, have a look with the entire tourist place. Check it out!</p>
                    </div>
                </div>
            </div>
        </div>
        <!-- <div class="destinations-list-slider-bg" id="slider"> -->
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="sliderSuggestedBlog">
                            @foreach((App\Blog::latest()->take(5)->get()) as $blogs)
                            <div class="slide">
                                <!-- <div class="d-list-slider-item slider-container"> -->
                                    <div class="single-destinations-list text-center">
                                        <div class="thumb">
                                            <img src="{{url($blogs->thumbnail_image)}}" alt="{{$blogs->slug}}" height="280" width="480">
                                            <div class="d-list-btn-wrap">
                                                <div class="d-list-btn">
                                                    <a class="btn btn-yellow" href="{{url('blog')}}/{{$blogs->slug}}">Read More</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="details" style="height: 120px;">
                                            <h5 class="title">{{$blogs->title}}</h5>
                                        </div>
                                    </div>
                                <!-- </div> -->
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        <!-- </div> -->
    </div>

    <!-- end suggested blog for you -->


    <!-- offer area start -->
    <div class="offer-area pd-top-0" id="newEvents">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-6 col-lg-8">
                    <div class="section-title text-center">
                        <h2 class="title">Upcoming Events</h2>
                        <p>Find a list of upcoming travel & outdoor events in Ahmedabad, India. At Events we always make sure to bring the correct information about all ongoing and outgoing events for you in worldwide. You can personalize the filters to search for the best event which you are interested. Now you are having most easy way to know about the events in your nearby places.</p>
                    </div>
                </div>
            </div>
        </div>
        <!-- <div class="destinations-list-slider-bg"> -->
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="slider">
                            <div class="slide">
                                <div class="d-list-slider-item" >
                                    <div class="single-destinations-list text-center">
                                        <div class="thumb">
                                            <span class="d-list-tag">Special Offer</span>
                                            <img src="assets/img/destination-list/1.png" alt="list" width="480">
                                            <div class="d-list-btn-wrap">
                                                <div class="d-list-btn">
                                                    <a class="btn btn-yellow" href="#">Book Now <i class="fa fa-paper-plane"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="details">
                                            <h4 class="title">Greece</h4>
                                            <p class="content">Atmosphere of the sunny country</p>
                                            <ul class="tp-list-meta border-bt-dot">
                                                <li><i class="fa fa-calendar-o"></i> 8oct</li>
                                                <li><i class="fa fa-clock-o"></i> 4 days</li>
                                                <li><i class="fa fa-star"></i> 4.3</li>
                                            </ul>
                                            <div class="tp-price-meta tp-price-meta-cl" style="height: 60px;">
                                            <!-- <p>Aenean sed nibh a magna posuere tempor. Nunc faucibus nunc aliquet. Donec congue, nunc vel tempor</p> -->
                                        </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="slide">
                                <div class="d-list-slider-item">
                                <div class="single-destinations-list text-center">
                                    <div class="thumb">
                                        <img src="assets/img/destination-list/2.png" alt="list" width="480">
                                        <div class="d-list-btn-wrap">
                                            <div class="d-list-btn">
                                                <a class="btn btn-yellow" href="#">Book Now <i class="fa fa-paper-plane"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="details">
                                        <h4 class="title">Italy</h4>
                                        <p class="content">Atmosphere of the sunny country</p>
                                        <ul class="tp-list-meta border-bt-dot">
                                            <li><i class="fa fa-calendar-o"></i> 8oct</li>
                                            <li><i class="fa fa-clock-o"></i> 4 days</li>
                                            <li><i class="fa fa-star"></i> 4.3</li>
                                        </ul>
                                        <div class="tp-price-meta tp-price-meta-cl" style="height: 60px;">
                                            <p>Aenean sed nibh a magna posuere tempor. Nunc faucibus nunc aliquet. Donec congue, nunc vel tempor</p>
                                        </div>
                                    </div>
                                </div>
                                </div>
                            </div>
                            <div class="slide">
                                <div class="d-list-slider-item">
                                    <div class="single-destinations-list text-center">
                                        <div class="thumb">
                                            <span class="d-list-tag">Special Offer</span>
                                            <img src="assets/img/destination-list/3.png" alt="list" width="480">
                                            <div class="d-list-btn-wrap">
                                                <div class="d-list-btn">
                                                    <a class="btn btn-yellow" href="#">Book Now <i class="fa fa-paper-plane"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="details">
                                            <h4 class="title">Canada</h4>
                                            <p class="content">Atmosphere of the sunny country</p>
                                            <ul class="tp-list-meta border-bt-dot">
                                                <li><i class="fa fa-calendar-o"></i> 8oct</li>
                                                <li><i class="fa fa-clock-o"></i> 4 days</li>
                                                <li><i class="fa fa-star"></i> 4.3</li>
                                            </ul>
                                            <div class="tp-price-meta tp-price-meta-cl" style="height: 60px;">
                                               <p>Aenean sed nibh a magna posuere tempor. Nunc faucibus nunc aliquet. Donec congue, nunc vel tempor</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                            <div class="slide">
                                <div class="d-list-slider-item">
                                    <div class="single-destinations-list text-center">
                                        <div class="thumb">
                                            <span class="d-list-tag">Special Offer</span>
                                            <img src="assets/img/destination-list/1.png" alt="list" width="480">
                                            <div class="d-list-btn-wrap">
                                                <div class="d-list-btn">
                                                    <a class="btn btn-yellow" href="#">Book Now <i class="fa fa-paper-plane"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="details">
                                            <h4 class="title">Greece</h4>
                                            <p class="content">Atmosphere of the sunny country</p>
                                            <ul class="tp-list-meta border-bt-dot">
                                                <li><i class="fa fa-calendar-o"></i> 8oct</li>
                                                <li><i class="fa fa-clock-o"></i> 4 days</li>
                                                <li><i class="fa fa-star"></i> 4.3</li>
                                            </ul>
                                            <div class="tp-price-meta tp-price-meta-cl" style="height: 60px;">
                                                <p>Aenean sed nibh a magna posuere tempor. Nunc faucibus nunc aliquet. Donec congue, nunc vel tempor</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <!-- </div> -->
    </div>
    <!-- offer area end -->
    <br><br>
    <!-- Review Area Start -->
    <div class="client-area pd-top-10">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-6 col-lg-8">
                    <div class="section-title text-center">
                        <h2 class="title" style="margin-bottom: 20px">What Our Customers Say</h2>
                    </div>
                </div>
            </div>
            {{--<div class="client-slider tp-common-slider-style">
                @if(isset($review))
                    @foreach($review as $key)

                    <div class="single-client-card">
                        <div class="quote"><i class="ti-quote-left"></i></div>   
                        <p class="content-text">{{$key->content}}</p>
                        <div class="media">
                            <div class="media-body">
                                @if($key->user)
                                    @foreach($key->user as $user)
                                        <h4>{{$user->name}} {{$user->surname}}</h4>
                                    @endforeach
                                @endif
                                @if($key->member)
                                    @foreach($key->member as $member)
                                        <h4>{{$member->name}}</h4>
                                    @endforeach
                                @endif
                                <span>{{$key->role}}</span>
                            </div>
                        </div>
                    </div>
                    @endforeach
                @endif 
            </div> --}}
            <div class="blog-slider tp-common-slider-style tps-arrow-left-right">
                @if(isset($review))
                    @foreach($review as $key)
                    <div class="blog-slider-item">
                        <div class="single-blog single-blog-wrap style-four" style="height: 300px;">
                            <div class="thumb single-blog-left-wrap">
                                @if(isset($key->review_image))
                                    @foreach($key->review_image as $keyImage)
                                        <img src="{{$keyImage->image}}" alt="blog" style="height:180px;width:250px;">
                                    @endforeach
                                @else
                                    <img src="assets/img/blog/11.png" alt="blog">
                                @endif
                            </div>
                            <div class="single-blog-details single-blog-right-wrap">
                               <div> <p class="content more" style="    height: 144px;">{{$key->content}}</p></div>
                                <div class="media">
                                    <div class="media-body">
                                        @if(isset($key->customer))
                                            @foreach($key->customer as $customers)
                                                <h4 style="margin-top: 20px">{{$customers->first_name}} {{$customers->last_name}}</h4>
                                            @endforeach
                                        @endif
                                        @if(isset($key->member))
                                            @foreach($key->member as $members)
                                                <h4 style="margin-top: 20px">{{$members->name}}</h4>
                                            @endforeach
                                        @endif
                                        <span>{{$key->role}}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
    <!-- Review Area End -->

    <!-- client review area start -->
    <div class="client-review-area client-review-area-home pd-top-70">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 align-self-center">
                    <div class="thumb wow animated fadeInRight" data-wow-duration="1s" data-wow-delay="0.3s">
                        <div class="client-review-thumb">
                            <img src="assets/img/others/client-review.png" alt="review">
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="single-client-review wow animated fadeInLeft" data-wow-duration="1s" data-wow-delay="0.3s">
                        <p class="sub-title">Travel Reviews</p>
                        <h3 class="location-name">India</h3>
                        <div class="tp-review-meta">
                            <i class="ic-yellow fa fa-star"></i>
                            <i class="ic-yellow fa fa-star"></i>
                            <i class="ic-yellow fa fa-star"></i>
                            <i class="ic-yellow fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <span>4.0</span>
                        </div>
                        <p>A land resounding with the riches and glories of opulent dynasties, powerful rulers, flourishing civilisations and profound history, India has a splendid heritage that is reflected in its architecture, monuments, arts, crafts, cultures and even religions. While formidable forts, ancient temples and grand palaces testify of the grandeur of times gone by, the various museums and galleries scattered across the country act as inventories of India's seamless past. Such is the magnificence of the structures in the country that UNESCO has identified several of them as heritage sites.</p>
                        <div class="media">
                            <div class="media-left">
                                <img src="assets/img/client/1.png" alt="client">
                            </div>
                            <div class="media-body">
                                <h6>Bharat Dhaneshwari</h6>
                                <p>Tourist</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!-- client review area End -->
    <div class="back-to-top">
    <span class="back-top"><i class="fa fa-angle-up"></i></span>
</div>
<!-- back to top area end -->

<div class="popup-button" onclick="document.getElementById('id01').style.display='block'">
    <a href="{{route('front.askforcall')}}" target="_blank" class="border-bottom-left-radius" style="font-size: 20px">Ask For Call</a>
</div>



@endsection
@section('css')

<style type="text/css">
   
.b-close {
    color: #fff;
    background: #ff0000;
    padding: 5px 10px;
    position: absolute;
    right: 0;
    top: 0;
    cursor: pointer;
}
#toolbar{
    display: none;
}
.moreelipses
{
    display: none !important;
}
/*.slider{
  width:400px;
  margin:20px auto;
  text-align: center;
  padding:20px;
  color:white;
  .parent-slide{padding:15px;}
  img{display: block;margin:auto;}
}*/

</style>
@endsection
@push('script')
<script src="https://www.ostraining.com/images/coding/bpopup/jquery.bpopup.min.js"></script>
<script type="text/javascript">
 $( document ).ready(function() {
    $('.slider').slick({
      autoplay:true,
      autoplaySpeed:1500,
      arrows:true,
      centerMode:true,
      slidesToShow:3,
      slidesToScroll:2,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            adaptiveHeight: true,
          },
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          },
        },
      ],
    });
    $('.sliderSuggestedBlog').slick({
        autoplay:true,
        autoplaySpeed:1500,
        arrows: true,
        centerMode: true,
        slidesToShow:3,
        slidesToScroll:2,
        responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            adaptiveHeight: true,
          },
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          },
        },
      ],
    });
    $('.sliderTourPackages').slick({
        autoplay:true,
        autoplaySpeed:1500,
        arrows: true,
        centerMode: true,
        slidesToShow:3,
        slidesToScroll:2,
        responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            adaptiveHeight: true,
          },
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          },
        },
      ],
    });
    // $('.slider-eventUpcomming').slick({
    //     autoplay:true,
    //     autoplaySpeed:1500,
    //     arrows:true,
    //     prevArrow:'<button type="button" class="slick-prev"></button>',
    //     nextArrow:'<button type="button" class="slick-next"></button>',
    //     centerMode:true,
    //     slidesToShow:3,
    //     slidesToScroll:2
    // });


    $('#myModal').modal('show');
    $('#mypopup').bPopup();
    $('iframe #toolbar').css('display','none');
    //read more
    var showChar = 100;
    var moretext = "<b> Read More (+)</b>";
    var lesstext = "<b>Read Less (-)</b>";
    $('.more').each(function() {
        var content = $(this).html();

        if(content.length > showChar) {

            var c = content.substr(0, showChar);
            var h = content.substr(showChar-1, content.length - showChar);

            var html = c + '<span class="moreelipses" ></span><span class="morecontent" style="display: none;">' + h + '&nbsp;&nbsp;</span>&nbsp;&nbsp;<a href="" class="morelink less">'+moretext+'</a>';

            $(this).html(html);
        }
    });
    $(".morelink").click(function(){
        if($(this).hasClass("less")) {
            $(this).removeClass("less");
            $(this).addClass("more");
            $(this).html(lesstext);
        } else {
            $(this).removeClass("more");
            $(this).addClass("less");
            $(this).html(moretext);
        }
        $(this).parent().prev().toggle();
        $(this).prev().toggle();
        return false;
    });
    
});

</script>
@endpush