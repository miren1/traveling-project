@extends('front.layouts.master')
@section('title','FAQ')
@push('style')
@endpush
@section('content')
<!-- breadcrumb area start -->
<div class="breadcrumb-area jarallax" style="background-image:url(assets/img/bg/1.png);">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-inner">
                    <h1 class="page-title">FAQs</h1>
                    <ul class="page-list">
                        <li><a href="{{ url()->previous()}}">Home</a></li>
                        <li>FAQs</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb area End -->

<!-- blog area start -->
<div class="faq-page-area pd-top-110">
    <div class="container">
        <div class="row">
            <div class="col-xl-12 col-lg-12">
                <div class="row">
                    <div class="col-xl-8">
                        <div class="section-title mb-0">
                            <h2 class="title">Frequently Asked Questions</h2>
                            {{-- <p>Aenean non accumsan ante. Duis et risus accumsan sem tempus porta nec sit amet est. Sed ut euismod quam. Suspendisse potenti. Aliquam fringilla</p> --}}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <ul class="nav nav-tabs tp-tabs">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#tabs_1">Interactive Window (Help & Support) and internal system</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#tabs_2">Whom should I contact if I have issues with JagJoyu’s system?</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link"  data-toggle="tab" href="#tabs_3">DAILY ROUTINE</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link"  data-toggle="tab" href="#tabs_4">LEAVES</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link"  data-toggle="tab" href="#tabs_5">SALARY & PAYSLIPS</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link"  data-toggle="tab" href="#tabs_6">TARGET RELATED FAQS</a>
                            </li>
                            <!-- <li class="nav-item">
                                <a class="nav-link"  data-toggle="tab" href="#tabs_7">Room types</a>
                            </li> -->
                        </ul>
                    </div>
                    <div class="col-md-8">
                        <div class="tab-content faq-tab-content" style="background-image: url(assets/img/others/12.png);">
                            <div class="tab-pane fade show active" id="tabs_1" >
                                <div class="faq-details">
                                    <p>What is JagJoyu’s Interactive Window (Help & Support)?
                                        An online integrated system, accessible post individual user-based login, and is utilized to raise request, check response, and get the queries clarified under different categories.</p>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="tabs_2">
                                <div class="faq-details">
                                    <h6>What are the working hours of the organisation?</h6>
                                    <p>JagJoyu Team need to work for at least 9 hours every day including the refreshment breaks.</p>
                                    
                                    <h6>Do we need to work over week off, public holidays and personal leaves?</h6>
                                    <p>No, for week offs, public holidays and personal leaves you don’t need to work unless you have to mitigate an important assignment in agreement with your manager.</p>
                                    
                                    <h6>What if I miss to work for any of the given assignment?</h6>
                                    <p class="mb-0">If you have missed or not completed an assigned tasks with an unaccepted reasons, will be considered as a non-compliance and may result in unpaid leave adjustment for that day, though paid leaves may be available in your account.</p>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="tabs_3">
                                <div class="faq-details">
                                    <h6>I have special dietary requirements – will they be catered for?</h6>
                                    <p>We will try our very hardest to accommodate all dietary requirements but in some out-of-the-way places it can be very difficult to guarantee. We will let you know if there are places on your itinerary where this is the case. Please let us know at the time of booking of any food requirements or allergies and we’ll pass the information</p>
                                    <h6>What will the food be like on my trip?</h6>
                                    <p class="mb-0">Food is one of the most exciting parts of travel. There may be some familiar fare but often you’ll be confronted with the new, interesting and downright weird of the culinary world but we like to think of it as an adventure for all the senses. In addition to this, our flexible itineraries often</p>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="tabs_4">
                                <div class="faq-details">
                                    <h6>Am I eligible for leaves during training or probation period?</h6>
                                    <p>At JagJoyu, we focus on providing rigorous training and consistent performance driven approach for the growth of company.Hence, we do not encourage taking leaves during your training period. However, for any medical emergency, you will be eligible for unpaid sick leaves during your training phase.</p>
                                    <h6>Do I need to take prior approval from the manager before applying for leaves/Work from Home?</h6>
                                    <p class="mb-0">Yes, it is mandatory to take approval from the manager before applying leaves/Work from Home. In case of emergency leaves, the manager should be informed at the earliest possible convenience.</p>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="tabs_5">
                                <div class="faq-details">
                                    <h6>How to view and download my Payslip(s)?</h6>
                                    <h6>How do I transfer my PF from previous company to JagJoyu?</h6>
                                    <p>Based on the details submitted in the PF form, new PF account will be created by JagJoyu accounts team and then you shall receive an email confirming the account creation. After two months of date of relieving from the previouscompany, you will have to transfer the PF from previous company to JagJoyu.</p>

                                    <h6>Does the unutilized leave gets carry forwarded to the next year?</h6>
                                    <p class="mb-0">Kindly refer Holidays / Leave Policy.</p>

                                    <h6>What is the Leave Calendar that we follow?</h6>
                                    <p class="mb-0">Kindly refer Holidays / Leave Policy.</p>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="tabs_6">
                                <div class="faq-details">
                                    <h6>Unable to meet or complete Target?</h6>
                                    <p>An employee can directly speak to reporting Manager for scope of improvement and assistance to delivery expected results.</p>
                                    <h6>Can there be a possibility of Target revision in between a year?</h6>
                                    <p class="mb-0">There may be a possibility of Target revision but it will only happen based on formal discussion with an Employee.</p>
                                </div>
                            </div>
                            <!-- <div class="tab-pane fade" id="tabs_7">
                                <div class="faq-details">
                                    <h6>07 I have special dietary requirements – will they be catered for?</h6>
                                    <p>We will try our very hardest to accommodate all dietary requirements but in some out-of-the-way places it can be very difficult to guarantee. We will let you know if there are places on your itinerary where this is the case. Please let us know at the time of booking of any food requirements or allergies and we’ll pass the information</p>
                                    <h6>What will the food be like on my trip?</h6>
                                    <p class="mb-0">Food is one of the most exciting parts of travel. There may be some familiar fare but often you’ll be confronted with the new, interesting and downright weird of the culinary world but we like to think of it as an adventure for all the senses. In addition to this, our flexible itineraries often</p>
                                </div>
                            </div> -->
                            
                        </div>
                    </div>
                </div>
            </div>   
            {{-- <div class="col-xl-3 col-lg-4">
                <aside class="sidebar-area">
                    <div class="widget widget_search bg-none pd-none">
                        <form class="search-form">
                            <div class="form-group">
                                <input type="text" placeholder="Search">
                            </div>
                            <button class="submit-btn" type="submit"><i class="ti-search"></i></button>
                        </form>
                    </div>
                    <div class="widget widget_categories mb-0">
                        <h2 class="widget-title">Category</h2>
                        <ul>
                            <li><a href="#">Software</a> 33</li>
                            <li><a href="#">App Landing</a> 81</li>
                            <li><a href="#">Saas Landing</a> 12</li>
                            <li><a href="#">Design Studio</a> 17</li>
                            <li><a href="#">Product Showcase</a> 62</li>
                        </ul>
                    </div>
                </aside>
            </div> --}}
        </div>
    </div>
</div>
<!-- blog area End -->

<!-- newslatter area Start -->
<!-- <div class="newslatter-area pd-top-120">
    <div class="container">
        <div class="newslatter-area-wrap border-tp-solid">
            <div class="row">
                <div class="col-xl-3 col-lg-6 col-md-5 offset-xl-2">
                    <div class="section-title mb-md-0">
                        <h2 class="title">Newsletter</h2>
                        <p>Sign up to receive the best offers</p>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-6 col-md-7 align-self-center offset-xl-1">
                    <div class="input-group newslatter-wrap">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fa fa-envelope"></i></span>
                        </div>
                        <input type="text" class="form-control" placeholder="Email">
                        <div class="input-group-append">
                            <button class="btn btn-yellow" type="button">Subscribe</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> -->
<!-- newslatter area End -->
@endsection

@push('script')
@endpush