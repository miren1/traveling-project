@extends('front.layouts.master')
@section('title','About Us')
@push('style')
<style type="text/css">
    .j-align{
        text-align: justify;
    }
</style>
@endpush
@section('content')

<!-- breadcrumb area start -->
<div class="breadcrumb-area jarallax" style="background-image:url(assets/img/bg/1.png);">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-inner">
                    <h1 class="page-title">About Us</h1>
                    <ul class="page-list">
                        <li><a href="{{ url()->previous()}}">Home</a></li>
                        <li>About Us</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- intro area start -->
<div class="intro-area pd-top-108">
    <div class="container">
        <div class="section-title text-lg-center text-left">
            <h2 class="title"><span>Vision &</span> Mission</h2>
        </div>
        <div class="row">
            <div class="col-lg-3 col-md-6">
                <div class="single-intro style-two">
                    <div class="thumb">
                        <img src="assets/img/icons/9.png" alt="img">
                    </div>
                    <h4 class="intro-title">Bring a new way of thinking for Travel plan.</h4>
                   <!--  <p>Sponsorships are like unicorns or leprechauns, talked about often but they don’t exist. There is one dollars</p> -->
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="single-intro style-two">
                    <div class="thumb">
                        <img src="assets/img/icons/11.png" alt="img">
                    </div>
                    <h4 class="intro-title">Focus on customer satisfaction and build value for long-time</h4>
                    <!-- <p>My response usually harsh. Offended at the that a career that’s taken more than a decade to create could be</p> -->
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="single-intro style-two">
                    <div class="thumb">
                        <img src="assets/img/icons/10.png" alt="img">
                    </div>
                    <h4 class="intro-title">Options based flexibility with cost-effective travel solution</h4>
                    <!-- <p>I have always made a living to make movies, never the other way around.I first I washed dishes in a seafood</p> -->
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="single-intro style-two">
                    <div class="thumb">
                        <img src="assets/img/icons/12.png" alt="img">
                    </div>
                    <h4 class="intro-title">Build a brand ‘Jag Joyu’</h4>
                   <!--  <p>Aenean sed nibh a magna posuere tempor. Nunc faucibus nunc aliquet. Donec congue, nunc vel tempor</p> -->
                </div>
            </div>
        </div>
    </div>
</div>
<!-- intro area End -->

<!-- about section area start -->
<div class="about-section pd-top-80">
    <div class="container">
        <div class="row">
            <div class="col-lg-5 align-self-center">
                <div class="section-title mb-lg-0">
                    <h2 class="title">Lets Go Travel <br> with Us</h2>
                    <p class="j-align">‘Jag Joyu Travel Solutions’ is an insured and registered Company. It has a group of young & experienced team members with an expertise in providing Complete Travel Solutions. It believes in following key core values by maintaining level of standards.</p>

                    <p class="j-align">With an exclusive focus on providing best tour and packages, it has Travel Investment Membership Plans called as JagJoyu’s TIP, to suit the travel needs as per budget of any traveler. The membership plans are made to travel anywhere at any-time as per convenience of a customer. (refermembership plans)</p>

                    <p class="j-align">The primary offering includes Group and Tailor made Travel Packages and Ticket Bookings. And, the additional services includes Visa, Forex Assistance, Bus / Car Rentals and Travel Insurance. Its diversified gamut of services for customers located all over the world, has made it prove the name ‘Jag Joyu’.</p>
                </div>
            </div>
            <div class="col-lg-5 offset-lg-2">
                <div class="thumb about-section-right-thumb">
                    <img src="assets/img/others/9.png" alt="img">
                    <img class="about-absolute-thumb" src="assets/img/others/10.png" alt="img">
                </div>
            </div>
        </div>
    </div>
</div>
<!-- about section area end -->

<div class="team-newslater-bg" style="background-image:url(assets/img/bg/4.png);">
    
</div>

@endsection


@push('script')
@endpush