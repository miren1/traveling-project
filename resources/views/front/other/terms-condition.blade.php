@extends('front.layouts.master')
@section('title','Terms Condition')
@push('style')
@endpush
@section('content')
<!-- breadcrumb area start -->
<div class="breadcrumb-area jarallax" style="background-image:url(assets/img/bg/1.png);">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-inner">
                    <h1 class="page-title">Terms & Condition</h1>
                    <ul class="page-list">
                        <li><a href="{{ url('/')}}">Home</a></li>
                        <li>T&C</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb area End -->

<!-- blog area start -->
<div class="faq-page-area pd-top-110">
    <div class="container">
        <div class="row">
            <div class="col-xl-12 col-lg-12">
                <div class="row">
                    <div class="col-xl-8">
                        <div class="section-title mb-0">
                            <h2 class="title">Terms & Conditions</h2>
                            {{-- <p>Aenean non accumsan ante. Duis et risus accumsan sem tempus porta nec sit amet est. Sed ut euismod quam. Suspendisse potenti. Aliquam fringilla</p> --}}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <ul class="nav nav-tabs tp-tabs">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#tabs_1">Attention</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#tabs_2">Acceptance of Agreement</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link"  data-toggle="tab" href="#tabs_3">Use of Website</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link"  data-toggle="tab" href="#tabs_4">Editing, Deleting and Modification</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link"  data-toggle="tab" href="#tabs_5">Links to Other Websites</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link"  data-toggle="tab" href="#tabs_6">No Warranties</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link"  data-toggle="tab" href="#tabs_7">Limitation of Liability</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link"  data-toggle="tab" href="#tabs_8">Applicable Laws</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link"  data-toggle="tab" href="#tabs_9">Severability</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link"  data-toggle="tab" href="#tabs_10">Not Legal Advice</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link"  data-toggle="tab" href="#tabs_11">Copyright</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link"  data-toggle="tab" href="#tabs_12">Terms Policy</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-8">
                        <div class="tab-content faq-tab-content" style="background-image: url(assets/img/others/12.png);">
                            <div class="tab-pane fade show active" id="tabs_1" >
                                <div class="faq-details">
                                    <p>
                                        Welcome to the Jag-Joyu Travel Solutions LLP. Web website (the "www.jagjoyu.com"). Please read these Terms of use carefully before using this Website. By using this Website you agree to comply with and be bound by these Terms of Use. If you do not agree to these terms, you must not use this Website.
                                    </p>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="tabs_2">
                                <div class="faq-details">
                                    <p>
                                        You agree to the terms and conditions outlined in this Terms of Use Agreement ("Agreement") with respect to our website. This Agreement constitutes the entire and only agreement between us and you with respect to the website and supersedes all prior or contemporaneous agreements, representations, warranties and understandings with respect to the website, the content, products or services provided by or through the website, and the subject matter of this Agreement. This Agreement may be amended at any time by us from time to time without specific notice to you. The latest Agreement will be posted on the website, and you must review this Agreement prior to using the website.
                                    </p>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="tabs_3">
                                <div class="faq-details">
                                    <p>Jag-Joyu Travel Solutions LLP authorizes you to view, print or download any content, graphic, form or document from the website for your personal, non-commercial use and you must not change or delete any such material or copyright notice appearing on such material. You may not modify the materials at this Website in any way or repost, republish, reproduce, publicly display, perform, assign, sublicense, sell or prepare derivative works of or otherwise use the materials for any purpose except as expressly permitted under this Agreement. Copyright in the materials at this Website is owned by or used with permission JAG-JOYU TRAVEL SOLUTIONS LLP and any unauthorized use of any materials at this website may violate copyright, trade mark and other proprietary (including but not limited to intellectual property) legal rights of JAG-JOYU TRAVEL SOLUTIONS LLP. As a user of this website you are granted a nonexclusive, nontransferable, revocable, limited license to access and use this website and content in accordance with these Terms of Use. Provider may terminate this license at any time for any reason.</p>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="tabs_4">
                                <div class="faq-details">
                                    <p>
                                        We reserve the right in our sole discretion to edit or delete any documents, information or other content appearing on the website.
                                    </p>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="tabs_5">
                                <div class="faq-details">
                                    <p>
                                        The Website contains links to other websites which are not created, owned or operated by JAG-JOYU TRAVEL SOLUTIONS LLP. JAG-JOYU TRAVEL SOLUTIONS LLP is not responsible for the content; accuracy or opinions expressed in such websites, and such websites are not investigated, monitored or checked for accuracy or completeness by us. Inclusion of any linked website on our website does not imply approval or endorsement of the linked website by us. If you decide to use these links you will leave our website and access these third-party websites, you do so at your own risk. JAG-JOYU TRAVEL SOLUTIONS LLP does not make any representations about third-party websites; or any information, other products or materials found there; or any results that may be obtained from using them.
                                    </p>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="tabs_6">
                                <div class="faq-details">
                                    <p>
                                        The information, materials and services provided at and accessible through this website are provided "as is", "as available" and, to the maximum extent permitted by applicable law, Jag-Joyu Travel Solutions LLP expressly disclaims all warranties, whether expressed or implied, including warranties of merchantability, fitness for a particular purpose, use, or the results of use of this website, any web websites linked to this website, or the materials or information contained at this or any such other websites. The information, materials and services provided at and accessible through this website may contain bugs, errors, viruses, problems or other limitations.
                                    </p>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="tabs_7">
                                <div class="faq-details">
                                    <p>
                                        JAG-JOYU TRAVEL SOLUTIONS LLP aggregate liability for damages claimed under this agreement and arising out of JAG-JOYU TRAVEL SOLUTIONS LLP performance of services hereunder shall be limited to the total purchase price you pay for any goods, services or information.

                                        To the maximum extent permitted by applicable law, in no event shall Jag-Joyu Travel Solutions LLP be liable whatsoever for your use of any information, materials or services provided at or accessible through this website. In particular, but not as a limitation thereof, Jag-Joyu Travel Solutions LLP is not liable for any indirect, special, incidental or consequential damages (including damage for loss of business, loss of profits, litigation, or the like), whether or not reasonably foreseeable and even if Jag-Joyu Travel Solutions LLP has been advised of the possibility of such damages and regardless of the form of action, whether in contract, tort (including negligence), strict liability, arising under statute or otherwise. The negation of damages set forth above are fundamental elements of the basis of the bargain between Jag-Joyu Travel Solutions LLP and you. This website and the information would not be provided without such limitations. No advice or information, whether oral or written, obtained by you from Jag-Joyu Travel Solutions LLP through the website shall create any warranty, representation or guarantee not expressly stated in this agreement.
                                    </p>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="tabs_8">
                                <div class="faq-details">
                                    <p>
                                        This Website is administered by JAG-JOYU TRAVEL SOLUTIONS LLP at its offices in Ahmedabad, Gujarat, India and is subject to the laws of the Ahmedabad, Gujarat, India. You agree to submit to the non-exclusive jurisdiction of the courts of India in relation to any dispute relating to this Agreement. JAG-JOYU TRAVEL SOLUTIONS LLP makes no representation that materials at this website are appropriate or available for use outside India, and access to them from territories where there contents are illegal is prohibited. You may not use, export or re-export the materials at this website or any copy or adaptation in violation of any applicable laws or regulations.
                                    </p>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="tabs_9">
                                <div class="faq-details">
                                    <p>
                                        If any provision of this Agreement is found invalid or unenforceable, that provision will be enforced to the maximum extent permissible, and the other provisions of this Agreement will remain in force.
                                    </p>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="tabs_10">
                                <div class="faq-details">
                                    <p>
                                        Content is not intended to and does not constitute legal advice and no attorney- client relationship is formed, nor is anything submitted to this website treated as confidential. The accuracy, completeness, adequacy or currency of the content is not warranted or guaranteed. Your use of content on this website or materials linked from this website is at your own risk.
                                    </p>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="tabs_11">
                                <div class="faq-details">
                                    <p>
                                        All material available on this website is protected by copyright laws. Distribution of the material from the website, for commercial purposes is prohibited. "The owners of the intellectual property, copyrights and trademarks or its affiliates or third party licensors". Domestic and International Copyright and Trademark laws protect the entire contents of the website. You are expressly prohibited from modifying, copying, reproducing, republishing, uploading, posting, transmitting or distributing any material on this website including text, graphics, code and/or software.
                                    </p>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="tabs_12">
                                <div class="faq-details">
                                    <p>
                                        JAG-JOYU TRAVEL SOLUTIONS LLP reserves the right to revise, amend or modify our TERMS policy at any time and in any manner it pleases. Any change or revision will be posted here.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>   
            {{-- <div class="col-xl-3 col-lg-4">
                <aside class="sidebar-area">
                    <div class="widget widget_search bg-none pd-none">
                        <form class="search-form">
                            <div class="form-group">
                                <input type="text" placeholder="Search">
                            </div>
                            <button class="submit-btn" type="submit"><i class="ti-search"></i></button>
                        </form>
                    </div>
                    <div class="widget widget_categories mb-0">
                        <h2 class="widget-title">Category</h2>
                        <ul>
                            <li><a href="#">Software</a> 33</li>
                            <li><a href="#">App Landing</a> 81</li>
                            <li><a href="#">Saas Landing</a> 12</li>
                            <li><a href="#">Design Studio</a> 17</li>
                            <li><a href="#">Product Showcase</a> 62</li>
                        </ul>
                    </div>
                </aside>
            </div> --}}
        </div>
    </div>
</div>
<!-- blog area End -->

<!-- newslatter area Start -->
<!-- <div class="newslatter-area pd-top-120">
    <div class="container">
        <div class="newslatter-area-wrap border-tp-solid">
            <div class="row">
                <div class="col-xl-3 col-lg-6 col-md-5 offset-xl-2">
                    <div class="section-title mb-md-0">
                        <h2 class="title">Newsletter</h2>
                        <p>Sign up to receive the best offers</p>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-6 col-md-7 align-self-center offset-xl-1">
                    <div class="input-group newslatter-wrap">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fa fa-envelope"></i></span>
                        </div>
                        <input type="text" class="form-control" placeholder="Email">
                        <div class="input-group-append">
                            <button class="btn btn-yellow" type="button">Subscribe</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> -->
<!-- newslatter area End -->
@endsection

@push('script')
@endpush