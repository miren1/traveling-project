@extends('front.layouts.master')
@section('title','Gallary')
@push('style')
@endpush
@section('content')
<!-- breadcrumb area start -->
<div class="breadcrumb-area jarallax" style="background-image:url(assets/img/bg/1.png);">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-inner">
                    <h1 class="page-title">Gallery</h1>
                    <ul class="page-list">
                        <li><a href="{{ url()->previous()}}">Home</a></li>
                        <li>Gallery</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb area End -->

<!-- gallery area start -->
<div class="gallery-area pd-top-108">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-7">
                <div class="section-title text-center">
                    <!-- <h2 class="title">Beautiful Beach of Greece</h2> -->
                    <!-- <p>Curabitur vulputate arcu odio, ac facilisis diam accumsan ut. Ut imperdiet et leo in vulputate. Sed eleifend lacus eu posuere viverra. Vestibulum id turpis lectus. Donec rhoncus quis elit</p> -->
                </div>
            </div>
        </div>
        <!-- Gallery area start -->
        <div class="gallery-area">
            <div class="container">
                <div class="gallery-filter-area row">
                    <div class="gallery-sizer col-1"></div>
                    <!-- gallery-item -->
                    <div class="tp-gallery-item col-md-4 col-12 mb-10">
                        <div class="tp-gallery-item-img">
                            <a class="popup-thumb" href="{{url('/assets/img/gallery-details/1.png')}}" data-effect="mfp-zoom-in">
                                <img src="{{asset('/assets/img/gallery-details/1.png')}}" alt="image">
                            </a>
                        </div>
                    </div>
                    <!-- gallery-item -->
                    <div class="tp-gallery-item col-md-4 col-12">
                        <div class="tp-gallery-item-img">
                            <a class="popup-thumb" href="{{url('/assets/img/gallery-details/2.png')}}" data-effect="mfp-zoom-in">
                                <img src="{{asset('/assets/img/gallery-details/2.png')}}" alt="image">
                            </a>
                        </div>
                    </div>
                    <!-- gallery-item -->
                    <div class="tp-gallery-item col-md-4 col-12">
                        <div class="tp-gallery-item-img">
                            <a class="popup-thumb" href="{{url('/assets/img/gallery-details/3.png')}}" data-effect="mfp-zoom-in">
                                <img src="{{asset('/assets/img/gallery-details/3.png')}}" alt="image">
                            </a>
                        </div>
                    </div>

                    <!-- gallery-item -->
                    <div class="tp-gallery-item col-md-4 col-12">
                        <div class="tp-gallery-item-img">
                            <a class="popup-thumb" href="{{url('/assets/img/gallery-details/4.png')}}" data-effect="mfp-zoom-in">
                                <img src="{{asset('/assets/img/gallery-details/4.png')}}" alt="image">
                            </a>
                        </div>
                    </div>
                    <!-- gallery-item -->
                    <div class="tp-gallery-item col-md-4 col-12">
                        <div class="tp-gallery-item-img">
                            <a class="popup-thumb" href="{{url('/assets/img/gallery-details/5.png')}}" data-effect="mfp-zoom-in">
                                <img src="{{asset('/assets/img/gallery-details/2.png')}}" alt="image">
                            </a>
                        </div>
                    </div>
                    <!-- gallery-item -->
                    <div class="tp-gallery-item col-md-7 col-12">
                        <div class="tp-gallery-item-img">
                            <a class="popup-thumb" href="{{url('/assets/img/gallery-details/6.png')}}" data-effect="mfp-zoom-in">
                                <img src="{{asset('/assets/img/gallery-details/6.png')}}" alt="image">
                            </a>
                        </div>
                    </div>

                    <!-- gallery-item -->
                    <div class="tp-gallery-item col-md-5 col-12">
                        <div class="tp-gallery-item-img">
                            <a class="popup-thumb" href="{{url('/assets/img/gallery-details/7.png')}}" data-effect="mfp-zoom-in">
                                <img src="{{asset('/assets/img/gallery-details/7.png')}}" alt="image">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Gallery area end -->
    </div>
</div>
<!-- gallery area End -->

<!-- ads area start -->
<!-- <div class="ads-area pd-top-90">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-8">
                <a class="ads-thumb" href="#">
                    <img src="assets/img/others/1.png" alt="ads">
                </a>
            </div>
        </div>
    </div>
</div> -->
<!-- ads area End -->

<!-- newslatter area Start -->
<!-- <div class="newslatter-area pd-top-120">
    <div class="container">
        <div class="newslatter-area-wrap border-tp-solid">
            <div class="row">
                <div class="col-xl-3 col-lg-6 col-md-5 offset-xl-2">
                    <div class="section-title mb-md-0">
                        <h2 class="title">Newsletter</h2>
                        <p>Sign up to receive the best offers</p>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-6 col-md-7 align-self-center offset-xl-1">
                    <div class="input-group newslatter-wrap">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fa fa-envelope"></i></span>
                        </div>
                        <input type="text" class="form-control" placeholder="Email">
                        <div class="input-group-append">
                            <button class="btn btn-yellow" type="button">Subscribe</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> -->
<!-- newslatter area End -->
@endsection

@push('script')
@endpush