@extends('front.layouts.master')
@section('title','Home')
@push('style')
@endpush
@section('content')
<!-- breadcrumb area start -->
<div class="breadcrumb-area jarallax" style="background-image:url(assets/img/bg/1.png);">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-inner">
                    <h1 class="page-title">Blog</h1>
                    <ul class="page-list">
                        <li><a href="{{ url()->previous()}}">Home</a></li>
                        <li>Blog</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb area End -->

<!-- blog area start -->
<div class="blog-list-area pd-top-120">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-4 col-md-6">
                <div class="single-blog">
                    <div class="thumb">
                        <img src="assets/img/blog/1.png" alt="blog">
                        <a class="tag" href="#">Europe</a>
                    </div>
                    <div class="single-blog-details">
                        <p class="date">19 September 2019</p>
                        <h4 class="title"><a href="#">Why You Shouldn’t Ride France.</a></h4>
                        <p class="content">Praesent eu dolor eu orci vehicula euismod. Vivamus sed sollicitudin libero, vel malesuada</p>
                        <a class="btn-read-more" href="blog-details.html"><span>Read More<i class="la la-arrow-right"></i></span></a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="single-blog">
                    <div class="thumb">
                        <img src="assets/img/blog/2.png" alt="blog">
                        <a class="tag" href="#">Africa</a>
                    </div>
                    <div class="single-blog-details">
                        <p class="date">19 September 2019</p>
                        <h4 class="title"><a href="#">Aliquam faucibus, nec commodo</a></h4>
                        <p class="content">Praesent eu dolor eu orci vehicula euismod. Vivamus sed sollicitudin libero, vel malesuada</p>
                        <a class="btn-read-more" href="blog-details.html"><span>Read More<i class="la la-arrow-right"></i></span></a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="single-blog">
                    <div class="thumb">
                        <img src="assets/img/blog/3.png" alt="blog">
                        <a class="tag" href="#">Europe</a>
                    </div>
                    <div class="single-blog-details">
                        <p class="date">19 September 2019</p>
                        <h4 class="title"><a href="#">Why You Shouldn’t Ride France.</a></h4>
                        <p class="content">Praesent eu dolor eu orci vehicula euismod. Vivamus sed sollicitudin libero, vel malesuada</p>
                        <a class="btn-read-more" href="blog-details.html"><span>Read More<i class="la la-arrow-right"></i></span></a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="single-blog">
                    <div class="thumb">
                        <img src="assets/img/blog/4.png" alt="blog">
                        <a class="tag" href="#">Iseland</a>
                    </div>
                    <div class="single-blog-details">
                        <p class="date">19 September 2019</p>
                        <h4 class="title"><a href="#">Duis pretium gravida enim, vel maximus</a></h4>
                        <p class="content">Praesent eu dolor eu orci vehicula euismod. Vivamus sed sollicitudin libero, vel malesuada</p>
                        <a class="btn-read-more" href="blog-details.html"><span>Read More<i class="la la-arrow-right"></i></span></a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="single-blog">
                    <div class="thumb">
                        <img src="assets/img/blog/5.png" alt="blog">
                        <a class="tag" href="#">Europe</a>
                    </div>
                    <div class="single-blog-details">
                        <p class="date">19 September 2019</p>
                        <h4 class="title"><a href="#">Duis pretium gravida enim</a></h4>
                        <p class="content">Praesent eu dolor eu orci vehicula euismod. Vivamus sed sollicitudin libero, vel malesuada</p>
                        <a class="btn-read-more" href="blog-details.html"><span>Read More<i class="la la-arrow-right"></i></span></a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="single-blog">
                    <div class="thumb">
                        <img src="assets/img/blog/6.png" alt="blog">
                        <a class="tag" href="#">Europe</a>
                    </div>
                    <div class="single-blog-details">
                        <p class="date">19 September 2019</p>
                        <h4 class="title"><a href="#">Why You Shouldn’t Ride France.</a></h4>
                        <p class="content">Praesent eu dolor eu orci vehicula euismod. Vivamus sed sollicitudin libero, vel malesuada</p>
                        <a class="btn-read-more" href="blog-details.html"><span>Read More<i class="la la-arrow-right"></i></span></a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="single-blog">
                    <div class="thumb">
                        <img src="assets/img/blog/7.png" alt="blog">
                        <a class="tag" href="#">Europe</a>
                    </div>
                    <div class="single-blog-details">
                        <p class="date">19 September 2019</p>
                        <h4 class="title"><a href="#">Etiam convallis elementum sapien</a></h4>
                        <p class="content">Praesent eu dolor eu orci vehicula euismod. Vivamus sed sollicitudin libero, vel malesuada</p>
                        <a class="btn-read-more" href="blog-details.html"><span>Read More<i class="la la-arrow-right"></i></span></a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="single-blog">
                    <div class="thumb">
                        <img src="assets/img/blog/8.png" alt="blog">
                        <a class="tag" href="#">Asia</a>
                    </div>
                    <div class="single-blog-details">
                        <p class="date">19 September 2019</p>
                        <h4 class="title"><a href="#">Duis porta, ligula rhoncus euismod</a></h4>
                        <p class="content">Praesent eu dolor eu orci vehicula euismod. Vivamus sed sollicitudin libero, vel malesuada</p>
                        <a class="btn-read-more" href="blog-details.html"><span>Read More<i class="la la-arrow-right"></i></span></a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="single-blog">
                    <div class="thumb">
                        <img src="assets/img/blog/9.png" alt="blog">
                        <a class="tag" href="#">Europe</a>
                    </div>
                    <div class="single-blog-details">
                        <p class="date">19 September 2019</p>
                        <h4 class="title"><a href="#">Aliquam faucibus, nec commodo</a></h4>
                        <p class="content">Praesent eu dolor eu orci vehicula euismod. Vivamus sed sollicitudin libero, vel malesuada</p>
                        <a class="btn-read-more" href="blog-details.html"><span>Read More<i class="la la-arrow-right"></i></span></a>
                    </div>
                </div>
            </div>
            <div class="col-lg-12  text-md-center text-left">
                <div class="tp-pagination text-md-center text-left d-inline-block mt-4">
                    <ul>
                        <li><a class="prev page-numbers" href="#"><i class="la la-long-arrow-left"></i></a></li>
                        <li><span class="page-numbers">1</span></li>
                        <li><span class="page-numbers current">2</span></li>
                        <li><a class="page-numbers" href="#">3</a></li>
                        <li><a class="page-numbers" href="#">4</a></li>
                        <li><a class="next page-numbers" href="#"><i class="la la-long-arrow-right"></i></a></li>
                    </ul>                          
                </div>
            </div>
        </div>
    </div>
</div>
<br>
<!-- blog area End -->

<!-- newslatter area Start -->
<!-- <div class="newslatter-area pd-top-110">
    <div class="container">
        <div class="newslatter-area-wrap border-tp-solid">
            <div class="row">
                <div class="col-xl-3 col-lg-6 col-md-5 offset-xl-2">
                    <div class="section-title mb-md-0">
                        <h2 class="title">Newsletter</h2>
                        <p>Sign up to receive the best offers</p>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-6 col-md-7 align-self-center offset-xl-1">
                    <div class="input-group newslatter-wrap">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fa fa-envelope"></i></span>
                        </div>
                        <input type="text" class="form-control" placeholder="Email">
                        <div class="input-group-append">
                            <button class="btn btn-yellow" type="button">Subscribe</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> -->
<!-- newslatter area End -->
@endsection


@push('script')
@endpush