@extends('front.layouts.master')
@section('title', 'Login')
@push('style')
        <!-- main css -->
        <link rel="stylesheet" href="{{ asset('assets/build/css/intlTelInput.css') }}">
        <link rel="stylesheet"
            href="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css" />
        <style>
            .copyright-inner .privacypolicy {
                text-align: center;
                font-size: 14px;
                letter-spacing: 0.28px;
                color: #ffffff;
                line-height: 44px;
            }

            .login-password {
                display: none;
            }
            .responsive {
              width: 100%;
              height: auto;
            }
            .dropdown-toggle::after{
                    display: none !important;
                }
            #dropdownHover a:hover{
                color: black !important;
            }

            .input-group .custom-select {
                background-color: #f8f8f8;
                border: 1px solid #f8f8f8;
                height: 45px;
                width: 100%;
                padding: 0 18px;
                color: var(--paragraph-color);
            }

            .tab-pane .shadow {
                box-shadow: 0 .5rem 1rem rgba(0, 0, 0, .15) !important;
            }

            .custom-tabs li a.active {
                background-color: #f3941e !important;
                color: #ffffff !important;
                border-radius: 0;
                font-family: var(--body-font) !important;
            }

            .type-number-login {
                padding-left: 50px !important;
            }

            /* #phone {
                padding-left: 50px !important;
            } */

            .user-details-title {
                font-family: "Poppins", sans-serif;
            }

            .login-form-wrap .single-input-wrap input {
                padding: 0px 18px !important;
            }

            .custom-tabs li a {
                color: #071c55;
                background: #f8f8f8;
                font-family: var(--body-font) !important;
                border: 0 !important;
                position: relative;
                border-radius: 0 !important;
            }

            .iti__country-name {
                color: black;
            }

            .box-account {
                color: #fff;
                padding: 20px;
                display: none;
                margin-top: 20px;
            }

            .agent_input label {
                color: #071c55 !important;
            }

            .user-design .thumb {
                height: 100%;
                margin-top: 225px;
                min-height: 547px;
                border-radius: 10px;
                margin-left: 30px;
                background-image: url("assets/img/loginleftimg.jpg");
                background-size: 100% 100%;
            }

            .hide-otp-personal {
                display: none;
            }

            .hide-otp-company {
                display: none;
            }

            .hide-email-otp-company {
                display: none;
            }

            .hide-otp-user {
                display: none;
            }

            .hide-email-otp-user {
                display: none;
            }

            .hide-otp-login {
                display: none;
            }

            .hide-email-otp-personal {
                display: none;
            }

            .user-account-btn .btn-yellow {
                border-radius: 31px !important;
                border-top-left-radius: 0 !important;
                border-bottom-left-radius: 0 !important;
            }

            .login-form-wrap h4 {
                font-family: var(--heading-font) !important;
            }
            .topmargin{
                margin-top: 100px !important;
            }
            .mt-3, .my-3 {
                margin-top: 0.5rem!important;
                margin-bottom: 1rem!important;
            }
            /* Chrome, Safari, Edge, Opera */
            input::-webkit-outer-spin-button,
            input::-webkit-inner-spin-button {
              -webkit-appearance: none;
              margin: 0;
            }

            /* Firefox */
            input[type=number] {
              -moz-appearance: textfield;
            }
            .lttr-space{
                 letter-spacing: 14vh;
                 text-align:center;"
            }

            @media only screen and (max-width:1024px){
                .navbar-area .nav-container .navbar-collapse .navbar-nav{
                    margin-top: 33px !important;
                    display: block !important;
                }
                .navbar-area .nav-container .desktop-logo img{
                    width: 84px !important;
                    margin-left: 15px !important;
                    margin-right: -25px !important;
                }
                .navbar-area .nav-container .navbar-collapse .navbar-nav li a{
                    font-size: 13px !important;
                }
                .btn {
                    font-size: 12px !important;
                    height: 50px !important;
                    padding: 0 11px !important;
                }
            }

            @media only screen and (max-width: 1280px){
                .navbar-area .nav-container .navbar-collapse .navbar-nav{
                    margin-top: 30px !important;
                }
                .navbar-area .nav-container .desktop-logo img{
                    margin-left: -15px !important;
                    margin-right: -50px !important;
                }
            }

            @media only screen and (max-width: 1150px){
                .btn {
                    font-size: 14px !important;
                    height: 50px !important;
                    padding: 0 4px !important;
                }
                .navbar-area .nav-container .desktop-logo img {
                    margin-left: -50px !important;
                    margin-right: -90px !important;
                }
                .navbar-area .nav-container .navbar-collapse .navbar-nav{
                    margin-right: -140px !important;
                }
            }

             @media only screen and (max-width: 1186px){
                .btn {
                    font-size: 12px !important;
                    height: 50px !important;
                    padding: 0 4px !important;
                }
                .navbar-area .nav-container .desktop-logo img {
                    margin-left: -60px !important;
                    margin-right: -90px !important;
                }
                .navbar-area .nav-container .navbar-collapse .navbar-nav{
                    margin-right: -90px !important;
                }
            }

            @media only screen and (max-width: 600px) {
                  .lttr-space{
                     letter-spacing: 5vh;
                     text-align:center;"
                }
            }
            @media only screen and (max-width: 575px){
                .lttr-space{
                     letter-spacing: 5vh;
                     text-align:center;"
                }
            }
            @media only screen and (max-width: 1550px){
                .lttr-space{
                     letter-spacing: 5vh;
                     text-align:center;"
                }
            }

        </style>
@endpush
@section('content')
@if(Auth::guard('webagent')->user() || Auth::guard('webcustomer')->user() || Auth::guard('webmember')->user())
<script>window.location = "{{route('home')}}";</script>
@endif
    <!-- breadcrumb area start -->
    <!-- <div class="breadcrumb-area jarallax" style="background-image:url(public/assets/img/bg/1.jpg); height:50px;">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb-inner">
                        
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <!-- breadcrumb area End -->
    <!--Start Login And Sign Up Area-->
    <div class="signUp-popup" id="signUp-popup">
        <div class="login-register-popup-wrap">
            <div class="row no-gutters">
                <div class="col-lg-6 user-design">
                    <div class="thumb" style="height: 50px;">
                        <!-- <img src="assets/img/loginleftimg.jpg" class="w-100 h-100" alt="img"> -->
                    </div>
                </div>
                <div class="col-lg-6 topmargin">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs custom-tabs w-100 d-flex justify-content-center my-2 mt-3 border-0">
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#Login">Login</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#signup">Sign Up</a>
                        </li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div id="Login" class="container tab-pane fade">
                            <form class="login-form-wrap my-5" id="Loginform" action="{{ route('front.login') }}"
                                method="post">
                                @csrf
                                <h4 class="text-center">Login</h4>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="single-input-wrap style-two">
                                            <div class="input-group">
                                                <select class="custom-select" name="contry" id="logincontry" required
                                                    data-bv-notempty="true"
                                                    data-bv-notempty-message="Contry is required">
                                                    <option value="">Choose Contry...</option>
                                                    <option value="india">India</option>
                                                    <!-- <option value="other">Other</option> -->
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 type" style="display: none;">
                                        <div class="single-input-wrap style-two">
                                            <div class="input-group">
                                                <select class="custom-select" name="type" id="type" required
                                                    data-bv-notempty="true"
                                                    data-bv-notempty-message="Choose user is required">
                                                    <option value="">Choose User...</option>
                                                    <option value="employee">Employee</option>
                                                    <option value="agent">Agent</option>
                                                    <option value="member">Member</option>
                                                    <option value="customer">User</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-9 col-9 loginContact" style="display: none">
                                        <div class="single-input-wrap style-two">
                                            <input id="phone" type="text" min="0" maxlength="10" minlength="10" name="contact" required placeholder="Mobile Number" class="type-number-login">
                                        </div>
                                    </div>
                                    <div class="col-md-9 col-9 loginEmail" style="display: none;">
                                        <div class="single-input-wrap style-two">
                                            <input id="email" type="email" name="email" required placeholder="E-mail" class="type-number-login">
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-3 my-auto text-center loginGetOtp" style="display: none">
                                        <div class="single-input-wrap style-two">
                                            <button class="btn-sm btn-yellow" id="getOtp">Get OTP</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 loginOtp" style="display: none">
                                        <label class="single-input-wrap style-two">
                                            <div class="row d-flex justify-content-center">
                                                <div class="col-md-12 col-12">
                                                    <div class="single-input-wrap style-two">
                                                        <input type="text" min="0" id="otp" maxlength="4" minlength="4" name="otp" required placeholder="0000" class="type-number-login lttr-space">
                                                    </div>
                                                </div>
                                            </div>
                                        </label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 single-input-wrap style-two loginPass">
                                        <input type="password" id="loginPass" placeholder="Password" name="password" readonly required data-bv-notempty="true" data-bv-notempty-message="Password is required">
                                        <span class="single-input-title" style="height: 36px;"><i class="fa fa-lock" style="margin-right: 20px;padding: 5px;"></i></span>
                                        <span><small>password enbled after verify OTP</small></span>
                                    </div>
                                </div>
                                <div class="single-input-wrap style-two">
                                    <a class="" href="{{ url('forgotpass') }}">Forget Password</a>
                                </div>
                                <div class="col-12">
                                    <input class="btn btn-yellow mt-3 text-center" type="submit" value="Sign In">
                                </div>
                                <!-- <div class="sign-in-btn">I already have an account. <a href="#">Sign In</a></div> -->
                                <div class="social-wrap">
                                    <p>&nbsp;</p>
                                    <ul class="social-icon">
                                        <li>
                                            <a class="facebook" href="https://www.facebook.com/JagJoyu" target="_blank"><i class="fa fa-facebook  "></i></a>
                                        </li>
                                        <li>
                                            <a class="twitter" href="https://twitter.com/JagJoyu" target="_blank"><i class="fa fa-twitter"></i></a>
                                        </li>
                                        <li>
                                            <a class="pinterest" href="https://www.instagram.com/jagjoyuts/" target="_blank"><i class="fa fa-instagram"></i></a>
                                        </li>
                                        <li>
                                            <a class="pinterest" href="https://www.youtube.com/channel/UCciZJ7X7weZiRIG6KOHOj9w" target="_blank"><i class="fa fa-youtube"></i></a>
                                        </li>
                                    </ul>
                                </div>
                            </form>
                        </div>
                        <div id="signup" class="container tab-pane active show">
                            <div class="col-12">
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs custom-tabs border-0">
                                    <li class="nav-item">
                                        <a class="nav-link font-weight-bold" data-toggle="tab"
                                            href="#agent-user">Agent</a>
                                    </li>
                                    <li class="nav-item font-weight-bold">
                                        <a class="nav-link active show" data-toggle="tab" href="#User-users">New User</a>
                                    </li>
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div id="agent-user" class="container tab-pane p-0">
                                        <br>
                                        <div class="user-settings bg-white tp-form-wrap-one shadow mb-3">
                                            <h3 class="user-details-title">Agent</h3>
                                            <!--Start Agent User Radio Button-->
                                            <div>
                                                <label><input type="radio" class="" name="agent_form"
                                                        value="Personal">&nbsp;&nbsp;Personal</label>
                                                <label><input type="radio" class="" name="agent_form"
                                                        value="Company">&nbsp;&nbsp;Company</label>
                                            </div>
                                            <div class="Personal box-account p-0">
                                                <form action="{{ route('agent.signup') }}" method="post"
                                                    id="agentPersonal" class="tp-form-wrap ">
                                                    @csrf
                                                    <input type="hidden" value="personal" name="personal">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="single-input-wrap style-two">
                                                                <div class="input-group">
                                                                    <select class="custom-select" name="contry" id="personalcontry" required
                                                                        data-bv-notempty="true"
                                                                        data-bv-notempty-message="Contry is required">
                                                                        <option value="">Choose Country...</option>
                                                                        <option value="india">India</option>
                                                                        <!-- <option value="other">Other</option> -->
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label class="single-input-wrap style-two">
                                                                <span class="single-input-title">First
                                                                    Name</span>
                                                                <input type="text" id="personal_fname" name="first_name"
                                                                    placeholder="First Name" class="input-box" required
                                                                    data-bv-notempty="true"
                                                                    data-bv-notempty-message="Fisrt Name is required">
                                                            </label>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label class="single-input-wrap style-two">
                                                                <span class="single-input-title">Last
                                                                    Name</span>
                                                                <input type="text" name="last_name" id="personal_lname" placeholder="Last Name"
                                                                    class="input-box" required data-bv-notempty="true"
                                                                    data-bv-notempty-message="Last Name is required">
                                                            </label>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label class="single-input-wrap style-two">
                                                                <span class="single-input-title">Contact</span>
                                                                <input id="phone1" type="text" name="contact_details"
                                                                    class="input-box type-number-personal" placeholder="Mobile Number"
                                                                    min="0" maxlength="10" minlength="10" required
                                                                data-bv-notempty="true"
                                                                data-bv-notempty-message="Contact required"
                                                                data-bv-remote="true" data-bv-remote-type="GET"
                                                                data-bv-remote-url="{{ url('customer/allphone') }}/All"
                                                                data-bv-remote-message="Opps ! Contact Already Exist">
                                                            </label>
                                                        </div>
                                                        <div class="col-md-3 my-auto text-center personalGetOtp" style="display: none;cursor:pointer;">
                                                            <a class="btn-sm btn-yellow" id="personalGetOtp">Get OTP</a>
                                                        </div>
                                                        <div class="col-md-12 personalOtp" style="display: none">
                                                            <label class="single-input-wrap style-two">
                                                                <div class="row d-flex justify-content-center">
                                                                    <div class="col-md-12 col-12">
                                                                        <div class="single-input-wrap style-two">
                                                                            <input type="text" min="0" id="personalotp" maxlength="4" minlength="4" name="contactOtp" required style="letter-spacing: 14vh;text-align:center;" placeholder="0000" class="type-number-login" data-bv-notempty="true" data-bv-notempty-message="Contact OTP is required">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </label>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label class="single-input-wrap style-two">
                                                                <span class="single-input-title">Email Address</span>
                                                                <input type="email" id="personalEmail" name="company_email" placeholder="E-mail" style="text-transform: lowercase" class="input-box type-email-personal" required data-bv-notempty="true"data-bv-notempty-message="E-mail is required">
                                                            </label>
                                                        </div>
                                                        <div class="col-md-3 my-auto text-center personalEmailGetOtp" style="display: none;cursor:pointer;">
                                                            <a class="btn-sm btn-yellow" id="personalEmailGetOtp">Get OTP</a>
                                                        </div>
                                                        <div class="col-md-12 personalEmailOtp" style="display: none">
                                                            <label class="single-input-wrap style-two">
                                                                <div class="row d-flex justify-content-center">
                                                                    <div class="col-md-12 col-12">
                                                                        <div class="single-input-wrap style-two">
                                                                            <input type="text" min="0" id="personalemailotp" maxlength="4" minlength="4" name="emailOtp" required style="letter-spacing: 14vh;text-align:center;" placeholder="0000" class="type-number-login" data-bv-notempty="true" data-bv-notempty-message="E-mail OTP is required">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </label>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <label class="single-input-wrap style-two">
                                                                <span class="single-input-title">PAN No
                                                                </span>
                                                                <input type="text" name="company_pan" placeholder="PAN No" onchange="validatePanNumber(this)"
                                                                    class="input-box" required data-bv-notempty="true"
                                                                    data-bv-notempty-message="PAN No is required">
                                                                    <span style="display: none; color:red; font-size: 85%;" id="pancard">Please enter valid Pan number</span>
                                                            </label>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <label class="single-input-wrap style-two">
                                                                <span class="single-input-title">Registration Fees
                                                                </span>
                                                                <input type="text" disabled="" name="membership_fee" value="1000" class="input-box">
                                                            </label>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label class="single-input-wrap style-two">
                                                                <span class="single-input-title">Password</span>
                                                                <input type="password" minlength="6" name="password"
                                                                    class="input-box" placeholder="Password" required
                                                                    data-bv-notempty="true"
                                                                    data-bv-notempty-message="Password is required"
                                                                    data-bv-identical="true"
                                                                    data-bv-identical-field="c_password"
                                                                    data-bv-identical-message="The password and its confirm password does not match.">
                                                            </label>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label class="single-input-wrap style-two">
                                                                <span class="single-input-title">Confirm Password
                                                                </span>
                                                                <input type="password" name="c_password" class="input-box"
                                                                    minlength="6" required data-bv-notempty="true"
                                                                    data-bv-notempty-message="Confirm Password is required"
                                                                    data-bv-identical="true"
                                                                    data-bv-identical-field="password"
                                                                    data-bv-identical-message="The password and its confirm password does not match.">
                                                            </label>
                                                        </div>
                                                        <div class="single-input-wrap style-two">
                                                            <a class="" style="color:#000; margin-left: 15px;" href="{{ url('privacy-policy') }}">Privacy Policy</a>
                                                        </div>
                                                        <span id="personalAgent" style="color: red;"></span>
                                                        <div class="col-12">
                                                            <input class="btn btn-yellow mt-3 text-center" id="personalRegister" type="submit"
                                                                value="Register" disabled>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="Company box-account p-0">
                                                <form action="{{ route('agent.signup') }}" method="post" id="agentCompany"
                                                    class="tp-form-wrap ">
                                                    @csrf
                                                    <input type="hidden" value="company" name="company">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="single-input-wrap style-two">
                                                                <div class="input-group">
                                                                    <select class="custom-select" name="contry" id="companycontry" required
                                                                        data-bv-notempty="true"
                                                                        data-bv-notempty-message="Contry is required">
                                                                        <option value="">Choose Country...</option>
                                                                        <option value="india">India</option>
                                                                        <!-- <option value="other">Other</option> -->
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <label class="single-input-wrap style-two">
                                                                <span class="single-input-title">Company name</span>
                                                                <input type="text" name="company_name" class="input-box"
                                                                    placeholder="Company name" required
                                                                    data-bv-notempty="true"
                                                                    data-bv-notempty-message="Company name is required">
                                                            </label>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label class="single-input-wrap style-two">
                                                                <span class="single-input-title">First name</span>
                                                                <input type="text" name="first_name" id="company_fname" 
                                                                    placeholder="First name" class="input-box" required
                                                                    data-bv-notempty="true"
                                                                    data-bv-notempty-message="First name is required">
                                                            </label>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label class="single-input-wrap style-two">
                                                                <span class="single-input-title">Last name</span>
                                                                <input type="text" name="last_name" placeholder="Last name" id="company_lname"
                                                                    class="input-box" required data-bv-notempty="true"
                                                                    data-bv-notempty-message="Last name is required">
                                                            </label>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label class="single-input-wrap style-two">
                                                                <span class="single-input-title">Contact</span>
                                                                <input id="phone2" name="contact_details" type="text"
                                                                    class="input-box type-number-company" placeholder="Mobile Number"
                                                                    min="0" maxlength="10" minlength="10" required
                                                                data-bv-notempty="true"
                                                                data-bv-notempty-message="Contact required"
                                                                data-bv-remote="true" data-bv-remote-type="GET"
                                                                data-bv-remote-url="{{ url('customer/allphone') }}/All"
                                                                data-bv-remote-message="Opps ! Contact Already Exist">
                                                            </label>
                                                        </div>
                                                        <div class="col-md-3 my-auto text-center companyGetOtp" style="display: none;cursor:pointer;">
                                                            <a class="btn-sm btn-yellow" id="companyGetOtp">Get OTP</a>
                                                        </div>
                                                        <div class="col-md-12 companyOtp" style="display: none">
                                                            <label class="single-input-wrap style-two">
                                                                <div class="row d-flex justify-content-center">
                                                                    <div class="col-md-12 col-12">
                                                                        <div class="single-input-wrap style-two">
                                                                            <input type="text" min="0" id="companyotp" maxlength="4" minlength="4" name="contactOtp" required style="letter-spacing: 14vh;text-align:center;" placeholder="0000" class="type-number-login" data-bv-notempty="true" data-bv-notempty-message="Contact OTP is required">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </label>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label class="single-input-wrap style-two">
                                                                <span class="single-input-title">Company Email
                                                                </span>
                                                                <input type="email" id="companyEmail" name="company_email"
                                                                    placeholder="Company E-mail"
                                                                    pattern="[a-zA-Z0-9._%+-]+@[a-z0-9.-]+\.[a-zA-Z]{2,4}"
                                                                    style="text-transform: lowercase"
                                                                    class="input-box  type-email-company" required
                                                                    data-bv-notempty="true"
                                                                    data-bv-notempty-message="Company E-mail is required">
                                                            </label>
                                                        </div>
                                                        <div class="col-md-3 my-auto text-center companyEmailGetOtp" style="display: none;cursor:pointer;">
                                                            <a class="btn-sm btn-yellow" id="companyEmailGetOtp">Get OTP</a>
                                                        </div>
                                                        <div class="col-md-12 companyEmailOtp" style="display: none">
                                                            <label class="single-input-wrap style-two">
                                                                <div class="row d-flex justify-content-center">
                                                                    <div class="col-md-12 col-12">
                                                                        <div class="single-input-wrap style-two">
                                                                            <input type="text" min="0" id="companyemailotp" maxlength="4" minlength="4" name="emailOtp" required style="letter-spacing: 14vh;text-align:center;" placeholder="0000" class="type-number-login" data-bv-notempty="true" data-bv-notempty-message="E-mail OTP is required">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </label>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <label class="single-input-wrap style-two">
                                                                <span class="single-input-title">Official Address</span>
                                                                <input type="text" name="official_address"
                                                                    placeholder="Official Address" class="input-box"
                                                                    required data-bv-notempty="true"
                                                                    data-bv-notempty-message="Official Address is required">
                                                            </label>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <label class="single-input-wrap style-two">
                                                                <span class="single-input-title">Registration Fees</span>
                                                                <input type="text" disabled="" name="membership_fee" value="1000" class="input-box">
                                                            </label>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label class="single-input-wrap style-two">
                                                                <span class="single-input-title">Company PAN No
                                                                </span>
                                                                <input type="text" name="company_pan"
                                                                    placeholder="Company PAN No" class="input-box" required
                                                                    data-bv-notempty="true"
                                                                    data-bv-notempty-message="Company PAN No is required">
                                                            </label>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label class="single-input-wrap style-two">
                                                                <span class="single-input-title">Company GST No
                                                                </span>
                                                                <input type="text" name="company_gst_no"
                                                                    placeholder="Company GST No" class="input-box" required
                                                                    data-bv-notempty="true"
                                                                    data-bv-notempty-message="Company GST No is required">
                                                            </label>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label class="single-input-wrap style-two">
                                                                <span class="single-input-title">Password</span>
                                                                <input type="password" minlength="6" name="password"
                                                                    class="input-box" placeholder="Password" required
                                                                    data-bv-notempty="true"
                                                                    data-bv-notempty-message="Password is required"
                                                                    data-bv-identical="true"
                                                                    data-bv-identical-field="c_password"
                                                                    data-bv-identical-message="The password and its confirm password does not match.">
                                                            </label>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label class="single-input-wrap style-two">
                                                                <span class="single-input-title">Confirm Password
                                                                </span>
                                                                <input type="password" name="c_password" class="input-box"
                                                                    minlength="6" required data-bv-notempty="true"
                                                                    data-bv-notempty-message="Confirm Password is required"
                                                                    data-bv-identical="true"
                                                                    data-bv-identical-field="password"
                                                                    data-bv-identical-message="The password and its confirm password does not match.">
                                                            </label>
                                                        </div>
                                                        <span id="companyError" style="color: red;"></span>
                                                        <div class="col-12" >
                                                            <input class="btn btn-yellow mt-3 text-center company_regi" id="company_regi" type="submit"
                                                                value="Register">
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                            <!--End Agent User Radio Button-->
                                        </div>
                                    </div>
                                    <div id="User-users" class="container tab-pane fade active show p-0">
                                        <br>
                                        <div class="user-recent-view bg-white tp-form-wrap-one shadow mb-3">
                                            <h3 class=" user-details-title">New User</h3>
                                            <form action="{{ route('signup') }}" method="post" id="signUp">
                                                @csrf
                                                <input type="hidden" id="usertype" name="usertype">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="single-input-wrap style-two">
                                                            <div class="input-group">
                                                                <select class="custom-select" name="contry" id="usercontry" required
                                                                    data-bv-notempty="true"
                                                                    data-bv-notempty-message="Contry is required">
                                                                    <option value="">Choose Country...</option>
                                                                    <option value="india">India</option>
                                                                    <!-- <option value="other">Other</option> -->
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label class="single-input-wrap style-two">
                                                            <span class="single-input-title">First name</span>
                                                            <input type="text" id="user_fname" class="input-box" name="first_name"
                                                                placeholder="First Name" required data-bv-notempty="true"
                                                                data-bv-notempty-message="First Name required">
                                                        </label>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label class="single-input-wrap style-two">
                                                            <span class="single-input-title">Last name</span>
                                                            <input type="text" id="user_lname" name="last_name" class="input-box"
                                                                placeholder="Last Name" required data-bv-notempty="true"
                                                                data-bv-notempty-message="Last Name required">
                                                        </label>
                                                    </div>
                                                    <div class="col-md-6" id="contact_div">
                                                        <label class="single-input-wrap style-two">
                                                            <span class="single-input-title">Contact</span>
                                                            <input id="phone3" type="text" name="mobile_number"
                                                                class="input-box type-number-user" minlength="10"
                                                                maxlength="10" min="0" placeholder="Mobile Number" required
                                                                data-bv-notempty="true"
                                                                data-bv-notempty-message="Contac required"
                                                                data-bv-remote="true" data-bv-remote-type="GET"
                                                                data-bv-remote-url="{{ url('customer/allphone') }}/All"
                                                                data-bv-remote-message="Opps ! Contact Already Exist">
                                                        </label>
                                                    </div>
                                                    <div class="col-md-3 my-auto text-center userGetOtp" style="display: none;cursor: pointer; color: white;">
                                                        <a class="btn-sm btn-yellow" id="userGetOtp">Get OTP</a>
                                                    </div>
                                                    <div class="col-md-12 userOtp" style="display: none">
                                                        <label class="single-input-wrap style-two">
                                                            <div class="row d-flex justify-content-center">
                                                                <div class="col-md-12 col-12">
                                                                    <div class="single-input-wrap style-two">
                                                                        <input type="text" min="0" id="userotp" maxlength="4" minlength="4" name="contactOtp" required style="letter-spacing: 14vh;text-align:center;" placeholder="0000" class="type-number-login" data-bv-notempty="true" data-bv-notempty-message="Contact OTP is required">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </label>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label class="single-input-wrap style-two">
                                                            <span class="single-input-title">Email Address</span>
                                                            <input type="email" id="userEmail" name="email" placeholder="Email"
                                                                class="input-box type-email-user"
                                                                pattern="[a-zA-Z0-9._%+-]+@[a-z0-9.-]+\.[a-zA-Z]{2,4}"
                                                                style="text-transform: lowercase" required
                                                                data-bv-notempty="true"
                                                                data-bv-notempty-message="E-mail required"
                                                                data-bv-remote="true" data-bv-remote-type="GET"
                                                                data-bv-remote-url="{{ url('customer/email') }}/All"
                                                                data-bv-remote-message="Opps ! Email Already Exist">
                                                        </label>
                                                    </div>
                                                    <div class="col-md-3 my-auto text-center userEmailGetOtp" style="display: none;cursor:pointer;">
                                                        <a class="btn-sm btn-yellow text-white" id="userEmailGetOtp">Get OTP</a>
                                                    </div>
                                                    <div class="col-md-12 userEmailOtp" style="display: none">
                                                        <label class="single-input-wrap style-two">
                                                            <div class="row d-flex justify-content-center">
                                                                <div class="col-md-12 col-12">
                                                                    <div class="single-input-wrap style-two">
                                                                        <input type="text" min="0" id="useremailotp" maxlength="4" minlength="4" name="emailOtp" required style="letter-spacing: 14vh;text-align:center;" placeholder="0000" class="type-number-login" data-bv-notempty="true" data-bv-notempty-message="E-mail OTP is required">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </label>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <label class="single-input-wrap style-two">
                                                            <span class="single-input-title">Whatsapp No</span>
                                                            <input type="number" name="whats_up" minlength="10"
                                                                maxlength="10" min="0" placeholder="Whats App No."
                                                                class="input-box" required data-bv-notempty="true"
                                                                data-bv-notempty-message="Whatsapp No. required">
                                                        </label>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label class="single-input-wrap style-two">
                                                            <span class="single-input-title">Password</span>
                                                            <input type="password" name="password" minlength="6"
                                                                placeholder="Password" class="input-box" required
                                                                data-bv-notempty="true"
                                                                data-bv-notempty-message="Password required"
                                                                data-bv-identical="true"
                                                                data-bv-identical-field="c_password"
                                                                data-bv-identical-message="The password and its confirm password does not match. ">
                                                        </label>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label class="single-input-wrap style-two">
                                                            <span class="single-input-title">Confirm Password</span>
                                                            <input type="password" name="c_password" minlength="6"
                                                                placeholder="Confirm Password" class="input-box" required
                                                                data-bv-notempty="true"
                                                                data-bv-notempty-message="Confirm Password required"
                                                                data-bv-identical="true" data-bv-identical-field="password"
                                                                data-bv-identical-message="The password and its confirm password does not match. ">
                                                        </label>
                                                    </div>
                                                    <span id="otpValidation" style="color: red;"></span>
                                                    <div class="col-12" >
                                                        <input class="btn btn-yellow mt-3 text-center user_regi" id="user_regi" type="submit"
                                                            value="Register">
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--End Login And Sign Up Area-->
    <!-- newslatter area Start -->
    <!-- <div class="newslatter-area pd-top-120">
        <div class="container">
            <div class="newslatter-area-wrap border-tp-solid">
                <div class="row">
                    <div class="col-xl-3 col-lg-6 col-md-5 offset-xl-2">
                        <div class="section-title mb-md-0">
                            <h2 class="title">Newsletter</h2>
                            <p>Sign up to receive the best offers</p>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-6 col-md-7 align-self-center offset-xl-1">
                        <div class="input-group newslatter-wrap">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fa fa-envelope"></i></span>
                            </div>
                            <input type="text" class="form-control" placeholder="Email">
                            <div class="input-group-append user-account-btn">
                                <button class="btn btn-yellow" type="button">Subscribe</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <!-- newslatter area End -->
@endsection

@push('script')
    {{-- <script src="{{ asset('assets/js/jquery-2.2.4.min.js') }}"></script>
    <script src="{{ asset('assets/js/popper.min.js') }}"></script>
    <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.magnific-popup.js') }}"></script>
    <script src="{{ asset('assets/js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('assets/js/wow.min.js') }}"></script>
    <script src="{{ asset('assets/js/slick.js') }}"></script>
    <script src="{{ asset('assets/js/waypoints.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.counterup.min.js') }}"></script>
    <script src="{{ asset('assets/js/imagesloaded.pkgd.min.js') }}"></script>
    <script src="{{ asset('assets/js/isotope.pkgd.min.js') }}"></script>
    <script src="{{ asset('assets/js/swiper.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.nice-select.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('assets/js/jarallax.min.js') }}"></script> --}}
    <!-- main js -->
    {{-- <script src="{{ asset('assets/js/main.js') }}"></script> --}}
    <!--Start Agent Radio Button Jquery-->
    <script type="text/javascript"
        src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js"></script>
    <script>
           $(document).on('click','.company_regi',function(){
            var companyOtp = $('#companyotp').val();
            if (companyOtp == '') {
                $('#companyError').html('please verify OTP.');
                $('.company_regi').prop('disabled',true);
                return false;
                // return false;
            }
        });
        $(document).ready(function() {
            $('input[type="radio"]').click(function() {
                var inputValue = $(this).attr("value");
                var targetBox = $("." + inputValue);
                $(".box-account").not(targetBox).hide();
                $(targetBox).show();
            });
        });

    </script>
    <!--End Agent Radio Button Jquery-->
    <!--Start User Login Get OTP input Jquery-->
    {{-- <script>
        $(document).ready(function() {
            $(".single-input-wrap .type-number-login").keypress(function() {
                $(".hide-otp-login").fadeIn(3000);
            });
        });

    </script> --}}
    <!--End User Login Get OTP input Jquery-->
    <!--Start User Verify OTP input Jquery-->
    {{-- <script>
        $(document).ready(function() {
            $(".single-input-wrap .verify-otp").click(function() {
                $(".login-password").fadeIn(3000);
            });
        });

    </script> --}}
    <!--End User Verify OTP input Jquery-->
    <!--Start Agent Personal Get OTP input Jquery-->
    {{-- <script>
        $(document).ready(function() {
            $(".single-input-wrap .type-number-personal").keypress(function() {
                $(".hide-otp-personal").fadeIn(3000);
            });
        });

    </script> --}}
    <!--End Agent Personal Get OTP input Jquery-->
    <!--Start Agent Company Get OTP input Jquery-->
    {{-- <script>
        $(document).ready(function() {
            $(".single-input-wrap .type-number-company").keypress(function() {
                $(".hide-otp-company").fadeIn(3000);
            });
        });

    </script> --}}
    <!--End Agent Company Get OTP input Jquery-->
    <!--Start User Get OTP input Jquery-->
    {{-- <script>
        $(document).ready(function() {
            $(".single-input-wrap .type-number-user").keypress(function() {
                $(".hide-otp-user").fadeIn(3000);
            });
        });

    </script> --}}
    <!--End User Get OTP input Jquery-->
    <!--Start User Get Email OTP input Jquery-->
    {{-- <script>
        $(document).ready(function() {
            $(".single-input-wrap .type-email-user").keypress(function() {
                $(".hide-email-otp-user").fadeIn(3000);
            });
        });

    </script> --}}
    <!--End User Get Email OTP input Jquery-->
    <!--Start Agent Company Get OTP Email input Jquery-->
    {{-- <script>
        $(document).ready(function() {
            $(".single-input-wrap .type-email-company").keypress(function() {
                $(".hide-email-otp-company").fadeIn(3000);
            });
        });

    </script> --}}
    <!--End Agent Company Get OTP Email input Jquery-->
    <!--Start Agent Personal Get OTP Email input Jquery-->
    {{-- <script>
        $(document).ready(function() {
            $(".single-input-wrap .type-email-personal").keypress(function() {
                $(".hide-email-otp-personal").fadeIn(3000);
            });
        });

    </script> --}}
    <!--End Agent Personal Get OTP Email input Jquery-->
    {{-- <script src="{{ asset('public/assets/build/js/intlTelInput.js') }}"></script>
    <script>
        var input = document.querySelector("#phone");
        var input1 = document.querySelector("#phone1");
        var input2 = document.querySelector("#phone2");
        var input3 = document.querySelector("#phone3");
        window.intlTelInput(input, {
            
            preferredCountries: ['in'],
            
            utilsScript: "public/assets/build/js/utils.js",
        });
        window.intlTelInput(input1, {
            
            preferredCountries: ['in'],
           
            utilsScript: "public/assets/build/js/utils.js",
        });
        window.intlTelInput(input2, {
           
            preferredCountries: ['in'],
           
            utilsScript: "public/assets/build/js/utils.js",
        });
        window.intlTelInput(input3, {
            
            preferredCountries: ['in'],
            
            utilsScript: "public/assets/build/js/utils.js",
        });

    </script> --}}

    <script>
        $(document).ready(function() {

            $('#Loginform').bootstrapValidator({
                
                fields: {
                    contact: {
                        message: 'contact is required',
                        validators: {
                            remote: {
                                type: 'POST',
                                url: "{{route('phone.check')}}",
                                dataType: 'json',
                                data: function(validator) {
                                    // console.log(validator.getFieldElements('contact').val());
                                    return {
                                        contact: validator.getFieldElements('contact').val(),
                                        type: validator.getFieldElements('type').val(),
                                        _token: "{{ csrf_token() }}",
                                    };
                                },
                                message: 'contact number is not registered !',
                                onSuccess: function(e, data) {
                                    // console.log(data.result.valid);
                                    if (data.result.valid==true)
                                    {
                                        $('.loginGetOtp').css('display','block');
                                    }
                                    else if (data.result.valid==false)
                                    {
                                        $('.loginGetOtp').css('display','none');
                                    }
                                    else
                                    {
                                        $('.loginGetOtp').css('display','none');
                                    }
                                },
                                onError: function(e, data) {
                                    $('.loginGetOtp').css('display','none');
                                }
                            }
                        }
                    },
                    email: {
                        message: 'E-mail is required',
                        validators: {
                            remote: {
                                type: 'POST',
                                url: "{{route('email.check')}}",
                                dataType: 'json',
                                data: function(validator) {
                                    // console.log(validator.getFieldElements('contact').val());
                                    return {
                                        email: validator.getFieldElements('email').val(),
                                        type: validator.getFieldElements('type').val(),
                                        _token: "{{ csrf_token() }}",
                                    };
                                },
                                message: 'E-mail is not registered !',
                                onSuccess: function(e, data) {
                                    // console.log(data.result.valid);
                                    if (data.result.valid==true)
                                    {
                                        $('.loginGetOtp').css('display','block');
                                    }
                                    else if (data.result.valid==false)
                                    {
                                        $('.loginGetOtp').css('display','none');
                                    }
                                    else
                                    {
                                        $('.loginGetOtp').css('display','none');
                                    }
                                },
                                onError: function(e, data) {
                                    $('.loginGetOtp').css('display','none');
                                }
                            }
                        }
                    },
                    otp: {
                        message: 'OTP is required',
                        validators: {
                            remote: {
                                type: 'POST',
                                url: "{{route('login.OTP')}}",
                                dataType: 'json',
                                data: function(validator) {
                                    // console.log(validator.getFieldElements('contact').val());
                                    return {
                                        contact: validator.getFieldElements('contact').val(),
                                        email: validator.getFieldElements('email').val(),
                                        otp: validator.getFieldElements('otp').val(),
                                        type: validator.getFieldElements('type').val(),
                                        _token: "{{ csrf_token() }}",
                                    };
                                },
                                message: 'Wrong OTP or OTP expired.',
                                onSuccess: function(e, data) {
                                    // console.log(data.result.valid);
                                    if (data.result.valid==true)
                                    {
                                        $('#loginPass').removeAttr('readonly');
                                    }
                                    else if (data.result.valid==false)
                                    {
                                        $('#loginPass').attr("readonly", "readonly");
                                    }
                                    else
                                    {
                                        $('#loginPass').attr("readonly", "readonly");
                                    }
                                },
                                onError: function(e, data) {
                                    $('#loginPass').attr("readonly", "readonly");
                                }
                            }
                        }
                    },
                }
            });
        });
        
        $(document).on('click','.user_regi',function(){
            var otp = $('#userotp').val();
            if (otp == '') {
                $('.user_regi').prop('disabled', true);
                $('#otpValidation').html('please verify OTP.');
                return false;
            }
        });

        $(document).on('keyup','#userotp',function(){
            $('#otpValidation').html('');
        });

     

        $(document).on('click','#personalRegister',function(){
            var pOtp = $('#personalotp').val();
            if (pOtp == '') {
                $('#personalRegister').prop('disabled', true);
                $('#personalAgent').html('please verify OTP.');
                return false;
            }
        });
        $(document).on('keyup','#personalotp',function(){
            $('#personalAgent').html('');
        });

        $(document).on('change','#logincontry',function(){

            var val = $(this).val();
            if(val!=="")
            {
                $('.type').show();
            }
            else
            {
                $('.type').hide();
            }


            $('.loginContact').hide();
            $('#phone').val(null);
            $('.loginEmail').hide();
            $('#email').val(null);
            $('.loginGetOtp').hide();
            $('.loginOtp').hide();
            $('#otp').val('');
            $('#type').val('');
            $('#getOtp').html('Get OTP');
            $('#loginPass').val('');
            $('#loginPass').attr("readonly", "readonly");
            
        });

        $(document).on('change','#type',function(){
            
            // $("#regi").removeAttr('disabled');
            // alert('a');

            var val = $(this).val();
            var contry = $('#logincontry').val();
            if(val!=="")
            {
                if(contry=="india")
                {
                    $('#phone').val(null);
                    $('.loginContact').show();
                    $('.loginEmail').hide();
                    $('#email').val(null);
                }
                else
                {
                    $('#email').val(null);
                    $('.loginEmail').show();
                    $('.loginContact').hide();
                    $('#phone').val(null);
                }
            }
            else
            {
                $('.loginContact').hide();
                $('#phone').val(null);
            }
            $('.loginGetOtp').hide();
            $('.loginOtp').hide();
            $('#otp').val('');
            $('#getOtp').html('Get OTP');
            $('#loginPass').val('');
            $('#loginPass').attr("readonly", "readonly");
            
        });

        $(document).on('click','#getOtp',function(){
            
            var contry = $('#logincontry').val();
             //alert(contry);
            if(contry=="india")
            {
                var val = $('#phone').val();
                var type = $('#type').val();
                if(val=="")
                {
                    alert('Please Enter Contact Number..');
                    $('.loginOtp').hide();
                    $('#otp').val('');
                }
                else
                {
                    $.ajax({
                        type: "post",
                        url: "{{route('phone.loginOtp')}}",
                        data: {
                                'mobile':val,
                                'type':type,
                                "_token": "{{ csrf_token() }}"
                            },
                        dataType:'json',
                        success: function(result){
                        }
                    });
                    $('#getOtp').html('Resend OTP');
                    $('#otp').val('');
                    $('.loginOtp').show();
                }
            }
            else
            {
                var email = $('#email').val();
                var type = $('#type').val();
                if(email=="")
                {
                    alert('Please Enter E-mail..');
                    $('.loginOtp').hide();
                    $('#otp').val('');
                }
                else
                {
                    $.ajax({
                        type: "post",
                        url: "{{route('email.loginOtp')}}",
                        data: {
                                'email':email,
                                'type':type,
                                "_token": "{{ csrf_token() }}"
                            },
                        dataType:'json',
                        success: function(result){
                        }
                    });
                    $('#getOtp').html('Resend OTP');
                    $('.loginOtp').show();
                    $('#otp').val('');
                }
            }
        });

        $(document).on('click','#companyGetOtp',function(){
            $("#company_regi").css("display", "block");
        });

         $(document).on('click','#userGetOtp',function(){
            
            $(".user_regi").css("display", "block");
        });

        

// Send OTP function for new user sign up
        $(document).on('click','#userGetOtp',function(){
            
            // $("#regi").css("display", "block");

            var contry = $('#usercontry').val();
            
            if(contry=="india")
            {
                var val = $('#phone3').val();
                var type = $('#usertype').val();
                if(val=="")
                {
                    alert('Please Enter Contact Number..');
                    $('#userGetOtp').hide();
                    $('#userotp').val('');
                }
                else
                {
                    $.ajax({
                        type: "post",
                        url: "{{route('signup.mobileotp')}}",
                        data: {
                                'mobile':val,
                                'type':type,
                                "_token": "{{ csrf_token() }}"
                            },
                        dataType:'json',
                        success: function(result){
                        }
                    });
                    $('#getOtp').html('Resend OTP');
                    $('#otp').val('');
                    $('.loginOtp').show();
                }
            }
            /*else
            {
                var email = $('#email').val();
                var type = $('#type').val();
                if(email=="")
                {
                    alert('Please Enter E-mail..');
                    $('.loginOtp').hide();
                    $('#otp').val('');
                }
                else
                {
                    $.ajax({
                        type: "post",
                        url: "{{route('email.loginOtp')}}",
                        data: {
                                'email':email,
                                'type':type,
                                "_token": "{{ csrf_token() }}"
                            },
                        dataType:'json',
                        success: function(result){
                        }
                    });
                    $('#getOtp').html('Resend OTP');
                    $('.loginOtp').show();
                    $('#otp').val('');
                }
            }*/
        });

        // Send OTP function for New Personal Agen sign up
        $(document).on('click','#personalGetOtp',function(){
            //alert($(".regi").val());
            //$(".regi").show();
            // $("#regi").css("display", "block");
            
            var contry = $('#personalcontry').val();
            
            if(contry=="india")
            {
                var val = $('#phone1').val();
                var type = $('#usertype').val();
                if(val=="")
                {
                    alert('Please Enter Contact Number..');
                    $('#personalGetOtp').hide();
                    $('#personalotp').val('');
                }
                else
                {
                    $.ajax({
                        type: "post",
                        url: "{{route('signup.mobileotp')}}",
                        data: {
                                'mobile':val,
                                'type':type,
                                "_token": "{{ csrf_token() }}"
                            },
                        dataType:'json',
                        success: function(result){
                        }
                    });
                    $('#getOtp').html('Resend OTP');
                    $('#otp').val('');
                    $('.loginOtp').show();
                }
            }
            /*else
            {
                var email = $('#email').val();
                var type = $('#type').val();
                if(email=="")
                {
                    alert('Please Enter E-mail..');
                    $('.loginOtp').hide();
                    $('#otp').val('');
                }
                else
                {
                    $.ajax({
                        type: "post",
                        url: "{{route('email.loginOtp')}}",
                        data: {
                                'email':email,
                                'type':type,
                                "_token": "{{ csrf_token() }}"
                            },
                        dataType:'json',
                        success: function(result){
                        }
                    });
                    $('#getOtp').html('Resend OTP');
                    $('.loginOtp').show();
                    $('#otp').val('');
                }
            }*/
        });

        $(document).on('change','#otp',function(){
            var val = $(this).val();
            if(val=="")
            {
                $('#loginPass').attr("readonly", "readonly");
            }
        });

        var obj = document.getElementById('otp');
        obj.addEventListener('keydown', stopCarret); 
        obj.addEventListener('keyup', stopCarret); 

        function stopCarret() {
            if (obj.value.length > 3){
                setCaretPosition(obj, 3);
            }
        }

        function setCaretPosition(elem, caretPos) {
            if(elem != null) {
                if(elem.createTextRange) {
                    var range = elem.createTextRange();
                    range.move('character', caretPos);
                    range.select();
                }
                else {
                    if(elem.selectionStart) {
                        elem.focus();
                        elem.setSelectionRange(caretPos, caretPos);
                    }
                    else
                        elem.focus();
                }
            }
        }

        function validatePanNumber(pan) {

            let pannumber = $(pan).val();
            var regex = /[a-zA-z]{5}\d{4}[a-zA-Z]{1}/;

            console.log(pannumber.match(regex));
            if (pannumber.match(regex) == null) {
                $("#pancard").show();
                $('.nextBtn').prop('disabled', true);
            }else{
                $("#pancard").hide();
                $('.nextBtn').prop('disabled', false);
            }
        }

        $('#user_fname').keypress(function (e) {
            var regex = new RegExp("^[a-zA-Z ]+$");
            var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
            if (regex.test(str)) {
                return true;
            }
            else
            {
            e.preventDefault();
            //alert('Please Enter Alphabate');
            return false;
            }
        });

        $('#user_lname').keypress(function (e) {
            var regex = new RegExp("^[a-zA-Z ]+$");
            var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
            if (regex.test(str)) {
                return true;
            }
            else
            {
            e.preventDefault();
            //alert('Please Enter Alphabate');
            return false;
            }
        });

        $('#personal_fname').keypress(function (e) {
            var regex = new RegExp("^[a-zA-Z ]+$");
            var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
            if (regex.test(str)) {
                return true;
            }
            else
            {
            e.preventDefault();
            //alert('Please Enter Alphabate');
            return false;
            }
        });

        $('#personal_lname').keypress(function (e) {
            var regex = new RegExp("^[a-zA-Z ]+$");
            var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
            if (regex.test(str)) {
                return true;
            }
            else
            {
            e.preventDefault();
            //alert('Please Enter Alphabate');
            return false;
            }
        });

        $('#company_fname').keypress(function (e) {
            var regex = new RegExp("^[a-zA-Z ]+$");
            var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
            if (regex.test(str)) {
                return true;
            }
            else
            {
            e.preventDefault();
            //alert('Please Enter Alphabate');
            return false;
            }
        });

        $('#company_lname').keypress(function (e) {
            var regex = new RegExp("^[a-zA-Z ]+$");
            var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
            if (regex.test(str)) {
                return true;
            }
            else
            {
            e.preventDefault();
            //alert('Please Enter Alphabate');
            return false;
            }
        });


    </script>
    @include('front.script.signupScript')

@endpush
