@extends('front.new-layouts.master')
@section('title',"India's No. 1 - Travel Investment Membership Plans  - Jag Joyu")
@section('content')
    <!-- navbar area end -->
    <!-- breadcrumb area start -->
    <div class="breadcrumb-area jarallax" style="background-image:url(assets/img/bg/1.png);">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb-inner">
                        <h1 class="page-title">Blog</h1>
                        <ul class="page-list">
                            <li><a href="index.html">Home</a></li>
                            <li>Blog</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- breadcrumb area End -->

    <!-- blog area start -->
    <div class="blog-list-area pd-top-120">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-4 col-md-6">
                    <div class="single-blog">
                        <div class="thumb">
                            <img src="assets/img/blog/1.png" alt="blog">
                            <a class="tag" href="#">Europe</a>
                        </div>
                        <div class="single-blog-details">
                            <p class="date">19 September 2019</p>
                            <h4 class="title"><a href="#">Why You Shouldn’t Ride France.</a></h4>
                            <p class="content">Praesent eu dolor eu orci vehicula euismod. Vivamus sed sollicitudin libero, vel malesuada</p>
                            <a class="btn-read-more" href="blog-details.html"><span>Read More<i class="la la-arrow-right"></i></span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- blog area End -->
@endsection