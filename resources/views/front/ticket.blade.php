@extends('front.layouts.master')
@section('title', 'Inquires')
@push('style')
<style type="text/css">
    .description {
        margin-top: 45px;
    }

    .description h3.title {
        margin-top: 10px !important;
    }
    .bottom_space{
        margin-bottom: -30px !important;
    }

    /****** IGNORE ******/

.copyright {
  display:block;
  margin-top: 100px;
  text-align: center;
  font-family: Helvetica, Arial, sans-serif;
  font-size: 12px;
  font-weight: bold;
  text-transform: uppercase;
}
.copyright a{
  text-decoration: none;
  color: #EE4E44;
}


/****** CODE ******/

.file-upload{display:block;text-align:center;font-family: Helvetica, Arial, sans-serif;font-size: 12px;}
.file-upload .file-select{display:block;border: 2px solid #dce4ec;color: #34495e;cursor:pointer;height:40px;line-height:40px;text-align:left;background:#FFFFFF;overflow:hidden;position:relative;}
.file-upload .file-select .file-select-button{background:#dce4ec;padding:0 10px;display:inline-block;height:40px;line-height:40px;}
.file-upload .file-select .file-select-name{line-height:40px;display:inline-block;padding:0 10px;}
.file-upload .file-select:hover{border-color:#34495e;transition:all .2s ease-in-out;-moz-transition:all .2s ease-in-out;-webkit-transition:all .2s ease-in-out;-o-transition:all .2s ease-in-out;}
.file-upload .file-select:hover .file-select-button{background:#34495e;color:#FFFFFF;transition:all .2s ease-in-out;-moz-transition:all .2s ease-in-out;-webkit-transition:all .2s ease-in-out;-o-transition:all .2s ease-in-out;}
.file-upload.active .file-select{border-color:#3fa46a;transition:all .2s ease-in-out;-moz-transition:all .2s ease-in-out;-webkit-transition:all .2s ease-in-out;-o-transition:all .2s ease-in-out;}
.file-upload.active .file-select .file-select-button{background:#3fa46a;color:#FFFFFF;transition:all .2s ease-in-out;-moz-transition:all .2s ease-in-out;-webkit-transition:all .2s ease-in-out;-o-transition:all .2s ease-in-out;}
.file-upload .file-select input[type=file]{z-index:100;cursor:pointer;position:absolute;height:100%;width:100%;top:0;left:0;opacity:0;filter:alpha(opacity=0);}
.file-upload .file-select.file-select-disabled{opacity:0.65;}
.file-upload .file-select.file-select-disabled:hover{cursor:default;display:block;border: 2px solid #dce4ec;color: #34495e;cursor:pointer;height:40px;line-height:40px;margin-top:5px;text-align:left;background:#FFFFFF;overflow:hidden;position:relative;}
.file-upload .file-select.file-select-disabled:hover .file-select-button{background:#dce4ec;color:#666666;padding:0 10px;display:inline-block;height:40px;line-height:40px;}
.file-upload .file-select.file-select-disabled:hover .file-select-name{line-height:40px;display:inline-block;padding:0 10px;}

</style>

<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.24/css/dataTables.bootstrap4.min.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.css"
    integrity="sha512-In/+MILhf6UMDJU4ZhDL0R0fEpsp4D3Le23m6+ujDWXwl3whwpucJG1PEmI3B07nyJx+875ccs+yX2CqQJUxUw=="
    crossorigin="anonymous" />
<link rel="stylesheet"
    href="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css" />
@endpush
@section('content')
<!-- Modal -->

 @if(isset($announcement->image))
<div id="LoadModel" class="modal show" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
      <div class="modal-body">
        @if (pathinfo($announcement->image, PATHINFO_EXTENSION) == 'pdf')
            <embed
                src="{{$announcement->image}}#toolbar=0&navpanes=0&scrollbar=0"
                type="application/pdf"
                frameBorder="0"
                scrolling="auto"
                height="100%"
                width="100%"
            ></embed>
        @else
            @if(isset($announcement->image))
                <img src="{{$announcement->image}}" width="100%" style="border-radius: 10px;">
            @endif
        @endif
      </div>
    </div>
  </div>
</div>
@endif

<!-- breadcrumb area start style-two-->
<div class="breadcrumb-area  jarallax">
    <div class="container">
        <br><br><br><br>
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-inner">
                    <!-- <h1 class="page-title"><a href="{{ url('/dashboard') }}"></a></h1> -->
                    <h1 class="page-title">Welcome @if($user){{$user->first_name}} {{$user->last_name}} @endif</h1>
                    
                    @if(Auth::guard('webagent')->user())
                    <ul class="page-list">
                        <li>Balance : <span>{{ Auth::guard('webagent')->user()->wallet}}</li>
                        <button type="button" class="btn btn-yellow btn-sm" data-toggle="modal" data-target="#topupModel"
                        id="btnexampleModalLong">
                        Charge Wallet
                        </button>
                    </ul>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb area End -->
@include('front.layouts.newmenu')
<!-- Modal -->

<!-- tour list area End -->
<div class="tour-list-area pd-top-60 pd-bottom-120">
    <div class="container">
        <div class="tp-main-search">
            <div class="row bottom_space">
                <div class="col-lg-2 col-md-4">
                    <div class="single-widget-search-input">
                        <select class="form-control  w-100 custom-select" id="my_category_id">
                            <option disabled selected>Select Category</option>
                            @if ($ticket_category)
                                @foreach ($ticket_category as $sub)
                                    @if ($sub)
                                    <option value="{{ $sub->id }}">{{ $sub->name }}</option>
                                    @endif
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
                <div class="col-lg-2 col-md-4">
                    <div class="single-widget-search-input">
                        <select class=" w-100 custom-select" id="my_category_sub_id">
                            <option disabled selected>Select SubCategory</option>
                        </select>
                    </div>
                </div>
                <div class="col-lg-2 col-md-4 ">
                    <div class="single-widget-search-input">
                        <select class=" w-100 custom-select" id="my_status">
                            <option value="">Inquires Status</option>
                            <option value="active">Active</option>
                            <option value="close">Close</option>
                        </select>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 ">
                    <div class="single-widget-search-input">
                        <div id="reportrange"
                            style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                            <i class="fa fa-calendar"></i>&nbsp;
                            <span></span> <i class="fa fa-caret-down"></i>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-4 order-12">
                    <button type="button" class="btn btn-yellow" data-toggle="modal" data-target="#exampleModalLong"
                        id="btnexampleModalLong">
                        Create Inquiry
                    </button>
                    
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-xl-12 col-lg-12 order-lg-12">
                <div class="tour-list-area">
                    <div class="table-responsive">
                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Categories</th>
                                    <th>Sub Categories</th>
                                    <th>Message</th>
                                    <th>Status</th>
                                    <th>Date&time</th>
                                    <th>view</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- tour list area Endf  -->
<!-- Modal -->
<div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Create Inquiry</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('front.tickets.store') }}" class="defaultForm tp-form-wrap" id="front_tickets"
                    method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="form-group col-md-6">
                            <span class="single-input-title">Select Category</span>
                            <select class="form-control w-100 custom-select" id="c_state_id" name="category" required
                                data-bv-notempty-message="Category is required">
                                <option value="">Category</option>
                                @if ($ticket_category)
                                    @foreach ($ticket_category as $sub)
                                        @if ($sub)
                                            <option value="{{ $sub->id }}">
                                                {{ $sub->name }}</option>
                                        @endif
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <span class="single-input-title">Select Sub Category</span>
                            <select class=" form-control w-100 custom-select" id="c_city_id" name="sub_category" required data-bv-notempty-message=" Sub Category is required">
                            </select>
                        </div>
                        <div class="form-group col-md-12">
                            <span class="single-input-title">Write your message.</span>
                            <textarea id="body" name="message" rows="8" class="form-control" required
                                data-bv-notempty-message="Message is required"></textarea>
                        </div>
                        <div class="form-group col-md-12">
                            <div class="file-upload">
                                <div class="file-select">
                                    <div class="file-select-button" id="fileName">Choose File</div>
                                    <div class="file-select-name" id="noFile">No file chosen...</div> 
                                    <input type="file" name="file" accept=".jpg,.jpeg,.png,.pdf" id="chooseFile">
                                </div>
                            </div>
                        </div>
                        <!-- <div class="form-group col-md-12">
                            <span class="single-input-title">File.</span>
                            <input type="file" name="file" accept=".jpg,.jpeg,.png,.pdf" class="form-control dropify"
                                data-bv-file-extension="jpg,jpeg,png,pdf" />
                        </div> -->
                        <div class="form-group col-md-12">
                            <button type="submit" name="save" class="btn btn-yellow mt-3 text-center">Send
                                Message</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>
@endsection
@push('script')

    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.24/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/js/dropify.min.js"></script>
    <script type="text/javascript"
        src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js"></script>

<script>

$(document).ready(function() {
    $('#topupform').bootstrapValidator();

});

$('#chooseFile').bind('change', function () {
  var filename = $("#chooseFile").val();
  if (/^\s*$/.test(filename)) {
    $(".file-upload").removeClass('active');
    $("#noFile").text("No file chosen..."); 
  }
  else {
    $(".file-upload").addClass('active');
    $("#noFile").text(filename.replace("C:\\fakepath\\", "")); 
  }
});
</script>
<script type="text/javascript">
    $(document).ready(function(){
            var myModal = new bootstrap.Modal(document.getElementById('LoadModel'), {})
myModal.toggle();
    });
    // $(document).on('click','.modelShow',function(){
    //     $('#LoadModel').modal('show');
    // });
</script>
    <script type="text/javascript">
        $(function() {

            var start = moment().subtract(29, 'days');
            var end = moment();

            function cb(start, end) {
                $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                var category = {};
                if ($('#my_category_id').val() != '') {
                    category['category_id'] = $('#my_category_id').val();
                }

                if ($('#my_category_sub_id').val() != '') {
                    category['sub_category_id'] = $('#my_category_sub_id').val();
                }
                if ($('#my_status').val() != '') {
                    category['status'] = $('#my_status').val();
                }
                let startDate = moment($('#reportrange').data('daterangepicker').startDate).format(
                    'YYYY-MM-DD HH:mm:ss');
                let endDate = moment($('#reportrange').data('daterangepicker').endDate).format(
                    'YYYY-MM-DD HH:mm:ss');
                if (startDate) {
                    category['start_date'] = startDate;
                }

                if (endDate) {
                    category['end_date'] = endDate;
                }
                //alert(category);
                getlead(category);
            }

            $('#reportrange').daterangepicker({
                startDate: start,
                endDate: end,
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1,
                        'month').endOf('month')]
                }
            }, cb);

            cb(start, end);

        });

        function getlead(dataSet) {
            console.log(dataSet);
            $('#example').DataTable().destroy();
            $("#example").DataTable({
                "processing": true,
                "serverSide": true,
                "ordering": false,
                "ajax": {
                    "url": "{{ url('tickets/list') }}",
                    "type": "POST",
                    "data": dataSet,
                },
                "columns": [{
                        "data": "id",
                        render: function(data, type, row, meta) {

                            return '#' + data;
                            // body...
                        }
                    },

                    {
                        "data": "id",render:function(data,type,alldata) {
                            if(alldata.ticket_categories!=null){
                                return alldata.ticket_categories.name;
                            }else{
                                return '';
                            }
                            //console.log(alldata.ticket_categories.name);
                        },
                    },
                    {
                        "data": "id"
                        ,render:function(data,type,alldata) {
                            if(alldata.ticket_sub_categories!=null){
                                return alldata.ticket_sub_categories.name;
                            }else{
                                return '';
                            }
                            //console.log(alldata.ticket_categories.name);
                        },
                    },
                    {
                        "data": "message",
                        render: function(data, type, row, mata) {
                            //console.log(row.lastmessage);
                           if (row.lastmessage) {
                            return row.lastmessage.message;
                            }else{
                            return row.message;
                            }
                        }
                    }, 
                    {
                        "data": "id",
                        render: function(data, type, row, meta) {
                            if (row.status == 'active') {
                                return '<span class="badge badge-success">' + row.status + '</span>';
                            } else {
                                return '<span class="badge badge-danger">' + row.status + '</span>';
                            }

                        }
                    },
                    {
                        "data": "created_at",
                        render: function(data, type, row, meta) {

                            return moment(data).format('DD-MM-YYYY,h:mm a');
                            // body...
                        }
                    },
                    {
                        "data": "id",
                        render: function(data, type, row, meta) {

                            var html = '<a  href="{{ url('tickets/view/') }}/' + btoa(data) +
                                '"><i class="fa fa-eye" aria-hidden="true"' +
                                'title="Detail View" style="font-size:20px"></i></a>';
                            return html;
                        }
                    },
                ]
            });
        }

        $(document).on("click", '#btnexampleModalLong', function(e) {
            $("#front_tickets")[0].reset();
            $('#front_tickets').bootstrapValidator('resetForm', true);

            var sform2 = $('#front_tickets')[0];
            $(sform2).removeClass('was-validated');
            sform2.reset();
            //console.log('hu');
            $("#front_tickets")[0].reset();
            $('#front_tickets').bootstrapValidator('resetForm', true);
        });
        $(document).ready(function() {


            $('#exampleModalLong').on('.modal.fade.show', function() {

            });

            var dataSet = {};
            getlead(dataSet);

            $('.dropify').dropify();

            $('#front_tickets').bootstrapValidator();

            $('select#c_state_id').change(function() {
                var value = $(this).val();
                var city = '';
                //console.log(value);
                statechange(value, city);
            });

            $('select#my_category_id').change(function() {
                var value = $(this).val();
                var city = '';
                newstatechange(value, city);
                var category = {};

                if (value != '') {
                    category['category_id'] = value;

                }
                if ($('#my_category_sub_id').val() != '') {
                    category['sub_category_id'] = $('#my_category_sub_id').val();
                }

                if ($('#my_status').val() != '') {
                    category['status'] = $('#my_status').val();
                }

                let startDate = moment($('#reportrange').data('daterangepicker').startDate).format(
                    'YYYY-MM-DD HH:mm:ss');
                let endDate = moment($('#reportrange').data('daterangepicker').endDate).format(
                    'YYYY-MM-DD HH:mm:ss');
                if (startDate) {
                    category['start_date'] = startDate;
                }

                if (endDate) {
                    category['end_date'] = endDate;
                }
                getlead(category);

            });

            $('select#my_category_sub_id').change(function() {
                var value = $(this).val();
                var city = '';
                //newstatechange(value, city);
                var category = {};

                if (value != '') {
                    category['sub_category_id'] = value;

                }
                if ($('#my_category_id').val() != '') {
                    category['category_id'] = $('#my_category_id').val();
                }

                if ($('#my_category_sub_id').val() != '') {
                    category['sub_category_id'] = $('#my_category_sub_id').val();
                }

                let startDate = moment($('#reportrange').data('daterangepicker').startDate).format(
                    'YYYY-MM-DD HH:mm:ss');
                let endDate = moment($('#reportrange').data('daterangepicker').endDate).format(
                    'YYYY-MM-DD HH:mm:ss');
                if (startDate) {
                    category['start_date'] = startDate;
                }

                if (endDate) {
                    category['end_date'] = endDate;
                }
                getlead(category);

            });

            $('select#my_status').change(function() {
                var value = $(this).val();
                var city = '';
                //newstatechange(value, city);
                var category = {};

                if (value != '') {
                    category['status'] = value;

                }
                if ($('#my_category_id').val() != '') {
                    category['category_id'] = $('#my_category_id').val();
                }

                if ($('#my_status').val() != '') {
                    category['status'] = $('#my_status').val();
                }
                let startDate = moment($('#reportrange').data('daterangepicker').startDate).format(
                    'YYYY-MM-DD HH:mm:ss');
                let endDate = moment($('#reportrange').data('daterangepicker').endDate).format(
                    'YYYY-MM-DD HH:mm:ss');
                if (startDate) {
                    category['start_date'] = startDate;
                }

                if (endDate) {
                    category['end_date'] = endDate;
                }
                getlead(category);
            });
        });
        function statechange(value, city) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var cat_id = value;
            $.ajax({
                url: "{{ route('front.tickets.getcat') }}",
                type: "POST",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "cat_id": cat_id
                },
                success: function(data, city) {
                    $('#c_city_id').empty();
                    $.each(data.cat, function(index, cat) {
                        $('#c_city_id').append('<option value="' + cat.id + '">' + cat.name +
                            '</option>');
                    })
                }
            })
        }

        function newstatechange(value, city) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var cat_id = value;
            $.ajax({
                url: "{{ route('front.tickets.getcat') }}",
                type: "POST",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "cat_id": cat_id
                },

                success: function(data, city) {
                    $('#my_category_sub_id').empty();
                    $.each(data.cat, function(index, cat) {
                        $('#my_category_sub_id').append('<option value="' + cat.id + '">' + cat.name +
                            '</option>');
                    })
                }
            })
        }
        // A $( document ).ready() block.
    </script>
@endpush
