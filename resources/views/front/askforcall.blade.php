@extends('front.layouts.master')
@section('title','Ask For Call')
@push('style')
<link rel="stylesheet"
    href="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css" />
<style type="text/css">
    ::-webkit-scrollbar {
    width: 10px
}

::-webkit-scrollbar-track {
    background: #eee
}

::-webkit-scrollbar-thumb {
    background: #888
}

::-webkit-scrollbar-thumb:hover {
    background: #555
}

.breadcrumb-area {
    text-align: center;
    padding: 65px 0 20px !important;
    background-size: cover;
    position: relative;
}

</style>
@endpush
@section('content')


<!-- breadcrumb area start -->
    <div class="breadcrumb-area jarallax" style="background-image:url({{ url('//front/assets/img/bg/1.jpg') }});">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb-inner">
                        <h1 class="page-title">Contact Us</h1>
                        <!-- <ul class="page-list">
                            <li><a href="index.html">Home</a></li>
                            <li>User Profile</li>
                        </ul> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- breadcrumb area End -->


    <!-- contact area End -->
    <div class="contact-area pd-top-108">
        <div class="container">

            <div class="row">
                <div class="col-xl-5 offset-xl-1 col-lg-6">
                    <div class="thumb">
                        <img src="assets/img/others/11.png" alt="img">
                    </div>
                </div>
                <div class="col-xl-5 col-lg-6">
                    <form class="tp-form-wrap" id="askforcall" action="{{route('front.save-askforcall')}}" method="POST" >
                    	 @csrf
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label class="single-input-wrap style-two">
                                    <span class="single-input-title">First Name</span>
                                    <input type="text" class="form-control" id="first_name" required="" data-bv-notempty-message="The First Name is required" name="first_name"
                        			placeholder="Enter First Name">
                                </label>
                            </div>
							<div class="form-group col-md-6">
                                <label class="single-input-wrap style-two">
                                    <span class="single-input-title">Last Name</span>
                                    <input type="text"  class="form-control" id="last_name" required="" 
                                    data-bv-notempty-message="The Last Name is required" name="last_name"
                        			placeholder="Enter Last Name">
                                </label>
                            </div>
							<div class="form-group col-lg-12">
                                <label class="single-input-wrap style-two">
                                    <span class="single-input-title">Email</span>
                                    <input type="email" id="email" class="form-control" required="" data-bv-notempty-message="The Email is required" name="email"
                        			placeholder="Enter Email">
                                </label>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="single-input-wrap style-two">
                                    <span class="single-input-title">Category</span>
                                    <select class="form-control w-100 custom-select" id="my_category_id" name="cat">
                                        <option disabled selected>Select Category</option>
                                    @if ($ticket_category)
                                        @foreach ($ticket_category as $sub)
                                            @if ($sub)
                                            <option value="{{ $sub->id }}">{{ $sub->name }}</option>
                                            @endif
                                        @endforeach
                                    @endif
                                </select>
                                </label>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="single-input-wrap style-two">
                                    <span class="single-input-title">Sub Category</span>
                                    <select class=" w-100 custom-select" name="sub_cat" id="my_category_sub_id"><option disabled selected>Select SubCategory</option></select>
                                    
                                </label>
                            </div>
                            <div class="col-md-6">
                                <label class="single-input-wrap style-two">
                                    <span class="single-input-title">Contact Number</span>
                                    <input id="ContactNumber" type="number" min="0" minlength="10" maxlength="10" class="form-control" required="" data-bv-notempty-message="The Contact Number is required" name="contactnumber"
                        			placeholder="Enter Contact Number" >
                                </label>
                            </div>
							<div class="col-md-6">
                                <label class="single-input-wrap style-two">
                                    <span class="single-input-title">Whatsapp Number</span>
                                    <input id="phone1" type="number" min="0" minlength="10" maxlength="10" class="form-control" required="" data-bv-notempty-message="The Whatsapp Number is required" name="wpnumber"
                        			placeholder="Enter Whatsapp Number">
                                </label>
                            </div>
                            {{-- <div class="col-lg-12">
                                <label class="single-input-wrap style-two">
                                    <span class="single-input-title">Appointment date and time</span>
                                    <input type="datetime-local" required="" data-bv-notempty-message="The Appointment date and time is required" name="datetime"
                        			placeholder="Enter Appointment date and time">
                                </label>
                            </div> --}}
                            <div class="col-12">
                            	<button type="submit" name="save" class="btn btn-yellow">Submit</button>
                                
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- contact area End -->
    &nbsp;
@endsection


@push('script')

<script type="text/javascript"
    src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js"></script>
<script>
$(document).ready(function() {
    $('#askforcall').bootstrapValidator();

});

$('#first_name').keypress(function (e) {
        var regex = new RegExp("^[a-zA-Z ]+$");
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (regex.test(str)) {
            return true;
        }
        else
        {
        e.preventDefault();
        alert('Please Enter Alphabate and space only');
        return false;
        }
    });

  $('#last_name').keypress(function (e) {
        var regex = new RegExp("^[a-zA-Z ]+$");
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (regex.test(str)) {
            return true;
        }
        else
        {
        e.preventDefault();
        alert('Please Enter Alphabate and space only');
        return false;
        }
    });

$('#my_category_id').change(function() {
    //alert("Hello");
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var cat_id = $("#my_category_id").val();
       // alert(cat_id);
        $.ajax({
            url: "{{ route('front.tickets.AllgetCat') }}",
            type: "POST",
            data: {
                "_token": "{{ csrf_token() }}",
                "cat_id": cat_id
            },

            success: function(data, city) {
                console.log(data);
                $('#my_category_sub_id').empty();
                $.each(data.cat, function(index, cat) {

                    $('#my_category_sub_id').append('<option value="' + cat.id + '">' + cat.name +
                        '</option>');
                })
            }
        })
    })
</script>
@endpush