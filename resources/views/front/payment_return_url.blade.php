<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>
    <!-- favicon -->
    <link rel=icon href="{{url('/front')}}/assets/img/favicon.png" sizes="20x20" type="image/png">
    <!-- Additional plugin css -->
    <link rel="stylesheet" href="{{url('/front')}}/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{url('/front')}}/assets/css/animate.css">
    <link rel="stylesheet" href="{{url('/front')}}/assets/css/magnific-popup.css">
    <link rel="stylesheet" href="{{url('/front')}}/assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="{{url('/front')}}/assets/css/slick.css">
    <link rel="stylesheet" href="{{url('/front')}}/assets/css/swiper.min.css">
    <link rel="stylesheet" href="{{url('/front')}}/assets/css/nice-select.css">
    <link rel="stylesheet" href="{{url('/front')}}/assets/css/jquery-ui.min.css">
    <!-- icons -->
    <link rel="stylesheet" href="{{url('/front')}}/assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{url('/front')}}/assets/css/themify-icons.css">
    <link rel="stylesheet" href="{{url('/front')}}/assets/css/line-awesome.min.css">
    <!-- main css -->
    <link rel="stylesheet" href="{{url('/front')}}/assets/css/style.css">
    <!-- responsive css -->
    <link rel="stylesheet" href="{{url('/front')}}/assets/css/responsive.css">

    <!--Start Custom CSS-->
    <style>
        /*This CSS Compulsary Add And Use*/
        .banner-slider-item .margin-top {
            margin-top: 80px;
        }

        @media only screen and (max-width: 1100px) {
            .banner-slider .banner-slider-item {
                height: 700px !important;
                padding: 170px 0 240px 0 !important;
                overflow: hidden !important;
            }
        }

        .custom_tab .tab-content {
            box-shadow: 0px 3px 13px #23397421 !important;
        }

        .tp-main-search {
            box-shadow: none;
        }

        .tp-holiday-plan-area.mg-top-96 {
            margin-top: 0 !important;
            padding-top: 72px !important;
        }

        .pd-top-110 {
            padding: 50px 0px !important;
        }

        .copyright-inner .privacypolicy {
            text-align: center;
            font-size: 14px;
            letter-spacing: 0.28px;
            color: #ffffff;
            line-height: 44px;
        }

        .banner-tour-package {
            visibility: hidden !important;
        }
        small.help-block {
            color: red;
        }
        .fixed {
            z-index: 9999;
        }

        .btn-outline-warning {
            color: #212529 !important;
            background-color: #ffc107 !important;
            border-color: #ffc107 !important;
        }

        .box {
            border: 4px solid #f3941e;
            border-radius: 15px;
        }
        .box-margin {
            margin-top: 30px;
            margin-left: 15px;
        }

    </style>
    <!--End Custom CSS-->
    {{-- <link rel="stylesheet" href="{{url('vendor/mckenziearts/laravel-notify/public/css/notify.css')}}" />    --}}
    @stack('style')

</head>
<body>
<!-- preloader area start -->
 <div class="preloader" id="preloader">
    <div class="preloader-inner">
        <div class="spinner">
            <div class="dot1"></div>
            <div class="dot2"></div>
        </div>
    </div>
</div>
<!-- preloader area end -->

<!-- navbar area start -->
<nav class="navbar navbar-area navbar-expand-lg nav-style-02">
    <div class="container nav-container" style="margin-top: 50px;">
        <div class="responsive-mobile-menu">
           
           
        </div>
        
        <div class="collapse navbar-collapse" id="tp_main_menu">
            <div class="logo-wrapper desktop-logo">
                <a href="{{route('home')}}" class="main-logo">
                    <img src="{{url('public/images/jj-logo.png')}}" alt="logo">
                </a>
                <a href="{{route('home')}}" class="sticky-logo">
                    <img src="{{url('public/images/jj-logo.png')}}" alt="logo">
                </a>
            </div>
            
        </div>
        
    </div>
</nav>
@section('title','Dashboard')

@section('content')

    
 
 <!--   @include('front.layouts.newmenu') -->

<!-- Start Modal for add wallet payment -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title w-100 font-weight-bold center">Add Wallet Payment</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                
            </div>
            <div class="modal-body mx-3">
                <form action="{{ url('payment/cashfreePayment')}}" method="post" id="wallet_form">
                    @csrf
                    <div class="md-form mb-5">
                        <label data-error="wrong" data-success="right">Amount</label>
                        <input type="text" required="" name="amount" class="form-control col-md-6 validate">
                        <input type="hidden" name="agent_id" value="Agt_854">
                        <input type="hidden" name="agent_name" value="Akshay Patel">
                        <input type="hidden" name="agent_email" value="akshay@xsquaretec.com">
                        <input type="hidden" name="agent_phone"  value="8866111719">
                    </div>
                    <div class="md-form mb-5">
                        <label data-error="wrong" data-success="right">Note</label>
                        <!-- <input type="text" id="form3" class="form-control col-md-6 validate"> -->
                        <textarea required="" name="note" class="form-control col-md-6 validate" rows="4"></textarea>
                    </div>
                    <div class="text-right">
                        <button type="submit" class="btn btn-yellow mt-3 text-center">Add to Wallet</button>
                        <!-- <button type="button" class="btn btn-default right" data-dismiss="modal">Close</button> -->
                    </div>
                </form>

            </div>
            <!-- <div class="modal-footer"> -->
                
            <!-- </div> -->
        </div>
    </div>
</div>
<!-- End Modal for add wallet payment -->
 <!--Start User Profile Area-->

 <div class="user-profile-area pd-top-120 mb-5 ">
    <div class="container">
        <div class="row">
            <div class="col-xl-10 col-lg-12 mx-auto">
                <div class="row">
                    <div class="col-xl-7 col-lg-8 mx-auto box">
                        <div class="tab-content user-tab-content box-margin">
                            <div class="tab-pane fade show active" id="tabs_1">
                                <div class="user-details">
                                    <!-- <h3 class="user-details-title">Profile</h3> -->
                                    <div class="tp-img-upload"> 
                                        <div class="tp-avatar-edit">
                                            <h4 class="name">Wallet Payment Details</h4>
                                        </div>
                                    </div>
                                    <form class="tp-form-wrap" action="{{$link->paymentLink}}" method="post">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label class="single-input-wrap style-two">
                                                    <span class="single-input-title"><b>Agent Name :</b> {{$result['agent_name']}}</span>
                                                    <!-- <input type="text" value="Akshay" > -->
                                                </label>
                                                <label class="single-input-wrap style-two">
                                                    <span class="single-input-title"><b>Phone Number :</b> {{$result['agent_phone']}}</span>
                                                    <!-- <input type="text" value="Patel" > -->
                                                </label>
                                                <label class="single-input-wrap style-two">
                                                    <span class="single-input-title"><b>Email :</b> {{$result['agent_email']}}</span>
                                                    <!-- <input type="text" value="India" > -->
                                                </label>
                                            
                                            </div>
                                            <div class="col-md-6">
                                                <label class="single-input-wrap style-two">
                                                    <span class="single-input-title"><b>Amount :</b> {{$result['amount']}}</span>
                                                    <!-- <input type="text" value="imshuvo97@gmail.com" > -->
                                                </label>
                                                <label class="single-input-wrap style-two">
                                                    <span class="single-input-title"><b>Note :</b> {{$result['note']}}</span>
                                                   <!--  <input type="text" value="1234567890" > -->
                                                </label>
                                            </div>
                                            <div class="form-group col-md-12">
                                                <button type="submit" name="save" class="btn btn-yellow mt-3 text-center">
                                                    Pay</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--End User Profile Area-->
<!-- Additional plugin js -->
    <script src="{{url('public/')}}/assets/js/jquery-2.2.4.min.js"></script>
    <script src="{{url('public/')}}/assets/js/popper.min.js"></script>
    <script src="{{url('public/')}}/assets/js/bootstrap.min.js"></script>
    <script src="{{url('public/')}}/assets/js/jquery.magnific-popup.js"></script>
    <script src="{{url('public/')}}/assets/js/owl.carousel.min.js"></script>
    <script src="{{url('public/')}}/assets/js/wow.min.js"></script>
    <script src="{{url('public/')}}/assets/js/slick.js"></script>
    <script src="{{url('public/')}}/assets/js/waypoints.min.js"></script>
    <script src="{{url('public/')}}/assets/js/jquery.counterup.min.js"></script>
    <script src="{{url('public/')}}/assets/js/imagesloaded.pkgd.min.js"></script>
    <script src="{{url('public/')}}/assets/js/isotope.pkgd.min.js"></script>
    <script src="{{url('public/')}}/assets/js/swiper.min.js"></script>
    <script src="{{url('public/')}}/assets/js/jquery.nice-select.min.js"></script>
    <script src="{{url('public/')}}/assets/js/jquery-ui.min.js"></script>

    <!-- main js -->
    <script src="{{url('/front')}}/assets/js/main.js"></script>

    <!--Start Equal Height Reviews Portion-->
    <script>
    
    //This Jquery Use Compulsary
    
        $(document).ready(function () {

            (function () {
                equalHeight(false);
            })();

            window.onresize = function () {
                equalHeight(true);
            }

            function equalHeight(resize) {
                var elements = document.getElementsByClassName("equalHeight"),
                    allHeights = [],
                    i = 0;
                if (resize === true) {
                    for (i = 0; i < elements.length; i++) {
                        elements[i].style.height = 'auto';
                    }
                }
                for (i = 0; i < elements.length; i++) {
                    var elementHeight = elements[i].clientHeight;
                    allHeights.push(elementHeight);
                }
                for (i = 0; i < elements.length; i++) {
                    elements[i].style.height = Math.max.apply(Math, allHeights) + 'px';
                    if (resize === false) {
                        elements[i].className = elements[i].className + " show";
                    }
                }
            }
        });

        //This Jquery Use Compulsary
    </script>
     <script src="{{ url('vendor/mckenziearts/laravel-notify/public/js/notify.js') }} "></script>
    
    <!--End Equal Height Reviews Portion-->
    @stack('script')      
</body>

</html>
