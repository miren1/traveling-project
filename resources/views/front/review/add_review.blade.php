@extends('front.layouts.master')
@section('title','Dashboard')
@section('content')
<div class="breadcrumb-area jarallax">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-inner">
                    @if(Auth::guard('webagent')->user())
                        <h1 class="page-title">Welcome {{ Auth::guard('webagent')->user()->first_name}} {{Auth::guard('webagent')->user()->last_name }}</h1>
                    @elseif(Auth::guard('webmember')->user())
                        <h1 class="page-title">Welcome {{ Auth::guard('webmember')->user()->name}}</h1>
                    @elseif(Auth::guard('webcustomer')->user())
                        <h1 class="page-title">Welcome {{ Auth::guard('webcustomer')->user()->first_name}} {{Auth::guard('webcustomer')->user()->last_name }}</h1>
                    @endif

                    @if(Auth::guard('webagent')->user())
                    <ul class="page-list">
                        <li>Balance : {{ Auth::guard('webagent')->user()->wallet}}</li>
                        <button type="button" class="btn btn-yellow btn-sm" data-toggle="modal" data-target="#topupModel"
                        id="btnexampleModalLong">
                        Charge Wallet
                        </button>
                    </ul>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb area End -->
 
@include('front.layouts.newmenu')
 <!--Start User Profile Area-->
<div class="user-profile-area pd-top-120 mb-5 ">
    <div class="container">
        <div class="row">
            <div class="col-xl-12 col-lg-12 mx-auto">
                <div class="tab-content user-tab-content">
                    <div class="tab-pane fade show active" id="tabs_1">
                        <div class="user-details">
                            <center><h2 class="user-details-title">Add Review</h2></center>
                            <form id="profile" action="{{ route('review-store')}}" method="post" enctype="multipart/form-data" novalidate="novalidate">
                                @csrf
                                <input type="hidden" id="id" name="id" value="@if(Auth::guard('webmember')->user()){{Auth::guard('webmember')->user()->id}}@else{{ Auth::guard('webcustomer')->user()->id}} @endif">

                                <input type="hidden" id="role" name="role" value="@if(Auth::guard('webmember')->user()){{'member'}}@else{{'user'}} @endif">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="single-input-wrap style-two">
                                            <span class="single-input-title">Content</span>
                                            <textarea class="form-control" id="content" name="content" maxlength="175" required></textarea>
                                  <p style="color:black;">* Only 175 characters are allowed</p>
                                            </label>

                                 <p style="color:green; display: none;" id="GFG"><span class="GFG" >175 </span> Characters Remaining</p>
                                        </div> 
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="name" class="single-input-title">Image</label>
                                            <input data-browse-on-zone-click="true" class="form-control custom-file-input" onchange="SelectImage()" id="input-id"
                                            type="file" name="image[]"  multiple="multiple"
                                            placeholder="Enter Your Image"value="">
                                         </div> 
                                         <div class="form-group">
                                            <button type="submit" id="submitbtn" class="btn btn-yellow float-right">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>     
            </div>
            <div class="col-xl-6 col-lg-6 mx-auto">
                <div class="row RearangeBox" id="image_preview">
                </div>
            </div>
        </div>
    </div>
</div>
<!--End User Profile Area-->
@endsection
@push('style')

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.min.css" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.8/css/fileinput.min.css" crossorigin="anonymous">

<link rel="stylesheet"
    href="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css" />

<style>
.btn-outline-warning {
    color: #212529 !important;
    background-color: #ffc107 !important;
    border-color: #ffc107 !important;
}

.bg-dash-blue {
    color: white;
    background-color: #061847;
    /*height: 67px !important;*/
}
.review-img
{
    width: 25%;
    display: flex;
    flex-wrap: wrap;
}
.remove {
    display: block;
    background: #444;
    border: 1px solid black;
    color: white;
    text-align: center;
    cursor: pointer;
}
.remove:hover {
    background: white;
    color: black;
}
.imgThumbContainer > .imgRemoveBtn{
    position: absolute;
    color: #e91e63ba;
    right: 2px;
    top: 2px;
    cursor: pointer;
    display: none;
}

.RearangeBox:hover > .imgRemoveBtn{ 
    display: block;
}
.error{
  color:red;
}
</style>
@endpush
@push('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.8/js/fileinput.min.js"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.js"></script>

<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js"></script>
<script>
    
$(document).ready(function() {
    $('#profile').bootstrapValidator();

});

$(document).ready(function() {
  $("#content").click(function() {
    $("#GFG").show();
    $(document).ready(function () {
            var max_length = 175;
            $('#content').keyup(function () {
                // alert("hii");
                var len = max_length - $(this).val().length;
                $('.GFG').text(len);

                if (len<50 && len>=10)
                {
                    $("#GFG").replaceWith("<span id='GFG' class='GFG' style='color:dark yellow'>" +len+ " Characters Remaining</span>" );
                } else if (len<10)
                {
                     $("#GFG").replaceWith("<span id='GFG' class='GFG' style='color:red'>" + len + " Characters Remaining</span>" );
                } else if (len>=50)
                {
                    $("#GFG").replaceWith("<span id='GFG' class='GFG' style='color:green'>" + len + " Characters Remaining</span>" );
                }
            });

  
        });
  });
});

/*
$(document).ready(function($) {
        
        $("#profile").validate({
        rules: {
            content: "required" 
        },
        messages: {
            content: "Please enter your Content",                   
        },
         
        submitHandler: function(form) {
            form.submit();
        }
        
    });
});
*/


$("#input-id").fileinput({
    'showUpload':false, 
    'showCancel':false,
    'showClose':false,
    'showRemove':false,
    'allowedFileExtensions': ["jpg","png",'jpeg'],
    maxFileCount: 5,
    maxFileSize: 2000,
    maxImageWidth: 1600,
    maxImageHeight: 1200,
    minImageWidth: 467,
    minImageHeight: 350,
    required: true,
    fileActionSettings: {showZoom: false}
});


function SelectImage()
    {
        $('#submitbtn').removeAttr('disabled');
        return false;
    }

    $('#submitbtn').click(function() {
        if( document.getElementById("input-id").files.length == 0 ){ 
            $('#submitbtn').removeClass('disabled'); 
            imagedata(false);
        }
    });

     
</script>
@endpush