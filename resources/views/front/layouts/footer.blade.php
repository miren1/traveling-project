<!-- footer area start -->
<footer class="footer-area" style="background-image: url({{url('/front/assets')}}/img/bg/2.png);">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-6">
                <div class="footer-widget widget">
                    <div class="about_us_widget">
                        <!-- <a href="index.html" class="footer-logo">
                                <img src="{{url('/front')}}//img/logo.png" alt="footer logo">
                            </a> -->
                        <h4 class="widget-title">Contact us</h4>
                        <ul class="widget_nav_menu">
                            <li style="color:#fff;"><!-- <a href="#"> --><b>Address</b><!-- </a> --></li>
                            <li style="color:#fff;"><!-- <a href="#"> -->35 First Floor, 4D Square, Visat to Gandhinagar Highway, Motera, Ahmedabad, 380005<!-- </a> --></li><br>
                            <li><a href="{{url('/askforcall')}}">Ask for a call
                                </a></li>
                            <li><a href="{{url('/dashboard/reviews')}}">Reviews
                                </a></li>
                        </ul>
                        <ul class="social-icon mt-5">
                            <li>
                                <a class="facebook" href="https://www.facebook.com/JagJoyu" target="_blank"><i class="fa fa-facebook  "></i></a>
                            </li>
                            <li>
                                <a class="twitter" href="https://twitter.com/JagJoyu" target="_blank"><i class="fa fa-twitter"></i></a>
                            </li>
                            <li>
                                <a class="pinterest" href="https://www.instagram.com/jagjoyuts/" target="_blank"><i class="fa fa-instagram"></i></a>
                            </li> 
                            <li>
                                <a class="pinterest" href="https://www.youtube.com/channel/UCciZJ7X7weZiRIG6KOHOj9w" target="_blank"><i class="fa fa-youtube"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="col-lg-7 col-md-6">
                <div class="footer-widget widget">
                    <div class="widget-contact">
                        
                            <iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d1834.9357260943166!2d72.59291860818233!3d23.101801113349914!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1s35%201st%20Floor%2C%20First%20Floor%2C%204D%20Square%2C%20Visat%2C%20to%2C%20Sarkhej%20-%20Gandhinagar%20Hwy%2C%20Motera%2C%20Ahmedabad%2C%20Gujarat%20380005!5e0!3m2!1sen!2sin!4v1630659313955!5m2!1sen!2sin" width="100%" height="300"" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                    </div>
                </div>
                <!-- <div> -->
                    
                    <!-- <div class="video-popup-btn-package-footer">
                        <a href="https://www.youtube.com/watch?v=c7XEhXZ_rsk" class="video-play-btn-package-footer mfp-iframe-package-footer"><i
                                class="fa fa-play"></i></a>
                    </div> -->
               <!--  </div> -->
            </div>

            <div class="col-lg-2 col-md-6 pl-lg-4">
                <div class="footer-widget widget">
                    <h4 class="widget-title">Quick Link</h4>
                    <ul class="widget_nav_menu">
                        <li><a href="{{route('about-us')}}" target="_blank">About us</a></li>
                        <!-- <li><a href="{{route('blog')}}" target="_blank">Blogs</a></li> -->
                        <li><a href="{{url('/careers')}}" target="_blank">Careers</a></li>
                        <!-- <li><a href="#">Events</a></li> -->
                        <li><a href="{{route('gallery')}}" target="_blank">Gallery</a></li>
                        <!-- <li><a href="{{route('FAQ')}}" target="_blank">FAQ</a></li> -->
                        <li><a href="{{route('terms-condition')}}" target="_blank">T&C</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="copyright-inner">
        <div class="container">
            <div class="row">
                <div class="privacypolicy col-lg-2 text-lg-left">
                    <a href="{{route('privacy-policy')}}">Privacy Policy</a>
                </div>
                <div class="copyright-text col-lg-8">
                    &copy; Jag Joyu 2021 All rights reserved
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- footer area end -->

<!-- back to top area start -->
<div class="back-to-top">
    <span class="back-top"><i class="fa fa-angle-up"></i></span>
</div>
<!-- back to top area end -->