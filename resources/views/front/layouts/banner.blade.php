<!-- banner area start -->
@if(isset($sliders))
@foreach($sliders as $slide)
<style >
.banner-slider .banner-slider-item.banner-bg-{{$slide->id}}:after {
background-image: url('{{url('/banner/'.$slide->image)}}') !important;
} 
@media only screen and (max-width: 575px){
    .banner-slider .video-popup-btn {
        margin-top: 0 !important;
        margin-left: 15px !important;
        left: 0 !important;
        top: 0 !important;
        margin-left: 70% !important;
    }
}

@media only screen and (max-width: 1550px){
    .video-popup-btn {
        border: 1px solid var(--main-color-one);
        padding: 5px;
        margin-top: -33px;
        margin-left: 10px !important;
        display: table-column;
    }
}

.banner-slider .banner-slider-item .banner-inner {
    margin-bottom: 50px !important;
    margin-top: 15% !important;
}

.tag_line{
    font-family: 'Arial Black';
    color: #fff;
    font-size: 38px;
}

@media only screen and (max-width: 1680px){
    .tag_line { 
        font-size: 18px; 
        margin-top: 18% !important;
    }
}

@media only screen and (max-width:1280px)  {
    .tag_line { 
        font-size: 18px; 
        margin-top: 50% !important;
    }
}

@media only screen and (min-width:992px){
    .tag_line { 
        font-size: 30px; 
        margin-top: 35% !important;
    }   
}

@media only screen and (max-width: 991px){
    .tag_line { 
        font-size: 15px; 
        margin-top: 18% !important;
    }
}

@media only screen and (min-width: 576px) and (max-width:991px) {
    .tag_line {
        font-size: 15px; 
        margin-top: 25% !important;
    }
}
.rw-wrapper{
    width: 80%;
    /*margin: 110px auto 0 auto;*/
    /*padding: 10px;*/
    padding-top: 20%;
    padding-bottom: 100px;
}

.rw-sentence span{
    font-family: 'Arial Black';
    color: #fff;
    font-size: 38px;
    white-space: nowrap;
    font-weight: normal;
}


.rw-words span{
    position: absolute;
    opacity: 0;
    overflow: hidden;
    width: 100%;
    font-family: 'Arial Black';
    color: #fff;
    font-size: 38px;
}

.rw-words-1 span{
    animation: rotateWordsFirst 25s linear infinite 0s;
}
.rw-words-2 span{
    animation: rotateWordsSecond 25s linear infinite 0s;
}
.rw-words span:nth-child(2) { 
    animation-delay: 5s; 
    font-family: 'Arial Black';
    color: #fff;
    font-size: 38px;
}
.rw-words span:nth-child(3) { 
    animation-delay: 10s; 
    font-family: 'Arial Black';
    color: #fff;
    font-size: 38px;
}
.rw-words span:nth-child(4) { 
    animation-delay: 15s; 
    font-family: 'Arial Black';
    color: #fff;
    font-size: 38px;
}
.rw-words span:nth-child(5) { 
    animation-delay: 20s; 
    font-family: 'Arial Black';
    color: #fff;
    font-size: 38px;
}
.rw-words span:nth-child(6) { 
    animation-delay: 25s; 
    font-family: 'Arial Black';
    color: #fff;
    font-size: 38px;
}

@keyframes rotateWordsFirst {
    0% { opacity: 1; animation-timing-function: ease-in; height: 0px; }
    8% { opacity: 1; height: 60px; }
    19% { opacity: 1; height: 60px; }
    25% { opacity: 1; height: 60px; }
    100% { opacity: 1; }
}

@keyframes rotateWordsSecond {
    0% { opacity: 1; animation-timing-function: ease-in; width: 0px; }
    10% { opacity: 0.3; width: 0px; }
    20% { opacity: 1; width: 100%; }
    27% { opacity: 0; width: 100%; }
    100% { opacity: 0; }
}
.sticky-container{
    padding:0px;
    margin:0px;
    position:fixed;
    right:-160px;
    top:230px;
    width:210px;
    z-index: 1100;
    margin-top: -80px;
}
.sticky li{
    list-style-type:none;
    background-color:#ffffff7d;
    color:#efefef;
    height:43px;
    padding:0px;
    margin:10px 0px 1px -15px;
    -webkit-transition:all 0.25s ease-in-out;
    -moz-transition:all 0.25s ease-in-out;
    -o-transition:all 0.25s ease-in-out;
    transition:all 0.25s ease-in-out;
    cursor:pointer;
    border-radius: 20px;
}
.sticky li:hover{
    margin-left:-50px;
}
.sticky li img{
    float:left;
    margin:1px 0px;
    /*margin-right:5px;*/
}
.sticky li p{
    padding-top:5px;
    margin:0px;
    line-height:16px;
    font-size:11px;
}
.sticky li p a{
    text-decoration:none;
    color:#2C3539;
}
.sticky li p a:hover{
    text-decoration:underline;
}

.sticky-container1 {
    padding: 0px;
    margin: 50%;
    position: fixed;
    right: 0px;
    top: 72%;
    width: 26px;
    z-index: 999;
    margin-top: -80px;
}
.sticky1 li{
    list-style-type:none;
    background-color:#ffffff7d;
    color:#efefef;
    height:43px;
    padding:0px;
    margin:10px 0px 1px -15px;
    -webkit-transition:all 0.25s ease-in-out;
    -moz-transition:all 0.25s ease-in-out;
    -o-transition:all 0.25s ease-in-out;
    transition:all 0.25s ease-in-out;
    cursor:pointer;
    border-radius: 20px;
}
.sticky1 li:hover{
    margin-left:-50px;
}
.sticky1 li img{
    float:left;
    margin:1px 0px;
    /*margin-right:5px;*/
}
.sticky1 li p{
    padding-top:5px;
    margin:0px;
    line-height:16px;
    font-size:11px;
}
.sticky1 li p a{
    text-decoration:none;
    color:#2C3539;
}
.sticky1 li p a:hover{
    text-decoration:underline;
}

</style>
@endforeach
@endif
<!-- <div class="sticky-container">
    <ul class="sticky">
        <li data-toggle="tooltip" data-placement="left" title="Tooltip on left">
            <img src="{{url('/images/update.png')}}">
        </li>
       <li>
            <img src="{{url('/images/event.png')}}">
        </li>
        <li>
            <img src="{{url('/images/gallery_O.png')}}">
        </li>
    </ul>
</div> -->

<!-- <div class="sticky-container1">
    <div class="sticky1">

        @if(isset($slide->video_url))
            <div class="video-popup-btn s-animate-video">
                <a href="{{$slide->video_url}}"
                class="video-play-btn mfp-iframe"><i class="fa fa-play"></i></a>
            </div>
        @else
            <div class="video-popup-btn s-animate-video">
                <a href="https://www.youtube.com/watch?v=dASGYXYIrlM"
                class="video-play-btn mfp-iframe"><i class="fa fa-play"></i></a>
            </div>
        @endif
    </div>
</div> -->
<div class="banner-area">
    <div class="banner-slider">
        @if(isset($sliders))
        @foreach($sliders as $slide)
        <div class="banner-slider-item banner-bg-{{$slide->id}}">
            <div class="container">
                <div class="row">
                    <div class="col-xl-8 col-lg-9 offset-xl-2 offset-lg-1">
                        <div class="row">
                            <div class="col-lg-12 col-sm-8">
                                <!-- <div class="banner-inner">
                                    <span class="tag_line">No season restriction, Travel anytime</span><br>
                                    <span class="tag_line">No extra charges</span><br>
                                    <span class="tag_line">No maintenance charges</span><br>
                                    <span class="tag_line">No annual fees</span><br>
                                    <span class="tag_line">No Hidden Charges</span><br>
                                    <span class="tag_line">No loss, Money-back Guarantee</span>
                                </div> -->
                                <section class="rw-wrapper">
                                    <h2 class="rw-sentence">
                                        <div class="rw-words rw-words-2">
                                            <span>No season restriction, Travel anytime</span>
                                            <span>No extra charges</span>
                                            <span>No annual fees</span>
                                            <span>No Hidden Charges</span>
                                            <span>No loss, Money-back Guarantee</span>
                                            <span></span>
                                        </div>
                                    </h2>
                                </section>
                            </div>
                            
                           <!--  <div class="col-lg-12 mt-lg-3 margin-top">
                                 <a class="btn btn-yellow" href="{{ url('membership-plans')}}">Become a Member</a>
                            </div> -->

                            <div class="col-lg-3 col-sm-4">
                               
                            </div>
                           <!--  <div class="col-lg-12 banner-tour-package">
                                <div class="row">
                                    <div class="col-sm-4 s-animate-3">
                                        <div class="tp-price-meta">
                                            <p>Price</p>
                                            <h2>620 <small>$</small></h2>
                                            <p class="tp-price-meta-details">7 Days Tour <span>on 2 person</span>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 s-animate-4">
                                        <div class="tp-price-meta">
                                            <p>5 Star</p>
                                            <h2>Hotel</h2>
                                            <p class="tp-price-meta-details">Hotels <span>to choice</span></p>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 s-animate-5">
                                        <div class="tp-price-meta">
                                            <p>Flight date</p>
                                            <h2>17</h2>
                                            <p class="tp-price-meta-details">September <span>or later</span></p>
                                        </div>
                                    </div>
                                </div>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
        @else
        <div class="mt-4">
        </div>  
        @endif

    </div>
    <div class="banner-social-meta">
        <div class="banner-slider-dots"></div>
        <ul class="social-icon invisible">
            <li>
                <a class="facebook" href="#" target="_blank"><i class="fa fa-facebook"></i></a>
            </li>
            <li>
                <a class="twitter" href="#" target="_blank"><i class="fa fa-twitter"></i></a>
            </li>
            <li>
                <a class="pinterest" href="#" target="_blank"><i class="fa fa-instagram"></i></a>
            </li>
        </ul>
    </div>
    <div class="container">
        <div class="banner-slider-controls">
            <div class="slider-nav tp-control-nav"></div>
            <!--slider-nav-->
            <div class="tp-slider-extra slider-extra">
                <div class="text">
                    <span class="first">01</span>
                    <span class="devider">/</span>
                    <span class="last"></span>
                </div>
            </div>
            <!--slider-extra-->
        </div>
    </div>
</div>
@push('script')


<script>
     /*var spans = document.querySelectorAll('.rw-words span'),
        maxwidth = 0,
        words = document.querySelector('.rw-words');
    for(var i=0,l=spans.length;i<l;i++){
        console.log(i + ' width: ' + spans[i].offsetWidth)
        maxwidth = spans[i].offsetWidth > maxwidth ? spans[i].offsetWidth : maxwidth;
    }
    words.style.width = maxwidth + 'px'*/
</script>



@endpush
<!-- banner area end 