<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!--<title>@yield('title')</title>-->
	<title>@yield('title')</title>
	<meta name="description" content="Best traveling services with proper hygiene & safety precaution . Jag Joyu Travel Solution is providing membership plans for customer benefits.">
    <!-- favicon -->
    <link rel=icon href="{{url('/front')}}/assets/img/favicon.png" sizes="20x20" type="image/png">
    <!-- Additional plugin css -->
    <link rel="stylesheet" href="{{url('/front')}}/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{url('/front')}}/assets/css/animate.css">
    <link rel="stylesheet" href="{{url('/front')}}/assets/css/magnific-popup.css">
    <link rel="stylesheet" href="{{url('/front')}}/assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="{{url('/front')}}/assets/css/slick.css">
    <link rel="stylesheet" href="{{url('/front')}}/assets/css/swiper.min.css">
    <link rel="stylesheet" href="{{url('/front')}}/assets/css/nice-select.css">
    <link rel="stylesheet" href="{{url('/front')}}/assets/css/jquery-ui.min.css">
    <!-- icons -->
    <link rel="stylesheet" href="{{url('/front')}}/assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{url('/front')}}/assets/css/themify-icons.css">
    <link rel="stylesheet" href="{{url('/front')}}/assets/css/line-awesome.min.css">
    <!-- main css -->
    <link rel="stylesheet" href="{{url('/front')}}/assets/css/style.css">
    <!-- responsive css -->
    <link rel="stylesheet" href="{{url('/front')}}/assets/css/responsive.css">

    <!--Start Custom CSS-->
    <style>
        .b-close {
            color: #fff;
            background: #ff0000;
            padding: 5px 10px;
            position: absolute;
            right: 0;
            top: 0;
            cursor: pointer;
        }
        #toolbar{
            display: none;
        }

        /*This CSS Compulsary Add And Use*/
        .banner-slider-item .margin-top {
            margin-top: 80px;
        }

        @media only screen and (max-width: 1100px) {
            .banner-slider .banner-slider-item {
                height: 700px !important;
                padding: 170px 0 240px 0 !important;
                overflow: hidden !important;
            }
        }

        .custom_tab .tab-content {
            box-shadow: 0px 3px 13px #23397421 !important;
        }

        .tp-main-search {
            box-shadow: none;
        }
/*
        .tp-holiday-plan-area.mg-top-96 {
            margin-top: 0 !important;
            padding-top: 72px !important;
        }*/

        .tp-holiday-plan-area.mg-top-96 {
            margin-top: 0 !important;
            padding-top: 20px !important;

        }
        .section-title {
            margin-bottom: 20px !important;
        }

        .section-title .title {
            font-size: 36px !important;
        }

        .single-destinations-list .details .title {
            font-weight: 550 !important;
            font-size: 23px !important;
        }

        .single-destinations-list.style-two .details {
            padding: 11px 0 !important;
            background: none !important;
        }

        .pd-top-110 {
            padding: 50px 0px !important;
        }

        .copyright-inner .privacypolicy {
            text-align: center;
            font-size: 14px;
            letter-spacing: 0.28px;
            color: #ffffff;
            line-height: 44px;
        }

        .banner-tour-package {
            visibility: hidden !important;
        }
        small.help-block {
            color: red;
        }
        .fixed {
            z-index: 9999;
        }
      
        /* Chrome, Safari, Edge, Opera */
        input::-webkit-outer-spin-button,
        input::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
        }

        /* Firefox */
        input[type=number] {
        -moz-appearance: textfield;
        }
        @media only screen and (max-width:1366px)  {
        #popupads {
            position: absolute !important;
            z-index: 9999 !important;
            opacity: 1 !important;
            background-color: #fff !important;
            text-align: center !important;
            background: #fff !important;
            padding: 2px !important;
            position: fixed !important;
            width: 50% !important;
            height: 60% !important;
            border-radius: 10px;
        }
        .b-close {
            color: #fff;
            background: #818181;
            padding: 2px 10px;
            position: absolute;
            right: -17px;
            top: -18px;
            cursor: pointer;
            border-radius: 20px;
        }
    }

    @media only screen and (max-width:1280px)  {
        #popupads {
            position: absolute !important;
            z-index: 9999 !important;
            opacity: 1 !important;
            background-color: #fff !important;
            text-align: center !important;
            background: #fff !important;
            padding: 2px !important;
            position: fixed !important;
            width: 50% !important;
            height: 19% !important;
            border-radius: 10px;
        }
        .b-close {
            color: #fff;
            background: #818181;
            padding: 2px 10px;
            position: absolute;
            right: -17px;
            top: -18px;
            cursor: pointer;
            border-radius: 20px;
        }
         .navbar-area .nav-container .navbar-collapse .navbar-nav{
            margin-top: 33px !important;
            display: block !important;
        }
        .navbar-area .nav-container .desktop-logo img{
            max-width: 100px !important;
            margin-left: -55px !important;
            margin-right: -58px !important;
        }
        .navbar-area .nav-container .navbar-collapse .navbar-nav li{
            margin: 0 9px !important;
        }
        .navbar-area .nav-container .navbar-collapse .navbar-nav li.menu-item-has-children .sub-menu{
            left: -14px !important;
            min-width: 154px !important;
        }
        .navbar-area .nav-container .navbar-collapse .navbar-nav li a{
            font-size: WHAT % ? !important;
        }
        .btn {
            font-size: WHAT % ? !important;
            height: 50px !important;
            padding: 0 11px !important;
        }
    }

    @media only screen and (max-width:1024px){
        #popupads {
            position: absolute !important;
            z-index: 9999 !important;
            opacity: 1 !important;
            background-color: #fff !important;
            text-align: center !important;
            background: #fff !important;
            padding: 2px !important;
            position: fixed !important;
            width: 50% !important;
            height: 25% !important;
            border-radius: 10px;
        }
        .b-close {
            color: #fff;
            background: #818181;
            padding: 2px 10px;
            position: absolute;
            right: -17px;
            top: -18px;
            cursor: pointer;
            border-radius: 20px;
        }   
        .navbar-area .nav-container .navbar-collapse .navbar-nav{
            margin-top: 33px !important;
            display: block !important;
        }
        .navbar-area .nav-container .desktop-logo img{
            width: 84px !important;
            margin-left: 0px !important;
            margin-right: -50px !important;
        }
        .navbar-area .nav-container .navbar-collapse .navbar-nav li a{
            font-size: WHAT % ? !important;
        }
        .navbar-area .nav-container .navbar-collapse .navbar-nav li.menu-item-has-children:hover > .sub-menu {
            margin-left: -90px !important;
            margin-top: 3px !important;
            border-radius: 29px !important;
        }
    }
       .dropdown-toggle::after{
            display: none !important;
        }
    #dropdownHover a:hover{
        color: black !important;
    }


    </style>
    <!--End Custom CSS-->
    <link rel="stylesheet" href="{{asset('vendor/mckenziearts/laravel-notify/css/notify.css')}}" /> 
    @stack('style')
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-207542746-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-207542746-1');
    </script>
</head>
<body>
    @include('front.new-layouts.header')
    @yield('content')
    <x:notify-messages />   
    
    @include('front.new-layouts.footer')   
    <!-- Additional plugin js -->
    <script src="{{url('/')}}/assets/js/jquery-2.2.4.min.js"></script>
    <script src="{{url('/')}}/assets/js/popper.min.js"></script>
    <script src="{{url('/')}}/assets/js/bootstrap.min.js"></script>
    <script src="{{url('/')}}/assets/js/jquery.magnific-popup.js"></script>
    <script src="{{url('/')}}/assets/js/owl.carousel.min.js"></script>
    <script src="{{url('/')}}/assets/js/wow.min.js"></script>
    <script src="{{url('/')}}/assets/js/slick.js"></script>
    <script src="{{url('/')}}/assets/js/waypoints.min.js"></script>
    <script src="{{url('/')}}/assets/js/jquery.counterup.min.js"></script>
    <script src="{{url('/')}}/assets/js/imagesloaded.pkgd.min.js"></script>
    <script src="{{url('/')}}/assets/js/isotope.pkgd.min.js"></script>
    <script src="{{url('/')}}/assets/js/swiper.min.js"></script>
    <script src="{{url('/')}}/assets/js/jquery.nice-select.min.js"></script>
    <script src="{{url('/')}}/assets/js/jquery-ui.min.js"></script>
    <!-- main js -->
    <script src="{{url('/front')}}/assets/js/main.js"></script>
    <!--Start Equal Height Reviews Portion-->
    <script>
    //This Jquery Use Compulsary
        $(document).ready(function () {
            (function () {
                equalHeight(false);
            })();
            window.onresize = function () {
                equalHeight(true);
            }
            function equalHeight(resize) {
                var elements = document.getElementsByClassName("equalHeight"),
                    allHeights = [],
                    i = 0;
                if (resize === true) {
                    for (i = 0; i < elements.length; i++) {
                        elements[i].style.height = 'auto';
                    }
                }
                for (i = 0; i < elements.length; i++) {
                    var elementHeight = elements[i].clientHeight;
                    allHeights.push(elementHeight);
                }
                for (i = 0; i < elements.length; i++) {
                    elements[i].style.height = Math.max.apply(Math, allHeights) + 'px';
                    if (resize === false) {
                        elements[i].className = elements[i].className + " show";
                    }
                }
            }
        });

        //This Jquery Use Compulsary
</script>
<script src="{{ asset('vendor/mckenziearts/laravel-notify/js/notify.js') }} "></script>
<!--End Equal Height Reviews Portion-->
@stack('script')      
</body>
</html>