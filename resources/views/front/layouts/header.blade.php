 <!-- preloader area start -->
 <div class="preloader" id="preloader">
    <div class="preloader-inner">
        <div class="spinner">
            <div class="dot1"></div>
            <div class="dot2"></div>
        </div>
    </div>
</div>
<!-- preloader area end -->

<!-- navbar area start -->
<nav class="navbar navbar-area navbar-expand-lg nav-style-02">
    <div class="container nav-container">
        <div class="responsive-mobile-menu">
            <div class="mobile-logo">
                @if(Auth::guard('webagent')->user() || Auth::guard('webcustomer')->user() || Auth::guard('webmember')->user())
                    <a href="{{url('/dashboard')}}">
                        <img src="{{url('/images/jj-logo.png')}}" alt="logo">
                    </a>
                @else
                    <a href="{{route('home')}}">
                        <img src="{{url('/images/jj-logo.png')}}" alt="logo">
                    </a>
                @endif
            </div>
            <button class="navbar-toggler float-right" type="button" data-toggle="collapse" data-target="#tp_main_menu" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggle-icon">
                    <span class="line"></span>
                    <span class="line"></span>
                    <span class="line"></span>
                </span>
            </button>
            <div class="nav-right-content">
                <ul class="pl-0">
                    @if(Auth::guard('webagent')->user() || Auth::guard('webcustomer')->user() || Auth::guard('webmember')->user())
                    <li class="notification">
                        <a class="" title="logout" href="{{route('front.logout')}}">
                            <i class="fa fa-sign-out fa-lg"></i>
                        </a>
                    </li>
                    @else
                    <li class="notification">
                        <a class="" href="{{route('login')}}">
                            <i class="fa fa-user" aria-hidden="true"></i>
                        </a>
                    </li>
                    @endif
                </ul>
            </div>
        </div>
        
        <div class="collapse navbar-collapse" id="tp_main_menu">
            <div class="logo-wrapper desktop-logo">
                {{--<a href="{{route('home')}}" class="main-logo">
                    <img src="{{url('/images/jj-logo-old.png')}}" alt="logo">
                </a>
                <a href="{{route('home')}}" class="sticky-logo">
                    <img src="{{url('/images/jj-logo.png')}}" alt="logo">
                </a>--}}

                @if(Auth::guard('webagent')->user() || Auth::guard('webcustomer')->user() || Auth::guard('webmember')->user())
                    <!-- <a href="{{url('/dashboard')}}" class="main-logo">
                        <img src="{{url('/images/jj-logo-old.png')}}" alt="logo">
                    </a> -->
                    <a href="{{url('/dashboard')}}">
                        <img src="{{url('/images/jj-logo.png')}}" alt="logo">
                    </a>
                @else
                    <!-- <a href="{{route('home')}}" class="main-logo">
                        <img src="{{url('/images/jj-logo-old.png')}}" alt="logo">
                    </a> -->
                    <a href="{{route('home')}}">
                        <img src="{{url('/images/jj-logo.png')}}" alt="logo">
                    </a>
                @endif
            </div>
            <ul class="dropdown-menu-btn">
                <li class="line"></li>
                <li class="line"></li>
                <li class="line"></li>
            </ul>
            <ul class="navbar-nav">        
                <li class="tp-lang d-lg-none">
                    <a href="{{url('/membership-plans')}}">Membership Plan</a>
                </li>
                @if(Auth::guard('webagent')->user() || Auth::guard('webcustomer')->user() || Auth::guard('webmember')->user())
                    <li class="search d-lg-none">
                    </li>
                @else
                    <li class="tp-lang">
                        <a href="{{url('/askforcall')}}">Ask for call</a>
                    </li>
                @endif
                <li class="menu-item">
                    <a href="{{route('gallery')}}">Gallery</a>
                </li>
                <!-- <li>
                    <a href="{{route('blog')}}">Blogs</a>
                </li> -->
                <li>
                    <a href="{{route('about-us')}}">About us</a>
                </li>
            </ul>
        </div>
        <div class="nav-right-content">
            <ul>
                <li class="tp-lang">
                    <a href="{{url('/membership-plans')}}">Membership Plan</a>
                </li>

                @if(Auth::guard('webagent')->user() || Auth::guard('webcustomer')->user() || Auth::guard('webmember')->user())
                <!-- <li class="">
                    <a href="{{url('/dashboard')}}">
                        <i class="fa fa-user" style="font-size: larger !important;" aria-hidden="true"></i>
                    </a>
                </li>
                 -->
                @endif
                
                @if(Auth::guard('webagent')->user() || Auth::guard('webcustomer')->user() || Auth::guard('webmember')->user())
                   
                @else
                    <li class="tp-lang">
                        <a href="{{url('/askforcall')}}">Ask for call</a>
                    </li>
                @endif
                
                @if(Auth::guard('webagent')->user() || Auth::guard('webcustomer')->user() || Auth::guard('webmember')->user())
                <li class="notification">
                    <a class="" title="logout" href="{{route('front.logout')}}">
                        <i class="fa fa-sign-out fa-lg"></i>
                    </a>
                </li>
                @else
                <li class="notification">
                    <a class="" href="{{route('login')}}">
                        <i class="fa fa-user" aria-hidden="true"></i>
                    </a>
                </li>
                @endif
            </ul>
        </div>
    </div>
</nav>
<!-- navbar area end -->