@extends('front.new-layouts.master')
@section('title',$image[0]->title)
@section('description',$image[0]->seo_description)
@section('keywords',$image[0]->seo_keywords)
@section('content')
    <!-- breadcrumb area start -->
    <div class="breadcrumb-area jarallax" style="background-image:url(assets/img/bg/1.png);">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb-inner">
                        <h1 class="page-title">{{$image[0]->category}}</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- breadcrumb area End -->

    <!-- gallery area start -->
    <div class="gallery-area pd-top-108">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-7">
                    <div class="section-title text-center">
                        <h4 class="title">{{$image[0]->title}}</h4>
                            <p></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
            <!-- Gallery area start -->

<!-- <div class="blog-list-area pd-top-120">

        <div class="container">
            <div class="row justify-content-center">
                @if(isset($image))
                    @foreach($image as $key)
                     @foreach($key->gallery_image as $value)
                    <div class="col-lg-4 col-md-6">
                        <div class="single-blog">
                            <a href="{{url('blog')}}/{{$key->slug}}">
                             
                                <div class="thumb">
                                   <a class="popup-thumb" href="{{$url}}/{{$value->image}}" data-effect="mfp-zoom-in">
                                            <img src="{{$url}}/{{$value->image}}" alt="image" height="300px" width="500px">
                                           {{-- <video width="320" height="240" controls>
                                                <source src="{{$url}}/{{$value->image}}" type="video/mp4">
                                            </video> --}}
                                        </a>
                                </div>
                                </div>
                            </a>
                           
                       
                    </div>
                    @endforeach
                    @endforeach
                @endif
            </div>
        </div>
    </div> -->





            <div class="gallery-area">
                <div class="container">
                    <div class="gallery-filter-area row">
                        <div class="gallery-sizer col-1"></div>
                        
                        @if(isset($image))
                            @foreach($image as $key)
                                @foreach($key->gallery_image as $value)
                            
                                <div class="tp-gallery-item col-md-4 col-12 mb-10">
                                    <div class="tp-gallery-item-img">
                                        <a class="popup-thumb" href="{{$url}}/{{$value->image}}" data-effect="mfp-zoom-in">
                                            <img src="{{$url}}/{{$value->image}}" alt="image" height="300px" width="500px">
                                           
                                        </a>
                                    </div>
                                </div>

                                @endforeach
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
            <!-- Gallery area end -->
      <!--   </div>
    </div> -->
    <!-- gallery area End -->
@endsection