@extends('front.new-layouts.master')
@section('title',"India's No. 1 - Travel Investment Membership Plans  - Jag Joyu")
@section('content')
    <!-- breadcrumb area start -->
    <div class="breadcrumb-area style-two jarallax" style="background-image:url(assets/img/bg/1.png);">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb-inner">
                        <h1 class="page-title">International</h1>
                        <ul class="page-list">
                            <li><a href="#">Home</a></li>
                            <li>International</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- breadcrumb area End -->

    <!-- destination area End -->
    <div class="destination-area">
        <div class="container-bg mg-top--70">
            <div class="container">
                <div class="row justify-content-center">
                    <a href="{{ route('intinerary-demo') }}">
                        <div class="col-lg-4 col-md-6">
                            <div class="single-destination-grid text-center">
                                <div class="thumb">
                                    <img src="assets/img/destination-list/8.png" alt="img">
                                </div>
                                <div class="details">
                                    <div class="tp-review-meta">
                                        <i class="ic-yellow fa fa-star"></i>
                                        <i class="ic-yellow fa fa-star"></i>
                                        <i class="ic-yellow fa fa-star"></i>
                                        <i class="ic-yellow fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <span>4.0</span>
                                    </div>
                                    <h3 class="title">Dazzling Dubai</h3>
                                    <p class="content">Dazzling Dubai is the world's second largest and second most- populous continent, being behind Asia in both. At about 30.3 million km² including adjacent islands, it 6% Earth's total surface area and 20% land area.</p>
                                    <a class="btn btn-gray" href="destination-details.html"><span>Explore<i class="la la-arrow-right"></i></span></a>
                                </div>
                            </div>
                        </div>
                    </a>
                    <a href="{{ route('intinerary-demo') }}">
                        <div class="col-lg-4 col-md-6">
                            <div class="single-destination-grid text-center">
                                <div class="thumb">
                                    <img src="assets/img/destination-list/8.png" alt="img">
                                </div>
                                <div class="details">
                                    <div class="tp-review-meta">
                                        <i class="ic-yellow fa fa-star"></i>
                                        <i class="ic-yellow fa fa-star"></i>
                                        <i class="ic-yellow fa fa-star"></i>
                                        <i class="ic-yellow fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <span>4.0</span>
                                    </div>
                                    <h3 class="title">Dazzling Dubai</h3>
                                    <p class="content">Dazzling Dubai is the world's second largest and second most- populous continent, being behind Asia in both. At about 30.3 million km² including adjacent islands, it 6% Earth's total surface area and 20% land area.</p>
                                    <a class="btn btn-gray" href="destination-details.html"><span>Explore<i class="la la-arrow-right"></i></span></a>
                                </div>
                            </div>
                        </div>
                    </a>
                    <a href="{{ route('intinerary-demo') }}">
                        <div class="col-lg-4 col-md-6">
                            <div class="single-destination-grid text-center">
                                <div class="thumb">
                                    <img src="assets/img/destination-list/8.png" alt="img">
                                </div>
                                <div class="details">
                                    <div class="tp-review-meta">
                                        <i class="ic-yellow fa fa-star"></i>
                                        <i class="ic-yellow fa fa-star"></i>
                                        <i class="ic-yellow fa fa-star"></i>
                                        <i class="ic-yellow fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <span>4.0</span>
                                    </div>
                                    <h3 class="title">Dazzling Dubai</h3>
                                    <p class="content">Dazzling Dubai is the world's second largest and second most- populous continent, being behind Asia in both. At about 30.3 million km² including adjacent islands, it 6% Earth's total surface area and 20% land area.</p>
                                    <a class="btn btn-gray" href="destination-details.html"><span>Explore<i class="la la-arrow-right"></i></span></a>
                                </div>
                            </div>
                        </div>
                    </a>
                    <a href="{{ route('intinerary-demo') }}">
                        <div class="col-lg-4 col-md-6">
                            <div class="single-destination-grid text-center">
                                <div class="thumb">
                                    <img src="assets/img/destination-list/8.png" alt="img">
                                </div>
                                <div class="details">
                                    <div class="tp-review-meta">
                                        <i class="ic-yellow fa fa-star"></i>
                                        <i class="ic-yellow fa fa-star"></i>
                                        <i class="ic-yellow fa fa-star"></i>
                                        <i class="ic-yellow fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <span>4.0</span>
                                    </div>
                                    <h3 class="title">Dazzling Dubai</h3>
                                    <p class="content">Dazzling Dubai is the world's second largest and second most- populous continent, being behind Asia in both. At about 30.3 million km² including adjacent islands, it 6% Earth's total surface area and 20% land area.</p>
                                    <a class="btn btn-gray" href="destination-details.html"><span>Explore<i class="la la-arrow-right"></i></span></a>
                                </div>
                            </div>
                        </div>
                    </a>
                    <a href="{{ route('intinerary-demo') }}">
                        <div class="col-lg-4 col-md-6">
                            <div class="single-destination-grid text-center">
                                <div class="thumb">
                                    <img src="assets/img/destination-list/8.png" alt="img">
                                </div>
                                <div class="details">
                                    <div class="tp-review-meta">
                                        <i class="ic-yellow fa fa-star"></i>
                                        <i class="ic-yellow fa fa-star"></i>
                                        <i class="ic-yellow fa fa-star"></i>
                                        <i class="ic-yellow fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <span>4.0</span>
                                    </div>
                                    <h3 class="title">Dazzling Dubai</h3>
                                    <p class="content">Dazzling Dubai is the world's second largest and second most- populous continent, being behind Asia in both. At about 30.3 million km² including adjacent islands, it 6% Earth's total surface area and 20% land area.</p>
                                    <a class="btn btn-gray" href="destination-details.html"><span>Explore<i class="la la-arrow-right"></i></span></a>
                                </div>
                            </div>
                        </div>
                    </a>
                    <a href="{{ route('intinerary-demo') }}">
                        <div class="col-lg-4 col-md-6">
                            <div class="single-destination-grid text-center">
                                <div class="thumb">
                                    <img src="assets/img/destination-list/8.png" alt="img">
                                </div>
                                <div class="details">
                                    <div class="tp-review-meta">
                                        <i class="ic-yellow fa fa-star"></i>
                                        <i class="ic-yellow fa fa-star"></i>
                                        <i class="ic-yellow fa fa-star"></i>
                                        <i class="ic-yellow fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <span>4.0</span>
                                    </div>
                                    <h3 class="title">Dazzling Dubai</h3>
                                    <p class="content">Dazzling Dubai is the world's second largest and second most- populous continent, being behind Asia in both. At about 30.3 million km² including adjacent islands, it 6% Earth's total surface area and 20% land area.</p>
                                    <a class="btn btn-gray" href="destination-details.html"><span>Explore<i class="la la-arrow-right"></i></span></a>
                                </div>
                            </div>
                        </div>
                    </a>
                    <a href="{{ route('intinerary-demo') }}">
                        <div class="col-lg-4 col-md-6">
                            <div class="single-destination-grid text-center">
                                <div class="thumb">
                                    <img src="assets/img/destination-list/8.png" alt="img">
                                </div>
                                <div class="details">
                                    <div class="tp-review-meta">
                                        <i class="ic-yellow fa fa-star"></i>
                                        <i class="ic-yellow fa fa-star"></i>
                                        <i class="ic-yellow fa fa-star"></i>
                                        <i class="ic-yellow fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <span>4.0</span>
                                    </div>
                                    <h3 class="title">Dazzling Dubai</h3>
                                    <p class="content">Dazzling Dubai is the world's second largest and second most- populous continent, being behind Asia in both. At about 30.3 million km² including adjacent islands, it 6% Earth's total surface area and 20% land area.</p>
                                    <a class="btn btn-gray" href="destination-details.html"><span>Explore<i class="la la-arrow-right"></i></span></a>
                                </div>
                            </div>
                        </div>
                    </a>
                    <a href="{{ route('intinerary-demo') }}">
                        <div class="col-lg-4 col-md-6">
                            <div class="single-destination-grid text-center">
                                <div class="thumb">
                                    <img src="assets/img/destination-list/8.png" alt="img">
                                </div>
                                <div class="details">
                                    <div class="tp-review-meta">
                                        <i class="ic-yellow fa fa-star"></i>
                                        <i class="ic-yellow fa fa-star"></i>
                                        <i class="ic-yellow fa fa-star"></i>
                                        <i class="ic-yellow fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <span>4.0</span>
                                    </div>
                                    <h3 class="title">Dazzling Dubai</h3>
                                    <p class="content">Dazzling Dubai is the world's second largest and second most- populous continent, being behind Asia in both. At about 30.3 million km² including adjacent islands, it 6% Earth's total surface area and 20% land area.</p>
                                    <a class="btn btn-gray" href="destination-details.html"><span>Explore<i class="la la-arrow-right"></i></span></a>
                                </div>
                            </div>
                        </div>
                    </a>
                    <a href="{{ route('intinerary-demo') }}">
                        <div class="col-lg-4 col-md-6">
                            <div class="single-destination-grid text-center">
                                <div class="thumb">
                                    <img src="assets/img/destination-list/8.png" alt="img">
                                </div>
                                <div class="details">
                                    <div class="tp-review-meta">
                                        <i class="ic-yellow fa fa-star"></i>
                                        <i class="ic-yellow fa fa-star"></i>
                                        <i class="ic-yellow fa fa-star"></i>
                                        <i class="ic-yellow fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <span>4.0</span>
                                    </div>
                                    <h3 class="title">Dazzling Dubai</h3>
                                    <p class="content">Dazzling Dubai is the world's second largest and second most- populous continent, being behind Asia in both. At about 30.3 million km² including adjacent islands, it 6% Earth's total surface area and 20% land area.</p>
                                    <a class="btn btn-gray" href="destination-details.html"><span>Explore<i class="la la-arrow-right"></i></span></a>
                                </div>
                            </div>
                        </div>
                    </a>
                    <div class="col-12">
                        <div class="btn-wrapper text-center">
                            <a class="btn btn-yellow mt-4" href="#"><span>Load More<i class="la la-arrow-right"></i></span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- destination area End -->
@endsection