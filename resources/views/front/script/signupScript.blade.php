<script>

    $(document).ready(function() {
        $('#signUp').bootstrapValidator({
            fields: {
                mobile_number: {
                    message: 'contact is required',
                    validators: {
                        remote: {
                            type: 'POST',
                            url: "{{ url('customer/phone') }}/All",
                            dataType: 'json',
                            data: function(validator) {
                                // console.log(validator.getFieldElements('contry').val());
                                return {
                                    contact: validator.getFieldElements('mobile_number').val(),
                                    contry: validator.getFieldElements('contry').val(),
                                    _token: "{{ csrf_token() }}",
                                };
                            },
                            message: 'Oops contact already exist !',
                            onSuccess: function(e, data) {
                                // console.log(data);
                                var contry = $('#usercontry').val();
                                if(contry=="india")
                                {
                                    if (data.result.valid==true)
                                    {
                                        $('.userGetOtp').css('display','block');
                                    }
                                    else if (data.result.valid==false)
                                    {
                                        $('.userGetOtp').css('display','none');
                                    }
                                    else
                                    {
                                        $('.userGetOtp').css('display','none');
                                    }
                                }
                            },
                            onError: function(e, data) {
                                $('.userGetOtp').css('display','none');
                            }
                        }
                    }
                },
                email: {
                    message: 'E-mail is required',
                    validators: {
                        remote: {
                            type: 'POST',
                            url: "{{url('customer/email')}}/All",
                            dataType: 'json',
                            data: function(validator) {
                                // console.log(validator.getFieldElements('contact').val());
                                return {
                                    email: validator.getFieldElements('email').val(),
                                    _token: "{{ csrf_token() }}",
                                };
                            },
                            message: 'E-mail Already Exists !',
                            onSuccess: function(e, data) {
                                var contry = $('#usercontry').val();
                                if(contry=="other")
                                {
                                    if (data.result.valid==true)
                                    {
                                        $('.userEmailGetOtp').css('display','block');
                                    }
                                    else if (data.result.valid==false)
                                    {
                                        $('.userEmailGetOtp').css('display','none');
                                    }
                                    else
                                    {
                                        $('.userEmailGetOtp').css('display','none');
                                    }
                                }
                            },
                            onError: function(e, data) {
                                $('.userEmailGetOtp').css('display','none');
                            }
                        }
                    }
                },
                contactOtp: {
                    message: 'OTP is required',
                    validators: {
                        remote: {
                            type: 'POST',
                            url: "{{route('signup.OTP')}}",
                            dataType: 'json',
                            data: function(validator) {
                                // console.log(validator.getFieldElements('contact').val());
                                return {
                                    contact: validator.getFieldElements('mobile_number').val(),
                                    status:'contact',
                                    type: 'customer',
                                    _token: "{{ csrf_token() }}",
                                };
                            },
                            message: 'Wrong OTP or OTP expired.',
                        }
                    }
                },
                emailOtp: {
                    message: 'OTP is required',
                    validators: {
                        remote: {
                            type: 'POST',
                            url: "{{route('signup.OTP')}}",
                            dataType: 'json',
                            data: function(validator) {
                                // console.log(validator.getFieldElements('email').val());
                                return {
                                    email: validator.getFieldElements('email').val(),
                                    status:'email',
                                    type: 'customer',
                                    _token: "{{ csrf_token() }}",
                                };
                            },
                            message: 'Wrong OTP or OTP expired.',
                        }
                    }
                },
            }
        });
    });

    $(document).ready(function() {
        $('#agentPersonal').bootstrapValidator({
            fields: {
                contact_details: {
                    message: 'contact is required',
                    validators: {
                        remote: {
                            type: 'POST',
                            url: "{{ url('agent/phone') }}/All",
                            dataType: 'json',
                            data: function(validator) {
                                // console.log(validator.getFieldElements('contry').val());
                                return {
                                    contact: validator.getFieldElements('contact_details').val(),
                                    contry: validator.getFieldElements('contry').val(),
                                    _token: "{{ csrf_token() }}",
                                };
                            },
                            message: 'Oops contact already exist !',
                            onSuccess: function(e, data) {
                                // console.log(data);
                                var contry = $('#personalcontry').val();
                                if(contry=="india")
                                {
                                    if (data.result.valid==true)
                                    {
                                        $('.personalGetOtp').css('display','block');
                                    }
                                    else if (data.result.valid==false)
                                    {
                                        $('.personalGetOtp').css('display','none');
                                    }
                                    else
                                    {
                                        $('.personalGetOtp').css('display','none');
                                    }
                                }
                            },
                            onError: function(e, data) {
                                $('.personalGetOtp').css('display','none');
                            }
                        }
                    }
                },
                company_email: {
                    message: 'E-mail is required',
                    validators: {
                        remote: {
                            type: 'POST',
                            url: "{{url('customer/email')}}/All",
                            dataType: 'json',
                            data: function(validator) {
                                // console.log(validator.getFieldElements('contact').val());
                                return {
                                    email: validator.getFieldElements('email').val(),
                                    _token: "{{ csrf_token() }}",
                                };
                            },
                            message: 'Oops E-mail already exist !',
                            onSuccess: function(e, data) {
                                var contry = $('#personalcontry').val();
                                if(contry=="other")
                                {
                                    if (data.result.valid==true)
                                    {
                                        $('.personalEmailGetOtp').css('display','block');
                                    }
                                    else if (data.result.valid==false)
                                    {
                                        $('.personalEmailGetOtp').css('display','none');
                                    }
                                    else
                                    {
                                        $('.personalEmailGetOtp').css('display','none');
                                    }
                                }
                            },
                            onError: function(e, data) {
                                $('.personalEmailGetOtp').css('display','none');
                            }
                        }
                    }
                },
                contactOtp: {
                    message: 'OTP is required',
                    validators: {
                        remote: {
                            type: 'POST',
                            url: "{{route('signup.OTP')}}",
                            dataType: 'json',
                            data: function(validator) {
                                // console.log(validator.getFieldElements('contact').val());
                                return {
                                    contact: validator.getFieldElements('contact_details').val(),
                                    status:'contact',
                                    type: 'personal',
                                    _token: "{{ csrf_token() }}",
                                };
                            },
                            message: 'Wrong OTP or OTP expired.',
                        }
                    }
                },
                emailOtp: {
                    message: 'OTP is required',
                    validators: {
                        remote: {
                            type: 'POST',
                            url: "{{route('signup.OTP')}}",
                            dataType: 'json',
                            data: function(validator) {
                                // console.log(validator.getFieldElements('email').val());
                                return {
                                    email: validator.getFieldElements('company_email').val(),
                                    status:'email',
                                    type: 'personal',
                                    _token: "{{ csrf_token() }}",
                                };
                            },
                            message: 'Wrong OTP or OTP expired.',
                        }
                    }
                },
            }
        });
    });

    $(document).ready(function() {
        $('#agentCompany').bootstrapValidator({
            fields: {
                contact_details: {
                    message: 'contact is required',
                    validators: {
                        remote: {
                            type: 'POST',
                            url: "{{ url('agent/phone') }}/All",
                            dataType: 'json',
                            data: function(validator) {
                                // console.log(validator.getFieldElements('contry').val());
                                return {
                                    contact: validator.getFieldElements('contact_details').val(),
                                    contry: validator.getFieldElements('contry').val(),
                                    _token: "{{ csrf_token() }}",
                                };
                            },
                            message: 'Oops contact already exist !',
                            onSuccess: function(e, data) {
                                // console.log(data);
                                var contry = $('#companycontry').val();
                                if(contry=="india")
                                {
                                    if (data.result.valid==true)
                                    {
                                        $('.companyGetOtp').css('display','block');
                                    }
                                    else if (data.result.valid==false)
                                    {
                                        $('.companyGetOtp').css('display','none');
                                    }
                                    else
                                    {
                                        $('.companyGetOtp').css('display','none');
                                    }
                                }
                            },
                            onError: function(e, data) {
                                $('.companyGetOtp').css('display','none');
                            }
                        }
                    }
                },
                company_email: {
                    message: 'E-mail is required',
                    validators: {
                        remote: {
                            type: 'POST',
                            url: "{{url('customer/email')}}/All",
                            dataType: 'json',
                            data: function(validator) {
                                // console.log(validator.getFieldElements('contact').val());
                                return {
                                    email: validator.getFieldElements('email').val(),
                                    type: validator.getFieldElements('type').val(),
                                    _token: "{{ csrf_token() }}",
                                };
                            },
                            message: 'E-mail is not registered !',
                            onSuccess: function(e, data) {
                                var contry = $('#companycontry').val();
                                if(contry=="other")
                                {
                                    if (data.result.valid==true)
                                    {
                                        $('.companyEmailGetOtp').css('display','block');
                                    }
                                    else if (data.result.valid==false)
                                    {
                                        $('.companyEmailGetOtp').css('display','none');
                                    }
                                    else
                                    {
                                        $('.companyEmailGetOtp').css('display','none');
                                    }
                                }
                            },
                            onError: function(e, data) {
                                $('.companyEmailGetOtp').css('display','none');
                            }
                        }
                    }
                },
                contactOtp: {
                    message: 'OTP is required',
                    validators: {
                        remote: {
                            type: 'POST',
                            url: "{{route('signup.OTP')}}",
                            dataType: 'json',
                            data: function(validator) {
                                // console.log(validator.getFieldElements('contact').val());
                                return {
                                    contact: validator.getFieldElements('contact_details').val(),
                                    status:'contact',
                                    type: 'company',
                                    _token: "{{ csrf_token() }}",
                                };
                            },
                            message: 'Wrong OTP or OTP expired.',
                        }
                    }
                },
                emailOtp: {
                    message: 'OTP is required',
                    validators: {
                        remote: {
                            type: 'POST',
                            url: "{{route('signup.OTP')}}",
                            dataType: 'json',
                            data: function(validator) {
                                // console.log(validator.getFieldElements('email').val());
                                return {
                                    email: validator.getFieldElements('company_email').val(),
                                    status:'email',
                                    type: 'company',
                                    _token: "{{ csrf_token() }}",
                                };
                            },
                            message: 'Wrong OTP or OTP expired.',
                        }
                    }
                },
            }
        });
    });

    $(document).on('click','#personalGetOtp',function(){
        var val = $('#phone1').val();
        var name = $('#personal_fname').val();
        if(val=="")
        {
            alert('Please Enter Contact Number');
        }
        else
        {
            MobileOtp(name, val,'personal');
            $('#personalGetOtp').html('Resend OTP');
            $('.personalOtp').show();
        }
    });

    $(document).on('click','#companyGetOtp',function(){
        var val = $('#phone2').val();
        var name = $('#company_fname').val();
        if(val=="")
        {
            alert('Please Enter Contact Number');
        }
        else
        {
            MobileOtp(name, val,'company');
            $('#companyGetOtp').html('Resend OTP');
            $('.companyOtp').show();
        }
    });

    $(document).on('click','#userGetOtp',function(){
        var val = $('#phone3').val();
        var name = $('#user_fname').val();
        if(val=="")
        {
            alert('Please Enter Contact Number');
        }
        else
        {
            MobileOtp(name, val,'customer');
            $('#userGetOtp').html('Resend OTP');
            $('.userOtp').show();
        }
    });

    $(document).on('click','.personalEmailGetOtp',function(){
        var val = $('#personalEmail').val();
        var name = $('#personal_fname').val();
        if(val=="")
        {
            alert('Please Enter E-mail');
        }
        else
        {
            EmailOtp(name, val, 'personal');
            $('#personalEmailGetOtp').html('Resend OTP');
            $('.personalEmailOtp').show();
        }
    });

    $(document).on('click','.companyEmailGetOtp',function(){
        var val = $('#companyEmail').val();
        var name = $('#company_fname').val();
        if(val=="")
        {
            alert('Please Enter E-mail');
        }
        else
        {
            EmailOtp(name, val, 'company');
            $('#companyEmailGetOtp').html('Resend OTP');
            $('.companyEmailOtp').show();
        }
    });

    $(document).on('click','.userEmailGetOtp',function(){
        var val = $('#userEmail').val();
        var name = $('#user_fname').val();
        if(val=="")
        {
            alert('Please Enter E-mail');
        }
        else
        {
            EmailOtp(name, val, 'customer');
            $('#userEmailGetOtp').html('Resend OTP');
            $('.userEmailOtp').show();
        }
    });

    $(document).on('change','#personalcontry',function(){
        var val = $(this).val();
        if(val=="india")
        {
            $('#phone1').val('');
            $('#personalEmail').val('');
        }
        else if(val=="other")
        {
            $('#phone1').val('');
            $('#personalEmail').val('');
        }
        $('.personalGetOtp').hide();
        $('.personalEmailGetOtp').hide();
        $('.personalOtp').hide();
        $('.personalEmailOtp').hide();
    });

    $(document).on('change','#companycontry',function(){
        var val = $(this).val();
        if(val=="india")
        {
            $('#phone2').val('');
            $('#companyEmail').val('');
        }
        else if(val=="other")
        {
            $('#phone2').val('');
            $('#companyEmail').val('');
        }
        $('.companyGetOtp').hide();
        $('.companyEmailGetOtp').hide();
        $('.companyOtp').hide();
        $('.companyEmailOtp').hide();
    });

    $(document).on('change','#usercontry',function(){
        var val = $(this).val();
        if(val=="india")
        {
            $('#phone3').val('');
            $('#userEmail').val('');
        }
        else if(val=="other")
        {
            $('#phone3').val('');
            $('#userEmail').val('');
        }
        $('.userGetOtp').hide();
        $('.userEmailGetOtp').hide();
        $('.userOtp').hide();
        $('.userEmailOtp').hide();
    });

    function MobileOtp(name, mobile, type)
    {
        $.ajax({
            type: "post",
            url: "{{route('signup.mobileotp')}}",
            data: {
                    'name':name,
                    'mobile':mobile,
                    'type':type,
                    "_token": "{{ csrf_token() }}"
                },
            dataType:'json',
            success: function(result){
            }
        });
    }

    function EmailOtp(name, email, type)
    {
        $.ajax({
            type: "post",
            url: "{{route('signup.emailotp')}}",
            data: {
                    'name':name,
                    'email':email,
                    'type':type,
                    "_token": "{{ csrf_token() }}"
                },
            dataType:'json',
            success: function(result){
            }
        });
    }

</script>