@extends('front.layouts.master')
@section('title','Dashboard')
@push('style')

@endpush
@section('content')

 <!--Start User Profile Area-->
 <div class="user-profile-area pd-top-120 mb-5 ">
    <div class="container">
        <div class="row">
            <div class="col-xl-10 col-lg-12 mx-auto">
                <div class="row">

                    <div class="col-xl-7 col-lg-8 mx-auto">
                        <div class="tab-content user-tab-content">
                            <div class="tab-pane fade show active" id="tabs_1">
                                <div class="user-details">
                                    <!-- <h3 class="user-details-title">Profile</h3> -->
                                    <div class="tp-img-upload">
                                        <div class="tp-avatar-preview">
                                            <div id="tp_imagePreview"
                                                style="background-image: url(assets/img/team/1.png);">
                                            </div>
                                        </div>
                                        <div class="tp-avatar-edit">
                                            <input type="file" id="tp_imageUpload" accept=".png, .jpg, .jpeg">
                                            <label class="btn btn-transparent" for="tp_imageUpload"><i
                                                    class="fa fa-picture-o"></i>Change Photo</label>
                                            <h4 class="name">Afsar Hossen</h4>
                                        </div>
                                    </div>
                                    <form class="tp-form-wrap">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label class="single-input-wrap style-two">
                                                    <span class="single-input-title">First Name</span>
                                                    <input type="text" value="Afsar" disabled>
                                                </label>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="single-input-wrap style-two">
                                                    <span class="single-input-title">Last Number</span>
                                                    <input type="text" value="Hossen" disabled>
                                                </label>
                                            </div>

                                            <div class="col-md-7">
                                                <label class="single-input-wrap style-two">
                                                    <span class="single-input-title">Country</span>
                                                    <input type="text" value="India" disabled>
                                                </label>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="single-input-wrap style-two">
                                                    <span class="single-input-title">Email Address</span>
                                                    <input type="text" value="imshuvo97@gmail.com" disabled>
                                                </label>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="single-input-wrap style-two">
                                                    <span class="single-input-title">Other Phone</span>
                                                    <input type="text" value="1234567890" disabled>
                                                </label>
                                            </div>

                                        </div>
                                    </form>
                                </div>
                            </div>
                           
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--End User Profile Area-->

@endsection

@push('script')
@endpush