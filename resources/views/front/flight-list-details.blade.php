@extends('front.layouts.master')
@section('title','Gallary')
@push('style')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
<style type="text/css">
    .flight-icon{
        width: 20%;
    }  

    .card {
        
        border: 0px solid rgba(0,0,0,.125) !important;
    }  
    .flightResultWrapper {
    /*border: 1px solid #e4e4e4;*/
    box-sizing: border-box;
    background: #fff;
    padding: 5%;
    position: relative;
    margin: 0 auto 4%;
    width: 90%;
}
    .flightResultWrapper .flightResultHeader {
        align-items: center;
        display: flex;
        margin-bottom: 20px;
        overflow: hidden;
    }
    * {
    padding: 0;
    margin: 0;
    box-sizing: border-box;
}
.flightResultWrapper .flightResultHeader h4 {
    color: #333;
    font-size: 21px;
    font-weight: 500;
    text-transform: capitalize;
    font-family: revert;
}
.flightResultWrapper .flightResultHeader p {
    color: #888;
    font-size: 12px;
}
.flightResultWrapper .flightResultHeader > div + div {
    margin-left: 3%;
}
.flightResultWrapper .flightResultHeader > div + div {
    margin-left: 3%;
}
.flightResultWrapper .flightResultHeader h4 {
    color: #333;
    font-size: 21px;
    font-weight: 500;
    text-transform: capitalize;
}
.flightResultWrapper .flightResultHeader p {
    color: #888;
    font-size: 12px;
}
#FlightResult table {
    border-collapse: collapse;
    text-align: left;
    width: 100%;
    table-layout: fixed;
}
#FlightResult table thead {
    border-bottom: 5px solid #fff;
}
#FlightResult table tbody tr {
    background: #f7f7f7;
    border-bottom: 5px solid #fff;
}
#FlightResult table tbody tr td {
    color: #333;
    font-size: 13px;
    padding: 5px;
    margin-bottom: 5px;
}
.mg-top-50{
    margin-top: 50px;   
}
input[type=button] {
    background: #f3941e;
    color: #fff;
    cursor: pointer;
    font-size: 13px;
    padding: 8px;
    text-transform: capitalize;
    transition: 0.3s;
}
@media only screen and (max-width: 768px){
    .flightResultWrapper {
        margin: 0 auto 2%;
        padding: 3%;
        width: 96%;
    }
    .flight-icon {
        width: 100%;
    }
    th{
        font-size: 10px;
        padding: 17px;
    }
    .bookTicBtn{
        font-size: 5px;
    }
}

@media only screen and (max-width: 1680px) {
    .flightResultWrapper {
        margin: 0 auto 2%;
        padding: 3%;
        width: 96%;
    }
    .flight-icon {
        width: 100%;
    }
    th{
        font-size: 10px;
        padding: 17px;
    }
    input[type=button]{
        font-size: 5px;
    }
}

@media only screen and (max-width: 1280px) {
    .flightResultWrapper {
        margin: 0 auto 2%;
        padding: 3%;
        width: 96%;
    }
    .flight-icon {
        width: 100%;
    }
    th{
        font-size: 10px;
        padding: 17px;
    }
    input[type=button]{
        font-size: 5px;
    }
}

@media only screen and (min-width: 992px) {

    .flightResultWrapper {
        margin: 0 auto 2%;
        padding: 3%;
        width: 96%;
    }
    .flight-icon {
        width: 20%;
    }
    th{
        font-size: 15px;
        padding: 4px;
    }
    input[type=button]{
        font-size: 12px;
    }
} 

@media only screen and (max-width: 991px) {
    .flightResultWrapper {
        margin: 0 auto 2%;
        padding: 3%;
        width: 96%;
    }
    .flight-icon {
        width: 100%;
    }
    th{
        font-size: 10px;
        padding: 17px;
    }
    
}

@media only screen and (min-width: 576px) and (max-width:991px) {
    .flightResultWrapper {
        margin: 0 auto 2%;
        padding: 3%;
        width: 96%;
    }
    .flight-icon {
        width: 100%;
    }
    th{
        font-size: 10px;
        padding: 17px;
    }
     input[type=button]{
        font-size: 5px;
    }
}
</style>

@endpush
@section('content')
<!-- breadcrumb area start -->
<div class="breadcrumb-area jarallax" style="background-image:url(assets/img/bg/1.png);">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-inner">
                    <h1 class="page-title">Flight List</h1>
                    <ul class="page-list">
                        <li><a href="#">Home</a></li>
                        <li>Flight List</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div><br><br>
<!-- breadcrumb area End -->

<!-- search area start -->
    <div class="search-area tp-main-search-area">
        <div class="container">
            <div class="tp-main-search">
                <div class="row">
                    <div class="col-lg-2 col-md-4">
                        <div class="tp-search-single-wrap">
                            <input class="w-100" type="text" placeholder="Mumbai (BOM)">
                            <i class="ti-location-pin"></i>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-4">
                        <div class="tp-search-single-wrap">
                            <input class="w-100" type="text" placeholder="Delhi (DEL)">
                            <i class="ti-location-pin"></i>
                        </div>
                    </div>
                    <div class="col-lg-1 col-md-1 order-lg-9">
                        <div class="tp-search-single-wrap float-left w-100">
                            <select class="select w-100" style="padding: 0 15px;" required="" data-bv-notempty-message="The Adult is required" name="adult">
                                <option value="0">1</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-1 col-md-1 order-lg-9">
                        <div class="tp-search-single-wrap float-left w-100">
                            <!-- <input class="w-100" type="text" name="child" placeholder="Child" style="padding: 0 15px;"> -->
                             <select class="select w-100" style="padding: 0 15px;" required="" data-bv-notempty-message="The Child is required" name="child">
                                <option value="">0</option>
                                <option value="0">0</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-4 order-lg-9">
                        <div class="tp-search-single-wrap float-left w-100">
                            <select class="select w-100">
                                <option value="">Normal Fare</option>
                                <option value="AN">Normal Fare</option>
                                <option value="AC">Coupon Fare</option>
                                <option value="ACO">Corporate Fare</option>
                                <option value="AR">Return Fare</option>
                                <option value="AS">Sale Fare</option>
                                <option value="AF">Family Fare</option>
                                <option value="AFF">Flexi Fare</option>
                                <option value="AT">Tactical Fare</option>
                                <option value="AM">SME Fare</option>
                                <option value="ASR">Special Round Trip</option>
                            </select>
                            <i class="fa fa-plus-circle"></i>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-8 order-lg-6">
                        <div class="tp-search-single-wrap float-left">
                            <div class="tp-search-date tp-departing-date-wrap w-50 float-left">
                                <input type="text" class="departing-date" placeholder="2021-10-15">
                                <i class="fa fa-calendar-minus-o" aria-hidden="true"></i>
                            </div>
                            <div class="tp-search-date tp-returning-date-wrap w-50 float-left">
                                <input type="text" class="returning-date" placeholder="Returning">
                                <img src="assets/img/icons/2.png" alt="icons">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-1 col-md-4 order-12">
                        <a class="btn btn-yellow" href="#"><i class="ti-search"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- search area end -->

    <!-- destinations-details-page start -->
    <div class="destinations-details-page mg-top-50">
        <div class="trip-plan-area">
            <div class="row justify-content-center">
                <div class="col-lg-12 col-md-6">
                    <div id="FlightResult">
                        <div class="flightOnward flightResultWrapper">
                            <div class="flightResultHeader">
                                <div class="headerStartingDate">
                                    <h4 class="headStartLoc">{{ $fromCity }}</h4>
                                    <p class="headStartDate">{{ $leaveDate }}</p>
                                </div>
                                <div>
                                    @if(isset($returnDate))
                                        <img src="assets/img/icons/2.png" alt="icons">
                                    @else
                                        <i class="exchangeIc fa fa-long-arrow-right"></i>
                                    @endif
                                </div>
                                <div class="headeReturnDate">
                                    <h4 class="headEndLoc">{{ $toCity }}</h4>
                                    <p class="headEndDate">@if(isset($returnDate)){{ $returnDate }}@else -- @endif</p>
                                </div>
                            </div>
                            
                            @if(isset($DELAMD))
                            <div class="row">
                                <div class="col-md-6">
                                    <table id="table">
                                        <thead>
                                            <tr>
                                                <th>Flight Name</th>
                                                <th>Departure 
                                                    &nbsp;<i class="fa fa-sort" aria-hidden="true"></i>
                                                </th>
                                                <th>Arrival 
                                                    &nbsp;<i class="fa fa-sort" aria-hidden="true"></i>
                                                </th>
                                                <th>Duration</th>
                                                <th>Price 
                                                    &nbsp;<i class="fa fa-sort" aria-hidden="true"></i>
                                                </th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($AMDDEL as $link)
                                                @foreach($link as $value)
                                                @foreach($value['fares'] as $key)
                                                    
                                                    <tr>
                                                        <td><img class="flight-icon" src="demo/flight/spicejet.png" alt="blog"></td>
                                                        <td>{{strrev (substr_replace(strrev ( $key['dept_time']),':',2,0))}}</td>
                                                        <td>{{strrev (substr_replace(strrev ( $key['arrive_time']),':',2,0))}}</td>
                                                        <td>2hrs 10mins
                                                            <br><span>0 Stop(s)</span>
                                                        </td>
                                                        <td>{{$key['total']}}</td>
                                                        <td>
                                                            <input type="button" class="bookTicBtn" value="Book Now">
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                @endforeach          
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-md-6">
                                    <table id="table">
                                        <thead>
                                            <tr>
                                                <th>Flight Name</th>
                                                <th>Departure 
                                                    &nbsp;<i class="fa fa-sort" aria-hidden="true"></i>
                                                </th>
                                                <th>Arrival 
                                                    &nbsp;<i class="fa fa-sort" aria-hidden="true"></i>
                                                </th>
                                                <th>Duration</th>
                                                <th>Price 
                                                    &nbsp;<i class="fa fa-sort" aria-hidden="true"></i>
                                                </th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($DELAMD as $link)
                                                @foreach($link as $value)
                                                    @foreach($value['fares'] as $key)
                                                        <tr>
                                                            <td><img class="flight-icon" src="demo/flight/spicejet.png" alt="blog"></td>
                                                            <td>{{strrev (substr_replace(strrev ( $key['dept_time']),':',2,0))}}</td>
                                                            <td>{{strrev (substr_replace(strrev ( $key['arrive_time']),':',2,0))}}</td>
                                                            <td>2hrs 10mins
                                                                <br><span>0 Stop(s)</span>
                                                            </td>
                                                            <td>{{$key['total']}}</td>
                                                            <td>
                                                                <input type="button" class="bookTicBtn" value="Book Now">
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                @endforeach
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            @else
                            <table id="table">
                                <thead>
                                    <tr>
                                        <th>Flight Name</th>
                                        <th>Departure 
                                            &nbsp;<i class="fa fa-sort" aria-hidden="true"></i>

                                        </th>
                                        <th>Arrival 
                                            &nbsp;<i class="fa fa-sort" aria-hidden="true"></i>

                                        </th>
                                        <th>Duration</th>
                                        <th>Price 
                                            &nbsp;<i class="fa fa-sort" aria-hidden="true"></i>

                                        </th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {{--@foreach($AMDDEL as $link)--}}
                                    @foreach($AMDDEL as $link)
                                        @foreach($link as $value)
                                            @foreach($value['fares'] as $key)
                                            {{dd($key)}}
                                            <tr>
                                                <td><img class="flight-icon" src="demo/flight/spicejet.png" alt="blog"></td>
                                                <!-- <td>{{date('H:i:s',$link['AN']['fares'][0]['dept_time'])}}</td>
                                                
                                                <td>{{date('H:i:s',$link['AN']['fares'][0]['arrive_time'])}}</td> -->

                                                <td>{{strrev (substr_replace(strrev ( $key['dept_time']),':',2,0))}}</td>
                                                <td>{{strrev (substr_replace(strrev ( $key['arrive_time']),':',2,0))}}</td>

                                                {{-- dd(date_create(strrev (substr_replace(strrev ( $key['dept_time']),':',2,0)))->diff(date_create(strrev (substr_replace(strrev ( $key['arrive_time']),':',2,0))))->format('%H:%I')) --}}
                                                
                                                <td>{{date_create(strrev (substr_replace(strrev ( $key['dept_time']),':',2,0)))->diff(date_create(strrev (substr_replace(strrev ( $key['arrive_time']),':',2,0))))->format('%H:%I')}}
                                                    <br><span>0 Stop(s)</span>
                                                </td>
                                                
                                                <td>{{$link['AN']['fares'][0]['total']}}</td>
                                                <td>
                                                    <input type="button" class="bookTicBtn" value="Book Now">
                                                </td>
                                            </tr>
                                            @endforeach
                                    @endforeach
                                    @endforeach
                                </tbody>
                            </table>
                            @endif
                        </div>
                        <div class="flightReturn flightResultWrapper" style="display: none;">
                            <div class="flightResultHeader">
                                <div class="headerStartingDate">
                                    <h4 class="returnStartLoc"></h4>
                                    <p class="returnStartDate"></p>
                                </div>
                                <div>
                                    <i class="exchangeIc fa fa-long-arrow-right"></i>
                                </div>
                                <div class="headeReturnDate">
                                    <h4 class="returnEndLoc"></h4>
                                    <p class="returnEndDate"></p>
                                </div>
                            </div>
                                <table id="table1"></table>
                        </div>
                    </div>
                </div>
            </div><br>
        </div>
        <!-- trip-plan End -->
    </div>
</div>
@endsection

@push('script')
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta2/js/all.min.js"></script> -->
@endpush