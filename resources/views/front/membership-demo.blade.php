@extends('front.new-layouts.master')
@section('title',"Membership Plan")
@section('description',"Description of Jag Joyu")
@section('keywords',"jagjoyu, travel, travel company in ahmedabad")
@section('content')
      <!-- breadcrumb area start -->
    <div class="breadcrumb-area style-two jarallax" style="background-image:url(assets/img/bg/1.png);">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb-inner">
                        <h1 class="page-title">Membership Plan</h1>
                        <ul class="page-list">
                            <li><a href="{{url('/')}}">Home</a></li>
                            <li>Membership Plan</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- breadcrumb area End -->

    <!-- client area end -->
   <div class="upcomming-tour upcomming-tour-bg pd-top-75 pd-bottom-120" style="background-image: url(assets/img/bg/11.png);">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-6 col-lg-8">
                    <div class="section-title">
                       <!--  <h2 class="title">What Our Clicnts Say</h2> -->
                       <br><br>
                    </div>
                </div>
            </div>
            <div class="swiper-container client-slider-two" style="width: 1500px;height: 600px;">
                <div class="swiper-wrapper">
                    <!-- item -->
                    <div class="swiper-slide" >
                        <div class="client-slider-item" style="width: 1300px;">
                            <div class="row" >
                                <div class="col-lg-5 thumb">
                                    <img src="demo/assets/img/tour/01.png" width="100%">
                                </div>
                                <div class="col-lg-7" style="background-color: #e1e1e182;">
                                    <div class="details">
                                        <div class="title-meta">
                                            <center><h3 style="color:#00133f;">Gold</h3></center>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-6 text-center">
                                                <span>2 Years</span>
                                                <h4>Rs.300000</h4>
                                            </div>

                                            <div class="col-6 text-center">
                                                <span>2 Years</span>
                                                <h4>Rs.300000</h4>
                                            </div>
                                            <div class="col-6 text-center">
                                                <span>2 Years</span>
                                                <h4>Rs.300000</h4>
                                            </div>
                                            <div class="col-6 text-center">
                                                <span>2 Years</span>
                                                <h4>Rs.300000</h4>
                                            </div>
                                            <p style="margin-left: 130px;">The largest country in the world, Russia offers a broad array of travel experiences.</p>
                                        </div> 
                                        <center><a class="btn btn-yellow" href="{{route('login')}}"><span>Know More<i class="la la-arrow-right"></i></span></a></center>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- item -->
                    <div class="swiper-slide" >
                        <div class="client-slider-item" style="width: 1300px;">
                            <div class="row" >
                                <div class="col-lg-5 thumb">
                                    <img src="demo/assets/img/tour/2.png" width="100%">
                                </div>
                                <div class="col-lg-7" style="background-color: #e1e1e182;">
                                    <div class="details">
                                        <div class="title-meta">
                                            <center><h3 style="color:#00133f;">Silver</h3></center>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-6 text-center">
                                                <span>2 Years</span>
                                                <h4>Rs.300000</h4>
                                            </div>

                                            <div class="col-6 text-center">
                                                <span>2 Years</span>
                                                <h4>Rs.300000</h4>
                                            </div>
                                            <div class="col-6 text-center">
                                                <span>2 Years</span>
                                                <h4>Rs.300000</h4>
                                            </div>
                                            <div class="col-6 text-center">
                                                <span>2 Years</span>
                                                <h4>Rs.300000</h4>
                                            </div>
                                            <p style="margin-left: 130px;">The largest country in the world, Russia offers a broad array of travel experiences.</p>
                                        </div> 
                                        <center><a class="btn btn-yellow" href="{{route('login')}}"><span>Know More<i class="la la-arrow-right"></i></span></a></center>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- item -->
                    <div class="swiper-slide" >
                        <div class="client-slider-item" style="width: 1300px;">
                            <div class="row">
                                <div class="col-lg-5 thumb">
                                    <img src="demo/assets/img/tour/3.png" width="100%">
                                </div>
                                <div class="col-lg-7" style="background-color: #e1e1e182;">
                                    <div class="details">
                                        <div class="title-meta">
                                            <center><h3 style="color:#00133f;">Platinum</h3></center>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-6 text-center">
                                                <span>2 Years</span>
                                                <h4>Rs.300000</h4>
                                            </div>

                                            <div class="col-6 text-center">
                                                <span>2 Years</span>
                                                <h4>Rs.300000</h4>
                                            </div>
                                            <div class="col-6 text-center">
                                                <span>2 Years</span>
                                                <h4>Rs.300000</h4>
                                            </div>
                                            <div class="col-6 text-center">
                                                <span>2 Years</span>
                                                <h4>Rs.300000</h4>
                                            </div>
                                            <p style="margin-left: 130px;">The largest country in the world, Russia offers a broad array of travel experiences.</p>
                                        </div>
                                        <center><a class="btn btn-yellow" href="{{route('login')}}"><span>Know More<i class="la la-arrow-right"></i></span></a></center> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Add Pagination -->
                <div class="tp-control-nav text-center">
                    <div class="slick-arrow swiper-buttons-prev"><i class="la la-long-arrow-left"></i></div>
                    <div class="slick-arrow swiper-buttons-next"><i class="la la-long-arrow-right"></i></div>
                </div>
                <!-- /.end carousel -->                    
            </div>
        </div>
    </div>
    <!-- client area end -->
@endsection
@push('script')
<script type="text/javascript">
    var mySwiper = new Swiper ('.swiper-container', 
    {
    speed:1000,
        direction: 'horizontal',
        navigation: 
        {
          nextEl: '.swiper-button-next',
          prevEl: '.swiper-button-prev',
        },
        pagination: 
        {
            el: '.swiper-pagination',
            dynamicBullets: true,
        },
        zoom: true,
        keyboard: 
        {
            enabled: true,
            onlyInViewport: false,
        },
        mousewheel: 
        {
            invert: true,
        },
    autoplay: 
    {
      delay: 2000,
    },
    loop: true,
    responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            adaptiveHeight: true,
          },
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          },
        },
      ],
    }); 
</script>
@endpush