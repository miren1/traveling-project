@extends('front.new-layouts.master')
@section('title','Careers')
@push('style')
<style type="text/css">
.breadcrumb-area.style-two {
    padding: 100px 0 50px !important;
}

.vacancy-title{
    font-size: 20px !important;
    margin-bottom: 10px !important;
    color:  #fff !important;
}
</style>
@endpush
@section('content')
    <!-- breadcrumb area start -->
    <div class="breadcrumb-area style-two jarallax" style="background-image:url(assets/img/bg/1.png);">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb-inner">
                        <h1 class="page-title">Career</h1>
                        <ul class="page-list">
                            <li><a href="{{url('/')}}">Home</a></li>
                            <li>Career</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- breadcrumb area End --> 
    <!--Start Job Post Profle Area-->
    <div class="faq-page-area pd-top-110">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12">
                    <div class="row">
                        <div class="col-md-4">
                            <ul class="nav nav-tabs tp-tabs">
                                @foreach($career as $key => $category)
                                <li class="nav-item">
                                    <a class="nav-link @if ($key === 0) active @endif show" data-toggle="tab" href="#tabs_{{$key+1}}">{{$category->department}}</a>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="col-md-8">
                            <div class="tab-content faq-tab-content"
                                style="background-image: url(assets/img/others/12.png);">
                                @foreach($career as $key => $content)
                                <div class="tab-pane fade @if ($key === 0) active @endif show" id="tabs_{{$key+1}}">
                                    <div class="faq-details">
                                        <center><h6 class="vacancy-title">Vacancy Details</h6></center>
                                        <hr class="border border-bottom border-warning">
                                        <table>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <h6>Opening Title : 
                                                            <span style="color:#fff;">{{$content->opening_title}}</span>
                                                        </h6>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <h6>Number Of Openings : 
                                                            <span style="color:#fff;">{{$content->number_of_openings}}</span>
                                                        </h6>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <h6>Experience : 
                                                            <span style="color:#fff;">{{$content->relevant_experience_required}}</span>
                                                        </h6>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <h6>Role & Responsibilities :
                                                            <span style="color:#fff;">{!! ($content->roles_responsibilities)!!}</span>
                                                        </h6>

                                                        <!-- <span class="text-white">
                                                            {!! ($content->roles_responsibilities) !!}
                                                            
                                                        </span> -->
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <h6>Base Location : 
                                                            <span style="color:#fff;">{!! ($content->base_location)!!}</span>
                                                        </h6>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <h6>Primary Skill :
                                                            <span style="color:#fff;">{!! ($content->primary_skills)!!}</span>
                                                        </h6>
                                                        <!-- <span class="text-white">
                                                            {!! ($content->primary_skills)!!}
                                                        </span> -->
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <h6>Pre Requiesite : 
                                                            <span style="color:#fff;">{!! ($content->pre_requiesite)!!}</span>
                                                        </h6>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <h6>Advantage : 
                                                            <span style="color:#fff;">{!! ($content->advantage)!!}</span>
                                                        </h6>
                                                    </td>
                                                </tr>
                                                @if($content->work_flexibility == 'yes')
                                                <tr>
                                                    <td>
                                                        <h6>Work Flexibility : 
                                                            <span style="color:#fff;">{{$content->work_flexibility}}</span>
                                                        </h6>
                                                    </td>
                                                </tr>
                                                @endif
                                                @if($content->salary_range == 'yes')
                                                <tr>
                                                    <td>
                                                        <h6>Salary Range : 
                                                            <span style="color:#fff;">{{$content->salary_range}}</span>
                                                        </h6>
                                                    </td>
                                                </tr>
                                                @endif
                                                @if($content->incentives == 'yes')
                                                <tr>
                                                    <td>
                                                        <h6>Incentives : 
                                                            <span style="color:#fff;">{{$content->incentives}}</span>
                                                        </h6>
                                                    </td>
                                                </tr>
                                                @endif
                                                @if($content->benefits == 'yes')
                                                <tr>
                                                    <td>
                                                        <h6>Benefits : 
                                                            <span style="color:#fff;">{{$content->benefits}}</span>
                                                        </h6>
                                                    </td>
                                                </tr>
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!--End Job Post Profile Area-->

    <!--Start Apply Post Form Area-->

    <!--End Apply Post Form Area-->
    
    <!--Start Our Team Area-->
   <!--  <div class="team-area pd-top-70">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="section-title text-center">
                        <h2 class="title">Our Team</h2>
                        <p>Donec dapibus mauris id odio ornare tempus. Duis sit amet accumsan justo, quis tempor
                            ligula. Quisque quis pharetra felis. Ut quis consequat orci, at consequat felis.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-sm-6"></div>
                <div class="col-lg-3 col-sm-6">
                    <div class="single-team text-center">
                        <div class="thumb">
                            <img src="assets/img/team/1.png" alt="team">
                        </div>
                        <h3 class="name"><a href="#">Amber Reid</a></h3>
                        <span>Creative Director</span>
                        <ul class="team-social">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
                
                <div class="col-lg-3 col-sm-6">
                    <div class="single-team text-center">
                        <div class="thumb">
                            <img src="assets/img/team/2.png" alt="team">
                        </div>
                        <h3 class="name"><a href="#">Bruce Pearson</a></h3>
                        <span>Creative Director</span>
                        <ul class="team-social">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>                
                </div>
            </div>
        </div>
    </div> -->
    <!--End our Team Area-->

    <!-- newslatter area Start -->
    <!-- <div class="newslatter-area pd-top-90">
        <div class="container">
            <div class="newslatter-area-wrap border-tp-solid">
                <div class="row">
                    <div class="col-xl-3 col-lg-6 col-md-5 offset-xl-2">
                        <div class="section-title mb-md-0">
                            <h2 class="title">Newsletter</h2>
                            <p>Please Sign up</p>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-6 col-md-7 align-self-center offset-xl-1">
                        <div class="input-group newslatter-wrap">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fa fa-envelope"></i></span>
                            </div>
                            <input type="text" class="form-control" placeholder="Email">
                            <div class="input-group-append">
                                <button class="btn btn-yellow" type="button">Subscribe</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <!-- newslatter area End -->

@endsection
@push('script')
@endpush