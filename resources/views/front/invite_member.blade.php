@extends('front.layouts.master')
@section('title','Membership Registration')

@push('style')
<style>
.card-design{
    box-shadow: 0px 0px 11px 0px rgb(0 0 0 / 25%);
    margin-top: 120px;
    margin-bottom: 50px;
    background: #f8f8f8;
}
.title-font{
    font-family: unset;
}
.bg-gray {
    margin-bottom: 17px !important;
}
/* Chrome, Safari, Edge, Opera */
input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
  -webkit-appearance: none;
  margin: 0;
}

/* Firefox */
input[type=number] {
  -moz-appearance: textfield;
}
</style>
@section('content')
<div class="breadcrumb-area jarallax" style="background-image:url(/assets/img/bg/1.png); z-index: 0;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-inner">
                    <div class="row justify-content-center">
                        <div class="col-xl-6 col-lg-8">
                            <div class="section-title mb-2 text-center style-two">
                                <h2 class="title">Invite <span>Member</span></h2>
                                <p>Enter member basic details</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb area End -->
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="tour-details-wrap">
                <div class="package-included-area"> 
                    <div class="package-included-location">
                        <div class="section-title text-lg-center text-left my-3">
                            <!-- <h2 class="title"><label>Member&nbsp;</label>Details</h2> -->
                        </div>
                    </div>
                    <section class="forms-section tp-form-wrap bg-gray tp-form-wrap-one">
                        <div class="outer-w3-agile mt-3">
                            <h4 class="tittle-w3-agileits title-font text-center mb-4">Add invite member </h4>
                            <form action="{{route('member-plans-store-member-req')}}" id="customerForm" method="post">
                                @csrf
                                @if(isset($plan))
                                    <input type="hidden" name="id" value="{{$plan->id}}">
                                @endif
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="inputEmail4">First Name</label>
                                        <input type="text" class="form-control" id="first_name" placeholder="Enter Name" required name='first_name' value="@if(isset($customer)){{$customer->first_name}}@endif" data-bv-notempty="true" data-bv-notempty-message="First Name is required">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="inputPassword4">Last Name</label>
                                        <input type="text" class="form-control" id="last_name" placeholder="Last Name" required name='last_name' value="@if(isset($customer)){{$customer->last_name}}@endif" data-bv-notempty="true" data-bv-notempty-message="Last Name is required">
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="inputEmail4">Contact Number</label>
                                        <input type="number" min="0" minlength="10" maxlength="10" class="form-control" id="contactnumber" placeholder="Enter Contact Number" value="@if(isset($customer)){{$customer->mobile_number}}@endif" required="" name='mobile_number' data-bv-notempty="true" data-bv-notempty-message="Contact Number is required">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="inputPassword4">WhatsApp Number</label>
                                        <input type="number" min="0" minlength="10" maxlength="10" class="form-control" id="wanumber" placeholder="Enter WhatsApp  Number" required="" name='whats_up' value="@if(isset($customer)){{$customer->whats_up}}@endif" data-bv-notempty="true" data-bv-notempty-message="WhatsApp Number is required">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label for="inputEmail4">Email</label>
                                        <input type="email" class="form-control" id="inputEmail4" pattern="[a-zA-Z0-9._%+-]+@[a-z0-9.-]+\.[a-zA-Z]{2,4}" placeholder="Email" required="" name='email' value="@if(isset($customer)){{$customer->email}}@endif" data-bv-notempty="true" data-bv-notempty-message="E-mail is required"
                                         data-bv-remote="true" data-bv-remote-type="GET"
                                        data-bv-remote-url="{{ url('invite/email') }}/@if(isset($customer)){{$customer->id}}@else All @endif"
                                        data-bv-remote-message="Opps ! Email Already Exist"
                                    </div>
                                </div>
                                
                                <button type="submit" class="btn btn-yellow" style="float: right; margin-top: -5px;">@if(isset($customer)) Update @else @endif invite</button>
                            </form>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('style')
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css"/>
@endpush
@push('script')
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js"></script>

<script>
  $(document).ready(function () {
    $('#customerForm').bootstrapValidator();
  });

  $('#first_name').keypress(function (e) {
        var regex = new RegExp("^[a-zA-Z ]+$");
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (regex.test(str)) {
            return true;
        }
        else
        {
        e.preventDefault();
        alert('Please Enter Alphabate and space only');
        return false;
        }
    });

  $('#last_name').keypress(function (e) {
        var regex = new RegExp("^[a-zA-Z ]+$");
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (regex.test(str)) {
            return true;
        }
        else
        {
        e.preventDefault();
        alert('Please Enter Alphabate and space only');
        return false;
        }
    });
</script>
@endpush