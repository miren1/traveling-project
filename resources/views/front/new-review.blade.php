@extends('front.new-layouts.master')
@section('title',"India's No. 1 - Travel Investment Membership Plans  - Jag Joyu")
@section('content')
    <!-- breadcrumb area start -->
    <div class="breadcrumb-area jarallax" style="background-image:url(assets/img/bg/1.png);">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb-inner">
                        <h1 class="page-title">Review</h1>
                         <ul class="page-list">
                            <li><a href="{{url('/')}}">Home</a></li>
                            <li>Review</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- breadcrumb area End -->

    <!-- client area start -->
   <div class="client-area pd-top-10">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-6 col-lg-8">
                    <div class="section-title text-center">
                        <h2 class="title" style="margin-bottom: 20px">What Our Clients Say</h2>
                    </div>
                </div>
            </div>
            <!-- {{--<div class="client-slider tp-common-slider-style">
                @if(isset($review))
                    @foreach($review as $key)

                    <div class="single-client-card">
                        <div class="quote"><i class="ti-quote-left"></i></div>   
                        <p class="content-text">{{$key->content}}</p>
                        <div class="media">
                            <div class="media-body">
                                @if($key->user)
                                    @foreach($key->user as $user)
                                        <h4>{{$user->name}} {{$user->surname}}</h4>
                                    @endforeach
                                @endif
                                @if($key->member)
                                    @foreach($key->member as $member)
                                        <h4>{{$member->name}}</h4>
                                    @endforeach
                                @endif
                                <span>{{$key->role}}</span>
                            </div>
                        </div>
                    </div>
                    @endforeach
                @endif 
            </div> --}} -->
            <div class="blog-slider tp-common-slider-style tps-arrow-left-right" style="margin-bottom: 50px">
                @if(isset($review))
                    @foreach($review as $key)
                    <div class="blog-slider-item">
                        <div class="single-blog single-blog-wrap style-four" style="height: 300px;">
                            <div class="thumb single-blog-left-wrap">
                                @if(isset($key->review_image))
                                    @foreach($key->review_image as $keyImage)
                                        <img src="{{$keyImage->image}}" alt="blog" style="height:180px;width:250px;">
                                    @endforeach
                                @else
                                    <img src="assets/img/blog/11.png" alt="blog">
                                @endif
                            </div>
                            <div class="single-blog-details single-blog-right-wrap">
                               <div> <p class="content more" style="height: 116px;">{{$key->content}}</p></div>
                                <div class="media">
                                    <div class="media-body">
                                        @if(isset($key->customer))
                                            @foreach($key->customer as $customers)
                                                <h4>{{$customers->first_name}} {{$customers->last_name}}</h4>
                                            @endforeach
                                        @endif
                                        @if(isset($key->member))
                                            @foreach($key->member as $members)
                                                <h4>{{$members->name}}</h4>
                                            @endforeach
                                        @endif
                                        <span>{{$key->role}}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
    <!-- client area end -->
@endsection
@section('css')
<style type="text/css">
.moreelipses
{
    display: none !important;
}
</style>
@endsection

@push('script')
<script src="https://www.ostraining.com/images/coding/bpopup/jquery.bpopup.min.js"></script>
<script type="text/javascript">
 $( document ).ready(function() {
    $('#mypopup').bPopup();
    $('iframe #toolbar').css('display','none');
    //read more
    var showChar = 100;
    var moretext = "<b> Read More (+)</b>";
    var lesstext = "<b>Read Less (-)</b>";
    $('.more').each(function() {
        var content = $(this).html();

        if(content.length > showChar) {

            var c = content.substr(0, showChar);
            var h = content.substr(showChar-1, content.length - showChar);

            var html = c + '<span class="moreelipses" ></span><span class="morecontent" style="display: none;">' + h + '&nbsp;&nbsp;</span>&nbsp;&nbsp;<a href="" class="morelink less">'+moretext+'</a>';

            $(this).html(html);
        }
    });
    $(".morelink").click(function(){
        if($(this).hasClass("less")) {
            $(this).removeClass("less");
            $(this).addClass("more");
            $(this).html(lesstext);
        } else {
            $(this).removeClass("more");
            $(this).addClass("less");
            $(this).html(moretext);
        }
        $(this).parent().prev().toggle();
        $(this).prev().toggle();
        return false;
    });
});
</script>
@endpush