@extends('front.layouts.master')
@section('title','Reviews')
@push('style')
<style>
    .holiday-plan-area .thumb img {
    height: 350px;
    }
    .package-margin{
        padding-top: 20px !important;
    }
</style>
@endpush
@section('content')
@if(isset($sliders))
@include('front.layouts.banner')
@endif


<!-- offer area start -->
<div class="offer-area pd-top-70 mb-5">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-6 col-lg-8">
                <div class="section-title text-center">
                    <h2 class="title">Reviews</h2>
                   
                </div>
            </div>
        </div>
    </div>
    <div class="destinations-list-slider-bg">
        <div class="container">
            <div class="row">
                <div class="col-xl-9 col-lg-10 offset-xl-1 order-lg-12">
                    <div class="destinations-list-slider">
                        <div class="d-list-slider-item">
                            <div class="single-destinations-list text-center">
                                <div class="thumb">
                                    <!-- <span class="d-list-tag">Special Offer</span> -->
                                    <img src="{{url('/front')}}/assets/img/destination-list/001.jpg" alt="list">
                                    
                                </div>
                                <div class="details textBlock equalHeight">
                                    <h4 class="title">Md Nahidul</h4>
                                    <p class="content text-justify">
                                        Dolorem accusamus voluptas possimus enim eveniet laboriosam nemo, voluptatum
                                        aliquid nostrum perferendis itaque eum, illum ullam dignissimos.</p>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="d-list-slider-item">
                            <div class="single-destinations-list text-center">
                                <div class="thumb">
                                    <!-- <span class="d-list-tag">Special Offer</span> -->
                                    <img src="{{url('/front')}}/assets/img/destination-list/002.jpg" alt="list">
                                    
                                </div>
                                <div class="details textBlock equalHeight">
                                    <h4 class="title">Emily Clark</h4>
                                    <p class="content text-justify">Lorem ipsum dolor sit amet, consectetur
                                        adipisicing elit.
                                        Dolorem accusamus voluptas possimus enim eveniet laboriosam nemo, voluptatum
                                        aliquid nostrum perferendis itaque eum, illum ullam dignissimos.</p>
                                </div>    
                            </div>
                        </div>
                        <div class="d-list-slider-item">
                            <div class="single-destinations-list text-center">
                                <div class="thumb">
                                    <!-- <span class="d-list-tag">Special Offer</span> -->
                                    <img src="{{url('/front')}}/assets/img/destination-list/003.jpg" alt="list">
                                    
                                </div>
                                <div class="details textBlock equalHeight">
                                    <h4 class="title">Cassi</h4>
                                    <p class="content text-justify">Lorem ipsum dolor sit amet, consectetur
                                        adipisicing elit.
                                        Dolorem accusamus voluptas possimus enim eveniet laboriosam nemo, voluptatum
                                        aliquid nostrum perferendis itaque eum, illum ullam dignissimos.</p>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="d-list-slider-item">
                            <div class="single-destinations-list text-center">
                                <div class="thumb">
                                    <!-- <span class="d-list-tag">Special Offer</span> -->
                                    <img src="{{url('/front')}}/assets/img/destination-list/004.jpg" alt="list">
                                    
                                </div>
                                <div class="details textBlock equalHeight">
                                    <h4 class="title">Lisa Redfern</h4>
                                    <p class="content text-justify">Lorem ipsum dolor sit, amet consectetur
                                        adipisicing elit. Explicabo fuga et, voluptas id exercitationem dignissimos!
                                    </p>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="d-list-slider-item">
                            <div class="single-destinations-list text-center">
                                <div class="thumb">
                                    <!-- <span class="d-list-tag">Special Offer</span> -->
                                    <img src="{{url('/front')}}/assets/img/destination-list/005.jpg" alt="list">
                                   
                                </div>
                                <div class="details textBlock equalHeight">
                                    <h4 class="title">Pravin Kumar</h4>
                                    <p class="content text-justify">Lorem ipsum dolor sit amet, consectetur
                                        adipisicing elit.
                                        Dolorem accusamus voluptas possimus enim eveniet laboriosam nemo, voluptatum
                                        aliquid nostrum perferendis itaque eum, illum ullam dignissimos.</p>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="d-list-slider-item">
                            <div class="single-destinations-list text-center">
                                <div class="thumb">
                                    <!-- <span class="d-list-tag">Special Offer</span> -->
                                    <img src="{{url('/front')}}/assets/img/destination-list/006.jpg" alt="list">
                                    
                                </div>
                                <div class="details textBlock equalHeight">
                                    <h4 class="title">Carl Kent</h4>
                                    <p class="content text-justify">Lorem ipsum dolor sit amet consectetur
                                        adipisicing elit. Ut labore eos neque recusandae, eum, molestiae molestias
                                        dolor maxime ratione placeat libero quas non vero quos.</p>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="d-list-slider-item">
                            <div class="single-destinations-list text-center">
                                <div class="thumb">
                                    <!-- <span class="d-list-tag">Special Offer</span> -->
                                    <img src="{{url('/front')}}/assets/img/destination-list/007.jpg" alt="list">
                                    
                                </div>
                                <div class="details textBlock equalHeight">
                                    <h4 class="title">Williamson</h4>
                                    <p class="content text-justify">Lorem ipsum dolor sit amet consectetur,
                                        adipisicing elit. Quisquam molestiae suscipit facere molestias, soluta
                                        asperiores dignissimos adipisci architecto? Vero, magnam?</p>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="d-list-slider-item">
                            <div class="single-destinations-list text-center">
                                <div class="thumb">
                                    <!-- <span class="d-list-tag">Special Offer</span> -->
                                    <img src="{{url('/front')}}/assets/img/destination-list/008.jpg" alt="list">
                                    
                                </div>
                                <div class="details textBlock equalHeight">
                                    <h4 class="title">Kristina</h4>
                                    <p class="content text-justify">Lorem ipsum dolor sit amet, consectetur
                                        adipisicing elit.
                                        Dolorem accusamus voluptas possimus enim eveniet laboriosam nemo, voluptatum
                                        aliquid nostrum perferendis itaque eum, illum ullam dignissimos.</p>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="d-list-slider-item">
                            <div class="single-destinations-list text-center">
                                <div class="thumb">
                                    <!-- <span class="d-list-tag">Special Offer</span> -->
                                    <img src="{{url('/front')}}/assets/img/destination-list/009.jpg" alt="list">
                                    
                                </div>
                                <div class="details textBlock equalHeight">
                                    <h4 class="title">Miranda Joy</h4>
                                    <p class="content text-justify">Lorem ipsum dolor sit amet, consectetur
                                        adipisicing elit.
                                        Dolorem accusamus voluptas possimus enim eveniet laboriosam nemo, voluptatum
                                        aliquid nostrum perferendis itaque eum, illum ullam dignissimos.</p>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="d-list-slider-item">
                            <div class="single-destinations-list text-center">
                                <div class="thumb">
                                    
                                    <img src="{{url('/front')}}/assets/img/destination-list/0010.jpg" alt="list">
                                    
                                </div>
                                <div class="details textBlock equalHeight">
                                    <h4 class="title">Steve Thomas</h4>
                                    <p class="content text-justify">Lorem ipsum dolor sit amet, consectetur
                                        adipisicing elit.
                                        Dolorem accusamus voluptas possimus enim eveniet laboriosam nemo, voluptatum
                                        aliquid nostrum perferendis itaque eum, illum ullam dignissimos.</p>
                                   
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-lg-2 align-self-center order-lg-11">
                    <div class="container">
                        <div class="destinations-slider-controls">
                            <div class="slider-nav tp-control-nav"></div>
                            <!--slider-extra-->
                            <div class="tp-slider-extra slider-extra">
                                <div class="text">
                                    <span class="first mr-2">01 </span>
                                    <span class="last"></span>
                                </div>
                                <!--text-->
                                <div class="d-list-progress" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                                    <span class="slider__label sr-only"></span>
                                </div>
                            </div>
                            <!--slider-extra-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- offer area end -->

@endsection


@push('script')


<script>
    $(document).ready(function() {
       var inof= {};
       
       getData(inof);
    });


   function getData(array){

       $.ajax({
           url: "{{route('tour.getTourType')}}", 
           type: "POST",
           dataType: "json",
           contentType: "application/json; charset=utf-8",
           data: JSON.stringify(array),
          // data:,
           success: function (result) {
               console.log(result.length);
               if(result.length!=0){
                   var $html = '';
                   $('#tour-type').html($html);
                   $.each(result, function(key, value) {
                   //console.log(value.name);
                       $html+='<div class="col-lg-4 col-sm-6">'+
                                '<a href="{{url("/packages")}}/'+value.slug+'">'+
                                '<div class="single-destinations-list style-two wow animated fadeInUp" data-wow-duration="0.4s"'+
                                ' data-wow-delay="0.1s">'+
                                '<div class="thumb">'+
                                        '<img src="{{url("uploads/plans_images")}}/'+value.photo+'" alt="'+value.name+'">'+
                                '</div>'+
                                '<div class="details">'+
                                    '<h4 class="title">'+value.name+'</h4>'+
                                '</div>'+
                                '</div></a></div>';
                   
                   });
                   $('#tour-type').html($html);
               }else{
                $('#tour-type').html('');
               }
           },
           error: function (err) {
           // check the err for error details
           }
       }); // ajax call closing

   }

</script>



@endpush