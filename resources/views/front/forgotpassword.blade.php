@extends('front.layouts.master')
@section('title', 'Forgot Password')
    @push('style')
        <!-- main css -->
        <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
        <!-- responsive css -->
        <link rel="stylesheet" href="{{ asset('assets/css/responsive.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/build/css/intlTelInput.css') }}">
        {{-- <link rel="stylesheet" href="{{asset('public/assets/build/css/demo.css')}}"> --}}
        <link rel="stylesheet"
            href="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css" />
        <style>
            .copyright-inner .privacypolicy {
                text-align: center;
                font-size: 14px;
                letter-spacing: 0.28px;
                color: #ffffff;
                line-height: 44px;
            }

            .login-password {
                display: none;
            }

            .input-group .custom-select {
                background-color: #f8f8f8;
                border: 1px solid #f8f8f8;
                height: 45px;
                width: 100%;
                padding: 0 18px;
                color: var(--paragraph-color);
            }

            .tab-pane .shadow {
                box-shadow: 0 .5rem 1rem rgba(0, 0, 0, .15) !important;
            }

            .custom-tabs li a.active {
                background-color: #f3941e !important;
                color: #ffffff !important;
                border-radius: 0;
                font-family: var(--body-font) !important;
            }

            .type-number-login {
                padding-left: 50px !important;
            }

            /* #phone {
                padding-left: 50px !important;
            } */

            .user-details-title {
                font-family: "Poppins", sans-serif;
            }

            .login-form-wrap .single-input-wrap input {
                padding: 0px 18px !important;
            }

            .custom-tabs li a {
                color: #071c55;
                background: #f8f8f8;
                font-family: var(--body-font) !important;
                border: 0 !important;
                position: relative;
                border-radius: 0 !important;
            }

            .iti__country-name {
                color: black;
            }

            .box-account {
                color: #fff;
                padding: 20px;
                display: none;
                margin-top: 20px;
            }

            .agent_input label {
                color: #071c55 !important;
            }

            .user-design .thumb {
                height: 100%;
                min-height: 900px;
                background-image: url("assets/img/loginleftimg.jpg");
                background-size: 100% 100%;
            }

            .hide-otp-personal {
                display: none;
            }

            .hide-otp-company {
                display: none;
            }

            .hide-email-otp-company {
                display: none;
            }

            .hide-otp-user {
                display: none;
            }

            .hide-email-otp-user {
                display: none;
            }

            .hide-otp-login {
                display: none;
            }

            .hide-email-otp-personal {
                display: none;
            }

            .user-account-btn .btn-yellow {
                border-radius: 31px !important;
                border-top-left-radius: 0 !important;
                border-bottom-left-radius: 0 !important;
            }

            .login-form-wrap h4 {
                font-family: var(--heading-font) !important;
            }
            .topmargin{
                margin-top: 100px !important;
            }
            .mt-3, .my-3 {
                margin-top: -0.5rem!important;
                margin-bottom: -1rem!important;
            }
            /* Chrome, Safari, Edge, Opera */
            input::-webkit-outer-spin-button,
            input::-webkit-inner-spin-button {
              -webkit-appearance: none;
              margin: 0;
            }

            /* Firefox */
            input[type=number] {
              -moz-appearance: textfield;
            }

        </style>
    @endpush
@section('content')
@if(Auth::guard('webagent')->user() || Auth::guard('webcustomer')->user() || Auth::guard('webmember')->user())
<script>window.location = "{{route('home')}}";</script>
@endif
<!--Start Login And Sign Up Area-->
<div class="signUp-popup" id="signUp-popup">
    <div class="login-register-popup-wrap">
        <div class="row no-gutters">
            <div class="col-lg-6 user-design">
                <div class="thumb">
                    <!-- <img src="assets/img/loginleftimg.jpg" class="w-100 h-100" alt="img"> -->
                </div>
            </div>
            <div class="col-lg-6 topmargin">
                <!-- Tab panes -->
                <div class="tab-content">
                    <div id="Login" class="container tab-pane active">
                        <form class="login-form-wrap my-5" id="Loginform" action="{{route('forgotpassword')}}"
                            method="post">
                            @csrf
                            <h4 class="text-center">Forgot Password</h4>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="single-input-wrap style-two">
                                        <div class="input-group">
                                            <select class="custom-select" name="type" id="type" required
                                                data-bv-notempty="true"
                                                data-bv-notempty-message="Choose user is required">
                                                <option value="">Choose User...</option>
                                                <option value="employee">Employee</option>
                                                <option value="agent">Agent</option>
                                                <option value="member">Member</option>
                                                <option value="customer">User</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 single-input-wrap style-two loginPass">
                                    <input type="email" placeholder="Enter Email" id="email" name="email" required data-bv-notempty="true" data-bv-notempty-message="Please Enter Email">
                                </div>
                            </div>
                            <div class="single-input-wrap style-two">
                                <a class="" href="{{route('login')}}">Back to Login</a>
                            </div>
                            <div class="col-12">
                                <input class="btn btn-yellow mt-3 text-center" type="submit" value="Submit">
                            </div><br>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('script')
    <script src="{{ asset('assets/js/jquery-2.2.4.min.js') }}"></script>
    <script src="{{ asset('assets/js/popper.min.js') }}"></script>
    <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.magnific-popup.js') }}"></script>
    <script src="{{ asset('assets/js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('assets/js/wow.min.js') }}"></script>
    <script src="{{ asset('assets/js/slick.js') }}"></script>
    <script src="{{ asset('assets/js/waypoints.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.counterup.min.js') }}"></script>
    <script src="{{ asset('assets/js/imagesloaded.pkgd.min.js') }}"></script>
    <script src="{{ asset('assets/js/isotope.pkgd.min.js') }}"></script>
    <script src="{{ asset('assets/js/swiper.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.nice-select.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('assets/js/jarallax.min.js') }}"></script>
    <!-- main js -->
    <script type="text/javascript"
        src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js"></script>
    
    <script>
        $(document).ready(function() {
            $('#Loginform').bootstrapValidator({
                fields: {
                    email: {
                        message: 'E-mail is required',
                        validators: {
                            remote: {
                                type: 'POST',
                                url: "{{route('email.check')}}",
                                dataType: 'json',
                                data: function(validator) {
                                    // console.log(validator.getFieldElements('contact').val());
                                    return {
                                        email: validator.getFieldElements('email').val(),
                                        type: validator.getFieldElements('type').val(),
                                        _token: "{{ csrf_token() }}",
                                    };
                                },
                                message: 'E-mail is not registered !'
                            }
                        }
                    }
                }
            });
        });


        $('select').on('change', function() {
         // alert("change");
          $('#email').val("").change();
        });
    </script>
@endpush
