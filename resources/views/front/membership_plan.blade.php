@extends('front.layouts.master')
@section('title','Membership Plan')
@push('style')
<style type="text/css">
.description {
margin-top: 45px;
}
.description h3.title {
margin-top: 10px !important;
}
</style>
<style>
    .client-slider-two .swiper-slide-active .client-slider-item {
            background-color: #000c2d;
            border-radius: 35px;
            margin-left: 0px;
            margin-right: 0px;
        }

        .details .single-intro .intro-title .intro-count {
            font-size: 50px;
            font-weight: 700;
        }

        .single-intro {
            box-shadow: rgb(17 12 46 / 15%) 0px 5px 30px 0px;
            border-radius: 35px;
            padding: 30px 10px;
        }

        .slider-box {
            width: 30%;
            padding: 10px;
            cursor: pointer;
            transform: scale(0.9);
            border-radius: 35px;
            position: relative;
            height: 310px;
        }

        .single-intro .intro-title .intro-cat {
            margin-left: -12px;
            margin-top: 4px;
        }

        .slider-box span {
            font-weight: 500;
            color: #071c55;
        }

        .box-slider-position {
            position: relative;
            overflow: hidden;
        }

        .slider-box-active p {
            color: lightgray;
        }

        .slider-box-active span {
            color: white;
            font-size: 20px;
        }

        .slider-box-active {
            background-color: #000c2d;
            transform: scale(1);
            padding: 5px;
            box-shadow: rgb(17 12 46 / 15%) 0px 5px 30px 0px;
            width: 40%;
            /* z-index: 999; */
            height: 340px;
        }

        .slider-box-active .single-intro .intro-title .intro-count {
            font-size: 50px;
            color: #f3941e;
        }

        .single-intro .intro-title .intro-count {
            font-size: 30px;
        }

        .single-intro .intro-title .intro-cat {
            background: transparent;
            font-family: var(--body-font);
        }

        .slider-box-active .single-intro .intro-title a {
            color: white !important;
        }

        .slider-box .btn {
            font-size: 12px;
            height: 40px;
            line-height: 40px;
            padding: 0 22px;
        }

        .slider-box-active .btn {
            font-size: 14px;
            height: 50px;
            line-height: 50px;
            padding: 0 22px;
        }

        .margin-space {
            margin-top: 100px;
            margin-bottom: 100px;
        }

        .single-intro p {
            padding-top: 10px !important;
        }

        .section-title h2 {
            color: #f3941e !important;
        }

        .section-title span {
            color: #071c55 !important;
        }

        .section-title p {
            color: #666 !important;
        }

        #ui-datepicker-div {
            display: none;
        }

        .mg-bottom-92 {
            margin-bottom: -30px;
        }

        .breadcrumb-area {
            padding: 100px 0 20px;
        }

        @media only screen and (max-width: 1680px) {
            .container-bg {
                padding: 31px 0 31px 0;
                /* margin: 0 30px; */
            }
        }

        @media only screen and (max-width: 1280px) {
            .mg-top--70 {
                margin-top: 0px !important;
            }
        }

        @media only screen and (min-width: 992px) {

            .slider-box:nth-child(1).slider-box-active+.slider-box {
                left: -40%;
            }

            .slider-box:nth-child(1).slider-box-active {
                left: 30%;
            }

            .slider-box:nth-child(3).slider-box-active {
                left: -30%;
            }

            .slider-box.alignment {
                left: 40%;
            }

            .slider-box-active {
                margin-bottom: 30px;
            }  

            .margin-space {
                margin-top: 100px;
                margin-bottom: 75px;
            }
        } 
        
        @media only screen and (max-width: 991px) {
            .slider-box {
                width: 100%;
                height: auto;
            }

            .margin-space {
                margin-top: 40px;
                margin-bottom: 0px;
            }

            .slider-box-active {
                background-color: #000c2d;
                transform: scale(0.9) !important;
                padding: 5px;
                box-shadow: rgb(17 12 46 / 15%) 0px 5px 30px 0px;
                width: 100%;
                /* z-index: 999; */
                height: 340px;
            }
        }

        @media only screen and (min-width: 576px) and (max-width:991px) {
            .breadcrumb-area {
                padding: 160px 0 50px;
            }
        }
</style>
@endpush
@section('content')
 <!-- breadcrumb area start -->
<div class="breadcrumb-area jarallax">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-inner">
                    <div class="row justify-content-center">
                        <div class="col-xl-6 col-lg-8">
                            <div class="section-title mb-2 text-center style-two">
                                <h1 class="page-title">Membership&nbsp;<span>Plans</span></h1>
                                <h3 style="color: white;">Choose a Plan that works best for you</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@if(Auth::guard('webagent')->user() || Auth::guard('webmember')->user() || Auth::guard('webcustomer')->user())
@include('front.layouts.newmenu')
@endif
<!-- breadcrumb area End -->
<!--Start Membership Plan Slider Area-->
<!--pd-top-108 pd-bottom-120-->
<div class="container margin-space">
       
        <div class="row box-slider-position">
            
           @if (isset($silver))
            <div class="slider-box child1">
                <div class="single-intro wow">
                    <h4 class="intro-title text-center">
                        <span class="intro-count">Silver</span>
                        <a class="intro-cat" href="#">Plan</a>
                    </h4>
                    <p class="content s-animate-3 border border-top border-left-0 border-right-0 border-bottom-0 "></p>
                    <div class="row">
                        @foreach ($silver as $g)
                            <div class="col-6 text-center">
                                <p>{{$g->years}} Years</p>
                                <span>Rs.{{$g->fixed_amt}}</span>
                            </div>
                        @endforeach
                    </div> 

                    <div class="text-center mt-4">
                        @if(Auth::guard('webagent')->user() || Auth::guard('webcustomer')->user() || Auth::guard('webmember')->user())
                            <a class="btn btn-yellow rounded" href="{{url('/membership-info')}}/silver">Know More</a>
                        @else
                            <a class="btn btn-yellow rounded" href="{{url('/login')}}">Know More</a>
                        @endif
                    </div>
                </div>
            </div>
            @endif
            @if (isset($gold))
            <div class="slider-box child2 slider-box-active">
                <div class="single-intro wow">
                    <h4 class="intro-title text-center">
                        <span class="intro-count">Gold</span>
                        <a class="intro-cat" href="#">Plan</a>
                    </h4>
                    <p class="content s-animate-3 border border-top border-left-0 border-right-0 border-bottom-0 "></p>
                    <div class="row">
                        @foreach ($gold as $g)
                            <div class="col-6 text-center">
                                <p>{{$g->years}} Years</p>
                                <span>Rs.{{$g->fixed_amt}}</span>
                            </div>
                        @endforeach
                    </div>
                    <div class="text-center mt-4">
                        @if(Auth::guard('webagent')->user() || Auth::guard('webcustomer')->user() || Auth::guard('webmember')->user())
                            <a class="btn btn-yellow rounded" href="{{url('/membership-info')}}/gold">Know More</a>
                        @else
                            <a class="btn btn-yellow rounded" href="{{url('/login')}}">Know More</a>
                        @endif
                    </div>
                </div>
            </div>
            @endif
            @if (isset($platinum))
            <div class="slider-box child3">
                <div class="single-intro wow">
                    <h4 class="intro-title text-center">
                        <span class="intro-count">Platinum</span>
                        <a class="intro-cat" href="#">Plan</a>
                    </h4>
                     <p class="content s-animate-3 border border-top border-left-0 border-right-0 border-bottom-0 "></p>
                   <!--  <p class="content s-animate-3 border border-top border-left-0 border-right-0 border-bottom-0 ">
                        Navagio Beach, or Shipwreck Beach, is an
                        exposed cove, sometimes
                        referred to as.</p> -->
                    <div class="row">
                        @foreach ($platinum as $g)
                            <div class="col-6 text-center">
                                <p>{{$g->years}} Years</p>
                                <span>Rs.{{$g->fixed_amt}}</span>
                            </div>
                        @endforeach
                    </div> 

                    <div class="text-center mt-4">
                        @if(Auth::guard('webagent')->user() || Auth::guard('webcustomer')->user() || Auth::guard('webmember')->user())
                            <a class="btn btn-yellow rounded" href="{{url('/membership-info')}}/platinum">Know More</a>
                        @else
                            <a class="btn btn-yellow rounded" href="{{url('/login')}}">Know More</a>
                        @endif
                    </div>
                </div>
            </div>
            @endif
        </div>
    </div> 

<!--End Membership Plan Slider Area-->
@endsection


@push('script') 
<script> 
        $(document).ready(function () { 
            var selector = '.slider-box';

            $(selector).on('click', function () {
                $(selector).removeClass('slider-box-active');
                $(this).addClass('slider-box-active');
            });
        });

        $(".child3").click(function () { 
            $(".child2").addClass("alignment");
        });
        $(".child1, .child2").click(function () {
            $(".child2").removeClass("alignment");
        });
    </script> 
@endpush