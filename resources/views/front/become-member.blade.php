@extends('front.layouts.master')
@section('title','Membership Registration')
@section('content')

<!-- breadcrumb area start -->
<div class="breadcrumb-area jarallax" style="background-image:url(assets/img/bg/1.png); z-index: 0;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-inner">
                    <div class="row justify-content-center">
                        <div class="col-xl-6 col-lg-8">
                            <div class="section-title mb-2 text-center style-two">
                                <h2 class="title">Become <span>Member</span></h2>
                                <p>Enter member details</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb area End -->
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="tour-details-wrap">
                <div class="package-included-area"> 
                    <div class="package-included-location">
                        <div class="section-title text-lg-center text-left my-3">
                            <!-- <h2 class="title"><label>Member&nbsp;</label>Details</h2> -->
                        </div>
                    </div> 
                    <div class="wizard pt-lg-5 pt-3 ">
                        <div class="wizard-inner">
                            <div class="connecting-line"></div>
                            <ul class="nav nav-tabs text-center setup-panel" role="tablist">
                                <li role="presentation" class="stepwizard-step active">
                                    <a href="#step1" data-toggle="tab" role="tab" aria-controls="step1"
                                        aria-expanded="true"><span class="round-tab"><i>Members</i>1</span></a>
                                </li>
                                <li role="presentation" class="stepwizard-step disabled">
                                    <a href="#step2" data-toggle="tab" role="tab" aria-controls="step2"
                                        aria-expanded="false" disabled="disabled"><span class="round-tab"><i>Details</i>2</span></a>
                                </li>
                                <li role="presentation" class="stepwizard-step disabled">
                                    <a href="#step3" data-toggle="tab" aria-controls="step3" role="tab" disabled="disabled"><span class="round-tab"><i>Security</i>3</span></a>
                                </li> 
                               <!--  <li role="presentation" class="disabled">
                                    <a href="#step4" data-toggle="tab" aria-controls="step4" role="tab"><span class="round-tab"><i>Mendate Subsciption</i>4</span></a>
                                </li>  -->
                                <li role="presentation" class="stepwizard-step disabled">
                                    <a href="#step5" data-toggle="tab" aria-controls="step5" role="tab" disabled="disabled"><span class="round-tab"><i>Payment</i>4</span></a>
                                </li> 
                            </ul>
                        </div>
                        @if ($message = Session::get('admincreation'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button> 
                                <strong>{{ $message }}</strong>
                        </div>
                        @endif
                         <form role="form" id="addMember" class="tp-form-wrap bg-gray tp-form-wrap-one" action="{{ url('store-member') }}" method="POST" enctype="multipart/form-data" novalidate="novalidate">
                        @csrf

                        <input type="hidden" id="plan_id" name="plan_id" value="{{$plan->id}}">
                        <input type="hidden" id="membership_free" name="plan_amount" value="{{$plan->mebership_fees}}">
                        <input type="hidden" id="plan_id" name="plan_id" value="{{$plan->id}}">
                        <input type="hidden" id="plan_id" name="plan_id" value="{{$plan->id}}">
                        <div class="tab-content" id="main_form">
                            <div class="tab-pane active" role="tabpanel" id="step1">
                                <h4 class="text-center">Step 1</h4>
                                <div class="location-review-area tp-form-wrap bg-gray tp-form-wrap-one">
                                    <div class="row">
                                        <div class="form-group col-lg-4">
                                            <select id="type" class="form-control" required data-bv-notempty="true" data-bv-notempty-message="Please Select Family Member">
                                                <option value="family" selected>Family - Max 6 Member
                                                </option>
                                                <option value="group">Group - Max 20 Member</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-3 nopadding">
                                            <div class="form-group">
                                                <select name="prefix[]" style="height: 32px;" class="form-control" required data-bv-notempty="true" data-bv-notempty-message="Please Select Prefix">
                                                    <option value="Mr.">Mr.</option>
                                                    <option value="Ms.">Ms.</option>
                                                    <option value="Mrs.">Mrs.</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-3 nopadding">
                                            <div class="form-group">
                                                <input type="text" class="form-control" required id="name" name="name[]" value="@if(isset($user)) {{$user->first_name}} {{$user->last_name}} @endif" placeholder="Name" data-bv-notempty="true" data-bv-notempty-message="Name is required">
                                            </div>
                                        </div>
                                        <div class="col-sm-3 nopadding">
                                            <div class="form-group">
                                                <input type="date" class="form-control" required id="dob" name="dob[]" alue="@if(isset($user)) {{$user->first_name}} {{$user->last_name}} @endif" placeholder="DOB" data-bv-notempty="true" data-bv-notempty-message="DOB is required">
                                                <small>Date of birth</small>
                                            </div>
                                        </div>
                                        <div class="col-sm-3 nopadding">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <input type="number" class="form-control" required id="adharno" name="adharno[]" value="" min="0" maxlength="14" minlength="14" placeholder="Aadhar No." data-bv-notempty="true" data-bv-notempty-message="Aadhar No. is required"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-3 nopadding">
                                            <div class="form-group">
                                                <input type="text" class="form-control" required id="panno" name="panno[]" value="" placeholder="Pan No." data-bv-notempty="true" data-bv-notempty-message="Pan No. is required">
                                            </div>
                                        </div>
                                        <div class="col-sm-3 nopadding">
                                            <div class="form-group">
                                                <input type="text" class="form-control" required id="passport" name="passportno[]" value="" placeholder="Passport No." data-bv-notempty="true" data-bv-notempty-message="Passport No. is required">
                                            </div>
                                        </div>
                                        <div class="col-sm-3 nopadding">
                                            <div class="form-group">
                                                <input type="date" class="form-control" required id="passportdate" name="passportdate[]" value="" placeholder="Passport No." data-bv-notempty="true" data-bv-notempty-message="Passport Exp Date is required">
                                                <small>Passport expiry date</small>
                                            </div>
                                        </div>
                                        <div class="col-sm-3 nopadding">
                                            <div class="form-group">
                                                <input type="text" class="form-control" required id="occupation" name="occupation[]" value="" placeholder="Occupation" data-bv-notempty="true" data-bv-notempty-message="Occupation is required">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-lg-3 col-md-6 icon-plus">
                                            <label>Other Member</label>
                                            <button type="button" onclick="education_fields();"> <i class="fa fa-plus" aria-hidden="true"></i> </button>
                                        </div>
                                    </div></br>
                                    <div id="education_fields"></div>
                                    <div class="row">
                                        <div class="col-lg-12 text-right mt-5">
                                            <button type="button" id="nextbtn1"
                                                class="btn btn-primary nextBtn next-step btn-sm next-btn1">Next
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" role="tabpanel" id="step2">
                                <h4 class="text-center">Step 2</h4>
                                <div class="location-review-area tp-form-wrap bg-gray tp-form-wrap-one">
                                    <!-- <form class="tp-form-wrap bg-gray tp-form-wrap-one"> -->
                                        <div class="row">
                                            <div class="form-group col-lg-6 col-md-6">
                                                <label for="">Email Address</label>
                                                <input type="email" class="form-control" name="email" id="email" placeholder="Enter your Email Address" required data-bv-notempty="true" value="@if(isset($user)) {{$user->company_email}} @endif" data-bv-notempty-message="Please Enter email address">
                                            </div>
                                            <div class="form-group col-lg-6 col-md-6">
                                                <label for="">Approx. Annual Income</label>
                                                <input type="text" class="form-control" name="annual_income" id="annual_income" placeholder="Enter Approx. Annual Income" required data-bv-notempty="true" data-bv-notempty-message="Please Enter approx annual income">
                                            </div>
                                            <div class="form-group col-lg-4 col-md-6">
                                                <label for="">Primary Contact No.</label>
                                                <input type="number" class="form-control" name="primary_contact" id="primary_contact" placeholder="Enter your Primary Contact No." required data-bv-notempty="true" data-bv-notempty-message="Please Enter your primary contact number" value="@if(isset($user)){{$user->contact_details}}@endif">
                                            </div>
                                            <div class="form-group col-lg-4 col-md-6">
                                                <label for="">Secondary Contact No.</label>
                                                <input type="number" class="form-control" name="secondary_contact" id="secondary_contact" placeholder="Enter your Secondary Contact No." required data-bv-notempty="true" data-bv-notempty-message="Please Enter your Secondary contact number">
                                            </div>
                                            <div class="form-group col-lg-4 col-md-6">
                                                <label for="">Whatsapp No.</label>
                                                <input type="number" class="form-control" name="whatsapp_number" id="whatsapp_number" placeholder="Enter your Whatsapp No." required data-bv-notempty="true" data-bv-notempty-message="Please Enter Whatsapp number">
                                            </div> 
                                            
                                            <div class="form-group col-lg-12 col-md-12">
                                                <label for="">Residence address</label>
                                                <textarea name="resi_address" id="resi_address" class="form-control border-0" cols="30" rows="3" placeholder="Enter your Residence Address" required data-bv-notempty="true" data-bv-notempty-message="Please Enter Residence address"></textarea>
                                            </div>
                                            <div class="form-group col-lg-4 col-md-6">
                                                <label for="">City</label>
                                                <input type="text" class="form-control" name="city" id="city"
                                                    placeholder="Enter your City" required data-bv-notempty="true" data-bv-notempty-message="Please Enter your City">
                                            </div>
                                            <div class="form-group col-lg-4 col-md-6">
                                                <label for="">State</label>
                                                <input type="text" class="form-control" name="state" id="state"
                                                    placeholder="Enter your State" required data-bv-notempty="true" data-bv-notempty-message="Please Enter your State">
                                            </div>
                                            <div class="form-group col-lg-4 col-md-6">
                                                <label for="">Pincode</label>
                                                <input type="text" class="form-control" name="pincode" id="pincode"
                                                    placeholder="Enter your Pincode" required data-bv-notempty="true" data-bv-notempty-message="Please Enter your Pincode">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12 text-right">
                                                <button type="button" id="nextbtn2"
                                                    class="btn btn-primary next-step btn-sm next-btn">Next</button>
                                            </div>
                                        </div>
                                </div>
                            </div>
                            
                            <div class="tab-pane" role="tabpanel" id="step3">
                                <h4 class="text-center">Step 3</h4>
                                <div class="location-review-area tp-form-wrap bg-gray tp-form-wrap-one">
                                    <div class="row">
                                        <div class="form-group col-lg-12">
                                            <label for="">Where did you get to know about JagJoyu’s TIP
                                                ?</label>
                                        </div>
                                        <div class="form-group col-lg-2 col-sm-6">
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio"
                                                    name="exampleRadios" id="exampleRadios1" value="facebook" required data-bv-notempty="true" data-bv-notempty-message="Please Select any one">
                                                <label class="form-check-label" for="exampleRadios1">
                                                    Facebook
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group col-lg-2 col-sm-6">
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio"
                                                    name="exampleRadios" id="exampleRadios2" value="instagram" required data-bv-notempty="true" data-bv-notempty-message="Please Select any one">
                                                <label class="form-check-label" for="exampleRadios2">
                                                    Instagram
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group col-lg-2 col-sm-6">
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio"
                                                    name="exampleRadios" id="exampleRadios3" value="linkedin" required data-bv-notempty="true" data-bv-notempty-message="Please Select any one">
                                                <label class="form-check-label" for="exampleRadios3">
                                                    Linkedin
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group col-lg-2 col-sm-6">
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio"
                                                    name="exampleRadios" id="exampleRadios4" value="twitter" required data-bv-notempty="true" data-bv-notempty-message="Please Select any one">
                                                <label class="form-check-label" for="exampleRadios4">
                                                    Twitter
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group col-lg-2 col-sm-6">
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio"
                                                    name="exampleRadios" id="exampleRadios5" value="newspaper" required data-bv-notempty="true" data-bv-notempty-message="Please Select any one">
                                                <label class="form-check-label" for="exampleRadios5">
                                                    Newspaper
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group col-lg-2 col-sm-6">
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio"
                                                    name="exampleRadios" id="exampleRadios6" value="email" required data-bv-notempty="true" data-bv-notempty-message="Please Select any one">
                                                <label class="form-check-label" for="exampleRadios6">
                                                    Email
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group col-lg-2 col-sm-6">
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio"
                                                    name="exampleRadios" id="exampleRadios7" value="member" required data-bv-notempty="true" data-bv-notempty-message="Please Select any one">
                                                <label class="form-check-label" for="exampleRadios7">
                                                    JagJoyu’s Member
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group col-lg-2 col-sm-6">
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio"
                                                    name="exampleRadios" id="exampleRadios8" value="agentname" required data-bv-notempty="true" data-bv-notempty-message="Please Select any one">
                                                <label class="form-check-label" for="exampleRadios8">
                                                    Agent Name
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group col-lg-2 col-sm-6">
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio"
                                                    name="exampleRadios" id="exampleRadios9" value="employeeref" required data-bv-notempty="true" data-bv-notempty-message="Please Select any one">
                                                <label class="form-check-label" for="exampleRadios9">
                                                    Employee Referral

                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group col-lg-2 col-sm-6">
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio"
                                                    name="exampleRadios" id="exampleRadios10" value="other" required data-bv-notempty="true" data-bv-notempty-message="Please Select any one">
                                                <label class="form-check-label" for="exampleRadios10">
                                                    Other
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row mt-2">
                                        <div class="form-group col-lg-6 col-md-6">
                                            <label for="">Reference ID</label>
                                            <input type="text" class="form-control" name="ref_id" id="ref_id"
                                                placeholder="Enter your Reference ID" required  data-bv-notempty="true" data-bv-notempty-message="Please Enter Reference ID">
                                        </div>
                                        <div class="form-group col-lg-6 col-md-6">
                                            <label for="">Photo to upload</label>
                                            <div class="input-group mb-3">
                                                <div class="custom-file">
                                                    <input type="file" class="custom-file-input" name="photo"
                                                        id="inputGroupFile02" required data-bv-notempty="true" data-bv-notempty-message="Please Select Photo">
                                                    <label class="custom-file-label"
                                                        for="inputGroupFile02">Choose
                                                        file</label>
                                                </div>
                                                <div class="input-group-append">
                                                    <span class="input-group-text" id="">Upload</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group col-lg-6 col-md-6">
                                            <label for="">Password</label>
                                            <input type="password" class="form-control" name="password" id="password"
                                                placeholder="Password" required data-bv-notempty="true" data-bv-notempty-message="Please Enter Password">
                                        </div>
                                        <div class="form-group col-lg-6 col-md-6">
                                            <label for="">Confirm Password</label>
                                            <input type="password" class="form-control" name="confirm_password" id="confirm_password"
                                                placeholder="Confirm Password" required data-bv-notempty="true" data-bv-notempty-message="Please Enter Confirm Password">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 text-right">
                                            <button type="button" id="nextbtn3"
                                                class="btn btn-primary next-step btn-sm next-btn">Next</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- ========== Start Mandate Details form ========== -->

                            <!-- <div class="tab-pane" role="tabpanel" id="step4">
                                <h4 class="text-center">Step 4</h4>
                                <div class="location-review-area tp-form-wrap bg-gray tp-form-wrap-one">
                                    <div class="row">
                                        <div class="form-group col-lg-12">
                                            <label for="">Please fill up this details for Subscription Plan</label>
                                        </div>
                                        <div class="form-group col-lg-4 col-md-4">
                                            <label for="">Select EMI Option</label>
                                            <select id="emi" class="form-control" onchange="generate_token()" required data-bv-notempty="true" name="emi" data-bv-notempty-message="Please Select EMI Type">
                                                <option value="monthly" selected>Monthly EMI</option>
                                                <option value="quarterly">Quarterly EMI</option>
                                                <option value="half-yearly">Half-Yearly EMI</option>
                                                <option value="annual">Annual EMI</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-lg-4 col-md-4">
                                            <label for="">Total Amount</label>
                                            <input type="text" class="form-control" name="total_amount" id="total_amount" placeholder="Enter your Amount" required data-bv-notempty="true" data-bv-notempty-message="Please Enter email address">
                                        </div>
                                        <div class="form-group col-lg-4 col-md-4">
                                            <label for="">Dabit Start Date</label>
                                            <input type="text" disabled class="form-control" name="dabit_start_date" id="dabit_start_date" required data-bv-notempty="true" data-bv-notempty-message="Please Select date">
                                        </div>
                                    </div>
                                    
                                    <div class="row">
                                        <div class="col-12 text-right">
                                            <button type="button" id="nextbtn3"
                                                class="btn btn-primary next-step btn-sm next-btn">Next</button>
                                        </div>
                                    </div>
                                </div>
                            </div> -->

                            <!-- =============End Mendate Details form ========== -->


                            <!-- ============== Payment form =============== -->
                            <div class="tab-pane" role="tabpanel" id="step5">
                                <h4 class="text-center">Step 5</h4>
                                <div class="location-review-area tp-form-wrap bg-gray tp-form-wrap-one">
                                    <!-- <form class="tp-form-wrap bg-gray tp-form-wrap-one"> -->
                                    <div class="row">
                                        <div class="col-md-3">
                                        </div>
                                        <div class="form-group col-lg-6 col-md-6">
                                            <label for="">Membership Free</label>
                                            <input type="text" disabled="" class="form-control" name="plan_amount" id="plan_amount" value="{{$plan->mebership_fees}}">
                                        
                                            <label for="">Description</label>
                                            <textarea name="description" id="description" class="form-control border-0" cols="40" rows="4" placeholder="Enter your Residence Address"></textarea>
                                        </div>
                                         <div class="col-md-3">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 text-right">
                                            <button type="submit"
                                                class="btn btn-primary next-step btn-sm next-btn">Pay Now</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- =============== Payment form ===============-->

                        </div>
                        </div>                      
                    </div> 
                    </form>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@push('style')

<!-- <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css"> -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css"/>
<style type="text/css">
    @import url('https://fonts.googleapis.com/css?family=Roboto');

/*------------------------*/
input:focus,
button:focus,
.form-control:focus{
    outline: none;
    box-shadow: none;
}
/*.form-control:disabled, .form-control[readonly]{
    background-color: #fff;
}*/
/*----------step-wizard------------*/
.d-flex{
    display: flex;
}
.justify-content-center{
    justify-content: center;
}
.align-items-center{
    align-items: center;
}

/*---------signup-step-------------*/
.bg-color{
    background-color: #333;
}
.signup-step-container{
    padding: 150px 0px;
    padding-bottom: 60px;
}
    .wizard .nav-tabs {
        position: relative;
        margin-bottom: 0;
        border-bottom-color: transparent;
    }

    .wizard > div.wizard-inner {
    position: relative;
    margin-bottom: 50px;
    text-align: center;
    }

.connecting-line {
    height: 2px;
    background: #f3941e;
    position: absolute;
    width: 75%;
    margin: 0 auto;
    left: 0;
    right: 0;
    top: 15px;
    z-index: 1;
}

.wizard .nav-tabs > li.active > a, .wizard .nav-tabs > li.active > a:hover, .wizard .nav-tabs > li.active > a:focus {
    color: #555555;
    cursor: default;
    border: 0;
    border-bottom-color: transparent;
}

span.round-tab {
    width: 30px;
    height: 30px;
    line-height: 30px;
    display: inline-block;
    border-radius: 50%;
    background: #fff;
    z-index: 2;
    position: absolute;
    left: 0;
    text-align: center;
    font-size: 16px;
    color: #0e214b;
    font-weight: 500;
    border: 1px solid #ddd;
}
span.round-tab i{
    color:#555555;
}
.wizard li.active span.round-tab {
    background: #f3941e !important;
    color: #fff;
    border-color: #f3941e !important;
}
.wizard li.active span.round-tab i{
    color: #5bc0de;
}
.wizard .nav-tabs > li.active > a i{
    color: #f3941e;
    font-size: 15px !important;
}

.wizard .nav-tabs > li {
    width: 25%;
}

.wizard li:after {
    content: " ";
    position: absolute;
    left: 46%;
    opacity: 0;
    margin: 0 auto;
    bottom: 0px;
    border: 5px solid transparent;
    border-bottom-color: red;
    transition: 0.1s ease-in-out;
}



.wizard .nav-tabs > li a {
    width: 30px;
    height: 30px;
    margin: 20px auto;
    border-radius: 100%;
    padding: 0;
    background-color: transparent;
    position: relative;
    top: 0;
}
.wizard .nav-tabs > li a i{
    position: absolute;
    top: -15px;
    font-style: normal;
    font-weight: 400;
    white-space: nowrap;
    left: 50%;
    transform: translate(-50%, -50%);
    font-size: 12px;
    font-weight: 700;
    color: #000;
}

    .wizard .nav-tabs > li a:hover {
        background: transparent;
    }

.wizard .tab-pane {
    position: relative;
    padding-top: 20px;
}

.next-btn:hover {
    background-color: #f3941e;
    color: white;
    }


.wizard h3 {
    margin-top: 0;
}
/*.prev-step,
.next-step{
    font-size: 13px;
    padding: 8px 24px;
    border: none;
    border-radius: 4px;
    margin-top: 30px;
}*/
/*.next-step{
    background-color: #f3941e;
}*/
.skip-btn{
    background-color: #cec12d;
}
.step-head{
    font-size: 20px;
    text-align: center;
    font-weight: 500;
    margin-bottom: 20px;
}
.term-check{
    font-size: 14px;
    font-weight: 400;
}
.custom-file {
    position: relative;
    display: inline-block;
    width: 100%;
    height: 40px;
    margin-bottom: 0;
}
.custom-file-input {
    position: relative;
    z-index: 2;
    width: 100%;
    height: 40px;
    margin: 0;
    opacity: 0;
}
.custom-file-label {
    position: absolute;
    top: 0;
    right: 0;
    left: 0;
    z-index: 1;
    height: 40px;
    padding: .375rem .75rem;
    font-weight: 400;
    line-height: 2;
    color: #495057;
    background-color: #fff;
    border: 1px solid #ced4da;
    border-radius: .25rem;
}
.custom-file-label::after {
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    z-index: 3;
    display: block;
    height: 38px;
    padding: .375rem .75rem;
    line-height: 2;
    color: #495057;
    content: "Browse";
    background-color: #e9ecef;
    border-left: inherit;
    border-radius: 0 .25rem .25rem 0;
}
.footer-link{
    margin-top: 30px;
}
.all-info-container{

}
.list-content{
    margin-bottom: 10px;
}
.list-content a{
    padding: 10px 15px;
    width: 100%;
    display: inline-block;
    background-color: #f5f5f5;
    position: relative;
    color: #565656;
    font-weight: 400;
    border-radius: 4px;
}
.list-content a[aria-expanded="true"] i{
    transform: rotate(180deg);
}
.list-content a i{
    text-align: right;
    position: absolute;
    top: 15px;
    right: 10px;
    transition: 0.5s;
}
/*.form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
    background-color: #fdfdfd;
}*/
.list-box{
    padding: 10px;
}
.signup-logo-header .logo_area{
    width: 200px;
}
.signup-logo-header .nav > li{
    padding: 0;
}
.signup-logo-header .header-flex{
    display: flex;
    justify-content: center;
    align-items: center;
}
.list-inline li{
    display: inline-block;
}
.pull-right{
    float: right;
}

.icon-plus {
    position: relative;
    }

    .icon-plus button {
    position: absolute;
    left: 16px;
    top: 36px;
    height: 38px;
    width: 35px;
    background-color: #071c55;
    border: none;
    color: white;
    border-radius: 4px;
    transition: 0.7s;
    }

    .icon-plus button:hover {
    background-color: #f3941e;
    color: white;
    }

    .icon-minus
    {
    width: 30px;
    height: 37px;
    border-radius: 1px 5px 5px 0px;
    }

    .stepwizard-step button[disabled] {
        opacity: 1 !important;
        filter: alpha(opacity=100) !important;
    }

/*-----------custom-checkbox-----------*/
/*----------Custom-Checkbox---------*/
input[type="checkbox"]{
    position: relative;
    display: inline-block;
    margin-right: 5px;
}
input[type="checkbox"]::before,
input[type="checkbox"]::after {
    position: absolute;
    content: "";
    display: inline-block;   
}
input[type="checkbox"]::before{
    height: 16px;
    width: 16px;
    border: 1px solid #999;
    left: 0px;
    top: 0px;
    background-color: #fff;
    border-radius: 2px;
}
input[type="checkbox"]::after{
    height: 5px;
    width: 9px;
    left: 4px;
    top: 4px;
}
input[type="checkbox"]:checked::after{
    content: "";
    border-left: 1px solid #fff;
    border-bottom: 1px solid #fff;
    transform: rotate(-45deg);
}
input[type="checkbox"]:checked::before{
    background-color: #18ba60;
    border-color: #18ba60;
}

@media (max-width: 767px){
    .sign-content h3{
        font-size: 40px;
    }
    .wizard .nav-tabs > li a i{
        display: none;
    }
    .signup-logo-header .navbar-toggle{
        margin: 0;
        margin-top: 8px;
    }
    .signup-logo-header .logo_area{
        margin-top: 0;
    }
    .signup-logo-header .header-flex{
        display: block;
    }
}

</style>
@endpush
@push('script')
<!-- <script src="//code.jquery.com/jquery-1.10.2.min.js"></script>
 <script src="https://code.jquery.com/jquery-3.4.0.slim.min.js"></script> 
<script src="{{url('/front')}}/assets/js/sha.js"></script>
<script src="https://www.paynimo.com/Paynimocheckout/client/lib/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript" src="https://www.paynimo.com/Paynimocheckout/server/lib/checkout.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script> -->
<!-- <script src="//code.jquery.com/jquery-1.11.1.min.js"></script> -->
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js"></script>
<script type="text/javascript">

$(document).ready(function () {
    $('.nav-tabs > li a[title]').tooltip();
    
    //Wizard
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {

        var target = $(e.target);
    
        if (target.parent().hasClass('disabled')) {
            return false;
        }
    });

    $(".next-step").click(function (e) {

        var active = $('.wizard .nav-tabs li.active');
        //active.next().removeClass('disabled');
        nextTab(active);

    });
    $(".prev-step").click(function (e) {

        var active = $('.wizard .nav-tabs li.active');
        prevTab(active);

    });
});

function nextTab(elem) {
    $(elem).next().find('a[data-toggle="tab"]').click();
}
function prevTab(elem) {
    $(elem).prev().find('a[data-toggle="tab"]').click();
}

$('.nav-tabs').on('click', 'li', function() {
    $('.nav-tabs li.active').removeClass('active');
    $(this).addClass('active');
});

var room = 1;
var $type = "Family";
var $max = 6;
$(document).ready(function () {
    $('#addMember').bootstrapValidator({});
});

$(document).on('change','#type',function(){
    // alert('true');
    var val = $(this).val();
    if(val=="group")
    {
        $type = "Group";
        $max = 20;
    }
    else
    {
        $type = "Family";
        $max = 6;
    }
});

$(document).ready( function() {
    var debit_start_date = $("#dabit_start_date").datepicker('setDate', new Date());
});

/*function generate_token(){
    var emi = $('#emi').val();

    var start_debit_date = $('#dabit_start_date').val();
  
    var date = new Date(start_debit_date);
    var newdate = new Date(date);

    var dd = newdate.getDate();
    var mm = newdate.getMonth() + 1;
    var y = newdate.getFullYear() + {{$plan->years}};

    var someFormattedDate = mm + '/' + dd + '/' + y;
    
    if(emi  == 'monthly'){
        var totalamount = {{$plan->monthly_emi}};
    }else if(emi  == 'quarterly'){
        var totalamount = {{$plan->quarterly_emi}};
    }else if(emi  == 'half-yearly'){
        var totalamount = {{$plan->half_yearly_emi}};
    }else{
        var totalamount = {{$plan->annual_emi}};
    }

    $('#total_amount').val(totalamount);

     let consumer_Id = Math.random().toString(36).substring(5);

    var merchantId = 'T658345';
    var txnId = 'c'+Date.now();
    var totalamount = totalamount;
    var consumerId = consumer_Id;
    var consumerMobileNo = $("#primary_contact").val();
    var consumerEmailId = $("#email").val();
    var debitStartDate = start_debit_date;
    var debitEndDate = someFormattedDate;
    var maxAmount = {{$plan->fixed_amt}};
    var amountType = 'M';
    var frequency = 'ADHO';
    var salt = '9114050656MYYEJX';

    //var data = "Val1@#|val2$%|val3(*|";
    var token = `${merchantId}|${txnId}|${totalamount}||${consumerId}|${consumerMobileNo}|${consumerEmailId}|${debitStartDate}|${debitEndDate}|${maxAmount}|${amountType}|${frequency}|||||${salt}`; 

    var jsSha = new jsSHA(token);
    var hash = jsSha.getHash("SHA-512", "HEX");

    function handleResponse(res) {

        if (typeof res != 'undefined' && typeof res.paymentMethod != 'undefined' && typeof res.paymentMethod.paymentTransaction != 'undefined' && typeof res.paymentMethod.paymentTransaction.statusCode != 'undefined' && res.paymentMethod.paymentTransaction.statusCode == '0300') {
            // success block
        } else if (typeof res != 'undefined' && typeof res.paymentMethod != 'undefined' && typeof res.paymentMethod.paymentTransaction != 'undefined' && typeof res.paymentMethod.paymentTransaction.statusCode != 'undefined' && res.paymentMethod.paymentTransaction.statusCode == '0398') {
            // initiated block
        } else {
            // error block
        }
    };

    $(document).off('click', '#btnSubmit').on('click', '#btnSubmit', function(e) {
        e.preventDefault();

        var configJson = {
            'tarCall': false,
            'features': {
                'showPGResponseMsg': true,
                'enableAbortResponse': true,
                'enableNewWindowFlow': true,    //for hybrid applications please disable this by passing false
                'enableExpressPay':true,
                'siDetailsAtMerchantEnd':true,
                'enableSI':true
            },
            'consumerData': {
                'deviceId': 'WEBSH2',   //possible values 'WEBSH1', 'WEBSH2' and 'WEBMD5'
                'token': token,
                'returnUrl': 'https://www.tekprocess.co.in/MerchantIntegrationClient/MerchantResponsePage.jsp',    //merchant response page URL
                'responseHandler': handleResponse,
                'paymentMode': 'netBanking',
                'merchantLogoUrl': 'https://www.paynimo.com/CompanyDocs/company-logo-md.png',  //provided merchant logo will be displayed
                'merchantId': merchantId,
                'currency': 'INR',
                'consumerId': consumerId,  //Your unique consumer identifier to register a eMandate/eNACH
                'consumerMobileNo': consumerMobileNo,
                'consumerEmailId': consumerEmailId,
                'txnId': txnId,   //Unique merchant transaction ID
                'items': [{
                    'itemId': 'test',
                    'amount': totalamount,
                    'comAmt': '0'
                }],
                'customStyle': {
                    'PRIMARY_COLOR_CODE': '#3977b7',   //merchant primary color code
                    'SECONDARY_COLOR_CODE': '#FFFFFF',   //provide merchant's suitable color code
                    'BUTTON_COLOR_CODE_1': '#1969bb',   //merchant's button background color code
                    'BUTTON_COLOR_CODE_2': '#FFFFFF'   //provide merchant's suitable color code for button text
                },
                //'accountNo': '346701501029',    //Pass this if accountNo is captured at merchant side for eMandate/eNACH
                //'accountHolderName': 'Name',  //Pass this if accountHolderName is captured at merchant side for ICICI eMandate & eNACH registration this is mandatory field, if not passed from merchant Customer need to enter in Checkout UI.
                //'ifscCode': 'ICIC0000001',        //Pass this if ifscCode is captured at merchant side.
                'accountType': 'Saving',  //Required for eNACH registration this is mandatory field
                'debitStartDate': debitStartDate,
                'debitEndDate': debitEndDate,
                'maxAmount': maxAmount,
                'amountType': amountType,
                'frequency': frequency //  Available options DAIL, WEEK, MNTH, QURT, MIAN, YEAR, BIMN and ADHO
            }
        };

        $.pnCheckout(configJson);
        if(configJson.features.enableNewWindowFlow){
            
            pnCheckoutShared.openNewWindow();
            
        }
    });
   
}*/

function education_fields() {
    
    if(room>=$max)
    {
        alert($type+' has allowed maximum '+$max+' member');
        return
    }
    room++;
    var objTo = document.getElementById('education_fields')
    var divtest = document.createElement("div");
    divtest.setAttribute("class", "form-group removeclass"+room);
    var rdiv = 'removeclass'+room;
    divtest.innerHTML = `<div class="row">
                            <div class="col-sm-3 nopadding">
                                <div class="form-group">
                                    <select name="prefix[]" style="height: 32px;" class="form-control" required data-bv-notempty="true" data-bv-notempty-message="Please Select Prefix">
                                        <option value="Mr.">Mr.</option>
                                        <option value="Ms.">Ms.</option>
                                        <option value="Mrs.">Mrs.</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3 nopadding">
                                <div class="form-group">
                                    <input type="text" class="form-control" required id="name" name="name[]" value="" placeholder="Name" data-bv-notempty="true" data-bv-notempty-message="Name is required">
                                </div>
                            </div>
                            <div class="col-sm-3 nopadding">
                                <div class="form-group">
                                    <input type="date" class="form-control" required id="dob" name="dob[]" value="" placeholder="DOB" data-bv-notempty="true" data-bv-notempty-message="DOB is required">
                                    <small>Date of birth</small>
                                </div>
                            </div>
                            <div class="col-sm-3 nopadding">
                                <div class="form-group">
                                    <input type="number" class="form-control" required id="adharno" name="adharno[]" value="" min="0" maxlength="14" minlength="14" placeholder="Aadhar No." data-bv-notempty="true" data-bv-notempty-message="Aadhar No. is required"/>
                                </div>
                            </div>
                            <div class="col-sm-3 nopadding">
                                <div class="form-group">
                                    <input type="text" class="form-control" required id="panno" name="panno[]" value="" placeholder="Pan No." data-bv-notempty="true" data-bv-notempty-message="Pan No. is required">
                                </div>
                            </div>
                            <div class="col-sm-3 nopadding">
                                <div class="form-group">
                                    <input type="text" class="form-control" required id="passport" name="passportno[]" value="" placeholder="Passport No." data-bv-notempty="true" data-bv-notempty-message="Passport No. is required">
                                </div>
                            </div>
                            <div class="col-sm-3 nopadding">
                                <div class="form-group">
                                    <input type="date" class="form-control" required id="passportdate" name="passportdate[]" value="" placeholder="Passport No." data-bv-notempty="true" data-bv-notempty-message="Passport Exp Date is required">
                                    <small>Passport expiry date</small>
                                </div>
                            </div>
                            <div class="col-sm-3 nopadding">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" class="form-control" required id="occupation" name="occupation[]" value="" placeholder="Occupation" data-bv-notempty="true" data-bv-notempty-message="Occupation is required">
                                        <div class="input-group-btn">
                                            <button class="btn-danger icon-minus" type="button" onclick="remove_education_fields(`+ room +`);"><i class="fa fa-minus" aria-hidden="true"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>`;
    
    objTo.appendChild(divtest);
    $('#addMember').bootstrapValidator('addField', 'prefix[]');
    $('#addMember').bootstrapValidator('addField', 'name[]');
    $('#addMember').bootstrapValidator('addField', 'dob[]');
    $('#addMember').bootstrapValidator('addField', 'adharno[]');
    $('#addMember').bootstrapValidator('addField', 'panno[]');
    $('#addMember').bootstrapValidator('addField', 'passportno[]');
    $('#addMember').bootstrapValidator('addField', 'passportdate[]');
    $('#addMember').bootstrapValidator('addField', 'occupation[]');
}
function remove_education_fields(rid) {
    room--;
   $('.removeclass'+rid).remove();
}

    </script>
    @endpush