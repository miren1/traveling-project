@extends('front.layouts.master')
@section('title','Careers')
@push('style')
<style>
    .form-check-inline {
        width: 100%;
    }

    .form-check .Box_01 {
        width: 79%;
    }

    .form-check .Box_02 {
        width: 30%;
        color: blue;
    }
    .swiper-slide .client-slider-item:hover    .form-check .Box_02{
       color: #f3941e;
    }

    .swiper-slide .client-slider-item .plan_list .font-weight {
        font-weight: 400;
        font-size: 11px;
        text-align: right;
        color: #8a8c94;
    }

    .swiper-slide .client-slider-item:hover {
        background-color: #071c55 !important;
        color: white !important;
        transition: all 1.2s;
    }

    .swiper-slide .client-slider-item {
        -ms-transform: scale(0.9);
        /* IE 9 */
        -webkit-transform: scale(0.9);
        /* Safari 3-8 */
        transform: scale(0.9);
        width: 100%;
    }

    .swiper-slide .client-slider-item:hover .plan_name {
        color: white !important;
    }

    .swiper-slide .client-slider-item:hover .font-weight {
        color: lightgray !important;
    }

    .swiper-slide .client-slider-item:hover .details .plan_heading .tp-price-meta p {
        color: white !important;
    }



    .member_btn .btn {
        font-size: 14px;
        height: 40px;
        line-height: 40px;
        padding: 0 12px;
    }

    .swiper-slide .client-slider-item {
        box-shadow: rgb(100 100 111 / 20%) 0px 7px 29px 0px;
        padding: 10px;
        border-radius: 25px;
        transition: transform 1.2s;
    }

    .swiper-slide .client-slider-item .plan_list .plan_name {
        font-size: 13px;
        color: #071c55;
        font-weight: 600;
    }

    .swiper-slide .client-slider-item .plan_list span {
        width: 50%;
    }

    .section-title h2 {
        color: #f3941e !important;
    }

    .section-title span {
        color: #071c55 !important;
    }

    .section-title p {
        color: #666 !important;
    }

    #ui-datepicker-div {
        display: none;
    }

    .mg-bottom-92 {
        margin-bottom: -30px;
    }

    .breadcrumb-area {
        padding: 130px 0 50px;
    }

    .tp-price-meta h2 {
        font-size: 20px;
        font-weight: 500;
    }

    .single-intro .intro-title .intro-count {
        font-size: 35px;
    }

    .single-intro .intro-title .intro-cat {
        margin-left: -7px;
        margin-top: 3px;
        font-size: 14px;
    }

    @media only screen and (max-width: 1680px) {
        .container-bg {
            padding: 31px 0 31px 0;
            /* margin: 0 30px; */
        }
    }

    @media only screen and (max-width: 1280px) {
        .mg-top--70 {
            margin-top: 0px !important;
        }
    }

    @media only screen and (max-width: 767px) {
        .client-slider-two .swiper-slide-active .client-slider-item .plan_list span {
            display: block !important;
            float: none !important;
        }

        .client-slider-two .swiper-slide-active .client-slider-item .plan_list br {
            display: none !important;
        }

    }

    @media only screen and (min-width:991px) {
        .swiper-slide .client-slider-item:hover {
            -ms-transform: scale(1.0);
            /* IE 9 */
            -webkit-transform: scale(1.0);
            /* Safari 3-8 */
            transform: scale(1.0);
        }
    }

    @media only screen and (min-width: 576px) and (max-width:991px) {
        .breadcrumb-area {
            padding: 160px 0 50px;
        }
    }
</style>
@endpush
@section('content')
    <!-- breadcrumb area start -->
    <div class="breadcrumb-area jarallax" style="background-image: none; z-index: 0;"
        data-jarallax-original-styles="background-image:url(assets/img/bg/1.png);">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb-inner">
                        <div class="row justify-content-center">
                            <div class="col-xl-6 col-lg-8">
                                <div class="section-title mb-2 text-center style-two">
                                    <h2 class="title">Silver&nbsp;<span>Plan</span></h2>
                                    <p>Choose a Plan that works best for you</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- breadcrumb area End -->


    <!--Start Silver Plans Area-->
    <div class="py-lg-5">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-3 col-sm-6 mt-sm-0">
                    <!-- item -->
                    <div class="swiper-slide swiper-slide-active" data-swiper-slide-index="0"
                        style="transition-duration: 0ms; transform: translate3d(0px, 0px, 0px) rotateX(0deg) rotateY(0deg); z-index: 1;">
                        <div class="client-slider-item">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="details">
                                        <div class=" plan_heading single-intro d-flex mb-2">
                                            <h4 class="intro-title">
                                                <span class="intro-count">02</span>
                                                <a class="intro-cat" href="#">Year</a>
                                            </h4>
                                            <div class="tp-price-meta ml-auto">
                                                <p class="my-auto">Price</p>
                                                <h2 class="mt-1 mr-3">120000<small>&#8377;</small></h2>
                                            </div>
                                        </div>

                                        <div class="plan_list">
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="row mx-1">
                                                        <span class="float-left plan_name">Registration Fee</span><span
                                                            class="float-right font-weight">Rs.2000</span>
                                                    </div>
                                                    <div class="row mx-1">
                                                        <span class="float-left plan_name">Monthly EMI</span><span
                                                            class="float-right font-weight">Rs.5000</span>
                                                    </div>
                                                    <div class="row mx-1">
                                                        <span class="float-left plan_name">Free Nights
                                                        </span><span class="float-right font-weight">1</span>
                                                    </div>
                                                    <div class="row mx-1">
                                                        <span class="float-left plan_name">Commission</span><span
                                                            class="float-right font-weight">
                                                            Yes</span>
                                                    </div>
                                                    <div class="row mx-1">
                                                        <span class="float-left plan_name">Flexibility to Travel in any
                                                            season</span><span
                                                            class="float-right font-weight">Yes</span>
                                                    </div>
                                                    <div class="row mx-1">
                                                        <span class="float-left plan_name">Flexibility to choose
                                                            Hotels/Resorts</span><span
                                                            class="float-right font-weight">Yes</span>
                                                    </div>
                                                    <div class="row mx-1">
                                                        <div class="form-check form-check-inline my-3 mr-0">
                                                            <div class="Box_01">
                                                                <input class="form-check-input" type="checkbox"
                                                                    id="inlineCheckbox1" value="option1">
                                                                <label class="form-check-label" for="inlineCheckbox1">I
                                                                    agree the T & C</label>
                                                            </div>
                                                            <div class="Box_02 text-right">
                                                                <a href="#">Click here.</a>
                                                            </div>

                                                        </div>
                                                    </div>

                                                    <div class="text-center mt-1 member_btn">
                                                        <a class="btn btn-yellow rounded" href="#">Become a
                                                            Member</i></a>
                                                    </div>


                                                </div>

                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- item -->
                </div>
                <div class="col-lg-3 col-sm-6 mt-sm-0">
                    <!-- item -->
                    <div class="swiper-slide swiper-slide-active" data-swiper-slide-index="0"
                        style="transition-duration: 0ms; transform: translate3d(0px, 0px, 0px) rotateX(0deg) rotateY(0deg); z-index: 1;">
                        <div class="client-slider-item">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="details">
                                        <div class=" plan_heading single-intro d-flex mb-2">
                                            <h4 class="intro-title">
                                                <span class="intro-count">03</span>
                                                <a class="intro-cat" href="#">Year</a>
                                            </h4>
                                            <div class="tp-price-meta ml-auto">
                                                <p class="my-auto">Price</p>
                                                <h2 class="mt-1 mr-3">120000<small>&#8377;</small></h2>
                                            </div>
                                        </div>

                                        <div class="plan_list">
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="row mx-1">
                                                        <span class="float-left plan_name">Registration Fee</span><span
                                                            class="float-right font-weight">Rs.2000</span>
                                                    </div>
                                                    <div class="row mx-1">
                                                        <span class="float-left plan_name">Monthly EMI</span><span
                                                            class="float-right font-weight">Rs.5000</span>
                                                    </div>
                                                    <div class="row mx-1">
                                                        <span class="float-left plan_name">Free Nights
                                                        </span><span class="float-right font-weight">1</span>
                                                    </div>
                                                    <div class="row mx-1">
                                                        <span class="float-left plan_name">Commission</span><span
                                                            class="float-right font-weight">
                                                            Yes</span>
                                                    </div>
                                                    <div class="row mx-1">
                                                        <span class="float-left plan_name">Flexibility to Travel in any
                                                            season</span><span
                                                            class="float-right font-weight">Yes</span>
                                                    </div>
                                                    <div class="row mx-1">
                                                        <span class="float-left plan_name">Flexibility to choose
                                                            Hotels/Resorts</span><span
                                                            class="float-right font-weight">Yes</span>
                                                    </div>
                                                    <div class="row mx-1">
                                                        <div class="form-check form-check-inline my-3 mr-0">
                                                            <div class="Box_01">
                                                                <input class="form-check-input" type="checkbox"
                                                                    id="inlineCheckbox1" value="option1">
                                                                <label class="form-check-label" for="inlineCheckbox1">I
                                                                    agree the T & C</label>
                                                            </div>
                                                            <div class="Box_02 text-right">
                                                                <a href="#">Click here.</a>
                                                            </div>

                                                        </div>
                                                    </div>

                                                    <div class="text-center mt-1 member_btn">
                                                        <a class="btn btn-yellow rounded" href="#">Become a
                                                            Member</i></a>
                                                    </div>


                                                </div>

                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- item -->
                </div>
                <div class="col-lg-3 col-sm-6 mt-sm-0">
                    <!-- item -->
                    <div class="swiper-slide swiper-slide-active" data-swiper-slide-index="0"
                        style="transition-duration: 0ms; transform: translate3d(0px, 0px, 0px) rotateX(0deg) rotateY(0deg); z-index: 1;">
                        <div class="client-slider-item">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="details">
                                        <div class=" plan_heading single-intro d-flex mb-2">
                                            <h4 class="intro-title">
                                                <span class="intro-count">04</span>
                                                <a class="intro-cat" href="#">Year</a>
                                            </h4>
                                            <div class="tp-price-meta ml-auto">
                                                <p class="my-auto">Price</p>
                                                <h2 class="mt-1 mr-3">120000<small>&#8377;</small></h2>
                                            </div>
                                        </div>

                                        <div class="plan_list">
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="row mx-1">
                                                        <span class="float-left plan_name">Registration Fee</span><span
                                                            class="float-right font-weight">Rs.2000</span>
                                                    </div>
                                                    <div class="row mx-1">
                                                        <span class="float-left plan_name">Monthly EMI</span><span
                                                            class="float-right font-weight">Rs.5000</span>
                                                    </div>
                                                    <div class="row mx-1">
                                                        <span class="float-left plan_name">Free Nights
                                                        </span><span class="float-right font-weight">1</span>
                                                    </div>
                                                    <div class="row mx-1">
                                                        <span class="float-left plan_name">Commission</span><span
                                                            class="float-right font-weight">
                                                            Yes</span>
                                                    </div>
                                                    <div class="row mx-1">
                                                        <span class="float-left plan_name">Flexibility to Travel in any
                                                            season</span><span
                                                            class="float-right font-weight">Yes</span>
                                                    </div>
                                                    <div class="row mx-1">
                                                        <span class="float-left plan_name">Flexibility to choose
                                                            Hotels/Resorts</span><span
                                                            class="float-right font-weight">Yes</span>
                                                    </div>
                                                    <div class="row mx-1">
                                                        <div class="form-check form-check-inline my-3 mr-0">
                                                            <div class="Box_01">
                                                                <input class="form-check-input" type="checkbox"
                                                                    id="inlineCheckbox1" value="option1">
                                                                <label class="form-check-label" for="inlineCheckbox1">I
                                                                    agree the T & C</label>
                                                            </div>
                                                            <div class="Box_02 text-right">
                                                                <a href="#">Click here.</a>
                                                            </div>

                                                        </div>
                                                    </div>

                                                    <div class="text-center mt-1 member_btn">
                                                        <a class="btn btn-yellow rounded" href="#">Become a
                                                            Member</i></a>
                                                    </div>


                                                </div>

                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- item -->
                </div>
                <div class="col-lg-3 col-sm-6 mt-sm-0">
                    <!-- item -->
                    <div class="swiper-slide swiper-slide-active" data-swiper-slide-index="0"
                        style="transition-duration: 0ms; transform: translate3d(0px, 0px, 0px) rotateX(0deg) rotateY(0deg); z-index: 1;">
                        <div class="client-slider-item">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="details">
                                        <div class=" plan_heading single-intro d-flex mb-2">
                                            <h4 class="intro-title">
                                                <span class="intro-count">05</span>
                                                <a class="intro-cat" href="#">Year</a>
                                            </h4>
                                            <div class="tp-price-meta ml-auto">
                                                <p class="my-auto">Price</p>
                                                <h2 class="mt-1 mr-3">120000<small>&#8377;</small></h2>
                                            </div>
                                        </div>

                                        <div class="plan_list">
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="row mx-1">
                                                        <span class="float-left plan_name">Registration Fee</span><span
                                                            class="float-right font-weight">Rs.2000</span>
                                                    </div>
                                                    <div class="row mx-1">
                                                        <span class="float-left plan_name">Monthly EMI</span><span
                                                            class="float-right font-weight">Rs.5000</span>
                                                    </div>
                                                    <div class="row mx-1">
                                                        <span class="float-left plan_name">Free Nights
                                                        </span><span class="float-right font-weight">1</span>
                                                    </div>
                                                    <div class="row mx-1">
                                                        <span class="float-left plan_name">Commission</span><span
                                                            class="float-right font-weight">
                                                            Yes</span>
                                                    </div>
                                                    <div class="row mx-1">
                                                        <span class="float-left plan_name">Flexibility to Travel in any
                                                            season</span><span
                                                            class="float-right font-weight">Yes</span>
                                                    </div>
                                                    <div class="row mx-1">
                                                        <span class="float-left plan_name">Flexibility to choose
                                                            Hotels/Resorts</span><span
                                                            class="float-right font-weight">Yes</span>
                                                    </div>
                                                    <div class="row mx-1">
                                                        <div class="form-check form-check-inline my-3 mr-0">
                                                            <div class="Box_01">
                                                                <input class="form-check-input" type="checkbox"
                                                                    id="inlineCheckbox1" value="option1">
                                                                <label class="form-check-label" for="inlineCheckbox1">I
                                                                    agree the T & C</label>
                                                            </div>
                                                            <div class="Box_02 text-right">
                                                                <a href="#">Click here.</a>
                                                            </div>
                                                        </div>

                                                    </div>

                                                    <div class="text-center mt-1 member_btn">
                                                        <a class="btn btn-yellow rounded" href="#">Become a
                                                            Member</i></a>
                                                    </div>


                                                </div>

                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- item -->
                </div>
            </div>
        </div>
    </div>

    <!--End Silver Plans Area-->
@endsection
@push('script')
@endpush