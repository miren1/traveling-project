@extends('admin.master')
@section('bodyData')
<section class="forms-section" style='min-height:500px'>
    <div class="outer-w3-agile mt-3">
        <h4 class="tittle-w3-agileits mb-4">Add Member</h4>
        
        @if ($message = Session::get('admincreation'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>	
                <strong>{{ $message }}</strong>
        </div>
        @endif

        <div class="stepwizard col-md-offset-3">
            <div class="stepwizard-row setup-panel">
                <div class="stepwizard-step">
                    <a href="#member" type="button" class="btn btn-primary btn-circle">1</a>
                    <p>Members</p>
                </div>
                <div class="stepwizard-step">
                    <a href="#details" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
                    <p>Details</p>
                </div>
                <div class="stepwizard-step">
                    <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
                    <p>Step 3</p>
                </div>
            </div>
        </div>
        <form role="form" id="addMember" action="{{route('add-member.store')}}" method="POST" enctype="multipart/form-data" class="mt-3">
            <div class="row setup-content" id="member">
                    <div class="col-md-12">
                        <div class="row form-group">
                            <div class="col-md-6">
                                @csrf
                                <input type="hidden" name="plan_id" value="{{$id}}" />
                                <select class="form-control" style="height: 30px;" name="type" id="type">
                                    <option value="family">Family - Max 6 Member</option>
                                    <option value="group">Group - Max 20 Member</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <p class="ml-4 mb-2">Primary Member</p>
                        </div>
                        <div class="row">
                            <div class="col-sm-3 nopadding">
                                <div class="form-group">
                                    <select name="prefix[]" style="height: 32px;" class="form-control" required data-bv-notempty="true" data-bv-notempty-message="Please Select Prefix">
                                        <option value="Mr.">Mr.</option>
                                        <option value="Ms.">Ms.</option>
                                        <option value="Mrs.">Mrs.</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3 nopadding">
                                <div class="form-group">
                                    <input type="text" class="form-control" required id="name" name="name[]" value="" placeholder="Name" data-bv-notempty="true" data-bv-notempty-message="Name is required">
                                </div>
                            </div>
                            <div class="col-sm-3 nopadding">
                                <div class="form-group">
                                    <input type="date" class="form-control" required id="dob" name="dob[]" value="" placeholder="DOB" data-bv-notempty="true" data-bv-notempty-message="DOB is required">
                                    <small>Date of birth</small>
                                </div>
                            </div>
                            <div class="col-sm-3 nopadding">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="number" class="form-control" required id="adharno" name="adharno[]" value="" min="0" maxlength="14" minlength="14" placeholder="Aadhar No." data-bv-notempty="true" data-bv-notempty-message="Aadhar No. is required">
                                        <div class="input-group-btn">
                                            <button class="btn btn-success" type="button"  onclick="education_fields();"> <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3 nopadding">
                                <div class="form-group">
                                    <input type="text" class="form-control" required id="panno" name="panno[]" value="" placeholder="Pan No." data-bv-notempty="true" data-bv-notempty-message="Pan No. is required">
                                </div>
                            </div>
                            <div class="col-sm-3 nopadding">
                                <div class="form-group">
                                    <input type="text" class="form-control" required id="passport" name="passportno[]" value="" placeholder="Passport No." data-bv-notempty="true" data-bv-notempty-message="Passport No. is required">
                                </div>
                            </div>
                            <div class="col-sm-3 nopadding">
                                <div class="form-group">
                                    <input type="date" class="form-control" required id="passportdate" name="passportdate[]" value="" placeholder="Passport No." data-bv-notempty="true" data-bv-notempty-message="Passport Exp Date is required">
                                    <small>Passport expiry date</small>
                                </div>
                            </div>
                            <div class="col-sm-3 nopadding">
                                <div class="form-group">
                                    <input type="text" class="form-control" required id="occupation" name="occupation[]" value="" placeholder="Occupation" data-bv-notempty="true" data-bv-notempty-message="Occupation is required">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <p class="ml-4 mb-2">Other Member</p>
                        </div>
                        <div id="education_fields">      
                        </div>
                        <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button>
                    </div>
            </div>
            <div class="row setup-content" id="details">
                <div class="col-xs-6 col-md-offset-3">
                    <div class="col-md-12">
                        <h3> Step 2</h3>
                        <div class="form-group">
                            <label class="control-label">Company Name</label>
                            <input maxlength="200" type="text" required="required" class="form-control" placeholder="Enter Company Name" />
                        </div>
                        <div class="form-group">
                            <label class="control-label">Company Address</label>
                            <input maxlength="200" type="text" required="required" class="form-control" placeholder="Enter Company Address"  />
                        </div>
                        <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button>
                    </div>
                </div>
            </div>
            <div class="row setup-content" id="step-3">
                <div class="col-xs-6 col-md-offset-3">
                    <div class="col-md-12">
                        <h3> Step 3</h3>
                        <button class="btn btn-success btn-lg pull-right" type="submit">Submit</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>

@endsection
@push('css')
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css"/>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<style type="text/css">
    .stepwizard-step p {
        margin-top: 10px;
    }
    .stepwizard-row {
        display: table-row;
    }
    .stepwizard {
        display: table;
        width: 50%;
        position: relative;
    }
    .stepwizard-step button[disabled] {
        opacity: 1 !important;
        filter: alpha(opacity=100) !important;
    }
    .stepwizard-row:before {
        top: 14px;
        bottom: 0;
        position: absolute;
        content: " ";
        width: 100%;
        height: 1px;
        background-color: #ccc;
        z-order: 0;
    }
    .stepwizard-step {
        display: table-cell;
        text-align: center;
        position: relative;
    }
    .btn-circle {
        width: 30px;
        height: 30px;
        text-align: center;
        padding: 6px 0;
        font-size: 12px;
        line-height: 1.428571429;
        border-radius: 15px;
    }
</style>
@endpush
@push('js')
<script src="//code.jquery.com/jquery-1.10.2.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js"></script>
<script>
    var room = 1;
    var $type = "Family";
    var $max = 6;
    $(document).ready(function () {
        $('#addMember').bootstrapValidator({});
    });

    $(document).on('change','#type',function(){
        var val = $(this).val();
        if(val=="group")
        {
            $type = "Group";
            $max = 20;
        }
        else
        {
            $type = "Family";
            $max = 6;
        }
    });
    function education_fields() {
        if(room>=$max)
        {
            alert($type+' has allowed maximum '+$max+' member');
            return
        }
        room++;
        var objTo = document.getElementById('education_fields')
        var divtest = document.createElement("div");
        divtest.setAttribute("class", "form-group removeclass"+room);
        var rdiv = 'removeclass'+room;
        divtest.innerHTML = `<div class="row">
                                <div class="col-sm-3 nopadding">
                                    <div class="form-group">
                                        <select name="prefix[]" style="height: 32px;" class="form-control" required data-bv-notempty="true" data-bv-notempty-message="Please Select Prefix">
                                            <option value="Mr.">Mr.</option>
                                            <option value="Ms.">Ms.</option>
                                            <option value="Mrs.">Mrs.</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-3 nopadding">
                                    <div class="form-group">
                                        <input type="text" class="form-control" required id="name" name="name[]" value="" placeholder="Name" data-bv-notempty="true" data-bv-notempty-message="Name is required">
                                    </div>
                                </div>
                                <div class="col-sm-3 nopadding">
                                    <div class="form-group">
                                        <input type="date" class="form-control" required id="dob" name="dob[]" value="" placeholder="DOB" data-bv-notempty="true" data-bv-notempty-message="DOB is required">
                                        <small>Date of birth</small>
                                    </div>
                                </div>
                                <div class="col-sm-3 nopadding">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="number" class="form-control" required id="adharno" name="adharno[]" value="" min="0" maxlength="14" minlength="14" placeholder="Aadhar No." data-bv-notempty="true" data-bv-notempty-message="Aadhar No. is required"/>
                                            <div class="input-group-btn">
                                                <button class="btn btn-danger" type="button" onclick="remove_education_fields(`+ room +`);"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3 nopadding">
                                    <div class="form-group">
                                        <input type="text" class="form-control" required id="panno" name="panno[]" value="" placeholder="Pan No." data-bv-notempty="true" data-bv-notempty-message="Pan No. is required">
                                    </div>
                                </div>
                                <div class="col-sm-3 nopadding">
                                    <div class="form-group">
                                        <input type="text" class="form-control" required id="passport" name="passportno[]" value="" placeholder="Passport No." data-bv-notempty="true" data-bv-notempty-message="Passport No. is required">
                                    </div>
                                </div>
                                <div class="col-sm-3 nopadding">
                                    <div class="form-group">
                                        <input type="date" class="form-control" required id="passportdate" name="passportdate[]" value="" placeholder="Passport No." data-bv-notempty="true" data-bv-notempty-message="Passport Exp Date is required">
                                        <small>Passport expiry date</small>
                                    </div>
                                </div>
                                <div class="col-sm-3 nopadding">
                                    <div class="form-group">
                                        <input type="text" class="form-control" required id="occupation" name="occupation[]" value="" placeholder="Occupation" data-bv-notempty="true" data-bv-notempty-message="Occupation is required">
                                    </div>
                                </div>
                            </div>
                    </div>`;
        
        objTo.appendChild(divtest);
        $('#addMember').bootstrapValidator('addField', 'prefix[]');
        $('#addMember').bootstrapValidator('addField', 'name[]');
        $('#addMember').bootstrapValidator('addField', 'dob[]');
        $('#addMember').bootstrapValidator('addField', 'adharno[]');
        $('#addMember').bootstrapValidator('addField', 'panno[]');
        $('#addMember').bootstrapValidator('addField', 'passportno[]');
        $('#addMember').bootstrapValidator('addField', 'passportdate[]');
        $('#addMember').bootstrapValidator('addField', 'occupation[]');
    }
    function remove_education_fields(rid) {
        room--;
	   $('.removeclass'+rid).remove();
    }
    $(document).ready(function () {
        var navListItems = $('div.setup-panel div a'),
        allWells = $('.setup-content'),
        allNextBtn = $('.nextBtn');

        allWells.hide();

        navListItems.click(function (e) {
            e.preventDefault();
            var $target = $($(this).attr('href')),
            $item = $(this);

            if (!$item.hasClass('disabled')) {
                navListItems.removeClass('btn-primary').addClass('btn-default');
                $item.addClass('btn-primary');
                allWells.hide();
                $target.show();
                $target.find('input:eq(0)').focus();
            }
        });

        allNextBtn.click(function(){
            if(room>$max)
            {
                alert($type+' has allowed maximum '+$max+' member');
                return
            }
            // if ($('#addMember').has('.has-error').length) {
            //     alert('SOMETHING WRONG');
            //     return
            // }
            var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
            curInputs = curStep.find("input[type='text'],input[type='number'],input[type='url'],textarea[textarea],input[type='date']"),
            isValid = true;

            $(".form-group").removeClass("has-error");
            for(var i=0; i<curInputs.length; i++){
                if (!curInputs[i].validity.valid){
                    isValid = false;
                    $(curInputs[i]).closest(".form-group").addClass("has-error");
                }
            }

            if (isValid)
                nextStepWizard.removeAttr('disabled').trigger('click');
        });

        $('div.setup-panel div a.btn-primary').trigger('click');
    });

    
</script>
@endpush