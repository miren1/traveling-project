@extends('employee.master')
@section('bodyData')
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
    integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>



<script>
function membership(r) {
    // console.log("function called");
    var role = r.value;
    var company = $("#company").val();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: "{{ url('employee-tours/tourfill') }}",
        method: 'GET',
        data: {
            query: role,
            company: company
        },
        dataType: 'json',
        success: function(data) {
            //  alert("sucess");
            $('#demo').empty();
            // $('tbody').html(data.table_data);
            $('#demo').append(data.table_data);
            $('#page').hide();
            
        }
    })
}

   function membershipplan(r) {
    
    var role = r.value;
   var company = $("#company").val();

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: "{{ route('tourfilter') }}",
        method: 'GET',
        data: {
            query: role,
            company: company
        },
        dataType: 'json',
        success: function(data) {
            //  alert("sucess");
            $('#demo').empty();
            // $('tbody').html(data.table_data);
            $('#demo').append(data.table_data);
            $('#page').hide();
        }
    })
}
    

</script>

<!-- Example single danger button -->
    <label class="control-label col-sm-offset-6 col-sm-6" for="company" style="top: 35px; margin-left: 10px"> Package For </label>
<div class="form-group" style="margin-top: 40px;">
    
    <div class="row" style="margin-bottom: 40px">
        <div class="col-sm-2">
       
        <select id="company" class="form-control" onchange='membership(this)' style="  display: inline !important; width: 250px; margin-left: 20px">
            <option value='All'>All</option>
            <?php  foreach ($plancat as $value) {
            # code...
        ?>
            <option value='<?php echo $value->id?>'><?php echo $value->name?></option>
            <?php } ?>
        </select>
         
    </div>

 
        <input type="text" name="Search" id="Search" class="form-control" placeholder="Search Here.." style="border: 1px solid;width: 250px;margin-left: 100px;" onkeyup="membershipplan(this)">
 
    </div>
</div>
<br>
<div class="employee-data">
@include('employee.packagesview')
</div>


@endsection