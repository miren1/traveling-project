@extends('employee.master')
@section('bodyData')
@if (Session::has('password_change') )
<div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>	
    <strong>{{ Session::get('password_change') }}</strong>
</div>

@endif
<section class="forms-section" style='height:400px'>

<div class="outer-w3-agile mt-3">
        <h4 class="tittle-w3-agileits mb-4">Change Password</h4>
        <form action="{{url('empchangePaswordUpdate')}}" method="post">
        @csrf
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputEmail4">Current Password</label>
                    <input type="password" class="form-control" id="inputEmail4" name='password' placeholder="Enter Current Password" required="" >
                </div>
                <div class="form-group col-md-6">
                    <label for="inputPassword4">New Password</label>
                    <input type="password" class="form-control" id="inputPassword4" name='npassword'placeholder="Enter New Password" required="" minlength=6 maxlength=12>
                </div>
            </div>
           
            
            <button type="submit" class="btn btn-primary">Update</button>
        </form>
    </div>

</section>

@endsection
