@extends('employee.master')
@section('bodyData')
<meta name="csrf-token" content="{{ csrf_token() }}">

<script>
function searchfun(name)
{   
    
      var query= document.getElementById('sname').value;
    // alert(query);
      // var sdate =  document.getElementById('sdate').value;
      // var status = document.getElementById('status').value;
     $.ajax({
   url:"{{ route('empinquieylive') }}",
   method:'GET',
   data:{query:query},
   dataType:'json',
   success:function(data)
   {
      $('#tableid').empty();
    // $('tbody').html(data.table_data);
    $('#tableid').append(data.table_data);
   }
  })
}
</script>
<style>
.fa-paper-plane
{
color:#083672
}
</style>
<div class="outer-w3-agile mt-3" style='min-height:400px'>
 
@if ($message = Session::get('UpdateInquiey'))
<div class="alert alert-success alert-block">
	<button type="button" class="close" data-dismiss="alert">×</button>	
        <strong>{{ $message }}</strong>
</div>
@endif
@if ($message = Session::get('inquiryTransfer'))
<div class="alert alert-success alert-block">
	<button type="button" class="close" data-dismiss="alert">×</button>	
        <strong>{{ $message }}</strong>
</div>
@endif

@if ($message = Session::get('DeleteInquiey'))
<div class="alert alert-success alert-block">
	<button type="button" class="close" data-dismiss="alert">×</button>	
        <strong>{{ $message }}</strong>
</div>
@endif
    <div class='row'>
    &nbsp;&nbsp;
  
      <div class='col-sm-10'>
            <div class="form-inline mx-auto search-form">
             <div class="col-sm-3">
                 <a href='allempinquiry'><button class='btn btn-primary'>Show Inquiry</button></a>
<!--         <input type="date" name='sdate' id='sdate' onchange='searchfun(this)'> -->
                  </div>
                <div class="col-sm-3">
                <input class="form-control mr-sm-2" type="search" placeholder="Search With Name,ID" aria-label="Search" name='sname' onkeypress='searchfun(this)' id='sname'>
                </div>
                
<!--                  <div class="col-sm-2">
                    <select onchange='searchfun(this)' id='status'>
                        <option value='done'>Done</option>
                        <option value='new' selected>New Inquiry</option>
                        <option value='follow'>Follow Up Inquiry</option>
                      
                    </select>
                  </div> -->
                <!-- <button class="btn btn-style my-2 my-sm-0" type="submit">Search</button> -->
            </div>
        </div>
        <div class='col-sm-2'></div>
</div>
<br>

    <table class="table">
        <thead class="thead-dark"><tr>
         <th scope="col">Id</th>
            <th scope="col">First Name</th>
            <th scope="col">Contact Number</th>
            <th scope="col">Email</th>
           <th scope="col">Appointment Date</th>
            <th scope="col">Status</th>
            
             <th scope="col">Action</th>
            </tr>
    </thead>
    <tbody id='tableid'>

            <tr>
            <?php  foreach ($users as $value) {
          # code...
        ?>		<td><?php echo "JJ-INQ".$value->id?></td>
                <td scope="row"><?php echo ucfirst($value->first_name)?></td>
               <td><?php echo $value->mobile_no?></td>
                <td><?php echo $value->email?></td>
                <td><?php echo $value->appointment_date?></td>
                 <td>
                    <b><?php  echo ucfirst($value->inq_status) ?></b></td>
                    
                    <td>
                      <a href="/empinquirydetails/{{$value->id}}">
                      
                      <i class="fa fa-info-circle" aria-hidden="true" title='Detail View' style='font-size:20px'></i>
                        </a>   

                         <a href="/empeditUpdateform/{{$value->id}}">
                         <!-- <button class="btn btn-info">Update</button> -->
                         <i class="fa fa-edit" aria-hidden="true" title='Edit' style='font-size:20px'></i>
                         </a>
                        
                         <a href="/empexchange/{{$value->id}}">
                         <!-- <button class="btn btn-info">Update</button> -->
                         <i class="fa fa-paper-plane" aria-hidden="true" title='Transfer Inquiry' style='font-size:20px;color:#083672'></i>
                         </a>
                        

                </td>
              
            </tr>  
            <?php } ?>
             
            

           
        </tbody>
    </table>
    <center>
   <div class="container" style='margin-left:30%'>
    {{ $users->links() }}
 

</div>

</center>
  
    


</div>
@endsection
