@extends('employee.master')
@section('bodyData')
<script src="//code.jquery.com/jquery-1.10.2.min.js"></script>
<script src = "https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script>

 $(function() {
        $( "#sdate" ).datepicker({
        minDate: new Date(),
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true,
        yearRange: '-110:+29'
    });
});
 $(function() {
        $( "#edate" ).datepicker({
        minDate: new Date(),
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true,
        yearRange: '-110:+29'
    });
});


function endDateValidation(){
    var startDate = $("#sdate").val();
    var endDate = $("#edate").val();

    console.log(startDate);
    console.log(endDate);

        if ((Date.parse(startDate) > Date.parse(endDate))) {
            alert("End date should be greater than Start date");
            document.getElementById("edate").value = "";
        }
    }

function calcDiff() {

    var a = document.getElementById("sdate").value,
        b = document.getElementById("edate").value,
        leave_type = document.getElementById("leavemode").value,
        c = 24*60*60*1000;
    //console.log(leave_type);
    var Difference_In_Time = new Date(b).getTime() - new Date(a).getTime(); 
      //console.log(Difference_In_Time);

      if(leave_type == 1){
    
        var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
        var days = Difference_In_Days+1;

      }else{
        
        var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
        var days = (Difference_In_Days+1)/2;

      }
      var hours = days * 8;

      $("#days").text(days);
      $("#day").val(days);
      $("#hours").text(hours);

        console.log(days); //show difference
        console.log(hours);
 }  

 function fileValidation() {
            var fileInput = 
                document.getElementById('file');
              
            var filePath = fileInput.value;
          
            // Allowing file type
            var allowedExtensions = 
                    /(\.jpg|\.jpeg|\.png|\.pdf)$/i;
              
            if (!allowedExtensions.exec(filePath)) {
                alert('Invalid file type, Please select again');
                fileInput.value = '';
                return false;
            } 
            else 
            {
            
                if (fileInput.files && fileInput.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        document.getElementById(
                            'imagePreview').innerHTML = 
                            '<img src="' + e.target.result
                            + '"/>';
                    };
                      
                    reader.readAsDataURL(fileInput.files[0]);
                }
            }
        }

</script>
<link href="https://use.fontawesome.com/releases/v5.0.4/css/all.css" rel="stylesheet">
<link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel = "stylesheet">
<style>
#sidebar {
    background: #083672;
}
#sidebar ul li.active>a, #sidebar a[aria-expanded="true"] {
    color: #fff;
    background: #083672;
}
#sidebar .sidebar-header {
    padding: 20px;
    background: #f3941e;
}
ul ul a {
    font-size: 0.9em !important;
    padding-left: 30px !important;
    background: #083672;
}
#sidebar ul li a:hover {
    color: #083672;
    background: #fff;
}
.btn-primary, .btn-primary:hover, .btn-primary:active, .btn-primary:visited {
    background-color: #083672 !important;
border-color: #083672 !important;
}
/*.btn-success, .btn-success:hover, .btn-success:active, .btn-success:visited {
    background-color: #083672 !important;
border-color: #083672 !important;
}*/
.btn-info, .btn-info:hover, .btn-info:active, .btn-info:visited {
    background-color: #f3941e !important;
    border-color: #f3941e !important;
}
.bg-primary
{
 background-color: #f3941e !important;
    border-color: #f3941e !important;
}

.fa-info-circle,.fa-edit
{
color:#083672;
}
ul.top-icons-agileits-w3layouts a.nav-link i {
    display: inline-block;
    font-size: 17px;
    color: #083672;
}
.nav-pills .nav-link.active, .nav-pills .show>.nav-link {
    color: #fff;
    background-color: #083672;
}
a {
    color: #083672;
    text-decoration: none;
    background-color: transparent;
    -webkit-text-decoration-skip: objects;
}
a:hover {
    color: #083672;
    text-decoration: underline;
}
.ui-datepicker-month{
    color: #f7af38 !important;
}

.ui-datepicker-year{
    color: #f7af38 !important;
}
/* Chrome, Safari, Edge, Opera */
input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
-webkit-appearance: none;
margin: 0;
}

/* Firefox */
input[type=number] {
-moz-appearance: textfield;
        }
/* #mytablebdm input {
   /* Awesome styling */
</style>
<section class="forms-section" style='min-height:500px'>
@if ($message = Session::get('CreateLeave'))
<div class="alert alert-success alert-block">
	<button type="button" class="close" data-dismiss="alert">×</button>	
        <strong>{{ $message }}</strong>
</div>
@endif

<div class="outer-w3-agile mt-3">
        <h4 class="tittle-w3-agileits mb-4">Insert Leave</h4>
        
        @if ($message = Session::get('inquiryAdd'))
<div class="alert alert-success alert-block">
	<button type="button" class="close" data-dismiss="alert">×</button>	
        <strong>{{ $message }}</strong>
</div>
@endif
<a style='color:red'><?php echo "Your Total Remaining PL For this Year:"?><?php echo $remaining_pl?></a>
<a style='color:red'><?php echo "Your Total Remaining CL For this Year:".$remaining_cl?></a>
        <form action="{{url('createLeave')}}" method="post" enctype="multipart/form-data">
        @csrf
      <div class="form-row">
      <input type='hidden' name='cl' value='<?php echo $remaining_cl?>'>
      <input type='hidden' name='pl' value='<?php echo $remaining_pl?>'>
        <div class="form-group col-md-6">
            <label for="inputEmail4">Leave Type</label>
            <select id="leavetype" class="form-control" name='leavetype' required>
                <option value='CL'>Casual Leaves </option>
                <option value='PL' selected>Planned Leaves</option>
                <option value='WFH'>Work From Home</option>
            </select>
        </div>
        <div class="form-group col-md-6">
            <label for="inputEmail4">Leave Mode</label>
            <select id="leavemode" class="form-control" name='leavemode' required>
            <option value='0'>Half</option>
            <option value='1' selected>Full</option>
               </select>
        </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="inputEmail4">Start Date</label>
                <input type='text' class="form-control" name='sdate' required id='sdate' >
            </div>
            <div class="form-group col-md-6">
                <label for="inputEmail4">End Date</label>
                <input type='text' class="form-control" name='edate' required id='edate' onblur='endDateValidation()' onchange="calcDiff()"> 
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <b>Days : </b><b><span id="days"></span></b>
                <input type='hidden' id="day" name='days' value=''>
            </div>
            {{-- <div class="form-group col-md-6">
                <b>Hours : </b><b><span id="hours"></span></b>
                
            </div> --}}
        </div>
        <div class="form-group">
            <label for="exampleFormControlTextarea1">Reason</label>
            <textarea class="form-control" name='reason' id="exampleFormControlTextarea1" rows="3" required></textarea>
        </div>
        <div class="form-group">
            <label for="exampleFormControlTextarea1">Documents</label>
            <input type='file' id="file" name='documentsleave[]' multiple onchange="return fileValidation()">
        </div>

        <button type="submit" class="btn btn-primary">Leave Apply</button>
        </form>
    </div>

</section>

@endsection
