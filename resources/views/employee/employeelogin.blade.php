<!DOCTYPE html>
<html lang="en">

<head>
    <title>Jag Joyu Employement</title>
    <!-- Meta Tags -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="utf-8">
    <meta name="keywords" content="Jag Joyu" />
<style>
.form-body-w3-agile label, .forgot a {
    font-size: 13px;
    font-weight: 600;
    letter-spacing: 1px;
    color: #083672;
}
</style>

    <script>
        addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
    <!-- //Meta Tags -->

    <!-- Style-sheets -->
    <!-- Bootstrap Css -->
     <!-- Style-sheets -->
    <!-- Bootstrap Css -->
    <link href="{{ asset('admin/css/bootstrap.css') }} " rel="stylesheet" type="text/css" media="all" />
    <!-- Bootstrap Css -->
    <!-- Common Css -->
    
    <link href="{{ asset('admin/css/style.css') }}" rel="stylesheet" type="text/css" media="all" />
    <!--// Common Css -->
    <!-- Nav Css -->
    <link rel="stylesheet" href="{{ asset('admin/css/style4.css') }}">
    <link rel="icon" type="image/png" href="<?php echo asset('images').'/'.'favicon.png'?>"/>

    <!--// Nav Css -->
    <!-- Fontawesome Css -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <link href="{{ asset('admin/css/fontawesome-all.css') }}" rel="stylesheet" type='text/css'>
    <!--// Fontawesome Css -->
    <!--// Style-sheets -->

    <!--web-fonts-->
    <link href="//fonts.googleapis.com/css?family=Poiret+One" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <!--//web-fonts-->
</head>

<body>
    <div class="bg-page py-5" style='background-color:#083672'>
        <div class="container">
            <!-- main-heading -->
            <h2 class="main-title-w3layouts mb-2 text-center text-white">Jag Joyu Employement </h2>
            <!--// main-heading -->
            @if ($message = Session::get('error'))
<div class="alert alert-danger alert-block">
	<button type="button" class="close" data-dismiss="alert">×</button>	
        <strong>{{ $message }}</strong>
</div>
@endif

@if ($message = Session::get('Empmessage'))
<div class="alert alert-danger alert-block">
	<button type="button" class="close" data-dismiss="alert">×</button>	
        <strong>{{ $message }}</strong>
</div>
@endif


          
            <div class="form-body-w3-agile text-center w-lg-50 w-sm-75 w-100 mx-auto mt-5">
                <form method="post" action="{!! url('employeeLoginCheck') !!}">
                @csrf
                    <div class="form-group">
                        <label style='color:#083672'>Email address</label>
                        <input type="email" class="form-control" placeholder="Enter Email" required="" name='email'>
                    </div>
                    <div class="form-group">
                        <label style='color:#083672'>Password</label>
                        <input type="password" class="form-control" placeholder="Enter Password" required="" name='password'>
                    </div>
                    <div class="d-sm-flex justify-content-between">
                        <!-- <div class="form-check col-md-6 text-sm-left text-center">
                            <input type="checkbox" class="form-check-input" id="exampleCheck1">
                            <label class="form-check-label" for="exampleCheck1">Remember me</label>
                        </div> -->
                        <div class="forgot col-md-6 text-sm-right text-center">
                            <!-- <a href="forgot.html">forgot password?</a> -->
                        </div>
                    </div>
                    <input type="submit" class="btn btn-primary error-w3l-btn mt-sm-5 mt-3 px-4" role='button' value='Login' style='background-color:#083672'>
                </form>
               
                <!-- <h1 class="paragraph-agileits-w3layouts mt-2">
                    <a href="index.html">Back to Home</a>
                </h1> -->
            </div>

            <!-- Copyright -->
            <div class="copyright-w3layouts py-xl-3 py-2 mt-xl-5 mt-4 text-center">
                <p>© 2020 All Rights Reserved | Design by Jag Joyu
                </p>
            </div>
            <!--// Copyright -->
        </div>
    </div>


    <!-- Required common Js -->
    <script src="{{ asset('admin/js/jquery-2.2.3.min.js')}}"></script>
    <!-- //Required common Js -->

    <!-- Js for bootstrap working-->
    <script src="{{ asset('admin/js/bootstrap.min.js') }} "></script>
    <!-- //Js for bootstrap working -->

</body>

</html>