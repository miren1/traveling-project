@extends('employee.master')

@section('bodyData')
<section class="forms-section">
    <div class="outer-w3-agile mt-3">
        <a href="{{route('member-plans')}}" class='btn btn-primary'>Membership Plan</a>
        <h4 class="tittle-w3-agileits mb-4">@if(isset($customer)) Edit @else Add @endif invite member </h4>
        <form action="{{route('member-plans-store-member-req')}}" id="customerForm" method="post">
            @csrf
            @if(isset($id))
                <input type="hidden" name="id" value="{{$id}}">
                <input type="hidden" name="emp_invite" value="emp_invite">
            @endif
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputEmail4">First Name</label>
                    <input type="text" class="form-control" id="firstname" placeholder="Enter Name" required name='first_name' value="@if(isset($customer)){{$customer->first_name}}@endif" data-bv-notempty="true" data-bv-notempty-message="First Name is required">
                </div>
                <div class="form-group col-md-6">
                    <label for="inputPassword4">Last Name</label>
                    <input type="text" class="form-control" id="lastname" placeholder="Last Name" required name='last_name' value="@if(isset($customer)){{$customer->last_name}}@endif" data-bv-notempty="true" data-bv-notempty-message="Last Name is required">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputEmail4">Contact Number</label>
                    <input type="number" min="0" minlength="10" maxlength="10" class="form-control" id="contactnumber" placeholder="Enter Contact Number" value="@if(isset($customer)){{$customer->mobile_number}}@endif" required="" name='mobile_number' data-bv-notempty="true" data-bv-notempty-message="Contact Number is required">
                </div>
                <div class="form-group col-md-6">
                    <label for="inputPassword4">WhatsApp Number</label>
                    <input type="number" min="0" minlength="10" maxlength="10" class="form-control" id="wanumber" placeholder="Enter WhatsApp  Number" required="" name='whats_up' value="@if(isset($customer)){{$customer->whats_up}}@endif" data-bv-notempty="true" data-bv-notempty-message="WhatsApp Number is required">
                </div>
            </div>
            <div class="row">
                    <div class="form-group col-md-6">
                        <label for="inputEmail4">Email</label>
                        <input type="email" class="form-control" id="inputEmail4" pattern="[a-zA-Z0-9._%+-]+@[a-z0-9.-]+\.[a-zA-Z]{2,4}" placeholder="Email" required="" name='email' value="@if(isset($customer)){{$customer->email}}@endif" data-bv-notempty="true" data-bv-notempty-message="E-mail is required" data-bv-remote="true" data-bv-remote-type="GET"
                        data-bv-remote-url="{{ url('customer/email') }}/@if(isset($customer)){{$customer->id}}@else All @endif"
                        data-bv-remote-message="Opps ! Email Already Exist">
                    </div>
            </div>
            
            <button type="submit" class="btn btn-primary">@if(isset($customer)) Update @else Create @endif invite</button>
        </form>
    </div>
</section>
@endsection
@push('css')
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css"/>
<style type="text/css">
    .help-block{
        color: #ce2320;
    }
</style>
@endpush
@push('js')
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js"></script>

<script>
  $(document).ready(function () {
    $('#customerForm').bootstrapValidator();
  });

  $('#firstname').keypress(function (e) {
        var regex = new RegExp("^[a-zA-Z]+$");
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (regex.test(str)) {
            return true;
        }
        else
        {
        e.preventDefault();
        alert('Please Enter Alphabate only');
        return false;
        }
    });

  $('#lastname').keypress(function (e) {
        var regex = new RegExp("^[a-zA-Z]+$");
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (regex.test(str)) {
            return true;
        }
        else
        {
        e.preventDefault();
        alert('Please Enter Alphabate only');
        return false;
        }
    });
</script>
@endpush