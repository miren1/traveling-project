@extends('employee.master')
@section('bodyData')
<div class="outer-w3-agile mt-3" style='min-height:400px'>
    <div class="table-responsive">
        <table id="example" class="table" style="width: 100%">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">First name</th>
                    <th scope="col">Last name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Contact Number</th>
                    <th scope="col">Whatsapp Number</th>
                    <th scope="col">Created at</th>
                    <th>Actions</th>
                </tr>
            </thead>
           
        </table>
    </div>
</div>
@endsection 

@push('css')
<style type="text/css">
    div#example_filter input[type="search"] {
    box-sizing: border-box;
    border: 1px solid #d2d6dc;
}
</style>
@endpush
@push('js')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script type="text/javascript">
 
      $(document).on("click", '.agentdelete', function(e) {
            e.preventDefault();
            var id = $(this).attr('data-id');
            swal({
                    title: "Are you sure?",
                    text: "You want to delete this Ask Call.",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {

                        $.ajax({
                            type: "POST",
                            url: "{{route('askforcalls-delete')}}",
                            data: {
                                "id": id,
                                "_token": "{{ csrf_token() }}"
                            },
                            success: function(response) {
                                if (response.status == "success") {
                                    swal(response.message, {
                                        icon: 'success',
                                    }).then((result) => {
                                         $('#example').DataTable().ajax.reload(null,false);
                                    });
                                } else {
                                    swal("Error","Something went wrong!",{icon:'error'});
                                }
                            }
                        });
                    }
                });
        });


    $(document).ready(function() {
  
    $table = $("#example").DataTable({
      "ajax": "{{ url('admin-askforcall/list') }}/"+{{$id}},
      "processing": true,
      "serverSide": true,
      "ordering": false,
      "columns": [
            { "data": "first_name"},
            { "data": "last_name"},
            { "data": "email"},
            { "data": "contactnumber"},
            { "data": "wpnumber"},
            // { "data": "created_at"},

            { "data": "created_at",render: function (data, type, row) {
          return moment(new Date(data).toString()).format('DD/MM/YYYY');
        }},
            { "data": "id", 
                    render:function(data,type,row,meta){
                      
                     var html= '<a  href="{{url("/admin-askforcall/askforcall_view/")}}/'+btoa(data)+'"><i class="fa fa-info-circle" aria-hidden="true"'+ 
                                'title="Detail View" style="font-size:20px"></i></a>'+
                    
                                '<a href="#" class="ml-1 agentdelete"  data-id="'+btoa(data)+'"><i class="fa fa-trash"'+
                                 'aria-hidden="true" style="color:red;font-size:20px" title="Delete"></i>'
                                '</a>';

                  return html;          
                      
              }

            },
        ]
    });


    
  } );
</script>

@endpush
