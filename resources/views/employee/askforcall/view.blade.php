@extends('employee.master')
@section('bodyData')


    <div class="container-fluid">
        <div class="row">
            <a href='{{url('/admin-askforcall')}}' class='btn btn-primary' style='color:white'>Show Ask Call</a>
            <div class="col-sm-12 mt-4">

                <ul class="nav nav-tabs">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle='tab' href="#profile">Ask Call</a>

                    </li>
                    {{-- <li class="nav-item">
                        <a class="nav-link" data-toggle='tab' href="#Address">Commission (Agent / Members)</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle='tab' href="#booking">Commission (Employee)</a>

                    </li> --}}

                </ul>

                <div class="tab-content">


                   


                    <!-- Profile Detail -->

                    <div id="profile" class="container tab-pane active">

                        <div class="row">

                            <div class="col-sm-8">
                                <br>

                                <ul class="list-group">

                                    <li class="list-group-item"><b>First name: </b>
                                      @if(isset($plan))
                                       {{ $plan->first_name }} 
                                      @endif
                                    </li>
                                    <li class="list-group-item"><b>Last name: </b>
                                      @if(isset($plan))
                                       {{ $plan->last_name }} 
                                      @endif
                                    </li>
                                    <li class="list-group-item"><b>Email: </b>
                                      @if(isset($plan))
                                       {{ $plan->email }} 
                                      @endif
                                    </li>
                                    <li class="list-group-item"><b>Contact number: </b>
                                      @if(isset($plan))
                                       {{ $plan->contactnumber }} 
                                      @endif
                                    </li>
                                    <li class="list-group-item"><b>Whatsapp number: </b>
                                      @if(isset($plan))
                                      {{  $plan->wpnumber }} 
                                      @endif  
                                      </li>
                                    <li class="list-group-item"><b>Appointment time : </b>
                                      @if(isset($plan))
                                      {{  $plan->datetime }} 
                                      @endif 
                                    </li>
                                    

                                </ul>
                            </div>
                        </div>


                    </div>







                </div>

            </div>





        </div>




    </div>



    



@endsection
