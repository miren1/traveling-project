@extends('employee.master')
@section('bodyData')
<script>
function result()
{
   
 var start = document.getElementById('start').value;
 var status = document.getElementById('status').value;
 var isApprove = document.getElementById('isapprove').value;
 if(start == '')
 {
   alert("Please Select Year And Month");
   return false;
 }
// var index = start.indexOf('-');
// var len = start.length;
// var year  =  start.slice(0,index);
// var month = start.slice(index+1,year);


//  console.log("start is " + start);
//  console.log("end is " + end);
 $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
   url:"{{ route('empleaveAjax') }}",
   method:'GET',
   data:{start:start,status:status,isApprove:isApprove},
   dataType:'json',
   success:function(data)
   {
    $('#demo').empty();
    $('#demo').append(data.table_data);
   
   }
  })}
</script>
<div class="container-fluid">
<div class="row">
<label for="start"> Select Month And Year : &nbsp;&nbsp;</label>

<input type="month" id="start" name="start"
        onchange='result()'>
        <label for="start">&nbsp;&nbsp; Select Status Of Leave : &nbsp;&nbsp;</label>

        <select onchange='result()' id='status'>
                        <option value='all' selected>All</option>

                        <option value='cl'>CL</option>
                        <option value='pl'>PL</option>
                        <option value='wfh'>WFH</option>
                        
                    </select>
                    <label for="start">&nbsp;&nbsp; Leave Approve : &nbsp;&nbsp;</label>
               
                    <select onchange='result()' id='isapprove'>
                        <option value='approved' selected>Approved</option>
                        <option value='rejected'>Rejected</option>
                     

                    </select>

</div>
<br>
<div class='row'>
<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">Start Date</th>
      <th scope="col">End Date</th>
      <th scope="col">Leave Mode</th>
      <th scope="col">Is Paid</th>
      <th scope="col">Leave Type</th>
      <th scope="col">Total Days</th>
      <th scope="col">Actions</th>
     
      

    </tr>
  </thead>
  <tbody id='demo'>
 
  </tbody>
</table>
</div>
</div>
</div>
</div>
</div>
           
@endsection 
