@extends('employee.master')
@section('bodyData')
<meta name="csrf-token" content="{{ csrf_token() }}">

<div class="outer-w3-agile mt-3">
    <div class='row'>
      <div class='col-md-6'>
        <div class="btn btn-primary"><i class="fa fa-list mr-2"></i>Employee Leave list</div>
      </div>
     
    </div>
<br>

    <table class="table table-hover" style="width: 100%;" id="dataTable">
        <thead class="thead-dark">
            <tr>
                <th scope="col">Sr. No</th>
                <th scope="col">Emp Name</th>
                <th scope="col">Start Date</th>
                <th scope="col">End Date</th>
                <th scope="col">Reason</th>
                <th scope="col">Total Days</th>
                <th scope="col">Status</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table> 
  </div>

    <center>
  </div>
  </div>
</div>
</center>
</div>
@endsection

@push('js')
<script>
  $(function() {
    $("#dataTable").DataTable({
        "pageLength": 5,
        "autoWidth": true,
        "lengthChange": true,
        "searching": true,
        "aLengthMenu": [[10,25, 50,100], [10,25, 50,100]],
        order: [
            [0, 'desc']
        ],
        "ajax": {
            "url": "{{ url('AllEmpLeaveReport') }}",
            "data": {
                "_token": "{{ csrf_token() }}"
            },
        },
        "columns": [
            {
                "data": "id",
                render: function(data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1
                }
            },
            {
                "data": "id",
                render: function(data, type, row, meta) {
                    
                   // console.log(row.id);
                    return row.name +' '+ row.surname
                }
            },
            {
                "data": "leave_date_start",orderable:false,
            },
            {
                "data": "leave_date_end",orderable:false,
            },
            {
                "data": "reason",orderable:false,
            },
            {
                "data": "total_leave_days",orderable:false,
            },

            {
                "data": "approve_status",
                render: function(data, type, row, meta) {
                    if(data == "Reject"){
                        return "Rejected";
                    }else if(data == "Approve"){
                        return "Approved";
                    }else{
                        return "Pending";
                    }
                }
            },
            /*{
                data: "id",orderable:false,
                render: function(data, row, alldata) {
                  var $id = btoa(alldata.id);
                    $html = `<a href="{{url('/admin-banner/edit')}}/`+$id+`">
                         <!-- <button class="btn btn-info">Update</button> -->
                         <i class="fa fa-edit" aria-hidden="true" title='Edit' style='font-size:20px'></i>
                         </a>
                      
                      <i class="fa fa-trash delete" style='font-size:20px;color:red;cursor:pointer;' title='delete banner' data-id="`+alldata.id+`" >
                      </i>`;
                    return $html;
                }
            },*/
        ]
    });

    $(document).on('click','.delete',function(){
      var id = $(this).data('id');
      swal({
        title: "Are you sure?",
        text: "You want to delete this Banner.",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      }).then((willDelete) => {
        if(willDelete)
        {
          $.ajax({
            method:'post',
            url: "{{route('admin-banner.destroy')}}",
            data: {id:id,
              '_token':"{{csrf_token()}}"
            },
            success : function(result){
              swal({title:result.title,icon:result.icon,text:result.message});
              $('#dataTable').DataTable().ajax.reload();
            }
          });
        }
      });
    });

    $(document).on('click','.leave',function(){
      var id = $(this).data('id');
      // alert('true');
      $('#modal-myvalue').val(id);
    });
  });
</script>
@endpush
