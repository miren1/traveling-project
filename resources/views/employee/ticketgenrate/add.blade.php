@extends('employee.master')
@push('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.css"
        integrity="sha512-In/+MILhf6UMDJU4ZhDL0R0fEpsp4D3Le23m6+ujDWXwl3whwpucJG1PEmI3B07nyJx+875ccs+yX2CqQJUxUw=="
        crossorigin="anonymous" />
    <link rel="stylesheet"
        href="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css" />
@endpush

@section('bodyData')
    <section class="forms-section">
        <div class="outer-w3-agile mt-3">
            <h4 class="tittle-w3-agileits mb-4"> Generator Ticket </h4>

            <form class="defaultForm" action="{{ route('employee-ticket-generator-store') }}" method="POST"
                enctype="multipart/form-data">
                @csrf


                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="Subject">Category</label>

                        <select id="c_state_id" name="category" class="form-control" required aria-required="true"
                            data-bv-notempty-message="Category is required">
                            <option value="">Select Category</option>
                            @if ($ticket_category)
                                @foreach ($ticket_category as $sub)
                                    @if ($sub)
                                        <option value="{{ $sub->id }}">
                                            {{ $sub->name }}</option>
                                    @endif
                                @endforeach
                            @endif
                        </select>


                    </div>
                    <div class="form-group col-md-6">
                        <label for="inputYears">Sub Category</label>
                        <select id="c_city_id" name="sub_category" class="form-control" required aria-required="true"
                            data-bv-notempty-message="Category is required">
                            <option value="">Select Sub Category</option>

                        </select>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="body">Message</label>
                        <textarea id="body" name="message" rows="8" class="form-control" required aria-required="true"
                            data-bv-notempty-message="Message is required"></textarea>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="inputEmail4">File <span style="color: red"></span></span></label>
                        <input type="file" name="file" class="form-control dropify" 
                            data-bv-file-extension="jpg,jpeg,png,pdf"  />
                    </div>
                </div>



                <button type="submit" name="save" class="btn btn-primary">Generator Ticket</button>
            </form>
        </div>
    </section>

@endsection


@push('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/js/dropify.min.js"
        integrity="sha512-8QFTrG0oeOiyWo/VM9Y8kgxdlCryqhIxVeRpWSezdRRAvarxVtwLnGroJgnVW9/XBRduxO/z1GblzPrMQoeuew=="
        crossorigin="anonymous"></script>
    <script type="text/javascript"
        src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js"></script>
    <script>
        $(document).ready(function() {
            $('.dropify').dropify();
            $('.defaultForm').bootstrapValidator();

            $('select#c_state_id').change(function() {
                var value = $(this).val();
                var city = '';
                console.log(value);
                statechange(value, city);
            });

        });


        function statechange(value, city) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var cat_id = value;
            $.ajax({
                url: "{{ route('employee-ticket-generator-getCat') }}",
                type: "POST",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "cat_id": cat_id
                },

                success: function(data, city) {
                    $('#c_city_id').empty();

                    $.each(data.cat, function(index, cat) {


                        $('#c_city_id').append('<option value="' + cat.id + '">' + cat.name +
                            '</option>');


                    })
                }
            })
        }

    </script>


    <script>



    </script>


@endpush
