@extends('employee.master')
@section('bodyData')
    <div class="outer-w3-agile mt-3" style='min-height:400px'>
        <div class='row'>

            <div class='col-sm-2'>
                <a href="{{ url('employee-ticket-generator/add') }}" style='text-align:left'>
                    <button class="btn btn-primary">Generator ticket</button>
                </a>
            </div>

        </div>
        <br>
        <div class="table-responsive">
            <table id="example" class="table" style="width: 100%">
                <thead class="thead-dark">
                    <tr>
                        {{-- <th scope="col">Subject</th>
                        <th scope="col">Message</th> --}}
                        <th scope="col">Categories</th>
                        <th scope="col">Sub Categories</th>
                        <th scope="col">Status</th>
                        <th scope="col">Created at</th>
                        <th>Actions</th>


                    </tr>
                </thead>

            </table>
        </div>
        <br> <br><br> <br> <br><br>



    </div>

@endsection
@push('css')
    <style type="text/css">
        div#example_filter input[type="search"] {
            box-sizing: border-box;
            border: 1px solid #d2d6dc;
        }

    </style>
@endpush
@push('js')
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <script type="text/javascript">
        $(document).on("click", '.agentdelete', function(e) {
            e.preventDefault();
            var id = $(this).attr('data-id');
            swal({
                    title: "Are you sure?",
                    text: "You want to delete this Plan.",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {

                        $.ajax({
                            type: "POST",
                            url: "{{ route('member-plans-delete') }}",
                            data: {
                                "id": id,
                                "_token": "{{ csrf_token() }}"
                            },
                            success: function(response) {
                                if (response.status == "success") {
                                    swal(response.message, {
                                        icon: 'success',
                                    }).then((result) => {
                                        $('#example').DataTable().ajax.reload(null, false);
                                    });
                                } else {
                                    swal("Error", "Something went wrong!", {
                                        icon: 'error'
                                    });
                                }
                            }
                        });
                    }
                });
        });


        $(document).ready(function() {

            $table = $("#example").DataTable({
                "ajax": "{{ url('employee-ticket-generator/list') }}",
                "processing": true,
                "serverSide": true,
                "ordering": false,
                "columns": [

                    {
                        "data": "ticket_categories.name"
                    },
                    {
                        "data": "ticket_sub_categories.name"
                    }
                    ,
                    {
                        "data": "status"
                    },
                    {
                        "data": "created_at",
                        render: function(data, type, row, meta) {

                            return moment(data).format('DD-MM-YYYY,h:mm a');
                            // body...
                        }
                    },
                    {
                        "data": "id",
                        render: function(data, type, row, meta) {

                            var html =  '<a  href="{{ url('employee-ticket-generator/view/') }}/' + btoa(data) +
                                '"><i class="fa fa-eye" aria-hidden="true"' +
                                'title="Detail View" style="font-size:20px"></i></a>';

                            return html;

                        }

                    },
                ]
            });



        });

    </script>

@endpush
