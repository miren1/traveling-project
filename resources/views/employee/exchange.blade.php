@extends('employee.master')
@section('bodyData')
<?php  $last_id = collect(request()->segments())->last() ;?>
<section class="forms-section" style='min-height:500px'>
<div class="outer-w3-agile mt-3">
        <h4 class="tittle-w3-agileits mb-4">Inquiry Transfer</h4>
        <form action="{{url('updatecommentsemp')}}" method="post">
        @csrf
        <input name='inid' type='hidden' value='<?php echo $last_id?>'>
     <div class="form-row">
               
               <div class="form-group col-md-6">
                   <label for="inputEmail4">Inquiry Transfer To</label>
                   <select id="inputState" class="form-control" name='emp_handle_by_fk' reqyired>
                   <?php //$tempselected = $inquiryData[0]->emp_handle_by_fk?>
                   <?php foreach ($users as $u) {
                      
                       $temp = $u->id;

                   ?>
                       
        <option value='<?php echo $temp;?>'><?php echo $u->name." ".$u->surname ?>
                      </option>

        
                   
                   <?php } ?>
                   </select>
               </div>
</div> <!-- // ROw Close -->


<div class="form-row">
               
<div class="form-group">
    <label for="exampleFormControlTextarea1">Reason For Transfer Inquiry</label>
    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name='comments' required></textarea>
  </div>
</div> <!-- // ROw Close -->
 
            <button type="submit" class="btn btn-primary">Update Inquiry</button>
        </form>
    </div>

</section>

@endsection
