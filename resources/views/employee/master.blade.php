<?php 
$id = Session::get('eid');
$count  = DB::table('messages')->where('is_read',0)->where('to',$id)->count();
$pendingleavs = DB::table("leaves")->where('user_notification_status',0)->where('approve_status','!=','pending')->orderBy('leaves.id','desc')->where('employee_fk_id',$id)->get();
// echo "<pre>";
// print_r($pendingleavs);
// die;;
$countleave = count($pendingleavs);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>Jag Joyu</title>
    <!-- Meta Tags -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="utf-8">
    <meta name="keywords" content="Jag Joyu" />
    <link rel="icon" type="image/png" href="<?php echo asset('images').'/'.'favicon.png'?>" />

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <script>
    addEventListener("load", function() {
        setTimeout(hideURLbar, 0);
    }, false);

    function hideURLbar() {
        window.scrollTo(0, 1);
    }
    </script>
    <style>
    .fc-sun {
        color: red;
    }
    </style>
    <link href="{{ asset('admin/css/bootstrap.css') }} " rel="stylesheet" type="text/css" media="all" />
    <link href="{{ asset('admin/css/style.css') }}" rel="stylesheet" type="text/css" media="all" />
    <link rel="stylesheet" href="{{ asset('admin/css/style4.css') }}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="{{ asset('admin/css/fontawesome-all.css') }}" rel="stylesheet" type='text/css'>
    <link href="https://use.fontawesome.com/releases/v5.0.4/css/all.css" rel="stylesheet">
   <!--web-fonts-->
   <link href="//fonts.googleapis.com/css?family=Poiret+One" rel="stylesheet">
   <link href="//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
   <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
   <script src="http://fullcalendar.io/js/fullcalendar-2.1.1/lib/jquery-ui.custom.min.js"></script>

   <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
   <script src="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.js"></script>
   <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.css" />
   <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" />
    <style>
        #sidebar {

            background: #083672;

        }

        #sidebar ul li.active>a,
        #sidebar a[aria-expanded="true"] {
            color: #fff;
            background: #083672;
        }

        #sidebar .sidebar-header {
            padding: 20px;
            background: #ccc5c5 !important;
        }

        ul ul a {
            font-size: 0.9em !important;
            padding-left: 30px !important;
            background: #083672;
        }

        #sidebar ul li a:hover {
            color: #083672;
            background: #fff;
        }

        .btn-primary,
        .btn-primary:hover,
        .btn-primary:active,
        .btn-primary:visited {
            background-color: #083672 !important;
            border-color: #083672 !important;
        }

        .btn-success,
        .btn-success:hover,
        .btn-success:active,
        .btn-success:visited {
            background-color: #083672 !important;
            border-color: #083672 !important;
        }

        .btn-info,
        .btn-info:hover,
        .btn-info:active,
        .btn-info:visited {
            background-color: #f3941e !important;
            border-color: #f3941e !important;
        }

        .fa-info-circle,
        .fa-edit {
            color: #083672;
        }

        ul.top-icons-agileits-w3layouts a.nav-link i {
            display: inline-block;
            font-size: 17px;
            color: #083672;
        }
        /* Chrome, Safari, Edge, Opera */
        input::-webkit-outer-spin-button,
        input::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
        }

        /* Firefox */
        input[type=number] {
        -moz-appearance: textfield;
        }
    </style>
    @stack('css')
    <link rel="stylesheet" href="{{url('vendor/mckenziearts/laravel-notify/css/notify.css')}}" />
    {{-- @notifyCss --}}
    <!--//web-fonts-->
</head>

<body>
    <div class="wrapper">
        <!-- Sidebar Holder -->
        <nav id="sidebar">
            <div class="sidebar-header">
                <h1>
                    <a href="{{url('empdashboard')}}"><img src="{{url('/images/jj-side-menu-logo.png')}}" alt=""></a>
                </h1>
                <span>J</span>
            </div>
            <!--             <div class="profile-bg"></div>-->
            <ul class="list-unstyled components">
                <li>
                    <a href="{{url('empdashboard')}}">
                        <i class="fas fa-th-large"></i>
                        Dashboard
                    </a>
                </li>
                 {{-- @if(App\Helpers\Adminhelper::setting()->askforcall_user_id==Auth::user()->id) --}}
                 @if(Auth::user())
                <li >
                    <a href="{{url("/admin-askforcall")}}" >
                        <i class="fa fa-users"></i>
                        Ask for calls
                       
                    </a>
                   
                </li>
                @endif 
                <li>
                    <a href="{{url('/leads')}}"> 
                    <i class="fa fa-users"></i>
                    Leads</a>
                </li>

                <li>
                    <a href="{{url('/employee-tours')}}"> 
                    <i class="fa fa-users"></i>
                    Tours</a>
                    </li>
                    <li class="{{ request()->segment(1) == 'member-plans' ? 'active' : '' }}" aria-expanded="{{ request()->segment(1) == 'member-plans' ? 'true' : '' }}">
                        <a href="#member-plans" data-toggle="collapse" aria-expanded="{{ request()->segment(1) == 'member-plans' ? 'true' : '' }}">
                            <i class="fa fa-users"></i>
                            Membership Plan
                            <i class="fas fa-angle-down fa-pull-right"></i>
                        </a>
                        <ul class="collapse {{ request()->segment(1) == 'member-plans' ? 'show' : '' }} list-unstyled" id="member-plans">
                            <li>
                                <!-- <a href="/employeemembershippackage">Show Plans</a> -->
                                <a href='{{url("/member-plans")}}'>Plans</a>
                            </li>
                            <li>
                                <!-- <a href="/employeemembershippackage">Show Plans</a> -->
                                <a href='{{url("/member-plans/invited_members")}}'>Invited members</a>
                            </li>
                       
                            
                        </ul>
                    </li> 
               

                {{-- <li class="active">
                    <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false">
                        <i class="fa fa-user"></i>
                        Inquiry
                        <i class="fas fa-angle-down fa-pull-right"></i>
                    </a>
                    <ul class="collapse list-unstyled" id="homeSubmenu">

                        <li>
                            <a href="{{ url('allempinquiry') }}">Show Inquiry</a>
                        </li>
                    </ul> </li>--}}

                {{-- <li>
                    <a href="#tours" data-toggle="collapse" aria-expanded="false">
                        <i class="fa fa-users"></i>
                        Membership Plan
                        <i class="fas fa-angle-down fa-pull-right"></i>
                    </a>
                    <ul class="collapse list-unstyled" id="tours">
                        <li>
                            <a href="/employeemembershippackage">Show Plans</a>
                        </li>


                    </ul>
                </li> --}}

                <li>
                    <a href="{{url('ticket-receive')}}">
                        <i class="fas fa-th-large"></i>
                        Receive Inquiry 
                    </a>
                </li>
               

                <li>
                    <a href="{{url('employee-ticket-generator')}}">
                        <i class="fas fa-th-large"></i>
                        Inquiry Generator
                    </a>
                </li>



                <!--                 <li>
                    <a href="#tplans" data-toggle="collapse" aria-expanded="false">
                        <i class="fas fa-table"></i>
                        Tours Packages 
                        
                        <i class="fas fa-angle-down fa-pull-right"></i>
                    </a>
                    <ul class="collapse list-unstyled" id="tplans">
                        <li>
                        
                            <a href="{{ url('showpackages')}}">Show Packages</a>
                        </li>
                        <li>
                            <a href="{{url('showtours')}}">Show Tours</a>
                        </li>
                        
                    </ul>
                </li> -->
                <li>
                    <a href="#tleaves" data-toggle="collapse" aria-expanded="false">
                        <i class="fas fa-table"></i>
                        Leaves

                        <i class="fas fa-angle-down fa-pull-right"></i>
                    </a>
                    <ul class="collapse list-unstyled" id="tleaves">
                        <li>

                            <a href="{{ url('empleaves_info')}}">Show Calender</a>
                        </li>
                        <li>
                            <a href="{{url('employeeLeaveForm')}}">Add Leaves</a>
                        </li>
                        <li>
                            <a href="{{ url('Emp_LeaveReport') }}">Leaves Report</a>
                        </li>
                        <!-- <li>
                            <a href="{{--url('showLeave')--}}">Show Leaves</a>
                        </li>
                        <li>
                            <a href="{{--url('totalLeave')--}}">Total Leaves</a>
                        </li> -->

                    </ul>
                </li>
                
                <!-- <li>
                    <a>
                        <i class="fa fa-info-circle"></i>
                        Orders
                    </a>
                </li> -->
                {{-- <li>
                    <a href="{{url('empchat')}}">
                        <i class="fa fa-comments"></i>
                        Messenger
                        <span class="badge badge-danger" id='leavecount'><?php echo $count?></span>
                    </a>

                </li> --}}



                <!-- <li>
                    <a href="{{--url('showbannerimage')--}}">
                        <i class="fas fa-clone"></i>
                        Banner
                    </a>
                </li>
                -->

                <!-- <li>
                    <a href="{{url('allrequest')}}">
                        <i class="fas fa-clone"></i>
                        Request Managment
                    </a>
                </li> -->
                <li>
                    <a href="#reports" data-toggle="collapse" aria-expanded="false">
                        <i class="fas fa-table"></i>
                        Reports

                        <i class="fas fa-angle-down fa-pull-right"></i>
                    </a>
                    <ul class="collapse list-unstyled" id="reports">

                        <li>
                            <a href="/employeesalaryselect">Salary</a>
                        </li>
                        <!--                         <li>
                            <a href="/empleaveshow">Leaves</a>
                        </li> -->
                        <li>
                            <a href="/employeetarget">Target</a>
                        </li>

                        <li>
                            <a href="/employeeleavereport">Leaves</a>
                        </li>

                    </ul>
                </li>

                <li>
                    <?php  $id = Session::get('eid');?>
                    <a href="{{url('/Yourprofile').'/'.base64_encode($id)}}">
                        <i class="fas fa-user"></i>Profile
                    </a>
                </li>

                


               
            </ul>
        </nav>

        <!-- Page Content Holder -->
        <div id="content">
            <!-- top-bar -->
            <nav class="navbar navbar-default mb-xl-5 mb-4">
                <div class="container-fluid">

                    <div class="navbar-header">
                        <button type="button" id="sidebarCollapse" class="btn btn-dark navbar-btn">
                            <i class="fas fa-bars"></i>
                        </button>
                    </div>
                    <ul class="top-icons-agileits-w3layouts float-right">
                      
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="far fa fa-envelope"></i>
                                <span class="badge" id='notification_msgcounts'>0</span>
                            </a>
                            <div class="dropdown-menu top-grid-scroll drop-1" style='position: absolute;
                                top: 100%;
                                left: -12rem;
                                width:20rem;
                                word-wrap: break-word;
                                z-index: 1000;
                                display: none;
                                float: left;
                                min-width: 10rem;
                                max-height: 350px;
                                overflow-y:auto;
                                padding: 0.5rem 0;
                                margin: 0.125rem 0 0;
                                font-size: 1rem;
                                color: #212529;
                                text-align: left;
                                list-style: none;
                                background-color: #fff;
                                background-clip: padding-box;
                                border: 1px solid rgba(0, 0, 0, 0.15);
                                border-radius: 0.25rem;' id='my-ticket-data'>
                                
                                
                                
                                
                               
                               
                              
                                <!-- /************ Close 1 Notification ************ */ -->
                                {{-- <a class="dropdown-item" href="{{url('allemprequest')}}">View all Notifications</a> --}}
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="far fa fa-user"></i>
                                <span class="badge" id='leavecount1'><?php echo $countleave?></span>
                            </a>
                            <div class="dropdown-menu top-grid-scroll drop-1" style='position: absolute;
                                top: 100%;
                                left: -12rem;
                                width:20rem;
                                word-wrap: break-word;
                                z-index: 1000;
                                display: none;
                                float: left;
                                min-width: 10rem;
                                max-height: 350px;
                                overflow-y:auto;
                                padding: 0.5rem 0;
                                margin: 0.125rem 0 0;
                                font-size: 1rem;
                                color: #212529;
                                text-align: left;
                                list-style: none;
                                background-color: #fff;
                                background-clip: padding-box;
                                border: 1px solid rgba(0, 0, 0, 0.15);
                                border-radius: 0.25rem;' id='appendleave'>
                                <h3 class="sub-title-w3-agileits"></h3>
                                <div id="parent-div">
                                </div>
                                <?php $i=1; foreach($pendingleavs as $value){
                                if($i<=3){?>
                                <a href="#" class="dropdown-item">
                                    <!-- <div class="notif-img-agileinfo">
                                       
                                    </div> -->
                                    <div class="notif-content-wthree">
                                        <span class="text-diff"><?php //echo $value->name?>
                                            <?php echo "Your Request For ". $value->leave_type." "."<br>".$value->leave_date_start." To ". $value->leave_date_end .$value->approve_status ."<br>","By jag Joyu"?>
                                        </span>
                                        <br>


                                        <buttongroup>
                                            <a href="{{ route('EmpLeaveResult',[$value->id, 'Approve']) }}"
                                                style='margin-left:1rem'>
                                                <button class='btn btn-success'>Ok</button></a>



                                        </buttongroup>
                                    </div>
                                    <!-- <div class="dropdown-divider"></div> -->

                                </a>
                                <?php } $i++; } ?>
                                <!-- /************ Close 1 Notification ************ */ -->
                                <a class="dropdown-item" href="{{url('allemprequest')}}">View all Notifications</a>
                            </div>
                        </li>



                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown2" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="far fa-user"></i>
                            </a>
                            <div class="dropdown-menu drop-3" style='position: absolute;
                            top: 100%;
                            left: -12rem;
                            width:15rem;
                            word-wrap: break-word;
                            z-index: 1000;
                            display: none;
                            float: left;
                            min-width: 6rem;
                            padding: 0.5rem 0;
                            margin: 0.125rem 0 0;
                            font-size: 1rem;
                            color: #212529;
                            text-align: left;
                            list-style: none;
                            background-color: #fff;
                            background-clip: padding-box;
                            border: 1px solid rgba(0, 0, 0, 0.15);
                            border-radius: 0.25rem;'>
                                <div class="profile d-flex mr-o">
                                    <!-- <div class="profile-l align-self-center">
                                        <img src="{{ asset('admin/images/profile.jpg') }}" class="img-fluid mb-3" alt="Responsive image">
                                    </div> -->
                                    <!-- profile-r align-self-center -->
                                    <div class="profile-r align-self-center">
                                        <center>
                                            <h5 class="dropdown-item mt-3">
                                                {{ Session::get('emp_name') }}

                                            </h5>
                                        </center>

                                    </div>
                                </div>
                                <!-- <a href="{{url('getAdminProfile')}}" class="dropdown-item mt-3">
                                    <h4>
                                        <i class="far fa-user mr-3"></i>Change Profile</h4>
                                </a> -->
                                <a href="{{url('empchangepasswordform')}}" class="dropdown-item mt-3">
                                    <h4>
                                        <i class="far fa-user mr-3"></i>Change Password
                                    </h4>
                                </a>
                                <!-- <a href="#" class="dropdown-item mt-3">
                                    <h4>
                                        <i class="fas fa-link mr-3"></i>Activity</h4>
                                </a>
                                <a href="#" class="dropdown-item mt-3">
                                    <h4>
                                        <i class="far fa-envelope mr-3"></i>Messages</h4>
                                </a>
                                <a href="#" class="dropdown-item mt-3">
                                    <h4>
                                        <i class="far fa-question-circle mr-3"></i>Faq</h4>
                                </a>
                                <a href="#" class="dropdown-item mt-3">
                                    <h4>
                                        <i class="far fa-thumbs-up mr-3"></i>Support</h4>
                                </a> -->
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="{{url('emplogout')}}">Logout</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>

            @yield('bodyData')
            <x:notify-messages />
            <div class="copyright-w3layouts py-xl-3 py-2 mt-xl-5 mt-4 text-center">
                <p>© {{ date('Y') }} All Rights Reserved | Design by Jag Joyu

                </p>
            </div>
           
         
            <!-- Required common Js -->
            <!-- <script src="{{ asset('admin/js/jquery-2.2.3.min.js') }} " ></script> -->
            <!-- <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.js"></script>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.css"/> -->
            <!-- //Required common Js -->

            <!-- loading-gif Js -->
            <script src="{{ url('vendor/mckenziearts/laravel-notify/js/notify.js') }} "></script>
            <script src="{{ asset('admin/js/modernizr.js') }} "></script>
            <script>
            //paste this code under head tag or in a seperate js file.
            // Wait for window load
            $(window).load(function() {
                // Animate loader off screen    
                $(".se-pre-con").fadeOut("slow");;
            });
            </script>
            <!--// loading-gif Js -->

            <!-- Sidebar-nav Js -->
            <script>
            $(document).ready(function() {
                $('#sidebarCollapse').on('click', function() {
                    $('#sidebar').toggleClass('active');
                });
            });
            </script>
            <!--// Sidebar-nav Js -->

            <!-- Graph -->
            {{-- <script src="{{ asset('admin/js/SimpleChart.js') }} "></script> --}}
            {{-- <script>
            var graphdata4 = {
                linecolor: "Random",
                title: "Thursday",
                values: [{
                        X: "6",
                        Y: 300.00
                    },
                    {
                        X: "7",
                        Y: 101.98
                    },
                    {
                        X: "8",
                        Y: 140.00
                    },
                    {
                        X: "9",
                        Y: 340.00
                    },
                    {
                        X: "10",
                        Y: 470.25
                    },
                    {
                        X: "11",
                        Y: 180.56
                    },
                    {
                        X: "12",
                        Y: 680.57
                    },
                    {
                        X: "13",
                        Y: 740.00
                    },
                    {
                        X: "14",
                        Y: 800.89
                    },
                    {
                        X: "15",
                        Y: 420.57
                    },
                    {
                        X: "16",
                        Y: 980.24
                    },
                    {
                        X: "17",
                        Y: 1080.00
                    },
                    {
                        X: "18",
                        Y: 140.24
                    },
                    {
                        X: "19",
                        Y: 140.58
                    },
                    {
                        X: "20",
                        Y: 110.54
                    },
                    {
                        X: "21",
                        Y: 480.00
                    },
                    {
                        X: "22",
                        Y: 580.00
                    },
                    {
                        X: "23",
                        Y: 340.89
                    },
                    {
                        X: "0",
                        Y: 100.26
                    },
                    {
                        X: "1",
                        Y: 1480.89
                    },
                    {
                        X: "2",
                        Y: 1380.87
                    },
                    {
                        X: "3",
                        Y: 1640.00
                    },
                    {
                        X: "4",
                        Y: 1700.00
                    }
                ]
            };
            $(function() {
                $("#Hybridgraph").SimpleChart({
                    ChartType: "Hybrid",
                    toolwidth: "50",
                    toolheight: "25",
                    axiscolor: "#E6E6E6",
                    textcolor: "#6E6E6E",
                    showlegends: false,
                    data: [graphdata4],
                    legendsize: "140",
                    legendposition: 'bottom',
                    xaxislabel: 'Hours',
                    title: 'Weekly Profit',
                    yaxislabel: 'Profit in $'
                });
            });
            </script> --}}
            <!--// Graph -->
            <!-- Bar-chart -->
            <script src="{{ asset('admin/js/rumcaJS.js') }}"></script>
            <script src="{{ asset('admin/js/example.js') }}"></script>
            <!--// Bar-chart -->
            <!-- Calender -->
            <script src="{{ asset('admin/js/moment.min.js') }}"></script>
            <script src="{{ asset('admin/js/pignose.calender.js') }}"></script>
            <script>
            //<![CDATA[
            $(function() {
                $('.calender').pignoseCalender({
                    select: function(date, obj) {
                        obj.calender.parent().next().show().text('You selected ' +
                            (date[0] === null ? 'null' : date[0].format('YYYY-MM-DD')) +
                            '.');
                    }
                });

                $('.multi-select-calender').pignoseCalender({
                    multiple: true,
                    select: function(date, obj) {
                        obj.calender.parent().next().show().text('You selected ' +
                            (date[0] === null ? 'null' : date[0].format('YYYY-MM-DD')) +
                            '~' +
                            (date[1] === null ? 'null' : date[1].format('YYYY-MM-DD')) +
                            '.');
                    }
                });
            });
            //]]>
            </script>
            <!--// Calender -->

            <!-- profile-widget-dropdown js-->
            <script src="{{ asset('admin/js/script.js')}}"></script>
            <!--// profile-widget-dropdown js-->

            <!-- Count-down -->
            {{-- <script src="{{ asset('admin/js/simplyCountdown.js')}}"></script>
            <link href="{{ asset('admin/css/simplyCountdown.css')}}" rel='stylesheet' type='text/css' />
            <script>
            var d = new Date();
            simplyCountdown('simply-countdown-custom', {
                year: d.getFullYear(),
                month: d.getMonth() + 2,
                day: 25
            });
            </script> --}}
            <!--// Count-down -->

            <!-- pie-chart -->
            <!-- <script src='{{-- asset("admin/js/amcharts.js") --}}'></script>
    <script>
        var chart;
        var legend;

        var chartData = [{
                country: "Lithuania",
                value: 260
            },
            {
                country: "Ireland",
                value: 201
            },
            {
                country: "Germany",
                value: 65
            },
            {
                country: "Australia",
                value: 39
            },
            {
                country: "UK",
                value: 19
            },
            {
                country: "Latvia",
                value: 10
            }
        ];

        AmCharts.ready(function () {
            // PIE CHART
            chart = new AmCharts.AmPieChart();
            chart.dataProvider = chartData;
            chart.titleField = "country";
            chart.valueField = "value";
            chart.outlineColor = "";
            chart.outlineAlpha = 0.8;
            chart.outlineThickness = 2;
            // this makes the chart 3D
            chart.depth3D = 20;
            chart.angle = 30;

            // WRITE
            chart.write("chartdiv");
        });
    </script> -->
            <!--// pie-chart -->

            <!-- dropdown nav -->
            <script>
            $(document).ready(function() {
                $(".dropdown").hover(
                    function() {
                        $('.dropdown-menu', this).stop(true, true).slideDown("fast");
                        $(this).toggleClass('open');
                    },
                    function() {
                        $('.dropdown-menu', this).stop(true, true).slideUp("fast");
                        $(this).toggleClass('open');
                    }
                );
            });
            </script>
            <!-- //dropdown nav -->

            <!-- Js for bootstrap working-->
            <script src="{{ asset('admin/js/bootstrap.min.js') }} "></script>

            <!-- //Js for bootstrap working -->
            {{-- <script src="https://js.pusher.com/5.0/pusher.min.js"></script> --}}

            {{-- <script>
            var receiver_id = '';
            var my_id = "{{ Auth::id() }}";
            $(document).ready(function() {
                get_my_ticket();
                // ajax setup form csrf token
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                // Enable pusher logging - don't include this in production
                Pusher.logToConsole = true;

                var pusher = new Pusher('{{env("MIX_PUSHER_APP_KEY")}}', {
                    cluster: 'ap2',
                    forceTLS: true
                });

                var channel = pusher.subscribe('my-channel');
                channel.bind('my-event', function(data) {
                    // lo(JSON.stringify(data));
                    if (my_id == data.from) {
                        $('#' + data.to).click();
                    } else if (my_id == data.to) {
                        if (receiver_id == data.from) {
                            // if receiver is selected, reload the selected user ...
                            $('#' + data.from).click();
                        } else {
                            // if receiver is not seleted, add notification for that user
                            var pending = parseInt($('#' + data.from).find('.pending').html());

                            if (pending) {
                                $('#' + data.from).find('.pending').html(pending + 1);
                            } else {
                                $('#' + data.from).append('<span class="pending">1</span>');
                            }
                        }
                    }
                });

                $('.user').click(function() {
                    $('.user').removeClass('active');
                    $(this).addClass('active');
                    $(this).find('.pending').remove();

                    receiver_id = $(this).attr('id');
                    $.ajax({
                        type: "get",
                        url: "message/" + receiver_id, // need to create this route
                        data: "",
                        cache: false,
                        success: function(data) {
                            $('#messages').html(data);
                            scrollToBottomFunc();
                        }
                    });
                });

                $(document).on('keyup', '.input-text input', function(e) {
                    var message = $(this).val();

                    // check if enter key is pressed and message is not null also receiver is selected
                    if (e.keyCode == 13 && message != '' && receiver_id != '') {
                        $(this).val(''); // while pressed enter text box will be empty

                        var datastr = "receiver_id=" + receiver_id + "&message=" + message;
                        $.ajax({
                            type: "post",
                            url: "message", // need to create this post route
                            data: datastr,
                            cache: false,
                            success: function(data) {

                            },
                            error: function(jqXHR, status, err) {},
                            complete: function() {
                                scrollToBottomFunc();
                            }
                        })
                    }
                });
            });

            // make a function to scroll down auto
            function scrollToBottomFunc() {
                $('.message-wrapper').animate({
                    scrollTop: $('.message-wrapper').get(0).scrollHeight
                }, 50);
            }
            </script>

            <script>
            var pusher = new Pusher('{{env("MIX_PUSHER_APP_KEY")}}', {
                cluster: '{{env("PUSHER_APP_CLUSTER")}}',
                encrypted: true
            });
            var channel = pusher.subscribe('message-channel');
            channel.bind('App\\Events\\Message', function(data) {

                if (data.to == "<?php //echo $id?>") {
                    document.getElementById('leavecount').innerHTML = data.counts;
                }
            });
            </script>

            <script>
            var pusher = new Pusher('{{env("MIX_PUSHER_APP_KEY")}}', {
                cluster: '{{env("PUSHER_APP_CLUSTER")}}',
                encrypted: true
            });

            var channel = pusher.subscribe('approve-channel');
            channel.bind('App\\Events\\Approve', function(data) {
                // alert("jrot");
                if (data.uid == "<?php //echo $id?>") {
                    document.getElementById('leavecount1').innerHTML = data.count;
                }
            });
            </script>
            --}}
            
            
            <script>
                 $(document).ready(function() {
                get_my_ticket();
                });
                function get_my_ticket() {
                
                    $.get("{{ url('/employee-ticket-generator/getmsg') }}/", function(data, status) {
                     console.log(JSON.parse(data));
                    var ticket = JSON.parse(data);
                    var count_one=0;    
                    var count_two=0;    
                    var $html = '';
                    $('#my-ticket-data').html('');
                    if(ticket){    
                    if(ticket.receive_message!==null){
                        count_one=ticket.receive_message_count;
                        $.each(ticket.receive_message, function(key, value) {
                            if(value!=null){
                                $html +='<a href="{{url('employee-ticket-generator/view')}}/'+btoa(value.ticket_id)+'" class="dropdown-item">'+
                                '<p class="sub-title-w2-agileits"><b>#'+value.ticket_id+'</b></p>'+
                                '<div class="notif-content-wthree">'+
                                '<span class="text-diff">'+value.message+'</span>'+
                                '<br><p><span class="text-diff">'+
                                moment(value.created_at).format('h:mm a')+'</span></p>'+
                                '</div></a>';
                            }
                            
                        });
                        
    
                    }
    
    
                    if(ticket.send_message!==null){
    
                        $.each(ticket.send_message, function(key, value) {
                            if(value!=null){
                                $html +='<a href="{{url('ticket-receive/view')}}/'+btoa(value.ticket_id)+'" class="dropdown-item">'+
                                '<p class="sub-title-w2-agileits"><b>#'+value.ticket_id+'</b></p>'+
                                '<div class="notif-content-wthree">'+
                                '<span class="text-diff">'+value.message+'</span>'+
                                '<br><p><span class="text-diff">'+moment(value.created_at).format(
                            'h:mm a')+'</span></p>'+
                                '</div></a>';
                            }
                            
                        });
                        count_two=ticket.send_message_count;
    
                    }
                    }
                    var totalinf=count_two+count_one;
                    $('#notification_msgcounts').text(totalinf);  
                    $('#my-ticket-data').html($html);
                   
                });
                // $('.message-body').animate({scrollTop: 100}, 500);
            }
    
    
                </script>
            @stack('js')


<script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
{{-- <script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script> --}}
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>


</body>

</html>