@extends('admin.master')
@section('bodyData')
    <section class="forms-section">
        <div class="outer-w3-agile mt-3">
            <h4 class="tittle-w3-agileits mb-4">Employee Id Creation</h4>
            <form action="#" method="post">
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="inputEmail4">First Name</label>
                        <input type="text" class="form-control" id="inputEmail4" placeholder="Enter Name" required=""
                            pattern="[A-Za-z]{3,40}">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="inputPassword4">Last Name</label>
                        <input type="password" class="form-control" id="inputPassword4" placeholder="Last Name" required=""
                            pattern="[A-Za-z]{3,40}">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="inputEmail4">Contact Number</label>
                        <input type="text" class="form-control" id="contactnumber" placeholder="Enter Contact Number"
                            required="">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="inputPassword4">Alternate Number</label>
                        <input type="text" class="form-control" id="alternatecontactnumber"
                            placeholder="Alternate Contact Number" required="">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="inputPassword4">WhatsApp Number</label>
                        <input type="text" class="form-control" id="wanumber" placeholder="Enter WhatsApp  Number"
                            required="">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="inputEmail4">Email</label>
                        <input type="email" class="form-control" id="inputEmail4" placeholder="Email" required="">
                    </div>

                </div>

                <div class="form-row">
                    <div class="form-group col-md-2">
                        <label for="inputPassword4">House Number</label>
                        <input type="text" class="form-control" id="wanumber" placeholder="Enter House  Number" required="">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="inputEmail4">Society Name</label>
                        <input type="text" class="form-control" id="inputEmail4" placeholder="Society Name" required="">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="inputEmail4">Landmark</label>
                        <input type="text" class="form-control" id="inputEmail4" placeholder="Landmark Name" required="">
                    </div>

                </div>

                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="inputCity">City</label>

                        <select id="inputState" class="form-control">
                            <option selected="">Select City</option>
                            <option>Surat</option>
                            <option>Ahmedabad</option>
                            <option>Rajkot</option>
                        </select>

                    </div>
                    <div class="form-group col-md-4">
                        <label for="inputState">State</label>
                        <select id="inputState" class="form-control">
                            <option selected="">Select state</option>
                            <option>Gujarat</option>
                            <option>Mp</option>

                        </select>
                    </div>
                    <div class="form-group col-md-2">
                        <label for="inputZip">Pin Code</label>
                        <input type="text" class="form-control" id="inputZip" required="">
                    </div>
                </div>
                <script>
                    function changeFunc() {
                        var d = $('#know').val();
                        if (d == 'eref') {
                            $('#erefblock').show();
                        }
                    }

                </script>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="inputPassword4">From where did you get to know about JagJoyu’s ?</label>
                        <select id="know" class="form-control" onchange="changeFunc();">
                            <option selected="">Select Option</option>
                            <option value="facebook">Facebook</option>
                            <option value="
                                          Linkedin">Linkedin</option>
                            <option value="
                                          Twitter">Twitter</option>

                            <option value="
                                          Instagram">Instagram</option>
                            <option value="
                                          Email">Email</option>
                            <option value="
                                Member">JagJoyu’s Member</option>
                            <option value="eref">Employee Referral</option>
                            <option value="
                                          Walkin">Walkin</option>

                        </select>

                    </div>

                    <div class="form-group col-md-6" style="display:none" id="erefblock">
                        <label for="inputEmail4" id="reflab">Enter Ref Id</label>
                        <input name="ref" placeholder="Add Ref Id" class="form-control" type="text" id="ref">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="inputEmail4">Photo</label>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="inputGroupFile03">
                            <label class="custom-file-label" for="inputGroupFile03">Choose file</label>
                        </div>

                    </div>
                    <div class="form-group col-md-6">
                        <label for="inputPassword4">Documents Upload</label>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="inputGroupFile03">
                            <label class="custom-file-label" for="inputGroupFile03">Choose file</label>
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="inputCity">Salary</label>
                        <input name="salary" placeholder="Salary " class="form-control" type="text" id="ref">



                    </div>
                    <div class="form-group col-md-4">
                        <label for="inputState">Variable Pay</label>
                        <input name="salary" placeholder="Variable pay" class="form-control" type="text" id="ref">

                    </div>
                    <div class="form-group col-md-2">
                        <label for="inputZip">Retention Bonus</label>
                        <input type="text" class="form-control" id="inputZip" required="" placeholder="Retention Bonus">
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="inputCity">PF</label>
                        <input name="salary" placeholder="PF " class="form-control" type="text" id="ref">



                    </div>
                    <div class="form-group col-md-4">
                        <label for="inputState">Gratuity</label>
                        <input name="salary" placeholder="Gratuity" class="form-control" type="text" id="ref">

                    </div>
                    <div class="form-group col-md-2">
                        <label for="inputZip">Professional Tax</label>
                        <input type="text" class="form-control" id="inputZip" required="" placeholder="Professional Tax">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="inputCity">Incentive</label>
                        <input name="salary" placeholder="Incentive " class="form-control" type="text" id="ref">



                    </div>
                    <div class="form-group col-md-2">
                        <label for="inputState">Leaves</label>
                        <input name="salary" placeholder="Leaves" class="form-control" type="text" id="ref">

                    </div>
                    <div class="form-group col-md-4">
                        <label for="inputZip">Joining Date</label>
                        <input type="date" name="jdate" class="form-control">
                    </div>
                </div>



                <button type="submit" class="btn btn-primary">Sign in</button>
            </form>
        </div>

    </section>

@endsection
