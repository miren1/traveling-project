@extends('admin.master')
@section('bodyData')
<section class="forms-section" style='min-height:500px'>
<div class="outer-w3-agile mt-3">
        <h4 class="tittle-w3-agileits mb-4">Employee Id Creation</h4>
        <form action="{{url('createEmployee')}}" method="post">
        @csrf
      <div class="form-row">
               
                <div class="form-group col-md-6">
                    <label for="inputEmail4">Email</label>
                    <input type="email" class="form-control" id="inputEmail4" placeholder="Enter Employee's Email Address" required="" name='email'>
                </div>
           </div>
            <button type="submit" class="btn btn-primary">Sent Mail</button>
        </form>
    </div>

</section>

@endsection
