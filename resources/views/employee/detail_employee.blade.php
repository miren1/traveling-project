@extends('employee.master')
@section('bodyData')
<div class="container-fluid">
  <div class="row">
    <div class="col-sm-12">
      <ul class="nav nav-tabs">
       <li class="nav-item">
        <a class="nav-link active" data-toggle='tab' href="#profile">Profile</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-toggle='tab' href="#salary">Address</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-toggle='tab' href="#Documents">Documents</a>

      </li>
      <li class="nav-item">
        <a class="nav-link" data-toggle='tab' href="#family">Family Detail</a>

      </li>
      
      <li class="nav-item">
        <a class="nav-link" data-toggle='tab' href="#emp">Employment Details</a>
      </li>
    </ul>
    
      <div class="tab-content">
        <div id="salary" class="container tab-pane">
          <div class="row">
            <div class="col-sm-6">
              <br>
              @if(isset($employeeProfile) && isset($p_cityname) && isset($p_statename) && isset($c_cityname) && isset($c_statename))
                <ul class="list-group list-group-flush">
                 <li class="list-group-item"><b>House No:</b><?php echo $employeeProfile->c_house_no?></li>
                 <li class="list-group-item"><b>Society Name: </b><?php echo $employeeProfile->c_society_name?></li>
                 <li class="list-group-item"><b>Land Mark: </b><?php echo $employeeProfile->c_landmark?></li>
                 <li class="list-group-item"><b>City:  </b><?php echo $c_cityname->name?></li>
                 <li class="list-group-item"><b>State: </b><?php echo $c_statename->name?></li>
                 <li class="list-group-item"><b>Permenant House Number: </b><?php echo $employeeProfile->p_house_no?></li>
                 <li class="list-group-item"><b>Permenant Society_name: </b><?php echo $employeeProfile->p_society_name?></li>
                 <li class="list-group-item"><b>Permenant Landmark: </b><?php echo $employeeProfile->p_landmark?></li>
                 <li class="list-group-item"><b>Permenant City: </b><?php echo $p_cityname->name?></li>
                 <li class="list-group-item"><b>Permenant State: </b><?php echo $p_statename->name?></li>
                 <li class="list-group-item"><b>Permenant Pincode: <?php echo $employeeProfile->p_pincode?></b></li>
               </ul>
              @else
                <div>No data filled</div>
              @endif
            </div>
          </div>
        </div>

        <div id="profile" class="container tab-pane active">
          <div class="row">
            <div class="col-sm-8">
              <br>
              @if($employeeProfile)
                <ul class="list-group">
                 <li class="list-group-item"><b>Employee Photo: </b><img style='height:100px;width:100px' src='{{"/avatar/".$employeeProfile->avatar}}'?> </li>
                 <li class="list-group-item"><b>Employee Id: </b><?php echo "JJ-EMP-".$employeeProfile->id?></li>
                 <li class="list-group-item"><b>Prefix: </b><?php echo $employeeProfile->prefix?></li>
                 <li class="list-group-item"><b>Designation: </b><?php echo $employeeProfile->designation?></li>
                 <li class="list-group-item"><b>Name: </b><?php echo $employeeProfile->name?> <?php echo $employeeProfile->middlename?> <?php echo $employeeProfile->surname?></li>
                 {{-- <li class="list-group-item"><b>Middlename: </b><?php echo $employeeProfile->middlename?></li> --}}
                 <li class="list-group-item"><b>Contact Number: </b><?php echo $employeeProfile->contact_number?></li>
                 <li class="list-group-item"><b>Alternate Number: </b><?php echo $employeeProfile->contact_number_alt?></li>
                 <li class="list-group-item"><b>Whatsapp Number:</b><?php echo $employeeProfile->whatsup_number?></li>
                 <li class="list-group-item"><b>Email:</b><?php echo $employeeProfile->email?></li>

                 <li class="list-group-item"><b>Where did you get to know about vacancy:</b><?php echo $employeeProfile->know_jagjoyu?></li>
                 <li class="list-group-item"><b>Reference Name:</b><?php echo $employeeProfile->reference_name?></li>
                 <li class="list-group-item"><b>Employee Status:</b><?php if($employeeProfile->employee_approved == 0){echo "UnApprove";}
                 if($employeeProfile->employee_approved == 1) {echo "Approved";}if($employeeProfile->employee_approved == 2){echo "Rejected";}?></li>


                 <li class="list-group-item"><b>Salary:</b><?php echo $employeeProfile->salary?></li>
                 <li class="list-group-item"><b>Account Number:</b><?php echo $employeeProfile->acc_no?></li>
                 <li class="list-group-item"><b>IFSC Code:</b><?php echo $employeeProfile->ifsc_code?></li>
                 <li class="list-group-item"><b>Bank Name:</b><?php echo $employeeProfile->bank_name?></li>

               </ul>
              @else
                <div>No data filled</div>
              @endif
            </div>
          </div>
        </div>
        <!-- /*************Family Detail ***************** */ -->
        <div id="family" class="container tab-pane" style='min-height:400px'>
          <div class="row">
            <div class="col-sm-8">
              @if(isset($employeeProfile))
                <table class="table table-striped">
                  <thead>
                    <tr>
                      <th scope="col">Prefix</th>
                      <th scope="col">Name</th>
                      <th scope="col">Mobile</th>
                      <th scope="col">Email</th>
                      <th scope="col">Address</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                    if(isset($employeeProfile)){$result = $employeeProfile->family_details; }else{$result = [];}
                    $family_data = json_decode($result,true);
                    ?>
                    @if(isset($family_data))
                    <?php  foreach ($family_data as $value) {
                      ?>
                      <tr>

                       <td><?php echo $value['prefix'];?></td>
                       <td><?php echo $value['name'];?></td>
                       <td><?php echo $value['mobile'];?></td>
                       <td><?php echo $value['email'];?></td>
                       <td><?php echo $value['address'];?></td>

                     </tr>
                     <?php } ?>
                    @endif

                   </tbody>
                 </table>
              @else
                <div>No data filled</div>
              @endif
            </div>
          </div>	
        </div>
        <!-- /*******************EmployeementDetails********************* */ -->
        <div id="emp" class="container tab-pane" style='min-height:400px'>
          @if($employeeProfile)
            <div class="row">
              <div class="col-sm-8">
                <table class="table table-striped">
                  <thead>
                    <!-- [{"companyname":"Version Up","sdate":"2020-11-03","edate":"2020-11-21","designation":"PHP Developer","salary":"20000","reason":"Finance Level "}] -->
                    <tr>
                      <th scope="col">Companyname</th>
                      <th scope="col">Sdate</th>
                      <th scope="col">Edate</th>
                      <th scope="col">Designation</th>
                      <th scope="col">Salary</th>
                      <th scope="col">Reason</th>
                    </tr>
                  </thead>
                  @if(isset($employeeProfile))
                    <tbody>
                      <?php 
                      if(isset($employeeProfile)){$result = $employeeProfile->employeement_details;}else{
                        $result = [];
                      }
                      $employeement_details = json_decode($result,true);
                      ?>
                      @if(isset($employeement_details))
                      <?php  foreach ($employeement_details as $value) {
                        ?>
                        <tr>
                          <td><?php echo $value['companyname'];?></td>
                          <td><?php echo $value['sdate'];?></td>
                          <td><?php echo $value['edate'];?></td>
                          <td><?php echo $value['designation'];?></td>
                          <td><?php echo $value['salary'];?></td>
                          <td><?php echo $value['reason'];?></td>


                        </tr>
                        <?php } ?>
                      @endif
                      </tbody>
                    </table>
                  @else
                  <div>No data filled</div>
                  @endif
                </div>
            </div>
            <?php if($employeeProfile->send_employment == 1){

             ?>
             <a href="{{ route('AdeminEmpResult',[$employeeProfile->id,'Approve']) }}">
               <button class='btn btn-success'>Approve</button></a>
               <a href="{{ route('AdeminEmpResult',[$employeeProfile->id, 'Reject']) }}"><button class='btn btn-danger'>Reject</button></a>
               <?php } ?>
          @else
            <div>No data filled</div>
          @endif
        </div>

        <!-- ********************Documents ************************ -->

        <div id="Documents" class="container tab-pane" name='tab' style='min-height:400px'>
          @if($employeeProfile)
            <table class="table table-striped">
              <thead>
                <tr>
                  <th scope="col">Document</th>
                  <th scope="col">Id</th>
                  <th>Link</th>
                </tr>
              </thead>
             <tbody>
                      @if($employeeProfile->aadhar_card_pic)
                      <tr>
                        <td>Adhar Card</td>
                         <td><?php echo $employeeProfile->aadhar_card_id?></td>
                        <td><a target="_blank" href="{{url('/documentemp?id=')}}{{$employeeProfile->id}}&document=aadhar_card_pic"><button class="btn btn-primary btn-sm">Adhar Card</button></a></td>
                      </tr>
                      @endif
                       @if($employeeProfile->driving_photo)
                      <tr>
                        <td>driving Card</td>
                        <td><?php echo $employeeProfile->driving_id?></td>
                        <td><a target="_blank" href="{{url('/documentemp?id=')}}{{$employeeProfile->id}}&document=driving_photo"><button class="btn btn-primary btn-sm">driving Card</button></a></td>
                      </tr>
                      @endif
                      @if($employeeProfile->pancard_photo)
                      <tr>
                        <td>Pan Card</td>
                        <td><?php echo $employeeProfile->pancard_id?></td>
                        <td><a target="_blank" href="{{url('/documentemp?id=')}}{{$employeeProfile->id}}&document=pancard_photo"><button class="btn btn-primary btn-sm">Pan Card</button></a></td>
                      </tr>
                      @endif
                      @if($employeeProfile->passport_photo)
                      <tr>
                        <td>Passport</td>
                         <td><?php echo $employeeProfile->passport_id?></td>
                        <td><a target="_blank" href="{{url('/documentemp?id=')}}{{$employeeProfile->id}}&document=passport_photo"><button class="btn btn-primary btn-sm">Passport</button></a></td>
                      </tr>
                      @endif
                      @if($employeeProfile->marksheet_10th_photo)
                      <tr>
                        <td>10Th Marksheet</td>
                        <td></td>
                        <td><a target="_blank" href="{{url('/documentemp?id=')}}{{$employeeProfile->id}}&document=marksheet_10th_photo"><button class="btn btn-primary btn-sm">10Th Marksheet</button></a></td>
                      </tr>
                      @endif
                      @if($employeeProfile->marksheet_12th_photo)
                      <tr>
                        <td>12Th Marksheet</td>
                        <td></td>
                        <td><a target="_blank" href="{{url('/documentemp?id=')}}{{$employeeProfile->id}}&document=marksheet_12th_photo"><button class="btn btn-primary btn-sm">12Th Marksheet</button></a></td>
                      </tr>
                      @endif
                      @if($employeeProfile->marksheet_bachelors_photo)
                      <tr>
                        <td>Bachelors Degree Certificate</td>
                        <td></td>
                        <td><a target="_blank" href="{{url('/documentemp?id=')}}{{$employeeProfile->id}}&document=marksheet_bachelors_photo"><button class="btn btn-primary btn-sm">Bachelors Degree Certificate</button></a></td>
                      </tr>
                      @endif
                      @if($employeeProfile->marksheet_masters_photo)
                      <tr>
                        <td>Master Degree Certificate</td>
                        <td></td>
                        <td><a target="_blank" href="{{url('/documentemp?id=')}}{{$employeeProfile->id}}&document=marksheet_masters_photo"><button class="btn btn-primary btn-sm">Master Degree Certificate</button></a></td>
                      </tr>
                      @endif
                      @if($employeeProfile->appointment)
                      <tr>
                        <td>Appointment Letter</td>
                        <td></td>
                        <td><a target="_blank" href="{{url('/documentemp?id=')}}{{$employeeProfile->id}}&document=appointment"><button class="btn btn-primary btn-sm">Appointment Letter</button></a></td>
                      </tr>
                      @endif
                      @if($employeeProfile->offerLetter)
                      <tr>
                        <td>Offer Letter</td>
                        <td></td>
                        <td><a target="_blank" href="{{url('/documentemp?id=')}}{{$employeeProfile->id}}&document=appointment"><button class="btn btn-primary btn-sm">Offer Letter</button></a></td>
                      </tr>
                      @endif
                      @if($employeeProfile->experience)
                      <tr>
                        <td>Experience Letter</td>
                        <td></td>
                        <td><a target="_blank" href="{{url('/documentemp?id=')}}{{$employeeProfile->id}}&document=appointment"><button class="btn btn-primary btn-sm">Experience Letter</button></a></td>
                      </tr>
                      @endif
                      @if($employeeProfile->resignation)
                      <tr>
                        <td>Resignation Letter</td>
                        <td></td>
                        <td><a target="_blank" href="{{url('/documentemp?id=')}}{{$employeeProfile->id}}&document=appointment"><button class="btn btn-primary btn-sm">Resignation Letter</button></a></td>
                      </tr>
                      @endif
                      @if($employeeProfile->increment)
                      <tr>
                        <td>Last Increment Letter</td>
                        <td></td>
                        <td><a target="_blank" href="{{url('/documentemp?id=')}}{{$employeeProfile->id}}&document=appointment"><button class="btn btn-primary btn-sm">Last Increment Letter</button></a></td>
                      </tr>
                      @endif
                      @if($employeeProfile->statment)
                      <tr>
                        <td>Bank Statement</td>
                        <td></td>
                        <td><a target="_blank" href="{{url('/documentemp?id=')}}{{$employeeProfile->id}}&document=appointment"><button class="btn btn-primary btn-sm">Bank Statment</button></a></td>
                      </tr>
                      @endif
                      <?php if($employeeProfile->salaryslips){
                        $temp = explode(",",$employeeProfile->salaryslips);
                        $i=1;
                        foreach($temp as $t)
                         { ?>
                         <tr>
                          <td>Salary Slip{{$i}}</td>
                          <td></td>
                          <td><a target="_blank" href="{{url('/documentemp?id=')}}{{$employeeProfile->id}}&document=appointment"><button class="btn btn-primary btn-sm">Salary Slip{{$i}}</button></a></td>
                        </tr>
                          <?php $i++;} 
                        } ?>
                        <?php if($employeeProfile->any_other_certificates){
                          $temp = explode(",",$employeeProfile->any_other_certificates);
                          foreach($temp as $t)
                           { $i=1; ?>
                           <tr>
                            <td>Certificate{{$i}}</td>
                            <td></td>
                            <td><a target="_blank" href="{{url('/documentemp?id=')}}{{$employeeProfile->id}}&document=any_other_certificates"><button class="btn btn-primary btn-sm">Certificate{{$i}}</button></a></td>
                          </tr>
                             <?php $i++; } 
                           } ?>
              </tbody>
            </table>
           
          @else
            <div>No data filled</div>
          @endif
        </div>
      </div>
    </div>
  </div>
 @endsection
