@extends('employee.master')
@section('bodyData')
 @if(isset($announcement->image))
        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
            <!-- Modal content-->
                <div class="modal-content" style="width: 500px;height:500px;margin-top: 100px;">
                    <div class="modal-header" style="border-color: black;">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                            @if(isset($announcement->image))
                                <img src="{{$announcement->image}}" style="border-radius:10px; width:500px;margin-top: 35px;">
                            @endif           
                    @if(isset($announcement->content))
                    <div class="modal-footer" style="background-color:white;border-color:black;">
                        <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
                    <p>{!!$announcement->content!!}</p>
                     </div>
                    @endif
                </div>
            </div>
        </div>
    @endif
<div class="container-fluid">
                <div class="row">
                    <div class="outer-w3-agile col-xl">
                       
                        <div class="stat-grid p-3 mt-3 d-flex align-items-center justify-content-between bg-success">
                            <div class="s-l">
                                <h5>Total target of this month</h5>
                               
                            </div>
                            <div class="s-r">
                            
    
                        <?php
                      
                  
                        
                        if(!empty($targers[0])){?>
                                <h6><?php // echo 0; ?>
                                </h6>
                                <h6><?php echo 
                                $targers[0]->target;?></h6>
                                <?php  }else{
                                      ?>
                                          <h6><?php echo 0 ; ?></h6>
                                <?php } ?>
                                </h6>
                            </div>
                        </div>
                        <br>
                        <div class="stat-grid p-3 d-flex align-items-center justify-content-between bg-primary">
                            <div class="s-l">
                                <h5>Total leads  converted in this month</h5>
                                <!-- <p class="paragraph-agileits-w3layouts text-white">Lorem Ipsum</p> -->
                            </div>
                            
                            <div class="s-r">
                           
                                <h6>
                                 	<?php echo $doneinq ?>
                                </h6>
                            </div>
                        </div>
                       
                       <br>
                       <div class="stat-grid p-3 d-flex align-items-center justify-content-between bg-danger">
                            <div class="s-l">
                                <h5>Total new inquiry count :</h5>
                                <!-- <p class="paragraph-agileits-w3layouts text-white">Lorem Ipsum</p> -->
                            </div>
                            <div class="s-r">
                            
                                <h6><?php echo 
                                $new;?></h6>
                            </div>
                        </div>
                <br>
                <div class="stat-grid p-3 d-flex align-items-center justify-content-between bg-info">
                            <div class="s-l">
                                <h5>Total follow status inquiry count :</h5>
                                <!-- <p class="paragraph-agileits-w3layouts text-white">Lorem Ipsum</p> -->
                            </div>
                            <div class="s-r">
                                <h6><?php echo 
                                $follow;?></h6>
                            </div>
                        </div>
                        
                    </div>
                   
                </div>
            </div>
           
                </div>
            </div>
           <script type="text/javascript">
                $(window).load(function(){        
                   $('#myModal').modal('show');
                }); 
           </script>
@endsection 
