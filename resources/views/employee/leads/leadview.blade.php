@extends('admin.master')
@section('bodyData')


    <div class="container-fluid">
        <div class="row">
            <a href='{{url('/groups/lead-view/')}}/{{base64_encode($group->group_id)}}' class='btn btn-primary' style='color:white'>Show Lead</a>
            <div class="col-sm-12 mt-4">

                <ul class="nav nav-tabs">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle='tab' href="#booking">Lead Info</a>

                    </li>
                   

                </ul>

                <div class="tab-content">


                    <!-- // Booking Detail -->
                    <div id="booking" class="container tab-pane active" style='min-height:400px'>
                        <div class="row">
                            <div class="col-sm-6">
                                <br>
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item"><b>First Name: </b> 
                                      @if(isset($group)){{$group->fname}}@endif
                                    </li>
                                    <li class="list-group-item"><b>Last Name:</b> 
                                      @if(isset($group)){{$group->lname}}@endif
                                    </li>

                                    <li class="list-group-item"><b>Email: </b> 
                                        @if(isset($group)){{$group->email}}@endif
                                      </li>
                                      <li class="list-group-item"><b>Number:</b> 
                                        @if(isset($group)){{$group->number}}@endif
                                      </li>

                                      <li class="list-group-item"><b>Whatsapp Number:</b> 
                                        @if(isset($group)){{$group->wnumber}}@endif
                                      </li>

                                       <li class="list-group-item"><b>Whatsapp Number:</b> 
                                        @if(isset($group)){{$group->wnumber}}@endif
                                      </li>


                                      <li class="list-group-item"><b>Area: </b> 
                                        @if(isset($group)){{$group->area}}@endif
                                      </li>
                                      <li class="list-group-item"><b>City:</b> 
                                        @if(isset($group)){{$group->city}}@endif
                                      </li>

                                      <li class="list-group-item"><b>State:</b> 
                                        @if(isset($group)){{$group->state}}@endif
                                      </li>

                                       <li class="list-group-item"><b>Country:</b> 
                                        @if(isset($group)){{$group->country}}@endif
                                      </li>


                                </ul>
                            </div>

                        </div>
                    </div>

                  







                </div>

            </div>





        </div>




    </div>



    



@endsection
