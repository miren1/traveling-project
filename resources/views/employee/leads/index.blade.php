@extends('employee.master')
@section('bodyData')
<section class="forms-section" style=''>
   


    <div class="container-fluid">
        <div class="row">
           
            <div class="col-sm-12 mt-4">

                <ul class="nav nav-tabs">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle='tab' href="#profile"> Provided Leads</a>

                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle='tab' href="#Address">Leads Assign to Another Employee</a>
                    </li>

                 
                  

                </ul>

                <div class="tab-content">


                    <!-- // Booking Detail -->
                    <div id="profile" class="container-fluid tab-pane active " >
                   
                    
                        <div class="row mt-2">
                            
                        
                            <div class="col-12 mt-2">
                                <form id="frm-example" action="{{route('leads-assign-employee')}}" method="POST">
                                    @csrf  
                                    <label for="fname">Employee Assigned</label>
                                    <div class="form-row">
                                        <div class="form-group col-md-3">
                                           
                                            <select class="form-control" name="employee_id" id="employee_id">
                                                <option value="">Select User</option>
                                                @if(isset($user))
                                                @foreach($user as $u)
                                                @if($u->name)
                                                <option value="{{$u->id}}">{{$u->name}}</option>
                                                @endif
                                                @endforeach
                                                @endif
                                            </select>
                                        </div>
                                        <div class="form-group col-md-2">
                                            
                                            <button type="submit" name="save" id="Assign_emp" class="btn btn-primary" disabled="true">Assign</button>
                                        </div>
                                        <div class="form-group col-md-2">

                                            <select class="form-control" name="group_id" id="group_id">
                                                <option value="">Select filter Group </option>
                                                @if(isset($group))
                                                @foreach($group as $u)
                                                @if($u->name)
                                                <option value="{{$u->id}}">{{$u->name}}</option>
                                                @endif
                                                @endforeach
                                                @endif
                                            </select>

                                        </div>

                                        <div class="form-group col-md-2">
                                           
                                            <select class="form-control" name="admin_id" id="admin_id">
                                                <option value="">Select filter user </option>
                                                @if(isset($admins))
                                                @foreach($admins as $u)
                                                @if($u->name)
                                                <option value="{{$u->id}}">{{$u->name}}</option>
                                                @endif
                                                @endforeach
                                                @endif
                                            </select>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <input class="form-control" type="datetime-local" id="status-date-time" name="date_time">
                                        </div>
                                        
                                    </div>

                                    <div class="table-responsive">
                                        <table id="Leads" class="table" style="width: 100%">
                                            <thead class="thead-dark">
                                                <tr>
                                                    <th scope="col"><input name="select_all" value="1" id="example-select-all" type="checkbox" /></th>
                                                    
                                                    <th scope="col">First Name</th>
                                                    <th scope="col">Last Name</th>
                                                    <th scope="col">Assign By</th>
                                                    <th scope="col">Group</th>
                                                    <th scope="col">Number</th>
                                                    <th scope="col">Whatsapp Number</th>
                                                    <th scope="col"> Activity count</th>
                                                    <th scope="col">Created at</th>
                                                    <th scope="col">Actions</th>
                                                </tr>
                                            </thead>
                                        
                                        </table>
                                    </div>
                                <br><br><br>
                                </form>
                            </div>

                           
                                
                        </div>  
                     
                        
                    </div>

                    <!-- // Address Inforamtion -->

                    <div id="Address" class="container-fluid tab-pane" >
                        <div class="row mt-2">
                            <div class="col-12 mt-4">

                            </div>
                            <div class="col-12">  
                                <div class="table-responsive">
                                    <table id="EmployeeLeads" class="table" style="width: 100%">
                                        <thead class="thead-dark">
                                            <tr>
                                                <th scope="col">First Name</th>
                                                <th scope="col">Last Name</th>
                                                <th scope="col">Group</th>
                                                <th scope="col">Assign Employee</th>
                                                <th scope="col">Number</th>
                                                <th scope="col">Whatsapp Number</th>
                                                <th scope="col"> Activity count</th>
                                                <th scope="col">Created at</th>
                                                <th scope="col">Actions</th>

                                            </tr>
                                        </thead>
                                    
                                    </table>
                                </div>
                                <br><br><br>
                            </div>
                        </div>
                    </div>

                 
                

                </div>

            </div>





        </div>




    </div>

</section>

    



@endsection


@push('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.css" integrity="sha512-In/+MILhf6UMDJU4ZhDL0R0fEpsp4D3Le23m6+ujDWXwl3whwpucJG1PEmI3B07nyJx+875ccs+yX2CqQJUxUw==" crossorigin="anonymous" />
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css"/>
<style type="text/css">
    div#example_filter input[type="search"] {
    box-sizing: border-box;
    border: 1px solid #d2d6dc;
}

input.CheckLead {
    text-align: left;
    float: left;
    margin-left: 8px;
}
</style>
@endpush


@push('js')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/js/dropify.min.js" integrity="sha512-8QFTrG0oeOiyWo/VM9Y8kgxdlCryqhIxVeRpWSezdRRAvarxVtwLnGroJgnVW9/XBRduxO/z1GblzPrMQoeuew==" crossorigin="anonymous"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js"></script>
<script type="text/javascript">
 
    $(document).on("click", '.Leadsdelete', function(e) {
            e.preventDefault();
            var id = $(this).attr('data-id');
            swal({
                    title: "Are you sure?",
                    text: "You want to delete this lead.",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {

                        $.ajax({
                            type: "POST",
                            url: "{{route('groups-lead-delete')}}",
                            data: {
                                "id": id,
                                "_token": "{{ csrf_token() }}"
                            },
                            success: function(response) {
                                if (response.status == "success") {
                                    swal(response.message, {
                                        icon: 'success',
                                    }).then((result) => {
                                         $('#Leads').DataTable().ajax.reload(null,false);
                                    });
                                } else {
                                    swal("Error","Something went wrong!",{icon:'error'});
                                }
                            }
                        });
                    }
                });
    });
        
    function getlead(dataSet){
    //var dataSet =[]; 
   
    $('#Leads').DataTable().destroy();
     var table = $("#Leads").DataTable({
      
      "processing": true,
      "serverSide": true,
      "ordering": false,
      'columnDefs': [{
         'targets': 0,
         'searchable':false,
         'orderable':false,
         'className': 'dt-body-left',
         }],
        "ajax": {
            "url": "{{ url('leads/list') }}",
            "type": "POST",
            "data": dataSet, 
       },
       "columns": [
            {"data":"id",'render': function (data, type, full, meta){
                return '<input type="checkbox" name="id[]" class="CheckLead " value="' 
                +data+'">';
            }},
            { "data": "id",'render': function (data, type, full, meta){
               
                return full.lead.fname;
            }},
            { "data": "id",'render': function (data, type, full, meta){
               
                return full.lead.lname;
            }},
            { "data": "id",'render': function (data, type, full, meta){
               
                return "<span class='badge badge-info ml-1'>"+full.user.name+"</span>";
            }},
            { "data": "id",'render': function (data, type, full, meta){
               
                return "<span class='badge badge-success ml-1'>"+full.group.name+"</span>";
            }},
            { "data": "lead.number"},
            { "data": "lead.wnumber"},
            { "data": "id",'render': function (data, type, full, meta){
               
               return "<span class='badge badge-primary ml-1'>"+full.activity_count+"</span>";
            }},
            { "data": "created_at",render:function(data,type,row,meta) {

                return moment(data).format('DD-MM-YYYY,h:mm a');
                
            }},
            { "data": "id", 
                    render:function(data,type,row,meta){
                      
                     var html= '<a  href="{{url("leads/view/")}}/'+btoa(row.lead.id)+'"><i class="fa fa-eye" aria-hidden="true"'+ 
                                'title="Detail View" style="font-size:20px"></i></a>';

                  return html; 
                         
              }

            },
        ]
    });

    //create

     // Handle click on "Select all" control
   $('#example-select-all').on('click', function(){
      // Check/uncheck all checkboxes in the table
      var rows = table.rows({ 'search': 'applied' }).nodes();
      $('input[type="checkbox"]', rows).prop('checked', this.checked);
      
      if($('#example-select-all:checked').length == 1)
      {
          $("#Assign_emp").attr("disabled", false); 
      }else{
          $("#Assign_emp").attr("disabled", true); 
      }  

   });

   // Handle click on checkbox to set state of "Select all" control
   $('#Leads tbody').on('change', 'input[type="checkbox"]', function(){
        var $fields = $('.CheckLead:checked').length;
        if($fields==0){
            $("#Assign_emp").attr("disabled", true); 
        }else{
            $("#Assign_emp").attr("disabled", false); 
        }
   });
   $('#Leads tbody').on('change', 'input[type="checkbox"]', function(){
      // If checkbox is not checked
      if(!this.checked){
        
         var el = $('#example-select-all').get(0);
         // If "Select all" control is checked and has 'indeterminate' property
         if(el && el.checked && ('indeterminate' in el)){
            // Set visual state of "Select all" control 
            // as 'indeterminate'
            el.indeterminate = true;
            
         }
      }else{
        $("#Assign_emp").attr("disabled", false); 
      }
   });

   $('#Leads').on( 'page.dt', function () {
    //var info = table.page.info();
         $('#frm-example').bootstrapValidator('resetForm', true);
        $('input[type="checkbox"]').removeAttr('checked');
        $('#example-select-all').removeAttr('checked');
        $("#Assign_emp").attr("disabled", true); 
    
    });
    




$('#frm-example').bootstrapValidator({
        message: 'This value is not valid',
        fields: {
            'employee_id': {
                validators: {
                    notEmpty: {
                        message: 'Please Select Employee.'
                    }
                }
            },
            'id[]': {
                validators: {
                    choice: {
                        min: 1,
                        message: 'Please choose 1 record you are good at'
                    }
                }
            },
     }
    }).on('success.form.bv', function(e) {
        e.preventDefault();
        var $form = $(e.target);
        var bv = $form.data('bootstrapValidator');
        var matches = [];
        var checkedcollection = table.$(".CheckLead:checked", { "page": "all" });
        checkedcollection.each(function (index, elem) {
            matches.push($(elem).val());
        });

        console.log(matches.length);
        var AccountsJsonString = JSON.stringify(matches);
        
        if(matches.length!=0){

        $.post($form.attr('action'), $form.serialize(), function(result) {
            //$('#loaderbg').hide();
             
             $("#Assign_emp").attr("disabled", true); 
                var data=JSON.parse(JSON.stringify(result));
                $('#example-select-all').removeAttr('checked');
                if(data.status=='success')
                {
                    swal(data.message, {
                    icon: 'success',
                    }).then((result) => {
                        $('#Leads').DataTable().ajax.reload(null,false);
                        $('#EmployeeLeads').DataTable().ajax.reload(null,false);
                        
                        $('#frm-example').bootstrapValidator('resetForm', true);
                    });
                }else 
                {
                swal("Error",data.message,{icon:'error'});
                    $('#Leads').DataTable().ajax.reload(null,false);
                    $('#frm-example').bootstrapValidator('resetForm', true);
                }

        },'json');

        }else{
            swal("You Have not selected any records of lead so please try again!!")
            .then((value) => {
                location.reload();
            });
        }

    });
    }    

    $('#group_id').on('change', function() {

            var groupArray = {};

             if($('#admin_id').val()!=''){
            
               groupArray['user_id']= $('#admin_id').val();
             }

             if($('#status-date-time').val()!=''){
                groupArray['activity_date']= $('#status-date-time').val();
             }

             
            // console.log(groupArray);
             if(this.value!=''){
               
                groupArray['group_id']=this.value;
                      
             }
                
           getlead(groupArray); 
             
    });


    $('#admin_id').on('change', function() {
            var adminArray = {}; 
          
             if($('#group_id').val()!=''){
              
               adminArray['group_id']=$('#group_id').val();
             }

             if($('#status-date-time').val()!=''){
                adminArray['activity_date']= $('#status-date-time').val();
             }


             if(this.value!=''){
              
               adminArray['user_id']=this.value;
               // getlead(adminArray);             
             }
               
         getlead(adminArray); 
            
    });

    $('#status-date-time').on('change', function() {
        var activityArray = {}; 
        if(this.value!=''){
              
            activityArray['activity_date']=this.value;
              // getlead(adminArray);             
        }
        if($('#group_id').val()!=''){
              
            activityArray['group_id']=$('#group_id').val();
        }
        if($('#admin_id').val()!=''){
            
            activityArray['user_id']= $('#admin_id').val();
        }
        getlead(activityArray); 

    });
   

    $(document).ready(function() {
    //var uri =;
    var myArray = {}; 
    getlead(myArray);
      


    var EmployeeLeads = $("#EmployeeLeads").DataTable({

      "ajax": "{{ url('leads/assign_list') }}",
      "processing": true,
      "serverSide": true,
      "ordering": false,
      'columnDefs': [{
         'targets': 0,
         'searchable':false,
         'orderable':false,
         'className': 'dt-body-left',
         }],
      "columns": [
            { "data": "id",'render': function (data, type, full, meta){
               
                return full.lead.fname;
            }},
            { "data": "id",'render': function (data, type, full, meta){
               
                return full.lead.lname;
            }},
            { "data": "id",'render': function (data, type, full, meta){
               
                return "<span class='badge badge-info ml-1'>"+full.group.name+"</span>";
            }},
            {"data":"id",'render': function (data, type, full, meta){
                return "<span class='badge badge-success ml-1'>"+full.employee.name+"</span>";
            }},
            { "data": "lead.number"},
            { "data": "lead.wnumber"},
            { "data": "id",'render': function (data, type, full, meta){
               
               return "<span class='badge badge-primary ml-1'>"+full.activity_count+"</span>";
            }},
            { "data": "id",render:function(data,type,row,meta) {

                return moment(row.created_at).format('DD-MM-YYYY,h:mm a');
                
            }},
            { "data": "id", 
                    render:function(data,type,row,meta){
                      
                     var html= '<a  href="{{url("leads/view/")}}/'+btoa(row.lead.id)+'"><i class="fa fa-eye" aria-hidden="true"'+ 
                                'title="Detail View" style="font-size:20px"></i></a>';

                  return html; 
                         
              }

            },
        ]
    });



   



  



  



    $(document).on("click", '.Unassign', function(e) {
            e.preventDefault();
            var id = $(this).attr('data-id');
            var group_id =$(this).attr('data-group');
            swal({
                    title: "Are you sure?",
                    text: "You want to Unassign All Leads from this Employee.",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {

                        $.ajax({
                            type: "POST",
                            url: "{{route('groups-unassign')}}",
                            data: {
                                "id": id,
                                'group_id':group_id,
                                "_token": "{{ csrf_token() }}"
                            },
                            success: function(response) {
                                if (response.status == "success") {
                                    swal(response.message, {
                                        icon: 'success',
                                    }).then((result) => {
                                         $('#Leads').DataTable().ajax.reload(null,false);
                                         $('#EmployeeLeads').DataTable().ajax.reload(null,false);
                                    });
                                } else {
                                    swal("Error","Something went wrong!",{icon:'error'});
                                }
                            }
                        });
                    }
        });
    });


    
  } );
</script>

@endpush
