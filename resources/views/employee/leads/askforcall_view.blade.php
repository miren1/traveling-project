@extends('employee.master')
@section('bodyData')
<section class="forms-section" style=''>
<a href='{{url('/admin-askforcall')}}' class='btn btn-primary' style='color:white; margin-left: 30px'>Back to leads</a>

<div class="container-fluid">
    <div class="row">
        <div class="col-sm-6">
            <br>
            <ul class="list-group list-group-flush">
                <li class="list-group-item"><b>First Name: </b> 
                    @if(isset($askforcall)){{$askforcall->first_name}}@endif
                </li>
                <li class="list-group-item"><b>Last Name: </b> 
                    @if(isset($askforcall)){{$askforcall->last_name}}@endif
                </li>
                <li class="list-group-item"><b>Email: </b> 
                    @if(isset($askforcall)){{$askforcall->email}}@endif
                </li>
            </ul>
        </div>
        <div class="col-sm-6">
            <br>
            <ul class="list-group list-group-flush">
                <li class="list-group-item"><b>Number: </b> 
                    @if(isset($askforcall)){{$askforcall->contactnumber}}@endif
                </li>
                <li class="list-group-item"><b>Whatsapp Number: </b> 
                    @if(isset($askforcall)){{$askforcall->wpnumber}}@endif
                </li>

            </ul>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12 mt-4">
            <ul class="nav nav-tabs">
                <li class="nav-item">
                 <a class="nav-link active" data-toggle='tab' href="#profile">Leads Status</a>
                </li>
                <!-- <li class="nav-item">
                    <a class="nav-link" data-toggle='tab' href="#Address">Assign lead Follow</a>
                </li> -->
            </ul>
            <div class="tab-content">
                <div id="profile" class="container-fluid tab-pane active " >   
                    <div class="row mt-4">
                        <div class='col-sm-2'>
                           @if(isset($askforcall))
                            <button id="add-group" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" style='text-align:left'>Add Status</button>
                            @endif    
                        </div>
                        <div class="col-12 mt-4">
                            <div class="table-responsive">
                                <table id="example" class="table" style="width: 100%">
                                    <thead class="thead-dark">
                                        <tr>
                                            <th scope="col">Created By</th>
                                            <th scope="col">Status</th>
                                            <th scope="col">Remarks</th>
                                            <th scope="col">Date&Time</th>
                                            <th scope="col">Created at</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div><br><br><br>
                        </div>
                          <!-- Modal -->
                        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
                                aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="status-head"> Add Status</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <form id="defaultForm" class="defaultForm" action="{{route('status-lead-store')}}" method="POST" enctype="multipart/form-data">
                                         @csrf
                                        <input type="hidden" name="id" id="lead_id" value="" />
                                        <input type="hidden" name="group_lead_id" id="group_lead_id" @if(isset($askforcall)) value="{{$askforcall->id}}" @endif />
                                        <div class="modal-body">
                                            <div class="form-row">
                                                <div class="form-group col-md-12">
                                                    <label for="inputAmount">Call Status</label>
                                                    <select class="form-control"  name="call_status_id" id="status-callstatus"  required="" data-bv-notempty-message="The Call Status is required" >
                                                        <option value="">Select Call Status</option>
                                                        @if(isset($callstatus))
                                                        @foreach($callstatus as $g)
                                                        <option value="{{$g->id}}">{{$g->name}}</option> 
                                                        @endforeach  
                                                        @endif
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-12" id="groupnew">
                                                    <label for="inputYears">Remarks</label>
                                                    <textarea type="text" class="form-control" id="status-remarks" name="remarks" 
                                                            placeholder="Enter Remarks" required=""></textarea>
                                                </div>
                                                <div class="form-group col-md-12" id="groupnewdate">
                                                    <label for="inputYears">Date & Time </label>
                                                    <input class="form-control" type="datetime-local" id="status-date-time" name="date_time">   
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary"
                                                data-dismiss="modal">Close</button>
                                            <button  type="submit" class="btn btn-primary" id="status-add">Add</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>       
                </div>
                <!-- <div id="Address" class="container-fluid tab-pane" >
                    <div class="row mt-2">
                        <div class="col-12 mt-4">
                        </div>
                        <div class="col-12">  
                            <div class="table-responsive">
                                <table id="assign_status_list" class="table" style="width: 100%">
                                    <thead class="thead-dark">
                                        <tr>
                                            <th scope="col">Assign By</th>
                                            <th scope="col">To</th>
                                            <th scope="col">Status</th>
                                            <th scope="col">Created at</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div><br><br><br>
                        </div>
                    </div>
                </div> -->
            </div>
        </div>
    </div>
</div>

</section>
 
<div class="modal fade" id="ViewStatus" tabindex="-1" role="dialog"
aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Call Status</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <br>
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item"><b>Status: </b> 
                                <p id="ViewStatusStatus"></p>
                            </li>
                            <li class="list-group-item"><b>Remarks: </b> 
                                <p id="ViewStatusRemarks" style="overflow-wrap: break-word;"></p>
                            </li>
                            <li class="list-group-item"><b>Date & time: </b> 
                                <p id="ViewStatusTime"></p>
                            </li>
                            <li class="list-group-item"><b>Created By: </b> 
                                <p id="ViewStatusCreatedBy"></p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection


@push('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.css" integrity="sha512-In/+MILhf6UMDJU4ZhDL0R0fEpsp4D3Le23m6+ujDWXwl3whwpucJG1PEmI3B07nyJx+875ccs+yX2CqQJUxUw==" crossorigin="anonymous" />
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css"/>
<style type="text/css">
    div#example_filter input[type="search"] {
    box-sizing: border-box;
    border: 1px solid #d2d6dc;
}

input.CheckLead {
    text-align: left;
    float: left;
    margin-left: 8px;
}
</style>
@endpush


@push('js')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/js/dropify.min.js" integrity="sha512-8QFTrG0oeOiyWo/VM9Y8kgxdlCryqhIxVeRpWSezdRRAvarxVtwLnGroJgnVW9/XBRduxO/z1GblzPrMQoeuew==" crossorigin="anonymous"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js"></script>

<script>
$(document).ready(function() {
var EmployeeLeads = $("#example").DataTable({

"ajax": "{{ url('leads/status_list') }}/{{base64_encode($askforcall->id)}}",
"processing": true,
"serverSide": true,
"ordering": false,
'columnDefs': [{
   'targets': 0,
   'searchable':false,
   'orderable':false,
   'className': 'dt-body-left',
   }],
"columns": [
        {"data": "id",'render': function (data, type, full, meta){
         
            return "<span class='badge badge-info ml-1'>"+full.employee.name+"</span>";
      }},
      { "data": "id",'render': function (data, type, full, meta){
         
          return full.call_status.name;
      }},
      { "data": "remarks",render:function(data,type,row,meta) {

        return  data.substr(0, 60);
        }
     
      },
      { "data": "id",render:function(data,type,row,meta) {
        if(row.date_time!=''){
            return moment(row.date_time).format('DD-MM-YYYY,h:mm a');
        }else{
            return '';
        }

      }},
      { "data": "id",render:function(data,type,row,meta) {

          return moment(row.created_at).format('DD-MM-YYYY,h:mm a');
          
      }},
      { "data": "id", 
              render:function(data,type,row,meta){
              var html='';
              @if(isset($assignlead)) 
               var html= '<button class="ml-1 edit_status" data-id="'+row.id+'" data-callstatus-id="'+row.call_status_id+'" data-remarks="'+row.remarks+'" data-date="'+row.date_time+'"><i class="fa fa-edit" aria-hidden="true"'+
                                'title="Edit" style="font-size:20px"></i></button>'+
                          '<button class="ml-1 view_status" data-id="'+row.id+'" data-callstatus-id="'+row.call_status.name+'" data-remarks="'+row.remarks+'" data-date="'+row.date_time+'" data-cdate="'+row.created_at+'"><i class="fa fa-eye" aria-hidden="true"'+ 
                          'title="Detail View" style="font-size:20px"></i></button>';
               @else

               var html='<button class="ml-1 view_status" data-id="'+row.id+'" data-callstatus-id="'+row.call_status.name+'" data-remarks="'+row.remarks+'" data-date="'+row.date_time+'" data-cdate="'+row.created_at+'"><i class="fa fa-eye" aria-hidden="true"'+ 
                          'title="Detail View" style="font-size:20px"></i></button>';
              
              
               @endif 
               

            return html; 
                   
        }

      },
  ]
});






var assign_status_list = $("#assign_status_list").DataTable({

"ajax": "{{ url('leads/assign_status_list') }}/{{base64_encode($askforcall->id)}}",
"processing": true,
"serverSide": true,
"ordering": false,
'columnDefs': [{
   'targets': 0,
   'searchable':false,
   'orderable':false,
   'className': 'dt-body-left',
   }],
    "columns": [
        {"data": "id",'render': function (data, type, full, meta){
            return "<span class='badge badge-info ml-1'>"+full.user.name+"</span>";
         //return full.employee.name;
        }},
        {"data": "id",'render': function (data, type, full, meta){
            return "<span class='badge badge-info ml-1'>"+full.employee.name+"</span>";
         //return full.employee.name;
      }},
      {"data":'status'},
      { "data": "id",render:function(data,type,row,meta) {

        return moment(row.created_at).format('DD-MM-YYYY,h:mm a');

     }},
  ]
});





});


$(document).on("click", '.edit_status', function(e) {
    
    var id = $(this).attr('data-id');
    var callstatus_id = $(this).attr('data-callstatus-id');
    var remarks = $(this).attr('data-remarks');
    var date_time =$(this).attr('data-date');
    
    $('#lead_id').val('');
    $('#status-head').text('Edit Status');
    $('#status-add').text('Update');
    $('#status-date-time').val('');
    $('#lead_id').val(id);
    $('#status-callstatus > option[selected="selected"]').removeAttr('selected');
    
    $('#status-callstatus option[value='+callstatus_id+']').attr('selected','selected');
    $('#status-remarks').val(remarks);
    $("#status-remarks").text(remarks);
    $('#status-date-time').val(date_time);
    $('#exampleModal').modal('show');
});



$(document).on("click", '.view_status', function(e) {
    console.log(id);
    var id = $(this).attr('data-id');
    var callstatus_id = $(this).attr('data-callstatus-id');
    var remarks = $(this).attr('data-remarks');
    var date = $(this).attr('data-date');
    var cdate = $(this).attr('data-cdate');
    
    $('#ViewStatusStatus').text('');
    $("#ViewStatusRemarks").text('');
    $('#ViewStatusCreatedBy').text('');
    $('#ViewStatusTime').text('');
    
    $('#ViewStatusStatus').text(callstatus_id);
    $("#ViewStatusRemarks").text(remarks);
    $('#ViewStatusCreatedBy').text(moment(cdate).format('DD-MM-YYYY,h:mm a'));
    $('#ViewStatusTime').text(moment(date).format('DD-MM-YYYY,h:mm a'));
    
    $('#ViewStatus').modal('show');
});



    $('#defaultForm').bootstrapValidator({
        message: 'This value is not valid',
        fields: {
            call_status: {
                validators: {
                    notEmpty: {
                        message: 'Please select Call status.'
                    }
                }
            },
            remarks: {
                validators: {
                    notEmpty: {
                        message: 'Please Enter Remarks.'
                    }
                }
            },

     }
	 }).on('success.form.bv', function(e) {
        e.preventDefault();
        var $form = $(e.target);
        var bv = $form.data('bootstrapValidator');

        $.post($form.attr('action'), $form.serialize(), function(result) {
            
            var data=JSON.parse(JSON.stringify(result));
            $('#example-select-all').removeAttr('checked');
            if(data.status=='success')
                {
                    swal(data.message, {
                    icon: 'success',
                    }).then((result) => {
                        var sform2 = $('#defaultForm')[0];
                        $(sform2).removeClass('was-validated');
                        sform2.reset();
                        
                        $('#exampleModal').modal('hide'); 
                        $('#example').DataTable().ajax.reload();
                    });
            }else 
            {
                swal("Error",data.message,{icon:'error'});
                var form2 = $('#defaultForm')[0];
                   $(form2).removeClass('was-validated');
                  form2.reset();
                 
                  $("#lead_id").val();
                  $('#exampleModal').modal('hide'); 
                  $('#example').DataTable().ajax.reload();
            }
        }, 'json');			
        //End Update
         });



$("#exampleModal").on('hide.bs.modal', function(){
    $("#defaultForm")[0].reset();
    $('#defaultForm').bootstrapValidator('resetForm', true);
    $('#lead_id').val('');
    $('#status-callstatus option[value=""]').attr('selected','selected');
    $('#status-head').val('Add Status');
    $('#status-add').val('Add');
      
});   




</script>

@endpush
