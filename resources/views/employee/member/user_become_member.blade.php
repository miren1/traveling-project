@extends('front.layouts.master')
@section('title','Membership Plans')

@push('style')
<!-- <link href="{{ asset('admin/css/style.css') }}" rel="stylesheet" type="text/css" media="all" /> -->

<link rel="stylesheet"
    href="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css" />

<style type="text/css">
#mytablebd input , #mytablebdm input
    {
        width:100px;
    
    }
#sidebar {
    background: #083672;
}
#sidebar ul li.active>a, #sidebar a[aria-expanded="true"] {
    color: #fff;
    background: #083672;
}
#sidebar .sidebar-header {
    padding: 20px;
    background: #f3941e;
}
ul ul a {
    font-size: 0.9em !important;
    padding-left: 30px !important;
    background: #083672;
}
#sidebar ul li a:hover {
    color: #083672;
    background: #fff;
}
.btn-primary, .btn-primary:hover, .btn-primary:active, .btn-primary:visited {
    background-color: #083672 !important;
border-color: #083672 !important;
}
/*.btn-success, .btn-success:hover, .btn-success:active, .btn-success:visited {
    background-color: #083672 !important;
border-color: #083672 !important;
}*/
.btn-info, .btn-info:hover, .btn-info:active, .btn-info:visited {
    background-color: #f3941e !important;
    border-color: #f3941e !important;
}
.bg-primary
{
 background-color: #f3941e !important;
    border-color: #f3941e !important;
}

.fa-info-circle,.fa-edit
{
color:#083672;
}
ul.top-icons-agileits-w3layouts a.nav-link i {
    display: inline-block;
    font-size: 17px;
    color: #083672;
}
.nav-pills .nav-link.active, .nav-pills .show>.nav-link {
    color: #fff;
    background-color: #083672;
}
/*a {
    color: #083672;
    text-decoration: none;
    background-color: transparent;
    -webkit-text-decoration-skip: objects;
}*/
/*a:hover {
    color: #083672;
    text-decoration: underline;
}*/
.ui-datepicker-month{
    color: #f7af38 !important;
}

.ui-datepicker-year{
    color: #f7af38 !important;
}
/* Chrome, Safari, Edge, Opera */
input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
-webkit-appearance: none;
margin: 0;
}

/* Firefox */
input[type=number] {
-moz-appearance: textfield;
}

.stepwizard-step p {
    margin-top: 10px;
}
.stepwizard-row {
    display: table-row;
}
.stepwizard {
    display: table;
    width: 100%;
    position: relative;
    margin-top:40px; 

}
.stepwizard-step button[disabled] {
    opacity: 1 !important;
    filter: alpha(opacity=100) !important;
}
.stepwizard-row:before {
    top: 24.4px;
    bottom: 0;
    position: absolute;
    content: " ";
    width: 65%;
    height: 1px;
    background-color: #ccc;
    z-order: 0;
    margin-left: 20%;
}
.stepwizard-step {
     display: table-cell;
    text-align: center;
    position: relative;
}
.btn-circle {
    width: 50px;
    height: 50px;
    text-align: center;
    padding: 6px 0;
    font-size: 25px;
    line-height: 1.428571429;
    border-radius: 25px;
}

h4.tittle-w3-agileits {
    font-size: 2em;
    letter-spacing: 1.5px;
    font-weight: 600;
    color: #083672;
    text-align: center;
}
/*
 *
 * TOOLTIPTSTER DEFAULT CSS
 */
 .tooltipster-default{border-radius:5px;border:2px solid #000;background:#4c4c4c;color:#fff}
 .tooltipster-default .tooltipster-content{font-family:Arial,sans-serif;font-size:14px;line-height:16px;padding:8px 10px;overflow:hidden}
 .tooltipster-icon{cursor:help;margin-left:4px}
 .tooltipster-base{padding:0;font-size:0;line-height:0;position:absolute;left:0;top:0;z-index:9999999;pointer-events:none;width:auto;overflow:visible}
 .tooltipster-base .tooltipster-content{overflow:hidden}
 .tooltipster-arrow{display:block;text-align:center;width:100%;height:100%;position:absolute;top:0;left:0;z-index:-1}
 .tooltipster-arrow span,.tooltipster-arrow-border{display:block;width:0;height:0;position:absolute}
 .tooltipster-arrow-top span,
 .tooltipster-arrow-top-right span,.tooltipster-arrow-top-left span{border-left:8px solid transparent !important;border-right:8px solid transparent !important;border-top:8px solid;bottom:-7px}
 .tooltipster-arrow-top .tooltipster-arrow-border,.tooltipster-arrow-top-right .tooltipster-arrow-border,.tooltipster-arrow-top-left .tooltipster-arrow-border{border-left:9px solid transparent !important;border-right:9px solid transparent !important;border-top:9px solid;bottom:-7px}
 .tooltipster-arrow-bottom span,.tooltipster-arrow-bottom-right span,.tooltipster-arrow-bottom-left span{border-left:8px solid transparent !important;border-right:8px solid transparent !important;border-bottom:8px solid;top:-7px}
 .tooltipster-arrow-bottom .tooltipster-arrow-border,.tooltipster-arrow-bottom-right .tooltipster-arrow-border,.tooltipster-arrow-bottom-left .tooltipster-arrow-border{border-left:9px solid transparent !important;border-right:9px solid transparent !important;border-bottom:9px solid;top:-7px}
 .tooltipster-arrow-top span,.tooltipster-arrow-top .tooltipster-arrow-border,.tooltipster-arrow-bottom span,.tooltipster-arrow-bottom .tooltipster-arrow-border{left:0;right:0;margin:0 auto}
 .tooltipster-arrow-top-left span,.tooltipster-arrow-bottom-left span{left:6px}
 .tooltipster-arrow-top-left .tooltipster-arrow-border,.tooltipster-arrow-bottom-left .tooltipster-arrow-border{left:5px}
 .tooltipster-arrow-top-right span,.tooltipster-arrow-bottom-right span{right:6px}
 .tooltipster-arrow-top-right .tooltipster-arrow-border,.tooltipster-arrow-bottom-right .tooltipster-arrow-border{right:5px}
 .tooltipster-arrow-left span,.tooltipster-arrow-left .tooltipster-arrow-border{border-top:8px solid transparent !important;border-bottom:8px solid transparent !important;border-left:8px solid;top:50%;margin-top:-7px;right:-7px}
 .tooltipster-arrow-left .tooltipster-arrow-border{border-top:9px solid transparent !important;border-bottom:9px solid transparent !important;border-left:9px solid;margin-top:-8px}
 .tooltipster-arrow-right span,.tooltipster-arrow-right .tooltipster-arrow-border{border-top:8px solid transparent !important;border-bottom:8px solid transparent !important;border-right:8px solid;top:50%;margin-top:-7px;left:-7px}
 .tooltipster-arrow-right .tooltipster-arrow-border{border-top:9px solid transparent !important;border-bottom:9px solid transparent !important;border-right:9px solid;margin-top:-8px}
 .tooltipster-fade{opacity:0;-webkit-transition-property:opacity;-moz-transition-property:opacity;-o-transition-property:opacity;-ms-transition-property:opacity;transition-property:opacity}
 .tooltipster-fade-show{opacity:1}
 .tooltipster-grow{-webkit-transform:scale(0,0);-moz-transform:scale(0,0);-o-transform:scale(0,0);-ms-transform:scale(0,0);transform:scale(0,0);-webkit-transition-property:-webkit-transform;-moz-transition-property:-moz-transform;-o-transition-property:-o-transform;-ms-transition-property:-ms-transform;transition-property:transform;-webkit-backface-visibility:hidden}
 .tooltipster-grow-show{-webkit-transform:scale(1,1);-moz-transform:scale(1,1);-o-transform:scale(1,1);-ms-transform:scale(1,1);transform:scale(1,1);-webkit-transition-timing-function:cubic-bezier(0.175,0.885,0.320,1);-webkit-transition-timing-function:cubic-bezier(0.175,0.885,0.320,1.15);-moz-transition-timing-function:cubic-bezier(0.175,0.885,0.320,1.15);-ms-transition-timing-function:cubic-bezier(0.175,0.885,0.320,1.15);-o-transition-timing-function:cubic-bezier(0.175,0.885,0.320,1.15);transition-timing-function:cubic-bezier(0.175,0.885,0.320,1.15)}
 .tooltipster-swing{opacity:0;-webkit-transform:rotateZ(4deg);-moz-transform:rotateZ(4deg);-o-transform:rotateZ(4deg);-ms-transform:rotateZ(4deg);transform:rotateZ(4deg);-webkit-transition-property:-webkit-transform,opacity;-moz-transition-property:-moz-transform;-o-transition-property:-o-transform;-ms-transition-property:-ms-transform;transition-property:transform}
 .tooltipster-swing-show{opacity:1;-webkit-transform:rotateZ(0);-moz-transform:rotateZ(0);-o-transform:rotateZ(0);-ms-transform:rotateZ(0);transform:rotateZ(0);-webkit-transition-timing-function:cubic-bezier(0.230,0.635,0.495,1);-webkit-transition-timing-function:cubic-bezier(0.230,0.635,0.495,2.4);-moz-transition-timing-function:cubic-bezier(0.230,0.635,0.495,2.4);-ms-transition-timing-function:cubic-bezier(0.230,0.635,0.495,2.4);-o-transition-timing-function:cubic-bezier(0.230,0.635,0.495,2.4);transition-timing-function:cubic-bezier(0.230,0.635,0.495,2.4)}
 .tooltipster-fall{top:0;-webkit-transition-property:top;-moz-transition-property:top;-o-transition-property:top;-ms-transition-property:top;transition-property:top;-webkit-transition-timing-function:cubic-bezier(0.175,0.885,0.320,1);-webkit-transition-timing-function:cubic-bezier(0.175,0.885,0.320,1.15);-moz-transition-timing-function:cubic-bezier(0.175,0.885,0.320,1.15);-ms-transition-timing-function:cubic-bezier(0.175,0.885,0.320,1.15);-o-transition-timing-function:cubic-bezier(0.175,0.885,0.320,1.15);transition-timing-function:cubic-bezier(0.175,0.885,0.320,1.15)}
 .tooltipster-fall.tooltipster-dying{-webkit-transition-property:all;-moz-transition-property:all;-o-transition-property:all;-ms-transition-property:all;transition-property:all;top:0 !important;opacity:0}
 .tooltipster-slide{left:-40px;-webkit-transition-property:left;-moz-transition-property:left;-o-transition-property:left;-ms-transition-property:left;transition-property:left;-webkit-transition-timing-function:cubic-bezier(0.175,0.885,0.320,1);-webkit-transition-timing-function:cubic-bezier(0.175,0.885,0.320,1.15);-moz-transition-timing-function:cubic-bezier(0.175,0.885,0.320,1.15);-ms-transition-timing-function:cubic-bezier(0.175,0.885,0.320,1.15);-o-transition-timing-function:cubic-bezier(0.175,0.885,0.320,1.15);transition-timing-function:cubic-bezier(0.175,0.885,0.320,1.15)}
 .tooltipster-slide.tooltipster-dying{-webkit-transition-property:all;-moz-transition-property:all;-o-transition-property:all;-ms-transition-property:all;transition-property:all;left:0 !important;opacity:0}
 .tooltipster-content-changing{opacity:.5;-webkit-transform:scale(1.1,1.1);-moz-transform:scale(1.1,1.1);-o-transform:scale(1.1,1.1);-ms-transform:scale(1.1,1.1);transform:scale(1.1,1.1)}

 /* *
  * TOOLTIPTSTER OVERRIDE CSS
  */
.tooltipster-light {
    border-radius: 5px;
    background-color: #F0BE00;
    color: #fff;
}
.error{
    color: red;
}

.tooltipster-content {
  font-family: Verdana, sans-serif;
  color: white;
  font-size: 13px;
  line-height: 16px;
  padding: 8px 10px;
}
.grey-circle{
    width: 50px;
    height: 50px;
    text-align: center;
    padding: 6px 0;
    font-size: 25px;
    line-height: 1.428571429;
    border-radius: 25px;
    background-color: #ccc;
}
.outer-w3-agile {
    padding: 1em 1em;
    -webkit-box-shadow: 0px 0px 11px 0px rgb(0 0 0 / 25%);
    -moz-box-shadow: 0px 0px 11px 0px rgba(0, 0, 0, 0.25);
    box-shadow: 0px 0px 11px 0px rgb(0 0 0 / 25%);
    background: #fff;
    margin-bottom: 4%;
}

.ui-datepicker-month{
    color: #f7af38 !important;
}

.ui-datepicker-year{
    color: #f7af38 !important;
}
.form-control {
    
    color: #495057 !important;
   
}
.dis
{
  pointer-events: none;
  opacity: 0.9;
}
.has-error {
    border-color: #a94442;
    -webkit-box-shadow: inset 0 1px 1px rgb(0 0 0 / 8%);
    box-shadow: inset 0 1px 1px rgb(0 0 0 / 8%);
}
</style>
@endpush
@section('content')


 <!-- breadcrumb area start -->
    <div class="breadcrumb-area jarallax" style="background-image: none; z-index: 0;"
        data-jarallax-original-styles="background-image:url(assets/img/bg/1.png);">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb-inner">
                        <div class="row justify-content-center">
                            <div class="col-xl-6 col-lg-8">
                                <div class="section-title mb-2 text-center style-two">
                                    <h2 class="title">@if(isset($plan)) {{strtoupper($plan->type)}} @endif<span>Plan</span></h2>
                                    <p>Choose a Plan that works best for you</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- breadcrumb area End -->
    <section class="forms-section" style="width:75%;margin: auto;min-height:500px;display: block;">
    <div class="outer-w3-agile mt-3">
        <h4 class="tittle-w3-agileits mb-4">Member Details</h4>
        
        @if ($message = Session::get('admincreation'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
                <strong>{{ $message }}</strong>
        </div>
        @endif

        <div class="stepwizard col-md-offset-3">
            <div class="stepwizard-row setup-panel">
                <div class="stepwizard-step">
                    <a href="#member" type="button" class="btn btn-primary grey-circle btn-circle">1</a>
                    <p>Members</p>
                </div>
                <div class="stepwizard-step dis">
                    <a href="#details" type="button" class="btn btn-default grey-circle btn-circle" disabled="disabled">2</a>
                    <p>Details</p>
                </div>
                <div class="stepwizard-step dis">
                    <a href="#step-3" type="button" class="btn btn-default grey-circle btn-circle" disabled="disabled">3</a>
                    <p>Submit</p>
                </div>
                <!-- <div class="stepwizard-step">
                    <a href="#step-4" type="button" class="btn btn-default btn-circle">4</a>
                    <p>Submit</p>
                </div> -->
            </div>
        </div>
        <form role="form" id="addMember" action="{{ url('store-member') }}" method="POST" enctype="multipart/form-data" class="mt-3">
            <div class="row setup-content" id="member">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="col-md-6">
                                @csrf
                                <input type="hidden" name="plan_id" value="{{$plan->id}}"/>
                                <input type="hidden" name="id" id="id" value="{{$user->id}}">
                                <input type="hidden" name="user_store" id="user_store" value="yes">
                                <select class="form-control" name="type" id="type">
                                    <option value="family">Family - Max 6 Member</option>
                                    <option value="group">Group - Max 20 Member</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <p class="ml-4 mb-2">Primary Member</p>
                        </div>
                        <div class="row">
                            <div class="col-sm-3 nopadding">
                                <div class="form-group">
                                    <small>Prefix</small>
                                    <select name="prefix[]" class="form-control" required data-bv-notempty="true" data-bv-notempty-message="Please Select Prefix">
                                        <option value="Mr.">Mr.</option>
                                        <option value="Ms.">Ms.</option>
                                        <option value="Mrs.">Mrs.</option>
                                    </select>
                                </div>

                            </div>
                            <div class="col-sm-3 nopadding">
                                <div class="form-group">
                                    <small>Name</small>
                                    <input type="text" class="form-control" required id="name" name="name[]" value="{{$user->first_name}} {{$user->last_name}}" placeholder="Name" data-bv-notempty="true" data-bv-notempty-message="Name is required">
                                </div>
                            </div>
                            <div class="col-sm-3 nopadding">
                                <div class="form-group">
                                    <small>Date of birth</small>
                                    <input type = "text" class="form-control" required id="dob" name="dob[]" value="" placeholder="DOB" data-bv-notempty="true" data-bv-notempty-message="DOB is required" readonly>
                                    
                                </div>
                            </div>
                            <div class="col-sm-3 nopadding">
                                <div class="form-group">
                                    <div class="form-group">
                                        <small>Adhar Card</small>
                                        <input type="number" class="form-control" id="adharno" name="adharno[]" value="" min="0" maxlength="12" minlength="12" placeholder="Aadhar No." required data-bv-notempty="true" data-bv-notempty-message="Aadhar No. is required">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3 nopadding">
                                <div class="form-group">
                                    <small>Pancard</small>
                                    <input type="text" class="form-control" id="panno" name="panno[]" value="" placeholder="Pan No." onchange="validatePanNumber(this)" maxlength="10" required>
                                     <span style="display: none; color:#a94442; font-size: 85%;" id="pancard">Please enter valid Pancard number</span>
                                </div>
                               
                            </div>
                            <div class="col-sm-3 nopadding">
                                <div class="form-group">
                                    <small>Passport</small>
                                    <input type="text" class="form-control" id="passport" name="passportno[]"placeholder="Passport No." onchange="validatePassport(this)" minlength="5" maxlength="15">
                                    <span style="display: none; color:#a94442; font-size: 85%;" id="passportNo">Please enter 7 OR 8 digit unique Passport number</span>
                                </div>
                            </div>
                            <div class="col-sm-3 nopadding" style="display: none;" id="passdate">
                                <div class="form-group">
                                    <small>Passport expiry date</small>
                                    <input type="text" class="form-control passportx" id="passportdate" name="passportdate[]" value="" placeholder="Passport Expiry" required data-bv-notempty="true" data-bv-notempty-message="Passport expiry date is required" readonly>
                                    
                                </div>
                            </div>
                            <div class="col-sm-3 nopadding">
                                <div class="form-group">
                                    <small>Occupation</small>
                                    <input type="text" class="form-control" id="occupation" name="occupation[]" placeholder="Occupation">
                                </div>
                            </div>
                        </div>
                        <div class="row">

                            <p class="ml-4 mb-2">Other Member</p>
                        </div>
                        <div class="input-group-btn">
                            <button class="btn btn-success btn-sm" type="button"  onclick="education_fields();"> <i class="fa fa-plus" aria-hidden="true"></i> </button>
                        </div><br>
                        <div id="education_fields">      
                        </div>
                        <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button>
                    </div>
            </div>
            <div class="row setup-content" id="details">
                <!-- <div class="col-xs-12 col-md-offset-3"> -->
                <div class="col-md-12">
                    <!-- <h3> Step 2</h3> -->
                    <div class="row">
                        <div class="col-md-6">
                             <div class="form-group">
                                <label class="control-label">Email Address</label>
                                <input maxlength="200" value="{{$user->email}}" type="email" name="contact_email" required="required" class="form-control" placeholder="Enter your Email Address."  />
                            </div>
                            <div class="form-group">
                                <label class="control-label">Approx. Annual Income</label>
                                <input maxlength="200" type="text" name="annual_income" id="annual_income" required="required" class="form-control" placeholder="Enter Approx. Annual Income"  />
                            </div>
                            <!-- <div class="form-group">
                                <label class="control-label">Name</label>
                                <input maxlength="200" name="name" type="text" required="required" class="form-control" placeholder="Enter your Name" />
                            </div> -->
                            <div class="form-group">
                                <label class="control-label">Primary Contact No.</label>
                                <input type="number" name="primary_contact" required="required" value="{{$user->mobile_number}}" class="form-control" placeholder="Enter your Primary Contact No." maxlength="10" minlength="10"  />
                            </div>
                            <div class="form-group">
                                <label class="control-label">Secondary Contact No.</label>
                                <input type="number" name="secondary_contact" class="form-control" placeholder="Enter your Secondary Contact No." maxlength="10" minlength="10"/>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Whatsapp No.</label>
                                <input type="number" name="whatsapp_number" required="required" value="{{$user->whats_up}}" class="form-control" placeholder="Enter your Whatsapp No." maxlength="10" minlength="10"/>
                            </div>
                            
                        </div>
                    
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Residence address</label>
                                  <textarea name="resi_address" required="required" class="form-control" placeholder="Enter your Residence address." rows="3"></textarea>
                            </div>
                            <div class="form-group">
                                <label class="control-label">City</label>
                                <input maxlength="200" name="city" type="text" required="required" class="form-control" placeholder="Enter your City" onkeypress="return /[a-z]/i.test(event.key)"/>
                            </div>
                            <div class="form-group">
                                <label class="control-label">State</label>
                                <input maxlength="200" name="state" type="text" required="required" class="form-control" placeholder="Enter your State" onkeypress="return /[a-z]/i.test(event.key)"/>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Pincode</label>
                                <input maxlength="6" minlength="6" id="pincode" name="pincode" type="text" required="required" class="form-control" placeholder="Enter your Pincode" />
                            </div>
                        </div>
                    </div>
                     <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button>
                </div>
            </div>
            <div class="row setup-content step-3" id="step-3">
                <div class="col-md-12">
                    <!-- <div class="col-md-3">
                    </div> -->
                    <div class="row">
                        <div class="col-md-6">
                            <!-- <h3> Step 3</h3> -->
                            <div class="form-group">
                                <label class="control-label">where did you get to know about JagJoyu’s TIP ?</label>
                                <br/>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="exampleRadios" id="inlineRadio1" value="option1">
                                    <label class="form-check-label" for="inlineRadio1">Facebook</label>
                                  </div>
                                  <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="exampleRadios" id="inlineRadio2" value="option2">
                                    <label class="form-check-label" for="inlineRadio2">Instagram</label>
                                  </div>
                                  <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="exampleRadios" id="inlineRadio3" value="option1">
                                    <label class="form-check-label" for="inlineRadio3">Linkedin</label>
                                  </div>
                                  <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="exampleRadios" id="inlineRadio4" value="option2">
                                    <label class="form-check-label" for="inlineRadio4">Twitter</label>
                                  </div>


                                  <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="exampleRadios" id="inlineRadio5" value="option1">
                                    <label class="form-check-label" for="inlineRadio5">Newspaper</label>
                                  </div>
                                  <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="exampleRadios" id="inlineRadio6" value="option2">
                                    <label class="form-check-label" for="inlineRadio6">Email</label>
                                  </div>
                                  <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="exampleRadios" id="inlineRadio7" value="option1">
                                    <label class="form-check-label" for="inlineRadio7">JagJoyu’s Member</label>
                                  </div>
                                  <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="exampleRadios" id="inlineRadio8" value="option2">
                                    <label class="form-check-label" for="inlineRadio8">Agent Name</label>
                                  </div>

                                  <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="exampleRadios" id="inlineRadio8" value="option1">
                                    <label class="form-check-label" for="inlineRadio8"> Employee Referral</label>
                                  </div>
                                  <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="exampleRadios" id="inlineRadio9" value="option2">
                                    <label class="form-check-label" for="inlineRadio9">Other</label>
                                  </div> 
                            </div>
                            @if ($user && $user->first_name)
                                <div class="form-group">
                                    <label class="control-label">Agent's Name</label>
                                    <input maxlength="200" name="ref_id" type="hidden"  class="form-control" readonly="" value="{{$user->first_name}} {{$user->last_name}}" />
                                </div>
                            @elseif($user && $user->name)
                                <div class="form-group">
                                    <label class="control-label">Agent's Name</label>
                                    <input maxlength="200" name="ref_id" type="hidden"  class="form-control" readonly="" value="{{$user->name}} {{$user->surname}}" />
                                </div>
                            @endif
                            <div class="form-group">
                                <label class="control-label">Membership Fees</label>
                                <input type="text" readonly name="plan_amount" class="form-control" value="{{$plan->mebership_fees}}">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Photo to upload</label>
                                <input type="file" class="form-control" id="file" name="photo" onchange="return fileValidation()">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group ">
                                <label for="control-label">Password</label>
                                <input type="password" class="form-control" id="password" placeholder="Password" minlength="6" required="" name='password' data-bv-notempty="true" data-bv-notempty-message="password is required" data-bv-identical="true"
                                data-bv-identical-field="c_password"
                                data-bv-identical-message="The password and its confirm password does not match. ">
                                <small class="help-block" id="CheckPasswordlength" style="display: none; color: #a94442;">Your password length must be in 6 character. </small>
                            </div>
                            <div class="form-group ">
                                <label for="control-label">Confirm Password</label>
                                <input type="password" minlength="6" class="form-control" id="c_password" placeholder="Confirm Password" required="" name='c_password' data-bv-notempty="true" data-bv-notempty-message="confirm password is required" data-bv-identical="true"
                                data-bv-identical-field="password"
                                data-bv-identical-message="The password and its confirm password does not match. ">
                                <small class="help-block" style="display: none; color: #a94442;" id="CheckPasswordMatch">Password does not match</small>
                            </div>
                            <div class="form-group">
                                <label for="control-label">Payment Mode</label><br>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input paymentmode" type="radio" name="paymentmode" id="paymentmode" value="upi">
                                    <label class="form-check-label">UPI</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input paymentmode" type="radio" name="paymentmode" id="paymentmode2" value="card" checked>
                                    <label class="form-check-label">Debit Card / Credit Card / Net Banking</label>
                                </div>
                                <!-- <select name="paymentmode" id="paymentmode" style="height: 32px;" class="form-control">
                                    <option value="">Select any one</option>
                                    <option value="upi">UPI</option>
                                    <option value="card">Card</option>
                                </select> -->
                            </div> 
                            <div class="form-group" style="display:none;" id="upi">
                                <label for="control-label">UPI</label>
                                <input type="text" class="form-control" placeholder="Enter your bank UPI" required="" name='bankupi' data-bv-notempty="true" data-bv-notempty-message="Bank UPI is required">
                            </div>
                            <div class="form-group" style="display:none;" id="note">
                                <label for="control-label">Note</label>
                                <textarea class="form-control" required="" name='note' data-bv-notempty="true" data-bv-notempty-message="Note is required"></textarea>
                            </div>
                            <!-- <button class="btn btn-primary btn-lg pull-right" type="button">Next</button> -->
                            <button class="btn btn-success btn-lg pull-right pay" type="submit" id="pay">Pay</button>
                        </div>
                        
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>
@endsection
@push('script')
<script src = "https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.15.0/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tooltipster/3.3.0/js/jquery.tooltipster.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js"></script>
<script>
    $(document).ready(function () {
        $('#form').bootstrapValidator({});
    }); 

    function checkPasswordLength() {
        var password = $("#password").val();
        
        if (password.length < 6){
            // $("#CheckPasswordMatch").html("Passwords length must be 6 character");
            $("#CheckPasswordlength").show();
            $('.pay').prop('disabled', true);
        }else{
            $("#CheckPasswordlength").hide();
            $('.pay').prop('disabled', false);
        }
        
    }
    $(document).ready(function () {
       $("#password").keyup(checkPasswordLength);
    });

    function checkPasswordMatch() {
        var password = $("#password").val();
        var confirmPassword = $("#c_password").val();
        if (password != confirmPassword){
            $("#CheckPasswordMatch").show();
            $('.pay').prop('disabled', true);
        }else{
            $("#CheckPasswordMatch").hide();
            $('.pay').prop('disabled', false);
        }
    }
    $(document).ready(function () {
       $("#c_password").keyup(checkPasswordMatch);
    });

    $(document).ready(function () {
  
      //validation
     $('input, select').tooltipster({
         trigger: 'custom',
         onlyOne: false,
         position: 'bottom',
         theme: 'tooltipster-light'
       });

        $("#form").validate({
            errorPlacement: function (error, element) {
                var lastError = $(element).data('lastError'),
                    newError = $(error).text();

                $(element).data('lastError', newError);

                if(newError !== '' && newError !== lastError){
                    $(element).tooltipster('content', newError);
                    $(element).tooltipster('show');
                }
            },
            success: function (label, element) {
                $(element).tooltipster('hide');
            }
        });

  
    /* This code handles all of the navigation stuff.
    ** Probably leave it. Credit to https://bootsnipp.com/snippets/featured/form-wizard-and-validation
    */
    var navListItems = $('div.setup-panel div a'),
            allWells = $('.setup-content'),
            allNextBtn = $('.nextBtn');

    allWells.hide();

    navListItems.click(function (e) {
        e.preventDefault();
        var $target = $($(this).attr('href')),
            $item = $(this);

        if (!$item.hasClass('disabled')) {
            navListItems.removeClass('btn-primary').addClass('btn-default');
            $item.addClass('btn-primary');
            //$('input, select').tooltipster("hide");
            allWells.hide();
            $target.show();
            $target.find('input:eq(0)').focus();
        }
    });

    /* Handles validating using jQuery validate.
    */
    allNextBtn.click(function(){
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
            curInputs = curStep.find("input"),
            isValid = true;

        //Loop through all inputs in this form group and validate them..
        for(var i=0; i<curInputs.length; i++){
            if (!$(curInputs[i]).valid()){
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }

        if (isValid){

          nextStepWizard.removeClass('disabled').trigger('click');

        $('#passportdate').on('click', function () {
            //alert("hello");
            $('.passportx').removeClass('disabled');
        });
        
        }
    });

    $('div.setup-panel div a.btn-primary').trigger('click');
    
});

function validatePanNumber(pan) {

        let pannumber = $(pan).val();
        var regex = /[a-zA-z]{5}\d{4}[a-zA-Z]{1}/;

        console.log(pannumber.match(regex));
        if (pannumber.match(regex) == null) {
            $("#pancard").show();
            $('.nextBtn').prop('disabled', true);
        }else{
            $("#pancard").hide();
            $('.nextBtn').prop('disabled', false);
        }
    }

    function validatePassport(pass) {

            let passnumber = $(pass).val();
           // var regex = /[A-z]{5}\d{4}[A-Z]{1}/;
            var regex = /[A-Z][0-9]{7}$/;
            console.log(passnumber.match(regex));
            if (passnumber.match(regex) ==  null) {
                $("#passportNo").show();
                $('.nextBtn').prop('disabled', true);
            }else{
                $("#passportNo").hide();
                $('.nextBtn').prop('disabled', false);
            }

            if(passnumber){
                //alert('done');
                $("#passdate").show();
                //$('#passportdate').parents("div.form-group").addClass("error");
            }else{
                //alert('show');
            }


    }


    function validatePanNumber2(pan2) {
        
        let pannumber2 = $(pan2).val();
        //alert(pannumber2);
        var regex = /[a-zA-z]{5}\d{4}[a-zA-Z]{1}/;

        console.log(pannumber2.match(regex));
        if (pannumber2.match(regex) == null) {
            alert("Enter valid pancard number");   
           // $("#panno2:first").addClass("has-error");         
            $('.nextBtn').prop('disabled', true);
        }else{
            $("#pancard2").hide();
            $('.nextBtn').prop('disabled', false);
        }
    }

    
    function validatePassport2(pass) {

        let passnumber2 = $(pass).val();
        var regex = /[A-Z][0-9]{7}$/;
        console.log(passnumber2.match(regex));
        if (passnumber2.match(regex) ==  null) {
            $("#passportNo2").show();
            $('.nextBtn').prop('disabled', true);
        }else{
            $("#passportNo2").hide();
            $('.nextBtn').prop('disabled', false);
        }
        if(passnumber2){
            $("#passdate2").show();
        }
    }

        $(function() {
            $( "#dob" ).datepicker({
            minDate: new Date(1900,1-1,1), maxDate: '-18Y',
            dateFormat: 'dd/mm/yy',
            changeMonth: true,
            changeYear: true,
            yearRange: '-110:-18',
            language: "es",
        });
    });

    $(function() {
            $( "#passportdate" ).datepicker({
            minDate: new Date(),
            dateFormat: 'dd/mm/yy',
            changeMonth: true,
            changeYear: true,
            yearRange: '-110:+29',
            language: "es",
        });
    });


//     
//     $(document).ready(function () {
//         $('#tabMenu a[href="#{{ old('tab') }}"]').tab('show')
//     });

//     $(document).ready(function () {
//         $('#myTab a[href="#{{ old('tab') }}"]').tab('show')
//     });


    //adharno
    $('#city').on('input', function() {
        var cursor_pos = $(this).getCursorPosition()
        if(!(/^[a-zA-Z ']*$/.test($(this).val())) ) {
            $(this).val($(this).attr('data-value'))
            $(this).setCursorPosition(cursor_pos - 1)
            return
        }
        $(this).attr('data-value', $(this).val())
    })

    function fileValidation() {
            var fileInput = 
                document.getElementById('file');
              
            var filePath = fileInput.value;
          
            // Allowing file type
            var allowedExtensions = 
                    /(\.jpg|\.jpeg|\.png)$/i;
              
            if (!allowedExtensions.exec(filePath)) {
                alert('Invalid file type');
                fileInput.value = '';
                return false;
            } 
            else 
            {
              
                // Image preview
                if (fileInput.files && fileInput.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        document.getElementById(
                            'imagePreview').innerHTML = 
                            '<img src="' + e.target.result
                            + '"/>';
                    };
                      
                    reader.readAsDataURL(fileInput.files[0]);
                }
            }
        }

    // function validatePassword() {
    //     var p = $('#password').val(),
    //         l_errors = [];
    //         d_errors = [];
    //     if (p.search(/[a-z]/i) < 0) {
    //         l_errors.push("Your password must contain at least one letter.");
            
    //     }
        
    //     if (p.search(/[0-9]/) < 0) {
    //         d_errors.push("Your password must contain at least one digit."); 
            
    //     }
       
    //     if (l_errors.length > 0) {
    //         $('#letter').show();
    //         $('#digit').hide();
            
    //         return false;
    //     }else if(d_errors.length > 0){
    //         $('#digit').show();
    //         $('#letter').hide();
            
    //         return false;
    //     }else{
    //         $('#letter').hide();
    //         $('#digit').hide();
    //     }
    //     return true;
    // }

    var room = 1;
    var $type = "Family";
    var $max = 6;
    
    $(document).on('change','#type',function(){
        var val = $(this).val();
        if(val=="group")
        {
            $type = "Group";
            $max = 20;
        }
        else
        {
            $type = "Family";
            $max = 6;
        }
    });
    function education_fields() {
        if(room>=$max)
        {
            alert('Only '+$max+' people are allowed in '+$type+' trip');
            // alert($type+' has allowed maximum '+$max+' member');
            return
        }
        room++;
        var objTo = document.getElementById('education_fields')
        var divtest = document.createElement("div");
        divtest.setAttribute("class", "form-group removeclass"+room);
        var rdiv = 'removeclass'+room;
        divtest.innerHTML = `<div class="row">
                                <div class="col-sm-3 nopadding">
                                    <div class="form-group">
                                        <select name="prefix[]" style="height: 32px;" class="form-control" required data-bv-notempty="true" data-bv-notempty-message="Please Select Prefix">
                                            <option value="Mr.">Mr.</option>
                                            <option value="Ms.">Ms.</option>
                                            <option value="Mrs.">Mrs.</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-3 nopadding">
                                    <div class="form-group">
                                        <input type="text" class="form-control" required id="name" name="name[]" value="" placeholder="Name" data-bv-notempty="true" data-bv-notempty-message="Name is required">
                                    </div>
                                </div>
                                <div class="col-sm-3 nopadding">
                                    <div class="form-group">
                                        <input type="text" class="form-control dateofbirth" required id="dateofbirth`+room+`" name="dob[]" value="" placeholder="DOB" data-bv-notempty="true" data-bv-notempty-message="DOB is required" readonly>
                                    </div>
                                </div>

                                <div class="col-sm-3 nopadding">

                                    <div class="form-group">
                                        <input type="text" class="form-control" id="adharno" name="adharno[]" value="" min="0" minlength="12" placeholder="Aadhar No." required data-bv-notempty="true" data-bv-notempty-message="Aadhar No. is required">
                                    </div>
                                </div>
                                <div class="col-sm-3 nopadding">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="panno2" name="panno[]" value="" placeholder="Pan No." onchange="validatePanNumber2(this)" required data-bv-notempty="true" data-bv-notempty-message="Pann No. is required">
                                        <span style="display: none; color:#a94442; font-size: 85%;" id="pancard2">Please enter valid Pancard number</span>
                                    </div>
                                </div>
                                <div class="col-sm-3 nopadding">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="passport" name="passportno[]" value="" placeholder="Passport No." onchange="validatePassport2(this)">
                                        <span style="display: none; color:#a94442; font-size: 85%;" id="passportNo2">Please enter 7 OR 8 digit unique Passport number</span>
                                    </div>
                                </div>
                                <div class="col-sm-3 nopadding">
                                    <div class="form-group">
                                        <input type="text" class="form-control  passportdate" id="passportdate`+room+`" name="passportdate[]" value="" placeholder="Passport expiry date." readonly>
                                       
                                    </div>
                                </div>
                                <div class="col-sm-2 nopadding">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="occupation" name="occupation[]" value="" placeholder="Occupation">
                                    </div>
                                </div>

                                <div class="col-sm-1 nopadding">
                                    <div class="input-group-btn">
                                        <button class="btn btn-danger" type="button" onclick="remove_education_fields(`+ room +`);"><i class="fa fa-minus" aria-hidden="true"></i></span>
                                        </button>
                                    </div>
                                </div> 
                            </div>
                    </div>`;
        
        objTo.appendChild(divtest);
        $('#addMember').bootstrapValidator('addField', 'prefix[]');
        $('#addMember').bootstrapValidator('addField', 'name[]');
        $('#addMember').bootstrapValidator('addField', 'dob[]');
        $('#addMember').bootstrapValidator('addField', 'adharno[]');
        $('#addMember').bootstrapValidator('addField', 'panno[]');
        //$('#addMember').bootstrapValidator('addField', 'passportno[]');
        $('#addMember').bootstrapValidator('addField', 'passportdate[]');
        //$('#addMember').bootstrapValidator('addField', 'occupation[]');
        //date picker for passport date
        $( ".dateofbirth" ).datepicker({
            minDate: new Date(1900,1-1,1), maxDate: '-18Y',
            dateFormat: 'dd/mm/yy',
            changeMonth: true,
            changeYear: true,
            yearRange: '-110:-18'
        });
        //date picker for passport date
        $( ".passportdate" ).datepicker({
            minDate: new Date(),
            dateFormat: 'dd/mm/yy',
            changeMonth: true,
            changeYear: true,
            yearRange: '-110:+29'
        });

    }

    function remove_education_fields(rid) {
        room--;
       $('.removeclass'+rid).remove();
    }

    //$('.paymentmode').on('change', function() {
    $('input[type=radio][name=paymentmode]').change(function() {
      //var paymentmode = $(".paymentmode").val();
     
        if(this.value == 'upi'){
             $("#upi").show();
             $("#note").show();
             // $("#qr").show();
        }else if (this.value == 'card'){
            $("#upi").hide();
             $("#note").hide();
             // $("#qr").hide();
        }
        
    });

    $('#name').keypress(function (e) {
        var regex = new RegExp("^[a-zA-Z]+$");
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (regex.test(str)) {
            return true;
        }
        else
        {
        e.preventDefault();
        alert('Please Enter Alphabate');
        return false;
        }
    });

    $(document).ready(function () {
      //called when key is pressed in textbox
      $("#annual_income").keypress(function (e) {
         //if the letter is not digit then display error and don't type anything
         if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            //display error message
            //$("#errmsg").html("Digits Only").show().fadeOut("slow");
            alert('Please Enter Digit only');
                   return false;
        }
       });
    });

    $(document).ready(function () {
      //called when key is pressed in textbox
      $("#pincode").keypress(function (e) {
         //if the letter is not digit then display error and don't type anything
         if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            //display error message
            //$("#errmsg").html("Digits Only").show().fadeOut("slow");
            alert('Please Enter Digit only');
                   return false;
        }
       });
    });
    

// ------------step-wizard-------------
$(document).ready(function () {
    $('.nav-tabs > li a[title]').tooltip();
    
    //Wizard
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {

        var target = $(e.target);
    
        if (target.parent().hasClass('disabled')) {
            return false;
        }
    });

    $(".next-step").click(function (e) {

        var active = $('.wizard .nav-tabs li.active');
        active.next().removeClass('disabled');
        nextTab(active);

    });
    $(".prev-step").click(function (e) {

        var active = $('.wizard .nav-tabs li.active');
        prevTab(active);

    });
});

function nextTab(elem) {
    $(elem).next().find('a[data-toggle="tab"]').click();
}
function prevTab(elem) {
    $(elem).prev().find('a[data-toggle="tab"]').click();
}


$('.nav-tabs').on('click', 'li', function() {
    $('.nav-tabs li.active').removeClass('active');
    $(this).addClass('active');
});



</script>
@endpush