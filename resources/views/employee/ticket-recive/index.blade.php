@extends('employee.master')
@section('bodyData')
    <div class="outer-w3-agile mt-3" style='min-height:400px'>
        <div class="row">
            <div class="col-lg-2 col-md-4">

                <div class="single-widget-search-input">
                    <select class=" w-100 custom-select" id="my_user">
                        <option value="">User type</option>
                        <option value="employee">Employee</option>
                        <option value="agent">Agent</option>
                        <option value="customer">Customer</option>
                    </select>
                </div>
            </div>
            <div class="col-lg-3 col-md-4">



                <div class="single-widget-search-input">
                    <select class="form-control  w-100 custom-select" id="my_category_id">
                        <option value="">Category</option>
                        @if ($ticket_category)
                            @foreach ($ticket_category as $sub)
                                @if ($sub)
                                    <option value="{{ $sub->id }}">
                                        {{ $sub->name }}</option>
                                @endif
                            @endforeach
                        @endif
                    </select>
                </div>

            </div>
            <div class="col-lg-2 col-md-4">

                <div class="single-widget-search-input">
                    <select class=" w-100 custom-select" id="my_category_sub_id">

                    </select>
                </div>

            </div>
            <div class="col-lg-2 col-md-4 ">
                <div class="single-widget-search-input">
                    <select class=" w-100 custom-select" id="my_status">
                        <option value="">Ticket Status</option>
                        <option value="active">Active</option>
                        <option value="close">Close</option>

                    </select>
                </div>
            </div>
            <div class="col-lg-3 col-md-8 ">
                <div class="single-widget-search-input">
                    <div id="reportrange"
                        style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                        <i class="fa fa-calendar"></i>&nbsp;
                        <span></span> <i class="fa fa-caret-down"></i>
                    </div>
                </div>
            </div>

        </div>
        <br>
        <div class="table-responsive">
            <table id="example" class="table" style="width: 100%">
                <thead class="thead-dark">
                    <tr>
                        {{-- <th scope="col">Subject</th> --}}
                        <th scope="col">User</th>
                        <th scope="col">Categories</th>
                        <th scope="col">Sub Categories</th>
                        <th scope="col">Status</th>
                        <th scope="col">Created at</th>
                        <th>Actions</th>


                    </tr>
                </thead>

            </table>
        </div>
        <br> <br><br> <br> <br><br>



    </div>

@endsection
@push('css')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<style type="text/css">
        div#example_filter input[type="search"] {
            box-sizing: border-box;
            border: 1px solid #d2d6dc;
        }
</style>
@endpush
@push('js')
{{-- <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script> --}}
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <script type="text/javascript">
        $(document).on("click", '.agentdelete', function(e) {
            e.preventDefault();
            var id = $(this).attr('data-id');
            var status=$(this).attr('data-status');
            swal({
                    title: "Are you sure?",
                    text: "You want to close this ticket.",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {

                        $.ajax({
                            type: "POST",
                            url: "{{ route('ticket-receive-status-chnage') }}",
                            data: {
                                "id": id,
                                "status":status,
                                "_token": "{{ csrf_token() }}"
                            },
                            success: function(response) {
                                if (response.status == "success") {
                                    swal(response.message, {
                                        icon: 'success',
                                    }).then((result) => {
                                        $('#example').DataTable().ajax.reload(null, false);
                                    });
                                } else {
                                    swal("Error", "Something went wrong!", {
                                        icon: 'error'
                                    });
                                }
                            }
                        });
                    }
                });
        });

        $(function() {

            var start = moment().subtract(29, 'days');
            var end = moment();

            function cb(start, end) {
                $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                var category = {};
                if ($('#my_category_id').val() != "") {
                    category['category_id'] = $('#my_category_id').val();
                }

                if ($('#my_category_sub_id').val() != '') {
                    category['sub_category_id'] = $('#my_category_sub_id').val();
                }
                if ($('#my_status').val() != '') {
                    category['status'] = $('#my_status').val();
                }
                let startDate = moment($('#reportrange').data('daterangepicker').startDate).format(
                    'YYYY-MM-DD HH:mm:ss');
                let endDate = moment($('#reportrange').data('daterangepicker').endDate).format(
                    'YYYY-MM-DD HH:mm:ss');
                if (startDate) {
                    category['start_date'] = startDate;
                }

                if (endDate) {
                    category['end_date'] = endDate;
                }
                if ($('#my_user').val() != '') {
                    category['user'] = $('#my_user').val();
                }
                getlead(category);
            }


           

            $('#reportrange').daterangepicker({
                startDate: start,
                endDate: end,
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1,
                        'month').endOf('month')]
                }
            }, cb);

            cb(start, end);

        });



        function getlead(dataSet) {
            $('#example').DataTable().destroy();
            $("#example").DataTable({
                "processing": true,
                "serverSide": true,
                "ordering": false,
                "ajax": {
                    "url": "{{ url('ticket-receive/list') }}",
                    "type": "POST",
                    "data": dataSet,
                },
                "columns": [{
                        "data": "id",
                        render: function(data, type, row, alldata) {
                            var name = '';
                            if (row.user != null) {
                                name = row.user.name + ' (Employee)';
                            } else if (row.agent != null) {
                                name = row.agent.first_name + ' ' + row.agent.last_name + ' (agent)';
                            } else if (row.customer != null) {
                                name = row.customer.first_name + ' ' + row.customer.last_name +
                                    ' (customer)';
                            } else {
                                name = '';
                            }
                            return name;

                        }
                    },

                    {
                        "data": "ticket_categories.name"
                    },
                    {
                        "data": "ticket_sub_categories.name"
                    }
                    ,
                    {
                        "data": "status",
                        render: function(data, type, row, meta) {

                            if(row.status=='active'){
                                return'<span class="badge badge-success">'+row.status+'</span>';  
                            }else{
                                return'<span class="badge badge-danger">'+row.status+'</span>';  
                            }
                            // body...
                        }
                    },
                    {
                        "data": "created_at",
                        render: function(data, type, row, meta) {

                            return moment(data).format('DD-MM-YYYY,h:mm a');
                            // body...
                        }
                    },
                    {
                        "data": "id",
                        render: function(data, type, row, meta) {
                            //console.log(row.status);
                            var status ='';
                            if(row.status=="active"){
                                status='<a href="#" class="ml-1 agentdelete"  data-id="'+btoa(data)+'" data-status="close"><i class="fa fa-window-close"'+
                                 'aria-hidden="true" style="color:red;font-size:20px" title="Colse ticket"></i>'+
                                 '</a>';
                            }else{
                                status='';
                            }

                            var html =  '<a  href="{{ url('ticket-receive/view/') }}/' + btoa(data) +
                                '"><i class="fa fa-eye" aria-hidden="true"' +
                                'title="Detail View" style="font-size:20px"></i></a>'+status;

                            return html;

                        }

                    },
                ]
            });


        }

        $(document).ready(function() {

            var dataSet = {};
            let startDate = moment($('#reportrange').data('daterangepicker').startDate).format(
                    'YYYY-MM-DD HH:mm:ss');
            let endDate = moment($('#reportrange').data('daterangepicker').endDate).format(
                    'YYYY-MM-DD HH:mm:ss');
            if (startDate) {
                    dataSet['start_date'] = startDate;
            }

            if (endDate) {
                    dataSet['end_date'] = endDate;
            }
            getlead(dataSet);

                $('select#c_state_id').change(function() {
                    var value = $(this).val();
                    var city = '';
                    //console.log(value);
                    statechange(value, city);


                });

                $('#my_category_id').change(function() {
                    var value = $('#my_category_id').val();
                   // console.log();
                    var city = '';
                   // console.log(value);
                    newstatechange(value, city);
                    var category = {};

                    if($('#my_category_id').val()!="") {
                        category['category_id'] = $('#my_category_id').val();

                    }else{
                        category['category_id'] = '';
                    }
                    //var v =$('#my_category_sub_id').val();
                    //console.log(v);
                    if ($('#my_category_sub_id').val()!="") {
                        category['sub_category_id'] = $('#my_category_sub_id').val();
                    }else{
                        category['sub_category_id'] = '';
                    }

                    if ($('#my_status').val() != '') {
                        category['status'] = $('#my_status').val();
                    }

                    let startDate = moment($('#reportrange').data('daterangepicker').startDate).format(
                        'YYYY-MM-DD HH:mm:ss');
                    let endDate = moment($('#reportrange').data('daterangepicker').endDate).format(
                        'YYYY-MM-DD HH:mm:ss');
                    if (startDate) {
                        category['start_date'] = startDate;
                    }

                    if (endDate) {
                        category['end_date'] = endDate;
                    }
                    if ($('#my_user').val() != '') {
                        category['user'] = $('#my_user').val();
                    }
                    getlead(category);

                });



                $('#my_user').change(function() {
                    var value = $(this).val();

                    var city = '';

                    var category = {};

                    if (value != '') {
                        category['user'] = value;

                    }
                    if ($('#my_category_id').val() != '') {
                        category['category_id'] = $('#my_category_id').val();
                    }

                    if ($('#my_category_sub_id').val() != '') {
                        category['sub_category_id'] = $('#my_category_sub_id').val();
                    }

                    let startDate = moment($('#reportrange').data('daterangepicker').startDate).format(
                        'YYYY-MM-DD HH:mm:ss');
                    let endDate = moment($('#reportrange').data('daterangepicker').endDate).format(
                        'YYYY-MM-DD HH:mm:ss');
                    if (startDate) {
                        category['start_date'] = startDate;
                    }

                    if (endDate) {
                        category['end_date'] = endDate;
                    }
                    getlead(category);

                });


                $('#my_category_sub_id').change(function() {
                    var value = $(this).val();
                    var city = '';

                    var category = {};

                    if (value != '') {
                        category['sub_category_id'] = value;

                    }
                    if ($('#my_category_id').val() != '') {
                        category['category_id'] = $('#my_category_id').val();
                    }

                    if ($('#my_category_sub_id').val() != '') {
                        category['sub_category_id'] = $('#my_category_sub_id').val();
                    }

                    let startDate = moment($('#reportrange').data('daterangepicker').startDate).format(
                        'YYYY-MM-DD HH:mm:ss');
                    let endDate = moment($('#reportrange').data('daterangepicker').endDate).format(
                        'YYYY-MM-DD HH:mm:ss');
                    if (startDate) {
                        category['start_date'] = startDate;
                    }

                    if (endDate) {
                        category['end_date'] = endDate;
                    }
                    if ($('#my_user').val() != '') {
                        category['user'] = $('#my_user').val();
                    }
                    getlead(category);

                });


                $('select#my_status').change(function() {
                    var value = $(this).val();
                    var city = '';
                    //newstatechange(value, city);
                    var category = {};

                    if (value != '') {
                        category['status'] = value;

                    }
                    if ($('#my_category_id').val() != '') {
                        category['category_id'] = $('#my_category_id').val();
                    }

                    if ($('#my_status').val() != '') {
                        category['status'] = $('#my_status').val();
                    }
                    let startDate = moment($('#reportrange').data('daterangepicker').startDate).format(
                        'YYYY-MM-DD HH:mm:ss');
                    let endDate = moment($('#reportrange').data('daterangepicker').endDate).format(
                        'YYYY-MM-DD HH:mm:ss');
                    if (startDate) {
                        category['start_date'] = startDate;
                    }

                    if (endDate) {
                        category['end_date'] = endDate;
                    }

                    if ($('#my_user').val() != '') {
                        category['user'] = $('#my_user').val();
                    }

                    getlead(category);

                });
            });


            function newstatechange(value, city) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var cat_id = value;
            $.ajax({
                url: "{{ route('ticket-receive-getCat') }}",
                type: "POST",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "cat_id": cat_id
                },

                success: function(data, city) {
                    $('#my_category_sub_id').empty();
                    $('#my_category_sub_id').append('<option value="" selected>Sub category</option>');
                    $.each(data.cat, function(index, cat) {


                        $('#my_category_sub_id').append('<option value="' + cat.id + '">' + cat.name +
                            '</option>');


                    })
                }
            })
        }


    </script>

@endpush
