@extends('admin.master')
@section('bodyData')
@if ($message = Session::get('profile_sucess'))
<div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>	
    <strong>{{ $message }}</strong>
</div>
@endif
<section class="forms-section" style='height:400px'>

<div class="outer-w3-agile mt-3">
        <h4 class="tittle-w3-agileits mb-4">Change Profile</h4>
        <form action="{{url('updateAdmin')}}" method="post">
        @csrf
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputEmail4">Name</label>
                    <input type="text" class="form-control" id="inputEmail4" name = 'name' placeholder="Enter Name" required="" value={{$data->name}}>
                </div>
                <div class="form-group col-md-6">
                    <label for="inputPassword4">Contact Number</label>
                    <input type="text" class="form-control" id="inputPassword4" name='contact_number'placeholder="Last Name" required="" value={{$data->contact_number}}>
                </div>
            </div>
           
            
            <button type="submit" class="btn btn-primary">Update</button>
        </form>
    </div>

</section>

@endsection
