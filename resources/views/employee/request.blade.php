@extends('employee.master')
@section('bodyData')

<div class="outer-w3-agile mt-3" style='min-height:400px'>
   
    <!-- <h4 class="tittle-w3-agileits mb-4">
                    Table Head Options</h4> -->
    <table class="table">
        <thead class="thead-dark">
            <tr>
               
              
               
                <th scope="col">Apply Date</th>
                <th scope="col">Leave Date</th>
             	<th scope="col">Reason</th>
                <th scope="col">Days</th>
                <th scope="col">Status</th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
        
        <?php foreach($LeaveInfo as $l) {?>
            <tr>
                
                <td><?php echo $l->leave_date_start?></td>
            	<td><?php echo $l->leave_date_end?></td>
             <td><?php echo $l->reason ?></td>
            <td><?php echo $l->total_days ?></td>
            
                <td><?php echo $l->approve_status?></td>
               
               
            <td><a href='/empapprovenotify/<?php echo $l->id?>'><button class='btn btn-primary'>Ok</button></a></td>
            
            <?php } ?>
               
            </tr>  
            
           
        </tbody>
    </table>
    


</div>
@endsection
